<?php

namespace frontend\modules\user\models;

use cheatsheet\Time;
use common\commands\SendEmailCommand;
use common\models\User;
use common\models\UserProfile;
use common\models\UserToken;
use frontend\modules\user\Module;
use yii\base\Exception;
use yii\base\Model;
use Yii;
use yii\helpers\Url;

/**
 * Signup form
 */
class SignupForm extends Model {
    /**
     * @var
     */
    public $username;
    public $firstName;
    public $lastName;
    /**
     * @var
     */
    public $email;
    /**
     * @var
     */
    public $password;

    public $accept_terms;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['username', 'firstName', 'lastName'], 'trim'],
            [['firstName', 'lastName', 'password', 'email', 'accept_terms'], 'required'],

            [['firstName', 'lastName'], 'string', 'min' => 2],
            [
                'username',
                'unique',
                'targetClass' => '\common\models\User',
                'message'     => Yii::t('frontend', 'This username has already been taken.'),
            ],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'email'],
            [
                'email',
                'unique',
                'targetClass' => '\common\models\User',
                'message'     => Yii::t('frontend', 'This email address has already been taken.'),
            ],

            ['password', 'string', 'min' => 6],

            ['accept_terms', 'boolean',],
            ['accept_terms', 'compare', 'compareValue' => 1, 'operator' => '==', 'type' => 'number', 'message' => 'Подтвердите соглашение.'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels() {
        return [
            'username'     => Yii::t('frontend', 'Username'),
            'firstName'    => Yii::t('frontend', 'First name'),
            'lastName'     => Yii::t('frontend', 'Last name'),
            'email'        => Yii::t('frontend', 'E-mail'),
            'password'     => Yii::t('frontend', 'Password'),
            'accept_terms' => '',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup() {
        if ($this->validate()) {
            $shouldBeActivated = $this->shouldBeActivated();
            $user              = new User();
            $user->username    = $this->username;
            $user->email       = $this->email;
            $user->status      = $shouldBeActivated ? User::STATUS_NOT_ACTIVE : User::STATUS_ACTIVE;
            $user->setPassword($this->password);
            if (!$user->save()) {
                throw new Exception("User couldn't be  saved");
            };
            $user->afterSignup([
                'firstname' => $this->firstName,
                'lastname' => $this->lastName,
            ]);
            if ($shouldBeActivated) {
                $token = UserToken::create(
                    $user->id,
                    UserToken::TYPE_ACTIVATION,
                    Time::SECONDS_IN_A_DAY
                );
                Yii::$app->commandBus->handle(new SendEmailCommand([
                    'subject' => Yii::t('frontend', 'Activation email'),
                    'view'    => 'activation',
                    'to'      => $this->email,
                    'params'  => [
                        'url' => Url::to(['/user/sign-in/activation', 'token' => $token->token], true),
                    ],
                ]));
            }

            return $user;
        }

        return null;
    }

    /**
     * @return bool
     */
    public function shouldBeActivated() {
        /** @var Module $userModule */
        $userModule = Yii::$app->getModule('user');
        if (!$userModule) {
            return false;
        } elseif ($userModule->shouldBeActivated) {
            return true;
        } else {
            return false;
        }
    }
}
