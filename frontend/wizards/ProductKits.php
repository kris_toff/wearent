<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 19.06.2017
 * Time: 14:48
 */

namespace frontend\wizards;


use common\models\Product;
use common\models\ProductAmazing;
use frontend\models\ProductSearch;
use yiimodules\categories\models\Categories;

class ProductKits {
    public static function getListForSlider($category_id, $limit = 15) {
        $category = Categories::findOne(['id' => $category_id, 'is_active' => 1]);
        if (is_null($category)) {
            return [];
        }

        return [
            'id' => $category['id'],
            'title'  => $category['name'],
            'slides' => Product::find()
                ->where(['is_deleted' => 0, 'category_id' => $category->id, 'status' => Product::OPEN])
                ->orderBy(['created_at' => SORT_DESC])
                ->limit($limit)
                ->all(),
        ];
    }

    public static function getListForSliderAmazing($limit) {
        return [
            'title'  => \Yii::t('frontend', 'Amazing goods'),
            'slides' => Product::find()->where(['is_deleted' => 0, 'status' => Product::OPEN])->leftJoin([
                'pa' => ProductAmazing::find()
                    ->select(['product_id', 'cpa' => 'COUNT([[product_id]])'])
                    ->groupBy('product_id'),
            ], '{{pa}}.[[product_id]] = ' . Product::tableName() . '.[[id]]')
                ->andWhere(['>', 'cpa', 0])
                ->orderBy([
                    'cpa'        => SORT_DESC,
                    'created_at' => SORT_DESC,
                ])->limit($limit)->all(),
        ];

        return [
            'title'  => 'Необычные товары',
            'slides' => Product::find()
                ->where(['is_deleted' => 0, 'is_amazing' => 1])
                ->orderBy(['created_at' => SORT_DESC])
                ->limit($limit)
                ->all(),
        ];
    }
}