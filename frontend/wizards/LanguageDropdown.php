<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 01.07.2017
 * Time: 0:37
 */

namespace frontend\wizards;

use yii\helpers\Html;
use yii\bootstrap\Dropdown;

class LanguageDropdown extends Dropdown {
    public function init() {
        $this->encodeLabels = false;

        $route       = \Yii::$app->controller->route;
        $appLanguage = \Yii::$app->language;
//        $appLanguage = substr(\Yii::$app->language, 0, 2);

        $params = \Yii::$app->request->get();
        array_unshift($params, "/$route");

        foreach (\Yii::$app->languages->list as $k => $item) {
            if ($k == $appLanguage) continue;

            $params['language'] = $k;
            $this->items[]      = [
                'label' => '<span class="labelImg">' .Html::img('/img/footer/lang_flags/flag-'
                        . strtolower(substr($k, 0, 2))
                        . '.png') . '</span> ' . \Yii::t('UI21', mb_strtoupper('$footer_language_' . $k . '$', 'UTF-8')),
                'url'   => $params,
            ];
        }

        parent::init();
    }
}