/**
 * Created by Planess group on 01.09.2017.
 */

(function(angular, $, _) {
    "use strict";

    var app = angular.module("pgaPage");

    app
        .controller("ProfileController", [
            "$log",
            "$scope",
            "$http",
            "$timeout",
            "$mdMedia",
            "Emails",
            "Phones",
            function($log, $scope, $http, $timeout, $mdMedia, Emails, Phones) {
                var tabsl = $("#update_info_form-tabs").find("[data-toggle='tab']");
                var subf = $("#update_info-form").find(".blockTwoBtn");

                tabsl.on("tabsX.click", function(event) {
                    subf.toggleClass("hidden", $.inArray(tabsl.index(event.currentTarget), [1, 4, 5]) >= 0);
                });

                $scope._mdMedia = $mdMedia;

                $scope.emails = Emails.query();
                $scope.phones = Phones.query();

            }
        ])
        .controller("ProfileAboutMeTabController", [
            "$scope", function($scope) {
                $scope.genderTextList = [];
                $scope.genderValue = null;
                $scope.genderText = function() {
                    return _.get($scope.genderTextList, _.toInteger($scope.genderValue));
                };

                $scope.days = _.range(1, 31);
                $scope.months = moment.months();
                // $scope.months = _.range(1, 13);
                $scope.years = _.range(1965, moment().year() - 15);

                $scope.$watch("birthdayMonth", function(o) {
                    var m = _.toInteger(o);
                    if (m > 0) {
                        $scope.days = _.range(1, 1 + moment().set("month", m - 1).daysInMonth());
                    } else {
                        $scope.days = _.range(1, 31);
                    }

                    if ($scope.birthdayDay) {
                        $scope.birthdayDay = _.clamp($scope.birthdayDay, 1, _.last($scope.days));
                    }
                });

                $scope.setDay = function(n) {
                    $scope.birthdayDay = n;
                };
                $scope.setMonth = function(n) {
                    $scope.birthdayMonth = n;
                };
                $scope.setYear = function(n) {
                    $scope.birthdayYear = n;
                };
            }
        ])
        .controller("ProfileMoreAboutMeTabController", [
            "$scope", "$mdPanel", "$mdMedia", function($scope, $mdPanel, $mdMedia) {
                var languages = {
                    id: "Bahasa Indonesia",
                    ms: "Bahasa Malaysia",
                    bn: "Bengali",
                    da: "Dansk",
                    de: "Deutsch",
                    en: "English",
                    es: "Español",
                    fr: "Français",
                    hi: "Hindi",
                    it: "Italiano",
                    hu: "Magyar",
                    nl: "Nederlands",
                    nn: "Norsk",
                    pl: "Polski",
                    pt: "Português",
                    pa: "Punjabi",
                    isl: "Sign Language", // язык жестов
                    se: "Suomi",
                    sv: "Svenska",
                    tl: "Tagalog",
                    tr: "Türkçe",
                    cs: "Čeština",
                    el: "Ελληνικά",
                    ru: "Русский",
                    uk: "Українська",
                    heb: "עברית",
                    ar: "العربية",
                    th: "ภาษาไทย",
                    zh: "中文",
                    ja: "日本語",
                    ko: "한국어"
                };
                $scope.LanguageList = [];
                $scope.languages = languages;

                $scope.updateLanguages = function(l) {
                    $scope.LanguageList = l;
                };

                $scope.openLangBox = function() {
                    var position = $mdPanel.newPanelPosition()
                        .relativeTo("#chips_languages")
                        .addPanelPosition($mdPanel.xPosition.ALIGN_START, $mdPanel.yPosition.ALIGN_BOTTOMS);

                    $mdPanel.open({
                        // contentElement: "#language_box",
                        templateUrl: "/profile/template?run=lang_box",
                        position: position,
                        hasBackdrop: true,
                        zIndex: 1500,
                        fullscreen: $mdMedia('max-width: 768px'),
                        // disableParentScroll: true,
                        escapeToClose: true,
                        clickOutsideToClose: true,
                        locals: {
                            lng: $scope.languages,
                            clng: $scope.LanguageList,
                            ul: $scope.updateLanguages
                        },
                        controller: [
                            "mdPanelRef", "$scope", function(mdPanelRef, $scope) {
                                var ctrl = this;
                                var list = angular.copy(ctrl.clng);

                                $scope._mdMedia = $mdMedia;

                                ctrl.close = function() {
                                    mdPanelRef.close();
                                };
                                ctrl.save = function() {
                                    ctrl.ul(list);
                                    ctrl.close();
                                };
                                $scope.toggle = function(v) {
                                    var idx = $.inArray(v, list);
                                    if (idx >= 0) {
                                        _.pull(list, v);
                                    } else {
                                        list.push(v);
                                    }
                                };

                                $scope.isSelected = function(n) {
                                    return $.inArray(n, list) >= 0;
                                };
                            }
                        ],
                        controllerAs: "ctrl"
                    })
                };
            }
        ])
        .controller("ProfileContactsTabController", [
            "$scope",
            "$timeout",
            "$window",
            "$mdDialog",
            "$mdToast",
            "$mdPanel",
            "Emails",
            "Phones",
            function($scope, $timeout, $window, $mdDialog, $mdToast, $mdPanel, Emails, Phones) {
                $scope._location = $window.location;

                $scope.addNewEmail = function() {
                    if (!_.some($scope.emails, "is_new")) {
                        $scope.emails.push(new Emails({
                            is_new: true
                        }));
                    }
                };
                $scope.removeEmail = function(idx) {
                    if (_.get($scope.emails, [idx, "is_new"], false)) {
                        $scope.emails.splice(idx, 1);
                    } else {
                        var c = $mdDialog.confirm()
                            .title($scope.confirm_email_title)
                            .textContent($scope.confirm_email_question)
                            .ok($scope.confirm_email_ok_btn)
                            .cancel($scope.confirm_email_cancel_btn);

                        $mdDialog.show(c).then(function() {
                            $scope.emails[idx].$delete();
                            $scope.emails.splice(idx, 1);
                        });
                    }
                };
                $scope.saveEmail = function(idx, model) {
                    _.unset($scope.emails[idx]["is_occupied"]);

                    var oldCopy = angular.copy($scope.emails[idx]);
                    model = _.omit(model, ["id", "is_new"]);
                    _.assign($scope.emails[idx], model);

                    if (_.get($scope.emails, [idx, "is_new"], false)) {
                        // только добавлен адрес, сохранить
                        $scope.emails[idx].$save(null, function() {
                            _.unset($scope.emails, [idx, "is_new"]);

                            $.notify({
                                message: "Great, email saved."
                            }, {
                                type: "warning"
                            });
                        }, function(response) {
                            // console.log("response",response);
                            if (_.has(response, "data.email")) {
                                $scope.emails[idx]["is_occupied"] = true;
                            }

                            $.notify({
                                message: "Opps, save failed."
                            }, {
                                type: "warning"
                            });
                        });
                    } else {
                        // уже существует, обновить
                        $scope.emails[idx].$update(null, function() {
                            oldCopy = undefined;

                            $.notify({
                                message: "Great, email saved."
                            }, {
                                type: "warning"
                            });
                        }, function(response) {
                            $scope.emails[idx] = angular.copy(oldCopy);
                            oldCopy = undefined;

                            // console.log("response", response);
                            if (_.has(response, "data.email")) {
                                $scope.emails[idx]["is_occupied"] = true;
                            }

                            $.notify({
                                message: "Opps, save failed."
                            }, {
                                type: "warning"
                            });
                        });
                    }
                };
                $scope.makeLogin = function(idx) {
                    $scope.emails[idx].$login({login: 1}, function(v) {
                        // обновление успешное
                        var fk = _.findKey($scope.emails, function(o) {
                            return _.toInteger(o.is_main);
                        });
                        if (!_.isUndefined(fk)) {
                            $scope.emails[fk].id = v.id;
                            $scope.emails[fk].is_main = 0;
                        }
                        $scope.emails[idx].id = -1;
                        $scope.emails[idx].is_verify = 1;
                        $scope.emails[idx].is_main = 1;
                    }, function(response) {
                        if (response.status === 406) {
                            // нет пароля
                            var position = $mdPanel.newPanelPosition()
                                .relativeTo("#emailfield-" + $scope.emails[idx].id)
                                .addPanelPosition($mdPanel.xPosition.OFFSET_END, $mdPanel.yPosition.CENTER)
                                .withOffsetX('18px');

                            $mdPanel.open({
                                templateUrl: "/profile/template?run=set_pass",
                                controller: [
                                    "mdPanelRef", "$scope", "$http", "email",
                                    function(mdPanelRef, $scope, $http, email) {
                                        var ctrl = this;
                                        ctrl.email = email.email;

                                        _.set($scope, ["rack", "email"], ctrl.email);

                                        $scope.submitSetPassForm = function() {
                                            if ($scope.setPasswordForm.$valid && !$scope.setPasswordForm.$pending) {
                                                $http.post("/profile/set-user-login", $scope.rack)
                                                    .then(function(response) {
// сохранился логин
                                                        email.is_main = 1;
                                                        $scope.close();
                                                    });
                                            }
                                        };
                                        $scope.checkReliablePass = function(val) {
                                            return /[A-Z]/.test(val) && /\d/.test(val) && /[\D\W\S]/.test(val);
                                        };

                                        $scope.close = function() {
                                            mdPanelRef.close();
                                        };
                                    }
                                ],
                                controllerAs: "$ctrl",
                                position: position,
                                fullscreen: $scope._mdMedia("max-width: 768px"),
                                hasBackdrop: true,
                                disableParentScroll: true,
                                locals: {
                                    email: $scope.emails[idx]
                                },
                                bindToController: false
                            });
                        }
                        // console.log(response);
                    });
                };

                $scope.addNewPhone = function() {
                    if (!_.some($scope.phones, "is_new")) {
                        $scope.phones.push(new Phones({
                            is_new: true,
                            is_public: true
                        }));
                    }
                };
                $scope.removePhone = function(idx) {
                    if (_.get($scope.phones, [idx, "is_new"], false)) {
                        $scope.phones.splice(idx, 1);
                    } else {
                        var c = $mdDialog.confirm()
                            .title($scope.confirm_phone_title)
                            .textContent($scope.confirm_phone_question)
                            .ok($scope.confirm_phone_ok_btn)
                            .cancel($scope.confirm_phone_cancel_btn);

                        $mdDialog.show(c).then(function() {
                            $scope.phones[idx].$delete();
                            $scope.phones.splice(idx, 1);
                        });
                    }
                };
                $scope.savePhone = function(idx, model) {
                    _.unset($scope.phones[idx]["is_occupied"]);

                    var oldCopy = angular.copy($scope.phones[idx]);
                    model = _.omit(model, ["id", "is_new"]);
                    _.assign($scope.phones[idx], model);

                    if (_.get($scope.phones, [idx, "is_new"], false)) {
                        // только добавлен телефон, сохранить
                        return $scope.phones[idx].$save(null, function() {
                            _.unset($scope.phones, [idx, "is_new"]);

                            $.notify({
                                message: "Great, phone saved."
                            }, {
                                type: "warning"
                            });
                        }, function(response) {
                            if (_.has(response, "data.phone")) {
                                $scope.phones[idx].is_occupied = true;
                            }

                            $.notify({
                                message: "Opps, save failed."
                            }, {
                                type: "warning"
                            });
                        });
                    } else {
                        // уже существует, обновить
                        return $scope.phones[idx].$update(null, function() {
                            oldCopy = undefined;

                            $.notify({
                                message: "Great, phone saved."
                            }, {
                                type: "warning"
                            });
                        }, function(response) {
                            $scope.phones[idx] = angular.copy(oldCopy);
                            oldCopy = undefined;

                            if (_.has(response, "data.phone")) {
                                $scope.phones[idx].is_occupied = true;
                            }

                            $.notify({
                                message: "Opps, save failed."
                            }, {
                                type: "warning"
                            });
                        });
                    }
                };

                $scope.setTrustedPhone = function(id) {
                    var v = _.find($scope.phones, {id: id});
                    v.is_verify = 1;
                };
                $scope.amountPublic = function() {
                    return _.size(_.filter($scope.phones, function(o) {
                        return _.toInteger(o.is_public);
                    }));
                };

                var isCopyVisible = true,
                    tid;
                $scope.hasCopied = false;
                $scope.isTest = true;
                $scope.isSupportCopy = function() {
                    return document.queryCommandSupported("copy") && isCopyVisible && $scope.isTest;
                };
                $scope.copyUserLink = function($event) {
                    var range = document.createRange();
                    range.selectNode(angular.element($event.target).prev("span").get(0));
                    $window.getSelection().addRange(range);

                    try {
                        if (document.execCommand("copy", false, null)) {
                            $scope.hasCopied = true;

                            if (tid) {
                                $timeout.cancel(tid);
                            }
                            tid = $timeout(function() {
                                $scope.hasCopied = false;
                            }, 3000);
                        } else {
                            isCopyVisible = false;
                        }
                    } catch (e) {
                        isCopyVisible = false;
                        console.log(e);
                    }

                    $window.getSelection().removeAllRanges();
                };
            }
        ])
        .controller("ProfileMediaTabController", [
            "$scope", "$timeout", "$http", "Avatars", function($scope, $timeout, $http, Avatars) {
                var initCrop = function(pc) {
                    var cropper;
                    pc = pc || "";

                    return function(imgORDeg, getinfo) {
                        if (getinfo) {
                            return cropper ? cropper.getData(true) : {};
                        }

                        if (cropper) {
                            if (_.isNumber(imgORDeg)) {
                                // это будет число для поворота
                                return cropper.rotate(_.round(imgORDeg));
                            } else {
                                cropper.destroy();
                            }
                        }

                        if (_.isString(imgORDeg) || _.isElement(imgORDeg)) {
                            cropper = new Cropper(imgORDeg, {
                                viewMode: 2,
                                aspectRatio: 1,
                                guides: false,
                                center: false,
                                preview: pc,
                                minCropBoxWidth: 100,
                                ready: function() {
                                    console.log(this.cropper);
                                    $(this.cropper.cropBox)
                                        .prepend("<img class='resizable-angle-picture cropper-point point-ne' src='/img/editProfile/resizeImg/resizeIconImg.svg' data-action='ne' />");
                                }
                            });
                        }
                    };
                };
                var crop = initCrop("#real_avatar_preview");
                var webPlayer;

                $scope.mkwebcam = false;
                $scope.isEmpty = true;
                $scope.isSend = false;

                $scope.sizeAvatars = function() {
                    return _.size($scope.avatars);
                };

                $scope.avatars = Avatars.query();
                $scope.mksnap_webcam = function() {
                    if ($scope.mkwebcam) {
                        // уже активна камера
                        Webcam.snap(function(data_uri, canvas, context) {
                            // console.log(data_uri, canvas, context);
                            var e = $("<img/>", {src: data_uri, class: "img-responsive"});
                            $("#avatar_preview").empty().append(e);

                            crop(e.get(0));

                            var bl = new Blob([data_uri]);
                            $timeout(function() {
                                $scope.fileSize = _.round(bl.size / 1048576, 2);
                            });

                            $timeout(function() {
                                $scope.mkwebcam = false;
                                $scope.isEmpty = false;


                                $scope.fileName = "photo_webcam_" + moment() + ".jpg";
                            });
                        });
                    } else {
                        Webcam.set({
                            width: 315,
                            height: 250,
                            dest_width: 907,
                            dest_height: 720
                        });
                        Webcam.attach("#avatar_preview");
                        $scope.mkwebcam = true;
                        $scope.isEmpty = true;

                        crop();
                    }
                };
                Webcam.on("error", function() {
                    Webcam.reset();

                    $timeout(function() {
                        $scope.mkwebcam = false;
                    });

                    console.log('error');
                });

                $scope.sendNewAvatar = function() {
                    var img = $("#avatar_preview").children("img:first");

                    if ($scope.isSend || !img.length) {
                        return;
                    }
                    $scope.isSend = true;
                    $scope.percentProgress = 0;

                    var data = _.trim(img.attr("src"));

                    var params = _.assign({
                        x: 0,
                        y: 0,
                        rotate: 0,
                        width: img.prop("naturalWidth"),
                        height: img.prop("naturalHeight")
                    }, _.pick(crop(null, true), ["x", "y", "width", "height", "rotate"]));
                    // console.log(params, crop(null, true));return;
                    var gets = _.transform(params, function(result, value, key) {
                        result.push(key + "=" + value);
                    }, []);

                    Webcam.on("uploadProgress", uploadProgress);
                    Webcam.on("uploadComplete", uploadComplete);

                    Webcam.upload(data, "/profile-avatar-rest-data?" + _.join(gets, "&"));

                    function uploadProgress(progress) {
                        $timeout(function() {
                            $scope.percentProgress = _.max([
                                _.round(progress * 100, 2),
                                $scope.percentProgress
                            ]);
                        });
                    }

                    function uploadComplete(code, text) {
                        Webcam.off("uploadProgress", uploadProgress);
                        Webcam.off("uploadComplete", uploadComplete);

                        $timeout(function() {
                            $scope.isSend = false;

                            if (200 === code) {
                                $scope.avatars.push(new Avatars(angular.fromJson(text)));

                                $scope.isEmpty = true;
                                crop();

                                $("#avatar_preview").empty();
                                $("#createPhotoBox").modal("hide");
                            }
                        });
                    }
                };
                $scope.removeAvatar = function(id) {
                    var item = _.find($scope.avatars, {id: id});
                    if (_.isUndefined(item)) {
                        return;
                    }

                    item.$remove(null, function() {
                        // console.log($scope.avatars, item);
                        _.pullAllBy($scope.avatars, [item], "id");
                        // console.log($scope.avatars);
                    });
                };
                $scope.makeAvatarDefault = function(id) {
                    var item = _.find($scope.avatars, {id: id});
                    if (_.isUndefined(item)) {
                        return;
                    }

                    _.forEach($scope.avatars, function(v) {
                        _.set(v, "is_main", 0);
                    });

                    item.is_main = 1;
                    item.$update();
                };

                angular.element("#createPhotoBox")
                    .on("hidden.bs.modal filebrowse", function() {
                        Webcam.reset();

                        $timeout(function() {
                            $scope.mkwebcam = false;
                        });
                    })
                    .on("filebrowse", "#selecter_new_file-input", function() {
                        Webcam.reset();

                        $timeout(function() {
                            $scope.mkwebcam = false;
                        });
                    })
                    .on("fileloaded", "#selecter_new_file-input", function(event, file, previewId, index, reader) {
                        // загрузка фото
                        if (!_.startsWith(_.get(file, "type", ""), "image")) {
                            $.notify({
                                message: "Format file must be image"
                            }, {
                                element: "#createPhotoBox .modal-content",
                                type: "danger"
                            });
                            $(event.currentTarget).fileinput("clear");
                            return;
                        }
                        // console.log(file);
                        $scope.fileSize = _.round(file.size / 1048576, 2);
                        $scope.fileName = file.name;

                        reader.onload = function(event) {
                            var e = $("<img/>", {src: event.target.result, class: "img-responsive"});
                            $("#avatar_preview").empty().append(e);
                            crop(e.get(0));

                            $timeout(function() {
                                $scope.isEmpty = false;
                            });
                        };
                        reader.readAsDataURL(file);
                    });

                angular.element("#createVideoBox")
                    .on("filebrowse", "#selecter_new_videofile-input", function() {
                        if (webPlayer) {
                            if (webPlayer.recorder.isRecording()) {
                                webPlayer.recorder.stop();
                            }
                            webPlayer.recorder.stopDevice();
                            webPlayer.recorder.destroy();
                            webPlayer = undefined;

                            $scope.webcamVideoStatus = 0;
                            $scope.isNewVideo = true;
                            $scope.$apply();
                        }
                    })
                    .on("fileloaded", "#selecter_new_videofile-input", function(event, file, previewId, index, reader) {
                        // console.log(file);
                        $scope.fileSize = _.round(file.size / 1048576, 2);
                        $scope.fileName = file.name;
                        // загрузка видео
                        reader.onload = function(event) {
                            var e = $("<video/>", {
                                controls: "",
                                text: "You can not preview this video"
                            });

                            $("<source/>", {
                                src: (window.URL || window.webkitURL).createObjectURL(file),
                                type: file.type
                            }).prependTo(e);
                            $("#video_preview").empty().append(e);
                        };
                        reader.readAsArrayBuffer(file);

                        $scope.isNewVideo = false;
                        $scope.$apply();
                    })
                    .on("hide.bs.modal", function() {
                        if (webPlayer) {
                            if (webPlayer.recorder.isRecording()) {
                                webPlayer.recorder.stop();
                            }
                            webPlayer.recorder.stopDevice();
                            webPlayer.recorder.destroy();
                            webPlayer = undefined;

                            $scope.webcamVideoStatus = 0;
                        }
                        $scope.isNewVideo = true;
                        $scope.$apply();
                    });
                $scope.rotateAvatar = function(angle) {
                    if (_.isUndefined(crop)) {
                        return;
                    }

                    crop(angle);
                };

                $scope.webcamVideoStatus = 0;
                $scope.isNewVideo = true;
                $scope.fileName = "";
                $scope.percentProgressVideo = 0;
                $scope.fileSize = 0;    // at bytes
                $scope.recordNewVideoFromWebcam = function() {
                    if (webPlayer) {
                        if (webPlayer.recorder.isRecording()) {
                            webPlayer.recorder.stop();
                        } else {
                            webPlayer.recorder.start();
                        }
                    } else {
                        $("#video_preview").empty();
                        $("<video/>", {id: "myVideo", class: "video-js vjs-default-skin"}).appendTo("#video_preview");

                        webPlayer = videojs("myVideo", {
                            // video.js options
                            controls: true,
                            loop: false,
                            width: 315,
                            height: 250,
                            plugins: {
                                // videojs-record plugin options
                                record: {
                                    image: false,
                                    audio: true,
                                    video: {
                                        facingMode: "user",
                                        width: 1280,
                                        height: 720,
                                        mandatory: {
                                            minWidth: 1280,
                                            minHeight: 720
                                        },
                                        frameRate: {
                                            ideal: 25,
                                            max: 30
                                        }
                                    },
                                    frameWidth: 1280,
                                    frameHeight: 720,
                                    maxLength: 30,
                                    debug: true
                                }
                            }
                        });

                        webPlayer
                            .on("deviceReady", function() {
                                $timeout(function() {
                                    $scope.webcamVideoStatus = 1;
                                    $scope.isNewVideo = false;
                                });
                            })
                            .on("startRecord", function() {
                                $timeout(function() {
                                    $scope.webcamVideoStatus = 2;
                                });
                            })
                            .on("stopRecord", function() {
                                $timeout(function() {
                                    $scope.fileName = "webcam_record_" + moment() + ".webm";
                                    $scope.webcamVideoStatus = 1;
                                });
                            })
                            .on("finishRecord", function() {
                                $timeout(function() {
                                    // console.log('size', webPlayer.recordedData);
                                    $scope.fileSize = _.round(_.divide(webPlayer.recordedData.size, 1024 * 1024), 2);
                                });
                            });

                        webPlayer.recorder.getDevice();
                    }
                };
                $scope.saveVideo = function() {
                    var form = new FormData();
                    var data;
                    if (webPlayer) {
                        // снимали на камеру
                        if (webPlayer.recorder.isRecording()) {
                            return;
                            // webPlayer.recorder.stop();
                        }
                        // webPlayer.recorder.stopDevice();
                        // webPlayer.recorder.saveAs({video: "test.webm"});return;
                        data = webPlayer.recordedData;
                        if (!_.isUndefined(data)) {
                            form.set("file", data, "webcam_record_" + moment() + ".webm");
                        }
                    } else {
                        // загружали с локальной машины
                        var file = $("#selecter_new_videofile-input").get(0).files[0];
                        form.set("file", file);
                    }

                    if (!form.has("file")) {
                        return;
                    }

                    $scope.isSend = true;
                    $http.post("/profile-video-rest-data", form, {
                        headers: {
                            'Content-Type': undefined
                        },
                        uploadEventHandlers: {
                            progress: function(event) {
                                if (event.lengthComputable) {
                                    $scope.percentProgress = _.max([
                                        _.round(event.loaded
                                            / event.total
                                            * 100, 2), $scope.percentProgress
                                    ]);
                                    // console.log('progress', $scope.percentProgressVideo);
                                }
                            }
                        }
                    })
                        .then(function(response) {
                            // console.log(response);
                            $("#user_media_video")
                                .removeClass("hidden")
                                .empty()
                                .append('<video controls="controls"><source src="'
                                    + response.data.path
                                    + '" type="'
                                    + response.data.type
                                    + '" /></video>');
                            $("#user_media_video")
                                .closest(".row.video")
                                .find(".blockAddVideo")
                                .remove();

                            $("#createVideoBox").modal("hide");
                        }, function(response) {
                            // too large 413 error
                            console.log('error');
                        })
                        .finally(function() {
                            console.log('finally');
                            $scope.isSend = false;
                            $scope.percentProgressVideo = 0;
                        });
                };
            }
        ])
        .controller("ProfileVerificationTabController", [
            "$scope",
            "$http",
            "$mdDialog",
            "uiUploader",
            "Documents",
            function($scope, $http, $mdDialog, $uiUploader, Documents) {
                // socials
                $scope.isGoogle = false;
                $scope.isFacebook = false;
                $scope.isTwitter = false;

                $scope.discard = function($event, name) {
                    $http
                        .delete("/profile/remove-social", {params: {client: name}})
                        .then(function(response) {
                            _.set($scope, "is" + _.upperFirst(_.lowerCase(name)), false);
                        });
                };

                // documents
                $scope.files = Documents.query();
                $scope.tempfiles = {};
                $scope.removeDocument = function($event, id) {
                    var idx = _.findIndex($scope.files, {id: id});
                    if (_.isNil($scope.files[idx].status)) {
                        $mdDialog
                            .show($mdDialog.confirm()
                                .textContent("Delete?")
                                .ok("Yes")
                                .cancel("NO!!!")
                                .targetEvent($event))
                            .then(function() {
                                if (idx >= 0) {
                                    $scope.files[idx].$delete(null, function() {
                                        _.pullAt($scope.files, idx);
                                    });
                                }
                            });
                    }
                };

                $scope.getSizePercent = function(idx, type) {
                    return _.clamp(_.floor(_.get($scope, ["tempfiles", type, idx, "loaded"], 0)
                        / _.get($scope, ["tempfiles", type, idx, "size"], 1)
                        * 100, 2), 0, 100);
                    console.log(_.cloneDeep($scope).tempfiles, idx, type, r);
                    return r;
                };
                $scope.getSize = function(bytes) {
                    return _.ceil(_.divide(bytes, Math.pow(1024, 2)), 2);
                };

                angular.element("#document_verification")
                    .on("change", "input[type='file']", function(event) {
                        var files = event.target.files;
                        var type = $(event.target).data("type");

                        var valid_files = _.differenceBy(files, $scope.files, _.get($scope, [
                            "tempfiles",
                            type
                        ], []), function(value) {
                            return _.toInteger(value.size);
                        });

                        if (files.length !== valid_files.length) {
                            // кажется добавлен уже присутствующий файл
                            var refuse = _.differenceBy(files, valid_files, "name");
                            $.notify({
                                message: "Кажется файл " + _.join(_.map(refuse, "name")) + " уже присутствует"
                            }, {
                                type: "danger"
                            });
                        }
                        if (_.isEmpty(valid_files)) {
                            return;
                        }

                        $uiUploader.addFiles(files);
                        $uiUploader.startUpload({
                            data: {
                                // files type
                                type: type
                            },
                            url: "/profile-document-rest-data",
                            concurrency: 2,

                            onCompleted: function(file, responseText, status) {
                                if (200 === status) {
                                    $scope.files.push(new Documents(angular.fromJson(responseText)));
                                } else {
                                    // error
                                    $.notify({
                                        message: "error"
                                    }, {
                                        type: "danger"
                                    });
                                }

                                _.remove($scope.tempfiles[type], {size: file.size});
                                $uiUploader.removeFile(file);
                                // console.log('completed', file, responseText, status);
                                $scope.$apply();
                            },
                            onProgress: function(file) {
                                // console.log('progress', file, file.loaded);
                                var idx = _.findIndex($scope.tempfiles[type], {size: file.size});
                                _.set($scope, ["tempfiles", type, idx, "loaded"], file.loaded);

                                $scope.$apply();
                            },
                            onError: function(event) {
                                console.log(event);
                            }
                        });
                        // console.log(files);
                        $scope.tempfiles[type] = _.concat(_.get($scope, [
                            "tempfiles",
                            type
                        ], []), $uiUploader.getFiles());

                        $scope.$apply();
                    })
                    .on("fileerror", function(event, data, msg) {
                        $.notify({
                            message: msg
                        }, {
                            type: "danger"
                        });
                    });

                angular.element("#social_render").on("click", ".link_to_contacts a", function(event) {
                    event.preventDefault();

                    $("#update_info_form-tabs").find("a[href='#contacts_tab']").tab("show");
                });

// $uiUploader.addFi
            }
        ])
        .controller("ProfileRecommendationTabController", [
            "$scope", "$mdConstant", function($scope, $mdConstant) {
                $scope.iemails = [];
                $scope.codes = [
                    $mdConstant.KEY_CODE.COMMA,
                    $mdConstant.KEY_CODE.SEMICOLON,
                    $mdConstant.KEY_CODE.ENTER,
                    $mdConstant.KEY_CODE.SPACE
                ];

                $scope.transform = function($chip) {
                    if (!/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/.test($chip)) {
                        return null;
                    }
                    console.log($chip);
                };
            }
        ]);

    app.filter("levelDeepO", function() {
        return function(v, s) {
            if (_.isEmpty(v) || !s) {
                return v;
            }

            s = _.toLower(_.toString(s));

            var regex = new RegExp(s, "i");
            return _.pickBy(v, function(value) {
                return regex.test(value);
            });
        }
    });

    app
        .component("emailFieldEdit", {
            templateUrl: "/profile/template?run=email_field",
            bindings: {
                index: "<",
                model: "<",
                formReference: "<pef",
                onDelete: "&",
                onSave: "&",
                onMakeLogin: "&"
            },
            controller: [
                "$scope", "$http", "$q", function($scope, $http, $q) {
                    var ctrl = this;
                    $scope.data = {};

                    ctrl.$onInit = function() {
                        $scope.data = angular.copy(ctrl.model);
                    };
                    ctrl.$onChanges = function() {
                        $scope.data = angular.copy(ctrl.model);
                    };

                    $scope.getField = function() {
                        return ctrl.formReference["emailField" + ctrl.index];
                    };
                    $scope.removeItem = function() {
                        ctrl.onDelete({idx: ctrl.index});
                    };

                    $scope.isDirty = function() {
                        return !angular.equals(ctrl.model.email, $scope.data.email);
                    };
                    $scope.saveEmail = function() {
                        if ($scope.getField().$valid && !$scope.getField().$pending) {
                            ctrl.onSave({idx: ctrl.index, model: $scope.data});
                        }
                    };
                    $scope.setLogin = function() {
                        ctrl.onMakeLogin({idx: ctrl.index});
                    };

                    // validators
                    $scope.uniqueEmail = function(value) {
                        if (value === ctrl.model.email) {
                            return $q.resolve();
                        }
                        return $http.post("/profile/check-unique-email", {
                            cemail: value
                        });
                    };
                }
            ]
        })
        .component("phoneFieldEdit", {
            templateUrl: "/profile/template?run=phone_field",
            bindings: {
                index: "<",
                model: "<",
                formReference: "<pef",
                allAmount: "&countPublic",
                onDelete: "&",
                onSave: "&",
                onTrust: "&onFtr"
            },
            controller: [
                "$scope",
                "$http",
                "$timeout",
                "$interval",
                "$mdPanel",
                "$mdMedia",
                "$q",
                function($scope, $http, $timeout, $interval, $mdPanel, $mdMedia, $q) {
                    var ctrl = this,
                        remainingTime = 60,
                        tid,
                        isSend = false;

                    $scope.isProcess = 0;

                    ctrl.$onInit = function() {
                        $scope.data = angular.copy(ctrl.model);
                    };

                    ctrl.setTrust = function() {
                        $scope.data.is_verify = 1;
                        ctrl.onTrust({id: ctrl.model.id});
                    };

                    $scope.getField = function() {
                        return ctrl.formReference["phonefield" + _.get(ctrl.model, "id", "")];
                    };
                    $scope.removeItem = function() {
                        ctrl.onDelete({idx: ctrl.index});
                    };
                    $scope.saveInfo = function() {
                        ctrl.onSave({idx: ctrl.index, model: $scope.data});
                    };
                    $scope.save = function($event) {
                        if ($scope.getField().$valid && !$scope.isProcess) {
                            if (angular.equals(ctrl.model.phone, $scope.data.phone)) {
                                if (!isSend) {
                                    $http.post("/profile/send-ver-sms", {
                                        id: ctrl.model.id
                                    }).then(function() {
                                        isSend = true;
                                        openVerifyBox($event, ctrl.model.id);
                                    });
                                } else {
                                    openVerifyBox($event, ctrl.model.id);
                                }
                            } else {
                                $scope.isProcess = 1;

                                ctrl.onSave({idx: ctrl.index, model: $scope.data})
                                    .then(function(data) {
                                        $scope.data = _.clone(ctrl.model);
                                        openVerifyBox($event, data.id);
                                    })
                                    .finally(function() {
                                        $scope.isProcess = 0;
                                    });
                            }
                        }
                    };

                    $scope.toggleLock = function() {
                        if (!!$scope.data.is_public && ctrl.allAmount() < 2) {
                            $.notify({
                                message: "Sorry, at least one phone number must be visible."
                            }, {
                                type: "danger"
                            });
                        } else {
                            $scope.data.is_public = _.toInteger(!$scope.data.is_public);
                            $scope.saveInfo();
                        }
                    };

                    // validators
                    $scope.uniquePhone = function(value) {
                        if (value === ctrl.model.phone) {
                            return $q.resolve();
                        }

                        return $http.post("/profile/check-unique-phone", {
                            cphone: value
                        });
                    };

                    function openVerifyBox($event, id) {
                        startTimer();
                        var panelPosition = $mdPanel.newPanelPosition()
                            .relativeTo($event.target)
                            .addPanelPosition($mdPanel.xPosition.OFFSET_END, $mdPanel.yPosition.ALIGN_BOTTOMS)
                            .withOffsetX('30px')
                            .withOffsetY('25px');

                        $mdPanel.open({
                            controller: [
                                "$mdPanel",
                                "mdPanelRef",
                                "$scope",
                                "$http",
                                "$interval",
                                "id",
                                function($mdPanel, mdPanelRef, $scope, $http, $interval, id) {
                                    var ctrl = this;

                                    ctrl.remaining = function() {
                                        return _.replace($scope.remainingText, "{seconds}", remainingTime);
                                    };

                                    $scope.isActiveResend = function() {
                                        return _.toInteger(!(remainingTime > 0));
                                    };
                                    $scope.remainingText = "";

                                    $scope.resendSms = function() {
                                        if (!$scope.isActiveResend()) {
                                            return;
                                        }

                                        $scope.code = "";
                                        $scope.confirmPhoneForm.confirm_code.$setValidity("wrong_code", true);

                                        $http.post("/profile/send-ver-sms", {
                                            id: id
                                        });
                                        startTimer(60);
                                    };
                                    $scope.apply = function($event) {
                                        if (!$event || $event.type === "keydown" && $event.keyCode === 13) {
                                            if ($event) {
                                                $event.preventDefault();
                                            }
                                            if (_.size($scope.code) !== 6) {
                                                return;
                                            }
                                            $scope.confirmPhoneForm.confirm_code.$setValidity("wrong_code", true);

                                            $http.post("/profile/check-ver-sms", {
                                                id: id,
                                                code: $scope.code
                                            }).then(function(response) {
                                                ctrl.setTrust();

                                                mdPanelRef.close();
                                            }, function() {
                                                $scope.confirmPhoneForm.confirm_code.$setValidity("wrong_code", false);
                                            });
                                        }
                                    };

                                    $scope.close = function() {
                                        mdPanelRef.close();
                                    };
                                    // console.log('form directly', $scope.testForm);
                                }
                            ],
                            controllerAs: "$ctrl",
                            position: panelPosition,
                            targetEvent: $event,
                            templateUrl: "/profile/template?run=confirm_phone",
                            clickOutsideToClose: true,
                            escapeToClose: true,
                            focusOnOpen: true,
                            locals: {
                                id: id,
                                setTrust: ctrl.setTrust
                            },
                            disableParentScroll: true,
                            fullscreen: $mdMedia("max-width: 768px"),
                        });
                    }

                    function startTimer(timeSecond) {
                        if (angular.isDefined(timeSecond)) {
                            remainingTime = _.toInteger(timeSecond);
                        }

                        if (remainingTime > 0 && !angular.isDefined(tid)) {
                            tid = $interval(function() {
                                remainingTime -= 1;

                                if (!remainingTime) {
                                    // время закончилось
                                    $interval.cancel(tid);
                                    tid = undefined;
                                }
                            }, 1000);
                        }
                    }
                }
            ]
        });

    app
        .factory("Emails", [
            "$resource", function($resource) {
                return $resource("/profile-email-rest-data/:id", {
                    id: "@id"
                }, {
                    update: {method: "PATCH"},
                    login: {method: "PUT"}
                });
            }
        ])
        .factory("Phones", [
            "$resource", function($resource) {
                return $resource("/profile-phone-rest-data/:id", {
                    id: "@id"
                }, {
                    update: {method: "PUT"}
                });
            }
        ])
        .factory("Avatars", [
            "$resource", function($resource) {
                return $resource("/profile-avatar-rest-data/:id", {
                    id: "@id"
                }, {
                    update: {method: "PATCH"}
                });
            }
        ])
        .factory("Documents", [
            "$resource",
            function($resource) {
                return $resource("/profile-document-rest-data/:id", {
                    id: "@id"
                });
            }
        ]);


}(window.angular, window.jQuery, window._));