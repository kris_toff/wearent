;(function($, angular) {
    "use strict";

    var app = angular.module("pgaPage");
    app
        .controller("SearchController", [
            "$scope",
            "$http",
            "$sce",
            "$parse",
            "$timeout",
            "uiGmapGoogleMapApi",
            function($scope, $http, $sce, $parse, $timeout, uiGmapGoogleMapApi) {
                var language = $("html").attr("lang");
                var path = "/" + _.toLower(language) + "/search/index";
                var pathMap = "/" + _.toLower(language) + "/search/map";
                var gets = _.fromPairs(_.invokeMap(_.compact(_.split(window.location.search.substr(1), "&")), String.prototype.split, "="));
                var page = 1;


                $scope.maxTimestamp = moment().unix();
                $scope.isOne = false; // true - есть другие товары, которые можно загрузить
                $scope.loadMore = function() {
                    if (!$scope.isOne) {
                        return false;
                    }

                    $http({
                        method: "get",
                        url: path,
                        timeout: 15000,
                        params: _.defaults({page: page + 1, maxtime: $scope.maxTimestamp}, gets)
                    })
                        .then(function(response) {
                            var data = $(_.trim(response.data));
                            data.find(".slider1").slick({
                                infinite: true,
                                prevArrow: '<button class="slick-prev"  href="#" ><img class="aa1"  src="/img/category/prevSlider1.svg" ></button>',
                                nextArrow: '<button class="slick-next" href="#" ><img class="aa1"  src="/img/category/nextSlider1.svg" ></button>'
                            });

                            $("#full-result").append(data);

                            // если достигли последней страницы товаров - выключить кнопку
                            if (response.headers("X-Pagination-Page-Count")
                                - response.headers("X-Pagination-Current-Page")
                                <= 0) {
                                $scope.isOne = false;
                            }

                            page = _.toInteger(response.headers("X-Pagination-Current-Page"));
                        }, function() {
                            $.notify({message: "failed"}, {type: "danger"});
                        })
                        .then(function() {
                            $(window).trigger("resize");
                        });
                };

                $scope.googleMap = {
                    instance: {},
                    events: {
                        "bounds_changed": _.debounce(updateMapArea, 1000),
                        dragstart: function() {
                            $scope.googleMap.window.close();
                        }
                    },
                    options: {
                        fullscreenControl: false,
                        mapTypeControl: false,
                        rotateControl: false,
                        streetViewControl: false,
                        minZoom: 3,
                        // maxZoom: 16
                    },
                    markers: {
                        type: "cluster",
                        typeOptions: {
                            maxZoom: 18,
                            // minimumClusterSize: 3,
                            gridSize: 20
                        },
                        click: updateInfoWindow
                    },
                    window: {
                        coords: {},
                        show: false,
                        templateUrl: "/search/template?run=infowindow",
                        templateParams: {
                            name: ""
                        },
                        content: "oko",
                        close: function() {
                            $scope.googleMap.window.content = "";
                            $scope.googleMap.window.show = false;
                            // $scope.googleMap.window.coords = {};
                        }
                    }
                };

                // обновить карту, когда она открывается
                angular.element(".mapsBtnOpen,.allScreen").one("click", function() {
                    _.invoke($scope, "googleMap.instance.refresh");
                });

                $scope.mapProducts = [];

                function updateMapArea(maps) {
                    var b = maps.getBounds();
                    // console.log(maps, b, b.toJSON(), b.toString(), b.toUrlValue());

                    $http({
                        method: "get",
                        url: pathMap,
                        timeout: 5000,
                        params: _.defaults({except: _.join(_.map($scope.mapProducts, "id"))}, b.toJSON(), gets),
                        headers: {
                            'X-Requested-With': 'XMLHttpRequest'
                        }
                    }).then(function(response) {
                        $scope.mapProducts = _.unionBy($scope.mapProducts, response.data, "id");
                    });
                }

                function updateInfoWindow(marker, eventName, model) {
                    // console.log(marker, eventName, model);
                    $scope.googleMap.window.coords = _.clone(model.coords);
                    $scope.googleMap.window.show = true;

                    $scope.googleMap.window.templateParams = _.assign($scope.googleMap.window.templateParams, model.product);
                }

                $scope.$watchCollection("mapProducts", function(n, o) {
                    // console.log("watch", n, o);
                });

                uiGmapGoogleMapApi.then(function(maps) {
                    // console.log(uiGmapGoogleMapApi, maps);
                });
                $scope.setMyLocation = function() {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function(position) {
                            console.log("good");
                            // var pos = {
                            //     lat: position.coords.latitude,
                            //     lng: position.coords.longitude
                            // };
                            //
                            // infoWindow.setPosition(pos);
                            // infoWindow.setContent('Location found.');
                            // map.setCenter(pos);
                        }, function() {
                            $.notify({
                                message: "Get location failed"
                            }, {
                                type: "danger"
                            });
                        });
                    } else {
                        // браузер не может определить местоположение
                        $.notify({
                            message: "Browser doesn't support geolocation"
                        }, {
                            type: "danger"
                        });
                    }
                };
            }
        ])
        .controller("SearchExtraFilterController", [
            "$scope",
            "$http",
            function($scope, $http) {
                $scope.minimal_rent_price = 0;
                $scope.maximal_rent_price = 0;

                $scope.minimal_pledge_price = 0;
                $scope.maximal_pledge_price = 0;

                $scope.amount_update_products = 0;

                $scope.distanceDelivery = 0;
                $scope.s_rate = 0;
                $scope.s_temp_rate = 0;

                $scope.time_from = undefined;
                $scope.time_to = undefined;

                var self = this;
                this.updatingAmount = false;

                var updateAmountProductsShortDelay = _.throttle(updateAmountProducts, 50, {leading: false});
                var updateAmountProductsLongDelay = _.throttle(updateAmountProducts, 950, {leading: false});


                angular.element("#ps-rent_cost")
                    .on("slide slideStop", _.partial(updateSlider, _, "minimal_rent_price", "maximal_rent_price"));
                angular.element("#ps-deposit_cost")
                    .on("slide slideStop", _.partial(updateSlider, _, "minimal_pledge_price", "maximal_pledge_price"));
                angular.element("#ps-rent_cost,#ps-deposit_cost")
                    .on("slide", updateAmountProductsLongDelay)
                    .on("slideStop", updateAmountProductsShortDelay);

                function updateSlider(o, left, right) {
                    _.set($scope, left, _.toNumber(o.value[0]));
                    _.set($scope, right, _.toNumber(o.value[1]));

                    $scope.$apply();
                }

                function updateAmountProducts() {
                    var params = _.reduce($("#extra-search-fields-form")
                        .serializeArray(), function(accumulator, value, index) {
                        accumulator[value.name] = value.value;
                        return accumulator;
                    }, {});
                    $scope.url = params;

                    self.updatingAmount = true;

                    $http({
                        method: "get",
                        url: "/search/calculate-amount",
                        params: params,
                        cache: true,
                        timeout: 2500
                    })
                        .then(function(response) {
                            $scope.amount_update_products = _.toInteger(response.data.a);
                        }, function(response) {
                            $scope.amount_update_products = 0;
                        })
                        .finally(function() {
                            self.updatingAmount = false;
                        });
                }


                // extra filter
                $scope.distanceDelivery = -1;
                $scope.changeDistance = function(v) {
                    $scope.distanceDelivery = v * 1000;
                    switch (v) {
                        case 25:
                            $scope.enableDelivery = true;
                            $scope.delivery5 = false;
                            $scope.delivery25 = true;
                            $scope.deliveryElse = "";
                            break;
                        case 5:
                            $scope.enableDelivery = true;
                            $scope.delivery25 = false;
                            $scope.delivery5 = true;
                            $scope.deliveryElse = "";
                            break;
                        case 0:
                            $scope.enableDelivery = true;
                            $scope.delivery25 = false;
                            $scope.delivery5 = false;
                            $scope.deliveryElse = "";
                            break;
                        case -1:
                            $scope.enableDelivery = false;
                            $scope.delivery25 = false;
                            $scope.delivery5 = false;
                            $scope.deliveryElse = "";
                            break;
                        default:
                            $scope.enableDelivery = true;
                            $scope.delivery25 = false;
                            $scope.delivery5 = false;
                    }
                };

                this.$postLink = function() {
                    if ($scope.distanceDelivery >= 0) {
                        $scope.enableDelivery = true;

                        if ($scope.distanceDelivery == 5) {
                            $scope.delivery5 = true;
                        } else if ($scope.distanceDelivery == 25) {
                            $scope.delivery25 = true;
                        } else if ($scope.distanceDelivery > 0) {
                            $scope.deliveryElse = $scope.distanceDelivery;
                        }
                    }
                };

                $scope.$watchGroup([
                    "distanceDelivery",
                    "s_rate",
                    "time_from",
                    "time_to"
                ], updateAmountProductsShortDelay);
                $scope.$watch("s_rate", function(v, o) {
                    $scope.s_temp_rate = 0;
                });

                angular.element("#extra-search-fields-form .fil_atr")
                    .on("input", "input[type='text'],input[type='number']", updateAmountProductsLongDelay)
                    .on("change", "input[type='radio'],input[type='checkbox'],select", updateAmountProductsShortDelay);
            }
        ])
        .controller("SearchBarController", [
            "$scope",
            "$element",
            "$http",
            "$window",
            "$sce",
            "$mdPanel",
            "uiGmapGoogleMapApi",
            function($scope, $element, $http, $window, $sce, $mdPanel, uiGmapGoogleMapApi) {
                $scope.categoryId = null;
                $scope.location = "";
                $scope.period_from = null;
                $scope.period_to = null;

                $scope.asyncCompleteWhat = [];
                $scope.asyncCompleteCategory = [];

                $scope.isWhatFocus = false;
                $scope.onlyCategory = 0;

                var selfElement = angular.element($element),
                    historyPanel;

                // колонки с категориями
                if (selfElement.is(".headSearch")) {
                    // поиск на главной странице внизу
                    angular.element(".selectProductTitle")
                        .on("click changed_category", "li", selectCategory);
                } else if (selfElement.is(".rent")) {
                    // панель поиска в шапке
                    angular.element(".selectProduct")
                        .on("click changed_category", "li", selectCategory);
                }

                function selectCategory(event, category) {
                    var c = category || $(event.currentTarget).attr("id");
                    $scope.categoryId = _.toInteger(c);
                    $scope.onlyCategory = 1;

                    // $("#whoSearchLeft").val( $(event.currentTarget).text() );
                    $scope.$apply();
                }

                $scope.enterWord = _.debounce(function($event) {
                    $scope.categoryId = null;
                    $scope.onlyCategory = 0;

                    var v = _.trim($($event.target).val());
                    if (v.length) {
                        $http({
                            method: "get",
                            url: "/search/category-autocomplete",
                            params: {
                                word: v
                            },
                            cache: true,
                            timeout: 1500
                        })
                            .then(function(response) {
                                $scope.asyncCompleteCategory = _.cloneDeep(response.data);
                                var t;
                                $.each($scope.asyncCompleteCategory, function(prop, value) {
                                    t = _.replace(value.name, new RegExp(v, "gi"), "<strong>$&</strong>");
                                    t = $sce.trustAsHtml(t);
                                    value.name = t;
                                });
                            });
                    } else {
                        $scope.asyncCompleteCategory = [];
                        $scope.$apply();
                    }
                }, 400);

                $scope.enterWordM = _.debounce(function($event) {
                    $scope.categoryId = null;
                    $scope.onlyCategory = 0;

                    // нужны слова подсказки
                    var v = _.trim($($event.target).val());
                    if (v.length) {
                        $http({
                            method: "get",
                            url: "/search/category-autocomplete",
                            params: {
                                word: v
                            },
                            cache: true,
                            timeout: 1500
                        })
                            .then(function(response) {
                                $scope.asyncCompleteCategory = _.cloneDeep(response.data);
                                // $scope.asyncCompleteWhat = _.cloneDeep(response.data.w);
                            }, function(response) {
                                $scope.asyncCompleteCategory = [];
                                console.log('error', response);
                            });
                    } else {
                        $scope.asyncCompleteCategory = [];
                        $scope.$apply();
                    }
                    // console.log($event, $($event.target).val());
                }, 400);

                $scope.setAutoWord = function($event) {
                    var li = $($event.target).closest("li", $event.currentTarget);
                    // console.log(li);
                    if (li) {
                        selfElement.find("#whoSearchLeft,#what_input-header").val(_.trim(li.text()));
                        $scope.categoryId = _.toInteger(li.data("id"));
                        $scope.onlyCategory = 1;
                    }
                };

                // console.log(uiGmapGoogleMapApi);
                uiGmapGoogleMapApi.then(function() {
                    selfElement.find(".search-autocomplete").each(function(index, element) {
                        var autocomplete = new google.maps.places.Autocomplete(element, {types: ["address"]});
                        autocomplete.addListener("place_changed", function() {
                            new_place(autocomplete);
                        });
                    });
                });

                this.$postLink = function() {
                    if (_.toInteger($scope.period_from) > 0 || _.toInteger($scope.period_to) > 0) {
                        var dt1 = moment.unix($scope.period_from).format("DD.MM.YYYY");
                        var dt2 = moment.unix($scope.period_to).format("DD.MM.YYYY");

                        selfElement.find("#getDate,#returnDate,#getDateMobile,#returnDateMobile,#from")
                            .each(function(index, element) {
                                var d = $(element).data("dateRangePicker");
                                if (d) {
                                    d.setDateRange(dt1, dt2, true);
                                }
                            });
                    }
                };

                // события календаря
                selfElement
                    .on("datepicker-change", "#getDate,#returnDate,#from", function(event, obj) {
                        $scope.period_from = moment(obj.date1).utcOffset(0, true).unix();
                        $scope.period_to = moment(obj.date2).utcOffset(0, true).unix();

                        $scope.$apply();
                    });
                // console.log('controller', this, $scope, $element);

                // работа с блоком истории запросов
                if (selfElement.is(".headSearch")) {
                    // поиск на главной странице внизу
                    historyPanel = $("#WRAPPANELhead");
                    historyPanel.on("click", ".liText", function(event) {
                        var item = $(event.currentTarget);

                        // видимые свойства
                        selfElement.find("#whoSearchLeft").val(_.trim(item.find(".left").text()));
                        selfElement.find("#whoSearchCenter").val(_.trim(item.find(".center").text()));

                        var dt1 = _.trim(item.find(".right .startDate").text());
                        var dt2 = _.trim(item.find(".right .end").text());

                        if (dt1.length || dt2.length) {
                            selfElement.find("#getDate").data("dateRangePicker").setDateRange(dt1, dt2, true);
                            selfElement.find("#returnDate").data("dateRangePicker").setDateRange(dt1, dt2, true);
                        } else {
                            selfElement.find("#getDate").data("dateRangePicker").clear();
                            selfElement.find("#returnDate").data("dateRangePicker").clear();
                        }
                    });
                } else if (selfElement.is(".rent")) {
                    // панель поиска в шапке
                    historyPanel = $("#WRAPPANEL");
                    historyPanel.on("click", ".liText", function(event) {
                        var item = $(event.currentTarget);

                        // видимые свойства
                        selfElement.find("#what_input-header").val(_.trim(item.find(".left").text()));
                        selfElement.find("#when_input-header").val(_.trim(item.find(".center").text()));

                        var dt1 = _.trim(item.find(".right .startDate").text());
                        var dt2 = _.trim(item.find(".right .end").text());

                        if (dt1.length || dt2.length) {
                            selfElement.find("#from").data("dateRangePicker")
                                .setDateRange(dt1, dt2, true);
                        } else {
                            selfElement.find("#from").data("dateRangePicker").clear();
                        }

                        selfElement.find("input").trigger("change");
                    });
                } else if (selfElement.is(".searchHeader")) {
                    // на телефонах
                    $scope.$watchGroup(["period_from", 'period_to'], function() {
                        if (!$scope.period_from || !$scope.period_to) {
                            $scope.period_mobile_from = undefined;
                            $scope.period_mobile_to = undefined;
                        } else {
                            $scope.period_mobile_from = moment.unix($scope.period_from).toDate();
                            $scope.period_mobile_to = moment.unix($scope.period_to).toDate();
                        }
                        // console.log('watch',$scope.period_mobile_from,$scope.period_mobile_to);
                    });
                }

                if (!_.isUndefined(historyPanel)) {
                    historyPanel.on("click", ".liText", function(event) {
                        var item = $(event.currentTarget),
                            data_tag = item.find(".data");

                        // внутренние переменные
                        $scope.categoryId = data_tag.data("category");
                        $scope.location = data_tag.data("location");
                        $scope.period_from = data_tag.data("from");
                        $scope.period_to = data_tag.data("to");

                        $scope.$apply();
                    });
                }

                $scope.openMobilePicker = function($event) {
                    var position = $mdPanel.newPanelPosition()
                        .absolute()
                        .top().bottom().left().right();

                    $mdPanel.open({
                        targetEvent: $event,
                        templateUrl: "/search/template?run=mobile_search_datepicker_box",
                        position: position,
                        fullscreen: true,
                        hasBackdrop: true,
                        origin: ".btnSearchMobile",
                        panelClass: "mobile_panel",
                        locals: {
                            scope: $scope
                        },
                        controller: [
                            "$scope", "mdPanelRef", "scope", function($scope, mdPanelRef, scope) {
                                $scope.start = scope.period_mobile_from;
                                $scope.stop = scope.period_mobile_to;
// console.log('open',$scope.start,$scope.stop);
                                // $scope.start = moment().add(1, "hours").startOf("hour").toDate();
                                // $scope.stop = moment().add(1, "days").toDate();

                                $scope.$watch("start", function(n) {
                                    if (moment(n).isAfter($scope.stop)) {
                                        $scope.stop = null;
                                    }
                                    $scope.doptions2.minDate = n;
                                });

                                var config = {
                                    maxMode: "day",
                                    showWeeks: false,
                                    startingDay: 1,
                                    initDate: new Date(),
                                    minDate: new Date()
                                };
                                $scope.doptions1 = _.clone(config);
                                $scope.doptions2 = _.clone(config);

                                $scope.closeWindow = function() {
                                    mdPanelRef.close();
                                };

                                $scope.saveInterval = function() {
                                    var x = _.floor(_.divide(_.toInteger(_.invoke($scope, "start.getTime")), 1000)),
                                        y = _.floor(_.divide(_.toInteger(_.invoke($scope, "stop.getTime")), 1000));
                                    if (x > 0 && y > 0) {
                                        scope.period_from = x;
                                        scope.period_to = y;
                                    } else {
                                        scope.period_from = null;
                                        scope.period_to = null;
                                    }
                                    console.log(scope.period_from, scope.period_to);

                                    $scope.closeWindow();
                                };
                            }
                        ]
                    });
                };

                function new_place(autocomplete) {
                    var geometry = autocomplete.getPlace().geometry;

                    if (_.isUndefined(geometry)) {
                        $scope.location = "";
                    } else {
                        $scope.location = geometry.viewport.toUrlValue(4);
                        // console.log(geometry.viewport.toJSON(), geometry.viewport.toUrlValue(4));
                    }

                    $scope.$apply();
                }
            }
        ]);
}(window.jQuery, window.angular));

jQuery(function($) {
    // $("#extra-search-fields-form")
    //     .find("select").selectmenu()
    //     .end()
    //     .find("[type='checkbox'],[type='radio']").checkboxradio();
});


/*
var page = 1;
    var gets = _.split(window.location.search.substr(1), "&");
    _.each(gets, function(value, key){
        if (value.substring(0, 5) === "page=") {
            page = _.toInteger(value.substring(5));
            gets.splice(key, 1);
            return false;
        }
    });
     return;
    var basicForm = $(".header .rent form").serialize();
    var filterForm = _.replace($(".windowFilter form").serialize(), /_csrf=[^&]+&?/, "");
    var dataForm = [basicForm, filterForm].join("&");



$(".btn.allPosts")
    .on("click", function(event) {
        event.preventDefault();

        $.ajax("/search?" + _.concat(gets, ["page=" + (page + 1)]).join("&"), {
            method: "post",
            data: dataForm,
            timeout: 2500
        })
            .done(function(response, textStatus, jqXHR) {
                page += 1;
                // window.history.pushState(null, null, "/search?page="+ page);
                $("#full-result").append(response);

                var lv = _.min([
                    jqXHR.getResponseHeader("X-Pagination-Per-Page"),
                    jqXHR.getResponseHeader("X-Pagination-Total-Count")
                    - jqXHR.getResponseHeader("X-Pagination-Current-Page")
                    * jqXHR.getResponseHeader("X-Pagination-Per-Page")
                ]);

                var btn = $("#nt-ladn");
                btn.text(lv);
                if (lv <= 0) btn.closest("div.block").remove();

                $(".slider1").not(".slick-slider").slick({
                    infinite: true,
                    prevArrow: '<button class="slick-prev"  href="#" ><img class="aa1"  src="/img/category/prevSlider1.svg" ></button>',
                    nextArrow: '<button class="slick-next" href="#" ><img class="aa1"  src="/img/category/nextSlider1.svg" ></button>'
                });

                $(".rateyoHome").not(".jq-ry-container")
       .rateYo({
                ratedFill: "#E35F46",
                starWidth: "17px",
                numStars: 5,
                fullStar: true,
                readOnly: true,
                rating: 3.5,
                "starSvg": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">' +
                '<path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"/>' +
                '</svg>'
            });
            })
    .fail(function(){
        console.log('error');
    });
    */