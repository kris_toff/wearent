/**
 * Created by Hp on 21.06.2017.
 */

(function($) {
    var settings = {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        autoplaySpeed: 1000,
        arrows: true,


        // centerMode: true,
        customPaging: '50px',

        nextArrow: '<button class="slick2-next" href="#" ><img class="aa"  src="/img/category/nextSlider.svg" > </button>',
        prevArrow: '<button class="slick2-prev" href="#" ><img class="aa" src="/img/category/prevSlider.svg" > </button>',
        responsive: [

            // {
            //     breakpoint: 1080,
            //     settings: {
            //         // arrows: false,
            //         // centerMode: true,
            //         centerPadding: '20px',
            //         slidesToShow: 3
            //     }
            // },
            {
                breakpoint: 1040,
                settings: {
                    centerMode: true,
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            // {
            //     breakpoint: 971,
            //     settings: {
            //         // arrows: false,
            //         centerMode: true,
            //         centerPadding: '20px',
            //         slidesToShow: 2
            //     }
            // },
            // {
            //     breakpoint: 787,
            //     settings: {
            //         // arrows: false,
            //         centerMode: true,
            //         // centerPadding: '10px',
            //         slidesToShow: 2,
            //         // customPaging: "20px",
            //         // centerPadding: '20px',
            //
            //     }
            // },
            {
                breakpoint: 768,
                settings: {
                    touchMove: false,
                    centerPadding: '20px',
                    slidesToShow: 1,
                    variableWidth: true,
                    touchThreshold: 3,
                }
            }

        ]
    };

    $(".slider2").each(function(index, element) {
        var y = $(element);
        if (!y.closest(".modal-dialog").length) {
            y.slick(_.merge({}, settings, {appendArrows: $(element).siblings().find(".leftSlider")}));
        }
    });

    $(".slider1").each(function(index, element) {
        var y = $(element);
        if (!y.closest(".modal-dialog").length){
            y.slick({
                infinite: true,
                prevArrow: '<button class="slick-prev"  href="#" ><img class="aa1" src="/img/category/prevSlider1.svg"></button>',
                nextArrow: '<button class="slick-next" href="#" ><img class="aa1" src="/img/category/nextSlider1.svg"></button>'
            });
        }
    });

    $("div.modal").one("shown.bs.modal", function(event){
        $(event.target).find(".slider2").each(function(index, element) {
                $(element).slick(_.merge({}, settings, {appendArrows: $(element).siblings().find(".leftSlider")}));
        });

        $(event.target).find(".slider1").each(function(index, element) {
                $(element).slick({
                    infinite: true,
                    prevArrow: '<button class="slick-prev"  href="#" ><img class="aa1" src="/img/category/prevSlider1.svg"></button>',
                    nextArrow: '<button class="slick-next" href="#" ><img class="aa1" src="/img/category/nextSlider1.svg"></button>'
                });
        });
    });

}(window.jQuery));
