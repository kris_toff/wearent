/**
 * Created by Planess group on 04.08.2017.
 */

(function($, _) {
    "use strict";

    var chatWindow = $("#chatWindowPanel");
    var chatList = chatWindow.find("#list_of_chats");
    var messagesArea = chatWindow.find("#conversation_area");

    // первая загрузка переписок
    initConversations();

    // id - номер диалога из базы
    // pid - номер товара, который обсуждается
    // tid - (trade id) номер сделки, по которой открывается чат
    var templateChatItem = '<div class="list-group-item" data-id="{{id}}" data-pid="{{pid}}" data-tid="{{tid}}">'
        + '<div class="media">'
        + '<a class="media-left"><img src="{{interlocutorAvatar}}" /></a>'
        + '<div class="media-body">'
        + '<h4 class="media-heading">{{title}}</h4>'
        + '<a class="text-left name" style="display: block;">{{name}} <span class="textGrey">{{lastVisit}}</span></a>'
        + '<span class="budge pull-right"></span><p class="text-left textMessage">{{lastMessage}}</p>'
        + '</div>'
        + '</div>'
        + '</div>';

    var templateMessageItem = '<div class="media">'
        + '<div class="media-left media-middle">'
        + '<a href="#"><img class="media-object" src="{{avatar}}" alt="avatar" /></a>'
        + '</div>'
        + '<div class="media-body">'
        + '<h5 class="media-heading">{{name}} <span>&middot; {{created}}</span></h5>'
        + '{{message}}'
        + '</div>'
        + '</div>';

    // var product_id = $("#product-id").val();
    //
    // // указано значение для примера
    var cacheMessages = {
        id: {
            // ...messages,
            0: {
                author: {},
                created_at: 123123,
                message: ""
            },
            isnew: false
        }
    };

    chatWindow
    //     .on("show.bs.modal", function(event) {
    //         var goal = $(event.relatedTarget).data("purpose");
    //
    //         switch (goal) {
    //             case "info":
    //                 // уточнение у хозяина, вызывается кнопкой на странице товара
    //                 // new conversation
    //                 createDialog();
    //                 break;
    //         }
    //     })
        .on("select.chat", function(event) {
            // этот обработчик для отображения сообщений пользователей
            var item = $(event.target).closest(".list-group-item");
            var id = item.data("id");

            item.addClass("active").siblings(".list-group-item").removeClass("active");

            var list = _.get(cacheMessages, id);
            var defer = $.Deferred();
            defer
                .done(function(l) {
                    // add messages into area
                    renderMessages(l);

                    chatWindow.find(".modal-body .blockHeader h3").text(item.find(".media-heading").text());
                })
                .fail(function() {
                    // error in fetching data
                    console.log('error');
                    messagesArea.append("Oh fuck");
                })
            ;

            var hasnew = _.get(cacheMessages, [id, "isnew"], false);
            if (list === undefined || hasnew) {
                $.get({
                    url: "/conversation/list-messages",
                    data: {
                        id: id,
                        onlynew: hasnew ? 1 : 0
                    },
                    timeout: 2500
                }).done(function(response) {
                    if (hasnew) {
                        _.remove(cacheMessages.id, "isnew");
                        _.set(cacheMessages, id, _.concat(cacheMessages.id, response));
                    } else {
                        _.set(cacheMessages, id, response);
                    }

                    defer.resolve(response);
                }).fail(function() {
                    defer.reject();
                });
            } else {
                defer.resolve(list);
            }
        })
        .on("select.chat", function(event) {
            // этот обработчик для добавления ссылки вверх в список сообщений
            var item = $(event.target).closest(".list-group-item");
            $("#chatconversationform-chat_id").val(item.data("id"));
            $("#chatconversationform-product_id").val(item.data("pid"));

            if (false && id === undefined || id === "") {
                // создан новый чат
                var pid = item.data("pid");

                messagesArea.prepend('<a href="/cargo/'
                    + pid
                    + '" style="display: block;" class="text-center chat-product-info"><img src="' + $(".imgProduct")
                        .children("img")
                        .attr("src") + '" style="max-height: 100px;" class="img-responsive center-block" /><p>'
                    + item.find(".media-heading").text()
                    + '</p></a>');

                return;
            }
        })
        .on("submit", "form", function(event) {
            event.preventDefault();

            var $form = $(event.currentTarget);
            var message = _.trim($form.find(".textEdit").text());
            if (message.length < 2) {
                updateList();
                return;
            }

            var item = chatList.children(".list-group-item.active").first();


            var formData = new FormData(event.currentTarget);
            formData.set($("#chatconversationform-message").attr("name"), message);
            // formData.set($("#chatconversationform-chat_id").attr("name"), item.data("id"));
            // formData.set($("#chatconversationform-product_id").attr("name"), item.data("pid"));

            var sbtn = $form.find("[type='submit']");
            sbtn.prop("disabled", true);
            var oldLabel = sbtn.text();
            sbtn.html('<span class="glyphicon glyphicon-knight" aria-hidden="true"></span>');

            $.post({
                url: $form.attr("action"),
                data: formData,
                timeout: 2000,
                contentType: false,
                processData: false,
                cache: false,
                dataType: "json"
            }).done(function(response) {

            }).fail(function() {

            }).always(function() {
                sbtn.text(oldLabel).prop("disabled", false);
            });

            resetForm();
        })
    ;

    chatList.on("click", ".list-group-item", function(event) {
        $(event.currentTarget).trigger("select.chat");
    });

    function renderMessages(list) {
        messagesArea.empty();
        console.log(_.get(list, "messages", list));

        var mlist = $("<div/>", {class: "media-list"});
        $.each(_.get(list, "messages", list), function(index, value) {
            mlist.append(shapeTemplate({
                name: "<strong>" + value.author.fullName + "</strong>",
                message: value.message,
                created: moment.unix(value.created_at).format("DD.MM.YYYY HH:mm:ss")
            }, templateMessageItem));
        });

        mlist.appendTo(messagesArea);
    }

    function createDialog() {
        var ch = chatList.children(".list-group-item[data-pid='" + product_id + "']");

        if (ch.length) {
            ch.first().trigger("select.chat");
        } else {
            $.get({
                url: "/conversation/interlocutor-info",
                data: {
                    pid: product_id
                },
                timeout: 3000
            }).done(function(response) {
                var ch = $(shapeTemplate({
                    pid: product_id,
                    interlocutorAvatar: _.get(response, "interlocutor.avatar_path", ""),
                    title: _.get(response, "default_subject", "Question for " + $("#product_name").text()),
                    name: _.get(response, "interlocutor.fullName")
                }, templateChatItem));

                ch.prependTo(chatList).trigger("select.chat");
            }).fail(function() {
                alert("error ");
            });
        }
    }

    function shapeTemplate(data, template) {
        $.each(data, function(prop, val) {
            template = _.replace(template, "{{" + prop + "}}", val);
        });

        return _.replace(template, /{{[a-z]+}}/gi, "");
    }

    function initConversations() {
        $.get("/conversation/list-conversations")
            .done(function(response) {
                // console.log(response);
                $.each(response, function(index, val) {
                    val["lastVisit"] = moment.unix(val.lastUpdate).local().format("DD.MM.YYYY HH:mm:ss");
                    chatList.append(shapeTemplate(val, templateChatItem));
                });
            })
            .fail(function() {
                console.log("чат не загрузился!!");

                $.notify({
                    message: "error loading"
                }, {
                    type: "danger",
                    delay: 2000
                });
            });
    }

    function updateList() {
        var ids = chatList.children(".list-group-item").map(function(index, element) {
            return $(element).data("id");
        }).get();

        $.ajax({
            url: "/conversation/update-data",
            timeout: 2200,
            data: {
                l: _.join(ids)
            },
            method: "get"
        }).done(function(response) {
            console.log('update', response);
            // handle new conversions
            var l = "";
            $.each(response.list, function(index, value) {
                value["lastVisit"] = moment.unix(value.lastUpdate).local().format("DD.MM.YYYY HH:mm:ss");
                l += shapeTemplate(value, templateChatItem);
                console.log(index);
            });
            console.log(l);
            chatList.prepend(l);

            // show badge of amount new messages in chat list
            chatList.find(".budge").empty();
            _.forOwn(cacheMessages, function(val, key) {
                val.isnew = false;
            });
            $.each(response.nwm, function(prop, value) {
                chatList.children(".list-group-item[data-id='" + prop + "']").find(".budge").text(value);
                if (_.has(cacheMessages, prop)) {
                    _.set(cacheMessages, [prop, "isnew"], true);
                }
            });

            console.log(cacheMessages);
        }).fail(function() {
            console.log("error");
        });
    }

    function resetForm() {
        var $form = chatWindow.find("form");
        var activeDialog = chatList.children(".list-group-item.active");
        $form.get(0).reset();

        $form.find("[name='_csrf']").val(yii.getCsrfToken());
        $("#chatconversationform-chat_id").val(activeDialog.data("id"));
        $("#chatconversationform-product_id").val(activeDialog.data("pid"));
        $form.find(".textEdit").contents().filter(function(index, element) {
            return !$(element).is("div");
        }).remove();
    }

}(window.jQuery, window._));
