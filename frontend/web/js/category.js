/**
 * Created by Hp on 10.05.2017.
 */
(function($, _) {
    "use strict";

    $(".listBlockCategory").on("click", "a", function(e) {
        var submenu = $(e.target).closest("a").nextAll("ul,ol");
        if (submenu.length > 0) {
            e.preventDefault();
            submenu.fadeToggle(300);
        }
    });

    $(".openAllCtegory").on("click", function(e) {
        e.preventDefault();
        $(e.target).parent().nextAll(".listBlockCategory").find("> ul > li > .submenu").fadeToggle(300);
    });

    function makeParams(newValue, delValue) {
        newValue = [_.trim(newValue || "")];
        delValue = [_.trim(delValue || "")];

        var p = _.split(_.trimStart(window.location.search, "?"), "&");
        _.remove(p, function(v) {
            return _.toLower(v.substr(0, 6)) === "fields";
        });

        var wrd = $(".contentTape > .chip").map(function(index, element) {
            return _.trim($(element).text());
        }).get().concat(newValue);
        _.remove(wrd, function(v) {
            return _.isNil(v) || (_.isString(v) && v.length < 1);
        });
        wrd = _.difference(wrd, delValue);
        console.log('new:', newValue, 'old:', delValue, 'result:', wrd);
        p.push("fields=" + _.uniq(wrd).join());

        return p.join("&");
    }

    $(document)
        .on("click", ".chip .close", function(event) {
            // console.log("chip delete", makeParams("", $(event.target).closest(".chip").text()));
            window.location = window.location.pathname + "?" + makeParams("", $(event.target).closest(".chip").text());
        });
    $("#search-form").on("submit", function(event) {
        event.preventDefault();

        var nv = $(event.target).find("input").val();
        nv = _.trim(nv);
        if (nv.length > 0) {
            window.location = window.location.pathname + "?" + makeParams(nv);
        }
    });
    (function() {
        var wrd = $(".contentTape > .chip").map(function(index, element) {
            return _.trim($(element).text());
        }).get();

        $(".searchTape").find("a .name").html(function(index, html) {
            var res;
            var exp, regTag;
            $.each(wrd, function(index, value) {
                exp = new RegExp(value, "ig");
                regTag = new RegExp("<[^>]+>[^<]*<[^>]+>", "ig");

                html = _.replace(html, exp, function(str, offset, s) {
                    while (res = regTag.exec(s)) {
                        if (res.index <= offset && regTag.lastIndex >= (offset + str.length)) {
                            return str;
                        }
                    }
                    return "<span class=\"sel\">" + str + "</span>";
                });
            });

            return html;
        });
    }());

    //випадаюче вікно для товарів
    (function() {
        var inputShowBlock = $(".subCategorySearch");
        var iListCateg = $(".wrappSearch");
        var title = $(".titleCategory");
        var blockForm = $(".allBlockXs");

        function showListCateg(event) {
            iListCateg.css("right", $(".extraSearchFields").offset().right).fadeIn(300);
        }

        function hideListCateg() {
            iListCateg.fadeOut(300);
        }

        function fadeTitle() {
            title.addClass("hidden-xs");
        }

        function showTitle() {
            title.removeClass("hidden-xs");
        }

        inputShowBlock.on("focus", function(ev) {
            showListCateg();
            if ($(window).width() <= '767') {
                fadeTitle();
            }
            ;
            blockForm.removeClass("col-xs-2").addClass("col-xs-12");
        });

        iListCateg.on("click", "li", function(event) {
            iListCateg.fadeOut(300);
            $(".subCategorySearch").val($(event.target).text(), 300).trigger("change");

            blockForm.removeClass("col-xs-12").addClass("col-xs-2");
            showTitle();
        });

        $(document).on("click", function(ev) {
            if ($(ev.target).closest(".subCategorySearch").length) {
                return;
            }
            ;

            hideListCateg();
        });
    }());


    //скрол
    try {
        //    scrollBar window
        $(".scroll-1").perfectScrollbar({
            wheelSpeed: 0.2,
            maxScrollbarLength: 150,
            minScrollbarLength: 80
        });
    } catch (e) {

    }


    //відкриває вікно фільтрів
    (function() {
        var open = $(".btnStatick .popupBtnOpen");
        var close = $("#closeFilterWindow");
        var blockTwoBtn = $(".btnStatick .blockbuttonUp");
        var windowFilter = $(".windowFilter");

        open.on("click", function() {
            if (screen.width > 768) {
                blockTwoBtn.addClass("hideBtnOpen");
            }
            windowFilter.toggleClass("openPopup");
            open.toggleClass("activeMaps");

        });

        close.on("click", function() {
            windowFilter.removeClass("openPopup");
            blockTwoBtn.removeClass("hideBtnOpen");
            open.removeClass("activeMaps");
        })

    }());


    //клік на кнопку відкриття карти
    (function() {
        var maps = $(".btnStatick .mapsBtnOpen");
        var heigWindow = $(window).height();
        maps.on("click", function() {
            if ($(window).width() <= '850') {
                $(".containerSearch ").fadeToggle();
                // $(".containerSearch").removeClass("scroll-1");

            }


            $(".standart").toggleClass("sliceLeft");
            // $(".containerSearch").toggleClass("scroll-1");
            maps.toggleClass("activeMaps");
            $(".mapsProduct").css("height", function() {
                return heigWindow - 133;
            });

            $(".containerSearch").css("height", function() {
                return heigWindow - 90;
            })
        });
    }());

    $(function() {
        $(window).resize(function() {
            var heigWindow = $(window).height();
            if ($(window).width() > '850') {
                $(".mapsProduct").css("height", function() {
                    return heigWindow - 133;
                });

                $(".containerSearch").css("height", function() {
                    return heigWindow - 90;
                });

            }
            else {
                return;

            }

        });
    });

    //відкриває повноектранний режим карти
    (function() {
        var fullScreen = $(".bottomForMaps .allScreen");
        fullScreen.on("click", function() {
            $(".containerSearch").toggleClass("visible");

            $(this).find(".icon").toggleClass("hidden");
        })
    }());


    //star ratimg
    $(function() {
        try {
            $(".rateyoHome").rateYo({
                ratedFill: "#E35F46",
                starWidth: "17px",
                numStars: 5,
                fullStar: true,
                readOnly: true,
                rating: 3.5,
                "starSvg": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">'
                +
                '<path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"/>'
                +
                '</svg>'
            });
        } catch (e) {
            console.error('error', e.message);
        }

    });

    try {
        //    scrollBar window
        $(".scroll-2").perfectScrollbar({
            wheelSpeed: 3,
            maxScrollbarLength: 350,
            minScrollbarLength: 70
        });
        console.log("scroll");
    } catch (e) {
        console.error('error', e.message);
    }


//    додає клас до блоку категорії щоб його виділити
    (function() {
        var block = $(".categoryPage .blockCategory");

        var contentCateg = $(".categoryPage .blockAllCategory");
        var firstSearch = $(".categoryPage .firstSubCategoryMob");
        var lastList = $(".categoryPage .twoSubCategMob");
        //click на блок з назвою категорі і відкривається список першого рівня
        block.on("click", function(event) {

            if ($(window).width() <= '767') {
                $(this).toggleClass("activeBlock");
                contentCateg.addClass("hidden-xs");
                firstSearch.removeClass("hidden-xs");

                $(".headerMobile .categContent")
                    .find(".back a")
                    .fadeIn(300)
                    .end()
                    .find(".subName")
                    .fadeIn(300)
                    .end()
                    .find(".textName")
                    .toggle()
                    .last().text($(event.currentTarget).find(".titleBlockCategory .name").text())
                ;

                // добавить картинку
                $(event.currentTarget)
                    .find(".imgTypCategory img")
                    .clone()
                    .appendTo(firstSearch.find(".imgCateg").empty());

                // копировать список ссылок
                $(event.currentTarget)
                    .find(".listBlockCategory > ul")
                    .clone()
                    .css("display", "")
                    .appendTo(firstSearch.find("ul").remove().end());

                $("html,body").scrollTop(0);
            }
        });

        // кнопка назад в категориях
        $(".headerMobile .categContent .back a")
            .on("click", function() {
                if (!lastList.hasClass("hidden-xs")) {
                    // с 2 на 1
                    firstSearch.removeClass("hidden-xs");
                    lastList.addClass("hidden-xs");
                } else if (!firstSearch.hasClass("hidden-xs")) {
                    // с 1 на главную
                    block.removeClass("activeBlock");
                    firstSearch.addClass("hidden-xs");
                    contentCateg.removeClass("hidden-xs");

                    $(".headerMobile .categContent")
                        .find(".back a")
                        .fadeOut(300)
                        .end()
                        .find(".subName")
                        .fadeOut(300)
                        .end()
                        .find(".textName")
                        .toggle()
                    ;
                }
            });

        //відкриває список третього рівня
        firstSearch.on("click", "li", function(event) {
            var li = $(event.currentTarget);
            if (li.has("ul.submenu").length) {
                event.preventDefault();

                firstSearch.addClass("hidden-xs");
                lastList.removeClass("hidden-xs");

                li.children("ul").clone().css("display", "").appendTo(lastList.children("ul").remove().end());
            }
        });
    }());


}(window.jQuery, window._));