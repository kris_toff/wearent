/**
 * Created by Hp on 17.05.2017.
 */
(function ($) {

    try {
        $('.slideComment').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            prevArrow: '<button class="slick-prev prevFoto"  href="#" ><img class="aa1"  src="/img/category/nextSlider.svg" ></button>',
            nextArrow: '<button class="slick-next nextFoto" href="#" ><img class="aa1"  src="/img/category/prevSlider.svg" ></button>',
        });

        $('.fotoManInf').slick({
            slidesToShow: 1,
            dots: true,
            slidesToScroll: 1,
            nextArrow: '<button class="slick-prev"  href="#" ><img class="aa1"  src="/img/category/nextSlider.svg" ></button>',
            prevArrow: '<button class="slick-next" href="#" ><img class="aa1"  src="/img/category/prevSlider.svg" ></button>'
        });
    } catch (e) {
        // console.error('error', e.message);
    }

    $(function () {
        $('[data-toggle="popover"]').popover()
    });

    try {
        $(".sliderStatus").slick({
            slidesToShow: 3,
            infinite: true,

            prevArrow: '<button class="slick-prev"  href="#" ><img class="aa1"  src="/img/category/prevSlider.svg" ></button>',
            nextArrow: '<button class="slick-next" href="#" ><img class="aa1"  src="/img/category/nextSlider.svg" ></button>'

        });

        $("#createVideo, .feedbackFrends").modal({
            show: false,
            backdrop: false,
        });
    } catch (e) {
        console.error('error', e.message);
    }

    $(function () {
        try {
            $(".rateyo").rateYo({
                ratedFill: "#E35F46",
                starWidth: "17px",
                numStars: 1,

            });
        } catch (e) {
            console.error('error', e.message);
        }

    });

    try {
        //    readmore
        $(".readmore").readmore({
            moreLink: '<a href="#">Читать полностью <img src="/img/editProfile/learnMoreRed.svg" alt="Learn more"></a>',
            lessLink: '<a href="#">Скрыть <img src="/img/editProfile/closeMoreRed.svg" alt="Close"></a>',
            speed: 75,
            maxHeight: 105,
        });

        $(".readmoreFedback").readmore({
            moreLink: '<a href="#">Читать полностью <img src="/img/editProfile/learnMoreRed.svg" alt="Learn more"></a>',
            lessLink: '<a href="#">Скрыть <img src="/img/editProfile/closeMoreRed.svg" alt="Close"></a>',
            speed: 75,
            maxHeight: 40,
        });
    } catch (e) {
        // console.error('error', e.message);
    }
    ;

//    на сторінці редагування відгуку рейтинг
    $(function () {
        try {
            $(".rateyoFeedback").rateYo({
                ratedFill: "#E35F46",
                starWidth: "17px",
                numStars: 5,
                fullStar: true,
                "starSvg": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">' +
                '<path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"/>' +
                '</svg>',

            });
        } catch (e) {
            console.error('error', e.message);
        }

    });


//    focus на поле для відгуку
    (function () {
        var area = $(".blockFedbackHear textarea");
        var border1 = $(".blockFedbackHear .blockFeedback");

        area.on("focus", function () {
            border1.addClass("activeBorder");
        });

        area.blur(function () {
            border1.removeClass("activeBorder");
        })

    }());


    (function () {
        $(".dropdown-menu").on('show.bs.dropdown', function (ev) {
            var b = $(ev.target);
            b.addClass("redd");
        });
    }());


    //додає клас для кнопки відкриття попапу на сторінці каси
    (function () {
        var btn = $(".transaction .infDesk");

        btn.on("click", function (ev) {
            $(this).toggleClass("activeInfDesk");
        })
    }());


    // (function () {
    //    $(document).on("click", ".popover .btnClose", function (event) {
    //
    //         $(event.target).closest(".popover").prev("a[data-toggle='popover']").popover('hide');
    //
    //        $(".transaction .infDesk").removeClass("activeInfDesk");
    //     });
    //
    // }());


    //клік на кнопку попередня сторінка на стор. угод
    (function () {
        var btn = $(".blockDeal .datailDeal");
        var windowDeal = $(".deal .hiddenDealPage");
        var detail = $(".deal .detailsDeal");
        var btnBack = $("#prevPage");


        btn.on("click", function () {
            windowDeal.hide();
            detail.show(200);
            console.log("detail");
        });


        btnBack.on("click", function () {
            detail.hide();
            windowDeal.show(200);
        })

    }());


//    вікно контактів нв ст. редагування видаляє блок пошти
    (function () {
        var blockEmail = $(".contact .emailBlock");
        var a;
        for (var i = 0; i < blockEmail.length; i++) {
            a = $(blockEmail[i]);
            console.log("click", a);
        }

        $("body").on("click", ".removeEmail", function () {
            if (blockEmail.length < 1) {
                console.log("<1", a);
                return false;
            }
            else {
                $(this).closest(".emailBlock").remove();
            }

        });

    }());


//    відкриває меню часових поясів на вкладці насторйки
    (function () {
        var btn = $("#lost .timeShow");
        var window = $("#droppTime");
        var bntPopover = $(".addNewTime");

        btn.on("click", function () {
            $(this).toggleClass("active");
            window.fadeToggle(300);
        });
        bntPopover.on('hide.bs.popover', function () {
            console.log("popClick");
            window.fadeToggle(300);

        });
    }());


    //    стор. відгуків --- відкриває\закриває блок для написання комента
    (function () {
        var comment = $(".oneComment");
        var link = $(".oneComment .addFeedback");
        var cancel = $(".oneComment .addFoto .cancel");
        var canselXs = $(".oneComment .canselXs");

        comment.on("click", ".addFeedback", function (ev) {
            var target = $(ev.target);
            target.closest(".media-heading").siblings(".addFoto").fadeIn(300);
            target.addClass("activeLink");
            target.closest(".media-heading").siblings(".canselXs").removeClass("hidden-xs");

        });

        cancel.on("click", function (ev) {
            var target = $(ev.target);
            target.closest(".addFoto").fadeOut(300);
            link.removeClass("activeLink");
        });

        canselXs.on("click", function (ev) {
            var tatget = $(ev.target);
            tatget.siblings(".addFoto").fadeOut(300);
            $(this).addClass("hidden-xs");
        });
    }());

    //стор. редагуввання відгуків --- відкриває\закиває блоки супорту і особистого повідомлення
    (function () {
        var title = $(".editFeedback .toggleOpen");

        title.on("click", function (ev) {
            var target = $(ev.target);
            var dropMenu = target.find(".closeBlock");
            target.closest(".notPadding").siblings(".contentBlock").fadeToggle(300);

            dropMenu.toggleClass("openBlock");
        })
    }());


    //відкиває\закриває попапи для нагород
    (function () {
        var award = $(".blockAward");
        var close = $(".infAward .close");
        var inf = $(".infAward");

        award.on("click", function (ev) {
            inf.fadeOut();
            ev.stopPropagation();
            var target = $(ev.currentTarget);
            target.siblings(".infAward").fadeIn(300);
        });

        close.on("click", function (ev) {
            ev.stopPropagation();
            var target = $(ev.target);
            target.closest(".infAward").fadeOut(300);
        });

        $("body").on("click", function () {
            inf.fadeOut(300);
        });
    }());


    //    стилі чекбокс
    (function () {
        $(".redCheckBox").iCheck({
            checkboxClass: 'icheckbox_flat-red',
            radioClass: 'iradio_flat-red',
            activeClass: 'active',
            cursor: false,
        });
    }());

    //додає клас лейблам в розділі відгуків(стають червоними)
    (function () {
        var input = $(".blockAllFedback .inAbsolute");
        var label = $(".blockAllFedback .styleLabel");

        input.on("click", function (e) {
            var target = $(e.target);
            if ($(this).prop("checked")) {
                target.closest(".icheckbox_flat-red").siblings(".styleLabel").addClass("onRed");
            }
            else {
                return;
            }

        })

    }());


}(window.jQuery));
