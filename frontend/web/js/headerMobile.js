/**
 * Created by Hp on 23.06.2017.
 */
(function ($) {
    //відкриває вікно пошуку
    (function () {

        //для пошуку
        var labelHeader = $(".headerMobile .labelHeader");
        var search = $(".searchHeader");
        var labelSearch = $(".searchleabel");
        var close = $(".searchleabel .closeSearchMobile");

        //лого вікна і кнопка
        var logo = $(".headerMobile .droppMenuMob");
        var menuLogo = $("#logoDroppMenu");
        var btnLogo = $(".logoBtnGrey");

        //меню кабінету і зображення
        var accountBtn = $(".myMenuBtn");
        var menuAccount = $("#menuMy");


        //контент і футер
        var content = $(".contentBody");
        var footer = $(".foter");


        //відкриває\закриває вікно пошуку
        //при цьому закривається лого і фото кабінету,
        //відкривається інший лейбл
        labelHeader.on("click", function () {
            menuLogo.fadeOut();
            accountBtn.fadeOut();
            menuAccount.fadeOut();
            search.fadeIn(300);
            labelHeader.hide();
            logo.hide();
            labelSearch.fadeIn(300);
            content.fadeOut();
            footer.fadeOut();
            btnLogo.removeClass("logoBtnRed");


        });

        close.on("click", function () {
            menuAccount.fadeOut();
            content.fadeIn();
            labelSearch.hide();
            accountBtn.fadeIn();
            search.fadeOut(300);
            labelHeader.fadeIn(300);
            logo.fadeIn(300);
            footer.fadeIn();
        });


        //відкриває меню по кліку на лого
        //при цьому закривається пошук і меню кабінету
        logo.on("click", function () {
            search.fadeOut();
            btnLogo.toggleClass("logoBtnRed");
            menuLogo.fadeToggle(300);
            content.fadeToggle(300);
            footer.fadeToggle(300);
            accountBtn.fadeToggle();
            labelHeader.fadeToggle(300);
            menuAccount.fadeOut();
        });


        //відкривається меню особистого кабінету
        //при цьому закриваєьється віка пошуку та меню лого
        accountBtn.on("click", function () {
           search.fadeOut();
           menuLogo.fadeOut();
           menuAccount.fadeToggle(300);
           content.fadeToggle(300);
           footer.fadeToggle(300);
        });

    }());


    //забирає клас при екрані менше 425px
    (function () {
        var body = $(".categoryPopup");
        var dellClass = $(".categoryPopup .leftCategory");
        var csel = $(".categoryPopup .centerSelect");
        var rsel = $(".categoryPopup .rightSelect");


            if(screen.width<768){
                dellClass.removeClass("scroll-1");
                csel.removeClass("scroll-1");
                rsel.removeClass("scroll-1");
            }

    }());


    //забирає клас hidden-xs в першого випадаючого списка для розташування товару
    (function () {
      var block = $(".categoryPopup .oneCategory");
      var leftBlock = $(".categoryPopup .rightCategoru");
      var leftSel =$(".categoryPopup .rightSelect");
      var hiddenLeft = $(".categoryPopup .leftCategory");
      var countStep = $(".popup .countStep");
      var backBtn = $(".popup .stepBack");
      block.on("click",function () {
          leftBlock.removeClass("hidden-xs");
          countStep.html("Шаг 2 из 3");
          if($(window).width() <= '767'){
              hiddenLeft.css("display", "none");
              backBtn.removeClass("hidden").addClass("backOneStep");
          }
      });

      leftBlock.on("click", "li", function () {
          leftSel.removeClass("hidden-xs");
          countStep.html("Шаг 3 из 3");
          if($(window).width() <= "767"){
              backBtn.removeClass("backOneStep").addClass("lastStep");
          }
          // $.addClass("hidden-xs");
      });

      //step back
      backBtn.on("click", function () {
         if(backBtn.hasClass("backOneStep")){
              backBtn.addClass("hidden");
              leftBlock.addClass("hidden-xs");
              hiddenLeft.css("display", "block");
              countStep.html("Шаг 1 из 3");

          }
          else if(backBtn.hasClass("lastStep")){
             console.log("lastStep");
             backBtn.addClass("backOneStep");
             leftSel.addClass("hidden-xs");
             countStep.html("Шаг 2 из 3");
         }
      })


    }());


    //вікно авторизації\входу
    (function () {
        var login = $("#loginMob");
        var reg = $("#regMob");

        var loginBlock = $(".regBlock .loginBlock");
        var regBlock = $(".regBlock .registrationBlock");

        var loginBtn = $("#logoDroppMenu .login");
        var regBtn = $("#logoDroppMenu .reg");

        function hideLogin(e) {
            loginBlock.fadeOut(300);
            $("#loginMob").addClass("activeBtn");
        }

        function showLogin(e) {
            loginBlock.fadeIn(300);
            $("#loginMob").removeClass("activeBtn");
        }

        function showReg(e) {
            regBlock.fadeIn(300);
            $("#regMob").removeClass("activeBtn");
        }

        function hideReg(e) {
            regBlock.fadeOut(300);
            $("#regMob").addClass("activeBtn");
        }


        login.on("click", function (ev) {
            hideReg();
            showLogin();
        });

        reg.on("click", function (ev) {
            hideLogin();
            showReg();
        });

        loginBtn.on("click", function (e) {
            showLogin();
            hideReg();
        });
        regBtn.on("click", function (e) {
            hideLogin();
            showReg();
        })
    }());


    //фокус на інпут што
    (function () {
        var input = $(".headSearchMobile #whoSearchLeftMobile");
        var btn = $(".headSearchMobile .allCategory");
        var redLabel = $(".left .aTopLeft");

        input.focus(function () {
            btn.fadeIn(300);
            redLabel.addClass("active");
        });
        input.blur(function () {
            btn.fadeOut(300);
            redLabel.removeClass("active");
        });
    }());


//    початок введення тексту в полі што
    (function () {
        var input = $(".headSearchMobile #whoSearchLeftMobile");
        var btn = $(".headSearchMobile .allCategory");
        var sel = $(".searchHeader .selectMob");

        input.on("input", function () {
            sel.fadeIn(300);
            btn.hide();
        });

        sel.on("click", "li", function (ev) {
            input.val($(ev.target).text());
            sel.fadeOut(300);
        });
        input.on("blur", function () {
            sel.fadeOut(300);
        })


    }());

    //фокус на полі місця закриває вікно катеорій
    (function () {
        var whenInput = $("#whenSearchCenterMobile");
        var redLabel = $(".center .aTop");

        whenInput.focus(function () {
            redLabel.addClass("active");

        });

        whenInput.blur(function () {
            redLabel.removeClass("active");
        });
    }());


    $(".generatePassMob[data-toggle='modal']").on("click", function (e) {
        $('#ModalMob').modal('hide');
    });


}(window.jQuery));