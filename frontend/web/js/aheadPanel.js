//    випадаюче вікно списку
(function($, _) {
    "use strict";

    var form = $("#search-category-form");
    var input = form.find("input.subCategorySearch");

    var iListCateg = $(".listCategorySearch");


    function showListCateg() {
        iListCateg.fadeIn(300);
    }

    function hideListCateg() {
        iListCateg.fadeOut(300);
    }

    input.on("focus", showListCateg);

    iListCateg.on("click", "li", function(event) {
        input.val($(event.target).text()).trigger("change");
        iListCateg.fadeOut(300);
    });

    $(document).on("click", function(ev) {
        if ($(ev.target).closest(input, ".row").length) {
            return;
        }

        hideListCateg();
    });

    function makeLinks(filterWord){
        var r = [];
        var a;
        filterWord = _.toLower(filterWord);
        $.each(categoryProductGroup, function(k, v){
            if (_.toLower(v).indexOf(filterWord) >= 0 ) {
                a = _.replace(v, new RegExp(filterWord, "ig"), function(m){
                    return "<span class='sel'>" + m + "</span>";
                });
                r.push("<li><a href='/category/repel?fields=" + v + "'>" + a + "</a></li>");
            }
        });
        return r;
    }
}(window.jQuery, window._));
