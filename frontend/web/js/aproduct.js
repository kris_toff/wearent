(function($, angular) {
    "use strict";

    var lang = $("html").attr("lang").substr(0, 2);

    var app = angular.module("pgaPage");

    app
        .controller("ProductController", [
            "$scope",
            "$timeout",
            "$http",
            "$mdPanel",
            "$mdMedia",
            "uiGmapGoogleMapApi",
            "uiCalendarConfig",
            function($scope, $timeout, $http, $mdPanel, $mdMedia, uiGmapGoogleMapApi, uiCalendarConfig) {
                var self = this;
                self.openMobileIntervalPanel = false;
                self.mobileWidth = false;

                $scope.isOpenModal = false;

                $scope.$watch(function() {
                    return $mdMedia("(max-width: 768px)");
                }, function(newValue) {
                    self.mobileWidth = newValue;

                    if (!newValue) {
                        $("#intervalModal").modal("hide");
                    }
                });

                // переменные настройки товара
                $scope.currencySymbol = ""; // символ валюты, который везде используется для форматирования фильтром currency
                $scope.isFastRent = false; // true - предлагается быстрая аренда для верифицированных пользователей с ## рейтингом

                // переменные для доставки товара
                var address_id, address_coords, confirmAddress = false;
                $scope.isDelivery = false; // true - владелец предлагает доставку товара, false - нет доставки товара
                $scope.useDelivery = false; // true - пользователь выбрал доставку товара к месту назначения
                $scope.deliveryPrice = 0; // стоимость доставки товара владельцем к месту указания
                $scope.deliveryMaxRadius = 0; // максимальный радиус доставки, на который согласен владелец, в метрах
                $scope.deliveryStatus = undefined; // используется для отображения подсказок; ok - радиус доставки выбран правильно, wrong - радиус доставки превышает возможности владельца, доставка не будет назначена

                // период бронирования товара
                $scope.rentFrom = null; // дата и время (экземпляр Date()) начала бронирования, установлены клиентом
                $scope.rentTo = null; // дата и время (экземпляр Date()) окончания бронирования, установлены клиентом
                $scope.blockedPoints = []; // список заблокированных дат, точнее уже существующие заказы на будущее
                $scope.amountHours = 0; // количество часов из календаря, период времени аренды в часах


                // временные параметры получаемые из БД
                var intervals = {1: "days", 2: "weeks", 3: "months", 4: "hours"};
                var conversions = {1: 24, 2: 168, 3: 720};
                var interval;
                $scope._isGoodUser = 0;
                $scope._timeFrom = null; // в формате HH:mm:ss
                $scope._timeTo = null; // в формате HH:mm:ss
                $scope._minTimeInterval = 1; // минимальное количество периодов для заказа товара
                $scope._maxTimeInterval = null; // максимальное количество периодов для заказа товара
                $scope._maxperiod = 360; // количество дней, на которые можно заранее забронировать товар
                $scope._lp = 0; // аналогично в БД lease_time [1 => day,2 => week,3 => month,4 => hour]
                $scope._lmp = 0; // аналогично в БД lease_max_time [1,2,3,4]
                $scope._discount_price = 0; // скидка на цену товара (в %)
                $scope._coordinatesDistributionPoint = {}; // координаты центральной точки отгрузки товара, не учитывая доставку

                $scope.period = 0; // количество единиц(!) периода времени, за который указана цена. Если цена указана за сутки, то здесь будет количество часов до оплаты, если цена за день/неделю - то здесь количество дней/недель до оплаты.

                $scope._price = 0; // основная цена товара
                $scope._priceCondition = {}; // цены в зависимости от продолжительности аренды. Ключ это минимальное количество часов аренды, значение - новая цена товара

                $scope.price = $scope._price;
                $scope.fullPrice = $scope.price * $scope.period;
                $scope.totalPrice = 0;  // Итоговая цена
                $scope.discount = 0; // актуальная скидка (точное значение в валюте) после анализа всех условий аренды
                $scope.goodUserDiscount = 0 // скидка (точное значение в валюте) верифицированного опльзователя

                $scope.guaranty = 0;

                // процент от суммы
                var serviceCommision = 10; // 10%
                $scope.serviceCommission = 0; // комиссия сайта за сделку (точное значение в валюте)

                $scope.isNewAddress = false; // если добавлен новый адрес доставки, true - отображать кнопки на карте для подтверждения адреса, false - кнопки спрятать
                $scope.visibleGuarantyBox = false; // переключатель, true - инфопанель под строкой гарантии отображается, false - инфопанель не отображается и видно только окончательную стоимость залога
                $scope.visibleDiscountBox = false; // аналогично предыдущему, true - инфопанель под скидкой отображается

                var locationInputs = {};
                var isGoto = false;
                $scope.config = {
                    datepicker: {
                        initDate: moment().add(1, "days").set({hour: 12, minute: 0, second: 0}).toDate(),
                        maxDate: null, // be overridden
                        maxMode: "day",
                        minDate: moment().add(1, "hours").startOf("hour").toDate(),
                        showWeeks: false,
                        shortcutPropagation: true
                    },
                    timepicker: {
                        minTime: undefined, // be overridden
                        maxTime: undefined, // be overridden
                        minuteStep: 15
                    },
                    calendar: {
                        firstDay: 1,
                        weekends: true,
                        eventLimit: 2,
                        selectable: false,
                        selectHelper: false,
                        editable: false,
                        eventOverlap: false,
                        allDaySlot: false,
                        defaultView: "agendaWeek",
                        slotDuration: "00:15:00",
                        slotLabelInterval: "01:00:00",
                        minTime: undefined, // be overridden
                        maxTime: undefined, // be overridden
                        header: {
                            left: "",
                            center: "",
                            right: "prev,next"
                        },
                        events: [
                            // {title: "one1", start: "2017-10-02", editable: true},
                            // {title: "four4", start: "2017-10-02", end: "2017-10-11", editable: false},
                        ],
                        select: function(start, end) {
                            // $("#calendar")
                            //     .fullCalendar("renderEvent", {title: "new", start: start, end: end, editable: true}, true);
                        },
                        eventAfterAllRender: function() {
                            if (isGoto
                                && !_.isNil(myevent)
                                && !_.isNil(uiCalendarConfig.calendars["intervalHoursCalendar"])) {
                                // console.log(uiCalendarConfig.calendars.intervalHoursCalendar);
                                uiCalendarConfig.calendars["intervalHoursCalendar"].fullCalendar("gotoDate", myevent.start);
                                isGoto = false;
                            }
                        }
                    },
                    maps: {
                        streetViewControl: false,
                        mapTypeControl: false
                    },
                    searchbox: {
                        template: 'searchbox.tpl.html',
                        templateOuter: "searchbox.outer.tpl.html",
                        events: {
                            place_changed: function(box) {
                                // $scope.newAddress = true;
                                confirmAddress = false;
                                if (!box.getPlace().geometry) {
                                    $scope.cancelLocation();
                                    return;
                                }

                                var distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng($scope._coordinatesDistributionPoint['latitude'], $scope._coordinatesDistributionPoint['longitude']), box.getPlace().geometry.location); // in meters

                                $scope.config.marker.options.visible = true;
                                $scope.config.marker.coords = {
                                    latitude: box.getPlace().geometry.location.lat(),
                                    longitude: box.getPlace().geometry.location.lng()
                                };
                                if (distance <= $scope.deliveryMaxRadius) {
                                    $scope.isNewAddress = true;
                                    $scope.deliveryStatus = "ok";
                                } else {
                                    $scope.isNewAddress = false;
                                    $scope.deliveryStatus = "wrong";
                                }
                                address_id = box.getPlace().id;
                                address_coords = box.getPlace().geometry.location;

                                // console.log(box, box.getPlace(), $scope.markerControl.getGMarkers());

                                var inp = $(".gmap-autocomplete-field").filter(function(index, element) {
                                    // console.log($(element).val(),locationInputs[ $(element).attr("id") ]);
                                    return $(element).val() != _.get(locationInputs, $(element).attr("id"), "");
                                });
                                // console.log(inp);

                                angular.element(".gmap-autocomplete-field")
                                    .val(_.get(box.getPlace(), "formatted_address", ""));

                                $(".gmap-autocomplete-field").each(function(index, element) {
                                    locationInputs[$(element).attr("id")] = $(element).val();
                                });

                                // сразу активируем доставку, если адрес написан в поле справа
                                if (inp.length > 0 && inp.is("#outerInputSearchbox")) {
                                    $scope.confirmLocation();
                                }
                            }
                        },
                        options: {
                            autocomplete: true,
                            types: ["address"]
                        }
                    },
                    marker: {
                        coords: {},
                        options: {
                            visible: false,
                            clickable: false
                        }
                    }
                };

                // слушатели
                var myevent = null;
                $scope.$watch("rentFrom", function(newValue) {
                    // диапазоны допустимого времени
                    var date = moment(newValue),
                        md, d;
                    if (date.isValid() && ($.inArray($scope._lp, [1, 4]) + 1)) {
                        if (!_.isNil($scope._timeFrom)) {
                            d = moment($scope._timeFrom, "HH:mm:ss");

                            md = date.clone().hours(d.hours()).minutes(d.minutes()).seconds(0).milliseconds(0);
                            $scope.config.timepicker.minTime = md.toDate();
                        }
                        if (!_.isNil($scope._timeTo)) {
                            d = moment($scope._timeTo, "HH:mm:ss");

                            md = date.clone().hours(d.hours()).minutes(d.minutes() + 1).seconds(0).milliseconds(0);
                            $scope.config.timepicker.maxTime = md.toDate();
                        }
                    } else {
                        $scope.config.timepicker.minTime = undefined;
                        $scope.config.timepicker.maxTime = undefined;
                    }
                });
                $scope.$watchGroup(["period", "rentFrom"], function() {
                    // исходя из периода аренды устанавливаем конечную дату сделки
                    var d = moment($scope.rentFrom);
                    // console.log(d, $scope.rentFrom, $scope.period, d.clone().add($scope.period, interval));
                    if (d.isValid() && _.toInteger($scope.period) > 0) {
                        $scope.rentTo = d.clone().add($scope.period, interval).toDate();
                        // console.log($scope.rentFrom, $scope.period);
                        if (myevent) {
                            myevent.start = d;
                            myevent.end = d.clone().add($scope.period, interval);
                            // console.log('update',$scope.config.calendar.events);
                        } else {
                            myevent = {title: "I am", start: d, end: d.clone().add($scope.period, interval)};
                            $scope.config.calendar.events.push(myevent);
                            // console.log('add');
                        }
                        isGoto = true;
                    } else {
                        $scope.rentTo = null;
                        $scope.config.calendar.events = [];
                        myevent = null;
                    }
                });
                $scope.$watchGroup(["rentFrom", "period", "useDelivery", "deliveryStatus"], function() {
                    // на основе данных считаем цену за продукт
                    var priceList = _.keys($scope._priceCondition);
                    $scope.amountHours = $scope.period * _.get(conversions, $scope._lp, 1);

                    $scope.price = $scope.period ? _.get($scope._priceCondition, priceList[_.sortedLastIndex(priceList, $scope.amountHours)
                    - 1], $scope._price) : 0;

                    // считаем скидку от цены для верифицированных пользователей

                    $scope.goodUserDiscount = ($scope.price > 0 ? $scope.price : $scope._price)
                        / 100
                        * $scope._discount_price;
                    // если пользователь верифицирован - применяем скидку
                    if ($scope._isGoodUser) {
                        $scope.price -= $scope.goodUserDiscount;
                        $scope.price = _.max([$scope.price, 0]);
                    }
                    $scope.fullPrice = $scope.period * $scope.price;
                    $scope.discount = _.max([$scope._price * $scope.period - $scope.fullPrice, 0]);

// console.log($scope._priceCondition, priceList, $scope.period, $scope.price, amountHours);

                    $scope.serviceCommission = $scope.fullPrice / 100 * serviceCommision;
                    $scope.totalPrice = $scope.fullPrice + $scope.serviceCommission;

                    if ($scope.isDelivery && $scope.useDelivery && confirmAddress) {
                        // console.log('total', $scope.deliveryPrice);
                        $scope.totalPrice += $scope.deliveryPrice;
                    }
                });

                // некоторые свойства будут получены конструкцией ng-init и применить их можно только после создания объекта
                this.$postLink = function() {
                    interval = intervals[$scope._lp];
                    $scope._priceCondition[0] = $scope._price;

                    $scope.config.datepicker = _.assign($scope.config.datepicker, {
                        maxDate: moment().add($scope._maxperiod, "days").toDate()
                    });

                    var minTime = moment($scope._timeFrom, "HH:mm:ss");
                    var maxTime = moment($scope._timeTo, "HH:mm:ss").add(1, "minutes");

                    var mint = minTime.isValid() ? minTime.toDate() : undefined,
                        maxt = maxTime.isValid() ? maxTime.toDate() : undefined;
                    $scope.config.timepicker = _.assign($scope.config.timepicker, {
                        minTime: mint,
                        maxTime: maxt
                    });
                    if (minTime.isValid()) {
                        $scope.config.calendar.minTime = minTime.format("HH:mm:ss");
                    }
                    if (maxTime.isValid()) {
                        $scope.config.calendar.maxTime = maxTime.format("HH:mm:ss");
                    }

                    // для слайдера максимальное первоначальное значение
                    if (!$scope._maxTimeInterval) {
                        $scope._maxTimeInterval = $scope._maxperiod;
                    }

                    // для карты положение маркера
                    $scope.config.marker.coords = $scope._coordinatesDistributionPoint;
                };

                // функции отображения и скрытия панели с датаами
                var panelRef, isSave = false;
                $scope.openModal = function($event) {
                    // сделать копию переменных для отмены, если нужно будет
                    var cp = _.cloneDeep(_.pick($scope, ["rentFrom", "rentTo", "period"]));

                    var position = $mdPanel.newPanelPosition().relativeTo($event.target)
                        .addPanelPosition($mdPanel.xPosition.ALIGN_END, $mdPanel.yPosition.BELOW)
                        .withOffsetX("0").withOffsetY("16px");

                    $mdPanel.open({
                        contentElement: ($.inArray($scope._lp, [1, 4]) + 1) ? "#timePanel" : "#datePanel",
                        targetEvent: $event,
                        hasBackdrop: true,
                        clickOutsideToClose: false,
                        escapeToClose: true,
                        trapFocus: true,
                        panelClass: "float_panel",
                        fullscreen: $mdMedia("(max-width: 768px)"),
                        // disableParentScroll: true,
                        zIndex: 1052,
                        position: position,
                        onOpenComplete: function() {
                            $(window).trigger("resize");
                        },
                        onRemoving: function() {
                            if (!isSave) {
                                isSave = false;
                                // отменить сохранение
                                _.forOwn(cp, function(v, k) {
                                    $scope[k] = v;
                                });
                            }
                        }
                    }).then(function(ref) {
                        panelRef = ref;
                    });
                };
                $scope.closeModal = function(save) {
                    isSave = !!save;

                    if (panelRef) {
                        panelRef.close();
                    }
                };

                // работа с google-картами
                var maps;
                $scope.markerControl = {};
                uiGmapGoogleMapApi.then(function(smaps) {
                    maps = smaps;
                    console.log(maps, 'marker', $scope.markerControl);
                });

                $scope.cancelLocation = function() {
                    $scope.config.marker.options.visible = false;
                    $scope.config.marker.coords = $scope._coordinatesDistributionPoint;
                    $scope.isNewAddress = false;
                    $scope.useDelivery = false;
                    $scope.deliveryStatus = undefined;

                    address_id = undefined;
                    address_coords = {};
                    locationInputs = {};

                    angular.element(".gmap-autocomplete-field").val("");
                };
                $scope.confirmLocation = function() {
                    if (google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng($scope._coordinatesDistributionPoint['latitude'], $scope._coordinatesDistributionPoint['longitude']), new google.maps.LatLng($scope.config.marker.coords.latitude, $scope.config.marker.coords.longitude))
                        <= $scope.deliveryMaxRadius) {
                        $scope.useDelivery = true;
                        $scope.isNewAddress = false;

                        confirmAddress = true;
                    }
                };

                // модальное окно на мобильных
                $scope.openMobileModals = function() {
                    if ($mdMedia("(min-width: 768px)")) {
                        return false;
                    }

                    if (!self.openMobileIntervalPanel) {
                        $("#intervalModal").modal("show");
                    } else {
                        $("#intervalModal").modal("hide");
                        $("#rentModal").modal("show");
                    }
                };

                $scope._productId = 0;
                $scope.changeFavourite = function() {
                    $http({
                        method: "put",
                        url: "/product/change-favourite",
                        data: {product_id: $scope._productId},
                        timeout: 15000
                    }).then(function(response) {
                        $scope.isFavourite = _.toInteger(response.data.result);
                    }, function(response) {
                        if (response.status == 403) {
                            // скорее всего пользователь не авторизован
                            $.notify({message: "Нужна авторизация"}, {type: "danger"});
                        }
                        console.log(response);
                    });
                };
                $scope.changeAmazing = function($event) {
                    $http({
                        method: "put",
                        url: "/product/change-amazing",
                        data: {product_id: $scope._productId},
                        timeout: 15000
                    }).then(function(response) {
                        $scope.isAmazing = _.toInteger(response.data.result);
                    }, function(response) {
                        if (response.status == 403) {
                            // скорее всего пользователь не авторизован
                            $.notify({message: "Нужна авторизация"}, {type: "danger"});
                        }
                        console.log(response);
                    });
                };


                $("#intervalModal")
                    .on("show.bs.modal", function() {
                        $timeout(function() {
                            $scope.isOpenModal = true;
                            self.openMobileIntervalPanel = true;
                        });
                    })
                    .on("hide.bs.modal", function() {
                        $timeout(function() {
                            $scope.isOpenModal = false;
                            self.openMobileIntervalPanel = false;
                        });
                    });
                $("#rentModal")
                    .on("show.bs.modal", function() {
                        $timeout(function() {
                            $scope.isOpenModal = true;
                        });
                    })
                    .on("hide.bs.modal", function() {
                        $timeout(function() {
                            $scope.isOpenModal = false;
                        });
                    });
            }
        ])
        .controller("RentModalController", [
            "$scope", "$http", function($scope, $http) {
                $scope.setPayment = function($event) {
                    $scope.payment_method = $($event.target).closest("li", $event.currentTarget).attr("name");
                };

                $scope.submitOrder = function($event) {
                    $event.preventDefault();

                    if (!_.isUndefined($event.originalEvent)){
                        var lang = _.toLower( $("html").attr("lang") );


                        $.ajax("/" + lang +"/order/checkout", {
                            method: "post",
                            data: $($event.target).serialize(),
                            timeout: 3000
                        }).success(function(){

                        }).fail(function(){

                        });

                    }
                };

                $scope.getVal = function(){
                    return 'er';
                };
            }
        ]);
}(window.jQuery, window.angular));

// до карусели добавить
$(".slickItem")
    .on("afterChange", function(slick, currentSlide) {
        // console.log("change", slick, currentSlide);
        var csp = currentSlide.currentSlide;

        $("#myModal")
            .find(".countSlides .actual")
            .text(csp + 1)
            .end()
            .find(".textForSlider .white")
            .text($(slick.target).find(".slick-track").children("img,video").eq(csp).data("covering-text") || "");
    })
    .on("beforeChange", function(slick, currentSlide, nextSlide) {
        // console.log(slick, currentSlide);
        $(slick.target)
            .find(".slick-track")
            .children("video")
            .each(function(index, element) {
                element.pause();
            });
    });

$("#myModal")
    .on("show.bs.modal", function() {
        $(document).on("keydown", slickByArrows);
    })
    .on("shown.bs.modal", function() {
        $(window).trigger("resize");
    })
    .on("hide.bs.modal", function() {
        $(document).off("keydown", slickByArrows);
    });

function slickByArrows(event) {
    console.log(event);
    if (event.keyCode === 39) {
        // вправо
        $(".slickItem").slick("slickNext");
    } else if (event.keyCode === 37) {
        // влево
        $(".slickItem").slick("slickPrev");
    }
}

$("#myModal").find(".modal-body .blockBtnBody")
    .on("click", "button", function(event) {
        var btn = $(event.target).closest("button");

        if (_.isNil(btn)) {
            return;
        }

        $(".sliderNav").slick("slickUnfilter");
        $(".slickItem").slick("slickUnfilter");
        if (btn.hasClass("foto")) {
            $(".sliderNav").slick("slickFilter", ":has([data-type='image'])");
            $(".slickItem").slick("slickFilter", "[data-type='image']");
        } else if (btn.hasClass("video")) {
            $(".sliderNav").slick("slickFilter", ":has([data-type='video'])");
            $(".slickItem").slick("slickFilter", "[data-type='video']");
        }
    });
