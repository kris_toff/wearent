// import * as _ from "./lodash/lodash";
(function($) {
    'use strict';
    //
    // //фокус на перший інпут і відкриває вікно списків продукції
    //
    // var iRent = $(".header .fooorm");
    // var extraField = iRent.find(".extraSearchFields");
    // var helpPanel = $(".rightRent");
    // var wrapHistory = $(".header  #WRAPPANEL");
    // var labelWhatBtn = $(".header .labelWhatBtn");
    // var inputOpen = $(".header .rentBlockImg");
    //
    //
    // var isShowFilterList = false;
    //
    //
    // function showHistory() {
    //     if (isActiveSearchFields() || isShowFilterList) return;
    //     wrapHistory.show();
    //     $(document).on("click", hideHistory);
    // }
    //
    // function hideHistory() {
    //     wrapHistory.hide();
    //     $(document).off("click", hideHistory);
    // }
    //
    // //доступ до кнопки категорій
    // function showLabelWhat() {
    //     labelWhatBtn.show();
    // }
    //
    // function hideLabelWhat() {
    //     labelWhatBtn.hide();
    // }
    //
    // //доступ до кнопки категорій
    // function showRent() {
    //     showSearchPanel();
    //     showHistory();
    //     showLabelWhat();
    //     $(".input-floating").width(282);
    //     $(".inputgreat10").width(292);
    //     console.log('showRent');
    // }
    //
    // function hideRent() {
    //     if (isActiveSearchFields() || isShowFilterList) return;
    //     hideSearchPanel();
    //     hideHistory();
    //     hideLabelWhat();
    //     $(".input-floating").width(326);
    //     console.log('hideRent');
    // }
    //
    //
    // function showSearchPanel() {
    //     extraField.show();
    //     helpPanel.hide();
    //     console.log('searchShow');
    // }
    //
    //
    // function hideSearchPanel() {
    //     extraField.hide();
    //     helpPanel.show();
    // }
    //
    // var isActiveSearchFields = function () {
    //    if ("input:focus") return true;
    //
    //
    //     var isActive = false;
    //     iRent.find("input:not([type='submit'])").each(function () {
    //             if ($(this).val().length) {
    //                 isActive = true;
    //                 return true;
    //             }
    //         }
    //     );
    //     return isActive;
    // };
    //
    // inputOpen.focus(showRent);
    //
    //
    //  inputOpen.on("mouseleave", hideRent);
    var isShowFilterList = false;

    (function() {
        var inputHeader = $(".fooorm input");
        var thereInput = $(".header .extraSearchFields");
        var history = $(".header #WRAPPANEL");
        var rightRent = $(".header .rightRent");
        var label = $(".header .labelWhatBtn");
        var select = $(".header .selectProduct");
        var when = $(".header .whenBlock");
        var bigLogo = $(".leftBlockHeader .logoBig");
        var smallLogo = $(".leftBlockHeader logoSmall");


        function showInput() {
            thereInput.show().addClass("open");
            rightRent.hide();
            label.show();
            select.hide();
            when.hide();
            history.css("left", $(".rent").offset().left).show();
            $(".input-floating").width(282);
            $(".inputgreat10").width(292);
        }

        function hideInput() {
            thereInput.hide().removeClass("open");
            history.hide();
            rightRent.show();
            label.hide();
            select.hide();
            when.hide();
            $(".input-floating").width(326);
        }

        inputHeader.focus(function() {
            showInput();
            if ($(window).width() <= '1190') {
                $(".manHeader").addClass("hidden");
            }
            if ($(window).width() <= "1100") {
                $(".logo").addClass("hidden");
                $(".leftBlockHeader").addClass("centerRent");
            }
        });


        $("body .row").on("click", function() {
            hideInput();
            $(".manHeader").removeClass("hidden");
            $(".logo").removeClass("hidden");
            $(".leftBlockHeader").removeClass("centerRent");
        });
    }());


    // перевірка на заповненість полів
    (function() {
        var form = $(".fooorm");
        var right = $(".header .extraSearchFields");
        var input = form.find(".focusInput");
        var rightRent = $(".header .rightRent");


        function validInput() {
            var h = false;
            input.each(function(index, element) {
                var el = $(element);
                if (el.val().length) {
                    h = true;
                    return false;
                }
            });

            return h;
        };

        input.blur(function() {
            if (validInput()) {
                rightRent.addClass("visible");
                right.show();
            } else {
                rightRent.removeClass("visible");
                right.removeClass("visibleExtra");
            }
        })
    }());


    //вікно авторизації\входу
    (function() {
        var login = $("#login");
        var reg = $("#reg");

        var loginBlock = $(".loginBlock");
        var regBlock = $(".registrationBlock");

        var loginBtn = $(".regBlock .login");
        var regBtn = $(".regBlock .reg");

        function hideLogin(e) {
            loginBlock.fadeOut(300);
            $("#login").addClass("activeBtn");
        }

        function showLogin(e) {
            loginBlock.fadeIn(300);
            $("#login").removeClass("activeBtn");
        }

        function showReg(e) {
            regBlock.fadeIn(300);
            $("#reg").removeClass("activeBtn");
        }

        function hideReg(e) {
            regBlock.fadeOut(300);
            $("#reg").addClass("activeBtn");
        }


        login.on("click", function(ev) {
            hideReg();
            showLogin();
        });

        reg.on("click", function(ev) {
            hideLogin();
            showReg();
        });

        loginBtn.on("click", function(e) {
            showLogin();
            hideReg();
        });
        regBtn.on("click", function(e) {
            hideLogin();
            showReg();
        })
    }());


    //вікно відновлення паролю
    $(".generatePass[data-toggle='modal']").on("click", function(e) {
        $('#Modal1').modal('hide');

    });


    //click на кнопку форми карегорій
    (function() {
        var iProductShow = $(".labelWhatBtn");
        var iTableProd = $(".selectProduct");
        var history = $(".header #WRAPPANEL");
        var whenBlock = $(".header .whenBlock");


        function whenBlockHide() {
            whenBlock.fadeOut(300)
        }


        function iTableProdShow() {
            iTableProd.css("left", $(".rent").offset().left).fadeIn(300);
            isShowFilterList = true;
        }

        function iTableProdHide() {
            iTableProd.fadeOut(300);
            isShowFilterList = false;
        }


        function borderBtnShow() {
            iProductShow.css('border', '1px solid #E35F46');
        }

        function borderBtnHide() {
            iProductShow.css('border', '1px solid #d8d8d8');
        }

        function hideHistory1() {
            history.hide();
        }

        iProductShow.on("click", function(ev) {
            ev.stopPropagation();
            iTableProdShow();
            hideHistory1();
            borderBtnShow();
            whenBlockHide();
            $(".labelWhatBtn").addClass("rotateBtn");
        });


        $(document).on("click", function(ev) {
            if ($(ev.target).closest(".selectProduct, .input-floating.fst").length) {
                return;
            }
            isShowFilterList = false;
            $(".labelWhatBtn").removeClass("rotateBtn");
            iTableProdHide();
            borderBtnHide();
            // hideRent();
        })
    }());


    //click на кнопку форми категорій головної сторінки
    (function() {
        var iProductShow = $(".btnBig");
        var iTableProd = $(".selectProductTitle");
        var history = $("#WRAPPANELhead");
        var whenBlock = $(".whenBlockTitle");

        function whenBlockHide() {
            whenBlock.fadeOut(300)
        }


        function iTableProdShow() {
            var position = $("#whoSearchLeft").offset();
            iTableProd.css({"left": position.left, "top": position.top}).fadeIn(300);
            isShowFilterList = true;
        }

        function iTableProdHide() {
            iTableProd.fadeOut(300);
            isShowFilterList = false;
        }

        function hideHistory() {
            history.fadeOut(300);
        }

        iProductShow.on("click", function(ev) {
            ev.stopPropagation();
            iTableProdShow();
            hideHistory();
            whenBlockHide();
            $(".btnBig").addClass("rotateBtn");
            $(".aTopLeft").css("color", "#E35F46");

        });


        $(document).on("click", function(ev) {
            if ($(ev.target).closest(".selectProductTitle,form.left").length) {
                return;
            }
            isShowFilterList = false;
            $(".btnBig").removeClass("rotateBtn");
            iTableProdHide();
            // hideRent();
            $(".atopleft").css("color", "#000");
        })
    }());


    //відкриває історію на головній сторінці
    (function() {
        var leftInput = $(".left .inputSearchHeadLeft");
        var history = $("#WRAPPANELhead");
        var listWindow = $(".selectProductTitle");
        var sityWindow = $(".whenBlockTitle");


        function showHistory() {
            var position = $("#whoSearchLeft").offset();
            history.css({"left": position.left, "top": position.top}).fadeIn(300);
        }

        function hideHistory() {
            history.fadeOut(300);
        }

        function hideListWindow() {
            listWindow.fadeOut(300);
        }

        function hideSityWindow() {
            sityWindow.fadeOut(300);
        }


        leftInput.focus(function(e) {
            showHistory();
            hideListWindow();
            hideSityWindow();
            $(".aTopLeft").css("color", "#E35F46");

        });
        $(document).on("click", function(e) {
            if ($(e.target).closest("#WRAPPANELhead,form.titleSearch").length) {
                return;
            }
            hideHistory();
            $(".aTopLeft").css("color", "#000");
            leftInput.attr('placeholder');
        });


    }());


    //кілік по історї
    (function() {
        var li = $("#WRAPPANELhead .liText");


    }());


    //відкриває категорії і підкатегорії
    (function() {
        var categoryId = 0;
        var subcategoryId = 0;

        var lb = $(".selectProduct .leftSelect ul");
        var cb = $(".selectProduct .centerSelect ul");
        var rb = $(".selectProduct .rightSelect ul");
        var lbDs = $(".selectProduct .leftSelectDisplay");


        lb.on("click", "li", function(e) {
            lbDs.fadeOut(300);
            cb.fadeIn(300);
            var b = $(e.target || e.srcElement);
            b.closest("li")
                .siblings()
                .removeClass("activeList")
                .end()
                .addClass("activeList");
            categoryId = $(e.target).closest("li", lb).attr("id");

            cb.empty();
            if (categoryNestedList[categoryId] === undefined) return false;

            $.each(categoryNestedList[categoryId]['node'], function(index, value) {
                cb.append($("<li>", {text: value['name'], id: value['id']}));
            });
        });

        cb.on("click", "li", function(e) {

            var b = $(e.target || e.srcElement);
            b.closest("li")
                .siblings()
                .removeClass("activeList")
                .end()
                .addClass("activeList");
            var li = $(e.target).closest("li", cb);
            if (!li.length) return false;

            subcategoryId = li.attr("id");
            rb.empty();
            rb.fadeIn(300);


            if (categoryNestedList[categoryId]['node'][subcategoryId] === undefined) return false;
            $.each(categoryNestedList[categoryId]['node'][subcategoryId]['node'], function(index, value) {
                rb.append($("<li>", {text: value['name'], id: value['id']}));
            });
        });

        rb.on("click", "li", function(event) {
            $("#what_input-header").val($(event.target).text(), 300).trigger("change");
            lbDs.fadeIn(300);
            categoryId = 0;
            subcategoryId = 0;

            $(event.target).trigger("changed_category", [$(event.currentTarget).attr("id")]);

            cb.empty();
            rb.empty();
        });
    }());


    //відкриває список на головній сторінці
    (function() {
        var categoryId = 0;
        var subcategoryId = 0;

        var lb = $(".selectProductTitle .leftSelect ul");
        var cb = $(".selectProductTitle .centerSelect ul");
        var rb = $(".selectProductTitle .rightSelect ul");
        var lbDs = $(".selectProductTitle .leftSelectDisplay");


        lb.on("click", "li", function(e) {
            lbDs.fadeOut(300);
            cb.fadeIn(300);
            var b = $(e.target || e.srcElement);
            b.closest("li")
                .siblings()
                .removeClass("activeList")
                .end()
                .addClass("activeList");
            categoryId = $(e.target).closest("li", lb).attr("id");

            cb.empty();
            if (categoryNestedList[categoryId] === undefined) return false;

            $.each(categoryNestedList[categoryId]['node'], function(index, value) {
                cb.append($("<li>", {text: value['name'], id: value['id']}));
            });
        });

        cb.on("click", "li", function(e) {

            var b = $(e.target || e.srcElement);
            b.closest("li")
                .siblings()
                .removeClass("activeList")
                .end()
                .addClass("activeList");
            var li = $(e.target).closest("li", cb);
            if (!li.length) return false;

            subcategoryId = li.attr("id");
            rb.empty();
            rb.fadeIn(300);


            if (categoryNestedList[categoryId]['node'][subcategoryId] === undefined) return false;
            $.each(categoryNestedList[categoryId]['node'][subcategoryId]['node'], function(index, value) {
                rb.append($("<li>", {text: value['name'], id: value['id']}));
            });
        });

        rb.on("click", "li", function(event) {
            $("#whoSearchLeft").val($(event.target).text(), 300).trigger("change");
            lbDs.fadeIn(300);
            categoryId = 0;
            subcategoryId = 0;

            $(event.target).trigger("changed_category", [$(event.currentTarget).attr("id")]);

            cb.empty();
            rb.empty();
        });
    }());


    //показує вікно для міст
    (function() {
        var inputWhere = $(".rentBlockWhat");
        var iTableProd = $(".selectProduct");
        var history = $(".header #WRAPPANEL");

        function iTableProdHide() {
            iTableProd.fadeOut(300);
        }

        function hideHistory() {
            history.hide();
        }

        inputWhere.focus(function() {
            hideHistory();
            iTableProdHide();
        });

        $(document).on("click", function(ev) {
            if ($(ev.target).closest(".whenBlock, .input-floating.fff").length) {
                return;
            }
            isShowFilterList = false;
        })
    }());


    //показує вікно для міст в заголовку на головній сторінці
    (function() {
        var inputWhere = $(".inputSearchHead");
        var iTableProd = $(".selectProduct");
        var history = $("#WRAPPANELhead");

        function iTableProdHide() {
            iTableProd.fadeOut(300);
        }

        function hideHistory() {
            history.fadeOut(300);
        }

        inputWhere.focus(function(em) {
            $(".aTop").css("color", "#E35F46");
            iTableProdHide();
            hideHistory();
        });

        $(document).on("click", function(ev) {
            if ($(ev.target).closest(".whenBlockTitle,form.titleSearch").length) {
                return;
            }
            isShowFilterList = false;
            $(".aTop").css("color", "#000");
        })
    }());


    $(function() {
        var history = $(".header #WRAPPANEL");
        var showDatePicker = $(".header .rentBlockWhenRight");

        function hideHistory() {
            history.hide();
        }

        showDatePicker.on("focus", function(e) {
            hideHistory();
        });
    });


    //календар в хедері
    (function() {
        var language = $("html").attr("lang").substr(0, 2);

        function formatDate(date) {

            var dd = date.getDate();
            if (dd < 10) dd = '0' + dd;

            var mm = date.getMonth() + 1;
            if (mm < 10) mm = '0' + mm;

            var yy = date.getFullYear();

            return dd + '.' + mm + '.' + yy;
        }

        function lang(l) {
            var w = {
                ru: {
                    days: 'дней'
                }
            };

            return _.get(w, [language, l], _.get(w ["ru", l], ""));
        }

        try {
            $("#from").dateRangePicker({
                autoClose: true,
                format: 'DD.MM.YYYY',
                separator: '                 ',
                language: language,
                startOfWeek: 'monday',// or monday
                getValue: function() {
                    return $(this).val();
                },
                setValue: function(s) {
                    if (!$(this).attr('readonly') && !$(this).is(':disabled') && s != $(this).val()) {
                        $(this).val(s);
                    }
                },
                startDate: false,
                endDate: false,
                time: {
                    enabled: false
                },
                minDays: 0,
                maxDays: 0,
                showShortcuts: false,
                shortcuts: {
                    //'prev-days': [1,3,5,7],
                    //'next-days': [3,5,7],
                    //'prev' : ['week','month','year'],
                    //'next' : ['week','month','year']
                },
                customShortcuts: [],
                inline: false,
                container: 'body',
                alwaysOpen: false,
                singleDate: false,
                lookBehind: false,
                batchMode: false,
                duration: 200,
                stickyMonths: false,
                dayDivAttrs: [],
                dayTdAttrs: [],
                applyBtnClass: '',
                singleMonth: 'auto',
                hoveringTooltip: function(days, startTime, hoveringTime) {
                    return days > 1 ? days + ' ' + lang('days') : '';
                },
                showTopbar: true,
                swapTime: false,
                selectForward: false,
                selectBackward: false,
                showWeekNumbers: false,
                customArrowPrevSymbol: '<i class="slick2-next" href="#" ><img class="aa"  src="/img/category/prevSlider.svg" > </i>',
                customArrowNextSymbol: '<i class="slick2-next" href="#" ><img class="aa"  src="/img/category/nextSlider.svg" > </i>',
                getWeekNumber: function(date) //date will be the first day of a week
                {
                    return moment(date).format('w');
                }
            });
        } catch (e) {
            console.error("error", e.message);
        }


        $("#from").on("datepicker-open", function(event, obj) {
            var btn = $(".fooorm").find(".btnSend");
            var box = $(obj.relatedTarget);
            console.log(btn.offset().left, btn.outerWidth(), box.width());
            box.css("left", btn.offset().left + btn.outerWidth() - box.width());
        })
            .on("datepicker-first-date-selected", function(event, obj) {
                var dt = new Date(obj.date1);
                var str = formatDate(dt);


                if (!$(this).attr('readonly') && !$(this).is(':disabled') && str != $(this).val()) {
                    $(this).val(str).trigger("change");
                }
            });


        $("#from").on("focus", function(event) {
            $(event.target).click();
        })

    }());


    //календар в заголовку
    (function() {
        var language = $("html").attr("lang").substr(0, 2);

        function formatDate(date) {

            var dd = date.getDate();
            if (dd < 10) dd = '0' + dd;

            var mm = date.getMonth() + 1;
            if (mm < 10) mm = '0' + mm;

            var yy = date.getFullYear();

            return dd + '.' + mm + '.' + yy;
        }

        function lang(l) {
            var w = {
                ru: {
                    days: 'дней'
                }
            };

            return _.get(w, [language, l], _.get(w ["ru", l], ""));
        }

        var config = {
            autoClose: true,
            format: 'DD.MM.YYYY',
            separator: '                  ',
            language: language,
            startOfWeek: 'monday',// or monday
            getValue: function() {
                if ($('#getDate').val() && $('#returnDate').val())
                    return $('#getDate').val() + ' to ' + $('#returnDate').val();
                else return '';
            },
            setValue: function(s, s1, s2) {
                $('#getDate').val(s1);
                $('#returnDate').val(s2);
                // console.log('s:', s, 's1:', s1, 's2:', s2);
            },
            startDate: false,
            endDate: false,
            time: {
                enabled: false
            },
            minDays: 0,
            maxDays: 0,
            showShortcuts: false,
            shortcuts: {
                //'prev-days': [1,3,5,7],
                //'next-days': [3,5,7],
                //'prev' : ['week','month','year'],
                //'next' : ['week','month','year']
            },
            customShortcuts: [],
            inline: false,
            container: 'body',
            alwaysOpen: false,
            singleDate: false,
            lookBehind: false,
            batchMode: false,
            duration: 200,
            stickyMonths: false,
            dayDivAttrs: [],
            dayTdAttrs: [],
            applyBtnClass: '',
            singleMonth: 'auto',
            hoveringTooltip: function(days, startTime, hoveringTime) {
                return days > 1 ? days + ' ' + lang('days') : '';
            },
            showTopbar: true,
            swapTime: false,
            selectForward: false,
            selectBackward: false,
            showWeekNumbers: false,
            customArrowPrevSymbol: '<i class="slick2-next" href="#" ><img class="aa"  src="/img/category/prevSlider.svg" > </i>',
            customArrowNextSymbol: '<i class="slick2-next" href="#" ><img class="aa"  src="/img/category/nextSlider.svg" > </i>',
            getWeekNumber: function(date) //date will be the first day of a week
            {
                return moment(date).format('w');
            }
        };

        try {
            $("#getDate").dateRangePicker(config);
            $("#returnDate").dateRangePicker(config);
        } catch (e) {
            console.error('error', e.message);
        }

        $("#getDate").on("datepicker-open", function(event, obj) {
            var btn = $(".titleSearch").find(".btnSearch");
            var box = $(obj.relatedTarget);
            box.css("left", btn.offset().left + 20 + btn.outerWidth() - box.width());
        })
            .on("datepicker-first-date-selected", function(event, obj) {
                var dt = new Date(obj.date1);
                var str = formatDate(dt);


                if (!$(this).attr('readonly') && !$(this).is(':disabled') && str != $(this).val()) {
                    $(this).val(str).trigger("change");
                }
            });


        $("#getDate").on("focus", function(event) {
            $(event.target).click();
        });

        $("#getDate,#returnDate")
            .on("datepicker-change", function(event, obj) {
                $("#getDate,#returnDate")
                    .not(event.currentTarget)
                    .data("dateRangePicker")
                    .setDateRange(moment(obj.date1).format("DD.MM.YYYY"), moment(obj.date2).format("DD.MM.YYYY"), true);
            });
    }());


    //показує вікно для розміщення товару
    (function() {
        var categoryId = 0;
        var subcategoryId = 0;

        var oCateg = $(".categoryPopup .wrapCategory a");
        var vdBlock = $(".categoryPopup .voidBlock");
        var cSelect = $(".categoryPopup .centerSelect ul");
        var rSelect = $(".categoryPopup .rightSelect ul");
        var hTitle = $(".categoryPopup .hiddenTitleCateg");
        var hSubTitle = $(".categoryPopup .hiddenTitleSubCateg");
        var formProd = $(".categoryPopup #append-product-form");
        var btn1 = $(".bottomPopup .send");

        oCateg.on("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            vdBlock.hide();
            hTitle.show(600);
            cSelect.show(600);
            var b = $(e.target || e.srcElement);
            b.closest(".oneCategory")
                .siblings()
                .removeClass("activeCheck")
                .end()
                .addClass("activeCheck");

            formProd.fadeOut();

            categoryId = $(e.target).closest("a").data("id");
            cSelect.empty();
            if (categoryNestedList[categoryId]['node'] === undefined) return false;

            $.each(categoryNestedList[categoryId]['node'], function(index, value) {
                cSelect.append($("<li>", {text: value['name'], 'data-id': index}));
            });


        });

        cSelect.on("mouseenter", "li", function(e) {
            hSubTitle.show(600);
            var b = $(e.target || e.srcElement);
            b.closest("li")
                .siblings()
                .removeClass("activeLi")
                .end()
                .addClass("activeLi");
            var li = $(e.target).closest("li", cSelect);
            if (!li.length) return false;

            subcategoryId = li.data("id");
            rSelect.empty().show(600);

            if (categoryNestedList[categoryId]['node'][subcategoryId]['node'] === undefined) {
                return false;
            }
            $.each(categoryNestedList[categoryId]['node'][subcategoryId]['node'], function(index, value) {
                rSelect.append($("<li>", {text: value['name'], 'data-id': index}));
            });
            console.log(categoryNestedList[categoryId]['node'][subcategoryId]['node']);
        });

        rSelect.on("click", "li", function(e) {
            var b = $(e.currentTarget);
            b
                .siblings()
                .removeClass("activeLi")
                .end()
                .addClass("activeLi");
            btn1.prop("disabled", false).addClass("activeBtnSend");

            $("#productform-category").val(b.data("id")).closest("form").trigger("submit");

            // cSelect.fadeOut(300);
            // rSelect.fadeOut(300);
            // formProd.fadeIn(300);
            // hTitle.fadeOut(300);
            // hSubTitle.fadeOut(300);
        });

        /*
         $(document.body).on("click", function () {
         vdBlock.fadeIn();
         hTitle.fadeOut();
         hSubTitle.fadeOut();
         categoryId = 0;
         subcategoryId = 0;
         cSelect.empty();
         rSelect.empty();
         });
         */
        return;


    }());


    // //кнопка відкриває\закриває весь список категорій на головній
    // (function () {
    //     var openList = $(".category .showAllHome");
    //     var sm6 = $(".category .slider .allCategoryBlock");
    //
    //     openList.on("click", function (e) {
    //         e.preventDefault();
    //
    //         sm6.removeClass("hidden-md hidden-lg hidden-sm hidden-xs");
    //         $(e.target).remove();
    //     });
    //
    // }());


    $(".btnRightRent").click(function() {
        $('.popup').bPopup({
            position: [0, 0, 0, 0],
            opacity: 0.9,
            display: 'fadeIn'
        });
        console.log("popup");
    });


    $(".btnCloce").click(function() {
        $('.b-modal').trigger("click");
    });


    //hover на розместить обьявление
    $(function() {
        var popovers = $('[data-toggle="popover"]');
        popovers
            .popover()
            .on('shown.bs.popover', function(event) {
                var pop = $('.popover.bottom');
                // var elem = $(event.target);
                pop.css('left', $(".btnRightRent").offset().left).fadeIn();
                pop.children(".arrow").css("left", "7%");
                // pop.css('left',parseInt($('.popover').css('top')) + 22 + 'px');
            });
    });


    //star ratimg
    $(function() {
        try {
            $(".rateyoHome").rateYo({
                ratedFill: "#E35F46",
                starWidth: "17px",
                numStars: 5,
                fullStar: true,
                // readOnly: true,
                rating: 3.5,
                "starSvg": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">'
                +
                '<path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"/>'
                +
                '</svg>',
                onInit: function(rating, rateYoInstance) {
                    rateYoInstance.rating($(rateYoInstance.node).data("rating"));
                    rateYoInstance.option("readOnly", true);
                }
            });
        } catch (e) {
            console.error('error', e.message);
        }

    });


    //    scrollBar window
    try {
        $(".scroll-1").perfectScrollbar({
            wheelSpeed: 0.1,
            maxScrollbarLength: 80,
            minScrollbarLength: 50
        });
    } catch (e) {
        console.error('error', e.message);
    }


    //    перехід по ентеру між інпутами
    $(function() {
        $(".rent form").on("submit", function(event) {
            var form = $(event.target);
            var inps = form.find("input.focusInput");

            if (inps.eq(0).is(":focus")) {
                event.preventDefault();
                inps.eq(1).focus();
            } else if (inps.eq(1).is(":focus")) {
                event.preventDefault();
                inps.eq(2).focus();
            }
        });
    });


    //    перехід по ентеру між інпутами
    $(function() {
        $(".headSearch form").on("submit", function(event) {
            var form = $(event.target);
            var inps = form.find("input.focusInput1");

            if (inps.eq(0).is(":focus")) {
                event.preventDefault();
                inps.eq(1).focus();
            }
            else if (inps.eq(1).is(":focus")) {
                event.preventDefault();
                inps.eq(2).focus();
            }
        });
    });


//    стилі чекбокс
    (function() {
        $(".redCheckBox").iCheck({
            checkboxClass: 'icheckbox_flat-red',
            radioClass: 'iradio_flat-red'
        });
    }());


// //    відкриває вікно для виводу повідомлень з чату на гол. стор.
//     (function () {
//         var window = $("#massageHeader");
//         var btn = $(".rightRent .message");
//         // var count = btn.find(".dynamicHelp");
//
//         btn.on("click", function (ev) {
//
//             window.fadeIn(300);
//             ev.stopPropagation();
//         });
//         $("body").on("click", function () {
//             window.fadeOut(300);
//         });
//     }());

    //masagees
    $("#masenges").modal({
        show: false,
        backdrop: false,
    });


}(window.jQuery));