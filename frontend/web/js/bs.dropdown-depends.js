/**
 * Created by Planess group on 24.07.2017.
 */

jQuery(function($) {
// выбор элемента выпадающего списка
    $(document).on("click", ".dropdown-menu", function(event) {
        var ul = $(event.currentTarget);
        var target = $(event.target);
        var li = target.closest("li", event.currentTarget);
        if (!li.length) return;

        var a = target.closest("a", event.currentTarget);
        if (a.length && a.attr("href") !== "#") {
            return;
        }

        event.preventDefault();

        // добавить выделение элемента меню
        selectActive(li);

        // если есть кнопка меню, нужно там поменять текст
        setLabel(ul, li);

        var targetid = ul.data("target");
        var val = li.data("value");
        if (val === undefined) {
            val = _.trim(li.text());
        }

        if (targetid !== undefined) {
            $("input#" + targetid).val(val);
            ul.trigger("change.bs.dropdown", [val, _.trim(li.text())]);

            // обновить зависимые меню
            $(".dropdown-menu").each(function(index, element) {
                var ul = $(element);
                if (ul.data("depend") !== targetid) return;

                updateDepends(ul, ul.data("target"), val);

                // сбросить значение зависимого списка
                reset(ul);
            });
        } else {
            ul.trigger("change.bs.dropdown", [val, _.trim(li.text())]);
        }
    });

// первая инициализация
    $(".dropdown-menu").each(function(index, element) {
        var ul = $(element);
        var target = ul.data("target");
        if (target === undefined) return;

        var val = $("#" + target).val();

        var dtarget = ul.data("depend");
        if (dtarget !== undefined) {
            // первоначально добавим выпадающий список для зависимого элемента
            updateDepends(ul, target, $("#" + dtarget).val());
        }

        // выделить элемент из списка
        var li = selectActive(ul, val);

        // если есть кнопка меню, нужно там поменять текст
        setLabel(ul, li);
    });

    function setLabel(ul, activeli) {
        var btntoggle = ul.siblings(".dropdown-toggle").first();

        if (btntoggle.length) {
            var t = "";

            if (activeli !== undefined && activeli.length) {
                var ch = activeli.children("a");
                var t = ch.length ? ch.html() : activeli.html();

                btntoggle.html(t).removeClass("default");
            } else {
                t = btntoggle.data("prompt") || "";
                btntoggle.text(_.trim(t)).addClass("default");
            }

            btntoggle.append('<span class="caret"></span>');
        }

        return btntoggle;
    }

    function selectActive(target, val) {
        var li = $([]);
        if (target.is("ul")) {
            li = target.children("li").filter(function(index, element) {
                var elem = $(element);
                var v = elem.data("value");
                var vu = _.trim(elem.text());

                return (v !== undefined && v == val) || vu == val;
            }).first();
        } else if (target.is("li")) {
            li = target;
        }

        li.addClass("active").siblings("li").removeClass("active");
        return li;
    }

    function updateDepends(ul, id, depend_val) {
        ul.children("li").remove();

        var nv = $([]);
        // dropdowndepends - внешняя переменная
        $.each(_.get(dropdowndepends, id + "." + depend_val, []), function(index, val) {
            var el = $('<li></li>', {
                html: '<a href="#" tabindex="-1">' + val + '</a></li>',
                data: {value: val, vtext: val}
            });
            nv = nv.add(el);
        });

        ul.prepend(nv);
    }

    function reset(ul){
        ul.children("li").removeClass("active");
        setLabel(ul);

        var tinput = ul.data("target");
        if (tinput !== undefined){
            $("#" + tinput).val("");

            // обновить зависимые меню
            $(".dropdown-menu").each(function(index, element) {
                var ul = $(element);
                if (ul.data("depend") !== tinput) return;

                // сбросить значение зависимого списка
                reset(ul);
            });
        }
    }
});
