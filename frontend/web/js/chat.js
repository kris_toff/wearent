/**
 * Created by Planess group on 04.08.2017.
 */

(function($, _, angular) {
    "use strict";

    var translation = {
        today: {
            ru: "[Сегодня]"
        }
    };

    var app = angular.module("pgaPage");

    app
        .controller("ListController", [
            "$log",
            "$scope",
            "$interval",
            "Conversations",
            "$timeout",
            "$mdMedia",
            function($log, $scope, $interval, Conversations, $timeout, $mdMedia) {
                var ctrl = this;
                $scope.onlyfavorite = 0;
                $scope.filterWord = "";

                ctrl._mdMedia = $mdMedia;

                $scope.$watchCollection(function() {
                    return Conversations.getList();
                }, _.debounce(function() {
                    // console.log('update');
                    $timeout(function() {
                        var f = angular.element("#list_of_chats").find(".list-group-item").has(".budge");

                        var i = angular.element("#massageHeader").find(".list-group");
                        var c = angular.element("#logoDroppMenu").find(".countMassage");
                        i.empty();
                        c.empty().addClass("hidden");

                        if (f.length) {
                            angular.element(".header .rightRent .message .dynamicHelp").text("(" + f.length + ")");
                            c.removeClass("hidden").text(f.length);

                            f.each(function(index, element) {
                                var y = $(element).clone();
                                y.children().not(".media").remove();

                                i.append(y.prop("outerHTML"));
                            });
                        }


                        var ca = $("#conversation_area");
                        var cae = ca.get(0);

                        var currentPosition = ca.scrollTop();
                        var fullHeight = _.max([cae.scrollHeight, cae.clientHeight]);
                        var newPosition = fullHeight - _.min([
                                cae.clientHeight,
                                cae.offsetHeight
                            ]);

                        if (_.inRange(currentPosition, newPosition - 2)) {
                            // $log.log(newPosition);
                            ca.scrollTop(newPosition);
                            // angular.element("#chatWindowPanel")
                            //     .find(".rightChat .blockContent")
                            //     .perfectScrollbar("destroy").perfectScrollbar();
                            // ca.animate({
                            //     scrollTop: newPosition
                            // }, 200);
                        }
                        ca.perfectScrollbar("update");
                    });
                }, 450, {maxWait: 1000}));

                ctrl.conversations = function() {
                    var y = Conversations.getList();

                    if ($scope.onlyFavorite) {
                        // _.property shorthand, is_favorite must be TRUE
                        y = _.filter(y, "is_favorite");
                    }
                    if ($scope.filterWord.length) {
                        var regex = new RegExp($scope.filterWord, "i");
                        y = _.filter(y, function(o) {
                            return regex.test(o.subject)
                                || regex.test(o.lastMessage)
                                || regex.test(o.interlocutor_name);
                        });
                    }

                    return y;
                }

                ctrl.setActive = function(id) {
                    Conversations.setActiveDialog(id);
                };

                ctrl.fc = function() {
                    return Conversations.getFavouriteAmount();
                }
            }
        ])
        .controller("ChatController", [
            "$scope",
            "$log",
            "Conversations",
            function($scope, $log, Conversations) {
                var ctrl = this;

                // старое название диалога, на случай замены заголовка
                var oldTitle;

                ctrl.model = function() {
                    return Conversations.getActiveModel();
                }
                ctrl.editTitle = false;

                // сообщения активного чата
                ctrl.messages = function() {
                    return Conversations.getMessages();
                };

                // кнопка закрепления чата в избранных
                $scope.toggleSnap = function() {
                    Conversations.toggleSnap();
                };

                // редактирование заголовка
                $scope.changeTitle = function($event) {
                    oldTitle = angular.element($event.currentTarget).text();

                    ctrl.editTitle = true;
                };
                $scope.finishChangeTitle = function($event) {
                    Conversations.setTitle(oldTitle, angular.element($event.currentTarget).text());

                    ctrl.editTitle = false;
                };


                // отправка нового сообщения
                $scope.sendMessage = function($event) {
                    $event.preventDefault();

                    var $form = angular.element($event.currentTarget);
                    var fd = new FormData($form.get(0));
                    fd.append("message", $form.find(".textEdit").text());

                    if (false !== Conversations.addMessage(fd)) {
                        // очистим форму
                        $form.get(0).reset();
                        $form
                            .find(".textEdit")
                            .contents()
                            .not(".ps-scrollbar-x-rail, .ps-scrollbar-y-rail").remove();
                    }
                };
            }
        ]);

    app
        .component("conversationItem", {
            templateUrl: "/conversation/template?p=ci",
            bindings: {
                model: "<",
                index: "<",
                fw: "<filterWord"
            },
            controller: [
                "$log", "Conversations", "$scope", "$sce", function($log, Conversations, $scope, $sce) {
                    var ctrl = this;

                    $scope.relatedTime = function(t) {
                        return moment.unix(t).fromNow();
                    };
                    ctrl.isActive = function() {
                        return angular.equals(ctrl.model, Conversations.getActiveModel());
                    };

                    $scope.getData = function(v) {
                        var s = _.get(ctrl.model, v, "");
                        var r = ctrl.fw
                        && s ? _.replace(s, new RegExp(ctrl.fw, "gi"), '<span class="selection">$&</span>') : s;

                        return $sce.trustAs($sce.HTML, r);
                    };
                }
            ]
        })
        .component("conversationMessageItem", {
            templateUrl: "/conversation/template?p=mi",
            bindings: {
                model: "<",
                // il - true if last item
                il: "<"
            },
            controller: [
                "$scope",
                "$timeout",
                "$log",
                function($scope, $timeout, $log) {
                    var ctrl = this;
                    var down = true;

                    ctrl.$onInit = function() {
                        // console.log('init');
                    };
                    ctrl.$onChanges = function() {
                        // console.log('change');
                    };
                    ctrl.$doCheck = function() {
                        // console.log('odcheck');
                        if (ctrl.il && down) {
                            down = false;

                            // $timeout(function () {
                            //     var ca = $("#conversation_area");
                            //     var cae = ca.get(0);
                            //
                            //     var currentPosition = ca.scrollTop();
                            //     var fullHeight = _.max([cae.scrollHeight, cae.clientHeight]);
                            //     var newPosition = fullHeight - _.min([
                            //             cae.clientHeight,
                            //             cae.offsetHeight
                            //         ]);
                            //
                            //     if (_.inRange(currentPosition, newPosition - 2)) {
                            //         $log.log(newPosition);
                            //         ca.scrollTop(newPosition);
                            //         angular.element("#chatWindowPanel")
                            //             .find(".rightChat .blockContent")
                            //             .perfectScrollbar("destroy").perfectScrollbar();
                            //         // ca.animate({
                            //         //     scrollTop: newPosition
                            //         // }, 200);
                            //     }
                            // }, 10);
                        }
                    };

                    ctrl.relatedTime = function(t) {
                        return moment.unix(t).fromNow();
                    };

                    ctrl.relatedDate = function() {
                        return moment.unix(ctrl.model.created_at).calendar(null, {
                            sameDay: function() {
                                return _.get(translation, ["today", this.locale()], "[Today]");
                            },
                            lastDay: function() {
                                return "[" + this.startOf("day").fromNow() + "]";
                            },
                            lastWeek: function() {
                                return "[" + this.startOf("day").fromNow() + "]";
                            },
                            sameElse: "DD.MM.YYYY"
                        });
                    };
                }
            ]
        });


    app
        .service("Conversations", ["$resource", "$log", "$timeout", Conversations]);

    function Conversations($resource, $log, $timeout) {
        var self = this;
        var Cnv = $resource("/conversation-rest-data/:id", {
            id: "@id"
        }, {
            update: {
                method: "PUT",
                cache: false,
                timeout: 1500,
                cancellable: true,
                hasBody: true
            }
        });
        var Mnv = $resource("/conversation-message-rest-data/:id", null, {
            add: {
                method: "POST",
                timeout: 2500,
                cache: false,
                cancellable: false,
                hasBody: true,
                headers: {
                    "Content-Type": undefined
                },
                transformRequest: angular.identity
            }
        });

        var list = Cnv.query();
        var tempL;
        var tempM;
        var messageList = {
            0: [
                // array of message objects
                {},
                {}
            ]
        };
        var activeModel;

        // private methods
        var realyConversationId = function() {
            return _.get(self.getActiveModel(), "id");
        };
        var updateData = function() {
            tempL = Cnv.query(function() {
                var oldId = _.get(activeModel, "id");
// console.log('temp start',tempL);
// tempL[0]["subject"] = "test";
                _.pullAllWith(tempL, list, _.isEqual);
                _.pullAllBy(list, tempL, "id");
// console.log('list middle',list);
                list = _.concat(list, tempL);
// console.log('temp end',tempL, list);

                if (!_.isUndefined(oldId)) {
                    activeModel = _.find(list, {id: oldId});
                }
// console.log(activeModel);
                _.delay(updateData, 10000);
            }, function() {
                _.delay(updateData, 10000);
            });
        };
        var updateMessages = function() {
            var ml = _.omit(messageList, [0]);
            if (_.isEmpty(ml)) {
                _.delay(updateMessages, 4500);
// console.log('return');
                return false;
            }

            var cids = _.keys(ml);
            var c1 = _.mapValues(ml, function(o) {
                return _.map(_.filter(o, function(v) {
                    return $.inArray(_.toInteger(v.status), [0, 1]) >= 0;
                }), "id");
            });
            var c2 = _.values(c1);
            _.remove(c2, _.isEmpty);
            var c3 = _.union(c2);
            var notread = _.join(c3);

            var params = {onlynew: 1, query_ids: _.join(cids)};
            if (!_.isEmpty(c3)) {
                params.nyv = notread;
            }
            if (!_.isUndefined(self.getActiveModel())) {
                params.active_dialog = _.get(self.getActiveModel(), "id");
            }

            tempM = Mnv.query(params, function(v) {
                var oldList;
                var nml;

                _.forEach(_.groupBy(v, "chat_id"), function(v, k) {
                    oldList = _.pullAllBy(_.get(messageList, k), v, "id");
                    nml = _.pullAllBy(v, oldList, "id");

                    _.set(messageList, k, _.concat(oldList, nml));

                    markPointOfDay(k);
                });

                _.delay(updateMessages, 4500);
            }, function() {
                _.delay(updateMessages, 4500);
            });
        };
        var markPointOfDay = function(id) {
            var m = _.get(messageList, id, []);

            var g = _.groupBy(m, function(o) {
                return moment.unix(o.created_at).startOf("day").valueOf();
            });
            g = _.mapValues(g, function(o) {
                return _.get(_.minBy(o, "created_at"), "id");
            });

            _.forEach(g, function(v) {
                _.set(_.find(m, {id: v}), "firstOnDay", true);
            });

            // console.log('group', g, m);
        };

        // _.delay(updateData, 10000);
        // _.delay(updateMessages, 4500);

        // public methods
        // getter
        this.getList = function() {
            return list;
        };
        this.getActiveModel = function() {
            return activeModel;
        };
        this.getFavouriteAmount = function() {
            return _.reduce(list, function(accumulator, value, index) {
                return accumulator + _.toInteger(_.get(value, "is_favorite", 0));
            }, 0);
        };
        this.getMessages = function() {
// console.log(messageList);
            return _.get(messageList, realyConversationId());
        };

        // setter
        this.setActiveDialog = function(id) {
            var newActiveModel = _.find(list, {id: id});
            if (angular.equals(activeModel, newActiveModel)) {
                return false;
            }

            activeModel = newActiveModel;

            if (_.isEmpty(_.get(messageList, id))) {
                _.set(messageList, id, Mnv.query({
                    query_ids: id,
                    active_dialog: id
                }, function() {
                    markPointOfDay(id);
                }));
            }
        };
        this.toggleSnap = _.throttle(function() {
            if (_.isUndefined(self.getActiveModel())) {
                return false;
            }

            var oldValue = self.getActiveModel()["is_favorite"];
            _.set(self.getActiveModel(), "is_favorite", _.toInteger(!oldValue));

            _.invoke(self.getActiveModel(), "$update", null, null, function() {
                _.set(self.getActiveModel(), "is_favorite", oldValue);
            });
        }, 750, {trailing: false});
        this.setTitle = function(oldValue, newValue) {
            oldValue = _.trim(oldValue);
            newValue = _.trim(newValue);

            if (angular.equals(oldValue, newValue) || _.isUndefined(this.getActiveModel())) {
                return false;
            }

            _.set(this.getActiveModel(), "subject", newValue);

            _.invoke(this.getActiveModel(), "$update", null, null, function() {
                _.set(self.getActiveModel(), "subject", oldValue);
            });
        };

        this.addMessage = function(data) {
            if (_.isUndefined(this.getActiveModel())) {
                return false;
            }

            // список сообщений
            var id = realyConversationId();
            var m = _.get(messageList, id, []);

            // добавление в список...
            var MR = new Mnv({
                chat_id: id,
                message: data.get("message"),
                created_at: _.toInteger(moment().valueOf() / 1000)
            });
            m.push(MR);
            var ti = m.length;

            // отправляем на сервер
            data.set("chat_id", id);
            Mnv.add({}, data, function(v) {
                $log.log(v);
                m[ti - 1] = _.cloneDeep(v);

                markPointOfDay(id);
                // обновим чат, чтобы он переместился вверх в списке
                _.set(self.getActiveModel(), "updated_at", _.toInteger(moment().valueOf() / 1000));
            }, function() {
                $log.log('error', arguments);
                _.pullAt(m, ti - 1);
            });

            $timeout(function() {
                var ca = $("conversation_area");
                ca.scrollTop(ca.prop("scrollHeight") - ca.height());
            });
        };
        this.createNewDialog = function(type) {
            $log.log('create new dialog', type);
        };
    }


    app
        .run([
            "Conversations", function(C) {
                // нажатия на кнопку добавления файла к сообщению
                $("#chatWindowPanel").on("click", "form .attach label", function() {
                    if (!_.isUndefined(C.getActiveModel())) {
                        angular.element("#addForMessage").trigger("click");
                    }
                });

                // нажатие на одно из сообщений в блоке новых сообщений сверху страницы
                angular.element("#massageHeader").on("click", ".list-group .list-group-item", function($event) {
                    var item = angular.element($event.currentTarget);
                    var chat_id = item.data("id");

                    $("#chatWindowPanel").modal("show");
                    C.setActiveDialog(chat_id);
                });

                // нажатие на кнопку открытия сообщений
                angular.element(".header .rightRent .message").on("click", function() {
                    if (angular.element("#massageHeader").find(".list-group .list-group-item").length) {
                        angular.element("#massageHeader").fadeIn(300);
                    } else {
                        angular.element("#chatWindowPanel").modal("show");
                    }
                });
                angular.element("#logoDroppMenu").find("li:eq(3)").on("click", function() {
                    angular.element("#chatWindowPanel").modal("show");
                });
            }
        ]);


    //open rightChat for screen < 650px


    (function () {

        var massage = $("#chatWindowPanel .leftChat");

        var rightChat = $("#chatWindowPanel .rightChat");
        var inputSearch = $(".leftChat .searchField");
        var back = rightChat.find(".backXs");
        var openSearch = $(".leftChat .searchMobileMasaage");
        var closeBtn = $(".leftChat .closeSearchInput");
        var btnSupport = $(".leftChat .blockFooter");

        //відкриває\закривє поле пошуку на мобільному чаті
        openSearch.on("click", function () {
            inputSearch.removeClass("hidden-xs");
        });

        closeBtn.on("click", function () {
            inputSearch.addClass("hidden-xs");
        });
        //відкриває\закриває праву сторону чату
        massage.on("click", ".list-group-item", function() {
            rightChat.removeClass("hiddenRightXs");
            inputSearch.addClass("hidden-xs");
            btnSupport.addClass("hidden-xs");

        });

        back.on("click", function() {
            rightChat.addClass("hiddenRightXs");
            btnSupport.removeClass("hidden-xs");
        });
     }());


}(window.jQuery, window._, window.angular));
