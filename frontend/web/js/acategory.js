;(function($, angular) {
    "use strict";

    var app = angular.module("pgaPage");
    app.controller("CategoryController", [
        "$scope", "$http", "$sce", function($scope, $http, $sce) {
            $scope.outcome = [];
            $scope.isFocusAutoComplete = false;

            $scope.scanAll = _.debounce(function($event) {
                var word = _.trim($($event.target).val());

                if (word.length) {
                    $http({
                        method: "get",
                        url: "/search/category-autocomplete",
                        params: {
                            word: word
                        },
                        cache: true,
                        timeout: 13000
                    }).then(function(response) {
                        $scope.outcome = _.cloneDeep(response.data);

                        $.each($scope.outcome, function(prop, value) {
                            value.label = $sce.trustAsHtml(_.replace(value.name, new RegExp(word, "gi"), '<span class="sel">$&</span>'));
                        });
                    });
                } else {
                    $scope.outcome = [];
                    $scope.$apply();
                }
            }, 300);
        }
    ]);
}(window.jQuery, window.angular));
