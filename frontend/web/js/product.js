/**
 * Created by Hp on 05.05.2017.
 */


(function ($) {
    'use strict';

    //click на читать полностью
    $(function () {
        var iReadAll = $("#iReadAll");
        var iRotateBtn = $(".rotateBtn");

        iReadAll.on("click", function (e) {
            iRotateBtn.css("transform", "rotate(180deg)");
        })

    }());

    $(function () {
        try {

            $(".rateyoProd").rateYo({
                ratedFill: "#E35F46",
                starWidth: "17px",
                numStars: 5

            });
        } catch (e) {
            console.error('error', e.message);
        }

    });


    try {
        $(".readmoreProduct").readmore({
            moreLink: '<a href="#">Читать полностью <img src="/img/editProfile/learnMoreRed.svg" alt="Learn more"></a>',
            lessLink: '<a href="#">Скрыть <img src="/img/editProfile/closeMoreRed.svg" alt="Close"></a>',
            speed: 75,
            maxHeight: 280
        });

        $(".readmoreFedback").readmore({
            moreLink: '<a href="#">Читать полностью <img src="/img/editProfile/learnMoreRed.svg" alt="Learn more"></a>',
            lessLink: '<a href="#">Скрыть <img src="/img/editProfile/closeMoreRed.svg" alt="Close"></a>',
            speed: 75,
            maxHeight: 40,
        });

        $(".blockaboutMe").readmore({
            moreLink: '<a href="#">Читать полностью <img src="/img/editProfile/learnMoreRed.svg" alt="Learn more"></a>',
            lessLink: '<a href="#">Скрыть <img src="/img/editProfile/closeMoreRed.svg" alt="Close"></a>',
            speed: 75,
            maxHeight: 105,
        });

        $(".list").readmore({
            moreLink: '<a href="#">Посмотреть все 13 данных <img src="/img/editProfile/learnMoreRed.svg" alt="Learn more"></a>',
            lessLink: '<a href="#">Скрыть <img src="/img/editProfile/closeMoreRed.svg" alt="Close"></a>',
            speed: 75,
            maxHeight: 170,
        });
    } catch (e) {
        console.error('error', e.message);
    }


//   інфо для оренди товару
    (function () {
        var discount = $(".sidebarProduct .yourDiscout");
        var bottomDiscount = $(".sidebarProduct .bottomDiscount");


        function showBottom() {
            bottomDiscount.fadeToggle(300);
        }

        discount.on("click", function (e) {
            showBottom();
            $(".yourDiscout").toggleClass("redText");
        })


    }());

    (function () {
        var pledge = $(".sidebarProduct .pledgeText");
        var blockPledge = $(".sidebarProduct .bottomPredge");

        function showBottom() {
            blockPledge.fadeToggle(300);
        }

        pledge.on("click", function () {
            showBottom();
            $(".pledgeText").toggleClass("redText");
        })
    }());

    //чекбокси
    try {
        $(".redCheckBox").iCheck({
            checkboxClass: 'icheckbox_flat-red',
            radioClass: 'iradio_flat-red'
        });
    } catch (e) {
        console.error('error', e.message);
    }


    //клік по кнопках на для відображення фото\відео
    (function () {
        var video = $("#fotoVideo .videoBlock");
        var foto = $("#fotoVideo .imgBlock");

        var btnFoto = $("#fotoVideo #toggleFoto");
        var btnVideo = $("#fotoVideo #toggleVideo");
        var btnAll = $("#fotoVideo #all");


        btnFoto.on("click", function () {
            video.fadeOut();
            foto.fadeIn(300);
            btnFoto.addClass("active").siblings().removeClass("active");
        });

        btnVideo.on("click", function () {
            foto.fadeOut();
            video.fadeIn(300);
            btnVideo.addClass("active").siblings().removeClass("active");
        });

        btnAll.on("click", function () {
            foto.show();
            video.show();
            btnAll.addClass("active").siblings().removeClass("active");

        });
    }());


//    клік на кнопку суппорту
    (function () {
        var btn = $(".supportBtn");
        var support = $(".rightChat .support");
        var suppInput = $(".rightChat .forSupport");


        btn.on("click", function () {
            support.toggleClass("activeSupp");
            suppInput.toggleClass("activeSupp");
        })
    }());


    (function () {
        var btn = '<button class="tglAttrs" ><span class="glyphicon glyphicon-menu-down"></span></button>';
        var form = $("#extra-search-fields-form");

        form.find(".form-group:has(.checkbox,.radio)").each(function (index, element) {
            var el = $(element);

            var b = el.find(".checkbox,.radio").slice(4);
            b.hide();

            if (b.length) {
                el.append(btn);
            }
        });

        form.on("click", ".tglAttrs", function (event) {
            event.preventDefault();
            $(event.target).closest(".form-group").find(".checkbox,.radio").slice(4).toggle(300);
            btn.toggleClass("glyphicon glyphicon-menu-up");

        });

    }());

    //калердар на сторінці створення товару
    $(function () {
        var language = "ru";

        function formatDate(date) {

            var dd = date.getDate();
            if (dd < 10) dd = '0' + dd;

            var mm = date.getMonth() + 1;
            if (mm < 10) mm = '0' + mm;

            var yy = date.getFullYear();

            return dd + '.' + mm + '.' + yy;
        }

        function lang(l) {
            var w = {
                ru: {
                    days: 'дней'
                }
            };

            return w[language][l];
        }

        try {
            $("#dateVisible").dateRangePicker({
                autoClose: false,
                format: 'DD.MM.YYYY',
                separator: '                 ',
                language: language,
                startOfWeek: 'monday',// or monday
                getValue: function () {
                    return $(this).val();
                },

                startDate: false,
                endDate: false,
                time: {
                    enabled: false
                },
                minDays: 0,
                maxDays: 0,
                showShortcuts: false,
                shortcuts: {},
                customShortcuts: [
                    //'prev-days': [1,3,5,7],
                    //'next-days': [3,5,7],
                    //'prev' : ['week','month','year'],
                    //'next' : ['week','month','year']
                ],
                inline: true,
                container: '.datePicker',
                alwaysOpen: true,
                singleDate: true,
                lookBehind: false,
                batchMode: false,
                duration: 200,
                stickyMonths: false,
                dayDivAttrs: [],
                dayTdAttrs: [],
                applyBtnClass: '',
                singleMonth: true,
                hoveringTooltip: function (days, startTime, hoveringTime) {
                    return days > 4 ? days + ' ' + lang('days') : '';
                },
                showTopbar: true,
                swapTime: false,
                selectForward: true,
                selectBackward: false,
                showWeekNumbers: false,
                customArrowPrevSymbol: '<i class="slick2-next" href="#" ><img class="aa"  src="/img/category/prevSlider.svg" > </i>',
                customArrowNextSymbol: '<i class="slick2-next" href="#" ><img class="aa"  src="/img/category/nextSlider.svg" > </i>',
                getWeekNumber: function (date) //date will be the first day of a week
                {
                    return moment(date).format('w');
                }
            });

            // $("#from").on("datepicker-open", function (event, obj) {
            //     var btn = $(".fooorm").find(".btnSend");
            //     var box = $(obj.relatedTarget);
            //
            //     box.css("left", btn.offset().left + btn.outerWidth() - box.width());
            // })
            //     .on("datepicker-first-date-selected", function (event, obj) {
            //         var dt = new Date(obj.date1);
            //         var str = formatDate(dt);
            //
            //
            //         if (!$(this).attr('readonly') && !$(this).is(':disabled') && str != $(this).val()) {
            //             $(this).val(str).trigger("change");
            //         }
            //     });
            //
            //
            // $("#from").on("focus", function (event) {
            //     $(event.target).click();
            // })

        } catch (e) {
            console.error('error', e.message);
        }


    }());

//    підказки для сторінки товару
    $(".sidebarProduct .popPrise").on("hidden.bs.popover", function () {

    });

    //слайдер для попапу перегляду фото на сторінці продукту

    $(".slickItem").slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        centerMode: true,
        asNavFor: '.sliderNav',
        // adaptiveHeight:true,
        // autoplay :true,
        nextArrow: '<button class="slick-prev" href="#" ><img class="aa"  src="/img/category/nextSlider.svg" > </button>',
        prevArrow: '<button class="slick-next" href="#" ><img class="aa" src="/img/category/prevSlider.svg" > </button>',
    });
    $('.sliderNav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.slickItem',
        infinite: true,
        focusOnSelect: true,
        variableWidth: true,
        nextArrow: '<button class="slick-prev" href="#" ><img class="aa"  src="/img/category/nextSlider.svg" > </button>',
        prevArrow: '<button class="slick-next" href="#" ><img class="aa" src="/img/category/prevSlider.svg" > </button>',
    });


//    відкриває\закриває слайдер і меню
    (function () {
        var btn = $(".wrapBody .blockOpenMemu");
        var open = $(".wrapBody .openSubSlider");
        var close = $(".wrapBody .closeSubSlider");
        var window = $(".bottomPathBody");
        var carret = $(".wrapBody .toggleOpen");

        btn.on("click", function () {
            window.toggleClass("hidden-sm hidden-md hidden-lg");
            close.toggleClass("hidden");
            open.fadeToggle();
            carret.toggleClass("active");


        })
    }());


//    клік на кнопку переглянути вільні дати
    (function () {
        var btn = $(".informationProduct .freeDate");
        var input = $(".sidebarProduct #inputWhenRent");

        btn.on("click", function () {
            input.focus();
            $(this).addClass("btn-defolt");
        });

        input.blur(function () {
            btn.removeClass("btn-defolt");
        })
    }());

    //календар в заголовку
    (function () {
        var language = "ru";

        function formatDate(date) {

            var dd = date.getDate();
            if (dd < 10) dd = '0' + dd;

            var mm = date.getMonth() + 1;
            if (mm < 10) mm = '0' + mm;

            var yy = date.getFullYear();

            return dd + '.' + mm + '.' + yy;
        }

        function lang(l) {
            var w = {
                ru: {
                    days: 'дней'
                }
            };

            return w[language][l];
        }

        try {
            $("#inputWhenRent, #inputWhenBack").dateRangePicker({
                autoClose: true,
                format: 'DD.MM.YYYY',
                separator: '',
                language: language,
                startOfWeek: 'monday',// or monday
                getValue: function () {
                    if ($('#inputWhenRent').val() && $('#inputWhenBack').val())
                        return $('#inputWhenRent').val() + ' to ' + $('#inputWhenBack').val();
                    else return '';
                },
                setValue: function (s, s1, s2) {
                    $('#inputWhenRent').val(s1);
                    $('#inputWhenBack').val(s2);
                    console.log('s:', s, 's1:', s1, 's2:', s2);
                },

                startDate: false,
                endDate: false,
                time: {
                    enabled: false
                },
                minDays: 0,
                maxDays: 0,
                showShortcuts: false,
                shortcuts: {
                    //'prev-days': [1,3,5,7],
                    //'next-days': [3,5,7],
                    //'prev' : ['week','month','year'],
                    //'next' : ['week','month','year']
                },
                customShortcuts: [],
                inline: false,
                container: 'body',
                alwaysOpen: false,
                singleDate: false,
                lookBehind: false,
                batchMode: false,
                duration: 200,
                stickyMonths: false,
                dayDivAttrs: [],
                dayTdAttrs: [],
                applyBtnClass: '',
                singleMonth: 'auto',
                hoveringTooltip: function (days, startTime, hoveringTime) {
                    return days > 1 ? days + ' ' + lang('days') : '';
                },
                showTopbar: true,
                swapTime: false,
                selectForward: true,
                selectBackward: false,
                showWeekNumbers: false,
                customArrowPrevSymbol: '<i class="slick2-next" href="#" ><img class="aa"  src="/img/category/prevSlider.svg" > </i>',
                customArrowNextSymbol: '<i class="slick2-next" href="#" ><img class="aa"  src="/img/category/nextSlider.svg" > </i>',
                getWeekNumber: function (date) //date will be the first day of a week
                {
                    return moment(date).format('w');
                }
            });


        } catch (e) {
            console.error('error', e.message);
        }

        $("#inputWhenRent").on("datepicker-open", function (event, obj) {
            var btn = $("#inputWhenRent");
            var box = $(obj.relatedTarget);
            box.css("left", btn.offset().left + btn.outerWidth() - box.width());
        })
            .on("datepicker-first-date-selected", function (event, obj) {
                var dt = new Date(obj.date1);
                var str = formatDate(dt);


                if (!$(this).attr('readonly') && !$(this).is(':disabled') && str != $(this).val()) {
                    $(this).val(str).trigger("change");
                }
            });


        $("#inputWhenRent").on("focus", function (event) {
            $(event.target).click();
        })
    }());


//відкриває\закриває блоки для запиту на бонюваня
    (function () {
        var btnOpenMassaage = $(".rentModal .nextStep");
        var firstBlock = $(".rentModal .topBlock");

        var btnMassageBlock = $(".rentModal .paymentStep");
        var massaggeBlock = $(".rentModal .massage");

        var lastBlock = $(".rentModal .lastBlock");
        var btnSend = $(".rentModal .reqyestPayment");

        //відкриває блок для повідемлення власнику
        btnOpenMassaage.on("click", function () {
            firstBlock.addClass("hidden-xs");
            massaggeBlock.removeClass("hidden-xs");
            $(this).addClass("hidden");
            btnMassageBlock.removeClass("hidden-xs");
        });

        //відкиває блок для вибору оплати
        btnMassageBlock.on("click", function () {
            $(this).addClass("hidden");
            massaggeBlock.addClass("hidden-xs");
            lastBlock.removeClass("hidden");
            btnSend.removeClass("hidden");
        });
    }());



//    сторінка створення товару вкладка фото\відео
    (function () {
        var blockFoto = $(".blockWrapp");
        var  shadow = $("#fotoVideo .shadow");

        blockFoto.on("click", function () {
            // var event = $(ev.target);
            // shadow.addClass("hidden");
            console.log("loatet fotio");

        });

        shadow.on("")
    }());



}(window.jQuery));