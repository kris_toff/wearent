/**
 * Created by Planess group on 01.09.2017.
 */

(function(angular) {
    "use strict";

    var lang = _.trim($("html").attr("lang"));

    // Angular init
    var app = angular.module("pgaPage", [
        "ngAnimate",
        "ngSanitize",
        "ngMessages",
        "ngResource",
        "ngCookies",
        "ngMessageFormat",
        "ngMaterial",
        "ui.bootstrap",
        "ui.mask",
        "ui.validate",
        "ui.uploader",
        "ui.calendar",
        "uiGmapgoogle-maps",
        "720kb.socialshare"
    ]);

    // configs
    app
        .config([
            "$compileProvider",
            "$locationProvider",
            "$httpProvider",
            function($compileProvider, $locationProvider, $httpProvider) {
                $compileProvider.debugInfoEnabled(false);
                $compileProvider.cssClassDirectivesEnabled(false);
                $compileProvider.commentDirectivesEnabled(false);

                $locationProvider.html5Mode({
                    enabled: false,
                    requireBase: true,
                    rewriteLinks: false
                });
                $locationProvider.hashPrefix("!");

                $httpProvider.defaults.xsrfCookieName = yii.getCsrfParam();
                $httpProvider.defaults.xsrfHeaderName = "X-CSRF-TOKEN";
                $httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = yii.getCsrfToken();
            }
        ])
        .config([
            "uiGmapGoogleMapApiProvider",
            "socialshareConfProvider",
            function(uiGmapGoogleMapApiProvider, socialshareConfProvider) {
                uiGmapGoogleMapApiProvider.configure({
                    v: "3.exp",
                    key: "AIzaSyBureh4Q5KfHjIxlbtLqoaDHM1xqDYI-zY",
                    libraries: "places",
                    language: lang
                });

                socialshareConfProvider.configure([
                    {
                        provider: "facebook",
                        conf: {
                            via: "165614500684123"
                        }
                    }
                ]);
            }
        ]);

    moment.locale(lang);
}(window.angular));