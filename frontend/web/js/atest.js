(function(angular) {
    var app = angular.module("pgaPage");
    app.controller("TestController", [
        "$scope", "$parse", function($scope, $parse) {
        $scope.s = function(){
            // $("#tf").attr("target", "_blank").trigger("submit").removeAttr("target");

            console.log($parse('config.min')($scope));
        };

        $scope.config = {
            min: new Date(),
            max: new Date(new Date().getTime() + 5*60*60*1000)
        };
            $scope.$watch("j", function(v){
                if (v){
                    $scope.config = {
                        min: v,
                        max: new Date(v.getTime() + 5*60*60*1000)
                    };
                }

            });
        }
    ]);

}(window.angular));