/**
 *  @author Eugene Terentev <eugene@terentev.net>
 */

(function($, _) {
    "use strict";

    $(document).on("click", "a:not([data-toggle])", function(event) {
        var a = $(event.currentTarget);
        var t = a.attr("href");

        if (_.startsWith(t, "#")) {
            event.preventDefault();

            var l = $(t);
            if (l.length) {
                $("body,html").animate({scrollTop: l.offset().top - 82}, 350);
            }
        }
    });

    // изменение валюты
    $("#switchCurrencyList").on("click", "a", function(event) {
        event.preventDefault();

        var href = $(event.target).attr("href");
        var url = href.substring(0, href.indexOf("?", 0));
        var data = href.substring(href.indexOf("?", 0) + 1);

        $.ajax(url, {
            method: "PUT",
            data: data
        })
            .done(function(response) {
                window.location.reload();
            })
            .fail(function() {

            });
    });
}(window.jQuery, window._));