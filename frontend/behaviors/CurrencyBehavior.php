<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 13.10.2017
 * Time: 20:08
 */

namespace frontend\behaviors;


use common\models\Currency;
use yii\base\Behavior;
use yii\web\Application;
use yii\web\Cookie;

class CurrencyBehavior extends Behavior {

    public function events() {
        return [
            Application::EVENT_BEFORE_REQUEST => [$this, 'beforeRequest'],
        ];
    }

    public function beforeRequest() {
        $currency = null;

        $session = \Yii::$app->session;
        $cookies = \Yii::$app->request->cookies;

        $session->open();
        if (\Yii::$app->user->isGuest) {
            // гость, попытаемся взять валюту из сессии, cookie илипринимаем валюту по-умолчанию
            $cid = $session->get('currencyId');

            if (is_null($cid)) {
                $cid = $cookies->getValue('currencyId');
            }

            $query = Currency::find()
                ->filterWhere(['id' => $cid])
                ->orderBy(['is_payment' => SORT_DESC])
                ->limit(1);

            $currency = $query->one();
        } else {
            // авторизован, берем из настроек или по-умолчанию
            $user     = \Yii::$app->user->identity;
            $currency = $user->userProfile->currency;

            if (is_null($currency)) {
                $query = Currency::find()
                    ->orderBy(['is_payment' => SORT_DESC])
                    ->limit(1);

                $currency = $query->one();
            }
        }

        if (!empty($currency)) {
            \Yii::$app->params['currency'] = $currency;
            \Yii::$app->formatter->currencyCode = $currency->code;

            // добавим валюту в session & cookie
            $session['currencyId'] = $currency->id;
            \Yii::$app->response->cookies->add(new Cookie([
                'name'   => 'currencyId',
                'value'  => $currency->id,
                'expire' => time() + 6 * 30 * 24 * 60 * 60,
            ]));

            if (!\Yii::$app->user->isGuest) {
                $user->userProfile->updateAttributes(['currency_id' => $currency->id]);
            }
        }

        $session->close();
    }
}