<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 23.08.2017
 * Time: 16:04
 */

namespace frontend\controllers;

use frontend\models\rest\Chat;
use frontend\models\rest\ChatMedia;
use frontend\models\rest\ChatMessage;
use yii\base\ErrorException;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\rest\Controller;
use yii\web\UploadedFile;

class ConversationMessageRestDataController extends Controller {
    public function actionIndex($query_ids) {
        $uid = \Yii::$app->user->id;

        // первым делом проверим какие диалоги доступны
        $query    = Chat::find()
            ->select(Chat::tableName() . '.[[id]]')
            ->where('is_deleted = 0 AND (from_id = :uid OR to_id = :uid)',
                ['uid' => $uid])
            ->andWhere(['in', 'id', explode(',', $query_ids)]);
        $chat_ids = $query->column();
//        print $query->createCommand()->rawSql;exit;

        if (empty($chat_ids)) {
            throw new InvalidParamException("Dialogs with id $query_ids not allowed.");
        }

        // запрос на извлечение сообщений указываемого диалога
        $query = ChatMessage::find()
            ->with('author.userProfile')
            ->where(['chat_id' => $chat_ids]);

        // если присутствует метка для полечения только новых сообщений
        if ((int)\Yii::$app->request->get('onlynew', 0)) {
            $inc_ids = explode(',', \Yii::$app->request->get('nyv', ''));
            $inc_ids = array_filter($inc_ids);

            // коррекция запроса, поиск будет только новых сообщений
            $query->andFilterWhere([
                'or',
                ['and', ['or', ['is_delivered' => 0], ['is_viewed' => 0]], ['<>', 'author_id', $uid]],
                [ChatMessage::tableName() . '.[[id]]' => $inc_ids],
            ]);
//            \Yii::$app->response->headers->set('SQL-QUERY', $query->createCommand()->rawSql);
        }

        $ids = $query->select(ChatMessage::tableName() . '.[[id]]')->column();

        // если указан текущий открытый диалог, отметим его сообщения прочитанными
        $active_dialog = \Yii::$app->request->get('active_dialog');
        if ($active_dialog && in_array($active_dialog, $chat_ids)) {
            // указан активный диалог, обновить его сообщения как прочитаны
            ChatMessage::updateAll(['is_viewed' => 1],
                ['and', ['chat_id' => $active_dialog], ['<>', 'author_id', $uid]]);
        }

//        print $query->createCommand()->rawSql;exit;

        return new ActiveDataProvider([
            'query'      => ChatMessage::find()->where([ChatMessage::tableName() . '.[[id]]' => $ids]),
            'pagination' => false,
            'sort'       => false,
        ]);
    }

    public function actionCreate() {
        $model = new ChatMessage();
        $model->load(\Yii::$app->request->post(), '');
        $model->image = UploadedFile::getInstanceByName('image');

        if (!$model->validate()) {
            throw new ErrorException("No validate.");
        } elseif (!$model->save(false)) {
            throw new ErrorException("No save.");
        }

        return $model;
    }
}
