<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.08.2017
 * Time: 15:31
 */

namespace frontend\controllers;

use frontend\models\rest\Chat;
use frontend\models\rest\ChatMessage;
use yii\base\ErrorException;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\rest\Controller;

class ConversationRestDataController extends Controller {
    public function actionIndex() {
        $ct = Chat::tableName();

        $db  = \Yii::$app->db;
        $uid = \Yii::$app->user->id;

        // обновление сообщений со статусом "не получено" на "получено"
        $subquery = $db->createCommand("SELECT [[id]] FROM $ct WHERE ([[from_id]] = :uid OR [[to_id]] = :uid) AND [[is_deleted]] = 0",
            [':uid' => $uid]);

        ChatMessage::updateAll(['is_delivered' => 1], [
            'and',
            ['in', 'chat_id', $subquery->queryColumn()],
            ['<>', 'author_id', $uid],
        ]);

        return new ActiveDataProvider([
            'query'      => $this->getQuery(),
            'pagination' => false,
            'sort'       => false,
        ]);
    }

    protected function getQuery($id = null) {
        $uid = \Yii::$app->user->id;

        $ct  = Chat::tableName();
        $cmt = ChatMessage::tableName();

        // запрос на диалоги
        $query = Chat::find()
            ->select("$ct.*")
            ->addSelect('ms.lastMessage,is_viewed AS isViewed,is_delivered AS isDelivered')
            ->addSelect(['amountNewMessages' => 'amq.am'])
            ->addSelect('{{fvt}}.[[isFavorite]]')
            ->with([
                'interlocutor.userProfile',
                'author.userProfile',
            ])
            ->where(['or', ['from_id' => $uid], ['to_id' => $uid]])
            ->andWhere(['is_deleted' => 0])
            ->andFilterWhere(['id' => $id]);

        // подзапрос для выборки диалога с максимальным значением временной метки
        // будет использован для поиска последнего сообщения в диалоге
        $subquery = ChatMessage::find()
            ->select(['chat_id', 'm' => 'MAX('.ChatMessage::tableName().'.[[id]])'])
            ->groupBy('chat_id');

        // запрос для извлечения последнего сообщения в диалоге
        $query2 = ChatMessage::find()
            ->select(['u.chat_id', 'lastMessage' => 'message', 'is_viewed', 'is_delivered'])
            ->join('LEFT JOIN', ['u' => $subquery],
                "{{u}}.[[chat_id]] = $cmt.[[chat_id]] AND {{u}}.[[m]] = $cmt.[[id]]")
            ->where('[[m]] IS NOT NULL')
            ->groupBy('chat_id');

        // подзапрос для КОЛИЧЕСТВА НОВЫХ сообщений
        $query3 = ChatMessage::find()
            ->select(['chat_id', 'am' => 'COUNT('.ChatMessage::tableName().'.[[id]])'])
            ->where(['is_viewed' => 0])
            ->andWhere(['<>', "$cmt.[[author_id]]", $uid])
            ->groupBy('chat_id');

        // подзапрос для поиска избранных диалогов
        $query4 = (new Query())
            ->select(['fv.chat_id', 'fv.user_id', new Expression('1 AS isFavorite')])
            ->from('{{%chat_favorite}} {{fv}}')
            ->where('{{fv}}.[[user_id]] = :uid', ['uid' => $uid]);

        $query->join('LEFT JOIN', ['ms' => $query2], "{{ms}}.[[chat_id]] = $ct.[[id]]")
            ->join('LEFT JOIN', ['amq' => $query3], "{{amq}}.[[chat_id]] = $ct.[[id]]")
            ->join('LEFT JOIN', ['fvt' => $query4], "$ct.[[id]] = {{fvt}}.[[chat_id]]");

//print $query->createCommand()->rawSql;exit;

        return $query;
    }

    public function actionCreate() {
        return \Yii::$app->request->post();
    }

    public function actionUpdate($id) {
        $model           = Chat::find()->where(['id' => $id])->one();
        $model->scenario = !\Yii::$app->user->isGuest
        && $model->from_id
        === \Yii::$app->user->id ? Chat::SCENARIO_UPDATE_OWN : Chat::SCENARIO_UPDATE;

        if (is_null($model) || !$model->load(\Yii::$app->request->getBodyParams(), '')) {

            throw new ErrorException("No load");
        }
        $model->isFavorite = \Yii::$app->request->getBodyParam('is_favorite');

        if (!$model->validate()) {
            throw new ErrorException("No validate.");
        } elseif (!$model->save()) {
            throw new ErrorException("No saved.");
        }

        return $this->getQuery($id)->one();
    }
}