<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 14.09.2017
 * Time: 17:01
 */

namespace frontend\controllers;


use common\models\UserVideo;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\UploadedFile;

class ProfileVideoRestDataController extends Controller {
    public function actionCreate() {
        $model = new UserVideo();
        $model->load(\Yii::$app->request->post(), '');
        $model->file = UploadedFile::getInstanceByName('file');

        if (!$model->validate()) {
            throw new BadRequestHttpException("Failed validation.");
        } elseif (!$model->save(false)) {
            throw new BadRequestHttpException("No saved.");
        }

        return $model;
    }
}
