<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 11.05.2017
 * Time: 15:19
 */

namespace frontend\controllers;

use common\models\User;
use frontend\models\components\AuthHandler;
use frontend\modules\user\models\LoginForm;
use frontend\modules\user\models\PasswordResetRequestForm;
use frontend\modules\user\models\SignupForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;

class AuthController extends Controller {
    public function actions() {
        return [
            'social' => [
                'class'           => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
                'successUrl'      => \Yii::$app->request->getReferrer() ?: Url::to(['/profile']),
            ],
        ];
    }

    public function actionRegister() {
        $model = new SignupForm();

        $request = \Yii::$app->request;
        if ($request->isPost && $model->load($request->post()) && $model->signup()) {
            return $this->render('register-ok');
        }

        $container = $request->headers->get('X-PJAX-Container', '');

        return $this->render('register-form', [
            'model' => $model,
            'v'     => ArrayHelper::getValue(explode('-', $container), 3, ''),
        ]);
    }

    public function actionAuth() {
        $model = new LoginForm();

        $request = \Yii::$app->request;
        if ($request->isPost && $model->load($request->post()) && $model->login()) {
            $current_page = \Yii::$app->request->post('return_url', '/profile');
//            print $current_page;
//            print_r( \Yii::$app->request->getBodyParams());exit;
            if ($current_page == \Yii::$app->defaultRoute) {
                $current_page = '/';
            }

            return $this->redirect('/' . ltrim($current_page, '/'));
        } elseif (!empty($model->errors) && !is_null($model->getUser())
            && $model->getUser()->status
            == User::STATUS_RESTRICTED
        ) {
            return $this->render('auth-restricted', [
                'time_to' => $model->getUser()->restricted_to,
            ]);
        }

        $container = $request->headers->get('X-PJAX-Container', '');
        return $this->render('auth-form', [
            'model' => $model,
            'v'     => ArrayHelper::getValue(explode('-', $container), 3, ''),
        ]);
    }

    public function actionRestore() {
        $model = new PasswordResetRequestForm();

        $request = \Yii::$app->request;
        if ($request->isPost && $model->load($request->post()) && $model->validate() && $model->sendEmail()) {
            return $this->render('restore-pass-sent');
        }

        return $this->render('restore-pass-form', [
            'model' => $model,
        ]);
    }

    public function actionResendRestore() {
        return $this->render('restore-pass-sent', [
            'text' => 'Письмо отправлено.',
        ]);
    }

    public function actionLogout() {
        \Yii::$app->user->logout();

        return $this->goBack(\Yii::$app->request->referrer);
    }


    public function actionTest() {
        return $this->render('ok');
    }

    public function onAuthSuccess($client) {
        (new AuthHandler($client))->handle();
    }
}