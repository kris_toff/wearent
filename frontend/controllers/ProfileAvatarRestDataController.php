<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 10.09.2017
 * Time: 12:23
 */

namespace frontend\controllers;


use common\models\UserAvatar;
use yii\helpers\FileHelper;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\UploadedFile;

class ProfileAvatarRestDataController extends Controller {

    public function actionIndex() {
        return UserAvatar::findAll(['is_deleted' => 0, 'user_id' => \Yii::$app->user->id]);
    }

    public function actionCreate() {
        $model       = new UserAvatar();
        $model->file = UploadedFile::getInstanceByName('webcam');

        if (!$model->validate()) {
            throw new BadRequestHttpException("File failed validation.");
        } elseif (!$model->save(false)) {
            throw new BadRequestHttpException("Failed save file.");
        }

        return $model;
    }

    public function actionDelete($id) {
        $model = UserAvatar::findOne(['id' => $id, 'user_id' => \Yii::$app->user->id]);
        if (is_null($model)) {
            throw new BadRequestHttpException("Not found avatar with id $id");
        }
//        elseif ((int)$model->is_main) {
//            throw new BadRequestHttpException("Can not be removed main photo.");
//        }

        $model->softDelete();
    }

    public function actionUpdate($id) {
        $model = UserAvatar::findOne(['id' => $id, 'user_id' => \Yii::$app->user->id]);
        if (is_null($model)) {
            throw new BadRequestHttpException("Not found avatar with id $id");
        }

        UserAvatar::updateAll(['is_main' => 0], ['user_id' => \Yii::$app->user->id]);

        $model->updateAttributes(['is_main' => 1]);

        return $model;
    }
}