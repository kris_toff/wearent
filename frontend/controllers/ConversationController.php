<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.08.2017
 * Time: 9:30
 */

namespace frontend\controllers;


use common\models\Chat;
use common\models\ChatMessage;
use common\models\Product;
use common\models\User;
use common\models\UserProfile;
use frontend\models\ChatConversationForm;
use GuzzleHttp\Exception\BadResponseException;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class ConversationController extends Controller {
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'verbs' => ['get', 'post'], 'roles' => ['@']],
                ],
            ],
        ];
    }

    public function actionTemplate($p) {
        return $this->renderPartial("template/$p");
    }
}