<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.05.2017
 * Time: 12:47
 */

namespace frontend\controllers;


use backend\models\CategoryAlias;
use common\models\Product;
use frontend\models\ProcessCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yiimodules\categories\models\Categories;

class CategoryController extends Controller {
    public function actionIndex() {
        $query = (new Query())
            ->from(Categories::tableName())
            ->where([Categories::tableName() . '.[[is_active]]' => 1])
            ->orderBy(['position' => SORT_ASC])
            ->indexBy('id');
//        print '<pre>';print_r($query->all());exit;
        $result = $query->all();

//print '<pre>'; print_r($c);exit;
        return $this->render('index', [
            'cgr'  => ProcessCategory::menuTree($result),
            'list' => ArrayHelper::map($result, 'slug', 'name'),
        ]);
    }

    public function actionSearch($fields = '') {
        $fields = trim($fields);

        if (!mb_strlen($fields, 'UTF-8')) {
            return $this->redirect(['/category']);
        }

        // общий экземпляр для запросов
        $query = Categories::find()
            ->where(['is_active' => 1])
            ->asArray()
            ->indexBy('id');
//        print $query->createCommand()->sql;exit;

        // все совпадения по категориям третьего уровня по слову
        $ql = clone $query;
        $ql->joinWith('aliases', false)
            ->andWhere([
                'or',
                ['like', Categories::tableName() . '.[[name]]', $fields],
                ['like', CategoryAlias::tableName() . '.[[name]]', $fields],
            ])
            ->andWhere([
                'not in',
                Categories::tableName() . '.[[id]]',
                Categories::find()->select('parent_id')->distinct()->where('[[parent_id]] IS NOT NULL'),
            ]);
        $result = $ql->all();
        $p      = $result;
//print '<pre>';print_r($result);exit;

        do {
            $pi     = ArrayHelper::map($p, 'id', 'parent_id');
            $ne     = array_diff($pi, [0, null], array_keys($pi));
            $ql     = clone $query;
            $p      = $ql->andWhere(['id' => array_values($ne)])->all();
            $result += $p;
        } while (count($p));

//print '<pre>';print_r($result);exit;
        return $this->render('repel', [
            'list' => ProcessCategory::menuTree($result, false),
            'word' => $fields,
        ]);
    }
}