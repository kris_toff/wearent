<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 16.04.2017
 * Time: 0:16
 */

namespace frontend\controllers;

use common\models\Currency;
use common\models\Product;
use common\models\ProductAmazing;
use common\models\ProductMedia;
use dosamigos\google\places\Search;
use frontend\models\ProcessCategory;
use frontend\models\ProductCurrency;
use frontend\models\ProductForm;
use frontend\models\ProductMediaHelper;
use frontend\models\ProductPreviewAR;
use Imagine\Image\Point;
use yii\base\ErrorException;
use yii\base\InvalidParamException;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\IntegrityException;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yiimodules\categories\models\Categories;
use yii\imagine\Image;

class ProductController extends Controller {
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['change-favourite', 'change-amazing'],
                'rules' => [
                    ['allow' => true, 'roles' => ['@'], 'actions' => ['change-favourite', 'change-amazing']],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'make-default'     => ['patch'],
                    'rotate'           => ['patch'],
                    'crop'             => ['patch'],
                    'change-favourite' => ['put'],
                    'change-amazing'   => ['put'],
                    '*'                => ['get', 'post', 'head', 'put'],
                ],
            ],
        ];
    }

    /**
     * Действие отображает страницу с товаром
     *
     * @param string $slug уникальное название товара, используется как часть url для его отображения
     *
     * @return string
     * @throws NotFoundHttpException
     * @throws ErrorException
     */
    public function actionView($slug) {
        $query = Product::find();
//print 'action|language:'.\Yii::$app->language.';locale:'.\Yii::$app->formatter->locale;exit;
//        if (is_numeric($slug)) {
//            $query->andWhere(['id' => $slug]);
//        } else
        if (is_string($slug)) {
            $query->andWhere(['slug' => $slug]);
        } else {
            $query->andWhere(new Expression('0'));
        }
        $query->andWhere(['is_deleted' => 0])
            ->with([
                'currency',
                'media',
                'author.userProfile.avatars' => function($query) {
                    $query->orderBy(['is_main' => SORT_DESC, 'created_at' => SORT_ASC]);
                },
            ])
            ->limit(1);

        /** @var Product $model */
        $model = $query->one();

        if (is_null($model)
            || ($model->status != Product::OPEN)
            && (\Yii::$app->user->isGuest
                || $model->creator_id
                != \Yii::$app->user->id)
        ) {
            throw new NotFoundHttpException('Product not found.');
        }

//  avatar
        $avatars = $model->author->userProfile->avatars;
        reset($avatars);
        $avatar = ArrayHelper::getValue($avatars, [0, 'path'], '/img/product/fotoFace.jpg');

// цены на товар
        $pledge = [
            'default' => ProductCurrency::getActualPrice($model->pledge_price,
                $model->currency_id),
            'good'    => ProductCurrency::getActualPrice(max(0,
                $model->pledge_price - round($model->pledge_price / 100 * $model->discount_pledge, 2)),
                $model->currency_id),
        ];

        // похожие товары
        $relatedProducts = $model->relatedProducts(7, true);

        return $this->render('view', [
            'model'        => $model,
            'owner_avatar' => $avatar,
            'media'        => $model->media,
            'prices'       => ['pledge' => $pledge],
            'goodUser'     => !\Yii::$app->user->isGuest && \Yii::$app->user->identity->getVerified(),
            'related'      => $relatedProducts,
        ]);
    }

    /**
     * Действие отображает страницу создания или редактировани товара
     *
     * @param null $link
     *
     * @return array|string
     * @throws BadRequestHttpException
     */
    public function actionCreateNew($link = null) {
        $request = \Yii::$app->request;
        $model   = new ProductForm();

        if ($request->isAjax) {
            // сохранение токара как черновика
            \Yii::$app->response->format = Response::FORMAT_JSON;

            $model->loadProduct(ArrayHelper::getValue($request->post(), [$model->formName(), 'id']));

            $model->scenario = ProductForm::SCENARIO_DRAFT;
            $model->load($request->post());

            if (!$model->validate()) {
                \Yii::$app->response->statusCode = 400;

                return [
                    'status' => 'invalid',
                    'errors' => $model->errors,
                ];
            }
            if (!$model->save(false)) {
                throw new BadRequestHttpException("not saved");
            }

            return [
                'status'  => 'success',
                'product' => ArrayHelper::toArray($model->product, [
                    Product::className() => [
                        'slug',
                    ],
                ]),
            ];
        } elseif ($request->isGet) {
            if (!$model->loadProduct($link)) {
                \Yii::$app->session->setFlash('load-product', 'error');

                return $this->goBack();
            }
//            print_r($model);exit;
            if (empty($link)) {
                return $this->redirect(['product/create-new', 'link' => $model->product->slug], 307);
            }
        } elseif ($request->isPost) {
            // либо сохранение товара, либо его создание
            $post = $request->post();
            $model->loadProduct(ArrayHelper::getValue($post, [$model->formName(), 'id']));

            $currentCategory = $model->category;

            if (!$model->load($post)) {
                // возвращаемся откуда пришли
                return $this->refresh();
            }
//print $currentCategory . ' ' . $model->category;exit;
            if ($model->id) {
                if ($currentCategory != $model->category) {
                    // поменять категорию
                    $model->scenario = ProductForm::SCENARIO_UPDATE_CATEGORY;

                    $model->save();

                    return $this->refresh();
                } else {
                    // сохраняется товар
                    $model->scenario = ProductForm::SCENARIO_DEFAULT;

                    if (!$model->validate()) {
                        print_r($model->errors);
                        exit;
                    } elseif (!$model->save(false)) {
                        \Yii::$app->session->setFlash('result-saved', false);

//                        return $this->refresh();
                    } else{
                        return $this->redirect(['/profile/my-products'], 307);
                    }
                }
            } else {
                // создание нового товара
                $model->scenario = ProductForm::SCENARIO_CREATE;

                if (!$model->save()) {
                    \Yii::$app->session->setFlash('result-saved', false);

                    return $this->refresh();
                }
            }

//            $product = Product::find()->where(['id' => $model->id, 'is_draft' => 0])->one();
//            if (!is_null($product)) {
            // переход либо на страницу товара либо в список собственных товаров
//                return $this->redirect('/', 307);
//            }

//            return $this->refresh();
        }

        $backUrl = $request->referrer;
        $media   = [
            'preview' => [],
            'config'  => [],
            'extra'   => [],
        ];
        foreach ($model->getMedia()->each(10) as $item) {
            $file = \Yii::getAlias('@webroot') . '/' . ltrim($item['link'], '/');

            if (!file_exists($file)) {
                continue;
            }

            switch (pathinfo($file, PATHINFO_EXTENSION)) {
            case 'png':
            case 'jpg':
            case 'jpeg':
                $ftype = 'image';
                break;
            case 'mp4':
                $ftype = 'video';
                break;
            default:
                $ftype = 'other';
            }

            $media['preview'][] = $item['link'];
            $media['config'][]  = [
                'key'      => $item['id'],
                'type'     => $ftype,
                'filetype' => mime_content_type($file),
            ];
            $media['extra'][]   = [
                'description' => $item['description'],
                'size'        => filesize($file),
            ];
        }

// список валют для выбора
        $currencies = Currency::findAll(['is_payment' => 1]);

        // создание breadcrumbs с категориями товара
        $ctg = Categories::find()->where(['is_active' => 1])->orderBy(['position' => SORT_ASC])->indexBy('id')->all();

        $cl     = [$model->product->category->name];
        $parent = $model->product->category->parentCategory;
        while (!is_null($parent)) {
            array_unshift($cl, $parent->name);
            $parent = $parent->parentCategory;
        }

        // подсказки для полей с названием и описанием товара
        $placeholders = (new Query())->from('{{%product_create_placeholders}}')
            ->where(['category_id' => $model->product->category_id])
            ->one();

//        $model->currency = 2;

        if (empty($model->timeZone)) {
            $geo = \Yii::$app->geoip->ip();
            $df  = new \DateTimeZone($geo->timeZone);

            $timezone        = array_search($df->getOffset(new \DateTime()), $model->timezonelist);
            $model->timeZone = ($timezone !== false) ? $timezone : '';
        }

        if (empty($model->address_lat) || empty($model->address_lng)) {
            $geo = \Yii::$app->geoip->ip();

            $model->address_lat = $geo->location->lat;
            $model->address_lng = $geo->location->lng;
        }

//print_r($currencies);exit;
        // на текущий момент переменная $model должна содержать актуальный товар
        return $this->render('productForm', [
            'model'        => $model,
            'backUrl'      => $backUrl,
            'media'        => $media,
            'currencies'   => $currencies,
            'categories'   => $ctg,
            'ctgList'      => $cl,
            'placeholders' => $placeholders,
        ]);
    }

    /**
     * Действие загружает изображения или видео для товара, при редактировании на вкладке 2
     *
     * @return array пустой массив для избежания ошибки ответа
     * @throws BadRequestHttpException
     * @throws ErrorException
     */
    public function actionLoadMedia() {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $request = \Yii::$app->request;

        $model = new ProductForm(['scenario' => ProductForm::SCENARIO_MEDIA]);
        if (!$model->load($request->post())) {
            throw new BadRequestHttpException("Failed load");
        }
        $model->media = UploadedFile::getInstance($model, 'media');

        if (!$model->validate()) {
            \Yii::$app->response->statusCode = 400;
            \Yii::$app->response->data       = $model->errors;
            \Yii::$app->response->send();

            return [];
        } elseif (false === ($result = $model->save(false))) {
            throw new ErrorException('Failed saved');
        }

        $files = ProductMediaHelper::separateFiles($model->id);

        return [
            'status' => 'ok',
            'count'  => count($files['images']) + count($files['videos']),
            'size'   => ProductMediaHelper::getSize($model->id),
            'text'   => [
                'i' => \Yii::t('product', '{c, plural, =0{} =1{# image} other{# images}}',
                    ['c' => count($files['images'])]),
                'v' => \Yii::t('product', '{c, plural, =0{} =1{# video} other{# videos}}',
                    ['c' => count($files['videos'])]),
                'a' => \Yii::t('product', '{c, number} {c, plural, =1{file} other{files}}',
                    ['c' => count($files['images']) + count($files['videos'])]),
            ],
            'html'   => strtr($this->renderPartial('create/media-options/preview-templates/' . $model->mediatype), [
                '{frameClass}' => '',
                '{previewId}'  => $result['id'],
                '{fileindex}'  => '',
                '{template}'   => '',
                '{data}'       => $result['link'],
                '{caption}'    => '',
                '{footer}'     => $this->renderPartial('create/media-options/layout-templates/footer-'
                    . $model->mediatype, [
                    'v' => '',
                ]),
                '{type}'       => 'video/mp4',
                '{zoomIcon}'   => '<i class="glyphicon glyphicon-zoom-in"></i>',
            ]),
        ];
    }

    /**
     * Действие удаляет изображение или видео на вкладке 2 редактирования товара
     *
     * @return array пустой массим для избежания ошибки ответа
     */
    public function actionDeleteMedia() {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $request = \Yii::$app->request;
        $id      = $request->post('key');
        $pid     = $request->post('pid');

        $model = ProductMedia::findOne(['product_id' => $pid, 'id' => $id, 'is_deleted' => 0]);
        if (!is_null($model)) {
            $model->moveLast();
            $model->softDelete();
        }

        $files = ProductMediaHelper::separateFiles($pid);

        return [
            'count' => count($files['images']) + count($files['videos']),
            'size'  => ProductMediaHelper::getSize($pid),
            'text'  => [
                'i' => \Yii::t('product', '{c, plural, =0{} =1{# image} other{# images}}',
                    ['c' => count($files['images'])]),
                'v' => \Yii::t('product', '{c, plural, =0{} =1{# video} other{# videos}}',
                    ['c' => count($files['videos'])]),
                'a' => \Yii::t('product', '{c, number} {c, plural, =1{file} other{files}}',
                    ['c' => count($files['images']) + count($files['videos'])]),
            ],
        ];
    }

    /**
     * Действие меняет подпись под изображением или видео при редактировании товара
     * Без ответа, результат не ожидается
     */
    public function actionSetDefine() {
        $request = \Yii::$app->request;

        $model = ProductMedia::findOne(['id' => $request->post('key'), 'is_deleted' => 0]);
        if (!is_null($model)) {
            $model->description = $request->post('define');
            $model->save();
        }
    }

    public function actionRemoveProduct() {
        return $this->goBack();
    }

    /**
     * Действие перемещает изображения из вкладки 2 при создании товара на новую позицию влево или вправо
     *
     * @return array пустой массив для избежания ошибки ответа
     */
    public function actionRotate() {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $request                     = \Yii::$app->request;

        $id         = $request->getBodyParam('id');
        $product_id = $request->getBodyParam('pid');

        $model = ProductMedia::findOne(['id' => $id, 'product_id' => $product_id, 'is_deleted' => 0]);
        if (!is_null($model)) {
            $image = Image::getImagine();

            $filepath = \Yii::getAlias('@webroot') . '/' . ltrim($model->link, '/');
            $wpath    = "/uploads/products/id{$product_id}/mpt/" . basename($filepath);
            $np       = \Yii::getAlias('@webroot') . $wpath;

            FileHelper::createDirectory(\Yii::getAlias('@webroot') . "/uploads/products/id{$product_id}/mpt");
            $image->open(\Yii::getAlias('@webroot') . '/' . ltrim($model->link, '/'))
                ->rotate($request->getBodyParam('angle'))->save($np);

            $model->updateAttributes(['link' => $wpath]);

            return [
                'src' => $wpath,
            ];
        }

        return [];
    }

    public function actionCrop() {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $request                     = \Yii::$app->request;

        $id         = $request->getBodyParam('id');
        $product_id = $request->getBodyParam('pid');

        $model = ProductMedia::findOne(['id' => $id, 'product_id' => $product_id, 'is_deleted' => 0]);
        if (!is_null($model)) {
            $filepath = \Yii::getAlias('@webroot') . '/' . ltrim($model->link, '/');
            $filename = basename($filepath);
            $wpath    = "/uploads/products/id{$product_id}/mpt/" . $filename;
            $np       = \Yii::getAlias('@webroot') . '/' . ltrim($wpath, '/');

            FileHelper::createDirectory(\Yii::getAlias('@webroot') . "/uploads/products/id{$product_id}/mpt");

            $sp    = [$request->getBodyParam('x'), $request->getBodyParam('y')];
            $image = Image::crop($filepath, $request->getBodyParam('width'), $request->getBodyParam('height'), $sp);
            $image->save($np);

            $model->updateAttributes(['link' => $wpath]);

            return [
                'src' => $wpath,
            ];
        }

        return [];
    }

    /**
     * Действие устанавливает метку изображения или видео по-умолчанию для товара
     *
     * @return array пустой массив для избежания ошибки ответа
     *
     * @throws BadRequestHttpException
     */
    public function actionMakeDefault() {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $request                     = \Yii::$app->request;

        $model = Product::find()->where([
            'is_deleted' => 0,
            'id'         => $request->post('pid'),
            'creator_id' => \Yii::$app->user->id,
        ])->one();

        if (!is_null($model)) {
            $model->scenario      = Product::SCENARIO_MEDIA;
            $model->media_default = $request->post('id');

            return ['status' => $model->update() ? 'success' : 'not_update'];
        }

        throw new BadRequestHttpException("Not found product with id {$request->post('pid')}");
    }

    /**
     * Метод возвращает перевод фразы с ценой товара со скидкой и название валюты склоняет в зависимости от количества
     * единиц. Работает только через ajax.
     *
     * @return array
     */
    public function actionPluralCurrency() {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $price    = max(0, \Yii::$app->request->getBodyParam('p'));
        $discount = min(100, max(\Yii::$app->request->getBodyParam('d'), 0));
        $currency = Currency::findOne(['id' => \Yii::$app->request->getBodyParam('c')]);
        if (is_null($currency)) {
            return [
                'number' => 0,
                'text'   => '0',
            ];
        }
        $currency_code = mb_strtoupper($currency->code, 'UTF-8');

        $result = $price - $price / 100 * $discount;

        return [
            'number' => \Yii::$app->formatter->asDecimal($result, 2),
            'text'   => \Yii::t('UI10.4.GREEN_PRICE', "\$GREEN_DICOUNT_PRICE_{$currency_code}$", [
                'total' => $result,
            ]),
            'lang'   => \Yii::$app->language,
        ];
    }

    public function actionPreview() {
        $request = \Yii::$app->request;
//        print 'good'.$slug;print_r($request->post());exit;

        $model = new ProductPreviewAR();

        if (!$model->loadProduct($request->post())) {
            print 'load failed';
        } else
            if (!$model->save()) {
                print 'save failed<pre>';
                print_r($request->post());
                print_r($model->errors);
                print_r($model);
            } else {
//                print_r($request->post());print_r($model['id']);exit;
//                print 'p:';print_r($model->pledge_price);exit;
                $avatars = $model->author->userProfile->avatars;
                reset($avatars);
                $avatar = ArrayHelper::getValue($avatars, [0, 'path'], '/img/product/fotoFace.jpg');

                return $this->render('view', [
                    'model'        => $model,
                    'owner_avatar' => $avatar,
                    'media'        => $model->getMedia()->all(),
                    'prices'       => [
                        'pledge' => [
                            'default' => ProductCurrency::getActualPrice($model->pledge_price,
                                is_numeric($model->currency_id) ? (integer)$model->currency_id : \Yii::$app->params['currency']),
                            'good'    => ProductCurrency::getActualPrice(max(0,
                                $model->pledge_price - round($model->pledge_price / 100 * $model->discount_pledge, 2)),
                                is_numeric($model->currency_id) ? (integer)$model->currency_id : \Yii::$app->params['currency']),
                        ],
                    ],
                    'goodUser'     => !\Yii::$app->user->isGuest && \Yii::$app->user->identity->getVerified(),
                ]);
                print 'ok';
                print_r($model);
            }

    }

    public function actionChangeFavourite() {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $pid = \Yii::$app->request->getBodyParam('product_id');
        if ((new Query())->from('{{%product_favorite}}')->where([
            'product_id' => $pid,
            'user_id'    => \Yii::$app->user->id,
        ])->exists()) {
            // надо удалить
            \Yii::$app->db->createCommand()
                ->delete('{{%product_favorite}}', ['product_id' => $pid, 'user_id' => \Yii::$app->user->id])
                ->execute();

            return [
                'result' => 0,
            ];
        } else {
            // надо добавить
            \Yii::$app->db->createCommand()
                ->insert('{{%product_favorite}}', ['product_id' => $pid, 'user_id' => \Yii::$app->user->id])
                ->execute();

            return ['result' => 1];
        }
    }

    public function actionChangeAmazing() {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $pid   = \Yii::$app->request->getBodyParam('product_id');
        $model = ProductAmazing::findOne(['product_id' => $pid, 'rater_id' => \Yii::$app->user->id]);
        if (is_null($model)) {
// добавить
            $model             = new ProductAmazing();
            $model->product_id = $pid;

            return ['result' => (int)$model->save()];
        } else {
            // удалить
            return ['result' => (int)!$model->delete()];
        }
    }
}
