<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 31.10.2017
 * Time: 16:43
 */

namespace frontend\controllers;


use yii\web\Controller;

class DeveloperModeController extends Controller {

    public function actionPageSuspend(){
        return $this->renderPartial('page-suspend');
    }

}