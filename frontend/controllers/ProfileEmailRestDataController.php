<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.09.2017
 * Time: 19:52
 */

namespace frontend\controllers;


use common\models\User;
use common\models\UserEmail;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\NotAcceptableHttpException;

class ProfileEmailRestDataController extends Controller {
    public function actionIndex() {
        $query = UserEmail::find();

        $query->select(new Expression('[[id]],[[email]],[[is_verify]],0 AS [[is_main]]'))->where([
            'user_id'    => \Yii::$app->user->id,
            'is_deleted' => 0,
        ]);
        if (!is_null(\Yii::$app->user->identity) && !empty(\Yii::$app->user->identity->email)){
            $query->union(User::find()
                ->select(new Expression('-1 AS [[id]],[[email]],1 AS [[is_verify]],1 AS [[is_main]]'))
                ->where(['id' => \Yii::$app->user->id]));
        }

//print $query->createCommand()->rawSql;exit;
        return $query->asArray()->all();

        return new ArrayDataProvider([
            'query'      => $query,
            'pagination' => false,
            'sort'       => false,
        ]);

        return \Yii::$app->user->identity->userProfile->emails;
    }

    public function actionUpdate($id, $login = false) {
        $model = UserEmail::findOne(['id' => $id, 'user_id' => \Yii::$app->user->id]);
        if (is_null($model)) {
            throw new BadRequestHttpException("Not found email with id $id");
        }

        if ($login) {
            $email = $model->user->email;
            if (empty($email) || empty($model->user->password_hash)) {
                // ранее не создавался пароль и регистрация проводилась через соцсети
                throw new NotAcceptableHttpException("No registration.");
            } else {
                $model->user->updateAttributes(['email' => $model->email]);
                $model->updateAttributes(['email' => $email]);

                $model = User::find()
                    ->select(new Expression($model->id . ' AS [[id]],[[email]],1 AS [[is_verify]],1 AS [[is_main]]'))
                    ->where(['id' => \Yii::$app->user->id])->one();
            }
        } else {
            if (!$model->load(\Yii::$app->request->getBodyParams(), '')) {
                throw new BadRequestHttpException("Invalid data.");
            } elseif (!$model->validate()) {
                \Yii::$app->response->statusCode = 400;
                \Yii::$app->response->data       = $model->errors;
                \Yii::$app->response->send();
            } elseif (!$model->save(false)) {
                throw new BadRequestHttpException("Save failed.");
            }
        }

        return $model;
    }

    public function actionCreate() {
        $model = new UserEmail();

        if (!$model->load(\Yii::$app->request->post(), '')) {
            throw new BadRequestHttpException("Invalid data.");
        } elseif (!$model->validate()) {
            \Yii::$app->response->statusCode = 400;
            \Yii::$app->response->data       = $model->errors;
            \Yii::$app->response->send();
        } elseif (!$model->save(false)) {
            throw new BadRequestHttpException("Save failed.");
        }

        return $model;
    }

    public function actionDelete($id) {
        $model = UserEmail::findOne([
            'user_id' => \Yii::$app->user->id,
            'id'      => $id,
        ]);
        if (is_null($model)) {
            throw new BadRequestHttpException("Nothing found with id $id");
        }

        if ((int)$model->is_verify) {
            $model->softDelete();
        } else {
            $model->delete();
        }

        return null;
    }
}