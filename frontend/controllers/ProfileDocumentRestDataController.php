<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.10.2017
 * Time: 15:13
 */

namespace frontend\controllers;


use frontend\models\UserDocument;
use yii\base\ErrorException;
use yii\data\ActiveDataProvider;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\UploadedFile;

class ProfileDocumentRestDataController extends Controller {

    public function actionIndex() {
        $query = UserDocument::find()->where(['user_id' => \Yii::$app->user->id, 'is_deleted' => 0]);

        return new ActiveDataProvider([
            'query'      => $query,
            'pagination' => false,
            'sort'       => false,
        ]);
    }

    public function actionDelete($id) {
        $model = UserDocument::findOne(['user_id' => \Yii::$app->user->id, 'id' => $id]);
        if (is_null($model)) {
            throw new BadRequestHttpException("No find.");
        }

        if (is_null($model->status)) {
            $model->softDelete();
        } else {
            throw new BadRequestHttpException("Document can not remove.");
        }
    }

    public function actionCreate() {
        $model = new UserDocument();

        $model->file = UploadedFile::getInstanceByName('file');
        if (!$model->load(\Yii::$app->request->post(), '')) {
            throw new BadRequestHttpException("Wrong data.");
        } elseif (!$model->validate()) {
            \Yii::$app->response->statusCode = 400;
            \Yii::$app->response->data       = $model->errors;
            \Yii::$app->response->send();
        } elseif (!$model->save(false)) {
            throw new BadRequestHttpException("No saved");
        }

        return $model;
    }
}
