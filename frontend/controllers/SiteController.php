<?php

namespace frontend\controllers;

use common\models\Product;
use common\models\ProductMedia;
use frontend\wizards\ProductKits;
use Yii;
use frontend\models\ContactForm;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yiimodules\categories\models\Categories;

/**
 * Site controller
 */
class SiteController extends Controller {
    public $layout = 'typicalFrontend';

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error'      => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha'    => [
                'class'           => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'set-locale' => [
                'class'   => 'common\actions\SetLocaleAction',
                'locales' => \Yii::$app->languages->list,
            ],
        ];
    }

    public function actionIndex() {
        $categories = Categories::find()->where(['is_active' => 1])->andWhere([
            'or',
            ['parent_id' => 0],
            ['parent_id' => null],
        ])->orderBy(['position' => SORT_ASC])->all();

        $toArray = [
            Product::className() => [
                'minislide'     => function($model) {
                    return ArrayHelper::toArray($model->getMedia()->andWhere([
                        'or like',
                        'link',
                        ['%.png', '%.jpg', '%.gif'],
                        false,
                    ])->all(), [
                        ProductMedia::className() => [
                            'src' => 'link',
                        ],
                    ]);
                },
                'product_name'  => 'name',
                'slug',
                'category'      => function($model) {
                    return $model->category->name;
                },
                'price',
                'currency'      => function($model) {
                    return $model->currency->code;
                },
                'min_period',
                'step'          => function($model) {
                    return $model->leasing;
                },
                'countComments' => function() {
                    return 5;
                },
            ],
        ];

        $team_selection = ArrayHelper::toArray(Product::getTeamSelection(), $toArray);
        $viewed         = ArrayHelper::toArray(Product::getLastViewed(), $toArray);
        $sliderLine1    = ArrayHelper::toArray(ProductKits::getListForSlider(24), $toArray);
        $sliderLine2    = ArrayHelper::toArray(ProductKits::getListForSlider(8), $toArray);
        $sliderAmazing  = ArrayHelper::toArray(ProductKits::getListForSliderAmazing(10), $toArray);
//print '<pre>';print_r($sliderLine1);exit;
        $query  = (new Query())
            ->from(Categories::tableName())
            ->where([Categories::tableName() . '.[[is_active]]' => 1])
            ->orderBy(['position' => SORT_ASC])
            ->indexBy('id');
        $result = $query->all();

        return $this->render('index', [
            'categories'      => $categories,
            'viewed_products' => $viewed,
            'team_selection'  => $team_selection,
            'sliderLine1'     => $sliderLine1,
            'sliderLine2'     => $sliderLine2,
            'sliderAmazing'   => $sliderAmazing,
            'product_count'   => Product::find()
                ->where(['is_deleted' => 0])
                ->joinWith('category')
                ->andWhere(['is_active' => 1])
                ->count('{{%product}}.[[id]]'),
            'list'            => ArrayHelper::map($result, 'slug', 'name'),
        ]);
    }

    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->contact(Yii::$app->params['adminEmail'])) {
                Yii::$app->getSession()->setFlash('alert', [
                    'body'    => Yii::t('frontend',
                        'Thank you for contacting us. We will respond to you as soon as possible.'),
                    'options' => ['class' => 'alert-success'],
                ]);

                return $this->refresh();
            } else {
                Yii::$app->getSession()->setFlash('alert', [
                    'body'    => \Yii::t('frontend', 'There was an error sending email.'),
                    'options' => ['class' => 'alert-danger'],
                ]);
            }
        }

        return $this->render('contact', [
            'model' => $model,
        ]);
    }
}
