<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 15.05.2017
 * Time: 11:08
 */

namespace frontend\controllers;


use backend\models\CategoryAlias;
use common\models\Product;
use frontend\models\HistorySearch;
use frontend\models\ProductCurrency;
use frontend\models\ProductSearch;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;
use yiimodules\categories\models\Categories;

class SearchController extends Controller {
    public function actionIndex() {
        $request = \Yii::$app->request;

        $model = new ProductSearch();
        $model->load($request->get());
//        print 'n';print_r($model->EavModel);exit;
        $data = $model->analyze();

        if ($request->isAjax) {
            $data->prepare(true);
            $pagination = $data->pagination;
//            print_r($model->catalog);exit;
            $headers = \Yii::$app->response->headers;
            $headers->set('X-Pagination-Total-Count', $data->totalCount);
            $headers->set('X-Pagination-Current-Page', $pagination->page + 1);
            $headers->set('X-Pagination-Per-Page', $pagination->pageSize);
            $headers->set('X-Pagination-Page-Count', $pagination->pageCount);

            return $this->renderPartial('product-list', [
                'products' => $data,
            ]);
        }
//print_r($data->getModels());print $data->count;exit;
        if (!$data->count) {
//            print_r($model);exit;
            return $this->render('no-result', ['searchForm' => $model, 'a' => \frontend\models\CategoryList::tree()]);
//            exit;
        }

        // сохранить в историю
        $sm = new HistorySearch();
        $sm->load(ArrayHelper::toArray($model, [
            ProductSearch::className() => [
                'field_what'         => 'note',
                'field_category_id'  => 'category_id',
                'field_where_string' => 'location_string',
                'field_where_coords' => 'location',
                'field_when_from'    => 'period_from',
                'field_when_to'      => 'period_to',
            ],
        ]), '');
//        print_r($sm);exit;

        if (!$sm->save()) {
//    print_r($sm->errors);exit;
            // do nothing
//            print 'no save';
        }

//        print_r($sm);exit;

        // это для случая, если указана категория но нет слова для поиска, тогда допишем название категории как слово поиска
        if (empty($model->note) && !empty($model->category_id)) {
            $model->note = Categories::find()->select('name')->where(['id' => $model->category_id])->scalar();
        }
//print '<pre>';print_r($model);exit;

        // собрать категории если поиск производился по слову без категории
        /** @var ActiveQuery $query */
        $query = clone $data->query;
        $query->with = null;
        $query->select(['name', 'category_id', 'c' => new Expression('COUNT([[id]])')])->groupBy('category_id');
        $query->with('category');
//print $query->createCommand()->rawSql;
//print '<pre>';print_r( $query->all() );exit;
//        print_r( $request->get());exit;

        return $this->render('index', [
            'model'        => $model,
            'dataProvider' => $data,
            'breadcrumbs'  => $model->catalogTreeBreadcrumbs(),
            'prices'       => array_intersect_key(array_merge([
                'minp'   => 0,
                'maxp'   => 0,
                'minplp' => 0,
                'maxplp' => 0,
            ],
                $model->prices),
                ['minp' => '', 'maxp' => '', 'minplp' => '', 'maxplp' => '']),
            'categories' => $model->category_id > 0 ? [] : $query->all()
        ]);
    }

    public function actionMap() {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $request = \Yii::$app->request;

        $query = Product::find()
            ->with([
                'category',
                'media'                      => function($query) {
                    $query->andWhere(['or like', 'link', ['%.png', '%.jpg', '%.gid'], false])
                        ->orderBy('created_at')->indexBy('id');
                },
                'author.userProfile.avatars' => function($query) {
                    $query->andWhere(['or like', 'path', ['%.png', '%.jpg', '%.gif'], false])
                        ->orderBy(['is_main' => SORT_DESC, 'created_at' => SORT_ASC]);
                },
            ])
            ->where(['is_deleted' => 0, 'is_draft' => 0])
            ->andWhere([
                'and',
                ['between', 'lat', $request->get('south'), $request->get('north')],
                ['between', 'lng', $request->get('west'), $request->get('east')],
            ]);
        $query->andFilterWhere(['not in', 'id', array_filter(explode(',', $request->get('except', '')))]);
        $query->orderBy(['lng' => SORT_ASC, 'lat' => SORT_DESC]);

//        $query->indexBy('id');
//        \Yii::$app->response->headers->add('sql', $query->createCommand()->rawSql);


        return ArrayHelper::toArray($query->all(), [
            Product::className() => [
                'id',
                'idKey'   => function($model) {
                    return 'id' . $model->id;
                },
                'coords'  => function($model) {
                    return ['latitude' => $model->lat, 'longitude' => $model->lng];
                },
                'options' => function($model) {
                    $price = ProductCurrency::formatPrice($model->price, $model->currency_id, false);
                    $w     = max(60, min(8 * strlen((string)$price), 130)) + 12; // ширина маркера
                    $h     = 30; // высота маркера

                    $points = [
                        'M 0 ' . (-$h / 2), // начальная точка
                        'l ' . ($w / 2 - 5) . ' 0', // линия сверху справа
                        'q 5 0, 5 5', // уголок правый верхний
                        'l 0 ' . ($h - 10), // линия справа вниз
                        'q 0 5, -5 5', // уголок справа нижний
                        'L 8 ' . ($h / 2), // линия снизу справа
                        'l -8 8', // засечка справа
                        'l -8 -8', // засечка слева
                        'L -' . ($w / 2 - 5) . ' ' . ($h / 2), // линия снизу слева
                        'q -5 0, -5 -5', // уголок снизу слева
                        'l 0 ' . (-$h + 10), // линия вертикальная справа
                        'q 0 -5, 5 -5', // уголок слева сверху
                        'z', // заканчиваем линию
                    ];

                    return [
                        'label'     => [
                            'text'  => $price,
                            'color' => '#fff',
                        ],
                        'animation' => 2,
                        //                        'icon'      => 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                        'icon'      => [
                            'path'         => implode(' ', $points),
                            'fillColor'    => '#40bc6e',
                            'fillOpacity'  => 0.9,
                            'scale'        => 1,
                            'strokeColor'  => '#ece5e4',
                            'strokeWeight' => 1,
                            'anchor'       => ['x' => 0, 'y' => $h / 2 + 8],
                        ],
                    ];
                },
                'product' => function($model) {
                    $img = ArrayHelper::getValue($model->media, [$model->media_default, "link"]);
                    if (is_null($img)) {
                        if (count($model->media)) {
                            $m = $model->media;
                            reset($m);

                            $img = current($m);
                            unset($m);
                        } else {
                            $img = '/img/category/imgCategory/defolt.svg';
                        }
                    }

                    $face = ArrayHelper::getValue($model->author->userProfile->avatars, [0, 'path'],
                        '/img/category/man.png');

                    return [
                        'name'         => $model->name,
                        'slug'         => $model->slug,
                        'category'     => $model->category->name,
                        'face_img'     => $img,
                        'price_step'   => $model->lease_time,
                        'price'        => ProductCurrency::formatPrice($model->price, $model->currency_id, false),
                        'pledge_price' => ProductCurrency::formatPrice($model->pledge_price, $model->currency_id,
                            false),
                        'min_period'   => $model->min_period,
                        'face_author'  => $face,
                        'user_id'      => $model->creator_id,
                        'is_fastrent'  => (int)$model->autoorder ? 1 : 0,
                        'rate'         => 2.5,
                    ];
                },
            ],
        ]);
    }

    public function actionNoResult() {
        return $this->render('no-result');
    }

    public function actionTemplate($run) {
        return $this->renderPartial("template/$run");
    }

    public function actionMobileAutocomplete($word) {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $word = trim($word);

        $query = Categories::find()
            ->select([Categories::tableName() . '.[[id]]', Categories::tableName() . '.[[name]]'])
            ->where(['is_active' => 1])
            ->joinWith('aliases', false, 'LEFT JOIN')
            ->andWhere([
                'or',
                ['like', Categories::tableName() . '.[[name]]', $word],
                ['like', CategoryAlias::tableName() . '.[[name]]', $word],
            ])
            ->andWhere([
                'not in',
                Categories::tableName() . '.[[id]]',
                Categories::find()->select('parent_id')->distinct()->where('[[parent_id]] IS NOT NULL'),
            ]);

//print $query->createCommand()->sql;exit;
        return [
            'w' => [],
            'c' => $query->all(),
        ];
    }

    public function actionCategoryAutocomplete($word) {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $word = trim($word);

        $query = Categories::find()
            ->select([Categories::tableName() . '.[[id]]', Categories::tableName() . '.[[name]]'])
            ->where(['is_active' => 1])
            ->joinWith('aliases', false, 'LEFT JOIN')
            ->andWhere([
                'or',
                ['like', Categories::tableName() . '.[[name]]', $word],
                ['like', CategoryAlias::tableName() . '.[[name]]', $word],
            ])
            ->andWhere([
                'not in',
                Categories::tableName() . '.[[id]]',
                Categories::find()->select('parent_id')->distinct()->where('[[parent_id]] IS NOT NULL'),
            ]);

        return $query->all();
    }

    public function actionCalculateAmount() {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new ProductSearch();
        $model->load(\Yii::$app->request->get());
        $data = $model->analyze();

        return ['a' => $data->getTotalCount()];
    }
}
