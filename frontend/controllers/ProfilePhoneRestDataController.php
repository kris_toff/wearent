<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 08.09.2017
 * Time: 11:20
 */

namespace frontend\controllers;


use common\models\UserPhone;
use yii\data\ActiveDataProvider;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;

class ProfilePhoneRestDataController extends Controller {

    public function actionIndex() {
        $query = UserPhone::find();
        $query->where(['user_id' => \Yii::$app->user->id, 'is_deleted' => 0]);

        return new ActiveDataProvider([
            'query'      => $query,
            'pagination' => false,
            'sort'       => false,
        ]);
    }

    public function actionDelete($id) {
        $model = UserPhone::findOne(['id' => $id, 'user_id' => \Yii::$app->user->id]);
        if (is_null($model)) {
            throw new BadRequestHttpException("Nothing found with id $id");
        }

        if ((int)$model->is_verify) {
            $model->softDelete();
        } else {
            $model->delete();
        }

        return null;
    }

    public function actionCreate() {
        $model = new UserPhone();

        if (!$model->load(\Yii::$app->request->post(), '')) {
            throw new BadRequestHttpException("Invalid data.");
        }

        $seq = array_rand(range(0, 9), 6);
        shuffle($seq);

        $token        = implode('', $seq);
        $model->token = $token;

        if (!$model->validate()) {
            \Yii::$app->response->statusCode = 400;
            \Yii::$app->response->data       = $model->errors;
            \Yii::$app->response->send();
        } elseif (!$model->save(false)) {
            throw new BadRequestHttpException("Save failed.");
        }

        return $model;
    }

    public function actionUpdate($id) {
        $model = UserPhone::findOne(['id' => $id, 'user_id' => \Yii::$app->user->id]);
        if (is_null($model)) {
            throw new BadRequestHttpException("Not found phone with id $id");
        }

        if (!(int)$model->is_verify){
            $seq = array_rand(range(0, 9), 6);
            shuffle($seq);

            $token        = implode('', $seq);
            $model->token = $token;
        }

        $model->load(\Yii::$app->request->getBodyParams(), '');
        $model->save();
sleep(2);
        return $model;
    }
}