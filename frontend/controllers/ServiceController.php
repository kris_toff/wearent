<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 29.05.2017
 * Time: 13:45
 */

namespace frontend\controllers;


use yii\web\Controller;

class ServiceController extends Controller
{
    public $layout = 'typicalFrontend';

    public function actionIndex()
    {
        return $this->render('index');
    }
}