<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 13.10.2017
 * Time: 23:48
 */

namespace frontend\controllers;


use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\Response;

class UtilsController extends Controller {
    public function behaviors() {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'change-currency' => ['put'],
                ],
            ],
        ];
    }

    public function actionChangeCurrency() {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $session = \Yii::$app->session;
        $session->open();

        $id = \Yii::$app->request->getBodyParam('c');
        if (\Yii::$app->user->isGuest) {
            // добавим валюту в session & cookie
            $session['currencyId'] = $id;
            \Yii::$app->response->cookies->add(new Cookie([
                'name'   => 'currencyId',
                'value'  => $id,
                'expire' => time() + 6 * 30 * 24 * 60 * 60,
            ]));
        } else {
            $user = \Yii::$app->user->identity;
            $user->userProfile->updateAttributes(['currency_id' => $id]);
        }

        $session->close();

        return [];
    }
}