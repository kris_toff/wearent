<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 19.12.2017
 * Time: 0:44
 */

namespace frontend\controllers;


use frontend\models\OrderForm;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

class OrderController extends Controller {
    public $enableCsrfValidation = false;

    public function actionCheckout() {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $request = \Yii::$app->request;

        $model = new OrderForm();
//        print_r($request->post());exit;
        if (!$model->load($request->post())) {
            throw new BadRequestHttpException("load failed");
        } elseif (!$model->validate()) {
            print_r($model->errors);exit;
            throw new BadRequestHttpException("validate failed");
        } elseif (!$model->save()) {
            throw new BadRequestHttpException("save failed");
        }

        return ['wer'];
    }
}