<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 12.05.2017
 * Time: 17:01
 */

namespace frontend\controllers;


use common\models\Product;
use common\models\User;
use common\models\UserEmail;
use common\models\UserPhone;
use common\models\UserProfile;
use frontend\models\components\AuthHandler;
use frontend\models\EstablishLoginForm;
use frontend\models\UserSocial;
use yii\authclient\AuthAction;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

class ProfileController extends Controller {
    public function actionIndex($id = null) {
        if (is_null($id)) {
            $id  = \Yii::$app->user->getId();
            $own = true;
        } elseif ($id == \Yii::$app->user->getId()) {
            $own = true;
        } else {
            $own = false;
        }

        $model   = UserProfile::find()->where(['user_id' => $id])->with([
            'avatars'       => function(ActiveQuery $query) {
                $query->orderBy(['is_main' => SORT_DESC, 'created_at' => SORT_DESC]);
            },
            'video'         => function(ActiveQuery $query) {
                $query->orderBy(['is_main' => SORT_DESC, 'created_at' => SORT_DESC]);
            },
            'user',
            'country',
            'languages',
            'user.products' => function(ActiveQuery $query) {
//            print $query->createCommand()->sql;exit;
                $query->andWhere(['status' => Product::OPEN, 'is_deleted' => 0])->orderBy(['created_at' => SORT_DESC]);

                $favQuery = (new Query());
                $favQuery->select(['product_id', 'isFavorite' => new Expression('1')])->from('{{%product_favorite}}');
                if (\Yii::$app->user->isGuest) {
                    $favQuery->where('0=1');
                } else {
                    $favQuery->where(['user_id' => \Yii::$app->user->id]);
                }
                $query->addSelect(Product::tableName() . '.*,{{u}}.[[isFavorite]]');
                $query->leftJoin([
                    'u' => $favQuery,
                ], '{{u}}.[[product_id]] = ' . Product::tableName() . '.[[id]]');

//                print $query->createCommand()->rawSql;exit;
            },
        ])->one();
        $previos = \Yii::$app->request->referrer;

        // избранные товаров
        $subQuery       = (new Query())->select('product_id')->from('{{%product_favorite}}')->where(['user_id' => $id]);
        $favouriteQuery = Product::find()->where(['id' => $subQuery, 'is_deleted' => 0, 'status' => Product::OPEN]);

        return $this->render('index', [
            'model'          => $model,
            'referrer'       => $previos,
            'own'            => $own,
            'favouriteQuery' => $favouriteQuery,
        ]);
    }

    public function actionEdit() {
        $model   = \Yii::$app->user->identity->userProfile;
        $request = \Yii::$app->request;

        if ($request->isAjax) {
            $umodel = \Yii::$app->user->identity;
            $umodel->load($request->post());

            \Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($umodel);
        } elseif ($request->isPost) {
            if (!($model->load($request->post()) && $model->save())) {
//                print_r($model->errors);
//                exit;
            } elseif (!($model->user->load($request->post()) && $model->user->save())) {
//                print_r($model->user->errors);
//                exit;
            } else {
                return $this->redirect(['/profile'], 307);
            }
        }

        return $this->render('edit/index', [
            'model' => $model,
        ]);
    }

    public function actionFeedback() {
        return $this->render('feedback');
    }

    public function actionMyProducts() {
        $query = Product::find()
            ->where(['is_deleted' => 0])
            ->andWhere(['creator_id' => \Yii::$app->user->id])
            ->with([
                'media' => function(ActiveQuery $query) {
                    $query->andWhere(['or like', 'link', ['%.png', '%.jpg', '%.gif'], false])->limit(1);
                },
                'category',
            ]);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'defaultPageSize' => 2,
            ],
            'sort'       => false,
        ]);

        return $this->render('goods', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionEditFeedback() {
        return $this->render('editFeedback');
    }

    public function actionCasa() {
        return $this->render('casa');
    }

    public function actionSettings() {
        return $this->render('setings');
    }

    public function actionDeal() {
        return $this->render('deal');
    }

    public function actionActions() {
        return $this->render('actions');
    }

    public function actionTemplate($run) {
        return $this->renderPartial("edit/templates/$run");
    }

    /**
     * Метод обработки запросов с почты для подтверджения адресов почты пользователей как достоверных
     *
     * @param string  $token
     * @param integer $n
     *
     * @return string
     * @throws \Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionRealfirm($token, $n) {
        $model = UserEmail::findOne(['user_id' => $this->user_id, 'id' => $n, 'is_deleted' => 0]);

        if (is_null($model)) {
            return $this->render('cemail/no-found');
        } elseif ((int)$model->is_verify) {
            return $this->render('cemail/already-done');
        } elseif (empty($this->token)) {
            return $this->render('cemail/no-register');
        }

        if (!strcmp($token, crypt($this->token, $this->user_id . ':' . $this->email))) {
            // совпадает
            $model->is_verify = 1;
            $model->token     = null;

            $result = $model->update();
        } else {
            return $this->render('cemail/wrong-token');
        }

        return $this->render('confirm-ok');
    }

    public function actionSendVerSms() {
        $model = UserPhone::findOne([
            'id'         => \Yii::$app->request->post('id'),
            'user_id'    => \Yii::$app->user->id,
            'is_deleted' => 0,
        ]);
        if (is_null($model)) {
            throw new BadRequestHttpException("This number not found.");
        }

        $seq = array_rand(range(0, 9), 6);
        shuffle($seq);

        $token = implode('', $seq);
        $model->updateAttributes(['token' => $token]);


        // отправляется смс
    }

    public function actionCheckVerSms() {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = UserPhone::findOne([
            'id'         => \Yii::$app->request->post('id'),
            'user_id'    => \Yii::$app->user->id,
            'is_deleted' => 0,
        ]);
        if (is_null($model)) {
            throw new BadRequestHttpException("This number not found.");
        } elseif (strcmp($model->token, \Yii::$app->request->post('code', ''))) {
            // не совпал код

            ///// delete!!
            if (\Yii::$app->request->post('code', '') != '123456') {
                throw new BadRequestHttpException("Code is wrong");
            }
        }

        $model->updateAttributes(['is_verify' => 1, 'token' => null, 'updated_at' => time()]);

        return $model;
    }

    public function actionCheckUniqueEmail() {
        $request                     = \Yii::$app->request;
        $email                       = $request->post('cemail');
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if (!empty($email)
            && (UserEmail::find()
                    ->where(['email' => $email, 'is_deleted' => 0, 'is_verify' => 1])
                    ->exists()
                || User::find()->where(['email' => $email])->exists())) {
            \Yii::$app->response->statusCode = 400;
        }
    }

    public function actionCheckUniquePhone() {
        $request                     = \Yii::$app->request;
        $phone                       = $request->post('cphone', '');
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if (!empty($phone)
            && UserPhone::find()
                ->where(['phone' => $phone, 'is_deleted' => 0, 'is_verify' => 1])
                ->exists()) {
            \Yii::$app->response->statusCode = 400;
        }
    }

    public function actionSetUserLogin() {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new EstablishLoginForm();
        $model->load(\Yii::$app->request->post(), '');

        if (!$model->validate()) {
            throw new BadRequestHttpException("Not validate.");
        } elseif (!$model->save(false)) {
            throw new BadRequestHttpException("Not saved.");
        }

        foreach (UserEmail::find()->where(['user_id' => $model->id, 'email' => $model->email])->each() as $im) {
            $im->delete();
        }
    }

    public function actionRemoveSocial() {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = UserSocial::findOne([
            'user_id' => \Yii::$app->user->id,
            'client'  => \Yii::$app->request->get('client'),
        ]);
        if (is_null($model)) {
            throw new BadRequestHttpException("No find client.");
        }

        $model->delete();
    }
}
