<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 11.08.2017
 * Time: 17:32
 */

namespace frontend\controllers;


use yii\web\Controller;

class NewsController extends Controller
{

    public $layout ='typicalFrontend';

    public function actionIndex(){
        return $this->render('index');
    }

    public function  actionView(){
        return $this-> render('article');
    }
}