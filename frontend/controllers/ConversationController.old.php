<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.08.2017
 * Time: 9:30
 */

namespace frontend\controllers;


use common\models\Chat;
use common\models\ChatMessage;
use common\models\Product;
use common\models\User;
use common\models\UserProfile;
use frontend\models\ChatConversationForm;
use GuzzleHttp\Exception\BadResponseException;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class ConversationController extends Controller {
    public function beforeAction($action) {
        if (!\Yii::$app->request->isAjax) {
            throw new MethodNotAllowedHttpException('This url can only handle the ajax request.');
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;

        return parent::beforeAction($action);
    }

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'verbs' => ['get', 'post'], 'roles' => ['@']],
                    ['allow' => true, 'actions' => ['update-data'], 'verbs' => ['head', 'get'], 'roles' => ['@']],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $id = \Yii::$app->request->get('id');

        return Chat::find()->where(['is_deleted']);
    }

    public function actionListMessages() {
        $request = \Yii::$app->request;

        $id       = $request->get('id');
        $only_new = $request->get('onlynew', false);

        $query = ChatMessage::find()
            ->select('message,created_at,author_id')
            ->with('author.userProfile')
            ->where(['chat_id' => $id]);
        $query->andFilterWhere(['is_seen' => $only_new ? 0 : ""]);

        ChatMessage::updateAll(['is_seen' => 1], ['chat_id' => $id]);

        return ArrayHelper::toArray($query->all(), [
            ChatMessage::className() => [
                'message',
                'created_at',
                'author' => 'author.userProfile',
            ],
            UserProfile::className() => [
                'fullName',
            ],
        ]);
    }

    public function actionView() {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $id   = \Yii::$app->request->get('id');
        $chat = Chat::find()->where(['id' => $id, 'is_deleted' => 0])->with('messages');
        if (is_null($chat)) {
            throw new BadRequestHttpException("Conversation with id $id does not exist.");
        }

        return $chat;
    }

    public function actionInterlocutorInfo() {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $product_id = \Yii::$app->request->get('pid');
        $model      = Product::find()
            ->where(['id' => $product_id, 'is_deleted' => 0, 'is_draft' => 0])
            ->with('author.userProfile')
            ->one();
        if (is_null($model)) {
            throw new InvalidParamException("Product with id $product_id does not exist.");
        } elseif ($model->author->id == \Yii::$app->user->id) {
            throw new ForbiddenHttpException("You cannot write message to youself.");
        }

        return array_merge(ArrayHelper::toArray($model, [
            Product::className() => [
                'id',
                'interlocutor' => function($model) {
                    return ArrayHelper::toArray($model->author->userProfile, [
                        UserProfile::className() => [
                            'fullName',
                            'avatar_path',
                        ],
                    ]);
                },
            ],
        ]), [
            'default_subject' => \Yii::t('UI11.2', '$DEFAULT_SUBJECT_CHAT$'),
        ]);
    }

    public function actionUpdateData() {
        $request = \Yii::$app->request;

        $oldIds = explode(',', $request->get('l', ''));
        $oldIds = array_filter($oldIds, function($o) {
            return !empty($o) && is_numeric($o) && (int)$o > 0;
        });

        $query = Chat::find()
            ->select(Chat::tableName() . '.*,{{ms}}.[[message]]')
            ->where([
                'or',
                ['from_id' => \Yii::$app->user->id],
                ['to_id' => \Yii::$app->user->id],
            ])
            ->andWhere(['not in', 'id', $oldIds])
            ->with([
                'interlocutor.userProfile',
                'author.userProfile',
            ]);

        $subquery = ChatMessage::find()
            ->select(['chat_id', 'm' => 'MAX([[updated_at]])'])
            ->groupBy('chat_id');
        $query2   = ChatMessage::find()
            ->select('u.chat_id,message')
            ->join('LEFT JOIN', ['u' => $subquery], '{{u}}.[[chat_id]] = '
                . ChatMessage::tableName()
                . '.[[chat_id]] AND {{u}}.[[m]] = '
                . ChatMessage::tableName()
                . '.[[updated_at]]')
            ->where('[[m]] IS NOT NULL')
            ->groupBy('chat_id');

        $query->join('LEFT JOIN', ['ms' => $query2], '{{ms}}.[[chat_id]] = ' . Chat::tableName() . '.[[id]]');

        $subqueryV2 = Chat::find()->select('id')->where([
            'or',
            ['to_id' => \Yii::$app->user->id],
            ['from_id' => \Yii::$app->user->id],
        ]);
        $queryV2    = ChatMessage::find()
            ->select(['chat_id', 'a' => 'COUNT([[id]])'])
            ->where(['<>', 'author_id', \Yii::$app->user->id])
            ->andWhere(['is_seen' => 0])
            ->andWhere(['chat_id' => $subqueryV2])
            ->groupBy('chat_id');

        return [
            'list' => ArrayHelper::toArray($query->all(), [
                Chat::className()        => [
                    'id',
                    'title'       => 'subject',
                    'pid'         => 'product_id',
                    'name'        => 'interlocutor.userProfile.fullName',
                    'lastMessage' => 'message',
                    'lastUpdate'  => 'updated_at',
                ],
                UserProfile::className() => [
                    'fullName',
                    'avatar' => 'avatar_path',
                ],
                ChatMessage::className() => [
                    'message',
                ],
                ChatMessage::className() => [
                    'id',
                    'message',
                ],
            ]),
            // new messages (only amount)
            'nwm'  => ArrayHelper::map($queryV2->asArray()->all(), 'chat_id', 'a'),
        ];
    }

    public function actionCreate() {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model       = new ChatConversationForm();
        $model->file = UploadedFile::getInstance($model, 'file');

        if (!($model->load(\Yii::$app->request->post()) && $model->save())) {
            print_r($model->errors);
            exit;
            throw new BadRequestHttpException("Validate form failed.");
        }

        return [
            'status'  => 'success',
            'message' => $model->message,
        ];
    }

    public function actionListConversations() {
        $query = Chat::find()
            ->select([Chat::tableName() . '.*', 'ms.message'])
            ->where(['or', ['from_id' => \Yii::$app->user->id], ['to_id' => \Yii::$app->user->id]])
            ->andWhere(['is_deleted' => 0])
            ->with([
                'interlocutor.userProfile',
                'author.userProfile',
            ])
            ->orderBy(['updated_at' => SORT_DESC, 'created_at' => SORT_DESC]);

        $subquery = ChatMessage::find()
            ->select(['chat_id', 'm' => 'MAX([[updated_at]])'])
            ->groupBy('chat_id');
        $query2   = ChatMessage::find()
            ->select('u.chat_id,message')
            ->join('LEFT JOIN', ['u' => $subquery], '{{u}}.[[chat_id]] = '
                . ChatMessage::tableName()
                . '.[[chat_id]] AND {{u}}.[[m]] = '
                . ChatMessage::tableName()
                . '.[[updated_at]]')
            ->where('[[m]] IS NOT NULL')
            ->groupBy('chat_id');

        $query->join('LEFT JOIN', ['ms' => $query2], '{{ms}}.[[chat_id]] = ' . Chat::tableName() . '.[[id]]');

        return ArrayHelper::toArray($query->all(), [
            Chat::className()        => [
                'id',
                'title'       => 'subject',
                'pid'         => 'product_id',
                'name'        => 'interlocutor.userProfile.fullName',
                'lastMessage' => 'message',
                'lastUpdate'  => 'updated_at',
            ],
            UserProfile::className() => [
                'fullName',
                'avatar' => 'avatar_path',
            ],
            ChatMessage::className() => [
                'message',
            ],
            ChatMessage::className() => [
                'id',
                'message',
            ],
        ]);
    }
}