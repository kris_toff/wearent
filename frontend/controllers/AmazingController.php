<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 01.09.2017
 * Time: 10:31
 */

namespace frontend\controllers;


use common\models\Product;
use common\models\ProductMedia;
use frontend\wizards\ProductKits;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class AmazingController extends Controller
{

    public $layout = 'typicalFrontend';

    public function actionIndex(){
        $toArray = [
            Product::className() => [
                'minislide'     => function($model) {
                    return ArrayHelper::toArray($model->media, [
                        ProductMedia::className() => [
                            'src' => 'link',
                        ],
                    ]);
                },
                'product_name'  => 'name',
                'slug',
                'category'      => function($model) {
                    return $model->category->name;
                },
                'price',
                'currency'      => function($model) {
                    return $model->currency->code;
                },
                'min_period',
                'step'          => function($model) {
                    return $model->leasing;
                },
                'countComments' => function() {
                    return 5;
                },
            ],
        ];

        $sliderLine1    = ArrayHelper::toArray(ProductKits::getListForSlider(24), $toArray);
        return $this->render('index',[
            'sliderLine1' => $sliderLine1
        ]);
    }

}