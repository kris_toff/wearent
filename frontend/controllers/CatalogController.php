<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 15.04.2017
 * Time: 14:35
 */

namespace frontend\controllers;


use backend\models\ProductAdvertisement;
use common\models\Product;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yiimodules\categories\models\Categories;

class CatalogController extends Controller {
    public function actionIndex($slug = '', $id = 0) {
//        \Yii::setAlias('@categories-assets', '@vendor/yiimodules/yii2-categories/assets');
        $id = max(intval($id), 0);

        if (empty($slug)) {
            // перебазировать на строковое
            $query = Categories::find()->select('slug');
            if ($id > 0) {
                $query->where(['is_active' => 1, 'id' => $id]);
            } else {
                $query->where(['or', ['parent_id' => 0], ['parent_id' => null]]);
            }
            $slug = $query->limit(1)->scalar();
            if (empty($slug)) throw new NotFoundHttpException('Страница не найдена.');

            return $this->redirect(['', 'slug' => $slug]);
        }
        $category = Categories::find()->where(['slug' => $slug, 'is_active' => 1])->limit(1)->one();
        if (is_null($category)) throw new NotFoundHttpException('Категория не найдена.');
        $rootId = (int)$category->id;

        $ids  = [];
        $nids = [$rootId];
        $i    = 0;
        do {
            $ids  = array_merge($ids, $nids);
            $nids = Categories::find()->select('id')->where(['parent_id' => $nids])->column();
            $i++;
        } while (!empty($nids) && $i < 20);
        $ids = array_unique($ids, SORT_NUMERIC);
        $ids = array_values($ids);

        $productIds = Product::find()->select('id')->where(['is_deleted' => 0, 'category_id' => $ids])->column();
        $topProducts=[];
        /*
        $topIds = ProductAdvertisement::find()->select('id')->distinct()->where(['and', ['and', ['product_id' => $productIds], ['active' => 1]], ['>', 'budget', 0], ['<', 'start_sell', $t], ['>', 'stop_sell', $t]])->column();
        $t          = time();
        print ProductAdvertisement::find()
            ->from(['pa' => ProductAdvertisement::find()->distinct()->where(['id'=>1])])
            ->select([Product::tableName() . '.*', ProductAdvertisement::tableName() . '.*'])
            ->where(['product_id' => $productIds, 'active' => 1])
            ->andWhere(['and', ['>', 'budget', 0], ['<', 'start_sell', $t], ['>', 'stop_sell', $t]])
            ->with('product.photo', 'product')->joinWith('product')
            ->asArray()
            ->limit(3)->createCommand()->rawSql;exit;
        $topProducts = new ActiveDataProvider([
            'query'      => ProductAdvertisement::find()
                ->distinct()
                ->select([Product::tableName() . '.*', ProductAdvertisement::tableName() . '.*'])
                ->where(['product_id' => $productIds, 'active' => 1])
                ->andWhere(['and', ['>', 'budget', 0], ['<', 'start_sell', $t], ['>', 'stop_sell', $t]])
                ->with('product.photo', 'product')->joinWith('product')
                ->asArray()
                ->limit(3),
            'pagination' => false,
            'sort'       => [
                'defaultOrder' => [
                    'click_cost' => SORT_DESC,
                    'budget'     => SORT_DESC,
                    'stop_sell'  => SORT_ASC,
                    'start_sell' => SORT_ASC,
                ],
            ],
            'key'        => 'product_id',
        ]);
print_r($topProducts->models);exit;*/
        $products = new ActiveDataProvider([
            'query'      => Product::find()->where(['id' => $productIds])->andWhere([
                'not in',
                'id',
                array_unique($topProducts->keys),
            ])->with('photo')->asArray(),
            'pagination' => [
                'pageParam'       => 'p',
                'pageSizeParam'   => 'ps',
                'defaultPageSize' => 20,
                'forcePageParam'  => false,
                'pageSizeLimit'   => [10, 100],
            ],
            'sort'       => [
                'attributes'   => [
                    'price' => [
                        'asc'     => ['price' => SORT_ASC, 'pledge_price' => SORT_ASC],
                        'desc'    => ['price' => SORT_DESC, 'pledge_price' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label'   => 'Price',
                    ],
                ],
                'defaultOrder' => [
                    'price' => SORT_DESC,
                ],
                'sortParam'    => 's',
            ],
        ]);

        return $this->render('index', [
            'category' => $category,
            'productProvider'    => $products,
            'topProductProvider' => $topProducts,
        ]);
    }
}