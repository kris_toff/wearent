<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.08.2017
 * Time: 14:08
 */

?>


<div class="list-group-item"
        data-id="{{$ctrl.model.id}}"
        ng-class="{active: $ctrl.isActive()}">

    <span class="status_icon pull-right">
        <md-icon ng-show="!+$ctrl.model.status">block</md-icon>
        <md-icon ng-show="+$ctrl.model.status == 1">done</md-icon>
        <md-icon ng-show="+$ctrl.model.status == 2">done_all</md-icon>
    </span>


    <div class="media">
        <div class="media-left">
            <a href="#"><img src="{{$ctrl.model.interlocutorAvatar || '/img/No_avatar.jpg'}}" /></a>
            <div class="user_link">
                <span class="user_online"
                        ng-class="{on: $ctrl.model.interlocutorOnline, off: !$ctrl.model.interlocutorOnline}"></span>
            </div>
        </div>
        <div class="media-body">
            <h4 class="media-heading" ng-bind-html="getData('subject')"></h4>
            <a class="text-left name" style="display: block;">
                <span ng-bind-html="getData('interlocutor_name')"></span>
                <span class="textGrey">&middot; {{relatedTime($ctrl.model.updated_at)}}</span></a>
            <span class="budge pull-right"
                    ng-if="$ctrl.model.amountNewMessages">{{$ctrl.model.amountNewMessages}}</span>
            <p class="text-left textMessage" ng-bind-html="getData('lastMessage')"></p>
        </div>
    </div>
</div>
