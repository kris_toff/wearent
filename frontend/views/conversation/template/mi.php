<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 23.08.2017
 * Time: 18:45
 */

?>

<div ng-show="$ctrl.model.firstOnDay" class="date_line">
    <hr />
    <div class="text-center">
        <span>{{$ctrl.relatedDate()}}</span>
    </div>
</div>

<span ng-if="$ctrl.model.is_own" class="pull-right">
    <md-icon ng-show="!+$ctrl.model.status">block</md-icon>
    <md-icon ng-show="+$ctrl.model.status == 1">done</md-icon>
    <md-icon ng-show="+$ctrl.model.status == 2">done_all</md-icon>
</span>

<div class="media">
    <div class="media-left media-middle">
        <a href="#" style="display:block;width:50px;"><img class="media-object img-responsive"
                    src="{{$ctrl.model.avatar || '/img/No_avatar.jpg'}}"
                    alt="avatar" /></a>
    </div>
    <div class="media-body">
        <h5 class="media-heading">{{$ctrl.model.author.fullName}}
            <span>&middot; {{$ctrl.relatedTime($ctrl.model.created_at)}}</span></h5>
        {{$ctrl.model.message}}
        <br />
        <img src="{{picture}}" ng-repeat="picture in $ctrl.model.media" />
    </div>
</div>

