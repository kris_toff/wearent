<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 07.08.2017
 * Time: 10:29
 */

use yii\bootstrap\Html;

/** @var \yii\web\View $this */

\yii\bootstrap\BootstrapPluginAsset::register($this);
\frontend\assets\ReadmoreAsset::register($this);
\frontend\assets\SliderAsset::register($this);
\frontend\assets\ProductAsset::register($this);
\frontend\assets\yyAsset::register($this);

?>


<div class="marg hidden-xs">

</div>
<div class="row actions  contentBody">
    <div class="container sidebarEditLeft hidden-xs hidden-sm">
        <div class="row">
            <div class="col-sm-12">
                <?= $this->render('left-menu') ?>
            </div>
        </div>
    </div>
    <div class="container content">
        <div class="row">
            <div class="col-xs-12">
                <div class="titleEdit">
                    <h4 class="text-left">
                        Спиок акций от Wearent
                    </h4>
                </div>
            </div>
        </div>

        <div class="row">
            <?= $this-> render('//action/action.php') ?>
            <?= $this-> render('//action/action.php') ?>
            <?= $this-> render('//action/action.php') ?>
            <?= $this-> render('//action/action.php') ?>
            <?= $this-> render('//action/action.php') ?>
        </div>
    </div>

</div>
