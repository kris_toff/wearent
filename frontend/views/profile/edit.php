<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.05.2017
 * Time: 12:01
 */


/** @var \yii\web\View $this */
/** @var \common\models\User $model */


\frontend\assets\FrontendAsset::register($this);
\yii\bootstrap\BootstrapPluginAsset::register($this);
\frontend\assets\SliderAsset::register($this);
\frontend\assets\ProductAsset::register($this);
\frontend\assets\yyAsset::register($this);
\frontend\assets\RateAsset::register($this);

?>


    <div class="row marg hidden-xs"></div>

    <div class="row edit contentBody">
        <div class="container sidebarEditLeft hidden-xs hidden-sm">
            <div class="row">
                <div class="col-sm-12">
                    <?= $this->render('edit/_left-menu') ?>
                </div>
            </div>
        </div>

        <!--    контент сторінки   -->
        <div class="container content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="titleEdit">
                        <h4 class="text-left">Редактировать профиль</h4>
                        <h4 class="hidden text-left titleBusiness">Бизнес аккаунт</h4>

                        <a href="/profile" class="showProfil">
                            <svg width="16px" height="21px" viewBox="0 0 16 21" version="1.1">
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="user" transform="translate(-1206.000000, -103.000000)" fill-rule="nonzero">
                                        <g id="product" transform="translate(1170.000000, 73.000000)">
                                            <path d="M46.5,39.5 C47.6,38.5 48.3,36.9 48.3,35.4 C48.3,32.9 46.3,30 43.7,30 C41.1,30 39.1,32.9 39.1,35.4 C39.1,36.9 39.8,38.4 40.9,39.5 C36.9,40.3 36,42.9 36,45 L36,50.4 C36,50.5 36.1,50.7 36.1,50.8 C36.1,50.9 36.3,50.9 36.5,50.9 L50.9,50.9 C51.2,50.9 51.4,50.7 51.4,50.4 L51.4,45 C51.3,42.9 50.4,40.3 46.5,39.5 Z M50.3,49.9 L36.9,49.9 L36.9,45 C36.9,42.2 38.6,40.7 42,40.3 C42.2,40.3 42.4,40.1 42.4,39.9 C42.4,39.7 42.3,39.5 42.2,39.4 C40.9,38.7 40,37 40,35.4 C40,33.4 41.6,31 43.6,31 C45.6,31 47.2,33.4 47.2,35.4 C47.2,37 46.3,38.6 45,39.4 C44.8,39.5 44.7,39.7 44.8,39.9 C44.9,40.1 45,40.3 45.2,40.3 C48.7,40.7 50.3,42.2 50.3,45 L50.3,49.9 L50.3,49.9 Z"
                                                    id="profile_icon"></path>
                                        </g>
                                    </g>
                                </g>
                            </svg>&nbsp;
                            Посмотреть профиль</a>
                    </div>


                    <div class="wrapPanel">
                        <div role="tabpanel" class="tabPaneledit">

                            <!-- Навігаційні елементи вкладок -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#data" aria-controls="home" role="tab"
                                            data-toggle="tab">О себе</a></li>

                                <li role="presentation"><a href="#staticData" aria-controls="staticData" role="tab"
                                            data-toggle="tab">Статистика</a></li>

                                <li role="presentation"><a href="#aboutSelf" aria-controls="aboutSelf" role="tab"
                                            data-toggle="tab">Дополнительно о себе</a></li>

                                <li role="presentation"><a href="#contacts" aria-controls="scontacts" role="tab"
                                            data-toggle="tab">Контакты</a></li>

                                <li role="presentation"><a href="#fotoVideo" aria-controls="fotoVideo" role="tab"
                                            data-toggle="tab">Фото и видео</a></li>

                                <li role="presentation"><a href="#trust"
                                            aria-controls="trust"
                                            role="tab"
                                            data-toggle="tab">Верификация</a>
                                </li>

                                <li role="presentation"><a href="#reference" aria-controls="reference" role="tab"
                                            data-toggle="tab">Рекомендации</a></li>

                                <!--business account-->
                                <li role="presentation"><a href="#aboutCompany" aria-controls="reference" role="tab"
                                            data-toggle="tab">О компании</a></li>
                                <!--контакти business account-->
                                <li role="presentation"><a href="#contactsCompany" aria-controls="reference" role="tab"
                                            data-toggle="tab">Контакты</a></li>

                                <!--оплата business account-->
                                <li role="presentation"><a href="#payCompany" aria-controls="reference" role="tab"
                                            data-toggle="tab">Оплата</a></li>

                            </ul>

                            <!-- Вкладки панелі -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane  fade in active" id="data">
                                    <!--                обовязкові дані-->
                                    <div class="row obligatoryInf allBlockContent">
                                        <div class="col-sm-12">
                                            <h4 class="text-left">Обязательное о себе</h4>

                                            <form action="" class="obligatoryForm styleInput">
                                                <label for="name">Имя:<br>
                                                    <input type="text" id="name" placeholder="Дмитрий">
                                                </label>

                                                <label for="surname">Фамилия:<br>
                                                    <input type="text" id="surname" placeholder="Чудаков">
                                                    <span class="svgForInput">
                                                    <svg width="26px" height="18px" viewBox="0 0 26 18">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="-UI14.1----Редактировать-профиль---Контакты" transform="translate(-640.000000, -315.000000)"
                fill-rule="nonzero" fill="#AFAFAF">
            <g id="Group-15" transform="translate(280.000000, 151.000000)">
                <g id="about" transform="translate(0.000000, 26.000000)">
                    <g id="Group-2" transform="translate(19.000000, 54.000000)">
                        <g id="Group-19" transform="translate(1.000000, 49.000000)">
                            <g id="1490919889_icon-21-eye-hidden" transform="translate(340.000000, 35.000000)">
                                <g id="Group">
                                    <g id="Page-1">
                                        <g id="icon-21-eye-hidden">
                                            <path d="M5.10869891,13.8913011 C1.61720816,11.8301147 0,9 0,9 C0,9 4,2 13,2 C14.3045107,2 15.5039752,2.14706466 16.6014388,2.39856122 L15.7519017,3.2480983 C14.8971484,3.0900546 13.9800929,3 13,3 C5,3 1.19995117,9 1.19995117,9 C1.19995117,9 2.71472808,11.3917225 5.84492713,13.1550729 L5.10869891,13.8913011 L5.10869891,13.8913011 L5.10869891,13.8913011 Z M9.398561,15.601439 C10.4960246,15.8529356 11.6954892,16.0000001 13,16 C22,15.999999 26,9 26,9 C26,9 24.3827918,6.1698856 20.8913008,4.1086992 L20.1550727,4.8449273 C23.2852719,6.6082776 24.8000488,9 24.8000488,9 C24.8000488,9 21,14.999999 13,15 C12.019907,15.0000001 11.1028515,14.9099455 10.2480981,14.7519019 L9.398561,15.601439 L9.398561,15.601439 L9.398561,15.601439 Z M16.8986531,8.1013469 C16.9649658,8.3902115 17,8.6910144 17,9 C17,11.2091391 15.2091391,13 13,13 C12.6910144,13 12.3902115,12.9649658 12.1013469,12.8986531 L13,12 C13.7677669,12.0000001 14.5355339,11.7071068 15.1213203,11.1213203 C15.7071068,10.5355339 16.0000001,9.7677669 16,9 L16.8986531,8.1013469 L16.8986531,8.1013469 L16.8986531,8.1013469 Z M13.8986531,5.1013469 C13.6097885,5.0350342 13.3089856,5 13,5 C10.7908609,5 9,6.7908609 9,9 C9,9.3089856 9.0350342,9.6097885 9.1013469,9.8986531 L10,9 C9.9999999,8.2322331 10.2928932,7.4644661 10.8786797,6.8786797 C11.4644661,6.2928932 12.2322331,5.9999999 13,6 L13.8986531,5.1013469 L13.8986531,5.1013469 L13.8986531,5.1013469 Z M4,17 L5,18 L22,1 L21,0 L4,17 Z"
                                                    id="eye-hidden"></path>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span>
                                                </label>

                                                <label for="sex">Пол:<br>
                                                    <div class="input-group">
                                                        <input type="text" id="sex" class="form-control"
                                                                placeholder="Выбрать пол">
                                                        <div class="input-group-btn">
                                                            <button type="button"
                                                                    class="btn btn-default dropdown-toggle btnForInput"
                                                                    data-toggle="dropdown" aria-expanded="false"> <span>
                                                                <svg width="10px" height="6px" viewBox="0 0 10 6">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"
            stroke-linejoin="round">
        <g id="UI-Центр-поддержки" transform="translate(-693.000000, -953.000000)" stroke="#90949B" stroke-width="2">
            <g id="REVIEWS-Copy" transform="translate(470.000000, 638.000000)">
                <g id="Group-38" transform="translate(94.000000, 306.000000)">
                    <polyline id="Path-3-Copy"
                            transform="translate(133.757144, 11.935000) scale(-1, 1) rotate(-270.000000) translate(-133.757144, -11.935000) "
                            points="131.822144 15.6921435 135.692143 11.8221436 132.047856 8.17785643"></polyline>
                </g>
            </g>
        </g>
    </g>
</svg></span></button>
                                                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                                                <li><a href="#">Мужской</a></li>
                                                                <li><a href="#">Женский</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <span class="svgForInput">
                                                    <svg width="26px" height="18px" viewBox="0 0 26 18">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="-UI14.1----Редактировать-профиль---Контакты" transform="translate(-640.000000, -315.000000)"
                fill-rule="nonzero" fill="#AFAFAF">
            <g id="Group-15" transform="translate(280.000000, 151.000000)">
                <g id="about" transform="translate(0.000000, 26.000000)">
                    <g id="Group-2" transform="translate(19.000000, 54.000000)">
                        <g id="Group-19" transform="translate(1.000000, 49.000000)">
                            <g id="1490919889_icon-21-eye-hidden" transform="translate(340.000000, 35.000000)">
                                <g id="Group">
                                    <g id="Page-1">
                                        <g id="icon-21-eye-hidden">
                                            <path d="M5.10869891,13.8913011 C1.61720816,11.8301147 0,9 0,9 C0,9 4,2 13,2 C14.3045107,2 15.5039752,2.14706466 16.6014388,2.39856122 L15.7519017,3.2480983 C14.8971484,3.0900546 13.9800929,3 13,3 C5,3 1.19995117,9 1.19995117,9 C1.19995117,9 2.71472808,11.3917225 5.84492713,13.1550729 L5.10869891,13.8913011 L5.10869891,13.8913011 L5.10869891,13.8913011 Z M9.398561,15.601439 C10.4960246,15.8529356 11.6954892,16.0000001 13,16 C22,15.999999 26,9 26,9 C26,9 24.3827918,6.1698856 20.8913008,4.1086992 L20.1550727,4.8449273 C23.2852719,6.6082776 24.8000488,9 24.8000488,9 C24.8000488,9 21,14.999999 13,15 C12.019907,15.0000001 11.1028515,14.9099455 10.2480981,14.7519019 L9.398561,15.601439 L9.398561,15.601439 L9.398561,15.601439 Z M16.8986531,8.1013469 C16.9649658,8.3902115 17,8.6910144 17,9 C17,11.2091391 15.2091391,13 13,13 C12.6910144,13 12.3902115,12.9649658 12.1013469,12.8986531 L13,12 C13.7677669,12.0000001 14.5355339,11.7071068 15.1213203,11.1213203 C15.7071068,10.5355339 16.0000001,9.7677669 16,9 L16.8986531,8.1013469 L16.8986531,8.1013469 L16.8986531,8.1013469 Z M13.8986531,5.1013469 C13.6097885,5.0350342 13.3089856,5 13,5 C10.7908609,5 9,6.7908609 9,9 C9,9.3089856 9.0350342,9.6097885 9.1013469,9.8986531 L10,9 C9.9999999,8.2322331 10.2928932,7.4644661 10.8786797,6.8786797 C11.4644661,6.2928932 12.2322331,5.9999999 13,6 L13.8986531,5.1013469 L13.8986531,5.1013469 L13.8986531,5.1013469 Z M4,17 L5,18 L22,1 L21,0 L4,17 Z"
                                                    id="eye-hidden"></path>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span>
                                                </label>

                                                <div class="birthDay">
                                                    <label for="day" class="dayBlock">День:<br>
                                                        <div class="input-group">
                                                            <input type="text" id="day" class="form-control">
                                                            <div class="input-group-btn">
                                                                <button type="button"
                                                                        class="btn btn-default dropdown-toggle btnForInput"
                                                                        data-toggle="dropdown"
                                                                        aria-expanded="false"><span><svg
                                                                                width="10px" height="6px"
                                                                                viewBox="0 0 10 6">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"
            stroke-linejoin="round">
        <g id="UI-Центр-поддержки" transform="translate(-693.000000, -953.000000)" stroke="#90949B" stroke-width="2">
            <g id="REVIEWS-Copy" transform="translate(470.000000, 638.000000)">
                <g id="Group-38" transform="translate(94.000000, 306.000000)">
                    <polyline id="Path-3-Copy"
                            transform="translate(133.757144, 11.935000) scale(-1, 1) rotate(-270.000000) translate(-133.757144, -11.935000) "
                            points="131.822144 15.6921435 135.692143 11.8221436 132.047856 8.17785643"></polyline>
                </g>
            </g>
        </g>
    </g>
</svg></span></button>
                                                                <ul class="dropdown-menu dropdown-menu-right"
                                                                        role="menu">
                                                                    <li><a href="#">Дія</a></li>
                                                                    <li><a href="#">Інша дія</a></li>

                                                                </ul>
                                                            </div><!-- /btn-group -->
                                                        </div><!-- /input-group -->
                                                    </label>

                                                    <label for="month" class="monthBlock">Месяц:<br>
                                                        <div class="input-group">
                                                            <input type="text" id="month" class="form-control">
                                                            <div class="input-group-btn">
                                                                <button type="button"
                                                                        class="btn btn-default dropdown-toggle btnForInput"
                                                                        data-toggle="dropdown"
                                                                        aria-expanded="false"><span><svg
                                                                                width="10px" height="6px"
                                                                                viewBox="0 0 10 6">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"
            stroke-linejoin="round">
        <g id="UI-Центр-поддержки" transform="translate(-693.000000, -953.000000)" stroke="#90949B" stroke-width="2">
            <g id="REVIEWS-Copy" transform="translate(470.000000, 638.000000)">
                <g id="Group-38" transform="translate(94.000000, 306.000000)">
                    <polyline id="Path-3-Copy"
                            transform="translate(133.757144, 11.935000) scale(-1, 1) rotate(-270.000000) translate(-133.757144, -11.935000) "
                            points="131.822144 15.6921435 135.692143 11.8221436 132.047856 8.17785643"></polyline>
                </g>
            </g>
        </g>
    </g>
</svg></span></button>
                                                                <ul class="dropdown-menu dropdown-menu-right"
                                                                        role="menu">
                                                                    <li><a href="#">Дія</a></li>
                                                                    <li><a href="#">Інша дія</a></li>

                                                                </ul>
                                                            </div><!-- /btn-group -->
                                                        </div><!-- /input-group -->
                                                    </label>

                                                    <label for="year" class="yearBlock">Год:<br>
                                                        <div class="input-group">
                                                            <input type="text" id="year" class="form-control">
                                                            <div class="input-group-btn">
                                                                <button type="button"
                                                                        class="btn btn-default dropdown-toggle btnForInput"
                                                                        data-toggle="dropdown"
                                                                        aria-expanded="false"><span><svg
                                                                                width="10px" height="6px"
                                                                                viewBox="0 0 10 6">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"
            stroke-linejoin="round">
        <g id="UI-Центр-поддержки" transform="translate(-693.000000, -953.000000)" stroke="#90949B" stroke-width="2">
            <g id="REVIEWS-Copy" transform="translate(470.000000, 638.000000)">
                <g id="Group-38" transform="translate(94.000000, 306.000000)">
                    <polyline id="Path-3-Copy"
                            transform="translate(133.757144, 11.935000) scale(-1, 1) rotate(-270.000000) translate(-133.757144, -11.935000) "
                            points="131.822144 15.6921435 135.692143 11.8221436 132.047856 8.17785643"></polyline>
                </g>
            </g>
        </g>
    </g>
</svg></span></button>
                                                                <ul class="dropdown-menu dropdown-menu-right"
                                                                        role="menu">
                                                                    <li><a href="#">Дія</a></li>
                                                                    <li><a href="#">Інша дія</a></li>

                                                                </ul>
                                                            </div><!-- /btn-group -->
                                                        </div><!-- /input-group -->
                                                    </label>

                                                    <span class="svgForInput">
                                                    <svg width="26px" height="18px" viewBox="0 0 26 18">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="-UI14.1----Редактировать-профиль---Контакты" transform="translate(-640.000000, -315.000000)"
                fill-rule="nonzero" fill="#AFAFAF">
            <g id="Group-15" transform="translate(280.000000, 151.000000)">
                <g id="about" transform="translate(0.000000, 26.000000)">
                    <g id="Group-2" transform="translate(19.000000, 54.000000)">
                        <g id="Group-19" transform="translate(1.000000, 49.000000)">
                            <g id="1490919889_icon-21-eye-hidden" transform="translate(340.000000, 35.000000)">
                                <g id="Group">
                                    <g id="Page-1">
                                        <g id="icon-21-eye-hidden">
                                            <path d="M5.10869891,13.8913011 C1.61720816,11.8301147 0,9 0,9 C0,9 4,2 13,2 C14.3045107,2 15.5039752,2.14706466 16.6014388,2.39856122 L15.7519017,3.2480983 C14.8971484,3.0900546 13.9800929,3 13,3 C5,3 1.19995117,9 1.19995117,9 C1.19995117,9 2.71472808,11.3917225 5.84492713,13.1550729 L5.10869891,13.8913011 L5.10869891,13.8913011 L5.10869891,13.8913011 Z M9.398561,15.601439 C10.4960246,15.8529356 11.6954892,16.0000001 13,16 C22,15.999999 26,9 26,9 C26,9 24.3827918,6.1698856 20.8913008,4.1086992 L20.1550727,4.8449273 C23.2852719,6.6082776 24.8000488,9 24.8000488,9 C24.8000488,9 21,14.999999 13,15 C12.019907,15.0000001 11.1028515,14.9099455 10.2480981,14.7519019 L9.398561,15.601439 L9.398561,15.601439 L9.398561,15.601439 Z M16.8986531,8.1013469 C16.9649658,8.3902115 17,8.6910144 17,9 C17,11.2091391 15.2091391,13 13,13 C12.6910144,13 12.3902115,12.9649658 12.1013469,12.8986531 L13,12 C13.7677669,12.0000001 14.5355339,11.7071068 15.1213203,11.1213203 C15.7071068,10.5355339 16.0000001,9.7677669 16,9 L16.8986531,8.1013469 L16.8986531,8.1013469 L16.8986531,8.1013469 Z M13.8986531,5.1013469 C13.6097885,5.0350342 13.3089856,5 13,5 C10.7908609,5 9,6.7908609 9,9 C9,9.3089856 9.0350342,9.6097885 9.1013469,9.8986531 L10,9 C9.9999999,8.2322331 10.2928932,7.4644661 10.8786797,6.8786797 C11.4644661,6.2928932 12.2322331,5.9999999 13,6 L13.8986531,5.1013469 L13.8986531,5.1013469 L13.8986531,5.1013469 Z M4,17 L5,18 L22,1 L21,0 L4,17 Z"
                                                    id="eye-hidden"></path>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span>

                                                </div>

                                                <label for="about" class="aboutSelfBlock">Расскажите о себе:<br>
                                                    <textarea name="" id="about" placeholder="Привет!
Меня зовут ...
Больше всего на свете я люблю....
По специальности я...
Вам понравится со мной сотрудничать, потому что я..."></textarea>
                                                    <span class="hidden-xs popoveerrr">Продолжите предложения или напишите своими словами. </span>
                                                </label>


                                            </form>

                                            <div class="blockTwoBtn">
                                                <button type="submit" class="btn greenBtn">Сохранить</button>
                                                <button type="reset" class="btn redBtn">Отменить</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade " id="staticData">
                                    <!--                статичні дані-->
                                    <div class="row statick allBlockContent">
                                        <div class="col-sm-12">
                                            <div class="status">
                                                <span class="leftText"><h4>Ваши статусы</h4></span>
                                                <div class="slStatus">
                                                    <div class="thumbnail">
                                                        <img data-src="" class="text-center" alt="...">
                                                        <div class="caption">
                                                            <h5 class="text-center">Лучший хозяин</h5>
                                                        </div>
                                                    </div>


                                                    <div class="thumbnail">
                                                        <img data-src="" class="text-center" alt="...">
                                                        <div class="caption">
                                                            <h5 class="text-center">Активный участник сообщества</h5>
                                                        </div>
                                                    </div>


                                                    <div class="thumbnail">
                                                        <img data-src="" class="text-center" alt="...">
                                                        <div class="caption">
                                                            <h5 class="text-center">Награда от компании Wearent</h5>
                                                        </div>
                                                    </div>


                                                </div>

                                            </div>
                                            <div class="rating">
                                                <span class="leftText"><h4>Ваши рейтинг</h4></span>
                                                <div class="ratingTop blockRating">
                                                    <p class="titleRating">Отзывы арендаторов:</p>
                                                    <table class="table-responsive">
                                                        <tr class="
provider">
                                                            <td class="fitrsTd">Общая оценка:</td>
                                                            <td class="twoTd">5</td>
                                                            <td>
                                                                <div class="rateyo"></div>
                                                            </td>
                                                            <td class="lastTd">
                                                                <div class="progress">
                                                                    <div class="progress-bar" role="progressbar"
                                                                            aria-valuenow="60"
                                                                            aria-valuemin="0" aria-valuemax="100"
                                                                            style="width: 100%;">
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr class="product">
                                                            <td class="fitrsTd">Качество товара:</td>
                                                            <td class="twoTd">4</td>
                                                            <td>
                                                                <div class="rateyo"></div>
                                                            </td>
                                                            <td class="lastTd">
                                                                <div class="progress">
                                                                    <div class="progress-bar" role="progressbar"
                                                                            aria-valuenow="60"
                                                                            aria-valuemin="0" aria-valuemax="100"
                                                                            style="width: 80%;">
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr class="description">
                                                            <td class="fitrsTd">Корректность описания:</td>
                                                            <td class="twoTd">4</td>
                                                            <td>
                                                                <div class="rateyo"></div>
                                                            </td>
                                                            <td class="lastTd">
                                                                <div class="progress">
                                                                    <div class="progress-bar" role="progressbar"
                                                                            aria-valuenow="60"
                                                                            aria-valuemin="0" aria-valuemax="100"
                                                                            style="width: 80%;">
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div class="ratingBottom blockRating">
                                                    <p class="titleRating">Отзывы хозяев:</p>
                                                    <table class="table-responsive">
                                                        <tr class="
provider">
                                                            <td class="fitrsTd">Общая оценка:</td>
                                                            <td class="twoTd">5</td>
                                                            <td>
                                                                <div class="rateyo"></div>
                                                            </td>
                                                            <td class="lastTd">
                                                                <div class="progress">
                                                                    <div class="progress-bar" role="progressbar"
                                                                            aria-valuenow="60"
                                                                            aria-valuemin="0" aria-valuemax="100"
                                                                            style="width: 100%;">
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr class="product">
                                                            <td class="fitrsTd">Качество товара:</td>
                                                            <td class="twoTd">4</td>
                                                            <td>
                                                                <div class="rateyo"></div>
                                                            </td>
                                                            <td class="lastTd">
                                                                <div class="progress">
                                                                    <div class="progress-bar" role="progressbar"
                                                                            aria-valuenow="60"
                                                                            aria-valuemin="0" aria-valuemax="100"
                                                                            style="width: 80%;">
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr class="description">
                                                            <td class="fitrsTd">Корректность описания:</td>
                                                            <td class="twoTd">4</td>
                                                            <td>
                                                                <div class="rateyo"></div>
                                                            </td>
                                                            <td class="lastTd">
                                                                <div class="progress">
                                                                    <div class="progress-bar" role="progressbar"
                                                                            aria-valuenow="60"
                                                                            aria-valuemin="0" aria-valuemax="100"
                                                                            style="width: 80%;">
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                            </div>

                                            <div class="speed">
                                                <div class="speedTitle">
                                                    <span class="leftText"><h4>Скорость ответа на письма</h4></span>
                                                    <span class="hoverPopup" role="button" data-toggle="popover"
                                                            data-trigger="hover" data-container="body" data-html="true"
                                                            data-placement="right"
                                                            data-content="<p> Скорость и регулярность ответов играет важную роль для выбора вашего товара - многим пользователям важно понимать, что ответ они получат в течении ближайших часов.</p>
                                      <p>Мы рекомендуем вам отвечать на все запросы и стараться поддерживать показатель 'скорость ответа' на значении в 2 часа  </p>"
                                                            data-template='<div class="popover speedBlockHover" role="tooltip">
                                 <div class="arrow" style="top: 25%;"></div>
                                 <div class="popover-content"></div>
                                 </div>'>
                                                    <svg class="speedHover" width="20px" height="20px"
                                                            viewBox="0 0 20 20">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="+UI16-Касса---модалы" transform="translate(-1124.000000, -595.000000)">
            <g id="Group-18">
                <g id="Group-24-Copy" transform="translate(1125.000000, 596.000000)">
                    <path d="M8.8,0 C4,0 0,3.9 0,8.8 C0,13.7 3.9,17.6 8.8,17.6 C13.7,17.6 17.6,13.7 17.6,8.8 C17.6,3.9 13.6,0 8.8,0 L8.8,0 Z"
                            id="Shape" stroke="#E35F46" fill-rule="nonzero"></path>
                    <g id="speedHover" transform="translate(7.000000, 4.000000)" fill="#E35F46">
                        <path d="M1.9,0 C2.5,0 2.9,0.5 2.9,1 C2.9,1.6 2.4,2 1.9,2 C1.3,2 0.9,1.5 0.9,1 C0.8,0.4 1.3,0 1.9,0 L1.9,0 Z"
                                id="Path"></path>
                        <path d="M3.4,8.9 L0.8,8.9 C0.4,8.9 0,8.6 0,8.1 C0,7.6 0.3,7.3 0.8,7.3 L1.4,7.3 L1.4,4.6 L0.8,4.6 C0.4,4.6 0,4.3 0,3.8 C0,3.3 0.3,3 0.8,3 L2.1,3 C2.5,3 2.9,3.3 2.9,3.8 L2.9,7.4 L3.5,7.4 C3.9,7.4 4.3,7.7 4.3,8.2 C4.3,8.7 3.8,8.9 3.4,8.9 L3.4,8.9 Z"
                                id="Path"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                </span>
                                                </div>

                                                <ul class="list-unstyled">
                                                    <li class="frequency">Частота ответов: <span>96%</span></li>
                                                    <li class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                                aria-valuenow="60"
                                                                aria-valuemin="0" aria-valuemax="100"
                                                                style="width: 96%;">
                                                            <span class="sr-only">60% Завершено</span>
                                                        </div>
                                                    </li>
                                                    <li class="time">Время ответа: <span>5 часов</span></li>
                                                </ul>

                                            </div>
                                            <div class="blockTwoBtn">
                                                <button type="submit" class="btn greenBtn">Сохранить</button>
                                                <button type="reset" class="btn redBtn">Отменить</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade " id="aboutSelf">
                                    <!--                додаткові дані-->
                                    <div class="row addInf allBlockContent">
                                        <div class="col-sm-12">
                                            <form action="" class="addDataForm styleInput">
                                                <div class="leave">
                                                    <h4>Где вы живете</h4>
                                                    <label for="day" class="dayBlock">Страна:<br>
                                                        <div class="input-group">
                                                            <input type="text" id="day" class="form-control">
                                                            <div class="input-group-btn">
                                                                <button type="button"
                                                                        class="btn btn-default dropdown-toggle btnForInput"
                                                                        data-toggle="dropdown"
                                                                        aria-expanded="false"><span><svg
                                                                                width="10px" height="6px"
                                                                                viewBox="0 0 10 6">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"
            stroke-linejoin="round">
        <g id="UI-Центр-поддержки" transform="translate(-693.000000, -953.000000)" stroke="#90949B" stroke-width="2">
            <g id="REVIEWS-Copy" transform="translate(470.000000, 638.000000)">
                <g id="Group-38" transform="translate(94.000000, 306.000000)">
                    <polyline id="Path-3-Copy"
                            transform="translate(133.757144, 11.935000) scale(-1, 1) rotate(-270.000000) translate(-133.757144, -11.935000) "
                            points="131.822144 15.6921435 135.692143 11.8221436 132.047856 8.17785643"></polyline>
                </g>
            </g>
        </g>
    </g>
</svg></span></button>
                                                                <ul class="dropdown-menu dropdown-menu-right"
                                                                        role="menu">
                                                                    <li><a href="#">Дія</a></li>
                                                                    <li><a href="#">Інша дія</a></li>

                                                                </ul>
                                                            </div><!-- /btn-group -->
                                                        </div><!-- /input-group -->
                                                    </label>

                                                    <label for="sity">Город:<br>
                                                        <input type="text" id="sity">
                                                    </label>
                                                </div>

                                                <div class="work">
                                                    <h4>Где вы работаете</h4>
                                                    <label for="work">Название компании:<br>
                                                        <input type="text" id="work">
                                                    </label>
                                                </div>


                                                <div class="language">
                                                    <h4>Какие вы знаете языки</h4>
                                                    <div class="languageForm chip"></div>
                                                </div>

                                                <div class="blockTwoBtn">
                                                    <button type="submit" class="btn greenBtn">Сохранить</button>
                                                    <button type="reset" class="btn redBtn">Отменить</button>
                                                </div>
                                            </form>


                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade " id="contacts">
                                    <!--                контакти-->
                                    <div class="row contact allBlockContent">
                                        <div class="col-sm-12">
                                            <div class="email">
                                                <span><h4>Электронные адреса</h4></span>
                                                <div class="emailForm styleInput">
                                                    <div class="emailBlock 0">
                                                        <label for="email">Электронный адрес:</label><br>

                                                        <div class="inlineInput">
                                                            <input type="email" id="email">
                                                            <a href="#" class="removeEmail">
                                                                <svg width="14px" height="19px" viewBox="0 0 14 19">

                                                                    <g id="Page-1" stroke="none" stroke-width="1"
                                                                            fill="none" fill-rule="evenodd">
                                                                        <g class="re"
                                                                                transform="translate(-1090.000000, -370.000000)"
                                                                                fill-rule="nonzero" stroke-width="0.3">
                                                                            <g id="content"
                                                                                    transform="translate(280.000000, 151.000000)">
                                                                                <g id="Group-22"
                                                                                        transform="translate(20.000000, 89.000000)">
                                                                                    <g id="Group-20">
                                                                                        <g id="Group-4">
                                                                                            <g id="Group-24"
                                                                                                    transform="translate(770.000000, 26.000000)">
                                                                                                <g id="delete_icon"
                                                                                                        transform="translate(21.000000, 105.000000)">
                                                                                                    <path d="M11.9953016,2.04505221 L8.84168254,2.04505221 L8.84168254,1.36682731 C8.84168254,0.617769076 8.27685714,0.01037751 7.58022222,0.01037751 L4.42660317,0.01037751 C3.72996825,0.01037751 3.16514286,0.617769076 3.16514286,1.36682731 L3.16514286,2.04505221 L0.0115238095,2.04505221 L0.0115238095,2.72327711 L0.661365079,2.72327711 L1.29269841,15.6094478 C1.29269841,16.3585402 1.85752381,16.9658976 2.55415873,16.9658976 L9.49212698,16.9658976 C10.1887619,16.9658976 10.7535873,16.3585402 10.7535873,15.6094478 L11.3738413,2.72327711 L11.9953333,2.72327711 L11.9953333,2.04505221 L11.9953016,2.04505221 Z M3.79587302,1.36682731 C3.79587302,0.99262249 4.0792381,0.68860241 4.42660317,0.68860241 L7.58022222,0.68860241 C7.92822222,0.68860241 8.21095238,0.99262249 8.21095238,1.36682731 L8.21095238,2.04505221 L3.79587302,2.04505221 L3.79587302,1.36682731 Z M10.1240635,15.5743213 L10.1228254,15.5915602 L10.1228254,15.6094478 C10.1228254,15.983004 9.84009524,16.2876727 9.49209524,16.2876727 L2.55412698,16.2876727 C2.2067619,16.2876727 1.92339683,15.983004 1.92339683,15.6094478 L1.92339683,15.5915602 L1.92279365,15.5737068 L1.29269841,2.72327711 L10.7425079,2.72327711 L10.1240635,15.5743213 Z"
                                                                                                            id="Shape"></path>
                                                                                                    <rect id="Rectangle-path"
                                                                                                            x="5.68803175"
                                                                                                            y="4.07969277"
                                                                                                            width="1"
                                                                                                            height="10.851496"></rect>
                                                                                                    <polygon id="Shape"
                                                                                                            points="4.4407619 14.9093755 3.79526984 4.07907831 3.16577778 4.12209036 3.81130159 14.9523876"></polygon>
                                                                                                    <polygon id="Shape"
                                                                                                            points="8.846 4.10089157 8.21650794 4.05852811 7.58022222 14.9100241 8.20971429 14.9523876"></polygon>
                                                                                                </g>
                                                                                            </g>
                                                                                        </g>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </svg>
                                                            </a>
                                                            <a href="#" class="hiddenEmail">
                                                                <svg width="26px" height="18px" viewBox="0 0 26 18">
                                                                    <g id="Page-1" stroke="none" stroke-width="1"
                                                                            fill="none" fill-rule="evenodd">
                                                                        <g class="re"
                                                                                transform="translate(-640.000000, -315.000000)"
                                                                                fill-rule="nonzero">
                                                                            <g id="Group-15"
                                                                                    transform="translate(280.000000, 151.000000)">
                                                                                <g id="about"
                                                                                        transform="translate(0.000000, 26.000000)">
                                                                                    <g id="Group-2"
                                                                                            transform="translate(19.000000, 54.000000)">
                                                                                        <g id="Group-19"
                                                                                                transform="translate(1.000000, 49.000000)">
                                                                                            <g id="1490919889_icon-21-eye-hidden"
                                                                                                    transform="translate(340.000000, 35.000000)">
                                                                                                <g id="Group">
                                                                                                    <g id="Page-1">
                                                                                                        <g id="icon-21-eye-hidden">
                                                                                                            <path d="M5.10869891,13.8913011 C1.61720816,11.8301147 0,9 0,9 C0,9 4,2 13,2 C14.3045107,2 15.5039752,2.14706466 16.6014388,2.39856122 L15.7519017,3.2480983 C14.8971484,3.0900546 13.9800929,3 13,3 C5,3 1.19995117,9 1.19995117,9 C1.19995117,9 2.71472808,11.3917225 5.84492713,13.1550729 L5.10869891,13.8913011 L5.10869891,13.8913011 L5.10869891,13.8913011 Z M9.398561,15.601439 C10.4960246,15.8529356 11.6954892,16.0000001 13,16 C22,15.999999 26,9 26,9 C26,9 24.3827918,6.1698856 20.8913008,4.1086992 L20.1550727,4.8449273 C23.2852719,6.6082776 24.8000488,9 24.8000488,9 C24.8000488,9 21,14.999999 13,15 C12.019907,15.0000001 11.1028515,14.9099455 10.2480981,14.7519019 L9.398561,15.601439 L9.398561,15.601439 L9.398561,15.601439 Z M16.8986531,8.1013469 C16.9649658,8.3902115 17,8.6910144 17,9 C17,11.2091391 15.2091391,13 13,13 C12.6910144,13 12.3902115,12.9649658 12.1013469,12.8986531 L13,12 C13.7677669,12.0000001 14.5355339,11.7071068 15.1213203,11.1213203 C15.7071068,10.5355339 16.0000001,9.7677669 16,9 L16.8986531,8.1013469 L16.8986531,8.1013469 L16.8986531,8.1013469 Z M13.8986531,5.1013469 C13.6097885,5.0350342 13.3089856,5 13,5 C10.7908609,5 9,6.7908609 9,9 C9,9.3089856 9.0350342,9.6097885 9.1013469,9.8986531 L10,9 C9.9999999,8.2322331 10.2928932,7.4644661 10.8786797,6.8786797 C11.4644661,6.2928932 12.2322331,5.9999999 13,6 L13.8986531,5.1013469 L13.8986531,5.1013469 L13.8986531,5.1013469 Z M4,17 L5,18 L22,1 L21,0 L4,17 Z"
                                                                                                                    id="eye-hidden"></path>
                                                                                                        </g>
                                                                                                    </g>
                                                                                                </g>
                                                                                            </g>
                                                                                        </g>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </svg>
                                                            </a>
                                                            <a href="#" class="red hidden-xs">Сделать логином для входа
                                                                в
                                                                аккаунт</a>
                                                        </div>
                                                    </div>
                                                    <div class="emailBlock 1">
                                                        <label for="email">Электронный адрес:</label><br>

                                                        <div class="inlineInput">
                                                            <input type="email" id="email">
                                                            <a href="#" class="removeEmail">
                                                                <svg width="14px" height="19px" viewBox="0 0 14 19">

                                                                    <g id="Page-1" stroke="none" stroke-width="1"
                                                                            fill="none" fill-rule="evenodd">
                                                                        <g class="re"
                                                                                transform="translate(-1090.000000, -370.000000)"
                                                                                fill-rule="nonzero" stroke-width="0.3">
                                                                            <g id="content"
                                                                                    transform="translate(280.000000, 151.000000)">
                                                                                <g id="Group-22"
                                                                                        transform="translate(20.000000, 89.000000)">
                                                                                    <g id="Group-20">
                                                                                        <g id="Group-4">
                                                                                            <g id="Group-24"
                                                                                                    transform="translate(770.000000, 26.000000)">
                                                                                                <g id="delete_icon"
                                                                                                        transform="translate(21.000000, 105.000000)">
                                                                                                    <path d="M11.9953016,2.04505221 L8.84168254,2.04505221 L8.84168254,1.36682731 C8.84168254,0.617769076 8.27685714,0.01037751 7.58022222,0.01037751 L4.42660317,0.01037751 C3.72996825,0.01037751 3.16514286,0.617769076 3.16514286,1.36682731 L3.16514286,2.04505221 L0.0115238095,2.04505221 L0.0115238095,2.72327711 L0.661365079,2.72327711 L1.29269841,15.6094478 C1.29269841,16.3585402 1.85752381,16.9658976 2.55415873,16.9658976 L9.49212698,16.9658976 C10.1887619,16.9658976 10.7535873,16.3585402 10.7535873,15.6094478 L11.3738413,2.72327711 L11.9953333,2.72327711 L11.9953333,2.04505221 L11.9953016,2.04505221 Z M3.79587302,1.36682731 C3.79587302,0.99262249 4.0792381,0.68860241 4.42660317,0.68860241 L7.58022222,0.68860241 C7.92822222,0.68860241 8.21095238,0.99262249 8.21095238,1.36682731 L8.21095238,2.04505221 L3.79587302,2.04505221 L3.79587302,1.36682731 Z M10.1240635,15.5743213 L10.1228254,15.5915602 L10.1228254,15.6094478 C10.1228254,15.983004 9.84009524,16.2876727 9.49209524,16.2876727 L2.55412698,16.2876727 C2.2067619,16.2876727 1.92339683,15.983004 1.92339683,15.6094478 L1.92339683,15.5915602 L1.92279365,15.5737068 L1.29269841,2.72327711 L10.7425079,2.72327711 L10.1240635,15.5743213 Z"
                                                                                                            id="Shape"></path>
                                                                                                    <rect id="Rectangle-path"
                                                                                                            x="5.68803175"
                                                                                                            y="4.07969277"
                                                                                                            width="1"
                                                                                                            height="10.851496"></rect>
                                                                                                    <polygon id="Shape"
                                                                                                            points="4.4407619 14.9093755 3.79526984 4.07907831 3.16577778 4.12209036 3.81130159 14.9523876"></polygon>
                                                                                                    <polygon id="Shape"
                                                                                                            points="8.846 4.10089157 8.21650794 4.05852811 7.58022222 14.9100241 8.20971429 14.9523876"></polygon>
                                                                                                </g>
                                                                                            </g>
                                                                                        </g>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </svg>
                                                            </a>
                                                            <a href="#" class="hiddenEmail">
                                                                <svg width="26px" height="18px" viewBox="0 0 26 18">
                                                                    <g id="Page-1" stroke="none" stroke-width="1"
                                                                            fill="none" fill-rule="evenodd">
                                                                        <g class="re"
                                                                                transform="translate(-640.000000, -315.000000)"
                                                                                fill-rule="nonzero">
                                                                            <g id="Group-15"
                                                                                    transform="translate(280.000000, 151.000000)">
                                                                                <g id="about"
                                                                                        transform="translate(0.000000, 26.000000)">
                                                                                    <g id="Group-2"
                                                                                            transform="translate(19.000000, 54.000000)">
                                                                                        <g id="Group-19"
                                                                                                transform="translate(1.000000, 49.000000)">
                                                                                            <g id="1490919889_icon-21-eye-hidden"
                                                                                                    transform="translate(340.000000, 35.000000)">
                                                                                                <g id="Group">
                                                                                                    <g id="Page-1">
                                                                                                        <g id="icon-21-eye-hidden">
                                                                                                            <path d="M5.10869891,13.8913011 C1.61720816,11.8301147 0,9 0,9 C0,9 4,2 13,2 C14.3045107,2 15.5039752,2.14706466 16.6014388,2.39856122 L15.7519017,3.2480983 C14.8971484,3.0900546 13.9800929,3 13,3 C5,3 1.19995117,9 1.19995117,9 C1.19995117,9 2.71472808,11.3917225 5.84492713,13.1550729 L5.10869891,13.8913011 L5.10869891,13.8913011 L5.10869891,13.8913011 Z M9.398561,15.601439 C10.4960246,15.8529356 11.6954892,16.0000001 13,16 C22,15.999999 26,9 26,9 C26,9 24.3827918,6.1698856 20.8913008,4.1086992 L20.1550727,4.8449273 C23.2852719,6.6082776 24.8000488,9 24.8000488,9 C24.8000488,9 21,14.999999 13,15 C12.019907,15.0000001 11.1028515,14.9099455 10.2480981,14.7519019 L9.398561,15.601439 L9.398561,15.601439 L9.398561,15.601439 Z M16.8986531,8.1013469 C16.9649658,8.3902115 17,8.6910144 17,9 C17,11.2091391 15.2091391,13 13,13 C12.6910144,13 12.3902115,12.9649658 12.1013469,12.8986531 L13,12 C13.7677669,12.0000001 14.5355339,11.7071068 15.1213203,11.1213203 C15.7071068,10.5355339 16.0000001,9.7677669 16,9 L16.8986531,8.1013469 L16.8986531,8.1013469 L16.8986531,8.1013469 Z M13.8986531,5.1013469 C13.6097885,5.0350342 13.3089856,5 13,5 C10.7908609,5 9,6.7908609 9,9 C9,9.3089856 9.0350342,9.6097885 9.1013469,9.8986531 L10,9 C9.9999999,8.2322331 10.2928932,7.4644661 10.8786797,6.8786797 C11.4644661,6.2928932 12.2322331,5.9999999 13,6 L13.8986531,5.1013469 L13.8986531,5.1013469 L13.8986531,5.1013469 Z M4,17 L5,18 L22,1 L21,0 L4,17 Z"
                                                                                                                    id="eye-hidden"></path>
                                                                                                        </g>
                                                                                                    </g>
                                                                                                </g>
                                                                                            </g>
                                                                                        </g>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </svg>
                                                            </a>
                                                            <a href="#" class="red hidden-xs">Сделать логином для входа
                                                                в
                                                                аккаунт</a>
                                                        </div>
                                                    </div>

                                                    <button type="submit" class="btn btn-success">Сохранить</button>
                                                    <br>

                                                    <a href="#" class="btn red addEmail">+ Добавить электронный
                                                        адрес</a>


                                                </div>
                                            </div>
                                            <div class="phone">
                                                <span><h4>Номера телефонов</h4></span>
                                                <div class="phoneForm styleInput">
                                                    <div class="blockNumber">
                                                        <label for="numberInput">Номер телефона:</label><br>
                                                        <div class="inlineBlock">
                                                            <input type="number" class="numberInput">
                                                            <a href="#" class="removeNumber">
                                                                <svg width="14px" height="19px" viewBox="0 0 14 19">

                                                                    <g id="Page-1" stroke="none" stroke-width="1"
                                                                            fill="none" fill-rule="evenodd">
                                                                        <g class="re"
                                                                                transform="translate(-1090.000000, -370.000000)"
                                                                                fill-rule="nonzero" stroke-width="0.3">
                                                                            <g id="content"
                                                                                    transform="translate(280.000000, 151.000000)">
                                                                                <g id="Group-22"
                                                                                        transform="translate(20.000000, 89.000000)">
                                                                                    <g id="Group-20">
                                                                                        <g id="Group-4">
                                                                                            <g id="Group-24"
                                                                                                    transform="translate(770.000000, 26.000000)">
                                                                                                <g id="delete_icon"
                                                                                                        transform="translate(21.000000, 105.000000)">
                                                                                                    <path d="M11.9953016,2.04505221 L8.84168254,2.04505221 L8.84168254,1.36682731 C8.84168254,0.617769076 8.27685714,0.01037751 7.58022222,0.01037751 L4.42660317,0.01037751 C3.72996825,0.01037751 3.16514286,0.617769076 3.16514286,1.36682731 L3.16514286,2.04505221 L0.0115238095,2.04505221 L0.0115238095,2.72327711 L0.661365079,2.72327711 L1.29269841,15.6094478 C1.29269841,16.3585402 1.85752381,16.9658976 2.55415873,16.9658976 L9.49212698,16.9658976 C10.1887619,16.9658976 10.7535873,16.3585402 10.7535873,15.6094478 L11.3738413,2.72327711 L11.9953333,2.72327711 L11.9953333,2.04505221 L11.9953016,2.04505221 Z M3.79587302,1.36682731 C3.79587302,0.99262249 4.0792381,0.68860241 4.42660317,0.68860241 L7.58022222,0.68860241 C7.92822222,0.68860241 8.21095238,0.99262249 8.21095238,1.36682731 L8.21095238,2.04505221 L3.79587302,2.04505221 L3.79587302,1.36682731 Z M10.1240635,15.5743213 L10.1228254,15.5915602 L10.1228254,15.6094478 C10.1228254,15.983004 9.84009524,16.2876727 9.49209524,16.2876727 L2.55412698,16.2876727 C2.2067619,16.2876727 1.92339683,15.983004 1.92339683,15.6094478 L1.92339683,15.5915602 L1.92279365,15.5737068 L1.29269841,2.72327711 L10.7425079,2.72327711 L10.1240635,15.5743213 Z"
                                                                                                            id="Shape"></path>
                                                                                                    <rect id="Rectangle-path"
                                                                                                            x="5.68803175"
                                                                                                            y="4.07969277"
                                                                                                            width="1"
                                                                                                            height="10.851496"></rect>
                                                                                                    <polygon id="Shape"
                                                                                                            points="4.4407619 14.9093755 3.79526984 4.07907831 3.16577778 4.12209036 3.81130159 14.9523876"></polygon>
                                                                                                    <polygon id="Shape"
                                                                                                            points="8.846 4.10089157 8.21650794 4.05852811 7.58022222 14.9100241 8.20971429 14.9523876"></polygon>
                                                                                                </g>
                                                                                            </g>
                                                                                        </g>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </svg>
                                                            </a>
                                                            <a href="#" class="lockNumber">
                                                                <svg width="19px" height="24px" viewBox="0 0 19 24">
                                                                    <g id="Page-1" stroke="none" stroke-width="1"
                                                                            fill="none" fill-rule="evenodd">
                                                                        <g class="rem"
                                                                                transform="translate(-637.000000, -572.000000)">
                                                                            <g id="Group-15"
                                                                                    transform="translate(280.000000, 151.000000)">
                                                                                <g id="about"
                                                                                        transform="translate(0.000000, 26.000000)">
                                                                                    <g id="Group-2-Copy"
                                                                                            transform="translate(0.000000, 323.000000)">
                                                                                        <g id="Group-2"
                                                                                                transform="translate(19.000000, 44.000000)">
                                                                                            <g id="Group-19-Copy-7"
                                                                                                    transform="translate(1.000000, 0.000000)">
                                                                                                <g id="Group-8"
                                                                                                        transform="translate(337.000000, 29.000000)">
                                                                                                    <rect id="Rectangle-4"
                                                                                                            x="0.5"
                                                                                                            y="9.5"
                                                                                                            width="18"
                                                                                                            height="12.8181818"
                                                                                                            rx="2"></rect>
                                                                                                    <circle id="Oval-4"
                                                                                                            cx="9.5"
                                                                                                            cy="15"
                                                                                                            r="1"></circle>
                                                                                                    <path d="M9.5,16.5 L9.5,18.5"
                                                                                                            id="Line"
                                                                                                            stroke-linecap="square"></path>
                                                                                                    <path d="M15.6000004,9.6711945 L15.6000004,6.49780094 L15.6000004,6.30000019 C15.6000004,2.79204554 12.7363639,0 9.30000019,0 C5.86363645,0 3,2.79204554 3,6.30000019"
                                                                                                            id="Shape"
                                                                                                            fill-rule="nonzero"></path>
                                                                                                </g>
                                                                                            </g>
                                                                                        </g>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </svg>
                                                            </a>
                                                        </div>
                                                        <p class="sendConfirm red">Выслать код подтверждения</p>
                                                        <label for="">К этому номеру привязан:</label>
                                                        <div class="inlineCheck">
                                                            <div class="blockCheck viber">
                                                                <img src="/img/editProfile/viber.svg" alt="">
                                                                <label for="viber">Viber</label>
                                                                <input type="checkbox" id="viber">
                                                            </div>
                                                            <div class="blockCheck app">
                                                                <img src="/img/editProfile/whatsup.svg" alt="">

                                                                <label for="app">WhatsApp</label>
                                                                <input type="checkbox" id="app">
                                                            </div>
                                                            <div class="blockCheck telegram">
                                                                <img src="/img/editProfile/telegram.svg" alt="">
                                                                <label for="telegram">Telegram</label>
                                                                <input type="checkbox" id="telegram">
                                                            </div>


                                                        </div>
                                                        <p class="sendCod red">+ Добавить номер телефона</p>


                                                        <label for="nameUserNumber" class="emailUser">Имя
                                                            пользователя:<br>
                                                            <input type="email" id="nameUserNumber"
                                                                    placeholder="URL страницы">
                                                        </label>


                                                    </div>
                                                </div>
                                            </div>
                                            <div class="blockTwoBtn">
                                                <button type="submit" class="btn greenBtn">Сохранить</button>
                                                <button type="reset" class="btn redBtn">Отменить</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade " id="fotoVideo">
                                    <!--                фото відео-->
                                    <div class="row fotoVideo allBlockContent">
                                        <form action="" method="post">
                                            <div class="col-sm-12">

                                                <!--                        foto-->
                                                <div class="row foto">

                                                    <div class="col-xs-12">
                                                        <h4>Текущие фото в профиле
                                                            <span class="popupHover"
                                                                    role="button"
                                                                    data-toggle="popover"
                                                                    data-trigger="hover"
                                                                    data-container="body"
                                                                    data-html="true"
                                                                    data-placement="auto"
                                                                    data-content="<p>Важно, чтобы в фотографиях был ваш портрет. Вот несколько причин:  </p>
                                      <ul>
                                        <li>Проект построен на доверии - снимать или сдавать вещи  проще, когда видишь глаза будущего партнера.</li>
                                        <li>При встрече, вам будет проще узнать  друг-друга.</li>

                                      </ul>"
                                                                    data-template='<div class="popover speedBlockHover fotoBlockHover" role="tooltip">
                                 <div class="arrow" style="top: 25%;"></div>
                                 <div class="popover-content"></div>
                                 </div>'>

                                                            <svg class="fotoHover" width="20px" height="20px"
                                                                    viewBox="0 0 20 20">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="-UI14.1----Редактировать-профиль---Фото-и-видео" transform="translate(-232.000000, -261.000000)">
            <g id="Group-2" transform="translate(0.000000, 199.000000)">
                <g id="inff" transform="translate(233.000000, 63.000000)">
                    <path d="M8.8,0 C4,0 0,3.9 0,8.8 C0,13.7 3.9,17.6 8.8,17.6 C13.7,17.6 17.6,13.7 17.6,8.8 C17.6,3.9 13.6,0 8.8,0 L8.8,0 Z"
                            id="Shape" fill-rule="nonzero"></path>
                    <g id="Group-27" transform="translate(7.000000, 4.000000)">
                        <path d="M1.9,0 C2.5,0 2.9,0.5 2.9,1 C2.9,1.6 2.4,2 1.9,2 C1.3,2 0.9,1.5 0.9,1 C0.8,0.4 1.3,0 1.9,0 L1.9,0 Z"
                                id="Path"></path>
                        <path d="M3.4,8.9 L0.8,8.9 C0.4,8.9 0,8.6 0,8.1 C0,7.6 0.3,7.3 0.8,7.3 L1.4,7.3 L1.4,4.6 L0.8,4.6 C0.4,4.6 0,4.3 0,3.8 C0,3.3 0.3,3 0.8,3 L2.1,3 C2.5,3 2.9,3.3 2.9,3.8 L2.9,7.4 L3.5,7.4 C3.9,7.4 4.3,7.7 4.3,8.2 C4.3,8.7 3.8,8.9 3.4,8.9 L3.4,8.9 Z"
                                id="Path"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                            </span>
                                                        </h4>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                    <div class="col-md-6 col-xs-12 blockForImgs ">
                                                        <div class="row">
                                                            <div class="col-sm-4 col-xs-6">
                                                                <div class="blockImg">
                                                                    <img src="/img/account/foto.png" alt="">
                                                                    <span class="blockHoverImg">
                                            <span class="glyphicon glyphicon-remove"></span>
                                            <p>Сделать основной</p>
                                        </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4 col-xs-6">
                                                                <div class="blockImg">
                                                                    <img src="/img/account/foto.png" alt="">
                                                                    <span class="blockHoverImg">
                                            <span class="glyphicon glyphicon-remove"></span>
                                            <p>Сделать основной</p>
                                        </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4 col-xs-6">
                                                                <div class="blockImg">
                                                                    <img src="/img/account/foto.png" alt="">
                                                                    <span class="blockHoverImg">
                                            <span class="glyphicon glyphicon-remove"></span>
                                            <p>Сделать основной</p>
                                        </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4 col-xs-6">
                                                                <div class="blockImg">
                                                                    <img src="/img/account/foto.png" alt="">
                                                                    <span class="blockHoverImg">
                                            <span class="glyphicon glyphicon-remove"></span>
                                            <p>Сделать основной</p>
                                        </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4 col-xs-6">
                                                                <div class="blockImg">
                                                                    <img src="/img/account/foto.png" alt="">
                                                                    <span class="blockHoverImg">
                                            <span class="glyphicon glyphicon-remove"></span>
                                            <p>Сделать основной</p>
                                        </span>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>

                                                    <div class="col-md-6 col-xs-12  ">
                                                        <div class="blockAddFoto text-center hidden">
                                                            <a href="#" class=""> + <br>У вас нет загруженых фото</a>

                                                        </div>

                                                    </div>
                                                    <div class="clearfix"></div>

                                                    <div class="col-sm-6 col-xs-12">
                                                        <a type="button" data-toggle="modal" data-target="#createFoto"
                                                                class="btnFoto btn text-center">Добавить фото</a>

                                                    </div>

                                                </div>


                                                <!--                        video-->
                                                <div class="row video">
                                                    <span class="leftText"><h4>Видео обращение</h4></span>
                                                    <div class="col-sm-6 col-xs-12">
                                                        <div class="embed-responsive embed-responsive-16by9">
                                                            <iframe src="https://www.youtube.com/embed/P5csD0SX8jI"
                                                                    frameborder="0"
                                                                    allowfullscreen></iframe>
                                                        </div>


                                                        <div class="blockAddVideo text-center hidden">
                                                            <a href="#" class="" data-toggle="modal"
                                                                    data-target="#createVideo"> + <br>У вас нет
                                                                загруженых видео</a>

                                                        </div>


                                                        <div class="row btnVideo">
                                                            <a href="#" class="btn btn-block web" data-toggle="modal"
                                                                    data-target="#createVideo">Заменить видео</a>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="blockTwoBtn">
                                                    <button type="submit" class="btn greenBtn">Сохранить</button>
                                                    <button type="reset" class="btn redBtn">Отменить</button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade " id="trust">
                                    <!--                верификация-->
                                    <div class="row trust allBlockContent">
                                        <div class="col-sm-12">
                                            <div class="topTrust">
                                                <h4 class="text-left">Доверие и верификация</h4>
                                                <p class="subText">В вашем профиле будут отображаться только общие
                                                    данные о
                                                    наличии
                                                    верификации.
                                                    Что это означает? Ваши данные, указанные для верификации, не будут
                                                    разглашаться - будут
                                                    приведены только общие сведения о наличии или отсутствии данных. Чем
                                                    больше этапов
                                                    верификации вы пройдете, тем большее к вам будет доверие среди
                                                    пользователей.
                                                </p>
                                                <a href="#" class="redLinck">Как это выглядит в публичном профиле?</a>
                                            </div>

                                            <div class="row styp">
                                                <span class="textStyp"><h4>Текущая степень доверия</h4></span>

                                                <div class="col-sm-6">
                                                    <p class="text-left boldTopText">Частично подтвержден</p>
                                                    <div class="lineRating"></div>

                                                    <p class="text-left textForAccordion">Чтобы повысить доверие к
                                                        профилю
                                                        до 100%,
                                                        нужно:</p>
                                                    <!--                                ACCORDION -->
                                                    <div class="panel-group" id="accordion" role="tablist"
                                                            aria-multiselectable="true">


                                                        <!--                                        email -->
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingOne">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse"
                                                                            data-parent="#accordion"
                                                                            href="#collapseOne"
                                                                            aria-expanded="true"
                                                                            aria-controls="collapseOne">
                                                    <span class="leftTitle">
                                                        Привязать электронный адрес <span class="redText">10%</span>
                                                    </span>
                                                                        <span class="rightTitle">
                                                        <svg width="20px" height="20px" viewBox="0 0 20 20"
                                                                version="1.1">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="dfg" transform="translate(-689.000000, -4402.000000)" fill-rule="nonzero">
            <g id="Group-15-Copy-5" transform="translate(280.000000, 3102.000000)">
                <g id="1491442848_payment_method_card_visa" transform="translate(250.000000, 1291.000000)">
                    <g id="Group" transform="translate(160.000000, 10.000000)">
                        <path d="M8.8,0 C4,0 0,3.9 0,8.8 C0,13.7 3.9,17.6 8.8,17.6 C13.7,17.6 17.6,13.7 17.6,8.8 C17.6,3.9 13.6,0 8.8,0 L8.8,0 Z"
                                id="Shape" stroke="#AFAFAF"></path>
                        <path d="M9.65,8.95 L11.75,6.85 C11.95,6.65 11.95,6.35 11.75,6.15 C11.55,5.95 11.25,5.95 11.05,6.15 L8.95,8.25 L6.85,6.15 C6.65,5.95 6.35,5.95 6.15,6.15 C5.95,6.35 5.95,6.65 6.15,6.85 L8.25,8.95 L6.15,11.05 C5.95,11.25 5.95,11.55 6.15,11.75 C6.25,11.85 6.35,11.85 6.55,11.85 C6.75,11.85 6.85,11.85 6.95,11.75 L9.05,9.65 L11.15,11.75 C11.25,11.85 11.35,11.85 11.55,11.85 C11.75,11.85 11.85,11.85 11.95,11.75 C12.15,11.55 12.15,11.25 11.95,11.05 L9.65,8.95 Z"
                                id="Shape" fill="#E35F46"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                                    </span>

                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne" class="panel-collapse collapse in"
                                                                    role="tabpanel"
                                                                    aria-labelledby="headingOne">
                                                                <div class="panel-body">
                                                                    content
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <!--                                    phone Number-->
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingTwo">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" data-toggle="collapse"
                                                                            data-parent="#accordion"
                                                                            href="#collapseTwo" aria-expanded="false"
                                                                            aria-controls="collapseTwo">
                                                    <span class="leftTitle">
                                                         Привязать номер мобильного телефона <span
                                                                class="redText">10%</span>
                                                    </span>
                                                                        <span class="rightTitle">
                                                        <svg width="20px" height="20px" viewBox="0 0 20 20"
                                                                version="1.1">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="dfg" transform="translate(-689.000000, -4402.000000)" fill-rule="nonzero">
            <g id="Group-15-Copy-5" transform="translate(280.000000, 3102.000000)">
                <g id="1491442848_payment_method_card_visa" transform="translate(250.000000, 1291.000000)">
                    <g id="Group" transform="translate(160.000000, 10.000000)">
                        <path d="M8.8,0 C4,0 0,3.9 0,8.8 C0,13.7 3.9,17.6 8.8,17.6 C13.7,17.6 17.6,13.7 17.6,8.8 C17.6,3.9 13.6,0 8.8,0 L8.8,0 Z"
                                id="Shape" stroke="#AFAFAF"></path>
                        <path d="M9.65,8.95 L11.75,6.85 C11.95,6.65 11.95,6.35 11.75,6.15 C11.55,5.95 11.25,5.95 11.05,6.15 L8.95,8.25 L6.85,6.15 C6.65,5.95 6.35,5.95 6.15,6.15 C5.95,6.35 5.95,6.65 6.15,6.85 L8.25,8.95 L6.15,11.05 C5.95,11.25 5.95,11.55 6.15,11.75 C6.25,11.85 6.35,11.85 6.55,11.85 C6.75,11.85 6.85,11.85 6.95,11.75 L9.05,9.65 L11.15,11.75 C11.25,11.85 11.35,11.85 11.55,11.85 C11.75,11.85 11.85,11.85 11.95,11.75 C12.15,11.55 12.15,11.25 11.95,11.05 L9.65,8.95 Z"
                                id="Shape" fill="#E35F46"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                                    </span>

                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseTwo" class="panel-collapse collapse"
                                                                    role="tabpanel"
                                                                    aria-labelledby="headingTwo">
                                                                <div class="panel-body">
                                                                    content
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <!--                                    google Plus-->
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingThree">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" data-toggle="collapse"
                                                                            data-parent="#accordion"
                                                                            href="#collapseThree" aria-expanded="false"
                                                                            aria-controls="collapseThree">
                                                    <span class="leftTitle">
                                                         Связать свой аккаунт с Google Plus <span
                                                                class="redText">10%</span>
                                                    </span>
                                                                        <span class="rightTitle">
                                                        <svg width="20px" height="20px" viewBox="0 0 20 20"
                                                                version="1.1">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="dfg" transform="translate(-689.000000, -4402.000000)" fill-rule="nonzero">
            <g id="Group-15-Copy-5" transform="translate(280.000000, 3102.000000)">
                <g id="1491442848_payment_method_card_visa" transform="translate(250.000000, 1291.000000)">
                    <g id="Group" transform="translate(160.000000, 10.000000)">
                        <path d="M8.8,0 C4,0 0,3.9 0,8.8 C0,13.7 3.9,17.6 8.8,17.6 C13.7,17.6 17.6,13.7 17.6,8.8 C17.6,3.9 13.6,0 8.8,0 L8.8,0 Z"
                                id="Shape" stroke="#AFAFAF"></path>
                        <path d="M9.65,8.95 L11.75,6.85 C11.95,6.65 11.95,6.35 11.75,6.15 C11.55,5.95 11.25,5.95 11.05,6.15 L8.95,8.25 L6.85,6.15 C6.65,5.95 6.35,5.95 6.15,6.15 C5.95,6.35 5.95,6.65 6.15,6.85 L8.25,8.95 L6.15,11.05 C5.95,11.25 5.95,11.55 6.15,11.75 C6.25,11.85 6.35,11.85 6.55,11.85 C6.75,11.85 6.85,11.85 6.95,11.75 L9.05,9.65 L11.15,11.75 C11.25,11.85 11.35,11.85 11.55,11.85 C11.75,11.85 11.85,11.85 11.95,11.75 C12.15,11.55 12.15,11.25 11.95,11.05 L9.65,8.95 Z"
                                id="Shape" fill="#E35F46"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                                    </span>

                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseThree" class="panel-collapse collapse"
                                                                    role="tabpanel"
                                                                    aria-labelledby="headingThree">
                                                                <div class="panel-body">
                                                                    <span class="text-left topText">Привязав к Google Plus вы:</span>
                                                                    <ul class="listGoogle">
                                                                        <li>Повысите доверие к своему профилю</li>
                                                                        <li>Найдете друзей</li>
                                                                        <li>А еще сможете быстро авторизироваться в
                                                                            сервисе
                                                                        </li>
                                                                    </ul>
                                                                    <div class="blockResultSeccess">
                                                                        <span class="text-left successReg">Вы успешно привязали следующий профиль:</span>
                                                                        <span class="email">vitaliirazgulin@gmail.com</span>
                                                                        <div class="inlineButt">
                                                                            <a href="#">Изменить</a>
                                                                            <a href="#">Посмотреть</a>
                                                                            <a href="#">Отвязать</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="blockResultError" style="display: none">
                                                                        <span class="text-left">Вы всегда сможете отвязать Google Plus</span>
                                                                        <a href="#" class="btn greenBtn">Привязать</a>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>


                                                        <!--                                    facebook  -->
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingFor">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" data-toggle="collapse"
                                                                            data-parent="#accordion"
                                                                            href="#collapseFor" aria-expanded="false"
                                                                            aria-controls="collapseFor">
                                                    <span class="leftTitle">Связать свой аккаунт с Facebook <span
                                                                class="redText">10%</span></span>
                                                                        <span class="rightTitle"><svg width="20px"
                                                                                    height="20px"
                                                                                    viewBox="0 0 20 20"
                                                                                    version="1.1">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="dfg" transform="translate(-689.000000, -4402.000000)" fill-rule="nonzero">
            <g id="Group-15-Copy-5" transform="translate(280.000000, 3102.000000)">
                <g id="1491442848_payment_method_card_visa" transform="translate(250.000000, 1291.000000)">
                    <g id="Group" transform="translate(160.000000, 10.000000)">
                        <path d="M8.8,0 C4,0 0,3.9 0,8.8 C0,13.7 3.9,17.6 8.8,17.6 C13.7,17.6 17.6,13.7 17.6,8.8 C17.6,3.9 13.6,0 8.8,0 L8.8,0 Z"
                                id="Shape" stroke="#AFAFAF"></path>
                        <path d="M9.65,8.95 L11.75,6.85 C11.95,6.65 11.95,6.35 11.75,6.15 C11.55,5.95 11.25,5.95 11.05,6.15 L8.95,8.25 L6.85,6.15 C6.65,5.95 6.35,5.95 6.15,6.15 C5.95,6.35 5.95,6.65 6.15,6.85 L8.25,8.95 L6.15,11.05 C5.95,11.25 5.95,11.55 6.15,11.75 C6.25,11.85 6.35,11.85 6.55,11.85 C6.75,11.85 6.85,11.85 6.95,11.75 L9.05,9.65 L11.15,11.75 C11.25,11.85 11.35,11.85 11.55,11.85 C11.75,11.85 11.85,11.85 11.95,11.75 C12.15,11.55 12.15,11.25 11.95,11.05 L9.65,8.95 Z"
                                id="Shape" fill="#E35F46"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span>

                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseFor" class="panel-collapse collapse"
                                                                    role="tabpanel"
                                                                    aria-labelledby="headingFor">
                                                                <div class="panel-body">
                                                                    <span class="text-left topText">Привязав к Google Plus вы:</span>
                                                                    <ul class="listGoogle">
                                                                        <li>Повысите доверие к своему профилю</li>
                                                                        <li>Найдете друзей</li>
                                                                        <li>А еще сможете быстро авторизироваться в
                                                                            сервисе
                                                                        </li>
                                                                    </ul>
                                                                    <div class="blockResultSeccess"
                                                                            style="display: none">
                                                                        <span class="text-left successReg">Вы успешно привязали следующий профиль:</span>
                                                                        <span class="email">vitaliirazgulin@gmail.com</span>
                                                                        <div class="inlineButt">
                                                                            <a href="#">Изменить</a>
                                                                            <a href="#">Посмотреть</a>
                                                                            <a href="#">Отвязать</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="blockResultError">
                                                                        <span class="text-left">Вы всегда сможете отвязать Facebook</span>
                                                                        <a href="#" class="btn greenBtn">Привязать</a>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>


                                                        <!--                                    Twitter-->
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingFive">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" data-toggle="collapse"
                                                                            data-parent="#accordion"
                                                                            href="#collapseFive" aria-expanded="false"
                                                                            aria-controls="collapseFive">
                                                    <span class="leftTitle">
                                                        Связать свой аккаунт с Twitter <span class="redText">10%</span>
                                                    </span>
                                                                        <span class="rightTitle"><svg width="20px"
                                                                                    height="20px"
                                                                                    viewBox="0 0 20 20"
                                                                                    version="1.1">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="dfg" transform="translate(-689.000000, -4402.000000)" fill-rule="nonzero">
            <g id="Group-15-Copy-5" transform="translate(280.000000, 3102.000000)">
                <g id="1491442848_payment_method_card_visa" transform="translate(250.000000, 1291.000000)">
                    <g id="Group" transform="translate(160.000000, 10.000000)">
                        <path d="M8.8,0 C4,0 0,3.9 0,8.8 C0,13.7 3.9,17.6 8.8,17.6 C13.7,17.6 17.6,13.7 17.6,8.8 C17.6,3.9 13.6,0 8.8,0 L8.8,0 Z"
                                id="Shape" stroke="#AFAFAF"></path>
                        <path d="M9.65,8.95 L11.75,6.85 C11.95,6.65 11.95,6.35 11.75,6.15 C11.55,5.95 11.25,5.95 11.05,6.15 L8.95,8.25 L6.85,6.15 C6.65,5.95 6.35,5.95 6.15,6.15 C5.95,6.35 5.95,6.65 6.15,6.85 L8.25,8.95 L6.15,11.05 C5.95,11.25 5.95,11.55 6.15,11.75 C6.25,11.85 6.35,11.85 6.55,11.85 C6.75,11.85 6.85,11.85 6.95,11.75 L9.05,9.65 L11.15,11.75 C11.25,11.85 11.35,11.85 11.55,11.85 C11.75,11.85 11.85,11.85 11.95,11.75 C12.15,11.55 12.15,11.25 11.95,11.05 L9.65,8.95 Z"
                                id="Shape" fill="#E35F46"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span>

                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseFive" class="panel-collapse collapse"
                                                                    role="tabpanel"
                                                                    aria-labelledby="headingFive">
                                                                <div class="panel-body">
                                                                    <span class="text-left topText">Привязав к Google Plus вы:</span>
                                                                    <ul class="listGoogle">
                                                                        <li>Повысите доверие к своему профилю</li>
                                                                        <li>Найдете друзей</li>
                                                                        <li>А еще сможете быстро авторизироваться в
                                                                            сервисе
                                                                        </li>
                                                                    </ul>
                                                                    <div class="blockResultSeccess"
                                                                            style="display: none">
                                                                        <span class="text-left successReg">Вы успешно привязали следующий профиль:</span>
                                                                        <span class="email">vitaliirazgulin@gmail.com</span>
                                                                        <div class="inlineButt">
                                                                            <a href="#">Изменить</a>
                                                                            <a href="#">Посмотреть</a>
                                                                            <a href="#">Отвязать</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="blockResultError">
                                                                        <span class="text-left">Вы всегда сможете отвязать Twitter</span>
                                                                        <a href="#" class="btn greenBtn">Привязать</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <!--                                    manu Card-->
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingSix">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" data-toggle="collapse"
                                                                            data-parent="#accordion"
                                                                            href="#collapseSix" aria-expanded="false"
                                                                            aria-controls="collapseSix">
                                                    <span class="leftTitle">
                                                         Привязать платежный инструмент <span class="redText">10%</span>
                                                    </span>
                                                                        <span class="rightTitle">
                                                        <svg width="20px" height="20px" viewBox="0 0 20 20"
                                                                version="1.1">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="dfg" transform="translate(-689.000000, -4402.000000)" fill-rule="nonzero">
            <g id="Group-15-Copy-5" transform="translate(280.000000, 3102.000000)">
                <g id="1491442848_payment_method_card_visa" transform="translate(250.000000, 1291.000000)">
                    <g id="Group" transform="translate(160.000000, 10.000000)">
                        <path d="M8.8,0 C4,0 0,3.9 0,8.8 C0,13.7 3.9,17.6 8.8,17.6 C13.7,17.6 17.6,13.7 17.6,8.8 C17.6,3.9 13.6,0 8.8,0 L8.8,0 Z"
                                id="Shape" stroke="#AFAFAF"></path>
                        <path d="M9.65,8.95 L11.75,6.85 C11.95,6.65 11.95,6.35 11.75,6.15 C11.55,5.95 11.25,5.95 11.05,6.15 L8.95,8.25 L6.85,6.15 C6.65,5.95 6.35,5.95 6.15,6.15 C5.95,6.35 5.95,6.65 6.15,6.85 L8.25,8.95 L6.15,11.05 C5.95,11.25 5.95,11.55 6.15,11.75 C6.25,11.85 6.35,11.85 6.55,11.85 C6.75,11.85 6.85,11.85 6.95,11.75 L9.05,9.65 L11.15,11.75 C11.25,11.85 11.35,11.85 11.55,11.85 C11.75,11.85 11.85,11.85 11.95,11.75 C12.15,11.55 12.15,11.25 11.95,11.05 L9.65,8.95 Z"
                                id="Shape" fill="#E35F46"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                                    </span>

                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseSix" class="panel-collapse collapse"
                                                                    role="tabpanel"
                                                                    aria-labelledby="headingSix">
                                                                <div class="panel-body">
                                                                    <p class="text-left topText">Привязанные платежные
                                                                        инструменты упростят
                                                                        аренду и получение дохода в дальнейшем.</p>
                                                                    <p class="text-left topText">Но вы всегда сможете
                                                                        сами
                                                                        выбрать с какой
                                                                        карты оплачивать аренду или куда получать
                                                                        доход.</p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <!--                                    document -->
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingSeven">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" data-toggle="collapse"
                                                                            data-parent="#accordion"
                                                                            href="#collapseSeven" aria-expanded="false"
                                                                            aria-controls="collapseSeven">
                                                    <span class="leftTitle">
                                                        Отправить сканы документов <span class="redText">10%</span>
                                                    </span>
                                                                        <span class="rightTitle">
                                                        <svg width="20px" height="20px" viewBox="0 0 20 20"
                                                                version="1.1">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="dfg" transform="translate(-689.000000, -4402.000000)" fill-rule="nonzero">
            <g id="Group-15-Copy-5" transform="translate(280.000000, 3102.000000)">
                <g id="1491442848_payment_method_card_visa" transform="translate(250.000000, 1291.000000)">
                    <g id="Group" transform="translate(160.000000, 10.000000)">
                        <path d="M8.8,0 C4,0 0,3.9 0,8.8 C0,13.7 3.9,17.6 8.8,17.6 C13.7,17.6 17.6,13.7 17.6,8.8 C17.6,3.9 13.6,0 8.8,0 L8.8,0 Z"
                                id="Shape" stroke="#AFAFAF"></path>
                        <path d="M9.65,8.95 L11.75,6.85 C11.95,6.65 11.95,6.35 11.75,6.15 C11.55,5.95 11.25,5.95 11.05,6.15 L8.95,8.25 L6.85,6.15 C6.65,5.95 6.35,5.95 6.15,6.15 C5.95,6.35 5.95,6.65 6.15,6.85 L8.25,8.95 L6.15,11.05 C5.95,11.25 5.95,11.55 6.15,11.75 C6.25,11.85 6.35,11.85 6.55,11.85 C6.75,11.85 6.85,11.85 6.95,11.75 L9.05,9.65 L11.15,11.75 C11.25,11.85 11.35,11.85 11.55,11.85 C11.75,11.85 11.85,11.85 11.95,11.75 C12.15,11.55 12.15,11.25 11.95,11.05 L9.65,8.95 Z"
                                id="Shape" fill="#E35F46"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                                    </span>

                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseSeven" class="panel-collapse collapse"
                                                                    role="tabpanel"
                                                                    aria-labelledby="headingSeven">
                                                                <div class="panel-body">
                                                                    <p class="text-left topText">Согласно
                                                                        законодательству
                                                                        многих стран, а
                                                                        так же требованию многих платежных систем, для
                                                                        проведения платежей
                                                                        мы обязаны запросить у вас сканы документов.</p>
                                                                    <p class="text-left topText">Согласно <a href="#"
                                                                                class="redText">Политике
                                                                            конфиденциальности</a>, сканы ваших
                                                                        документов
                                                                        хранятся на
                                                                        отдельном защищенном сервере.</p>
                                                                    <ul class="listDocument">
                                                                        <li>Удостоверение личности<br>
                                                                            <span class="greyText">Подходит паспорт или другой документ, удостоверяющий личность. Например, в некоторых странах вместо паспорта используются ID карты. </span>
                                                                            <form enctype="multipart/form-data"
                                                                                    method="post">
                                                                                <label for="loadImg" class="dashedBtn">
                                                                                    <svg class="strech" width="16px"
                                                                                            height="18px">
                                                                                        <g id="Page-1"
                                                                                                stroke="none"
                                                                                                stroke-width="1"
                                                                                                fill="none"
                                                                                                fill-rule="evenodd"
                                                                                                stroke-linecap="round"
                                                                                                stroke-linejoin="round">
                                                                                            <g id="strech"
                                                                                                    transform="translate(-566.000000, -5192.000000)">
                                                                                                <g id="Group-15-Copy-5"
                                                                                                        transform="translate(280.000000, 3102.000000)">
                                                                                                    <g id="Group-51"
                                                                                                            transform="translate(19.000000, 22.000000)">
                                                                                                        <g id="Group-21"
                                                                                                                transform="translate(0.000000, 165.000000)">
                                                                                                            <g id="Group-13-Copy-6"
                                                                                                                    transform="translate(207.000000, 1491.000000)">
                                                                                                                <g id="attach_icon"
                                                                                                                        transform="translate(68.214102, 421.227241) rotate(-330.000000) translate(-68.214102, -421.227241) translate(64.214102, 411.727241)">
                                                                                                                    <path d="M5.53191489,9.66105263 L5.53191489,16.2547368 C5.53191489,17.7536842 4.2893617,18.9831579 2.77446809,18.9831579 L2.77446809,18.9831579 C1.25957447,18.9831579 0.0170212766,17.7536842 0.0170212766,16.2547368 L0.0170212766,9.66105263"
                                                                                                                            id="Shape"></path>
                                                                                                                    <path d="M0.0170212766,10.3578947 L0.0170212766,3.95789474 C0.0170212766,1.79368421 1.81276596,0.0168421053 4,0.0168421053 L4,0.0168421053 C6.18723404,0.0168421053 7.98297872,1.79368421 7.98297872,3.95789474 L7.98297872,10.3578947"
                                                                                                                            id="Shape"></path>
                                                                                                                    <path d="M2.51914894,13.6 L2.51914894,4.42947368 C2.51914894,3.61263158 3.19148936,2.93894737 4.02553191,2.93894737 C4.85106383,2.93894737 5.53191489,3.60421053 5.53191489,4.42947368 L5.53191489,9.6"
                                                                                                                            id="Shape"></path>
                                                                                                                </g>
                                                                                                            </g>
                                                                                                        </g>
                                                                                                    </g>
                                                                                                </g>
                                                                                            </g>
                                                                                        </g>
                                                                                    </svg>&nbsp;Загрузить</label>
                                                                                <input type="file" name="photo" multiple
                                                                                        accept="application/pdf"
                                                                                        id="loadImg">
                                                                                <input type="submit" class="submitImg"
                                                                                        value="Отправить"
                                                                                        style="display: none">
                                                                            </form>
                                                                        </li>
                                                                        <li>Подтверждение адреса проживания</li>
                                                                        <li>Иной документ</li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                            <div class="blockTwoBtn">
                                                <button type="submit" class="btn greenBtn">Сохранить</button>
                                                <button type="reset" class="btn redBtn">Отменить</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade " id="reference">
                                    <!--                рекомендация-->
                                    <div class="row reference allBlockContent">
                                        <div class="col-sm-12">
                                            <h4 class="text-left">Рекомендации</h4>
                                            <p>
                                                <span class="redText">Наше сообщество основано на доверии и репутации.</span>
                                                Вы можете
                                                попросить своих
                                                знакомых написать вам рекомендации. Они будут показаны в вашем профиле и
                                                позволят другим
                                                участникам лучше вас узнать. Рекомендации нужно просить только у людей,
                                                которые хорошо вас
                                                знают.</p>
                                            <div class="row  invite">
                                                <div class="col-sm-4"><span class="leftText">Пригласить через</span>
                                                </div>
                                                <div class="col-sm-8"> 1 form</div>
                                            </div>

                                            <div class="row frends">
                                                <span class="textfrends"><h4>Ваши друзья</h4></span>
                                                <div class="col-sm-6">

                                                    <!--                                PANEL FEEDBACL -->
                                                    <div class="panel-group" id="accordion" role="tablist"
                                                            aria-multiselectable="true">

                                                        <!--                                    FIRST PANEL-->
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading"
                                                                    role="tab"
                                                                    id="headingOneFeedback">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse"
                                                                            data-parent="#accordion"
                                                                            href="#collapseOneFeedback"
                                                                            aria-expanded="true"
                                                                            aria-controls="collapseOneFeedback">
                                                    <span class="leftBlockTitle"><img src="/img/product/fotoFace.jpg"
                                                                alt="imgAccount">Доба Дмитрий</span>
                                                                        <span class="rightBlockTitle"></span>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOneFeedback"
                                                                    class="panel-collapse collapse in"
                                                                    role="tabpanel"
                                                                    aria-labelledby="headingOneFeedback">
                                                                <div class="panel-body">
                                                                    <div class="blockClient allBlock">
                                                    <span class="text-left" data-toggle="modal"
                                                            data-target=".feedbackFrends"><svg width="19px"
                                                                height="13px"
                                                                viewBox="0 0 19 13"
                                                                version="1.1">
    <defs>
        <polyline id="path-1" points="0 5.23350423 4.09188306 9.83430764 12 0"></polyline>
        <polyline id="path-2" points="5 5.23350423 9.09188306 9.83430764 17 0"></polyline>
    </defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="UI14.2-------Дизайн-подсказок-по-ховеру" transform="translate(-572.000000, -5841.000000)">
            <g id="Group-15-Copy-6" transform="translate(280.000000, 5436.000000)">
                <g id="Group-16">
                    <g id="button-copy-2" transform="translate(293.000000, 402.000000)">
                        <g id="Group-10">
                            <g id="Group-5">
                                <g id="Group-30" transform="translate(0.000000, 4.000000)">
                                    <g id="Path-2-Copy-2">
                                        <use xlink:href="#path-1"></use>
                                        <use stroke="#4DC97B" stroke-width="2" xlink:href="#path-1"></use>
                                    </g>
                                    <g id="Path-2-Copy">
                                        <use xlink:href="#path-2"></use>
                                        <use stroke="#4DC97B" stroke-width="2" xlink:href="#path-2"></use>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>&nbsp;Вы написали пользователю рекомендацию</span>
                                                                        <div class="inlineButt">
                                                                            <a href="#"
                                                                                    class="redLinck">Редактировать</a>
                                                                            <a href="#" class="redLinck">Удалить</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="blockUser allBlock">
                                                    <span class="text-left"><svg width="19px" height="13px"
                                                                viewBox="0 0 19 13" version="1.1">
    <defs>
        <polyline id="path-1" points="0 5.23350423 4.09188306 9.83430764 12 0"></polyline>
        <polyline id="path-2" points="5 5.23350423 9.09188306 9.83430764 17 0"></polyline>
    </defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="UI14.2-------Дизайн-подсказок-по-ховеру" transform="translate(-572.000000, -5841.000000)">
            <g id="Group-15-Copy-6" transform="translate(280.000000, 5436.000000)">
                <g id="Group-16">
                    <g id="button-copy-2" transform="translate(293.000000, 402.000000)">
                        <g id="Group-10">
                            <g id="Group-5">
                                <g id="Group-30" transform="translate(0.000000, 4.000000)">
                                    <g id="Path-2-Copy-2">
                                        <use xlink:href="#path-1"></use>
                                        <use stroke="#4DC97B" stroke-width="2" xlink:href="#path-1"></use>
                                    </g>
                                    <g id="Path-2-Copy">
                                        <use xlink:href="#path-2"></use>
                                        <use stroke="#4DC97B" stroke-width="2" xlink:href="#path-2"></use>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>&nbsp;Пользователь написал вам рекомендацию</span>
                                                                        <div class="inlineButt">
                                                                            <a href="#" class="redLinck">Посмотреть</a>
                                                                            <a href="#" class="redLinck">Публиковать</a>
                                                                            <a href="#" class="redLinck">Не
                                                                                публиковать</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <!--                                    TWO PANEL-->
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading"
                                                                    role="tab"
                                                                    id="headingTwoFeedback">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed"
                                                                            data-toggle="collapse"
                                                                            data-parent="#accordion"
                                                                            href="#collapseTwoFeedback"
                                                                            aria-expanded="false"
                                                                            aria-controls="collapseTwoFeedback">
                                                    <span class="leftBlockTitle"><img src="/img/product/fotoFace.jpg"
                                                                alt="imgAccount">Доба Дмитрий</span>
                                                                        <span class="rightBlockTitle"></span>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseTwoFeedback"
                                                                    class="panel-collapse collapse"
                                                                    role="tabpanel"
                                                                    aria-labelledby="headingTwoFeedback">
                                                                <div class="panel-body">
                                                                    <div class="blockClient allBlock">
                                                    <span class="text-left" data-toggle="modal"
                                                            data-target=".feedbackFrends"><svg width="19px"
                                                                height="13px"
                                                                viewBox="0 0 19 13"
                                                                version="1.1">
    <defs>
        <polyline id="path-1" points="0 5.23350423 4.09188306 9.83430764 12 0"></polyline>
        <polyline id="path-2" points="5 5.23350423 9.09188306 9.83430764 17 0"></polyline>
    </defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="UI14.2-------Дизайн-подсказок-по-ховеру" transform="translate(-572.000000, -5841.000000)">
            <g id="Group-15-Copy-6" transform="translate(280.000000, 5436.000000)">
                <g id="Group-16">
                    <g id="button-copy-2" transform="translate(293.000000, 402.000000)">
                        <g id="Group-10">
                            <g id="Group-5">
                                <g id="Group-30" transform="translate(0.000000, 4.000000)">
                                    <g id="Path-2-Copy-2">
                                        <use xlink:href="#path-1"></use>
                                        <use stroke="#4DC97B" stroke-width="2" xlink:href="#path-1"></use>
                                    </g>
                                    <g id="Path-2-Copy">
                                        <use xlink:href="#path-2"></use>
                                        <use stroke="#4DC97B" stroke-width="2" xlink:href="#path-2"></use>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>&nbsp;Вы написали пользователю рекомендацию</span>
                                                                        <div class="inlineButt">
                                                                            <a href="#"
                                                                                    class="redLinck">Редактировать</a>
                                                                            <a href="#" class="redLinck">Удалить</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="blockUser allBlock">
                                                    <span class="text-left"><svg width="19px" height="13px"
                                                                viewBox="0 0 19 13" version="1.1">
    <defs>
        <polyline id="path-1" points="0 5.23350423 4.09188306 9.83430764 12 0"></polyline>
        <polyline id="path-2" points="5 5.23350423 9.09188306 9.83430764 17 0"></polyline>
    </defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="UI14.2-------Дизайн-подсказок-по-ховеру" transform="translate(-572.000000, -5841.000000)">
            <g id="Group-15-Copy-6" transform="translate(280.000000, 5436.000000)">
                <g id="Group-16">
                    <g id="button-copy-2" transform="translate(293.000000, 402.000000)">
                        <g id="Group-10">
                            <g id="Group-5">
                                <g id="Group-30" transform="translate(0.000000, 4.000000)">
                                    <g id="Path-2-Copy-2">
                                        <use xlink:href="#path-1"></use>
                                        <use stroke="#4DC97B" stroke-width="2" xlink:href="#path-1"></use>
                                    </g>
                                    <g id="Path-2-Copy">
                                        <use xlink:href="#path-2"></use>
                                        <use stroke="#4DC97B" stroke-width="2" xlink:href="#path-2"></use>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>&nbsp;Пользователь написал вам рекомендацию</span>
                                                                        <div class="inlineButt">
                                                                            <a href="#" class="redLinck">Посмотреть</a>
                                                                            <a href="#" class="redLinck">Публиковать</a>
                                                                            <a href="#" class="redLinck">Не
                                                                                публиковать</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <!--                                    TREE PANEL-->
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading"
                                                                    role="tab"
                                                                    id="headingThreeFeedback">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed"
                                                                            data-toggle="collapse"
                                                                            data-parent="#accordion"
                                                                            href="#collapseThreeFeedback"
                                                                            aria-expanded="false"
                                                                            aria-controls="collapseThreeFeedback">
                                                    <span class="leftBlockTitle"><img src="/img/product/fotoFace.jpg"
                                                                alt="imgAccount">Доба Дмитрий</span>
                                                                        <span class="rightBlockTitle"></span>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseThreeFeedback"
                                                                    class="panel-collapse collapse"
                                                                    role="tabpanel"
                                                                    aria-labelledby="headingThreeFeedback">
                                                                <div class="panel-body">
                                                                    <div class="blockClient allBlock">
                                                    <span class="text-left" data-toggle="modal"
                                                            data-target=".feedbackFrends"><svg width="19px"
                                                                height="13px"
                                                                viewBox="0 0 19 13"
                                                                version="1.1">
    <defs>
        <polyline id="path-1" points="0 5.23350423 4.09188306 9.83430764 12 0"></polyline>
        <polyline id="path-2" points="5 5.23350423 9.09188306 9.83430764 17 0"></polyline>
    </defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="UI14.2-------Дизайн-подсказок-по-ховеру" transform="translate(-572.000000, -5841.000000)">
            <g id="Group-15-Copy-6" transform="translate(280.000000, 5436.000000)">
                <g id="Group-16">
                    <g id="button-copy-2" transform="translate(293.000000, 402.000000)">
                        <g id="Group-10">
                            <g id="Group-5">
                                <g id="Group-30" transform="translate(0.000000, 4.000000)">
                                    <g id="Path-2-Copy-2">
                                        <use xlink:href="#path-1"></use>
                                        <use stroke="#4DC97B" stroke-width="2" xlink:href="#path-1"></use>
                                    </g>
                                    <g id="Path-2-Copy">
                                        <use xlink:href="#path-2"></use>
                                        <use stroke="#4DC97B" stroke-width="2" xlink:href="#path-2"></use>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>&nbsp;Вы написали пользователю рекомендацию</span>
                                                                        <div class="inlineButt">
                                                                            <a href="#"
                                                                                    class="redLinck">Редактировать</a>
                                                                            <a href="#" class="redLinck">Удалить</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="blockUser allBlock">
                                                    <span class="text-left"><svg width="19px" height="13px"
                                                                viewBox="0 0 19 13" version="1.1">
    <defs>
        <polyline id="path-1" points="0 5.23350423 4.09188306 9.83430764 12 0"></polyline>
        <polyline id="path-2" points="5 5.23350423 9.09188306 9.83430764 17 0"></polyline>
    </defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="UI14.2-------Дизайн-подсказок-по-ховеру" transform="translate(-572.000000, -5841.000000)">
            <g id="Group-15-Copy-6" transform="translate(280.000000, 5436.000000)">
                <g id="Group-16">
                    <g id="button-copy-2" transform="translate(293.000000, 402.000000)">
                        <g id="Group-10">
                            <g id="Group-5">
                                <g id="Group-30" transform="translate(0.000000, 4.000000)">
                                    <g id="Path-2-Copy-2">
                                        <use xlink:href="#path-1"></use>
                                        <use stroke="#4DC97B" stroke-width="2" xlink:href="#path-1"></use>
                                    </g>
                                    <g id="Path-2-Copy">
                                        <use xlink:href="#path-2"></use>
                                        <use stroke="#4DC97B" stroke-width="2" xlink:href="#path-2"></use>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>&nbsp;Пользователь написал вам рекомендацию</span>
                                                                        <div class="inlineButt">
                                                                            <a href="#" class="redLinck">Посмотреть</a>
                                                                            <a href="#" class="redLinck">Публиковать</a>
                                                                            <a href="#" class="redLinck">Не
                                                                                публиковать</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="blockTwoBtn">
                                                <button type="submit" class="btn greenBtn">Сохранить</button>
                                                <button type="reset" class="btn redBtn">Отменить</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <!--              business acoount              -->

                                <div role="tabpanel" class="tab-pane fade " id="aboutCompany">
                                    <!--                про компанію-->
                                    <div class="row reference allBlockContent">
                                        <div class="col-sm-12">
                                            <h4 class="text-left">Обязательное о компании</h4>
                                            <form action="" method="post" class="styleInput">
                                                <div class="nameCompany">
                                                    <label for="nameConpany">Название компании:<br>
                                                        <input type="text" id="nameConpany" placeholder="Ромашка">
                                                    </label>
                                                </div>

                                                <div class="row textAbout">
                                                    <div class="col-sm-6">
                                                        <label for="textAbout">Расскажите о компании:<br>
                                                            <textarea name="" id="textAbout"></textarea>
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6 sample">
                                                        <p>Проект построен на доверии</p>
                                                        <p>Раскажите о компании<br>
                                                            В чем ваше конкретное достоинство?<br>
                                                            Какие у вас ключевые принципы?<br>
                                                            Сколько людей в вашей компании?</p>
                                                    </div>
                                                </div>

                                                <div class="row requisites">
                                                    <div class="col-sm-6">
                                                        <label for="requisites">Реквизиты компании:<br>
                                                            <textarea name="" id="requisites"></textarea>
                                                        </label>
                                                    </div>
                                                    <div class="col-sm-6 sample">
                                                        <p>Проект построен на доверии</p>
                                                        <p>Раскажите о компании<br>
                                                            В чем ваше конкретное достоинство?<br>
                                                            Какие у вас ключевые принципы?<br>
                                                            Сколько людей в вашей компании?</p>
                                                    </div>
                                                </div>

                                                <div class="blockTwoBtn">
                                                    <button type="submit" class="btn greenBtn">Сохранить</button>
                                                    <button type="reset" class="btn redBtn">Отменить</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane fade " id="contactsCompany">
                                    <!--                контакти компанії-->
                                    <div class="row reference allBlockContent">
                                        <div class="col-sm-12">
                                            <form action="" method="post" class="styleInput">

                                                <!--  email company  -->
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="mail">
                                                            <h4 class="text-left">Электронный адрес</h4>
                                                            <label for="emailCompany">Укажите эл. адрес:<br>
                                                                <input type="email" placeholder="company@gmail.com"
                                                                        id="emailCompany">
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!--numberCompanu-->
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="numberPhoneCompany">
                                                            <h4 class="text-left">Номер телефона</h4>
                                                            <label for="">Введите номер телефона:<br>
                                                                <input type="tel" placeholder="+7">
                                                            </label>
                                                            <br>

                                                            <a class="createNumberCompany red">+ Добавить номер
                                                                телефона</a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- web-resours Company-->
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="site">
                                                            <h4 class="text-left">Веб сайт</h4>

                                                            <label for="webSite">Укажите веб сайт компании:<br>
                                                                <input type="url" id="webSite" placeholder="www.">
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!--  address company -->
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="addressCompany">
                                                            <h4 class="text-left">Адрес</h4>
                                                            <label for="addressCompany">Физический адрес офиса:<br>
                                                                <input type="text">
                                                            </label>

                                                            <div class="map">

                                                            </div>

                                                            <a class="addAddress text-left">+ Добавить адрес</a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="blockTwoBtn">
                                                    <button type="submit" class="btn greenBtn">Сохранить</button>
                                                    <button type="reset" class="btn redBtn">Отменить</button>
                                                </div>

                                            </form>


                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane fade " id="payCompany">
                                    <!--                оплата компанії-->
                                    <div class="row reference allBlockContent">
                                        <form action="" method="post" class="styleInput">
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12 listPlan">
                                                    <h4 class="text-left">Выбрать план:</h4>
                                                    <label for="monthPay"><input type="radio" id="monthPay">&nbsp;Ежемесячная
                                                        оплата - <span class="red">660 рублей </span>/ в
                                                        месяц</label><br>
                                                    <label for="periodPay"><input type="radio" id="periodPay">&nbsp;Оптлата
                                                        6
                                                        месяцев - <span class="red">3500 рублей </span>/ за 6 месяцев
                                                        (экономия
                                                        10%) </label><br>
                                                    <label for="yearPay"><input type="radio" id="yearPay">&nbsp;Оплата 1
                                                        год
                                                        -
                                                        <span class="red">6300 рублей  </span>/ за 1 год (экономия
                                                        20%)</label><br>

                                                </div>
                                                <div class="col-sm-6 col-xs-12 text-right typeCurrency">
                                                    <p class="time">
                                                        <svg width="18px" height="18px" viewBox="0 0 18 18">
                                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                                    fill-rule="evenodd">
                                                                <g id="-UI14.12.5----Включить-и-редактировать-Бизнес-аккаунт"
                                                                        transform="translate(-1037.000000, -241.000000)"
                                                                        fill-rule="nonzero">
                                                                    <g id="Group-15"
                                                                            transform="translate(280.000000, 151.000000)">
                                                                        <g id="ok_green_fill"
                                                                                transform="translate(757.000000, 90.000000)">
                                                                            <path d="M8.8,0 C4,0 0,3.9 0,8.8 C0,13.7 3.9,17.6 8.8,17.6 C13.7,17.6 17.6,13.7 17.6,8.8 C17.6,3.9 13.6,0 8.8,0 L8.8,0 Z"
                                                                                    id="Shape" fill="#4DC97B"></path>
                                                                            <path d="M8.65,11.2656854 C8.55,11.2656854 8.35,11.1656854 8.25,11.0656854 L6.15,8.76568542 C5.95,8.56568542 5.95,8.26568542 6.15,8.06568542 C6.35,7.86568542 6.65,7.86568542 6.85,8.06568542 L8.65,9.96568542 L11.85,6.16568542 C12.05,5.96568542 12.35,5.96568542 12.55,6.06568542 C12.75,6.26568542 12.75,6.56568542 12.65,6.76568542 L9.05,10.9656854 C8.95,11.1656854 8.85,11.2656854 8.65,11.2656854 L8.65,11.2656854 Z"
                                                                                    id="Shape" fill="#FFFFFF"></path>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>&nbsp;Услуга оплачена до:
                                                        <span class="red">01.08.2017</span>
                                                    </p>


                                                    <div class="dropdown">
                                                        <p class="grey">Выбрать валюту оплаты:</p>
                                                        <button class="btn btn-default dropdown-toggle" type="button"
                                                                id="dropdownMenu1" data-toggle="dropdown"
                                                                aria-expanded="true">
                                                            Рубли
                                                            <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu"
                                                                aria-labelledby="dropdownMenu1">
                                                            <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                        href="#">Дія</a>
                                                            </li>
                                                            <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                        href="#">Інша
                                                                    дія</a></li>
                                                            <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                        href="#">Щось
                                                                    ще тут</a></li>
                                                            <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                        href="#">Відокремлений
                                                                    лінк</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="blockTwoBtn">
                                                <div class="row">
                                                    <div class="col-xs-6 col-xs-12"><label for="autoMailing"><input
                                                                    type="checkbox" id="autoMailing">&nbsp;Я согласен с
                                                            автоматической оплатой в дальнейшем</label>
                                                    </div>
                                                    <div class="col-xs-6 col-xs-12 text-right">
                                                        <div class="dropdown">
                                                            <button class="btn btn-default dropdown-toggle"
                                                                    type="button"
                                                                    id="dropdownMenu1"
                                                                    data-toggle="dropdown"
                                                                    aria-expanded="true">
                                                                Выбрать метод оплаты
                                                                <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu" role="menu"
                                                                    aria-labelledby="dropdownMenu1">
                                                                <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                            href="#">Дія</a></li>
                                                                <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                            href="#">Інша дія</a></li>
                                                                <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                            href="#">Щось ще тут</a></li>
                                                                <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                            href="#">Відокремлений лінк</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <a href="#" class="btn green">Оплатить</a>
                                                    </div>
                                                </div>

                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <!--                випадаюче вікно для створення\загрузки відео-->
                <div class="modal fade createVideo" id="createVideo" tabindex="3" role="dialog"
                        aria-labelledby="createVideoLabel"
                        aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">

                                    <svg width="8px" height="8px" viewBox="0 0 8 8">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
        <g id="UI15.1----Отзывы-(личный-кабинет)---дефолт" transform="translate(-525.000000, -774.000000)"
                stroke="#FFFFFF" fill="#FFFFFF">
            <g id="content" transform="translate(280.000000, 151.000000)">
                <g id="Group-22" transform="translate(20.000000, 89.000000)">
                    <g id="Group-20">
                        <g id="Group-19" transform="translate(0.000000, 204.000000)">
                            <g id="Group-25" transform="translate(70.000000, 273.000000)">
                                <g id="close" transform="translate(151.000000, 53.000000)">
                                    <g transform="translate(4.888889, 4.888889)" id="Combined-Shape">
                                        <path d="M3.11111111,2.48257175 L0.832788199,0.204248838 C0.659221847,0.0306824861 0.37781519,0.0306824861 0.204248838,0.204248838 L0.204248838,0.832788199 L2.48257175,3.11111111 L0.151606014,5.44207685 C-0.0219603375,5.6156432 -0.0219603375,5.89704986 0.151606014,6.07061621 C0.325172366,6.24418256 0.606579024,6.24418256 0.780145375,6.07061621 L3.11111111,3.73965047 L5.44207685,6.07061621 C5.6156432,6.24418256 5.89704986,6.24418256 6.07061621,6.07061621 C6.24418256,5.89704986 6.24418256,5.6156432 6.07061621,5.44207685 L3.73965047,3.11111111 L6.01797338,0.832788199 C6.19153974,0.659221847 6.19153974,0.37781519 6.01797338,0.204248838 L5.38943402,0.204248838 L3.11111111,2.48257175 Z"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                </span>
                                </button>
                                <h4 class="modal-title">Создать новое видео обращение</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-6 leftBlock">
                                        <form action="" method="post">
                                            <div class="notVideo text-center">
                                                <p class="grey">+</p>
                                                <p class="grey">У вас нет загруженых видео</p>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label for="loatedVideo" class="btn btn-block red loadVideo">Загрузить
                                                        файл</label>
                                                    <input type="file" id="loatedVideo" accept="video/*" class="hidden">
                                                </div>
                                                <div class="col-sm-6">
                                                    <a href="#" class="btn btn-block green">Записать видео</a>

                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                    <div class="col-sm-6 rightBlock">
                                        <div role="tabpanel" class="panelInstruction">

                                            <!-- Навігаційні елементи вкладок -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active text-center">
                                                    <a href="#home" aria-controls="home" role="tab" data-toggle="tab"
                                                            class="leftPanel">Советы по созданию</a>
                                                </li>
                                                <li role="presentation" class="text-center">
                                                    <a href="#profile"
                                                            aria-controls="profile"
                                                            role="tab"
                                                            data-toggle="tab"
                                                            class="rightPanel">О чем расказать</a>
                                                </li>

                                            </ul>

                                            <!-- Вкладки панелі -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane instruct active" id="home">
                                                    <h5>Инструкция по созданию видео</h5>
                                                    <ul>
                                                        <li>
                                                            Нам нравятся крупные планы, но все же советуем вам сесть на
                                                            расстоянии вытянутой руки от экрана.
                                                        </li>
                                                        <li>
                                                            У вас только 30 секунд, постарайтесь уложиться.
                                                        </li>
                                                        <li>
                                                            Старайтесь, чтобы источник света был перед вами, а не
                                                            позади.
                                                        </li>
                                                        <li>
                                                            Смотрите в камеру, чтобы установить связь со зрителем.
                                                        </li>
                                                        <li>
                                                            Прежде чем видео будет загружено, вы сможете его
                                                            просмотреть.
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                <!--            випадаюче вікно для створеня\загрузки фото в кабінет-->
                <div class="modal fade createFoto" id="createFoto" tabindex="3" role="dialog"
                        aria-labelledby="createFotoLabel"
                        aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">

                                    <svg width="8px" height="8px" viewBox="0 0 8 8">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
        <g id="UI15.1----Отзывы-(личный-кабинет)---дефолт" transform="translate(-525.000000, -774.000000)"
                stroke="#FFFFFF" fill="#FFFFFF">
            <g id="content" transform="translate(280.000000, 151.000000)">
                <g id="Group-22" transform="translate(20.000000, 89.000000)">
                    <g id="Group-20">
                        <g id="Group-19" transform="translate(0.000000, 204.000000)">
                            <g id="Group-25" transform="translate(70.000000, 273.000000)">
                                <g id="close" transform="translate(151.000000, 53.000000)">
                                    <g transform="translate(4.888889, 4.888889)" id="Combined-Shape">
                                        <path d="M3.11111111,2.48257175 L0.832788199,0.204248838 C0.659221847,0.0306824861 0.37781519,0.0306824861 0.204248838,0.204248838 L0.204248838,0.832788199 L2.48257175,3.11111111 L0.151606014,5.44207685 C-0.0219603375,5.6156432 -0.0219603375,5.89704986 0.151606014,6.07061621 C0.325172366,6.24418256 0.606579024,6.24418256 0.780145375,6.07061621 L3.11111111,3.73965047 L5.44207685,6.07061621 C5.6156432,6.24418256 5.89704986,6.24418256 6.07061621,6.07061621 C6.24418256,5.89704986 6.24418256,5.6156432 6.07061621,5.44207685 L3.73965047,3.11111111 L6.01797338,0.832788199 C6.19153974,0.659221847 6.19153974,0.37781519 6.01797338,0.204248838 L5.38943402,0.204248838 L3.11111111,2.48257175 Z"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                </span>
                                </button>
                                <h4 class="modal-title">Загрузка и редактирование фотографии</h4>
                            </div>
                            <form action="" method="post">
                                <div class="modal-body">

                                    <div class="row">
                                        <div class="col-sm-6 leftBlock">

                                            <div class="editFoto">

                                            </div>

                                            <label for="loatedVideo" class="btn btn-block red loadVideo">Загрузить новое
                                                фото</label>
                                            <input type="file" id="loatedVideo" accept="image/*" class="hidden">

                                        </div>
                                        <div class="col-sm-6 rightBlock">
                                            <div class="webFoto">
                                                <div class="yourFoto">

                                                    <p class="text-center">Ваша аватарка</p>
                                                    <div class="text-center avatar">
                                                        <svg width="76px" height="95px" viewBox="0 0 76 95">
                                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                                    fill-rule="evenodd" stroke-linecap="round"
                                                                    stroke-linejoin="round">
                                                                <g id="UI14.7-----Загрузка-и-редактирование-фото"
                                                                        transform="translate(-801.000000, -753.000000)"
                                                                        stroke="#A0A0A0">
                                                                    <g id="Group-7"
                                                                            transform="translate(-1.000000, 0.000000)">
                                                                        <g id="product-copy-5"
                                                                                transform="translate(335.000000, 638.000000)">
                                                                            <g id="Group"
                                                                                    transform="translate(445.000000, 94.000000)">
                                                                                <g id="avatar_icon"
                                                                                        transform="translate(23.000000, 22.000000)">
                                                                                    <path d="M48.2230932,52.5831579 L54.0006356,52.5831579 C65.1705508,52.5831579 74.2220339,61.5774737 74.2220339,72.6768421 L74.2220339,87.0294737"
                                                                                            id="Shape"></path>
                                                                                    <path d="M0.462076271,87.2208421 L0.462076271,72.8682105 C0.462076271,61.7688421 9.51355932,52.7745263 20.6834746,52.7745263 L26.4610169,52.7745263"
                                                                                            id="Shape"></path>
                                                                                    <path d="M47.4527542,49.904 C48.0305085,51.2435789 48.415678,52.5831579 48.415678,54.1141053 C48.415678,60.0465263 43.6010593,64.8307368 37.6309322,64.8307368 C31.6608051,64.8307368 26.8461864,60.0465263 26.8461864,54.1141053 C26.8461864,52.7745263 27.0387712,51.2435789 27.6165254,50.0953684"
                                                                                            id="Shape"></path>
                                                                                    <path d="M56.1190678,27.8966316 C56.1190678,39.1873684 48.9934322,53.54 37.6309322,53.54 C26.2684322,53.54 19.1427966,39.1873684 19.1427966,27.8966316 C19.1427966,16.6058947 20.1057203,0.148210526 37.6309322,0.148210526 C54.9635593,0.148210526 56.1190678,16.6058947 56.1190678,27.8966316 Z"
                                                                                            id="Shape"></path>
                                                                                    <path d="M13.557839,93.9187368 L13.557839,79.7574737"
                                                                                            id="Shape"></path>
                                                                                    <path d="M61.3188559,93.9187368 L61.3188559,79.7574737"
                                                                                            id="Shape"></path>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </div>
                                                    <p class="text-center grey">На фото должны быть именно ваше лицо!
                                                        Групповые фото будут баниться.</p>
                                                </div>

                                                <a href="#" class="btn btn-block red">Сделать фото с вебкамеры</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default cloce" data-dismiss="modal">Отменить
                                    </button>
                                    <button type="button" class="btn btn-primary send">Сохранить</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!--                випадаюче вікно для коментаря -->
                <div class="modal fade feedbackFrends createVideo" id="feedbackFrends" tabindex="-1" role="dialog"
                        aria-labelledby="myModalLabel"
                        aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                            aria-hidden="true">&times;</span><span
                                            class="sr-only">Закрити</span></button>
                                <h4 class="modal-title" id="myModalLabel">Расскажите о вашем друге</h4>
                            </div>
                            <div class="modal-body">
                                <span>
                                    Пожалуйста, расскажите о том, что вам нравится в вашем друге - это займет не более 2-х минут и поможет тысячам других пользователей узнать его поближе.
                                </span>
                                <div class="aboutWhom">
                                    <a href="#"><img src="/img/product/fotoFace.jpg"" alt=""></a>
                                    <a href="#" class="nameFrend">Доба Дмитрий</a>
                                </div>

                                <div class="fieldText">
                                    <span>Продолжите предложения или напишите своими словами:</span>
                                    <textarea name="comment" id="commentFrend"></textarea>
                                </div>

                                <div class="checkText">
                                    <input type="checkbox">
                                    <span>Напомнить мне написать рекомендацию позже</span>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <form enctype="multipart/form-data" method="post">
                                    <label for="loadImg" class="redBtn">
                                        <svg class="strech" width="16px" height="18px">
                                            <g id="Page-1" stroke="none" stroke-width="1"
                                                    fill="none" fill-rule="evenodd"
                                                    stroke-linecap="round" stroke-linejoin="round">
                                                <g id="strech"
                                                        transform="translate(-566.000000, -5192.000000)">
                                                    <g id="Group-15-Copy-5"
                                                            transform="translate(280.000000, 3102.000000)">
                                                        <g id="Group-51"
                                                                transform="translate(19.000000, 22.000000)">
                                                            <g id="Group-21"
                                                                    transform="translate(0.000000, 165.000000)">
                                                                <g id="Group-13-Copy-6"
                                                                        transform="translate(207.000000, 1491.000000)">
                                                                    <g id="attach_icon"
                                                                            transform="translate(68.214102, 421.227241) rotate(-330.000000) translate(-68.214102, -421.227241) translate(64.214102, 411.727241)">
                                                                        <path d="M5.53191489,9.66105263 L5.53191489,16.2547368 C5.53191489,17.7536842 4.2893617,18.9831579 2.77446809,18.9831579 L2.77446809,18.9831579 C1.25957447,18.9831579 0.0170212766,17.7536842 0.0170212766,16.2547368 L0.0170212766,9.66105263"
                                                                                id="Shape"></path>
                                                                        <path d="M0.0170212766,10.3578947 L0.0170212766,3.95789474 C0.0170212766,1.79368421 1.81276596,0.0168421053 4,0.0168421053 L4,0.0168421053 C6.18723404,0.0168421053 7.98297872,1.79368421 7.98297872,3.95789474 L7.98297872,10.3578947"
                                                                                id="Shape"></path>
                                                                        <path d="M2.51914894,13.6 L2.51914894,4.42947368 C2.51914894,3.61263158 3.19148936,2.93894737 4.02553191,2.93894737 C4.85106383,2.93894737 5.53191489,3.60421053 5.53191489,4.42947368 L5.53191489,9.6"
                                                                                id="Shape"></path>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>&nbsp;Прикрепить фото</label>
                                    <input type="file" name="photo" multiple
                                            accept="application/pdf"
                                            id="loadImg">
                                    <input type="submit" class="submitImg" value="Отправить"
                                            style="display: none">
                                </form>
                                <button type="submit" class="btn btn-success">Оптравить</button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>


<?php

$this->registerJsFile('/js/aprofile.js', [
    'position' => \yii\web\View::POS_END,
    'depends'  => [
        'frontend\assets\AppAsset',
    ],
]);