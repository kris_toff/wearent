<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 12.05.2017
 * Time: 17:02
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/** @var \yii\web\View $this */
/** @var \common\models\UserProfile $model */
/** @var string|null $referrer */
/** @var boolean $own true - собственная страница, false - страница другого пользователя */
/** @var \yii\db\ActiveQuery $favouriteQuery Запрос на избранные товары */

\frontend\assets\FrontendAsset::register($this);
\yii\bootstrap\BootstrapPluginAsset::register($this);
\frontend\assets\CheckboxAsset::register($this);
\frontend\assets\ReadmoreAsset::register($this);
\frontend\assets\SliderAsset::register($this);
\frontend\assets\ProductAsset::register($this);
\frontend\assets\yyAsset::register($this);
\frontend\assets\RateAsset::register($this);

$lg = $model['languages'];
//print_r($lg);exit;
$lgs = [
    'id'  => 'Bahasa Indonesia',
    'ms'  => 'Bahasa Malaysia',
    'bn'  => 'Bengali',
    'da'  => 'Dansk',
    'de'  => 'Deutsch',
    'en'  => 'English',
    'es'  => 'Español',
    'fr'  => 'Français',
    'hi'  => 'Hindi',
    'it'  => 'Italiano',
    'hu'  => 'Magyar',
    'nl'  => 'Nederlands',
    'nn'  => 'Norsk',
    'pl'  => 'Polski',
    'pt'  => 'Português',
    'pa'  => 'Punjabi',
    'isl' => 'Sign Language', // язык жестов
    'se'  => 'Suomi',
    'sv'  => 'Svenska',
    'tl'  => 'Tagalog',
    'tr'  => 'Türkçe',
    'cs'  => 'Čeština',
    'el'  => 'Ελληνικά',
    'ru'  => 'Русский',
    'uk'  => 'Українська',
    'heb' => 'עברית',
    'ar'  => 'العربية',
    'th'  => 'ภาษาไทย',
    'zh'  => '中文',
    'ja'  => '日本語',
    'ko'  => '한국어',
];
$l   = array_intersect_key($lgs, array_flip(ArrayHelper::getColumn($lg, 'lang_code')));

usort($l, function($a, $b) {
    return strcasecmp($a, $b);
});
?>


<div class="row wrapp contentBody">
    <div class="container containerProfile">
        <div class="row profilePage">
            <div class="linkFirstPage col-sm-12">
                <?php if (!is_null($referrer)): ?>
                    <?= Html::a('← ' . \Yii::t('UI13', '$PAGE_PREVIOUS_REFERRER_LINK$'), $referrer); ?>
                <?php endif; ?>

                <?php if ($own): ?>
                    <a class="hidden-xs" href="/profile/edit">
                        <svg class="editIcon" width="18px" height="18px" viewBox="0 0 18 18">
                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g id="editIcon" transform="translate(-1088.000000, -291.000000)" fill-rule="nonzero">
                                    <g id="content" transform="translate(280.000000, 151.000000)">
                                        <g id="Group-22" transform="translate(20.000000, 89.000000)">
                                            <g id="Group-20">
                                                <g id="Group-4">
                                                    <g id="Group-24" transform="translate(770.000000, 26.000000)">
                                                        <g id="edit_icon" transform="translate(18.000000, 25.000000)">
                                                            <path d="M15.6,9.6 C15.3,9.6 15.1,9.8 15.1,10.1 L15.1,16.5 L1.7,16.5 L1.7,3.1 L8.2,3.1 C8.5,3.1 8.7,2.9 8.7,2.6 C8.7,2.3 8.5,2.1 8.2,2.1 L1.2,2.1 C0.9,2.1 0.7,2.3 0.7,2.6 L0.7,17 C0.7,17.3 0.9,17.5 1.2,17.5 L15.6,17.5 C15.9,17.5 16.1,17.3 16.1,17 L16.1,10.1 C16.1,9.9 15.9,9.6 15.6,9.6 Z"
                                                                    id="Shape"></path>
                                                            <path d="M17.7,3.8 L14.5,0.6 C14.3,0.4 14,0.4 13.8,0.6 L5.4,9 C5.3,9.1 5.3,9.2 5.3,9.3 L4.7,13 C4.7,13.2 4.7,13.3 4.8,13.4 C4.9,13.5 5,13.5 5.2,13.5 L5.3,13.5 L9,12.9 C9.1,12.9 9.2,12.8 9.3,12.8 L17.7,4.4 C17.9,4.3 17.9,4 17.7,3.8 Z M8.7,12 L5.8,12.4 L6.2,9.5 L11.8,3.9 L14.2,6.3 L8.7,12 Z M15.1,5.7 L12.7,3.3 L14.3,1.7 L16.7,4.1 L15.1,5.7 Z"
                                                                    id="Shape"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        <?= \Yii::t('UI13', '$EDIT_PROFILE_LINK$'); ?>
                    </a>
                <?php endif; ?>
            </div>

            <!--            КОНТЕНТ -->


            <!--            БЛОК З НАЗВОЮ КОРИСТУВАЧА-->
            <div class="blockInf styleBlock col-sm-12 col-md-8 col-xs-12">
                <div class="topLinck">
                    <?php if ($own): ?>
                        <a class="hidden-sm hidden-md hidden-lg editProfile" href="/profile/edit">
                            <svg class="editIcon" width="18px" height="18px" viewBox="0 0 18 18">
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="editIcon"
                                            transform="translate(-1088.000000, -291.000000)"
                                            fill-rule="nonzero">
                                        <g id="content" transform="translate(280.000000, 151.000000)">
                                            <g id="Group-22" transform="translate(20.000000, 89.000000)">
                                                <g id="Group-20">
                                                    <g id="Group-4">
                                                        <g id="Group-24" transform="translate(770.000000, 26.000000)">
                                                            <g id="edit_icon"
                                                                    transform="translate(18.000000, 25.000000)">
                                                                <path d="M15.6,9.6 C15.3,9.6 15.1,9.8 15.1,10.1 L15.1,16.5 L1.7,16.5 L1.7,3.1 L8.2,3.1 C8.5,3.1 8.7,2.9 8.7,2.6 C8.7,2.3 8.5,2.1 8.2,2.1 L1.2,2.1 C0.9,2.1 0.7,2.3 0.7,2.6 L0.7,17 C0.7,17.3 0.9,17.5 1.2,17.5 L15.6,17.5 C15.9,17.5 16.1,17.3 16.1,17 L16.1,10.1 C16.1,9.9 15.9,9.6 15.6,9.6 Z"
                                                                        id="Shape"></path>
                                                                <path d="M17.7,3.8 L14.5,0.6 C14.3,0.4 14,0.4 13.8,0.6 L5.4,9 C5.3,9.1 5.3,9.2 5.3,9.3 L4.7,13 C4.7,13.2 4.7,13.3 4.8,13.4 C4.9,13.5 5,13.5 5.2,13.5 L5.3,13.5 L9,12.9 C9.1,12.9 9.2,12.8 9.3,12.8 L17.7,4.4 C17.9,4.3 17.9,4 17.7,3.8 Z M8.7,12 L5.8,12.4 L6.2,9.5 L11.8,3.9 L14.2,6.3 L8.7,12 Z M15.1,5.7 L12.7,3.3 L14.3,1.7 L16.7,4.1 L15.1,5.7 Z"
                                                                        id="Shape"></path>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                            <?= \Yii::t('UI13', '$EDIT_PROFILE_LINK$'); ?>
                        </a>
                    <?php endif; ?>

                    <?= \yii\bootstrap\ButtonDropdown::widget([
                        'label'            => '<span class="circle"></span><span class="circle"></span><span class="circle"></span>',
                        'encodeLabel'      => false,
                        'dropdown'         => [
                            'items'        => [
                                [
                                    'label'  => '<svg class="imgLiPunct" width="18px" height="18px" viewBox="0 0 18 18"
                                                     version="1.1">

                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                       fill-rule="evenodd">
                                                        <g id="imgLiPunct"
                                                           transform="translate(-671.000000, -1156.000000)"
                                                           fill-rule="nonzero"
                                                        ">
                                                        <g id="reviews" transform="translate(210.000000, 1019.000000)">
                                                            <g id="Group-22"
                                                               transform="translate(21.000000, 86.000000)">
                                                                <g id="Group-20">
                                                                    <g id="Group-4">
                                                                        <g id="Group-24"
                                                                           transform="translate(422.000000, 26.000000)">
                                                                            <g id="edit_icon"
                                                                               transform="translate(18.000000, 25.000000)">
                                                                                <path d="M15.6,9.6 C15.3,9.6 15.1,9.8 15.1,10.1 L15.1,16.5 L1.7,16.5 L1.7,3.1 L8.2,3.1 C8.5,3.1 8.7,2.9 8.7,2.6 C8.7,2.3 8.5,2.1 8.2,2.1 L1.2,2.1 C0.9,2.1 0.7,2.3 0.7,2.6 L0.7,17 C0.7,17.3 0.9,17.5 1.2,17.5 L15.6,17.5 C15.9,17.5 16.1,17.3 16.1,17 L16.1,10.1 C16.1,9.9 15.9,9.6 15.6,9.6 Z"
                                                                                      id="Shape"></path>
                                                                                <path d="M17.7,3.8 L14.5,0.6 C14.3,0.4 14,0.4 13.8,0.6 L5.4,9 C5.3,9.1 5.3,9.2 5.3,9.3 L4.7,13 C4.7,13.2 4.7,13.3 4.8,13.4 C4.9,13.5 5,13.5 5.2,13.5 L5.3,13.5 L9,12.9 C9.1,12.9 9.2,12.8 9.3,12.8 L17.7,4.4 C17.9,4.3 17.9,4 17.7,3.8 Z M8.7,12 L5.8,12.4 L6.2,9.5 L11.8,3.9 L14.2,6.3 L8.7,12 Z M15.1,5.7 L12.7,3.3 L14.3,1.7 L16.7,4.1 L15.1,5.7 Z"
                                                                                      id="Shape"></path>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    </g>
                                                </svg>' . \Yii::t('UI13', '$CONTROL_MENU_COMPLAINT_ITEM$'),
                                    'url'    => '#',
                                    'encode' => false,
                                ],
                            ],
                            'clientEvents' => [
                                'show.bs.dropdown' => 'function(){ console.log("goofd"); }',
                            ],
                        ],
                        'options'          => [
                            'class' => 'editPopup openDropProfile',
                        ],
                        'containerOptions' => [
                            'class' => 'rightInineBlock dropdown hidden-sm hidden-md hidden-lg',
                        ],
                    ]) ?>
                </div>


                <div class="media topInf">
                    <div class="media-left">
                        <div class="fotoManInf" ng-cloak>
                            <?php foreach ($model->avatars as $item): ?>
                                <?= Html::tag('div', '',
                                    ['class' => 'fotoAccount', 'style' => "background-image: url({$item['path']})"]); ?>
                            <?php endforeach; ?>
                            <!--                            <div class="Foto1 fotoAccount"></div>-->
                            <!--                            <div class="Foto2 fotoAccount"></div>-->
                            <!--                            <div class="Foto3 fotoAccount"></div>-->
                        </div>
                    </div>
                    <div class="media-body textAbounUser">
                        <h4 class="text-left"><?= Html::encode($model['fullName']); ?></h4>
                        <p class="placeUser text-left">
                            <svg width="13px" height="18px" viewBox="0 0 13 18" version="1.1">
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="+UI5.-Квитанция-об-оплате" transform="translate(-973.000000, -382.000000)"
                                            fill-rule="nonzero" fill="#AFAFAF">
                                        <g id="Group-13" transform="translate(366.000000, 142.000000)">
                                            <g id="Group-11" transform="translate(20.000000, 208.000000)">
                                                <g id="Group" transform="translate(587.000000, 30.000000)">
                                                    <g id="Group-16" transform="translate(0.000000, 2.000000)">
                                                        <path d="M6.5,17.5 L6.1,17.1 C5.9,16.8 0.4,10.6 0.4,6.4 C0.4,3 3.1,0.3 6.5,0.3 C9.9,0.3 12.6,3 12.6,6.4 C12.6,10.6 7.1,16.8 6.9,17.1 L6.5,17.5 Z M6.5,1.3 C3.7,1.3 1.4,3.6 1.4,6.4 C1.4,9.7 5.2,14.5 6.5,16 C7.7,14.5 11.6,9.7 11.6,6.4 C11.6,3.6 9.3,1.3 6.5,1.3 Z"
                                                                id="Shape"></path>
                                                        <path d="M6.5,9.6 C4.7,9.6 3.2,8.1 3.2,6.3 C3.2,4.5 4.7,3 6.5,3 C8.3,3 9.8,4.5 9.8,6.3 C9.8,8.1 8.3,9.6 6.5,9.6 Z M6.5,4 C5.2,4 4.2,5 4.2,6.3 C4.2,7.6 5.2,8.6 6.5,8.6 C7.8,8.6 8.8,7.6 8.8,6.3 C8.8,5.1 7.8,4 6.5,4 Z"
                                                                id="Shape"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                            <?php
                            $c = $model->country;
                            if (!is_null($c)) {
                                print \Yii::t('UI14.3-countries',
                                    '$COUNTRY_NAME_' . strtoupper($c->code) . '$');
                            }
                            ?>,
                            <?= Html::encode($model->city) ?>
                        </p>
                        <p class=" text-left">
                            <svg width="15px" height="17px" viewBox="0 0 15 17">
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="UI18.2.-Блог" transform="translate(-232.000000, -596.000000)"
                                            fill-rule="nonzero" fill="#AFAFAF">
                                        <g id="Group-10" transform="translate(210.000000, 520.000000)">
                                            <g id="Group" transform="translate(22.000000, 76.000000)">
                                                <path d="M14.1,1.8 L11,1.8 L11,1 C11,0.7 10.8,0.5 10.5,0.5 C10.2,0.5 10,0.7 10,1 L10,1.9 L5,1.9 L5,1 C5,0.7 4.8,0.5 4.5,0.5 C4.2,0.5 4,0.7 4,1 L4,1.9 L0.9,1.9 C0.6,1.9 0.4,2.1 0.4,2.4 L0.4,15.6 C0.4,15.9 0.6,16.1 0.9,16.1 L14.1,16.1 C14.4,16.1 14.6,15.9 14.6,15.6 L14.6,2.4 C14.6,2.1 14.4,1.8 14.1,1.8 Z M4,2.8 L4,3.7 C4,4 4.2,4.2 4.5,4.2 C4.8,4.2 5,4 5,3.7 L5,2.8 L10,2.8 L10,3.7 C10,4 10.2,4.2 10.5,4.2 C10.8,4.2 11,4 11,3.7 L11,2.8 L13.6,2.8 L13.6,6.2 L1.4,6.2 L1.4,2.8 L4,2.8 Z M1.4,15.1 L1.4,7.3 L13.6,7.3 L13.6,15.1 L1.4,15.1 Z"
                                                        id="Shape"></path>
                                                <path d="M8.8,9.5 L7.1,11.5 L6.2,10.6 C6,10.4 5.7,10.4 5.5,10.6 C5.3,10.8 5.3,11.1 5.5,11.3 L6.7,12.6 C6.8,12.7 6.9,12.8 7.1,12.8 C7.2,12.8 7.4,12.7 7.5,12.6 L9.6,10.2 C9.8,10 9.8,9.7 9.5,9.5 C9.3,9.3 9,9.3 8.8,9.5 Z"
                                                        id="Shape"></path>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                            <?= \Yii::t('UI9.1', '$ABOUT_OWNER_JOINED$',
                                ['date' => $model->user->created_at]) ?>
                        </p>
                        <div class="buttonTwo hidden-xs">
                            <a href="#" class="btn subscript ">
                                <svg width="20px" height="20px" viewBox="0 0 20 20">
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="UI13.0----Профиль-пользователя---пустой"
                                                transform="translate(-407.000000, -281.000000)"
                                                fill-rule="nonzero"
                                                fill="#FFFFFF">
                                            <g id="Group-2" transform="translate(210.000000, 151.000000)">
                                                <g id="Group-16" transform="translate(174.000000, 122.000000)">
                                                    <g id="subscribe_icon" transform="translate(23.000000, 8.000000)">
                                                        <path d="M12.2,18.1 L1.7,18.1 L1.7,13.7 C1.7,11.2 3.2,9.8 6.4,9.5 C6.6,9.5 6.8,9.3 6.8,9.1 C6.8,8.9 6.7,8.7 6.6,8.6 C5.4,7.9 4.6,6.4 4.6,5 C4.6,3.2 6.1,1.1 7.9,1.1 C9.7,1.1 11.2,3.3 11.2,5 C11.2,6.4 10.3,7.9 9.2,8.6 C9,8.7 8.9,8.9 9,9.1 C9.1,9.3 9.2,9.5 9.4,9.5 C10.4,9.6 11.3,9.8 11.9,10.2 C12.2,10.3 12.4,10.2 12.6,10 C12.7,9.8 12.6,9.5 12.4,9.3 C11.9,9.1 11.3,8.9 10.7,8.7 C11.6,7.7 12.3,6.3 12.3,5 C12.3,2.7 10.4,0.1 8,0.1 C5.6,0.1 3.7,2.8 3.7,5 C3.7,6.3 4.3,7.7 5.3,8.7 C1.7,9.5 0.9,11.8 0.9,13.7 L0.9,18.6 C0.9,18.9 1.1,19.1 1.4,19.1 L12.4,19.1 C12.7,19.1 12.9,18.9 12.9,18.6 C12.9,18.3 12.5,18.1 12.2,18.1 Z"
                                                                id="Shape"></path>
                                                        <path d="M19.8,14.2 C19.9,14.1 19.9,13.9 19.8,13.8 C19.8,13.7 19.7,13.7 19.7,13.6 L16.3,10.2 C16.1,10 15.8,10 15.6,10.2 C15.4,10.4 15.4,10.7 15.6,10.9 L18.1,13.4 L12.2,13.4 C11.9,13.4 11.7,13.6 11.7,13.9 C11.7,14.2 11.9,14.4 12.2,14.4 L18.1,14.4 L15.6,16.9 C15.4,17.1 15.4,17.4 15.6,17.6 C15.7,17.7 15.8,17.7 16,17.7 C16.2,17.7 16.3,17.7 16.4,17.6 L19.8,14.2 C19.7,14.3 19.8,14.3 19.8,14.2 Z"
                                                                id="Shape"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>&nbsp; Подписаться</a>
                            <a href="#" class="btn contact ">
                                <svg width="24px" height="16px" viewBox="0 0 24 16">
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g class="re" transform="translate(-600.000000, -284.000000)">
                                            <g id="Group-2" transform="translate(210.000000, 151.000000)">
                                                <g id="Group-16-Copy" transform="translate(360.000000, 122.000000)">
                                                    <g id="Group-11" transform="translate(30.000000, 8.000000)">
                                                        <g id="mial_icon" transform="translate(0.000000, 3.000000)">
                                                            <path d="M1.23926278,1.20054155 C1.05648733,1.37344812 0.942399979,1.61876503 0.942399979,1.89125116 L0.942399979,13.1871485 C0.942399979,13.4601935 1.05469603,13.7063015 1.23747189,13.8794155 L1.23747189,13.8794155 L8.52871981,7.53919983 L1.23926278,1.20054155 L1.23926278,1.20054155 L1.23926278,1.20054155 L1.23926278,1.20054155 Z M22.3225276,1.19898425 C22.5053035,1.3720982 22.6175995,1.61820617 22.6175995,1.89125116 L22.6175995,13.1871485 C22.6175995,13.4596346 22.5035122,13.7049516 22.3207367,13.8778581 L15.0312797,7.53919983 L22.3225276,1.19898425 L22.3225276,1.19898425 L22.3225276,1.19898425 Z M14.326204,8.15230914 L21.2039995,14.1359997 L2.35599995,14.1359997 L9.23379548,8.15230914 L11.7799997,10.3663998 L14.326204,8.15230914 L14.326204,8.15230914 L14.326204,8.15230914 Z M1.88818775,0 C0.845370449,0 0,0.842989748 0,1.87920237 L0,13.1991973 C0,14.2370521 0.838512858,15.0783997 1.88818775,15.0783997 L21.6718117,15.0783997 C22.714629,15.0783997 23.5599995,14.2354099 23.5599995,13.1991973 L23.5599995,1.87920237 C23.5599995,0.84134756 22.7214866,0 21.6718117,0 L1.88818775,0 L1.88818775,0 L1.88818775,0 Z M11.7799997,9.14129129 L21.2039995,0.942399979 L2.35599995,0.942399979 L11.7799997,9.14129129 L11.7799997,9.14129129 L11.7799997,9.14129129 Z"
                                                                    id="mail-envelope-closed"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>&nbsp; Связаться</a>
                        </div>
                        <?= \yii\bootstrap\ButtonDropdown::widget([
                            'label'            => '<span class="circle"></span><span class="circle"></span><span class="circle"></span>',
                            'encodeLabel'      => false,
                            'dropdown'         => [
                                'items'        => [
                                    [
                                        'label'  => ' <svg class="imgLiPunct" width="18px" height="18px" viewBox="0 0 18 18"
                                         version="1.1">

                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                           fill-rule="evenodd">
                                            <g id="imgLiPunct" transform="translate(-671.000000, -1156.000000)"
                                               fill-rule="nonzero"
                                            ">
                                            <g id="reviews" transform="translate(210.000000, 1019.000000)">
                                                <g id="Group-22" transform="translate(21.000000, 86.000000)">
                                                    <g id="Group-20">
                                                        <g id="Group-4">
                                                            <g id="Group-24"
                                                               transform="translate(422.000000, 26.000000)">
                                                                <g id="edit_icon"
                                                                   transform="translate(18.000000, 25.000000)">
                                                                    <path d="M15.6,9.6 C15.3,9.6 15.1,9.8 15.1,10.1 L15.1,16.5 L1.7,16.5 L1.7,3.1 L8.2,3.1 C8.5,3.1 8.7,2.9 8.7,2.6 C8.7,2.3 8.5,2.1 8.2,2.1 L1.2,2.1 C0.9,2.1 0.7,2.3 0.7,2.6 L0.7,17 C0.7,17.3 0.9,17.5 1.2,17.5 L15.6,17.5 C15.9,17.5 16.1,17.3 16.1,17 L16.1,10.1 C16.1,9.9 15.9,9.6 15.6,9.6 Z"
                                                                          id="Shape"></path>
                                                                    <path d="M17.7,3.8 L14.5,0.6 C14.3,0.4 14,0.4 13.8,0.6 L5.4,9 C5.3,9.1 5.3,9.2 5.3,9.3 L4.7,13 C4.7,13.2 4.7,13.3 4.8,13.4 C4.9,13.5 5,13.5 5.2,13.5 L5.3,13.5 L9,12.9 C9.1,12.9 9.2,12.8 9.3,12.8 L17.7,4.4 C17.9,4.3 17.9,4 17.7,3.8 Z M8.7,12 L5.8,12.4 L6.2,9.5 L11.8,3.9 L14.2,6.3 L8.7,12 Z M15.1,5.7 L12.7,3.3 L14.3,1.7 L16.7,4.1 L15.1,5.7 Z"
                                                                          id="Shape"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                        </g>
                                    </svg> ' . \Yii::t('UI13', '$CONTROL_MENU_COMPLAINT_ITEM$'),
                                        'url'    => '#',
                                        'encode' => false,
                                    ],
                                ],
                                'clientEvents' => [
                                    'show.bs.dropdown' => 'function(){ console.log("goofd"); }',
                                ],
                            ],
                            'options'          => [
                                'class' => 'editPopup openDropProfile',
                            ],
                            'containerOptions' => [
                                'class' => 'rightInineBlock dropdown hidden-xs',
                            ],
                        ]) ?>
                    </div>

                    <div class="buttonTwo hidden-sm hidden-md hidden-lg">
                        <a href="#" class="btn subscript ">
                            <svg width="20px" height="20px" viewBox="0 0 20 20">
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="UI13.0----Профиль-пользователя---пустой"
                                            transform="translate(-407.000000, -281.000000)"
                                            fill-rule="nonzero"
                                            fill="#FFFFFF">
                                        <g id="Group-2" transform="translate(210.000000, 151.000000)">
                                            <g id="Group-16" transform="translate(174.000000, 122.000000)">
                                                <g id="subscribe_icon" transform="translate(23.000000, 8.000000)">
                                                    <path d="M12.2,18.1 L1.7,18.1 L1.7,13.7 C1.7,11.2 3.2,9.8 6.4,9.5 C6.6,9.5 6.8,9.3 6.8,9.1 C6.8,8.9 6.7,8.7 6.6,8.6 C5.4,7.9 4.6,6.4 4.6,5 C4.6,3.2 6.1,1.1 7.9,1.1 C9.7,1.1 11.2,3.3 11.2,5 C11.2,6.4 10.3,7.9 9.2,8.6 C9,8.7 8.9,8.9 9,9.1 C9.1,9.3 9.2,9.5 9.4,9.5 C10.4,9.6 11.3,9.8 11.9,10.2 C12.2,10.3 12.4,10.2 12.6,10 C12.7,9.8 12.6,9.5 12.4,9.3 C11.9,9.1 11.3,8.9 10.7,8.7 C11.6,7.7 12.3,6.3 12.3,5 C12.3,2.7 10.4,0.1 8,0.1 C5.6,0.1 3.7,2.8 3.7,5 C3.7,6.3 4.3,7.7 5.3,8.7 C1.7,9.5 0.9,11.8 0.9,13.7 L0.9,18.6 C0.9,18.9 1.1,19.1 1.4,19.1 L12.4,19.1 C12.7,19.1 12.9,18.9 12.9,18.6 C12.9,18.3 12.5,18.1 12.2,18.1 Z"
                                                            id="Shape"></path>
                                                    <path d="M19.8,14.2 C19.9,14.1 19.9,13.9 19.8,13.8 C19.8,13.7 19.7,13.7 19.7,13.6 L16.3,10.2 C16.1,10 15.8,10 15.6,10.2 C15.4,10.4 15.4,10.7 15.6,10.9 L18.1,13.4 L12.2,13.4 C11.9,13.4 11.7,13.6 11.7,13.9 C11.7,14.2 11.9,14.4 12.2,14.4 L18.1,14.4 L15.6,16.9 C15.4,17.1 15.4,17.4 15.6,17.6 C15.7,17.7 15.8,17.7 16,17.7 C16.2,17.7 16.3,17.7 16.4,17.6 L19.8,14.2 C19.7,14.3 19.8,14.3 19.8,14.2 Z"
                                                            id="Shape"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>&nbsp; Подписаться ??</a>
                        <a href="#" class="btn contact ">
                            <svg width="24px" height="16px" viewBox="0 0 24 16">
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g class="re" transform="translate(-600.000000, -284.000000)">
                                        <g id="Group-2" transform="translate(210.000000, 151.000000)">
                                            <g id="Group-16-Copy" transform="translate(360.000000, 122.000000)">
                                                <g id="Group-11" transform="translate(30.000000, 8.000000)">
                                                    <g id="mial_icon" transform="translate(0.000000, 3.000000)">
                                                        <path d="M1.23926278,1.20054155 C1.05648733,1.37344812 0.942399979,1.61876503 0.942399979,1.89125116 L0.942399979,13.1871485 C0.942399979,13.4601935 1.05469603,13.7063015 1.23747189,13.8794155 L1.23747189,13.8794155 L8.52871981,7.53919983 L1.23926278,1.20054155 L1.23926278,1.20054155 L1.23926278,1.20054155 L1.23926278,1.20054155 Z M22.3225276,1.19898425 C22.5053035,1.3720982 22.6175995,1.61820617 22.6175995,1.89125116 L22.6175995,13.1871485 C22.6175995,13.4596346 22.5035122,13.7049516 22.3207367,13.8778581 L15.0312797,7.53919983 L22.3225276,1.19898425 L22.3225276,1.19898425 L22.3225276,1.19898425 Z M14.326204,8.15230914 L21.2039995,14.1359997 L2.35599995,14.1359997 L9.23379548,8.15230914 L11.7799997,10.3663998 L14.326204,8.15230914 L14.326204,8.15230914 L14.326204,8.15230914 Z M1.88818775,0 C0.845370449,0 0,0.842989748 0,1.87920237 L0,13.1991973 C0,14.2370521 0.838512858,15.0783997 1.88818775,15.0783997 L21.6718117,15.0783997 C22.714629,15.0783997 23.5599995,14.2354099 23.5599995,13.1991973 L23.5599995,1.87920237 C23.5599995,0.84134756 22.7214866,0 21.6718117,0 L1.88818775,0 L1.88818775,0 L1.88818775,0 Z M11.7799997,9.14129129 L21.2039995,0.942399979 L2.35599995,0.942399979 L11.7799997,9.14129129 L11.7799997,9.14129129 L11.7799997,9.14129129 Z"
                                                                id="mail-envelope-closed"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>&nbsp; Связаться ??</a>
                    </div>
                </div>

                <div class="reewardInf row hidden">
                    <div class="col-sm-3 col-xs-6">
                        <div class="thumbnail award">
                            <div class="blockAward">
                                <svg width="24px" height="29px" viewBox="0 0 24 29">
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"
                                            stroke-linecap="round" stroke-linejoin="round">
                                        <g id="UI13.1----Профиль-пользователя---дефолт"
                                                transform="translate(-316.000000, -396.000000)">
                                            <g id="Group-2" transform="translate(210.000000, 151.000000)">
                                                <g id="Group-43" transform="translate(66.000000, 244.000000)">
                                                    <g id="Group-8" transform="translate(0.000000, 2.000000)">
                                                        <g id="icon_1" transform="translate(41.000000, 0.000000)">
                                                            <path d="M11.2,0.2 L12.7,0.8 C16.1,2.2 18.6,3.2 22.2,3.7 L22.2,3.7 L22.2,7.7 C22.2,17.1 17.6,20.8 11.2,27.3 C4.8,20.9 0.2,17.2 0.2,7.7 L0.2,3.7 L0.2,3.7 C3.8,3.2 6.3,2.2 9.7,0.8 L11.2,0.2"
                                                                    id="Shape" stroke="#A0A0A0"></path>
                                                            <polygon id="Shape" stroke="#66BC80"
                                                                    points="10.2 16.9 17 10.1 14.5 7.6 10.2 11.9 7.9 9.7 5.4 12.2"></polygon>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                                <div class="caption">
                                    <h4 class="text-center">Подтвержден на 100%</h4>
                                </div>
                            </div>

                            <div class="infAward confirmed
 text-left " style="display: none">
                                <div class="title">
                                    <h4 class="titleAward">Верификация профиля</h4>
                                    <span>
                                        <svg class="close" width="12px" height="12px" viewBox="0 0 12 12">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
        <g id="UI13.2----Мой-профиль-Подсказки-при-наведении-на-награды-Верификация-наград"
                transform="translate(-375.000000, -603.000000)"
                stroke="#FFFFFF"
                fill="#FFFFFF">
            <g id="product-copy-5" transform="translate(10.000000, 578.000000)">
                <g id="close" transform="translate(358.000000, 18.000000)">
                    <g transform="translate(7.944444, 7.944444)" id="Combined-Shape">
                        <path d="M5.05555556,4.03417909 L1.35328082,0.331904362 C1.0712355,0.0498590399 0.613949684,0.0498590399 0.331904362,0.331904362 L0.331904362,1.35328082 L4.03417909,5.05555556 L0.246359773,8.84337488 C-0.0356855484,9.1254202 -0.0356855484,9.58270602 0.246359773,9.86475134 C0.528405095,10.1467967 0.985690913,10.1467967 1.26773624,9.86475134 L5.05555556,6.07693202 L8.84337488,9.86475134 C9.1254202,10.1467967 9.58270602,10.1467967 9.86475134,9.86475134 C10.1467967,9.58270602 10.1467967,9.1254202 9.86475134,8.84337488 L6.07693202,5.05555556 L9.77920675,1.35328082 C10.0612521,1.0712355 10.0612521,0.613949684 9.77920675,0.331904362 L8.75783029,0.331904362 L5.05555556,4.03417909 Z"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                    </span>
                                </div>
                                <p class="progressBar">Частично подтвержден</p>
                                <ul class="list-unstyled">
                                    <li><p>Привязать электронный адрес <span class="red">10%</span></p>
                                        <span><svg width="19px" height="18px" viewBox="0 0 19 18">

    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="+UI2.-Главная" transform="translate(-210.000000, -3421.000000)" fill-rule="nonzero">
            <g id="Group-11" transform="translate(0.000000, 579.000000)">
                <g id="Group-7" transform="translate(0.000000, 2726.000000)">
                    <g id="Group-6" transform="translate(210.000000, 58.000000)">
                        <g id="Group-3">
                            <g id="Group-Copy-4" transform="translate(0.000000, 58.000000)">
                                <path d="M9.4,1.4 C13.7,1.4 17.2,4.9 17.2,9.2 C17.2,13.5 13.7,17 9.4,17 C5.1,17 1.6,13.5 1.6,9.2 C1.6,4.9 5.1,1.4 9.4,1.4 L9.4,1.4 Z M9.4,0.4 C4.6,0.4 0.6,4.3 0.6,9.2 C0.6,14.1 4.5,18 9.4,18 C14.3,18 18.2,14.1 18.2,9.2 C18.2,4.3 14.2,0.4 9.4,0.4 L9.4,0.4 Z"
                                        id="Shape"
                                        fill="#afafaf"></path>
                                <path d="M8.9,11.6 C8.8,11.6 8.6,11.5 8.5,11.4 L6.4,9.1 C6.2,8.9 6.2,8.6 6.4,8.4 C6.6,8.2 6.9,8.2 7.1,8.4 L8.9,10.3 L12.1,6.5 C12.3,6.3 12.6,6.3 12.8,6.4 C13,6.6 13,6.9 12.9,7.1 L9.3,11.3 C9.2,11.5 9.1,11.6 8.9,11.6 L8.9,11.6 Z"
                                        id="Shape"
                                        fill="#4CDB58"></path>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span>
                                    </li>
                                    <li>
                                        <p>Привязать номер мобильного телефона <span class="red">10%</span></p><span><svg
                                                    width="19px"
                                                    height="18px"
                                                    viewBox="0 0 19 18">

    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="+UI2.-Главная" transform="translate(-210.000000, -3421.000000)" fill-rule="nonzero">
            <g id="Group-11" transform="translate(0.000000, 579.000000)">
                <g id="Group-7" transform="translate(0.000000, 2726.000000)">
                    <g id="Group-6" transform="translate(210.000000, 58.000000)">
                        <g id="Group-3">
                            <g id="Group-Copy-4" transform="translate(0.000000, 58.000000)">
                                <path d="M9.4,1.4 C13.7,1.4 17.2,4.9 17.2,9.2 C17.2,13.5 13.7,17 9.4,17 C5.1,17 1.6,13.5 1.6,9.2 C1.6,4.9 5.1,1.4 9.4,1.4 L9.4,1.4 Z M9.4,0.4 C4.6,0.4 0.6,4.3 0.6,9.2 C0.6,14.1 4.5,18 9.4,18 C14.3,18 18.2,14.1 18.2,9.2 C18.2,4.3 14.2,0.4 9.4,0.4 L9.4,0.4 Z"
                                        id="Shape"
                                        fill="#afafaf"></path>
                                <path d="M8.9,11.6 C8.8,11.6 8.6,11.5 8.5,11.4 L6.4,9.1 C6.2,8.9 6.2,8.6 6.4,8.4 C6.6,8.2 6.9,8.2 7.1,8.4 L8.9,10.3 L12.1,6.5 C12.3,6.3 12.6,6.3 12.8,6.4 C13,6.6 13,6.9 12.9,7.1 L9.3,11.3 C9.2,11.5 9.1,11.6 8.9,11.6 L8.9,11.6 Z"
                                        id="Shape"
                                        fill="#4CDB58"></path>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span>
                                    </li>
                                    <li><p>В Google Plus <span class="red">348 друзей</span>
                                        </p><span><svg width="19px" height="18px" viewBox="0 0 19 18">

    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="+UI2.-Главная" transform="translate(-210.000000, -3421.000000)" fill-rule="nonzero">
            <g id="Group-11" transform="translate(0.000000, 579.000000)">
                <g id="Group-7" transform="translate(0.000000, 2726.000000)">
                    <g id="Group-6" transform="translate(210.000000, 58.000000)">
                        <g id="Group-3">
                            <g id="Group-Copy-4" transform="translate(0.000000, 58.000000)">
                                <path d="M9.4,1.4 C13.7,1.4 17.2,4.9 17.2,9.2 C17.2,13.5 13.7,17 9.4,17 C5.1,17 1.6,13.5 1.6,9.2 C1.6,4.9 5.1,1.4 9.4,1.4 L9.4,1.4 Z M9.4,0.4 C4.6,0.4 0.6,4.3 0.6,9.2 C0.6,14.1 4.5,18 9.4,18 C14.3,18 18.2,14.1 18.2,9.2 C18.2,4.3 14.2,0.4 9.4,0.4 L9.4,0.4 Z"
                                        id="Shape"
                                        fill="#afafaf"></path>
                                <path d="M8.9,11.6 C8.8,11.6 8.6,11.5 8.5,11.4 L6.4,9.1 C6.2,8.9 6.2,8.6 6.4,8.4 C6.6,8.2 6.9,8.2 7.1,8.4 L8.9,10.3 L12.1,6.5 C12.3,6.3 12.6,6.3 12.8,6.4 C13,6.6 13,6.9 12.9,7.1 L9.3,11.3 C9.2,11.5 9.1,11.6 8.9,11.6 L8.9,11.6 Z"
                                        id="Shape"
                                        fill="#4CDB58"></path>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span>

                                    </li>
                                    <li>
                                        <p class="grey">Facebook <span class="red">не подтвержден</span></p>
                                        <span></span>
                                    </li>
                                    <li>
                                        <p class="grey">Twitter <span class="red">не подтвержден</span></p><span></span>
                                    </li>
                                    <li>
                                        <p>Привязан платежный инструмент</p>
                                        <span><svg width="19px" height="18px" viewBox="0 0 19 18">

    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="+UI2.-Главная" transform="translate(-210.000000, -3421.000000)" fill-rule="nonzero">
            <g id="Group-11" transform="translate(0.000000, 579.000000)">
                <g id="Group-7" transform="translate(0.000000, 2726.000000)">
                    <g id="Group-6" transform="translate(210.000000, 58.000000)">
                        <g id="Group-3">
                            <g id="Group-Copy-4" transform="translate(0.000000, 58.000000)">
                                <path d="M9.4,1.4 C13.7,1.4 17.2,4.9 17.2,9.2 C17.2,13.5 13.7,17 9.4,17 C5.1,17 1.6,13.5 1.6,9.2 C1.6,4.9 5.1,1.4 9.4,1.4 L9.4,1.4 Z M9.4,0.4 C4.6,0.4 0.6,4.3 0.6,9.2 C0.6,14.1 4.5,18 9.4,18 C14.3,18 18.2,14.1 18.2,9.2 C18.2,4.3 14.2,0.4 9.4,0.4 L9.4,0.4 Z"
                                        id="Shape"
                                        fill="#afafaf"></path>
                                <path d="M8.9,11.6 C8.8,11.6 8.6,11.5 8.5,11.4 L6.4,9.1 C6.2,8.9 6.2,8.6 6.4,8.4 C6.6,8.2 6.9,8.2 7.1,8.4 L8.9,10.3 L12.1,6.5 C12.3,6.3 12.6,6.3 12.8,6.4 C13,6.6 13,6.9 12.9,7.1 L9.3,11.3 C9.2,11.5 9.1,11.6 8.9,11.6 L8.9,11.6 Z"
                                        id="Shape"
                                        fill="#4CDB58"></path>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span>
                                    </li>
                                    <li>
                                        <p class="grey">Документы не подтверждены</p>
                                    </li>
                                </ul>
                                <a href="#" class="btn btn-block red">Добавить другие подтверждения</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-6">
                        <div class="thumbnail award">
                            <div class="blockAward">
                                <svg width="30px" height="30px" viewBox="0 0 30 30">
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="UI13.1----Профиль-пользователя---дефолт"
                                                transform="translate(-458.000000, -395.000000)" fill-rule="nonzero">
                                            <g id="Group-2" transform="translate(210.000000, 151.000000)">
                                                <g id="Group-43" transform="translate(66.000000, 244.000000)">
                                                    <g id="Group-8-Copy" transform="translate(144.333333, 0.000000)">
                                                        <g id="icon_2" transform="translate(37.666667, 0.000000)">
                                                            <path d="M12.3,18.4 C12.2,18.4 12.1,18.4 12,18.3 C11.3,18.1 11.1,17.3 10.8,16.6 C10.7,16.2 10.5,15.7 10.3,15.6 C10.1,15.5 9.6,15.5 9.2,15.5 C8.5,15.5 7.6,15.5 7.2,14.9 C6.8,14.3 7,13.6 7.3,12.8 C7.4,12.4 7.6,11.9 7.5,11.7 C7.4,11.5 7,11.2 6.7,11 C6.1,10.6 5.4,10.1 5.4,9.3 C5.4,8.5 6.1,8.1 6.7,7.6 C7,7.4 7.5,7.1 7.5,6.9 C7.6,6.7 7.4,6.2 7.3,5.8 C7.1,5.1 6.8,4.3 7.2,3.7 C7.6,3.1 8.5,3.1 9.2,3.1 C9.6,3.1 10.2,3.1 10.3,3 C10.5,2.9 10.6,2.4 10.8,2 C11,1.3 11.3,0.5 12,0.3 C12.7,0.1 13.3,0.6 13.9,1 C14.2,1.3 14.7,1.6 14.9,1.6 C15.1,1.6 15.6,1.3 15.9,1 C16.5,0.5 17.1,0.1 17.8,0.3 C18.5,0.5 18.7,1.3 19,2 C19.1,2.4 19.3,2.9 19.5,3 C19.7,3.1 20.2,3.1 20.6,3.1 C21.3,3.1 22.2,3.1 22.6,3.7 C23,4.3 22.8,5 22.5,5.8 C22.4,6.2 22.2,6.7 22.3,6.9 C22.4,7.1 22.8,7.4 23.1,7.6 C23.7,8 24.4,8.5 24.4,9.3 C24.4,10.1 23.7,10.5 23.1,11 C22.8,11.2 22.3,11.5 22.3,11.7 C22.2,11.9 22.4,12.4 22.5,12.8 C22.7,13.5 23,14.3 22.6,14.9 C22.2,15.5 21.3,15.5 20.6,15.5 C20.2,15.5 19.6,15.5 19.5,15.6 C19.3,15.7 19.2,16.2 19,16.6 C18.8,17.3 18.5,18.1 17.8,18.3 C17.1,18.5 16.5,18 15.9,17.6 C15.6,17.3 15.1,17 14.9,17 C14.7,17 14.2,17.3 13.9,17.6 C13.4,18 12.8,18.4 12.3,18.4 Z M9.5,14.4 C10,14.4 10.5,14.4 10.9,14.7 C11.3,15 11.5,15.7 11.7,16.3 C11.8,16.7 12,17.3 12.2,17.4 C12.4,17.5 12.9,17.1 13.2,16.8 C13.7,16.4 14.3,16 14.8,16 C15.4,16 15.9,16.4 16.4,16.8 C16.7,17 17.2,17.4 17.4,17.4 C17.6,17.3 17.8,16.7 17.9,16.3 C18.1,15.7 18.3,15.1 18.7,14.7 C19.2,14.4 19.8,14.4 20.4,14.4 C20.8,14.4 21.5,14.4 21.6,14.3 C21.7,14.1 21.5,13.5 21.4,13.2 C21.2,12.6 21,12 21.2,11.4 C21.4,10.9 21.9,10.5 22.4,10.2 C22.8,10 23.3,9.6 23.3,9.4 C23.3,9.2 22.8,8.8 22.4,8.6 C21.9,8.2 21.4,7.9 21.2,7.4 C21,6.9 21.2,6.2 21.4,5.6 C21.5,5.2 21.7,4.6 21.6,4.5 C21.5,4.3 20.8,4.4 20.4,4.4 C19.8,4.4 19.1,4.4 18.7,4.1 C18.3,3.8 18.1,3.1 17.9,2.5 C17.8,2.1 17.6,1.5 17.4,1.4 C17.2,1.3 16.7,1.7 16.4,2 C15.9,2.4 15.3,2.8 14.8,2.8 C14.2,2.8 13.7,2.4 13.2,2 C12.9,1.8 12.4,1.4 12.2,1.4 C12,1.5 11.8,2.1 11.7,2.5 C11.5,3.1 11.3,3.7 10.9,4.1 C10.4,4.4 9.8,4.4 9.2,4.4 C8.8,4.4 8.1,4.4 8,4.5 C7.9,4.7 8.1,5.3 8.2,5.6 C8.4,6.2 8.6,6.8 8.4,7.4 C8.2,7.9 7.7,8.3 7.2,8.6 C6.8,8.8 6.3,9.2 6.3,9.4 C6.3,9.6 6.8,10 7.2,10.2 C7.7,10.6 8.2,10.9 8.4,11.4 C8.6,11.9 8.4,12.6 8.2,13.2 C8.1,13.6 7.9,14.2 8,14.3 C8.1,14.5 8.8,14.4 9.2,14.4 L9.5,14.4 Z"
                                                                    id="Shape" fill="#66BC80"></path>
                                                            <path d="M14.9,13 C12.8,13 11.2,11.3 11.2,9.3 C11.2,7.2 12.9,5.6 14.9,5.6 C17,5.6 18.6,7.3 18.6,9.3 C18.6,11.4 16.9,13 14.9,13 Z M14.9,6.6 C13.4,6.6 12.2,7.8 12.2,9.3 C12.2,10.8 13.4,12 14.9,12 C16.4,12 17.6,10.8 17.6,9.3 C17.6,7.8 16.4,6.6 14.9,6.6 Z"
                                                                    id="Shape" fill="#66BC80"></path>
                                                            <path d="M26.8,22.7 L29,19.6 C29.1,19.4 29.1,19.2 29,19.1 C28.9,18.9 28.7,18.8 28.6,18.8 L19.3,18.8 L19.1,18.8 L19.1,18.8 L19.1,18.8 C19,18.8 19,18.9 18.9,18.9 C18.9,18.9 18.9,18.9 18.9,19 C18.9,19.1 18.8,19.2 18.8,19.2 L18.8,21.5 L11,21.5 L11,19.2 C11,19.1 11,19 10.9,19 C10.9,19 10.9,19 10.9,18.9 C10.9,18.8 10.8,18.8 10.7,18.8 L10.7,18.8 L10.7,18.8 L10.5,18.8 L1.2,18.8 C1,18.8 0.8,18.9 0.8,19.1 C0.7,19.3 0.7,19.5 0.8,19.6 L3,22.7 L0.8,25.8 C0.7,26 0.7,26.2 0.8,26.3 C0.9,26.5 1.1,26.6 1.2,26.6 L6.8,26.6 L6.8,28.9 C6.8,29.2 7,29.4 7.3,29.4 L22.6,29.4 C22.9,29.4 23.1,29.2 23.1,28.9 L23.1,26.6 L28.7,26.6 C28.9,26.6 29.1,26.5 29.1,26.3 C29.2,26.1 29.2,25.9 29.1,25.8 L26.8,22.7 Z M19.8,20.4 L21.2,21.6 L19.8,21.6 L19.8,20.4 Z M8.6,21.6 L10,20.4 L10,21.6 L8.6,21.6 Z M2.1,25.6 L4,23 C4.1,22.8 4.1,22.6 4,22.4 L2.1,19.8 L9.1,19.8 L6.9,21.7 L6.9,21.7 C6.9,21.7 6.9,21.8 6.8,21.8 C6.8,21.8 6.8,21.8 6.8,21.9 L6.8,22 L6.8,22.1 L6.8,22.1 L6.8,25.6 L2.1,25.6 Z M22,28.4 L7.7,28.4 L7.7,22.6 L22,22.6 L22,28.4 Z M23,25.6 L23,22.1 L23,22.1 L23,22 L23,21.9 C23,21.9 23,21.9 23,21.8 C23,21.8 23,21.7 22.9,21.7 L22.9,21.7 L20.7,19.8 L27.7,19.8 L25.8,22.4 C25.7,22.6 25.7,22.8 25.8,23 L27.7,25.6 L23,25.6 Z"
                                                                    id="Shape" fill="#A0A0A0"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                                <div class="caption">
                                    <h4 class="text-center">Лучший хозяин</h4>
                                </div>
                            </div>

                            <div class="infAward text-left" style="display: none">
                                <div class="title">
                                    <h4 class="titleAward">Лучший хозяин</h4>
                                    <span>
                                        <svg class="close" width="12px" height="12px" viewBox="0 0 12 12">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
        <g id="UI13.2----Мой-профиль-Подсказки-при-наведении-на-награды-Верификация-наград"
                transform="translate(-375.000000, -603.000000)"
                stroke="#FFFFFF"
                fill="#FFFFFF">
            <g id="product-copy-5" transform="translate(10.000000, 578.000000)">
                <g id="close" transform="translate(358.000000, 18.000000)">
                    <g transform="translate(7.944444, 7.944444)" id="Combined-Shape">
                        <path d="M5.05555556,4.03417909 L1.35328082,0.331904362 C1.0712355,0.0498590399 0.613949684,0.0498590399 0.331904362,0.331904362 L0.331904362,1.35328082 L4.03417909,5.05555556 L0.246359773,8.84337488 C-0.0356855484,9.1254202 -0.0356855484,9.58270602 0.246359773,9.86475134 C0.528405095,10.1467967 0.985690913,10.1467967 1.26773624,9.86475134 L5.05555556,6.07693202 L8.84337488,9.86475134 C9.1254202,10.1467967 9.58270602,10.1467967 9.86475134,9.86475134 C10.1467967,9.58270602 10.1467967,9.1254202 9.86475134,8.84337488 L6.07693202,5.05555556 L9.77920675,1.35328082 C10.0612521,1.0712355 10.0612521,0.613949684 9.77920675,0.331904362 L8.75783029,0.331904362 L5.05555556,4.03417909 Z"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                    </span>
                                </div>
                                <p>
                                    Награда "Лучший хозяин" присваивается Арендодателям, если средний балл по отзывам
                                    Арендаторов, не меньше чем 4,7 балла
                                </p>
                            </div>

                        </div>
                    </div>

                    <div class="clearfix hidden-sm hidden-md hidden-lg"></div>

                    <div class="col-sm-3 col-xs-6">
                        <div class="thumbnail award">
                            <div class="blockAward">
                                <svg width="30px" height="30px" viewBox="0 0 30 30">
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="UI13.1----Профиль-пользователя---дефолт"
                                                transform="translate(-603.000000, -395.000000)" fill-rule="nonzero">
                                            <g id="Group-2" transform="translate(210.000000, 151.000000)">
                                                <g id="Group-43" transform="translate(66.000000, 244.000000)">
                                                    <g id="Group-8-Copy-2" transform="translate(288.666667, 0.000000)">
                                                        <g id="icon_3" transform="translate(38.333333, 0.000000)">
                                                            <path d="M5.1,26.1 L1.2,26.1 C0.9,26.1 0.7,25.9 0.7,25.6 L0.7,19 C0.7,16 3.2,13.5 6.2,13.5 C6.5,13.5 6.7,13.7 6.7,14 C6.7,14.3 6.5,14.5 6.2,14.5 C3.7,14.5 1.7,16.5 1.7,19 L1.7,25.1 L5.1,25.1 C5.4,25.1 5.6,25.3 5.6,25.6 C5.6,25.9 5.4,26.1 5.1,26.1 Z"
                                                                    id="Shape" fill="#A0A0A0"></path>
                                                            <path d="M7,14.4 C5.1,14.4 3.5,12.1 3.5,10.2 C3.5,8.6 3.5,6 7,6 C10.5,6 10.5,8.7 10.5,10.2 C10.5,12.1 8.9,14.4 7,14.4 Z M7,7 C4.7,7 4.5,8.3 4.5,10.2 C4.5,11.6 5.7,13.4 7,13.4 C8.3,13.4 9.5,11.6 9.5,10.2 C9.5,8.2 9.3,7 7,7 Z"
                                                                    id="Shape" fill="#A0A0A0"></path>
                                                            <path d="M22.9,14.4 C21,14.4 19.4,12.1 19.4,10.2 C19.4,8.6 19.4,6 22.9,6 C26.4,6 26.4,8.7 26.4,10.2 C26.4,12.1 24.8,14.4 22.9,14.4 Z M22.9,7 C20.6,7 20.4,8.3 20.4,10.2 C20.4,11.6 21.6,13.4 22.9,13.4 C24.2,13.4 25.4,11.6 25.4,10.2 C25.4,8.2 25.2,7 22.9,7 Z"
                                                                    id="Shape" fill="#A0A0A0"></path>
                                                            <path d="M28.6,26.1 L24.7,26.1 C24.4,26.1 24.2,25.9 24.2,25.6 C24.2,25.3 24.4,25.1 24.7,25.1 L28.1,25.1 L28.1,20 C28.1,17 25.6,14.5 22.6,14.5 C22.3,14.5 22.1,14.3 22.1,14 C22.1,13.7 22.3,13.5 22.6,13.5 C26.2,13.5 29.1,16.4 29.1,20 L29.1,25.6 C29.1,25.9 28.9,26.1 28.6,26.1 Z"
                                                                    id="Shape" fill="#A0A0A0"></path>
                                                            <path d="M24.7,29.7 L5.1,29.7 C4.8,29.7 4.6,29.5 4.6,29.2 L4.6,18.5 C4.6,15.5 7.1,13 10.1,13 L18.7,13 C22.3,13 25.2,15.9 25.2,19.5 L25.2,29.2 C25.2,29.5 25,29.7 24.7,29.7 Z M5.6,28.7 L24.2,28.7 L24.2,19.5 C24.2,16.5 21.7,14 18.7,14 L10.1,14 C7.6,14 5.6,16 5.6,18.5 L5.6,28.7 Z"
                                                                    id="Shape" fill="#4B97E2"></path>
                                                            <path d="M14.9,17.6 C13.2,17.6 11.8,16.2 11.8,14.5 C11.8,13.9 12,13.4 12.2,12.9 C12.3,12.7 12.7,12.6 12.9,12.7 C13.1,12.8 13.2,13.2 13.1,13.4 C12.9,13.7 12.8,14.1 12.8,14.5 C12.8,15.6 13.7,16.6 14.9,16.6 C16,16.6 17,15.7 17,14.5 C17,14.2 16.9,13.9 16.8,13.7 C16.7,13.4 16.8,13.2 17.1,13 C17.4,12.9 17.6,13 17.8,13.3 C18,13.7 18.1,14.1 18.1,14.5 C18,16.2 16.6,17.6 14.9,17.6 Z"
                                                                    id="Shape" fill="#4B97E2"></path>
                                                            <path d="M14.9,14 C11.9,14 9.4,10.3 9.4,7.3 C9.4,4.6 9.4,0.6 14.9,0.6 C20.4,0.6 20.4,4.6 20.4,7.3 C20.4,10.4 17.9,14 14.9,14 Z M14.9,1.6 C10.4,1.6 10.4,4.5 10.4,7.3 C10.4,9.9 12.5,13 14.9,13 C17.3,13 19.4,9.9 19.4,7.3 C19.4,4.5 19.4,1.6 14.9,1.6 Z"
                                                                    id="Shape" fill="#4B97E2"></path>
                                                            <path d="M8.7,29.7 C8.4,29.7 8.2,29.5 8.2,29.2 L8.2,24.4 C8.2,24.1 8.4,23.9 8.7,23.9 C9,23.9 9.2,24.1 9.2,24.4 L9.2,29.2 C9.2,29.5 9,29.7 8.7,29.7 Z"
                                                                    id="Shape" fill="#4B97E2"></path>
                                                            <path d="M21.1,29.7 C20.8,29.7 20.6,29.5 20.6,29.2 L20.6,24.4 C20.6,24.1 20.8,23.9 21.1,23.9 C21.4,23.9 21.6,24.1 21.6,24.4 L21.6,29.2 C21.6,29.5 21.4,29.7 21.1,29.7 Z"
                                                                    id="Shape" fill="#4B97E2"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                                <div class="caption">
                                    <h4 class="text-center">Активный участник сообщества</h4>
                                </div>
                            </div>

                            <div class="infAward text-left" style="display: none">
                                <div class="title">
                                    <h4 class="titleAward">Активный участник сообщества</h4>
                                    <span>
                                        <svg class="close" width="12px" height="12px" viewBox="0 0 12 12">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
        <g id="UI13.2----Мой-профиль-Подсказки-при-наведении-на-награды-Верификация-наград"
                transform="translate(-375.000000, -603.000000)"
                stroke="#FFFFFF"
                fill="#FFFFFF">
            <g id="product-copy-5" transform="translate(10.000000, 578.000000)">
                <g id="close" transform="translate(358.000000, 18.000000)">
                    <g transform="translate(7.944444, 7.944444)" id="Combined-Shape">
                        <path d="M5.05555556,4.03417909 L1.35328082,0.331904362 C1.0712355,0.0498590399 0.613949684,0.0498590399 0.331904362,0.331904362 L0.331904362,1.35328082 L4.03417909,5.05555556 L0.246359773,8.84337488 C-0.0356855484,9.1254202 -0.0356855484,9.58270602 0.246359773,9.86475134 C0.528405095,10.1467967 0.985690913,10.1467967 1.26773624,9.86475134 L5.05555556,6.07693202 L8.84337488,9.86475134 C9.1254202,10.1467967 9.58270602,10.1467967 9.86475134,9.86475134 C10.1467967,9.58270602 10.1467967,9.1254202 9.86475134,8.84337488 L6.07693202,5.05555556 L9.77920675,1.35328082 C10.0612521,1.0712355 10.0612521,0.613949684 9.77920675,0.331904362 L8.75783029,0.331904362 L5.05555556,4.03417909 Z"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                    </span>
                                </div>
                                <p>
                                    Данная награда выдается тем, кто активно помогает другим пользователям разобраться в
                                    сервисе (назв.сайта).</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-6">
                        <div class="thumbnail award">
                            <div class="blockAward">
                                <svg width="33px" height="30px" viewBox="0 0 33 30">
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="UI13.1----Профиль-пользователя---дефолт"
                                                transform="translate(-745.000000, -395.000000)" fill-rule="nonzero">
                                            <g id="Group-2" transform="translate(210.000000, 151.000000)">
                                                <g id="Group-43" transform="translate(66.000000, 244.000000)">
                                                    <g id="Group-8-Copy-3" transform="translate(433.000000, 0.000000)">
                                                        <g id="icon_4" transform="translate(36.000000, 0.000000)">
                                                            <path d="M19.5,15.7 C19.4,15.7 19.3,15.7 19.3,15.6 L16.6,14.2 L13.9,15.6 C13.7,15.7 13.5,15.7 13.4,15.6 C13.2,15.5 13.2,15.3 13.2,15.1 L13.7,12.1 L11.5,10 C11.4,9.9 11.3,9.7 11.4,9.5 C11.5,9.3 11.6,9.2 11.8,9.2 L14.8,8.8 L16.2,6 C16.4,5.7 16.9,5.7 17.1,6 L18.5,8.8 L21.5,9.2 C21.7,9.2 21.8,9.4 21.9,9.5 C22,9.7 21.9,9.9 21.8,10 L19.6,12.1 L20.1,15.1 C20.1,15.3 20.1,15.5 19.9,15.6 C19.7,15.7 19.6,15.7 19.5,15.7 Z M16.5,13.1 C16.6,13.1 16.7,13.1 16.7,13.2 L18.8,14.3 L18.4,12 C18.4,11.8 18.4,11.7 18.5,11.6 L20.2,10 L17.9,9.7 C17.7,9.7 17.6,9.6 17.5,9.4 L16.5,7.3 L15.5,9.4 C15.4,9.5 15.3,9.6 15.1,9.7 L12.8,10 L14.5,11.6 C14.6,11.7 14.7,11.9 14.6,12 L14.2,14.3 L16.3,13.2 C16.4,13.2 16.5,13.1 16.5,13.1 Z"
                                                                    id="Shape" fill="#E8B434"></path>
                                                            <path d="M31.6,7.3 C31,6.5 30,6 28.9,6 L28.9,6 C28.1,6 27.4,6.2 26.7,6.7 L26.7,3.9 L27.2,3.9 C28.2,3.9 29,3.1 29,2.1 L29,1.9 C29,0.9 28.2,0.1 27.2,0.1 L5.8,0.1 C4.8,0.1 4,0.9 4,1.9 L4,2.1 C4,3.1 4.8,3.9 5.8,3.9 L6.3,3.9 L6.3,6.7 C5.7,6.3 4.9,6 4.2,6 L4.2,6 L4.2,6 L4.2,6 C3.1,6 2.1,6.5 1.5,7.3 C0.9,8.1 0.7,9.1 1,10.1 C1.2,10.9 1.7,11.6 2.5,12.1 L5.7,14.4 C6.2,14.8 6.2,15.3 6.1,15.7 C6,15.9 5.8,16.4 5.1,16.4 L5.1,16.9 L5.1,17.4 C6,17.4 6.7,16.8 7,16 C7.3,15.1 7,14.1 6.2,13.6 L3,11.3 C2.6,11 2.1,10.5 1.9,9.8 C1.7,9.1 1.8,8.4 2.2,7.8 C2.6,7.2 3.3,6.9 4.1,6.9 C4.9,6.9 5.7,7.3 6.3,7.9 L6.3,10.4 C6.3,14.9 9.3,18.8 13.3,20.1 L13.3,20.1 C13.3,21 13.7,21.8 14.3,22.4 C13.4,24.1 11.6,25.3 9.7,25.3 C9.1,25.3 8.5,25.8 8.5,26.5 L8.5,29.4 C8.5,29.7 8.7,29.9 9,29.9 L24,29.9 C24.3,29.9 24.5,29.7 24.5,29.4 L24.5,26.5 C24.5,25.9 24,25.3 23.3,25.3 C21.3,25.3 19.5,24.2 18.7,22.4 C19.3,21.8 19.7,21 19.7,20.1 L19.7,20.1 C23.8,18.8 26.7,14.9 26.7,10.4 L26.7,7.9 C27.2,7.3 28,6.9 28.9,6.9 L28.9,6.9 C29.7,6.9 30.4,7.2 30.8,7.8 C31.2,8.3 31.3,9 31.1,9.8 C30.9,10.5 30.4,11 30,11.3 L26.8,13.6 C26.1,14.1 25.8,15.1 26,16 C26.3,16.8 27,17.4 27.8,17.4 L27.9,17.4 C28.2,17.4 28.4,17.2 28.4,16.9 C28.4,16.6 28.2,16.4 27.9,16.4 C27.2,16.4 27,15.8 27,15.7 C26.9,15.3 26.9,14.8 27.4,14.4 L30.6,12.1 C31.4,11.5 31.9,10.8 32.1,10.1 C32.4,9.1 32.2,8.1 31.6,7.3 Z M4.9,2.1 L4.9,1.9 C4.9,1.4 5.3,1.1 5.7,1.1 L27.2,1.1 C27.7,1.1 28,1.5 28,1.9 L28,2.1 C28,2.6 27.6,2.9 27.2,2.9 L5.8,2.9 C5.3,2.9 4.9,2.5 4.9,2.1 Z M23.4,26.3 C23.5,26.3 23.6,26.4 23.6,26.5 L23.6,28.9 L9.6,28.9 L9.6,26.5 C9.6,26.4 9.7,26.3 9.8,26.3 C12.1,26.3 14.2,25 15.2,23 C15.6,23.2 16.1,23.3 16.6,23.3 C17.1,23.3 17.6,23.2 18,23 C19,25 21.1,26.3 23.4,26.3 Z M16.5,22.3 C15.4,22.3 14.5,21.5 14.3,20.4 C15,20.6 15.7,20.6 16.5,20.6 C17.3,20.6 18,20.5 18.7,20.4 C18.6,21.5 17.7,22.3 16.5,22.3 Z M16.5,19.6 C11.4,19.6 7.3,15.5 7.3,10.4 L7.3,3.9 L25.8,3.9 L25.8,10.4 C25.8,15.5 21.6,19.6 16.5,19.6 Z"
                                                                    id="Shape" fill="#A0A0A0"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                                <div class="caption">
                                    <h4 class="text-center">Награда от компании <span>Wearent</span></h4>
                                </div>
                            </div>

                            <div class="infAward text-left" style="display: none">
                                <div class="title">
                                    <h4 class="titleAward">Награда от компании <span class="red">Wearent</span></h4>
                                    <span>
                                        <svg class="close" width="12px" height="12px" viewBox="0 0 12 12">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
        <g id="UI13.2----Мой-профиль-Подсказки-при-наведении-на-награды-Верификация-наград"
                transform="translate(-375.000000, -603.000000)"
                stroke="#FFFFFF"
                fill="#FFFFFF">
            <g id="product-copy-5" transform="translate(10.000000, 578.000000)">
                <g id="close" transform="translate(358.000000, 18.000000)">
                    <g transform="translate(7.944444, 7.944444)" id="Combined-Shape">
                        <path d="M5.05555556,4.03417909 L1.35328082,0.331904362 C1.0712355,0.0498590399 0.613949684,0.0498590399 0.331904362,0.331904362 L0.331904362,1.35328082 L4.03417909,5.05555556 L0.246359773,8.84337488 C-0.0356855484,9.1254202 -0.0356855484,9.58270602 0.246359773,9.86475134 C0.528405095,10.1467967 0.985690913,10.1467967 1.26773624,9.86475134 L5.05555556,6.07693202 L8.84337488,9.86475134 C9.1254202,10.1467967 9.58270602,10.1467967 9.86475134,9.86475134 C10.1467967,9.58270602 10.1467967,9.1254202 9.86475134,8.84337488 L6.07693202,5.05555556 L9.77920675,1.35328082 C10.0612521,1.0712355 10.0612521,0.613949684 9.77920675,0.331904362 L8.75783029,0.331904362 L5.05555556,4.03417909 Z"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                    </span>
                                </div>
                                <p>
                                    Мы благодарим данного пользователя за то, что он помог нам с переводом сайта на
                                    армянский язык.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if (mb_strlen($model['description'], 'UTF-8')): ?>
                    <div class="aboutMeinf">
                        <h4><?= \Yii::t('UI13.1', '$ABOUT_ME_LABEL$'); ?>:</h4>
                        <p class="readmore"><?= Html::encode($model['description']); ?></p>
                    </div>
                <?php endif; ?>
            </div>

            <!--            САЙДБАР СТОРІНКИ-->
            <div class="sidebarProf col-sm-12 col-md-4 col-xs-12">

                <!--                ВЕРХНІЙ САЙДБАР-->
                <div class="addition styleBlock ">
                    <p class="text-left blackFon"><?= \Yii::t('UI13.1', '$RIGHTSIDE_STATISTICS_TITLE$'); ?></p>
                    <div class="paddingContent">
                        <?php if (count($model['video'])): ?>
                            <div class="video text-center">
                                <div class="wrapVideo">
                                    <?= Html::img(ArrayHelper::getValue($model,
                                        'video.0.poster_path') ?: '/img/80734072_640.jpg',
                                        ['class' => 'img-responsive']); ?>

                                    <div class="openModal">
                                        <button type="button"
                                                class="openModalVideo"
                                                data-toggle="modal"
                                                data-target="#modalVideo">
                                            <svg width="60px" height="60px" viewBox="0 0 60 60">
                                                <g id="Page-1"
                                                        stroke="none"
                                                        stroke-width="1"
                                                        fill="none"
                                                        fill-rule="evenodd"
                                                        fill-opacity="0.8">
                                                    <g class="re" transform="translate(-440.000000, -1195.000000)">
                                                        <g id="Group" transform="translate(210.000000, 151.000000)">
                                                            <g id="Group-6" transform="translate(19.000000, 84.000000)">
                                                                <g id="upload"
                                                                        transform="translate(1.000000, 49.000000)">
                                                                    <g id="Group-8"
                                                                            transform="translate(0.000000, 355.000000)">
                                                                        <g id="mini_photo-copy-6"
                                                                                transform="translate(0.000000, 426.000000)">
                                                                            <g id="play_icon"
                                                                                    transform="translate(210.000000, 130.000000)">
                                                                                <path d="M30,60 C13.4314575,60 0,46.5685425 0,30 C0,13.4314575 13.4314575,0 30,0 C46.5685425,0 60,13.4314575 60,30 C60,46.5685425 46.5685425,60 30,60 Z M25,43 L44,30.5 L25,18 L25,43 Z"
                                                                                        id="Combined-Shape"></path>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="ratingBlock">
                            <div class="owner">
                                <p><?= \Yii::t('UI13.1', '$RIGHTSIDE_RATING_OWNER_LABEL$'); ?></p>
                                <div class="numberReting">
                                    <span class="numberOw">??</span>
                                    <span class="rateyo"></span>
                                </div>
                            </div>

                            <div class="tenant owner">
                                <p><?= \Yii::t('UI13.1', '$RIGHTSIDE_RATING_TENANT_LABEL$'); ?></p>
                                <div class="numberReting">
                                    <span class="numberTen">??</span>
                                    <span class="rateyo"></span>
                                </div>
                            </div>
                        </div>

                        <a href="#"
                                class="btnSelected btn btn-block"
                                data-toggle="modal"
                                data-target="#selected"><?= \Yii::t('UI13.1', '$RIGHTSIDE_FAVORITE_BTN$'); ?>
                            (<?= Html::tag('span', $favouriteQuery->count()); ?>)</a>

                        <div class="mediaBlock">
                            <div class="media">
                                <a class="media-left" href="#">
                                    <img src="/img/editProfile/job.svg" alt="job">
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading"><?= \Yii::t('UI13.1', '$RIGHTSIDE_WORK_TITLE$'); ?></h4>
                                    <p><?= Html::encode(ArrayHelper::getValue($model, 'company', '')); ?></p>
                                </div>
                            </div>
                            <div class="media">
                                <a class="media-left" href="#">
                                    <img src="/img/editProfile/language.svg" alt="">
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading"><?= \Yii::t('UI13.1',
                                            '$RIGHTSIDE_LANGUAGE_TITLE$'); ?></h4>
                                    <p><?= mb_strtolower(implode(', ', $l), 'UTF-8'); ?></p>
                                </div>
                            </div>
                        </div>

                        <div class="confirmed">
                            <ul class="list-unstyled">
                                <li class="frequency"><?= \Yii::t('UI13.1', '$RIGHTSIDE_FREQUENCY_ANSWER_LABEL$'); ?>:
                                    <span>??</span></li>
                                <li class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="96"
                                            aria-valuemin="0" aria-valuemax="100" style="width: 96%;">
                                    </div>
                                </li>
                                <li class="replet frequency"><?= \Yii::t('UI13.1',
                                        '$RIGHTSIDE_TIME_RESPONSE_LABEL$'); ?>: <span>??</span></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <!--                НИЖНІЙ САЙДБАР-->
                <div class="advert styleBlock ">
                    <p class="text-left blackFon"><?= \Yii::t('UI13.1', '$RIGHTSIDE_PUBLICATIONS_TITLE$'); ?></p>
                    <div class="paddingContent">
                        <div class="blockMedia">
                            <?php
                            /** @var \common\models\Product $item */
                            foreach ($model['user']['products'] as $item): ?>
                                <div class="media">
                                    <?php
                                    $query = $item->getMedia()->andWhere([
                                        'or like',
                                        'link',
                                        ['%.png', '%.jpg', '%.gif'],
                                        false,
                                    ])->andWhere(['is_deleted' => 0])->limit(1);
                                    $m = $query->one();

                                    print Html::a(Html::img(ArrayHelper::getValue($m,
                                        'link') ?: '/img/category/imgCategory/defolt.svg'), "/{$item['slug']}",
                                        ['class' => 'media-left']);
                                    ?>
                                    <div class="media-body">
                                        <h4 class="media-heading"><?= Html::encode($item['name']); ?></h4>
                                        <p><?= \Yii::$app->formatter->asDate($item['created_at'], 'long'); ?></p>
                                    </div>
                                </div>
                            <?php endforeach; ?>

                        </div>
                    </div>
                    <div class="bottomPlaseBtn">
                        <a href="#"
                                data-toggle="modal"
                                data-target="#userProduct"
                                class="btn btn-block showAllMedia"><?= \Yii::t('UI13.1',
                                '$RIGHTSIDE_ALL_PUBLICATIONS_BTN$'); ?></a>
                    </div>
                </div>
            </div>

            <!--            БЛОК ДЛЯ ВІДГУКІВ-->
            <div class="blockFedbackHear styleBlock col-sm-12 col-md-8 col-xs-12">

                <h3>
                    <svg width="29px" height="26px" viewBox="0 0 29 26">
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="UI13.13-Профиль-Бизнес-аккаунта" transform="translate(-20.000000, -2131.000000)"
                                    fill-rule="nonzero" fill="#AFAFAF">
                                <g id="review" transform="translate(0.000000, 2111.000000)">
                                    <g id="review_icon" transform="translate(20.000000, 20.000000)">
                                        <path d="M26.7,0 L5.8,0 C4.6,0 3.5,1 3.5,2.3 L3.5,2.8 C3.5,3.1 3.7,3.3 4,3.3 C4.3,3.3 4.5,3.1 4.5,2.8 L4.5,2.3 C4.5,1.6 5.1,1 5.8,1 L26.7,1 C27.4,1 28,1.6 28,2.3 L28,17.4 C28,18.1 27.4,18.7 26.7,18.7 C26.4,18.7 26.2,18.9 26.2,19.2 C26.2,19.5 26.4,19.7 26.7,19.7 C27.9,19.7 29,18.7 29,17.4 L29,2.3 C28.9,1 27.9,0 26.7,0 Z"
                                                id="Shape"></path>
                                        <path d="M22.8,3.9 L1.9,3.9 C1.1,3.9 0.4,4.6 0.4,5.4 L0.4,20.5 C0.4,21.3 1.1,22 1.9,22 L4.5,22 L4.5,25.2 C4.5,25.4 4.6,25.6 4.8,25.7 L5,25.7 C5.1,25.7 5.2,25.7 5.3,25.6 L9.3,22 L22.8,22 C23.6,22 24.3,21.3 24.3,20.5 L24.3,5.4 C24.3,4.6 23.6,3.9 22.8,3.9 Z M23.3,20.6 C23.3,20.9 23.1,21.1 22.8,21.1 L9,21.1 C8.9,21.1 8.8,21.1 8.7,21.2 L5.4,24.1 L5.4,21.5 C5.4,21.2 5.2,21 4.9,21 L1.8,21 C1.5,21 1.3,20.8 1.3,20.5 L1.3,5.4 C1.3,5.1 1.5,4.9 1.8,4.9 L22.7,4.9 C23,4.9 23.2,5.1 23.2,5.4 L23.2,20.6 L23.3,20.6 Z"
                                                id="Shape"></path>
                                        <path d="M17.4,10.2 L7.3,10.2 C7,10.2 6.8,10.4 6.8,10.7 C6.8,11 7,11.2 7.3,11.2 L17.4,11.2 C17.7,11.2 17.9,11 17.9,10.7 C17.9,10.4 17.6,10.2 17.4,10.2 Z"
                                                id="Shape"></path>
                                        <path d="M17.4,14.8 L7.3,14.8 C7,14.8 6.8,15 6.8,15.3 C6.8,15.6 7,15.8 7.3,15.8 L17.4,15.8 C17.7,15.8 17.9,15.6 17.9,15.3 C17.9,15 17.6,14.8 17.4,14.8 Z"
                                                id="Shape"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>&nbsp;Оставьте отзыв
                </h3>
                <div class="blockFeedback">
                    <form action="" method="post">
                        <textarea id="comment" placeholder="Cуществует целый ряд различий"></textarea>
                        <div class="inlineBlockBtn">
                            <label class="fotoBtn">
                                <svg class="attach" width="18px" height="19px" viewBox="0 0 18 19">
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g id="attach" transform="translate(-536.000000, -920.000000)"
                                                fill-rule="nonzero">
                                            <g id="review" transform="translate(210.000000, 751.000000)">
                                                <g id="Group-25" transform="translate(20.000000, 63.000000)">
                                                    <g id="button" transform="translate(284.000000, 98.000000)">
                                                        <path d="M27.1461851,10.7088095 L27.1461851,23.4189583 C27.1461851,23.4189583 27.1461851,28.2113113 31.313447,28.2113094 C35.4807089,28.2113076 35.4807089,23.4189583 35.4807089,23.4189583 L35.4807089,10.7088095 C35.4807089,10.1879018 34.5546507,10.1879018 34.5546507,10.7088095 L34.5546507,23.4189592 C34.5546507,23.4189592 34.8913988,27.2642045 31.313447,27.2642045 C28.0722433,27.2642045 28.0722433,23.4189592 28.0722433,23.4189592 L28.0722433,10.7088095 C28.0722433,10.7088095 28.0722433,8.32210592 30.3873888,8.32210497 C32.7025343,8.32210403 32.7025343,10.7088095 32.7025343,10.7088095 L32.7025343,23.4189592 C32.7025343,23.4189592 32.7025343,24.9130685 31.351245,24.9130675 C29.9999566,24.9130656 29.9243597,23.4189592 29.9243597,23.4189592 L29.9243597,11.3338988 C29.9243597,10.8129911 28.9983015,10.8129911 28.9983015,11.3338988 L28.9983015,23.4189583 C28.9983015,23.4189583 28.8256324,25.9548849 31.351245,25.954883 C33.6285925,25.9548811 33.6285925,23.4189583 33.6285925,23.4189583 L33.6285925,10.7088095 C33.6285925,10.7088095 33.6285925,7.37499905 30.3873888,7.375 C27.1461851,7.37500095 27.1461851,10.7088095 27.1461851,10.7088095 Z"
                                                                id="attach"
                                                                transform="translate(31.313447, 17.793155) rotate(45.000000) translate(-31.313447, -17.793155) "></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>&nbsp;Прикрепить фото <input type="file" accept="image/*" class="hidden"></label>
                            <button type="submit" class="submitBtn">Отправить</button>
                        </div>
                    </form>

                </div>
            </div>

            <!--            БЛОК ВІДГУКІВ-->
            <div class="blockFedback styleBlock col-sm-12 col-md-8 col-xs-12">
                <div class="blockAllFedback">
                    <div class="tableTop">
                        <div class="row">

                            <form action="" method="post">
                                <div class="col-xs-6 col-sm-3 blockCheck text-center">
                                    <label for="postsUser" class="postsUser styleLabel">Посты<br> пользователя</label>
                                    <input class="inAbsolute redCheckBox" id="postsUser" type="checkbox">
                                </div>
                                <div class="col-xs-6 col-sm-3 blockCheck text-center">
                                    <label for="postOwner" class="postsOwner styleLabel">Отзывы<br> хозяев</label>
                                    <input class="inAbsolute redCheckBox" id="postOwner" type="checkbox">
                                </div>
                                <div class="clearfix hidden-sm hidden-md hidden-lg "></div>
                                <div class="col-xs-6 col-sm-3 blockCheck text-center">
                                    <label for="postRent" class="postsRent styleLabel">Отзывы<br> арендаторов</label>
                                    <input class="inAbsolute redCheckBox" id="postRent" type="checkbox">
                                </div>
                                <div class="col-xs-6 col-sm-3 blockCheck text-center">
                                    <label for="postsRecomend" class="postsRocomed styleLabel">Рекомендации</label>
                                    <input class="inAbsolute redCheckBox" id="postsRecomend" type="checkbox">
                                </div>
                            </form>

                        </div>
                    </div>

                    <div class="postFedback">
                        <div class="postText">
                            <div class="fedbackAll">
                                <img src="/img/product/fotoFace.jpg" alt="" class="imgUserFedback">
                                <span class="greenInputFoto"></span>
                            </div>
                            <div class="textFedback">
                                <div class="userTextFedback">
                                    <div class="inlineTitle">
                                        <h4>Доба Дмитрий</h4>
                                        <span>• 1 час назад</span>
                                    </div>

                                    <p class="text-left howProduct">Товар: <span>Перфоратор на зло соседям</span></p>
                                    <a href="#" class="eventBtn">Сдал в оренду</a>
                                    <p class="textFedbackAll">Существует целый ряд различных факторов, оказывающих
                                        влияние
                                        на
                                        принятие решения покупателем.</p>
                                </div>

                                <?= \yii\bootstrap\ButtonDropdown::widget([
                                    'label'            => '<span class="circle"></span>
                            <span class="circle"></span>
                            <span class="circle"></span>',
                                    'encodeLabel'      => false,
                                    'dropdown'         => [
                                        'items'        => [
                                            [
                                                'label'  => '<svg class="imgLiPunct" width="18px" height="18px" viewBox="0 0 18 18"
                                                     version="1.1">

                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                       fill-rule="evenodd">
                                                        <g id="imgLiPunct"
                                                           transform="translate(-671.000000, -1156.000000)"
                                                           fill-rule="nonzero"
                                                        ">
                                                        <g id="reviews" transform="translate(210.000000, 1019.000000)">
                                                            <g id="Group-22"
                                                               transform="translate(21.000000, 86.000000)">
                                                                <g id="Group-20">
                                                                    <g id="Group-4">
                                                                        <g id="Group-24"
                                                                           transform="translate(422.000000, 26.000000)">
                                                                            <g id="edit_icon"
                                                                               transform="translate(18.000000, 25.000000)">
                                                                                <path d="M15.6,9.6 C15.3,9.6 15.1,9.8 15.1,10.1 L15.1,16.5 L1.7,16.5 L1.7,3.1 L8.2,3.1 C8.5,3.1 8.7,2.9 8.7,2.6 C8.7,2.3 8.5,2.1 8.2,2.1 L1.2,2.1 C0.9,2.1 0.7,2.3 0.7,2.6 L0.7,17 C0.7,17.3 0.9,17.5 1.2,17.5 L15.6,17.5 C15.9,17.5 16.1,17.3 16.1,17 L16.1,10.1 C16.1,9.9 15.9,9.6 15.6,9.6 Z"
                                                                                      id="Shape"></path>
                                                                                <path d="M17.7,3.8 L14.5,0.6 C14.3,0.4 14,0.4 13.8,0.6 L5.4,9 C5.3,9.1 5.3,9.2 5.3,9.3 L4.7,13 C4.7,13.2 4.7,13.3 4.8,13.4 C4.9,13.5 5,13.5 5.2,13.5 L5.3,13.5 L9,12.9 C9.1,12.9 9.2,12.8 9.3,12.8 L17.7,4.4 C17.9,4.3 17.9,4 17.7,3.8 Z M8.7,12 L5.8,12.4 L6.2,9.5 L11.8,3.9 L14.2,6.3 L8.7,12 Z M15.1,5.7 L12.7,3.3 L14.3,1.7 L16.7,4.1 L15.1,5.7 Z"
                                                                                      id="Shape"></path>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    </g>
                                                </svg>
                                                Редактировать',
                                                'url'    => '#',
                                                'encode' => false,
                                            ],
                                            [
                                                'label'  => ' <svg class="imgLiPunct" width="16px" height="15px" viewBox="0 0 16 15"
                                                     version="1.1">

                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                       fill-rule="evenodd">
                                                        <g id="imgLiPunct"
                                                           transform="translate(-671.000000, -1199.000000)">
                                                            <g id="reviews"
                                                               transform="translate(210.000000, 1019.000000)">
                                                                <g id="Group-22"
                                                                   transform="translate(21.000000, 86.000000)">
                                                                    <g id="Group-20">
                                                                        <g id="Group-4">
                                                                            <g id="Group-24"
                                                                               transform="translate(422.000000, 26.000000)">
                                                                                <g id="report_icon"
                                                                                   transform="translate(18.000000, 68.000000)">
                                                                                    <path d="M14.8,0.4 L1.8,0.4 C1.2,0.4 0.7,0.9 0.7,1.5 L0.7,10.9 C0.7,11.5 1.2,12 1.8,12 L3.2,12 L3.2,13.8 C3.2,14 3.3,14.2 3.5,14.3 L3.7,14.3 C3.8,14.3 3.9,14.3 4,14.2 L6.4,12 L14.7,12 C15.3,12 15.8,11.5 15.8,10.9 L15.8,1.5 C15.9,0.9 15.4,0.4 14.8,0.4 Z M14.9,10.9 C14.9,11 14.8,11 14.8,11 L6.3,11 C6.2,11 6.1,11 6,11.1 L4.3,12.6 L4.3,11.4 C4.3,11.1 4.1,10.9 3.8,10.9 L1.9,10.9 C1.8,10.9 1.8,10.8 1.8,10.8 L1.8,1.4 C1.8,1.3 1.9,1.3 1.9,1.3 L14.9,1.3 C15,1.3 15,1.4 15,1.4 L15,10.9 L14.9,10.9 Z"
                                                                                          id="Shape"
                                                                                          fill-rule="nonzero"></path>
                                                                                    <path d="M8,8 L9.072,8 L9.072,9 L8,9 L8,8 Z M8.04,3.288 L9.04,3.288 L9.04,4.936 L8.776,7.408 L8.304,7.408 L8.04,4.936 L8.04,3.288 Z"
                                                                                          id="!"
                                                                                          opacity="0.900000036"></path>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                                Сообщить о нарушении',
                                                'url'    => '#',
                                                'encode' => false,
                                            ],
                                            [
                                                'label'  => ' <svg class="imgLiPunct" width="14px" height="19px" viewBox="0 0 14 19"
                                                     version="1.1">

                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                       fill-rule="evenodd">
                                                        <g id="imgLiPunct"
                                                           transform="translate(-673.000000, -1235.000000)"
                                                           fill-rule="nonzero" stroke="#AFAFAF" stroke-width="0.3">
                                                            <g id="reviews"
                                                               transform="translate(210.000000, 1019.000000)">
                                                                <g id="Group-22"
                                                                   transform="translate(21.000000, 86.000000)">
                                                                    <g id="Group-20">
                                                                        <g id="Group-4">
                                                                            <g id="Group-24"
                                                                               transform="translate(422.000000, 26.000000)">
                                                                                <g id="delete_icon"
                                                                                   transform="translate(21.000000, 105.000000)">
                                                                                    <path d="M11.9953016,2.04505221 L8.84168254,2.04505221 L8.84168254,1.36682731 C8.84168254,0.617769076 8.27685714,0.01037751 7.58022222,0.01037751 L4.42660317,0.01037751 C3.72996825,0.01037751 3.16514286,0.617769076 3.16514286,1.36682731 L3.16514286,2.04505221 L0.0115238095,2.04505221 L0.0115238095,2.72327711 L0.661365079,2.72327711 L1.29269841,15.6094478 C1.29269841,16.3585402 1.85752381,16.9658976 2.55415873,16.9658976 L9.49212698,16.9658976 C10.1887619,16.9658976 10.7535873,16.3585402 10.7535873,15.6094478 L11.3738413,2.72327711 L11.9953333,2.72327711 L11.9953333,2.04505221 L11.9953016,2.04505221 Z M3.79587302,1.36682731 C3.79587302,0.99262249 4.0792381,0.68860241 4.42660317,0.68860241 L7.58022222,0.68860241 C7.92822222,0.68860241 8.21095238,0.99262249 8.21095238,1.36682731 L8.21095238,2.04505221 L3.79587302,2.04505221 L3.79587302,1.36682731 Z M10.1240635,15.5743213 L10.1228254,15.5915602 L10.1228254,15.6094478 C10.1228254,15.983004 9.84009524,16.2876727 9.49209524,16.2876727 L2.55412698,16.2876727 C2.2067619,16.2876727 1.92339683,15.983004 1.92339683,15.6094478 L1.92339683,15.5915602 L1.92279365,15.5737068 L1.29269841,2.72327711 L10.7425079,2.72327711 L10.1240635,15.5743213 Z"
                                                                                          id="Shape"></path>
                                                                                    <rect id="Rectangle-path"
                                                                                          x="5.68803175" y="4.07969277"
                                                                                          width="1"
                                                                                          height="10.851496"></rect>
                                                                                    <polygon id="Shape"
                                                                                             points="4.4407619 14.9093755 3.79526984 4.07907831 3.16577778 4.12209036 3.81130159 14.9523876"></polygon>
                                                                                    <polygon id="Shape"
                                                                                             points="8.846 4.10089157 8.21650794 4.05852811 7.58022222 14.9100241 8.20971429 14.9523876"></polygon>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                                Удалить',
                                                'url'    => '#',
                                                'encode' => false,
                                            ],
                                        ],
                                        'clientEvents' => [
                                            'show.bs.dropdown' => 'function(){ console.log("goofd"); }',
                                        ],
                                    ],
                                    'options'          => [
                                        'class' => 'editPopup openDropProfile',
                                    ],
                                    'containerOptions' => [
                                        'class' => 'rightInineBlock dropdown',
                                    ],
                                ]) ?>

                            </div>
                        </div>

                        <div class="likeBlock">
                            <svg class="like" width="19px" height="18px" viewBox="0 0 19 18" version="1.1">

                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="like" transform="translate(-819.000000, -1976.000000)" fill="#E35F46">
                                        <g id="REVIEWS-Copy" transform="translate(211.000000, 1664.000000)">
                                            <path d="M617.847182,329.658704 L617.5,329.811631 L617.152818,329.658704 C612.690409,326.906456 608,320.641669 608,317.162149 C608.015114,314.313018 610.333545,312 613.181818,312 C614.982068,312 616.571159,312.924512 617.5,314.32388 C618.428841,312.924512 620.017932,312 621.818182,312 C624.666455,312 626.984886,314.313018 627,317.162149 C627,320.641669 622.309591,326.906456 617.847182,329.658704 L617.847182,329.658704 Z M621.818182,312.864992 C620.369864,312.864992 619.02475,313.589223 618.218977,314.802645 L617.5,315.885732 L616.781023,314.80221 C615.97525,313.589223 614.630136,312.864992 613.181818,312.864992 C610.813727,312.864992 608.876159,314.794825 608.863636,317.162149 C608.863636,320.231113 613.282864,326.20221 617.5,328.856273 C621.717136,326.20221 626.136364,320.231547 626.136364,317.166494 C626.123841,314.794825 624.186273,312.864992 621.818182,312.864992 L621.818182,312.864992 Z"
                                                    id="Shape"></path>
                                        </g>
                                    </g>
                                </g>
                            </svg> &nbsp;22
                        </div>
                    </div>
                    <!--                    другий комент-->
                    <div class="postFedback">
                        <div class="postText">
                            <div class="fedbackAll">
                                <img src="/img/product/fotoFace.jpg" alt="" class="imgUserFedback">
                                <span class="greenInputFoto"></span>
                            </div>
                            <div class="textFedback">
                                <div class="userTextFedback">
                                    <div class="inlineTitle">
                                        <h4>Доба Дмитрий</h4>
                                        <span>• 1 час назад</span>
                                    </div>
                                    <p class="textFedbackAll readmoreFedback ">Существует целый ряд различных факторов,
                                        оказывающих влияние на принятие решения покупателем...
                                        Существует целый ряд различных факторов, оказывающих влияние на принятие решения
                                        покупателем...
                                        Существует целый ряд различных факторов, оказывающих влияние на принятие решения
                                        покупателем...</p>
                                </div>

                                <?= \yii\bootstrap\ButtonDropdown::widget([
                                    'label'            => '<span class="circle"></span>
                            <span class="circle"></span>
                            <span class="circle"></span>',
                                    'encodeLabel'      => false,
                                    'dropdown'         => [
                                        'items'        => [
                                            [
                                                'label'  => '<svg class="imgLiPunct" width="18px" height="18px" viewBox="0 0 18 18"
                                                     version="1.1">

                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                       fill-rule="evenodd">
                                                        <g id="imgLiPunct"
                                                           transform="translate(-671.000000, -1156.000000)"
                                                           fill-rule="nonzero"
                                                        ">
                                                        <g id="reviews" transform="translate(210.000000, 1019.000000)">
                                                            <g id="Group-22"
                                                               transform="translate(21.000000, 86.000000)">
                                                                <g id="Group-20">
                                                                    <g id="Group-4">
                                                                        <g id="Group-24"
                                                                           transform="translate(422.000000, 26.000000)">
                                                                            <g id="edit_icon"
                                                                               transform="translate(18.000000, 25.000000)">
                                                                                <path d="M15.6,9.6 C15.3,9.6 15.1,9.8 15.1,10.1 L15.1,16.5 L1.7,16.5 L1.7,3.1 L8.2,3.1 C8.5,3.1 8.7,2.9 8.7,2.6 C8.7,2.3 8.5,2.1 8.2,2.1 L1.2,2.1 C0.9,2.1 0.7,2.3 0.7,2.6 L0.7,17 C0.7,17.3 0.9,17.5 1.2,17.5 L15.6,17.5 C15.9,17.5 16.1,17.3 16.1,17 L16.1,10.1 C16.1,9.9 15.9,9.6 15.6,9.6 Z"
                                                                                      id="Shape"></path>
                                                                                <path d="M17.7,3.8 L14.5,0.6 C14.3,0.4 14,0.4 13.8,0.6 L5.4,9 C5.3,9.1 5.3,9.2 5.3,9.3 L4.7,13 C4.7,13.2 4.7,13.3 4.8,13.4 C4.9,13.5 5,13.5 5.2,13.5 L5.3,13.5 L9,12.9 C9.1,12.9 9.2,12.8 9.3,12.8 L17.7,4.4 C17.9,4.3 17.9,4 17.7,3.8 Z M8.7,12 L5.8,12.4 L6.2,9.5 L11.8,3.9 L14.2,6.3 L8.7,12 Z M15.1,5.7 L12.7,3.3 L14.3,1.7 L16.7,4.1 L15.1,5.7 Z"
                                                                                      id="Shape"></path>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    </g>
                                                </svg>
                                                Редактировать',
                                                'url'    => '#',
                                                'encode' => false,
                                            ],
                                            [
                                                'label'  => ' <svg class="imgLiPunct" width="16px" height="15px" viewBox="0 0 16 15"
                                                     version="1.1">

                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                       fill-rule="evenodd">
                                                        <g id="imgLiPunct"
                                                           transform="translate(-671.000000, -1199.000000)">
                                                            <g id="reviews"
                                                               transform="translate(210.000000, 1019.000000)">
                                                                <g id="Group-22"
                                                                   transform="translate(21.000000, 86.000000)">
                                                                    <g id="Group-20">
                                                                        <g id="Group-4">
                                                                            <g id="Group-24"
                                                                               transform="translate(422.000000, 26.000000)">
                                                                                <g id="report_icon"
                                                                                   transform="translate(18.000000, 68.000000)">
                                                                                    <path d="M14.8,0.4 L1.8,0.4 C1.2,0.4 0.7,0.9 0.7,1.5 L0.7,10.9 C0.7,11.5 1.2,12 1.8,12 L3.2,12 L3.2,13.8 C3.2,14 3.3,14.2 3.5,14.3 L3.7,14.3 C3.8,14.3 3.9,14.3 4,14.2 L6.4,12 L14.7,12 C15.3,12 15.8,11.5 15.8,10.9 L15.8,1.5 C15.9,0.9 15.4,0.4 14.8,0.4 Z M14.9,10.9 C14.9,11 14.8,11 14.8,11 L6.3,11 C6.2,11 6.1,11 6,11.1 L4.3,12.6 L4.3,11.4 C4.3,11.1 4.1,10.9 3.8,10.9 L1.9,10.9 C1.8,10.9 1.8,10.8 1.8,10.8 L1.8,1.4 C1.8,1.3 1.9,1.3 1.9,1.3 L14.9,1.3 C15,1.3 15,1.4 15,1.4 L15,10.9 L14.9,10.9 Z"
                                                                                          id="Shape"
                                                                                          fill-rule="nonzero"></path>
                                                                                    <path d="M8,8 L9.072,8 L9.072,9 L8,9 L8,8 Z M8.04,3.288 L9.04,3.288 L9.04,4.936 L8.776,7.408 L8.304,7.408 L8.04,4.936 L8.04,3.288 Z"
                                                                                          id="!"
                                                                                          opacity="0.900000036"></path>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                                Сообщить о нарушении',
                                                'url'    => '#',
                                                'encode' => false,
                                            ],
                                            [
                                                'label'  => ' <svg class="imgLiPunct" width="14px" height="19px" viewBox="0 0 14 19"
                                                     version="1.1">

                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                       fill-rule="evenodd">
                                                        <g id="imgLiPunct"
                                                           transform="translate(-673.000000, -1235.000000)"
                                                           fill-rule="nonzero" stroke="#AFAFAF" stroke-width="0.3">
                                                            <g id="reviews"
                                                               transform="translate(210.000000, 1019.000000)">
                                                                <g id="Group-22"
                                                                   transform="translate(21.000000, 86.000000)">
                                                                    <g id="Group-20">
                                                                        <g id="Group-4">
                                                                            <g id="Group-24"
                                                                               transform="translate(422.000000, 26.000000)">
                                                                                <g id="delete_icon"
                                                                                   transform="translate(21.000000, 105.000000)">
                                                                                    <path d="M11.9953016,2.04505221 L8.84168254,2.04505221 L8.84168254,1.36682731 C8.84168254,0.617769076 8.27685714,0.01037751 7.58022222,0.01037751 L4.42660317,0.01037751 C3.72996825,0.01037751 3.16514286,0.617769076 3.16514286,1.36682731 L3.16514286,2.04505221 L0.0115238095,2.04505221 L0.0115238095,2.72327711 L0.661365079,2.72327711 L1.29269841,15.6094478 C1.29269841,16.3585402 1.85752381,16.9658976 2.55415873,16.9658976 L9.49212698,16.9658976 C10.1887619,16.9658976 10.7535873,16.3585402 10.7535873,15.6094478 L11.3738413,2.72327711 L11.9953333,2.72327711 L11.9953333,2.04505221 L11.9953016,2.04505221 Z M3.79587302,1.36682731 C3.79587302,0.99262249 4.0792381,0.68860241 4.42660317,0.68860241 L7.58022222,0.68860241 C7.92822222,0.68860241 8.21095238,0.99262249 8.21095238,1.36682731 L8.21095238,2.04505221 L3.79587302,2.04505221 L3.79587302,1.36682731 Z M10.1240635,15.5743213 L10.1228254,15.5915602 L10.1228254,15.6094478 C10.1228254,15.983004 9.84009524,16.2876727 9.49209524,16.2876727 L2.55412698,16.2876727 C2.2067619,16.2876727 1.92339683,15.983004 1.92339683,15.6094478 L1.92339683,15.5915602 L1.92279365,15.5737068 L1.29269841,2.72327711 L10.7425079,2.72327711 L10.1240635,15.5743213 Z"
                                                                                          id="Shape"></path>
                                                                                    <rect id="Rectangle-path"
                                                                                          x="5.68803175" y="4.07969277"
                                                                                          width="1"
                                                                                          height="10.851496"></rect>
                                                                                    <polygon id="Shape"
                                                                                             points="4.4407619 14.9093755 3.79526984 4.07907831 3.16577778 4.12209036 3.81130159 14.9523876"></polygon>
                                                                                    <polygon id="Shape"
                                                                                             points="8.846 4.10089157 8.21650794 4.05852811 7.58022222 14.9100241 8.20971429 14.9523876"></polygon>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                                Удалить',
                                                'url'    => '#',
                                                'encode' => false,
                                            ],

                                        ],
                                        'clientEvents' => [
                                            'show.bs.dropdown' => 'function(){ console.log("goofd"); }',
                                        ],
                                    ],
                                    'options'          => [
                                        'class' => 'editPopup openDropProfile',
                                    ],
                                    'containerOptions' => [
                                        'class' => 'rightInineBlock dropdown',
                                    ],
                                ]) ?>


                            </div>
                        </div>
                        <div class="likeBlock">

                            <svg class="like" width="19px" height="18px" viewBox="0 0 19 18" version="1.1">

                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="like" transform="translate(-819.000000, -1976.000000)" fill="#E35F46">
                                        <g id="REVIEWS-Copy" transform="translate(211.000000, 1664.000000)">
                                            <path d="M617.847182,329.658704 L617.5,329.811631 L617.152818,329.658704 C612.690409,326.906456 608,320.641669 608,317.162149 C608.015114,314.313018 610.333545,312 613.181818,312 C614.982068,312 616.571159,312.924512 617.5,314.32388 C618.428841,312.924512 620.017932,312 621.818182,312 C624.666455,312 626.984886,314.313018 627,317.162149 C627,320.641669 622.309591,326.906456 617.847182,329.658704 L617.847182,329.658704 Z M621.818182,312.864992 C620.369864,312.864992 619.02475,313.589223 618.218977,314.802645 L617.5,315.885732 L616.781023,314.80221 C615.97525,313.589223 614.630136,312.864992 613.181818,312.864992 C610.813727,312.864992 608.876159,314.794825 608.863636,317.162149 C608.863636,320.231113 613.282864,326.20221 617.5,328.856273 C621.717136,326.20221 626.136364,320.231547 626.136364,317.166494 C626.123841,314.794825 624.186273,312.864992 621.818182,312.864992 L621.818182,312.864992 Z"
                                                    id="Shape"></path>
                                        </g>
                                    </g>
                                </g>
                            </svg> &nbsp;22
                        </div>
                    </div>
                    <!--                    третій комент-->
                    <div class="postFedback">
                        <div class="postText">
                            <div class="fedbackAll">
                                <img src="/img/product/fotoFace.jpg" alt="" class="imgUserFedback">
                                <span class="greenInputFoto"></span>
                            </div>
                            <div class="textFedback">
                                <div class="userTextFedback">
                                    <div class="inlineTitle">
                                        <h4>Доба Дмитрий</h4>
                                        <span>• 1 час назад</span>
                                    </div>

                                    <p class="text-left howProduct">Товар: <span>Перфоратор на зло соседям</span></p>
                                    <a href="#" class="eventBtn">Сдал в оренду</a>
                                    <p class="textFedbackAll">Существует целый ряд различных факторов, оказывающих
                                        влияние</p>

                                    <div class="slideComment">
                                        <div><img src="/img/slideComment/s4.jpg" alt="" class="img-responsive"></div>
                                        <div><img src="/img/slideComment/s3.jpg" alt="" class="img-responsive"></div>
                                        <div><img src="/img/slideComment/s2.jpg" alt="" class="img-responsive"></div>
                                    </div>

                                </div>
                                <?= \yii\bootstrap\ButtonDropdown::widget([
                                    'label'            => '<span class="circle"></span>
                            <span class="circle"></span>
                            <span class="circle"></span>',
                                    'encodeLabel'      => false,
                                    'dropdown'         => [
                                        'items'        => [
                                            [
                                                'label'  => '<svg class="imgLiPunct" width="18px" height="18px" viewBox="0 0 18 18"
                                                     version="1.1">

                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                       fill-rule="evenodd">
                                                        <g id="imgLiPunct"
                                                           transform="translate(-671.000000, -1156.000000)"
                                                           fill-rule="nonzero"
                                                        ">
                                                        <g id="reviews" transform="translate(210.000000, 1019.000000)">
                                                            <g id="Group-22"
                                                               transform="translate(21.000000, 86.000000)">
                                                                <g id="Group-20">
                                                                    <g id="Group-4">
                                                                        <g id="Group-24"
                                                                           transform="translate(422.000000, 26.000000)">
                                                                            <g id="edit_icon"
                                                                               transform="translate(18.000000, 25.000000)">
                                                                                <path d="M15.6,9.6 C15.3,9.6 15.1,9.8 15.1,10.1 L15.1,16.5 L1.7,16.5 L1.7,3.1 L8.2,3.1 C8.5,3.1 8.7,2.9 8.7,2.6 C8.7,2.3 8.5,2.1 8.2,2.1 L1.2,2.1 C0.9,2.1 0.7,2.3 0.7,2.6 L0.7,17 C0.7,17.3 0.9,17.5 1.2,17.5 L15.6,17.5 C15.9,17.5 16.1,17.3 16.1,17 L16.1,10.1 C16.1,9.9 15.9,9.6 15.6,9.6 Z"
                                                                                      id="Shape"></path>
                                                                                <path d="M17.7,3.8 L14.5,0.6 C14.3,0.4 14,0.4 13.8,0.6 L5.4,9 C5.3,9.1 5.3,9.2 5.3,9.3 L4.7,13 C4.7,13.2 4.7,13.3 4.8,13.4 C4.9,13.5 5,13.5 5.2,13.5 L5.3,13.5 L9,12.9 C9.1,12.9 9.2,12.8 9.3,12.8 L17.7,4.4 C17.9,4.3 17.9,4 17.7,3.8 Z M8.7,12 L5.8,12.4 L6.2,9.5 L11.8,3.9 L14.2,6.3 L8.7,12 Z M15.1,5.7 L12.7,3.3 L14.3,1.7 L16.7,4.1 L15.1,5.7 Z"
                                                                                      id="Shape"></path>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                    </g>
                                                </svg>
                                                Редактировать',
                                                'url'    => '#',
                                                'encode' => false,
                                            ],
                                            [
                                                'label'  => ' <svg class="imgLiPunct" width="16px" height="15px" viewBox="0 0 16 15"
                                                     version="1.1">

                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                       fill-rule="evenodd">
                                                        <g id="imgLiPunct"
                                                           transform="translate(-671.000000, -1199.000000)">
                                                            <g id="reviews"
                                                               transform="translate(210.000000, 1019.000000)">
                                                                <g id="Group-22"
                                                                   transform="translate(21.000000, 86.000000)">
                                                                    <g id="Group-20">
                                                                        <g id="Group-4">
                                                                            <g id="Group-24"
                                                                               transform="translate(422.000000, 26.000000)">
                                                                                <g id="report_icon"
                                                                                   transform="translate(18.000000, 68.000000)">
                                                                                    <path d="M14.8,0.4 L1.8,0.4 C1.2,0.4 0.7,0.9 0.7,1.5 L0.7,10.9 C0.7,11.5 1.2,12 1.8,12 L3.2,12 L3.2,13.8 C3.2,14 3.3,14.2 3.5,14.3 L3.7,14.3 C3.8,14.3 3.9,14.3 4,14.2 L6.4,12 L14.7,12 C15.3,12 15.8,11.5 15.8,10.9 L15.8,1.5 C15.9,0.9 15.4,0.4 14.8,0.4 Z M14.9,10.9 C14.9,11 14.8,11 14.8,11 L6.3,11 C6.2,11 6.1,11 6,11.1 L4.3,12.6 L4.3,11.4 C4.3,11.1 4.1,10.9 3.8,10.9 L1.9,10.9 C1.8,10.9 1.8,10.8 1.8,10.8 L1.8,1.4 C1.8,1.3 1.9,1.3 1.9,1.3 L14.9,1.3 C15,1.3 15,1.4 15,1.4 L15,10.9 L14.9,10.9 Z"
                                                                                          id="Shape"
                                                                                          fill-rule="nonzero"></path>
                                                                                    <path d="M8,8 L9.072,8 L9.072,9 L8,9 L8,8 Z M8.04,3.288 L9.04,3.288 L9.04,4.936 L8.776,7.408 L8.304,7.408 L8.04,4.936 L8.04,3.288 Z"
                                                                                          id="!"
                                                                                          opacity="0.900000036"></path>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                                Сообщить о нарушении',
                                                'url'    => '#',
                                                'encode' => false,
                                            ],
                                            [
                                                'label'  => ' <svg class="imgLiPunct" width="14px" height="19px" viewBox="0 0 14 19"
                                                     version="1.1">

                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                       fill-rule="evenodd">
                                                        <g id="imgLiPunct"
                                                           transform="translate(-673.000000, -1235.000000)"
                                                           fill-rule="nonzero" stroke="#AFAFAF" stroke-width="0.3">
                                                            <g id="reviews"
                                                               transform="translate(210.000000, 1019.000000)">
                                                                <g id="Group-22"
                                                                   transform="translate(21.000000, 86.000000)">
                                                                    <g id="Group-20">
                                                                        <g id="Group-4">
                                                                            <g id="Group-24"
                                                                               transform="translate(422.000000, 26.000000)">
                                                                                <g id="delete_icon"
                                                                                   transform="translate(21.000000, 105.000000)">
                                                                                    <path d="M11.9953016,2.04505221 L8.84168254,2.04505221 L8.84168254,1.36682731 C8.84168254,0.617769076 8.27685714,0.01037751 7.58022222,0.01037751 L4.42660317,0.01037751 C3.72996825,0.01037751 3.16514286,0.617769076 3.16514286,1.36682731 L3.16514286,2.04505221 L0.0115238095,2.04505221 L0.0115238095,2.72327711 L0.661365079,2.72327711 L1.29269841,15.6094478 C1.29269841,16.3585402 1.85752381,16.9658976 2.55415873,16.9658976 L9.49212698,16.9658976 C10.1887619,16.9658976 10.7535873,16.3585402 10.7535873,15.6094478 L11.3738413,2.72327711 L11.9953333,2.72327711 L11.9953333,2.04505221 L11.9953016,2.04505221 Z M3.79587302,1.36682731 C3.79587302,0.99262249 4.0792381,0.68860241 4.42660317,0.68860241 L7.58022222,0.68860241 C7.92822222,0.68860241 8.21095238,0.99262249 8.21095238,1.36682731 L8.21095238,2.04505221 L3.79587302,2.04505221 L3.79587302,1.36682731 Z M10.1240635,15.5743213 L10.1228254,15.5915602 L10.1228254,15.6094478 C10.1228254,15.983004 9.84009524,16.2876727 9.49209524,16.2876727 L2.55412698,16.2876727 C2.2067619,16.2876727 1.92339683,15.983004 1.92339683,15.6094478 L1.92339683,15.5915602 L1.92279365,15.5737068 L1.29269841,2.72327711 L10.7425079,2.72327711 L10.1240635,15.5743213 Z"
                                                                                          id="Shape"></path>
                                                                                    <rect id="Rectangle-path"
                                                                                          x="5.68803175" y="4.07969277"
                                                                                          width="1"
                                                                                          height="10.851496"></rect>
                                                                                    <polygon id="Shape"
                                                                                             points="4.4407619 14.9093755 3.79526984 4.07907831 3.16577778 4.12209036 3.81130159 14.9523876"></polygon>
                                                                                    <polygon id="Shape"
                                                                                             points="8.846 4.10089157 8.21650794 4.05852811 7.58022222 14.9100241 8.20971429 14.9523876"></polygon>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                                Удалить',
                                                'url'    => '#',
                                                'encode' => false,
                                            ],
                                        ],
                                        'clientEvents' => [
                                            'show.bs.dropdown' => 'function(){ console.log("goofd"); }',
                                        ],
                                    ],
                                    'options'          => [
                                        'class' => 'editPopup openDropProfile',
                                    ],
                                    'containerOptions' => [
                                        'class' => 'rightInineBlock dropdown',
                                    ],
                                ]) ?>

                            </div>
                        </div>
                        <div class="likeBlock">

                            <svg class="like" width="19px" height="18px" viewBox="0 0 19 18" version="1.1">

                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="like" transform="translate(-819.000000, -1976.000000)">
                                        <g id="REVIEWS-Copy" transform="translate(211.000000, 1664.000000)">
                                            <path d="M617.847182,329.658704 L617.5,329.811631 L617.152818,329.658704 C612.690409,326.906456 608,320.641669 608,317.162149 C608.015114,314.313018 610.333545,312 613.181818,312 C614.982068,312 616.571159,312.924512 617.5,314.32388 C618.428841,312.924512 620.017932,312 621.818182,312 C624.666455,312 626.984886,314.313018 627,317.162149 C627,320.641669 622.309591,326.906456 617.847182,329.658704 L617.847182,329.658704 Z M621.818182,312.864992 C620.369864,312.864992 619.02475,313.589223 618.218977,314.802645 L617.5,315.885732 L616.781023,314.80221 C615.97525,313.589223 614.630136,312.864992 613.181818,312.864992 C610.813727,312.864992 608.876159,314.794825 608.863636,317.162149 C608.863636,320.231113 613.282864,326.20221 617.5,328.856273 C621.717136,326.20221 626.136364,320.231547 626.136364,317.166494 C626.123841,314.794825 624.186273,312.864992 621.818182,312.864992 L621.818182,312.864992 Z"
                                                    id="Shape"></path>
                                        </g>
                                    </g>
                                </g>
                            </svg>&nbsp; 22
                        </div>
                    </div>

                    <div class="btnPlace">
                        <a href="#" class="btn btn-block btnAllPost ">Посмотреть еще 10 отзывов</a>
                    </div>
                </div>
                <div class="nowPost" style="display: none">
                    <img src="/img/product/nowPost.svg" alt="nowPost">
                    <span class="titleMasseg">У вас нет ни одного поста </span>
                    <span class="subTitleMasseg">Поделитесь вашим настроением </span>
                    <div class="blockComment">
                        <form action="" method="post">
                            <span>Введите текст</span><br>
                            <textarea id="comment"></textarea><br>
                            <div class="butt">
                                <button id="button">Отправить</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

            <!--            БЛОК РЕЄСТРАЦІЇ КОРИСТУВАЧА-->
            <div class="blockDateReg styleBlock col-sm-12 col-xs-12 col-md-8">
                <p class="text-center"><?= \Yii::t('UI13.1', '$USER_JOINED_DATE$',
                        ['date' => $model['user']['created_at']]); ?></p>
                <div class="nowPost" style="display: none">
                    <img src="/img/product/nowPost.svg" alt="nowPost">
                    <span class="titleMasseg">У вас нет ни одного поста </span>
                    <span class="subTitleMasseg">Поделитесь вашим настроением </span>
                    <div class="blockComment">

                        <form action="" method="post">
                            <span>Введите текст</span><br>
                            <textarea id="comment"></textarea><br>
                            <div class="butt">
                                <button id="button">Отправить</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="selected" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">
                        <svg width="8px" height="8px" viewBox="0 0 8 8">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
        <g id="UI15.1----Отзывы-(личный-кабинет)---дефолт"
                transform="translate(-685.000000, -774.000000)"
                stroke="#FFFFFF"
                fill="#FFFFFF">
            <g id="content" transform="translate(280.000000, 151.000000)">
                <g id="Group-22" transform="translate(20.000000, 89.000000)">
                    <g id="Group-20">
                        <g id="Group-19" transform="translate(0.000000, 204.000000)">
                            <g id="Group-25" transform="translate(70.000000, 273.000000)">
                                <g id="close-copy" transform="translate(311.000000, 53.000000)">
                                    <g id="close" transform="translate(4.888889, 4.888889)">
                                        <path d="M3.11111111,2.48257175 L0.832788199,0.204248838 C0.659221847,0.0306824861 0.37781519,0.0306824861 0.204248838,0.204248838 L0.204248838,0.832788199 L2.48257175,3.11111111 L0.151606014,5.44207685 C-0.0219603375,5.6156432 -0.0219603375,5.89704986 0.151606014,6.07061621 C0.325172366,6.24418256 0.606579024,6.24418256 0.780145375,6.07061621 L3.11111111,3.73965047 L5.44207685,6.07061621 C5.6156432,6.24418256 5.89704986,6.24418256 6.07061621,6.07061621 C6.24418256,5.89704986 6.24418256,5.6156432 6.07061621,5.44207685 L3.73965047,3.11111111 L6.01797338,0.832788199 C6.19153974,0.659221847 6.19153974,0.37781519 6.01797338,0.204248838 L5.38943402,0.204248838 L3.11111111,2.48257175 Z"
                                                id="Combined-Shape-Copy"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span></button>
                <h4 class="modal-title" id="myModalLabel"><?= \Yii::t('UI13.11', '$HEADER$',
                        ['name' => $model['firstname']]); ?></h4>
            </div>
            <div class="modal-body">
<!--                --><?php //print_r($model['user']['products']);exit; ?>
                <?php foreach($favouriteQuery->each() as $item): ?>
                <?= $this->render('/blocks/_product-list-item', ['model' => $item]); ?>
                <?php endforeach; ?>

                <?php $this->render('_selectedProduct', [
                    'model' => [
                        'minislide'     => [],
                        'slug'          => '',
                        'product_name'  => 'Long name',
                        'category'      => 'Music',
                        'price'         => 0,
                        'currency'      => '',
                        'min_period'    => 1,
                        'step'          => 1,
                        'countComments' => 1,
                    ],
                ]) ?>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="userProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">
                        <svg width="8px" height="8px" viewBox="0 0 8 8">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
        <g id="UI15.1----Отзывы-(личный-кабинет)---дефолт"
                transform="translate(-685.000000, -774.000000)"
                stroke="#FFFFFF"
                fill="#FFFFFF">
            <g id="content" transform="translate(280.000000, 151.000000)">
                <g id="Group-22" transform="translate(20.000000, 89.000000)">
                    <g id="Group-20">
                        <g id="Group-19" transform="translate(0.000000, 204.000000)">
                            <g id="Group-25" transform="translate(70.000000, 273.000000)">
                                <g id="close-copy" transform="translate(311.000000, 53.000000)">
                                    <g id="close" transform="translate(4.888889, 4.888889)">
                                        <path d="M3.11111111,2.48257175 L0.832788199,0.204248838 C0.659221847,0.0306824861 0.37781519,0.0306824861 0.204248838,0.204248838 L0.204248838,0.832788199 L2.48257175,3.11111111 L0.151606014,5.44207685 C-0.0219603375,5.6156432 -0.0219603375,5.89704986 0.151606014,6.07061621 C0.325172366,6.24418256 0.606579024,6.24418256 0.780145375,6.07061621 L3.11111111,3.73965047 L5.44207685,6.07061621 C5.6156432,6.24418256 5.89704986,6.24418256 6.07061621,6.07061621 C6.24418256,5.89704986 6.24418256,5.6156432 6.07061621,5.44207685 L3.73965047,3.11111111 L6.01797338,0.832788199 C6.19153974,0.659221847 6.19153974,0.37781519 6.01797338,0.204248838 L5.38943402,0.204248838 L3.11111111,2.48257175 Z"
                                                id="Combined-Shape-Copy"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span></button>
                <h4 class="modal-title" id="myModalLabel"><?= \Yii::t('UI13.10', '$HEADER$',
                        ['name' => $model['firstname']]); ?></h4>
            </div>
            <div class="modal-body">

                <?php foreach ($model['user']['products'] as $item): ?>
                    <?= $this->render('/blocks/_product-list-item', ['model' => $item]); ?>
                <?php endforeach; ?>

                <?php $this->render('_selectedProduct', [
                    'model' => [
                        'minislide'     => [],
                        'slug'          => '',
                        'product_name'  => 'Long name',
                        'category'      => 'Music',
                        'price'         => 0,
                        'currency'      => '',
                        'min_period'    => 1,
                        'step'          => 1,
                        'countComments' => 1,
                    ],
                ]) ?>

            </div>
        </div>
    </div>
</div>

<!--модал відео-->
<?php if (count($model['video'])): ?>
<div class="modal fade" id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">
                        <svg class="closeSvg" width="16px" height="16px" viewBox="0 0 8 8">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
        <g id="U10.6-Просмотр---статистика-без-рекламы-подробно"
                transform="translate(-1034.000000, -272.000000)"
                stroke="#FFFFFF"
                fill="#FFFFFF">
            <g id="Group-28">
                <g id="Group-23" transform="translate(385.000000, 258.000000)">
                    <g id="close" transform="translate(645.000000, 10.000000)">
                        <g transform="translate(4.888889, 4.888889)" id="Combined-Shape">
                            <path d="M3.11111111,2.48257175 L0.832788199,0.204248838 C0.659221847,0.0306824861 0.37781519,0.0306824861 0.204248838,0.204248838 L0.204248838,0.832788199 L2.48257175,3.11111111 L0.151606014,5.44207685 C-0.0219603375,5.6156432 -0.0219603375,5.89704986 0.151606014,6.07061621 C0.325172366,6.24418256 0.606579024,6.24418256 0.780145375,6.07061621 L3.11111111,3.73965047 L5.44207685,6.07061621 C5.6156432,6.24418256 5.89704986,6.24418256 6.07061621,6.07061621 C6.24418256,5.89704986 6.24418256,5.6156432 6.07061621,5.44207685 L3.73965047,3.11111111 L6.01797338,0.832788199 C6.19153974,0.659221847 6.19153974,0.37781519 6.01797338,0.204248838 L5.38943402,0.204248838 L3.11111111,2.48257175 Z"></path>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span>
                </button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div class="embed-responsive embed-responsive-16by9">
                    <video controls>
                        <?= Html::tag('source', '', ['src' => $model['video'][0]['path'], 'type' => $model['video'][0]['type']]); ?>
                    </video>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<?php
$this->registerJs(<<<JS
jQuery("#modalVideo").on("hide.bs.modal", function(event){
    jQuery(event.target).find("video").get(0).pause();
});
JS
);
