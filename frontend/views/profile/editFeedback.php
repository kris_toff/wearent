<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 28.08.2017
 * Time: 10:33
 */

use yii\bootstrap\Html;

/** @var \yii\web\View $this */

\yii\bootstrap\BootstrapPluginAsset::register($this);
\frontend\assets\CheckboxAsset::register($this);
\frontend\assets\ReadmoreAsset::register($this);
\frontend\assets\RateAsset::register($this);
\frontend\assets\SliderAsset::register($this);
\frontend\assets\ProductAsset::register($this);
\frontend\assets\yyAsset::register($this);

?>

<div class="row editFeedback">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 titlePage">
                <h3 class="text-left">Отзыв на арендатора</h3>
                <span class="stepEdit hidden-xs">Шаг 1 из 1</span>
            </div>
        </div>
        <div class="row contentEdit">
            <div class="col-xs-12">
                <form action="" method="post" class="styleInput">
                    <div class="row topContent">
                        <div class="col-xs-12 col-sm-9 notPadding">
                            <h4 class="titleBlock">Отзыв на арендатора
                                <span class="stepEdit hidden-lg hidden-sm hidden-md">Шаг 1 из 1</span>
                            </h4>

                            <p class="borderXs">Пожалуйста, оставьте отзыв - это займет не более 2-х минут и поможет тысячам других пользователей.</p>
                        </div>
                        <div class="col-xs-12 col-sm-3 notPadding">
                            <label class="reminder"><input type="checkbox" class="redCheckBox">Напомнить мне написать ответ через 2 дня</label>
                        </div>
                    </div>

                    <div class="row detailsDealEdit">
                        <div class="col-xs-12 notPadding">
                            <h4 class="titleBlock">Детали сделки</h4>
                        </div>
                        <div class="col-xs-12 col-sm-4 notPadding">
                            <div class="media user">
                                <div class="media-left">
                                    <img src="/img/product/fotoFace.jpg" alt="...">
                                    <div class="onlineCheck"></div>
                                </div>
                                <div class="media-body">
                                    <h5 class="media-heading">Пользователь</h5>
                                    <p class="nameUser">Доба Дмитрий</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5 notPadding">
                            <div class="media product">
                                <div class="media-left">
                                    <img src="/img/Bitmap.jpg" alt="...">
                                </div>
                                <div class="media-body">
                                    <h5 class="media-heading">Товар</h5>
                                    <p class="nameProduct">Длинное название товара, длинное название товара и не только</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row ratingUser">
                        <div class="col-xs-12 notPadding">
                            <h4 class="titleBlock">Оцените пользователя</h4>
                        </div>

                        <div class="col-xs-12 notPadding">
                            <table class="table">
                                <tr>
                                    <td class="text-left text">Общая оценка пользователя:</td>
                                    <td class="text-left rating"><span class="rateyoFeedback"></span></td>
                                </tr>
                                <tr>
                                    <td class="text-left text">Бережность в использованиитовара:</td>
                                    <td class="text-left rating"><span class="rateyoFeedback"></span></td>
                                </tr>
                                <tr>
                                    <td class="text-left text">Корректность описания:</td>
                                    <td class="text-left rating"><span class="rateyoFeedback"></span></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-xs-12 notPadding">
                            <p class="captionForFeedback">Продолжите предложения или напишите своими словами:</p>
                            <div class="feedback">
                                <div class="topForText">
                                    <textarea name="" id="" placeholder="Мне понравилось в этой сделке то, что....
Условия аренды были соблюдены...
После возврата товар был...
Я благодарен арендатору за..."></textarea>

                                </div>
                                <div class="bottomForText">
                                    <div class="fotoBlock">

                                    </div>
                                    <div class="loatedFoto text-right">
                                        <input id="loatedForEdit" type="file" class="hidden" accept="image/*">
                                        <label for="loatedForEdit"><span>
                                                <svg width="18px" height="19px" viewBox="0 0 18 19" >
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g class="re" transform="translate(-692.000000, -1133.000000)" fill-rule="nonzero">
            <g id="Group" transform="translate(210.000000, 106.000000)">
                <g id="Group-25" transform="translate(20.000000, 497.000000)">
                    <g id="Group-13" transform="translate(0.000000, 391.000000)">
                        <g id="button" transform="translate(440.000000, 131.000000)">
                            <path d="M27.1461851,10.7088095 L27.1461851,23.4189583 C27.1461851,23.4189583 27.1461851,28.2113113 31.313447,28.2113094 C35.4807089,28.2113076 35.4807089,23.4189583 35.4807089,23.4189583 L35.4807089,10.7088095 C35.4807089,10.1879018 34.5546507,10.1879018 34.5546507,10.7088095 L34.5546507,23.4189592 C34.5546507,23.4189592 34.8913988,27.2642045 31.313447,27.2642045 C28.0722433,27.2642045 28.0722433,23.4189592 28.0722433,23.4189592 L28.0722433,10.7088095 C28.0722433,10.7088095 28.0722433,8.32210592 30.3873888,8.32210497 C32.7025343,8.32210403 32.7025343,10.7088095 32.7025343,10.7088095 L32.7025343,23.4189592 C32.7025343,23.4189592 32.7025343,24.9130685 31.351245,24.9130675 C29.9999566,24.9130656 29.9243597,23.4189592 29.9243597,23.4189592 L29.9243597,11.3338988 C29.9243597,10.8129911 28.9983015,10.8129911 28.9983015,11.3338988 L28.9983015,23.4189583 C28.9983015,23.4189583 28.8256324,25.9548849 31.351245,25.954883 C33.6285925,25.9548811 33.6285925,23.4189583 33.6285925,23.4189583 L33.6285925,10.7088095 C33.6285925,10.7088095 33.6285925,7.37499905 30.3873888,7.375 C27.1461851,7.37500095 27.1461851,10.7088095 27.1461851,10.7088095 Z" id="attach" transform="translate(31.313447, 17.793155) rotate(45.000000) translate(-31.313447, -17.793155) "></path>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span>&nbsp;Прикрепить фото</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row personalMassage">
                        <div class="col-xs-12 notPadding">
                            <h4 class="titleBlock toggleOpen">Оставить личное пожелание или благодарность Пользователю <svg class="closeBlock" width="24px" height="24px" viewBox="0 0 8 14" >
                                    <g id="Page-1"  stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                        <g class="re" transform="translate(-1207.000000, -591.000000)" stroke-width="2">
                                            <g id="Group-11" transform="translate(0.000000, 579.000000)">
                                                <g id="Recommended">
                                                    <g id="Lenta" transform="translate(210.000000, 0.000000)">
                                                        <g id="arrows" transform="translate(927.000000, 1.000000)">
                                                            <g id="Group-37" transform="translate(56.000000, 0.000000)">
                                                                <polyline id="arrow_right_black" transform="translate(17.935389, 17.699575) scale(-1, 1) rotate(90.000000) translate(-17.935389, -17.699575) " points="15 23.3991501 20.8707778 17.5283723 15.3424055 12"></polyline>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg></h4>

                        </div>
                        <div class="contentBlock " style="display: none">
                            <div class="col-xs-12 notPadding">
                                <p>Это личное сообщение - его увидит только Арендатор.</p>
                                <p>Продолжите предложения или напишите своими словами:</p>
                                <div class="feedback">
                                    <div class="topForText">
                                        <textarea name="" id="" placeholder="Мне понравилось в этой сделке то, что....
Условия аренды были соблюдены...
После возврата товар был...
Я благодарен арендатору за..."></textarea>
                                    </div>
                                    <div class="bottomForText">
                                        <div class="fotoBlock">

                                        </div>
                                        <div class="loatedFoto text-right">
                                            <input id="loatedForEdit" type="file" class="hidden" accept="image/*">
                                            <label for="loatedForEdit"><span>
                                                <svg width="18px" height="19px" viewBox="0 0 18 19" >
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g class="re" transform="translate(-692.000000, -1133.000000)" fill-rule="nonzero">
            <g id="Group" transform="translate(210.000000, 106.000000)">
                <g id="Group-25" transform="translate(20.000000, 497.000000)">
                    <g id="Group-13" transform="translate(0.000000, 391.000000)">
                        <g id="button" transform="translate(440.000000, 131.000000)">
                            <path d="M27.1461851,10.7088095 L27.1461851,23.4189583 C27.1461851,23.4189583 27.1461851,28.2113113 31.313447,28.2113094 C35.4807089,28.2113076 35.4807089,23.4189583 35.4807089,23.4189583 L35.4807089,10.7088095 C35.4807089,10.1879018 34.5546507,10.1879018 34.5546507,10.7088095 L34.5546507,23.4189592 C34.5546507,23.4189592 34.8913988,27.2642045 31.313447,27.2642045 C28.0722433,27.2642045 28.0722433,23.4189592 28.0722433,23.4189592 L28.0722433,10.7088095 C28.0722433,10.7088095 28.0722433,8.32210592 30.3873888,8.32210497 C32.7025343,8.32210403 32.7025343,10.7088095 32.7025343,10.7088095 L32.7025343,23.4189592 C32.7025343,23.4189592 32.7025343,24.9130685 31.351245,24.9130675 C29.9999566,24.9130656 29.9243597,23.4189592 29.9243597,23.4189592 L29.9243597,11.3338988 C29.9243597,10.8129911 28.9983015,10.8129911 28.9983015,11.3338988 L28.9983015,23.4189583 C28.9983015,23.4189583 28.8256324,25.9548849 31.351245,25.954883 C33.6285925,25.9548811 33.6285925,23.4189583 33.6285925,23.4189583 L33.6285925,10.7088095 C33.6285925,10.7088095 33.6285925,7.37499905 30.3873888,7.375 C27.1461851,7.37500095 27.1461851,10.7088095 27.1461851,10.7088095 Z" id="attach" transform="translate(31.313447, 17.793155) rotate(45.000000) translate(-31.313447, -17.793155) "></path>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span>&nbsp;Прикрепить фото</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class=" row massageInSupport">
                        <div class="col-xs-12 notPadding">
                            <h4 class="titleBlock toggleOpen">Рассказать Службе поддержки о Пользователе&nbsp;
                                    <svg class="closeBlock" width="24px" height="24px" viewBox="0 0 8 14" >
    <g id="Page-1"  stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
        <g class="re" transform="translate(-1207.000000, -591.000000)" stroke-width="2">
            <g id="Group-11" transform="translate(0.000000, 579.000000)">
                <g id="Recommended">
                    <g id="Lenta" transform="translate(210.000000, 0.000000)">
                        <g id="arrows" transform="translate(927.000000, 1.000000)">
                            <g id="Group-37" transform="translate(56.000000, 0.000000)">
                                <polyline id="arrow_right_black" transform="translate(17.935389, 17.699575) scale(-1, 1) rotate(90.000000) translate(-17.935389, -17.699575) " points="15 23.3991501 20.8707778 17.5283723 15.3424055 12"></polyline>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></h4>
                        </div>
                        <div class="contentBlock " style="display: none">
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium dolore necessitatibus pariatur sapiente? Aut, autem dolorem est illum laudantium maiores maxime minima optio praesentium provident quia rem repellat, tenetur ullam!
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi architecto eaque iure molestiae placeat sequi sit ut. Alias, aliquid beatae dicta ea eaque omnis pariatur placeat ratione voluptatum! Autem, voluptatum.
                            </p>
                        </div>
                    </div>

                    <div class="row footerEditFeedback">
                        <div class="col-xs-12 col-sm-6 notPadding text-left">
                            <p class="advertisement">Через 14 дней отзывы будут опубликованы. <span  data-container="body" data-toggle="popover" data-placement="right auto" data-html="true" data-trigger="hover" data-content="<p>После завершения аренды в течении 2-х недель обе стороны могут оставить отзывы друг на друга. По завершении 2-х недельного срока, отзывы будут опубликованы, а редактирование будет невозможным.</p>
<p>Как только отзывы будут опубликованы, мы вам отправим уведомление.</p>
<p>У вас будет возможность дать ответ на отзыв другой стороны - например, поблагодарить за приятную работу.</p>">
                                    <svg class="inform" width="17px" height="17px" viewBox="0 0 17 17" >
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g class="re" transform="translate(-1068.000000, -837.000000)" fill-rule="nonzero" >
            <g id="FORM" transform="translate(903.000000, 624.000000)">
                <g id="Group-16" transform="translate(20.000000, 213.000000)">
                    <g id="Group-14">
                        <g id="Group-3" transform="translate(145.000000, 0.000000)">
                            <path d="M14.2,13.4 C15.5,12 16.2,10.1 16.2,8.1 C16.2,3.6 12.6,0 8.1,0 C3.6,0 0,3.6 0,8.1 C0,12.6 3.6,16.2 8.1,16.2 C10.2,16.2 12,15.4 13.4,14.2 L14.2,13.4 Z M1,8 C1,4.1 4.2,0.9 8.1,0.9 C12,0.9 15.2,4.1 15.2,8 C15.2,11.9 12,15.1 8.1,15.1 C4.2,15.1 1,12 1,8 Z" id="Shape"></path>
                            <path d="M9.5,11.2 L8.6,11.2 L8.6,7.4 C8.6,7.1 8.4,6.9 8.1,6.9 L6.8,6.9 C6.5,6.9 6.3,7.1 6.3,7.4 C6.3,7.7 6.5,7.9 6.8,7.9 L7.6,7.9 L7.6,11.2 L6.8,11.2 C6.5,11.2 6.3,11.4 6.3,11.7 C6.3,12 6.5,12.2 6.8,12.2 L9.4,12.2 C9.7,12.2 9.9,12 9.9,11.7 C9.9,11.4 9.7,11.2 9.5,11.2 Z" id="Shape"></path>
                            <circle id="Oval" cx="7.9" cy="4.4" r="1.1"></circle>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>

                                </span>
                            </p>
                        </div>
                        <div class="col-xs-12 col-sm-6 notPadding text-right">
                            <a href="#" class="btn cansel">Отменить</a>
                            <button type="submit" class="btn send">Отправить</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>


