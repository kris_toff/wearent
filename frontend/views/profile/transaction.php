<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 11.07.2017
 * Time: 15:33
 */


/** @var \yii\web\View $this */

?>

<div class="transaction">
    <h5 class="hidden-md hidden-sm hidden-lg">Транзакиця <span class="dateTransaction">01.08.2016</span></h5>
    <a class="hidden-md hidden-sm hidden-lg infTrans" role="button" data-toggle="popover" data-trigger="hover" data-html="true" data-placement="bottom" data-content="
    <ul class='list-unstyled'>
    <li><span class='left'>Средства были списаны cо счета:</span><span class='black right'>USD</span></li>
    <li><span class='left'>Баланс до операции:</span><span class='black right'>$1000.00</span></li>
    <li><span class='left'>Баланс после операции:</span><span class='black right'>$1000.40 (+$0.40)</span></li>
    <li><span class='left'>Сдача товара: </span><span class='red product right'>Дрелль Макита</span></li>
    </ul>">
        <svg width="17px" height="17px" viewBox="0 0 17 17">
            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <g id="casaHistory" transform="translate(-377.000000, -552.000000)" fill-rule="nonzero">
                    <g id="KACCA" transform="translate(0.000000, 259.000000)">
                        <g id="Group" transform="translate(0.000000, 56.000000)">
                            <g id="Tr-1-Copy" transform="translate(20.000000, 235.000000)">
                                <g id="Group-3-Copy" transform="translate(357.000000, 2.000000)">
                                    <path d="M16.3,15.5 L14.2,13.4 C15.5,12 16.2,10.1 16.2,8.1 C16.2,3.6 12.6,0 8.1,0 C3.6,0 0,3.6 0,8.1 C0,12.6 3.6,16.2 8.1,16.2 C10.2,16.2 12,15.4 13.4,14.2 L15.5,16.3 C15.6,16.4 15.7,16.4 15.9,16.4 C16.1,16.4 16.2,16.4 16.3,16.3 C16.5,16.1 16.5,15.7 16.3,15.5 Z M1,8 C1,4.1 4.2,0.9 8.1,0.9 C12,0.9 15.2,4.1 15.2,8 C15.2,11.9 12,15.1 8.1,15.1 C4.2,15.1 1,12 1,8 Z"
                                          id="Shape"></path>
                                    <path d="M9.5,11.2 L8.6,11.2 L8.6,7.4 C8.6,7.1 8.4,6.9 8.1,6.9 L6.8,6.9 C6.5,6.9 6.3,7.1 6.3,7.4 C6.3,7.7 6.5,7.9 6.8,7.9 L7.6,7.9 L7.6,11.2 L6.8,11.2 C6.5,11.2 6.3,11.4 6.3,11.7 C6.3,12 6.5,12.2 6.8,12.2 L9.4,12.2 C9.7,12.2 9.9,12 9.9,11.7 C9.9,11.4 9.7,11.2 9.5,11.2 Z"
                                          id="Shape"></path>
                                    <circle id="Oval" cx="7.9" cy="4.4" r="1.1"></circle>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </svg>

        <svg class="btnClose" width="8px" height="8px" viewBox="0 0 8 8" >
            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
                <g id="btnCloseInform" transform="translate(-525.000000, -774.000000)" stroke="#FFFFFF" fill="#FFFFFF">
                    <g id="content" transform="translate(280.000000, 151.000000)">
                        <g id="Group-22" transform="translate(20.000000, 89.000000)">
                            <g id="Group-20">
                                <g id="Group-19" transform="translate(0.000000, 204.000000)">
                                    <g id="Group-25" transform="translate(70.000000, 273.000000)">
                                        <g id="close" transform="translate(151.000000, 53.000000)">
                                            <g transform="translate(4.888889, 4.888889)" id="Combined-Shape">
                                                <path d="M3.11111111,2.48257175 L0.832788199,0.204248838 C0.659221847,0.0306824861 0.37781519,0.0306824861 0.204248838,0.204248838 L0.204248838,0.832788199 L2.48257175,3.11111111 L0.151606014,5.44207685 C-0.0219603375,5.6156432 -0.0219603375,5.89704986 0.151606014,6.07061621 C0.325172366,6.24418256 0.606579024,6.24418256 0.780145375,6.07061621 L3.11111111,3.73965047 L5.44207685,6.07061621 C5.6156432,6.24418256 5.89704986,6.24418256 6.07061621,6.07061621 C6.24418256,5.89704986 6.24418256,5.6156432 6.07061621,5.44207685 L3.73965047,3.11111111 L6.01797338,0.832788199 C6.19153974,0.659221847 6.19153974,0.37781519 6.01797338,0.204248838 L5.38943402,0.204248838 L3.11111111,2.48257175 Z"></path>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </svg>
    </a>
    <ul class="list-unstyled">
        <li class="first">
            <span class="left hidden-lg hidden-sm hidden-md">Дата:</span>
            <span class="right black">1 августа, 2016<br><i>10:23</i></span>
        </li>

        <li class="two">
            <span class="left hidden-lg hidden-sm hidden-md">Категория</span>
            <span class="right black">Получили оплату аренды</span>
        </li>
        <li class="three">
            <span class="left hidden-lg hidden-sm hidden-md">Статус</span>
            <span class="right audit hidden">В обработке</span>
            <span class="right success ">
                <svg class="imgStatus" width="19px" height="18px" viewBox="0 0 19 18" >
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="+UI2.-Главная" transform="translate(-210.000000, -3421.000000)" fill-rule="nonzero">
            <g id="Group-11" transform="translate(0.000000, 579.000000)">
                <g id="Group-7" transform="translate(0.000000, 2726.000000)">
                    <g id="Group-6" transform="translate(210.000000, 58.000000)">
                        <g id="Group-3">
                            <g id="Group-Copy-4" transform="translate(0.000000, 58.000000)">
                                <path d="M9.4,1.4 C13.7,1.4 17.2,4.9 17.2,9.2 C17.2,13.5 13.7,17 9.4,17 C5.1,17 1.6,13.5 1.6,9.2 C1.6,4.9 5.1,1.4 9.4,1.4 L9.4,1.4 Z M9.4,0.4 C4.6,0.4 0.6,4.3 0.6,9.2 C0.6,14.1 4.5,18 9.4,18 C14.3,18 18.2,14.1 18.2,9.2 C18.2,4.3 14.2,0.4 9.4,0.4 L9.4,0.4 Z" id="Shape" fill="#afafaf"></path>
                                <path d="M8.9,11.6 C8.8,11.6 8.6,11.5 8.5,11.4 L6.4,9.1 C6.2,8.9 6.2,8.6 6.4,8.4 C6.6,8.2 6.9,8.2 7.1,8.4 L8.9,10.3 L12.1,6.5 C12.3,6.3 12.6,6.3 12.8,6.4 C13,6.6 13,6.9 12.9,7.1 L9.3,11.3 C9.2,11.5 9.1,11.6 8.9,11.6 L8.9,11.6 Z" id="Shape" fill="#4CDB58"></path>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>&nbsp;Оплаченно</span>
            <span class="right hidden cansel">
                <svg class="imgStatus" width="20px" height="20px" viewBox="0 0 20 20" >
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="+UI16-Касса---история" transform="translate(-845.000000, -393.000000)" fill-rule="nonzero">
            <g id="Group-2" transform="translate(280.000000, 207.000000)">
                <g id="Group-6" transform="translate(17.000000, 16.000000)">
                    <g id="Group-11">
                        <g id="negative-request---icon" transform="translate(549.000000, 171.000000)">
                            <path d="M8.8,0 C4,0 0,3.9 0,8.8 C0,13.7 3.9,17.6 8.8,17.6 C13.7,17.6 17.6,13.7 17.6,8.8 C17.6,3.9 13.6,0 8.8,0 L8.8,0 Z" id="Shape" stroke="#AFAFAF"></path>
                            <path d="M9.65,8.95 L11.75,6.85 C11.95,6.65 11.95,6.35 11.75,6.15 C11.55,5.95 11.25,5.95 11.05,6.15 L8.95,8.25 L6.85,6.15 C6.65,5.95 6.35,5.95 6.15,6.15 C5.95,6.35 5.95,6.65 6.15,6.85 L8.25,8.95 L6.15,11.05 C5.95,11.25 5.95,11.55 6.15,11.75 C6.25,11.85 6.35,11.85 6.55,11.85 C6.75,11.85 6.85,11.85 6.95,11.75 L9.05,9.65 L11.15,11.75 C11.25,11.85 11.35,11.85 11.55,11.85 C11.75,11.85 11.85,11.85 11.95,11.75 C12.15,11.55 12.15,11.25 11.95,11.05 L9.65,8.95 Z" id="Shape" fill="#E35F46"></path>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>&nbsp;Отклоненно</span>
        </li>
        <li class="for">
            <span class="left hidden-lg hidden-sm hidden-md">Сумма</span>
            <span class="right green">+ $0.40</span>
        </li>
        <li>
            <button class="right hidden-xs infDesk" role="button" data-toggle="popover" data-trigger="focus " data-html="true"
               data-placement="top auto" data-content='<ul class="list-unstyled">
    <li><span class="left">Средства были списаны cо счета:</span><span class="black right">USD</span></li>
    <li><span class="left">Баланс до операции:</span><span class="black right">$1000.00</span></li>
    <li><span class="left">Баланс после операции:</span><span class="black right">$1000.40 <i class="greenText">(+$0.40)</i></span></li>
    <li><span class="left">Сдача товара: </span><span class="red right"><a href="#">Дрелль Макита</a></span></li>

    <svg class="btnClose" id="btnCloseTransaction" width="8px" height="8px" viewBox="0 0 8 8" >
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
                        <g id="btnCloseInform" transform="translate(-525.000000, -774.000000)" stroke="#FFFFFF" fill="#fff">
                            <g id="content" transform="translate(280.000000, 151.000000)">
                                <g id="Group-22" transform="translate(20.000000, 89.000000)">
                                    <g id="Group-20">
                                        <g id="Group-19" transform="translate(0.000000, 204.000000)">
                                            <g id="Group-25" transform="translate(70.000000, 273.000000)">
                                                <g id="close" transform="translate(151.000000, 53.000000)">
                                                    <g transform="translate(4.888889, 4.888889)" id="Combined-Shape">
                                                        <path d="M3.11111111,2.48257175 L0.832788199,0.204248838 C0.659221847,0.0306824861 0.37781519,0.0306824861 0.204248838,0.204248838 L0.204248838,0.832788199 L2.48257175,3.11111111 L0.151606014,5.44207685 C-0.0219603375,5.6156432 -0.0219603375,5.89704986 0.151606014,6.07061621 C0.325172366,6.24418256 0.606579024,6.24418256 0.780145375,6.07061621 L3.11111111,3.73965047 L5.44207685,6.07061621 C5.6156432,6.24418256 5.89704986,6.24418256 6.07061621,6.07061621 C6.24418256,5.89704986 6.24418256,5.6156432 6.07061621,5.44207685 L3.73965047,3.11111111 L6.01797338,0.832788199 C6.19153974,0.659221847 6.19153974,0.37781519 6.01797338,0.204248838 L5.38943402,0.204248838 L3.11111111,2.48257175 Z"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
    </ul>'>
                <svg width="17px" height="17px" viewBox="0 0 17 17">
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="casaHistory" transform="translate(-377.000000, -552.000000)"
                           fill-rule="nonzero" fill="#90949B">
                            <g id="KACCA" transform="translate(0.000000, 259.000000)">
                                <g id="Group" transform="translate(0.000000, 56.000000)">
                                    <g id="Tr-1-Copy" transform="translate(20.000000, 235.000000)">
                                        <g id="Group-3-Copy" transform="translate(357.000000, 2.000000)">
                                            <path d="M16.3,15.5 L14.2,13.4 C15.5,12 16.2,10.1 16.2,8.1 C16.2,3.6 12.6,0 8.1,0 C3.6,0 0,3.6 0,8.1 C0,12.6 3.6,16.2 8.1,16.2 C10.2,16.2 12,15.4 13.4,14.2 L15.5,16.3 C15.6,16.4 15.7,16.4 15.9,16.4 C16.1,16.4 16.2,16.4 16.3,16.3 C16.5,16.1 16.5,15.7 16.3,15.5 Z M1,8 C1,4.1 4.2,0.9 8.1,0.9 C12,0.9 15.2,4.1 15.2,8 C15.2,11.9 12,15.1 8.1,15.1 C4.2,15.1 1,12 1,8 Z"
                                                  id="Shape"></path>
                                            <path d="M9.5,11.2 L8.6,11.2 L8.6,7.4 C8.6,7.1 8.4,6.9 8.1,6.9 L6.8,6.9 C6.5,6.9 6.3,7.1 6.3,7.4 C6.3,7.7 6.5,7.9 6.8,7.9 L7.6,7.9 L7.6,11.2 L6.8,11.2 C6.5,11.2 6.3,11.4 6.3,11.7 C6.3,12 6.5,12.2 6.8,12.2 L9.4,12.2 C9.7,12.2 9.9,12 9.9,11.7 C9.9,11.4 9.7,11.2 9.5,11.2 Z"
                                                  id="Shape"></path>
                                            <circle id="Oval" cx="7.9" cy="4.4" r="1.1"></circle>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>


            </button>
        </li>
    </ul>
</div>
