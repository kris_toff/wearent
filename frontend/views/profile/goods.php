<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 07.08.2017
 * Time: 12:23
 */

use yii\bootstrap\Html;
use yii\widgets\Pjax;

/** @var \yii\web\View $this */

\yii\bootstrap\BootstrapPluginAsset::register($this);
\frontend\assets\ReadmoreAsset::register($this);
\frontend\assets\SliderAsset::register($this);
\frontend\assets\ProductAsset::register($this);
\frontend\assets\yyAsset::register($this);

/** @var \yii\data\ActiveDataProvider $dataProvider */

?>

<div class="marg hidden-xs">

</div>
<div class="row goods  contentBody">
    <div class="container sidebarEditLeft hidden-xs hidden-sm">
        <div class="row">
            <div class="col-sm-12">
                <?= $this->render('edit/_left-menu') ?>
            </div>
        </div>
    </div>
    <div class="container content">
        <div class="row">
            <div class="col-xs-12">
                <div class="titleEdit">
                    <h4 class="text-left">
                        <?= \Yii::t('UI6', '$HEADER$'); ?>
                    </h4>
                </div>
            </div>
        </div>

        <div class="row greyBackg">
            <div class="col-xs-12 col-sm-7">
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                        Сортировка по статусу <svg class="caretMenu" width="14px" height="14px" viewBox="0 0 8 14">
                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                <g class="re" transform="translate(-1207.000000, -591.000000)"  stroke-width="2">
                                    <g id="Group-11" transform="translate(0.000000, 579.000000)">
                                        <g id="Recommended">
                                            <g id="Lenta" transform="translate(210.000000, 0.000000)">
                                                <g id="arrows" transform="translate(927.000000, 1.000000)">
                                                    <g id="Group-37" transform="translate(56.000000, 0.000000)">
                                                        <polyline id="arrow_right_black" transform="translate(17.935389, 17.699575) scale(-1, 1) rotate(90.000000) translate(-17.935389, -17.699575) " points="15 23.3991501 20.8707778 17.5283723 15.3424055 12"></polyline>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>

                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Дія</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Інша дія</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Щось ще тут</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Відокремлений лінк</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5 styleInput">
                <form action="" class="searchGoods text-right" method="post">
                    <input type="text" class="" placeholder="Какое объявление вы хотите найти?">
                </form>
            </div>
        </div>

        <?php Pjax::begin([
               'id'=> 'products-list-pjax',

        ]); ?>

        <div class="row allGoods">
            <div class="col-xs-12">

                <?= \yii\widgets\ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemView' => '_goodsBlock',
                        'summary' => '',
                ]); ?>
<!--                --><?//= $this-> render('goodsBlock'); ?>
            </div>
        </div>

<!--        <div class="row">-->
<!--            <div class="col-sm-12">-->
<!--                <nav>-->
<!--                    <ul class="pagination">-->
<!--                        <li><a href="#"><span aria-hidden="true">Пред. страница</span></a></li>-->
<!--                        <li><a href="#">1</a></li>-->
<!--                        <li><a href="#">2</a></li>-->
<!--                        <li><a href="#">3</a></li>-->
<!--                        <li><a href="#">4</a></li>-->
<!--                        <li><a href="#">5</a></li>-->
<!--                        <li><a href="#"><span aria-hidden="true">След. страница</span></a></li>-->
<!--                    </ul>-->
<!--                </nav>-->
<!--            </div>-->
<!--        </div>-->


        <?php Pjax::end(); ?>

    </div>
</div>

