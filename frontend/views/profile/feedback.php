<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 09.06.2017
 * Time: 11:17
 */

use yii\bootstrap\Html;

/** @var \yii\web\View $this */

\yii\bootstrap\BootstrapPluginAsset::register($this);
\frontend\assets\ReadmoreAsset::register($this);
\frontend\assets\SliderAsset::register($this);
\frontend\assets\ProductAsset::register($this);
\frontend\assets\yyAsset::register($this);

?>

<div class="marg">

</div>
<div class="row feedback">
    <div class="container sidebarEditLeft">
        <div class="row">
            <div class="col-sm-12">
                <?= $this->render('left-menu') ?>
            </div>
        </div>
    </div>


    <div class="container content">
        <div class="row">
            <div class="col-sm-12">
                <div class="titleEdit">
                    <h4 class="text-left">Отзывы</h4>
                </div>

                <div class="wrapPanel">
                    <div role="tabpanel" class="tabpanelFeedback">

                        <!-- Навігаційні елементи вкладок -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active left text-center"><a href="#feedbackAbout"
                                                                                       aria-controls="feedbackAbout"
                                                                                       role="tab" data-toggle="tab">Отзывы
                                    о вас</a></li>
                            <li role="presentation" class="right text-center"><a href="#feedbackYour"
                                                                                 aria-controls="feedbackYour" role="tab"
                                                                                 data-toggle="tab">Ваши отзывы</a></li>
                        </ul>

                        <!-- Вкладки панелі -->
                        <div class="tab-content">

                            <!--                            відгуки про вас-->
                            <div role="tabpanel" class="tab-pane active" id="feedbackAbout">

                                <div class="comment firstComment">
                                    <?= $this->render('comment') ?>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="feedbackYour">Lorem ipsum dolor sit amet,
                                consectetur
                                adipisicing elit. Alias architecto assumenda, at cumque debitis doloribus esse ex
                                impedit
                                iusto, molestias necessitatibus nobis, nostrum optio pariatur perferendis reprehenderit
                                sint
                                tenetur unde.
                            </div>

                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
    <form action="/" method="post">

    </form>
</div>





