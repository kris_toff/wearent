<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 12.06.2017
 * Time: 10:33
 */

/** @var \yii\web\View $this */

?>


<div class="card">
    <div class="cardLeft">
        <span class="topBlock"></span>
        <div class="infBlock">
            <img src="" alt="MasterCard">
            <span class="numberCard">4120 **** **** 4212</span>
            <div class="bottom">
                <span class="name">Chudakov Dmitry</span>
                <span class="date">12/17</span>
            </div>
        </div>


    </div>
    <div class="cardBody">
        <div class="statusSuccess" ">
            <svg width="19px" height="18px" viewBox="0 0 19 18">
                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="UI9.-Карточка-товара---альтернатива" transform="translate(-231.000000, -717.000000)"
                       fill-rule="nonzero">
                        <g id="DESCRIBTION" transform="translate(210.000000, 624.000000)">
                            <g id="Group-5">
                                <g id="Group-2" transform="translate(21.000000, 93.000000)">
                                    <g id="Group">
                                        <path d="M9.4,1.4 C13.7,1.4 17.2,4.9 17.2,9.2 C17.2,13.5 13.7,17 9.4,17 C5.1,17 1.6,13.5 1.6,9.2 C1.6,4.9 5.1,1.4 9.4,1.4 L9.4,1.4 Z M9.4,0.4 C4.6,0.4 0.6,4.3 0.6,9.2 C0.6,14.1 4.5,18 9.4,18 C14.3,18 18.2,14.1 18.2,9.2 C18.2,4.3 14.2,0.4 9.4,0.4 L9.4,0.4 Z"
                                              id="Shape" fill="#AFAFAF"></path>
                                        <path d="M8.9,11.6 C8.8,11.6 8.6,11.5 8.5,11.4 L6.4,9.1 C6.2,8.9 6.2,8.6 6.4,8.4 C6.6,8.2 6.9,8.2 7.1,8.4 L8.9,10.3 L12.1,6.5 C12.3,6.3 12.6,6.3 12.8,6.4 C13,6.6 13,6.9 12.9,7.1 L9.3,11.3 C9.2,11.5 9.1,11.6 8.9,11.6 L8.9,11.6 Z"
                                              id="Shape" fill="#4CDB58"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>&nbsp;Карта привязана
        </div>
        <div class="audit" style="display: none;">
            <span><svg width="19px" height="18px" viewBox="0 0 19 18">
                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="UI9.-Карточка-товара---альтернатива" transform="translate(-231.000000, -717.000000)"
                       fill-rule="nonzero">
                        <g id="DESCRIBTION" transform="translate(210.000000, 624.000000)">
                            <g id="Group-5">
                                <g id="Group-2" transform="translate(21.000000, 93.000000)">
                                    <g id="Group">
                                        <path d="M9.4,1.4 C13.7,1.4 17.2,4.9 17.2,9.2 C17.2,13.5 13.7,17 9.4,17 C5.1,17 1.6,13.5 1.6,9.2 C1.6,4.9 5.1,1.4 9.4,1.4 L9.4,1.4 Z M9.4,0.4 C4.6,0.4 0.6,4.3 0.6,9.2 C0.6,14.1 4.5,18 9.4,18 C14.3,18 18.2,14.1 18.2,9.2 C18.2,4.3 14.2,0.4 9.4,0.4 L9.4,0.4 Z"
                                              id="Shape" fill="#AFAFAF"></path>
                                        <path d="M8.9,11.6 C8.8,11.6 8.6,11.5 8.5,11.4 L6.4,9.1 C6.2,8.9 6.2,8.6 6.4,8.4 C6.6,8.2 6.9,8.2 7.1,8.4 L8.9,10.3 L12.1,6.5 C12.3,6.3 12.6,6.3 12.8,6.4 C13,6.6 13,6.9 12.9,7.1 L9.3,11.3 C9.2,11.5 9.1,11.6 8.9,11.6 L8.9,11.6 Z"
                                              id="Shape" fill="#4CDB58"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>&nbsp;Веривикация</span>
            <p class="text-left">В целях безопасности, с вашей карты будет списана не значительная контрольная сумма Пожалуйста, укажите какая сумма была списана:</p><br>
            <a href="#" class="editSumm">Указать списанную сумму</a>
        </div>
    </div>
</div>
