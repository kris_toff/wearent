<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 09.06.2017
 * Time: 13:02
 */

/** @var \yii\web\View $this */

?>

<ul class="media-list">
    <li class="media">
        <a class="media-left " href="#">
            <img src="..." alt="imgAccount">
        </a>
        <div class="media-body">

            <div class="media-heading">
                <div class="leftHeading">
                    <div class="inline">
                        <h4 class="name">Доба Дмитрий</h4> <span>• 1 час назад</span>
                        <a href="#">Ответить</a>
                    </div>
                    <div class="columnHeading">
                        <p class="tovar">Tовар: <span>Перфоратор на зло соседям</span></p>
                        <span class="givGet">Сдавал в аренду</span>
                    </div>


                </div>
                <div class="rightHeading dropdown">

                    <button class="editPopup" id="dLabeComment12" type="button" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                        <span class="circle"></span>
                        <span class="circle"></span>
                        <span class="circle"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabeComment12">
                        <li><a href="#">
                                <svg class="imgLiPunct" width="18px" height="18px" viewBox="0 0 18 18"
                                     version="1.1">

                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                       fill-rule="evenodd">
                                        <g id="imgLiPunct"
                                           transform="translate(-671.000000, -1156.000000)"
                                           fill-rule="nonzero"
                                        ">
                                        <g id="reviews" transform="translate(210.000000, 1019.000000)">
                                            <g id="Group-22"
                                               transform="translate(21.000000, 86.000000)">
                                                <g id="Group-20">
                                                    <g id="Group-4">
                                                        <g id="Group-24"
                                                           transform="translate(422.000000, 26.000000)">
                                                            <g id="edit_icon"
                                                               transform="translate(18.000000, 25.000000)">
                                                                <path d="M15.6,9.6 C15.3,9.6 15.1,9.8 15.1,10.1 L15.1,16.5 L1.7,16.5 L1.7,3.1 L8.2,3.1 C8.5,3.1 8.7,2.9 8.7,2.6 C8.7,2.3 8.5,2.1 8.2,2.1 L1.2,2.1 C0.9,2.1 0.7,2.3 0.7,2.6 L0.7,17 C0.7,17.3 0.9,17.5 1.2,17.5 L15.6,17.5 C15.9,17.5 16.1,17.3 16.1,17 L16.1,10.1 C16.1,9.9 15.9,9.6 15.6,9.6 Z"
                                                                      id="Shape"></path>
                                                                <path d="M17.7,3.8 L14.5,0.6 C14.3,0.4 14,0.4 13.8,0.6 L5.4,9 C5.3,9.1 5.3,9.2 5.3,9.3 L4.7,13 C4.7,13.2 4.7,13.3 4.8,13.4 C4.9,13.5 5,13.5 5.2,13.5 L5.3,13.5 L9,12.9 C9.1,12.9 9.2,12.8 9.3,12.8 L17.7,4.4 C17.9,4.3 17.9,4 17.7,3.8 Z M8.7,12 L5.8,12.4 L6.2,9.5 L11.8,3.9 L14.2,6.3 L8.7,12 Z M15.1,5.7 L12.7,3.3 L14.3,1.7 L16.7,4.1 L15.1,5.7 Z"
                                                                      id="Shape"></path>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                    </g>
                                </svg>
                                Редактировать</a></li>
                        <li><a href="#">
                                <svg class="imgLiPunct" width="16px" height="15px" viewBox="0 0 16 15"
                                     version="1.1">

                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                       fill-rule="evenodd">
                                        <g id="imgLiPunct"
                                           transform="translate(-671.000000, -1199.000000)">
                                            <g id="reviews"
                                               transform="translate(210.000000, 1019.000000)">
                                                <g id="Group-22"
                                                   transform="translate(21.000000, 86.000000)">
                                                    <g id="Group-20">
                                                        <g id="Group-4">
                                                            <g id="Group-24"
                                                               transform="translate(422.000000, 26.000000)">
                                                                <g id="report_icon"
                                                                   transform="translate(18.000000, 68.000000)">
                                                                    <path d="M14.8,0.4 L1.8,0.4 C1.2,0.4 0.7,0.9 0.7,1.5 L0.7,10.9 C0.7,11.5 1.2,12 1.8,12 L3.2,12 L3.2,13.8 C3.2,14 3.3,14.2 3.5,14.3 L3.7,14.3 C3.8,14.3 3.9,14.3 4,14.2 L6.4,12 L14.7,12 C15.3,12 15.8,11.5 15.8,10.9 L15.8,1.5 C15.9,0.9 15.4,0.4 14.8,0.4 Z M14.9,10.9 C14.9,11 14.8,11 14.8,11 L6.3,11 C6.2,11 6.1,11 6,11.1 L4.3,12.6 L4.3,11.4 C4.3,11.1 4.1,10.9 3.8,10.9 L1.9,10.9 C1.8,10.9 1.8,10.8 1.8,10.8 L1.8,1.4 C1.8,1.3 1.9,1.3 1.9,1.3 L14.9,1.3 C15,1.3 15,1.4 15,1.4 L15,10.9 L14.9,10.9 Z"
                                                                          id="Shape"
                                                                          fill-rule="nonzero"></path>
                                                                    <path d="M8,8 L9.072,8 L9.072,9 L8,9 L8,8 Z M8.04,3.288 L9.04,3.288 L9.04,4.936 L8.776,7.408 L8.304,7.408 L8.04,4.936 L8.04,3.288 Z"
                                                                          id="!"
                                                                          opacity="0.900000036"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                                Сообщить о нарушении</a></li>
                        <li><a href="#">
                                <svg class="imgLiPunct" width="14px" height="19px" viewBox="0 0 14 19"
                                     version="1.1">

                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                       fill-rule="evenodd">
                                        <g id="imgLiPunct"
                                           transform="translate(-673.000000, -1235.000000)"
                                           fill-rule="nonzero" stroke="#AFAFAF" stroke-width="0.3">
                                            <g id="reviews"
                                               transform="translate(210.000000, 1019.000000)">
                                                <g id="Group-22"
                                                   transform="translate(21.000000, 86.000000)">
                                                    <g id="Group-20">
                                                        <g id="Group-4">
                                                            <g id="Group-24"
                                                               transform="translate(422.000000, 26.000000)">
                                                                <g id="delete_icon"
                                                                   transform="translate(21.000000, 105.000000)">
                                                                    <path d="M11.9953016,2.04505221 L8.84168254,2.04505221 L8.84168254,1.36682731 C8.84168254,0.617769076 8.27685714,0.01037751 7.58022222,0.01037751 L4.42660317,0.01037751 C3.72996825,0.01037751 3.16514286,0.617769076 3.16514286,1.36682731 L3.16514286,2.04505221 L0.0115238095,2.04505221 L0.0115238095,2.72327711 L0.661365079,2.72327711 L1.29269841,15.6094478 C1.29269841,16.3585402 1.85752381,16.9658976 2.55415873,16.9658976 L9.49212698,16.9658976 C10.1887619,16.9658976 10.7535873,16.3585402 10.7535873,15.6094478 L11.3738413,2.72327711 L11.9953333,2.72327711 L11.9953333,2.04505221 L11.9953016,2.04505221 Z M3.79587302,1.36682731 C3.79587302,0.99262249 4.0792381,0.68860241 4.42660317,0.68860241 L7.58022222,0.68860241 C7.92822222,0.68860241 8.21095238,0.99262249 8.21095238,1.36682731 L8.21095238,2.04505221 L3.79587302,2.04505221 L3.79587302,1.36682731 Z M10.1240635,15.5743213 L10.1228254,15.5915602 L10.1228254,15.6094478 C10.1228254,15.983004 9.84009524,16.2876727 9.49209524,16.2876727 L2.55412698,16.2876727 C2.2067619,16.2876727 1.92339683,15.983004 1.92339683,15.6094478 L1.92339683,15.5915602 L1.92279365,15.5737068 L1.29269841,2.72327711 L10.7425079,2.72327711 L10.1240635,15.5743213 Z"
                                                                          id="Shape"></path>
                                                                    <rect id="Rectangle-path"
                                                                          x="5.68803175" y="4.07969277"
                                                                          width="1"
                                                                          height="10.851496"></rect>
                                                                    <polygon id="Shape"
                                                                             points="4.4407619 14.9093755 3.79526984 4.07907831 3.16577778 4.12209036 3.81130159 14.9523876"></polygon>
                                                                    <polygon id="Shape"
                                                                             points="8.846 4.10089157 8.21650794 4.05852811 7.58022222 14.9100241 8.20971429 14.9523876"></polygon>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                                Удалить</a></li>
                    </ul>

                </div>
            </div>

            <p class="text readmoreFedback">Существует целый ряд различных факторов, оказывающих влияние на принятие
                решения
                покупателем. Существует целый ряд различных факторов, оказывающих влияние на принятие решения
                покупателем.Существует целый ряд различных факторов, оказывающих влияние на принятие решения
                покупателем.Существует целый ряд различных факторов, оказывающих влияние на принятие решения
                покупателем.Существует целый ряд различных факторов, оказывающих влияние на принятие решения
                покупателем.
            </p>
            <p class="like text-right">
                <svg class="likeIcon" width="19px" height="18px" viewBox="0 0 19 18" >
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="likeIcon" transform="translate(-1235.000000, -523.000000)">
                            <g id="content" transform="translate(280.000000, 151.000000)">
                                <g id="Group-22" transform="translate(20.000000, 89.000000)">
                                    <g id="Group-20">
                                        <g id="Group-19" transform="translate(0.000000, 204.000000)">
                                            <g id="like_icon-copy" transform="translate(935.000000, 79.000000)">
                                                <path d="M9.84718182,17.6587043 L9.5,17.8116311 L9.15281818,17.6587043 C4.69040909,14.9064558 0,8.64166921 0,5.16214939 C0.0151136364,2.31301829 2.33354545,3.85870198e-16 5.18181818,3.85870198e-16 C6.98206818,3.85870198e-16 8.57115909,0.924512195 9.5,2.32387957 C10.4288409,0.924512195 12.0179318,0 13.8181818,0 C16.6664545,0 18.9848864,2.31301829 19,5.16214939 C19,8.64166921 14.3095909,14.9064558 9.84718182,17.6587043 L9.84718182,17.6587043 Z M13.8181818,0.864992378 C12.3698636,0.864992378 11.02475,1.58922256 10.2189773,2.80264482 L9.5,3.88573171 L8.78102273,2.80221037 C7.97525,1.58922256 6.63013636,0.864992378 5.18181818,0.864992378 C2.81372727,0.864992378 0.876159091,2.7948247 0.863636364,5.16214939 C0.863636364,8.2311128 5.28286364,14.2022104 9.5,16.8562729 C13.7171364,14.2022104 18.1363636,8.23154726 18.1363636,5.1664939 C18.1238409,2.7948247 16.1862727,0.864992378 13.8181818,0.864992378 L13.8181818,0.864992378 Z" id="Shape"></path>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg> 12</p>

            <div class="slideComment">
                <div><img src="/img/slideComment/s4.jpg" alt="" class="img-responsive"></div>
                <div><img src="/img/slideComment/s3.jpg" alt="" class="img-responsive"></div>
                <div><img src="/img/slideComment/s2.jpg" alt="" class="img-responsive"></div>
            </div>


            <div class="media">
                <a class="media-left" href="#">
                    <img src="..." alt="imgAccount">
                </a>
                <div class="media-body">

                    <div class="media-heading">
                        <div class="leftHeading">
                            <div class="inline">
                                <h4 class="name">Доба Дмитрий</h4> <span>• 1 час назад</span>
                                <a href="#">Ответить</a>
                            </div>
                            <div class="columnHeading">
                                <!--                                <p class="tovar">Tовар: <span>Перфоратор на зло соседям</span></p>-->
                                <!--                                <span class="givGet">Сдавал в аренду</span>-->
                            </div>


                        </div>
                        <div class="rightHeading dropdown">

                            <button class="editPopup" id="dLabeComment1" type="button" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                                <span class="circle"></span>
                                <span class="circle"></span>
                                <span class="circle"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabeComment1">
                                <li><a href="#">
                                        <svg class="imgLiPunct" width="18px" height="18px" viewBox="0 0 18 18"
                                             version="1.1">

                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                               fill-rule="evenodd">
                                                <g id="imgLiPunct"
                                                   transform="translate(-671.000000, -1156.000000)"
                                                   fill-rule="nonzero"
                                                ">
                                                <g id="reviews" transform="translate(210.000000, 1019.000000)">
                                                    <g id="Group-22"
                                                       transform="translate(21.000000, 86.000000)">
                                                        <g id="Group-20">
                                                            <g id="Group-4">
                                                                <g id="Group-24"
                                                                   transform="translate(422.000000, 26.000000)">
                                                                    <g id="edit_icon"
                                                                       transform="translate(18.000000, 25.000000)">
                                                                        <path d="M15.6,9.6 C15.3,9.6 15.1,9.8 15.1,10.1 L15.1,16.5 L1.7,16.5 L1.7,3.1 L8.2,3.1 C8.5,3.1 8.7,2.9 8.7,2.6 C8.7,2.3 8.5,2.1 8.2,2.1 L1.2,2.1 C0.9,2.1 0.7,2.3 0.7,2.6 L0.7,17 C0.7,17.3 0.9,17.5 1.2,17.5 L15.6,17.5 C15.9,17.5 16.1,17.3 16.1,17 L16.1,10.1 C16.1,9.9 15.9,9.6 15.6,9.6 Z"
                                                                              id="Shape"></path>
                                                                        <path d="M17.7,3.8 L14.5,0.6 C14.3,0.4 14,0.4 13.8,0.6 L5.4,9 C5.3,9.1 5.3,9.2 5.3,9.3 L4.7,13 C4.7,13.2 4.7,13.3 4.8,13.4 C4.9,13.5 5,13.5 5.2,13.5 L5.3,13.5 L9,12.9 C9.1,12.9 9.2,12.8 9.3,12.8 L17.7,4.4 C17.9,4.3 17.9,4 17.7,3.8 Z M8.7,12 L5.8,12.4 L6.2,9.5 L11.8,3.9 L14.2,6.3 L8.7,12 Z M15.1,5.7 L12.7,3.3 L14.3,1.7 L16.7,4.1 L15.1,5.7 Z"
                                                                              id="Shape"></path>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                            </g>
                                        </svg>
                                        Редактировать</a></li>
                                <li><a href="#">
                                        <svg class="imgLiPunct" width="16px" height="15px" viewBox="0 0 16 15"
                                             version="1.1">

                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                               fill-rule="evenodd">
                                                <g id="imgLiPunct"
                                                   transform="translate(-671.000000, -1199.000000)">
                                                    <g id="reviews"
                                                       transform="translate(210.000000, 1019.000000)">
                                                        <g id="Group-22"
                                                           transform="translate(21.000000, 86.000000)">
                                                            <g id="Group-20">
                                                                <g id="Group-4">
                                                                    <g id="Group-24"
                                                                       transform="translate(422.000000, 26.000000)">
                                                                        <g id="report_icon"
                                                                           transform="translate(18.000000, 68.000000)">
                                                                            <path d="M14.8,0.4 L1.8,0.4 C1.2,0.4 0.7,0.9 0.7,1.5 L0.7,10.9 C0.7,11.5 1.2,12 1.8,12 L3.2,12 L3.2,13.8 C3.2,14 3.3,14.2 3.5,14.3 L3.7,14.3 C3.8,14.3 3.9,14.3 4,14.2 L6.4,12 L14.7,12 C15.3,12 15.8,11.5 15.8,10.9 L15.8,1.5 C15.9,0.9 15.4,0.4 14.8,0.4 Z M14.9,10.9 C14.9,11 14.8,11 14.8,11 L6.3,11 C6.2,11 6.1,11 6,11.1 L4.3,12.6 L4.3,11.4 C4.3,11.1 4.1,10.9 3.8,10.9 L1.9,10.9 C1.8,10.9 1.8,10.8 1.8,10.8 L1.8,1.4 C1.8,1.3 1.9,1.3 1.9,1.3 L14.9,1.3 C15,1.3 15,1.4 15,1.4 L15,10.9 L14.9,10.9 Z"
                                                                                  id="Shape"
                                                                                  fill-rule="nonzero"></path>
                                                                            <path d="M8,8 L9.072,8 L9.072,9 L8,9 L8,8 Z M8.04,3.288 L9.04,3.288 L9.04,4.936 L8.776,7.408 L8.304,7.408 L8.04,4.936 L8.04,3.288 Z"
                                                                                  id="!"
                                                                                  opacity="0.900000036"></path>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                        Сообщить о нарушении</a></li>
                                <li><a href="#">
                                        <svg class="imgLiPunct" width="14px" height="19px" viewBox="0 0 14 19"
                                             version="1.1">

                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                               fill-rule="evenodd">
                                                <g id="imgLiPunct"
                                                   transform="translate(-673.000000, -1235.000000)"
                                                   fill-rule="nonzero" stroke="#AFAFAF" stroke-width="0.3">
                                                    <g id="reviews"
                                                       transform="translate(210.000000, 1019.000000)">
                                                        <g id="Group-22"
                                                           transform="translate(21.000000, 86.000000)">
                                                            <g id="Group-20">
                                                                <g id="Group-4">
                                                                    <g id="Group-24"
                                                                       transform="translate(422.000000, 26.000000)">
                                                                        <g id="delete_icon"
                                                                           transform="translate(21.000000, 105.000000)">
                                                                            <path d="M11.9953016,2.04505221 L8.84168254,2.04505221 L8.84168254,1.36682731 C8.84168254,0.617769076 8.27685714,0.01037751 7.58022222,0.01037751 L4.42660317,0.01037751 C3.72996825,0.01037751 3.16514286,0.617769076 3.16514286,1.36682731 L3.16514286,2.04505221 L0.0115238095,2.04505221 L0.0115238095,2.72327711 L0.661365079,2.72327711 L1.29269841,15.6094478 C1.29269841,16.3585402 1.85752381,16.9658976 2.55415873,16.9658976 L9.49212698,16.9658976 C10.1887619,16.9658976 10.7535873,16.3585402 10.7535873,15.6094478 L11.3738413,2.72327711 L11.9953333,2.72327711 L11.9953333,2.04505221 L11.9953016,2.04505221 Z M3.79587302,1.36682731 C3.79587302,0.99262249 4.0792381,0.68860241 4.42660317,0.68860241 L7.58022222,0.68860241 C7.92822222,0.68860241 8.21095238,0.99262249 8.21095238,1.36682731 L8.21095238,2.04505221 L3.79587302,2.04505221 L3.79587302,1.36682731 Z M10.1240635,15.5743213 L10.1228254,15.5915602 L10.1228254,15.6094478 C10.1228254,15.983004 9.84009524,16.2876727 9.49209524,16.2876727 L2.55412698,16.2876727 C2.2067619,16.2876727 1.92339683,15.983004 1.92339683,15.6094478 L1.92339683,15.5915602 L1.92279365,15.5737068 L1.29269841,2.72327711 L10.7425079,2.72327711 L10.1240635,15.5743213 Z"
                                                                                  id="Shape"></path>
                                                                            <rect id="Rectangle-path"
                                                                                  x="5.68803175" y="4.07969277"
                                                                                  width="1"
                                                                                  height="10.851496"></rect>
                                                                            <polygon id="Shape"
                                                                                     points="4.4407619 14.9093755 3.79526984 4.07907831 3.16577778 4.12209036 3.81130159 14.9523876"></polygon>
                                                                            <polygon id="Shape"
                                                                                     points="8.846 4.10089157 8.21650794 4.05852811 7.58022222 14.9100241 8.20971429 14.9523876"></polygon>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                        Удалить</a></li>
                            </ul>

                        </div>

                    </div>


                    <div class="text readmoreFedback">Существует целый ряд различных факторов, оказывающих влияние на
                        принятие решения
                        покупателем. Существует целый ряд различных факторов, оказывающих влияние на принятие решения
                        покупателем.
                    </div>
                    <p class="like text-right">12</p>
                </div>
            </div>
        </div>


    </li>
</ul>
