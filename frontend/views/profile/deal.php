<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 26.07.2017
 * Time: 11:11
 */


use yii\bootstrap\Html;

/** @var \yii\web\View $this */

\yii\bootstrap\BootstrapPluginAsset::register($this);
\frontend\assets\ReadmoreAsset::register($this);
\frontend\assets\SliderAsset::register($this);
\frontend\assets\ProductAsset::register($this);
\frontend\assets\yyAsset::register($this);

?>


<div class="marg">

</div>
<div class="row deal contentBody">
    <div class="container sidebarEditLeft hidden-xs hidden-sm">
        <div class="row">
            <div class="col-sm-12">
                <?= $this->render('left-menu') ?>
            </div>
        </div>
    </div>

    <div class="container content hiddenDealPage" ">
        <div class="row">
<!--            <a href="#" class="prevPage text-left hidden-lg hidden-md">← Предыдущая страница</a>-->

            <div class="titleEdit">
                <h4 class="text-left">История ваших сделок</h4>
            </div>

            <div class="wrapPanel">
                <div role="tabpanel" class="tabpanelDeal">

                    <!-- Навігаційні елементи вкладок -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active text-center left"><a href="#youAreRenting"
                                                                                   aria-controls="youAreRenting"
                                                                                   role="tab" data-toggle="tab">
                                <svg width="8px" height="14px" viewBox="0 0 8 14">

                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"
                                       stroke-linecap="round" stroke-linejoin="round">
                                        <g id="redHov" transform="translate(-47.000000, -281.000000)" stroke-width="2">
                                            <g id="Switcher" transform="translate(0.000000, 259.000000)">
                                                <g id="2">
                                                    <g id="Group-8" transform="translate(48.000000, 19.000000)">
                                                        <polyline id="Path-3-Copy"
                                                                  transform="translate(2.935389, 9.699575) scale(-1, 1) translate(-2.935389, -9.699575) "
                                                                  points="-4.52970994e-13 15.3991501 5.87077781 9.52837229 0.342405526 4"></polyline>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                                Вы арендуете</a></li>
                        <li role="presentation" class="text-center right"><a href="#yourProduct"
                                                                             aria-controls="yourProduct" role="tab"
                                                                             data-toggle="tab">
                                <svg width="8px" height="14px" viewBox="0 0 8 14">
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"
                                       stroke-linecap="round" stroke-linejoin="round">
                                        <g id="whiteHov" transform="translate(-384.000000, -6256.000000)"
                                           stroke-width="2">
                                            <g id="Group-11" transform="translate(0.000000, 579.000000)">
                                                <g id="STATISTICS" transform="translate(0.000000, 5404.000000)">
                                                    <g id="Bitmap">
                                                        <g id="Group-5" transform="translate(210.000000, 160.000000)">
                                                            <polyline id="Path-3"
                                                                      points="175 125.39915 180.870778 119.528372 175.342406 114"></polyline>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>&nbsp;Вы сдаете</a></li>
                    </ul>

                    <!-- Вкладки панелі -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="youAreRenting">
                            <div class="backgGrey">
                                <form action="" method="post" class="styleInput formForDeal">
                                    <div class="dropdown">
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                                                data-toggle="dropdown" aria-expanded="true">
                                            Сортировка по статусу&nbsp;
                                            <svg class="hidden-xs" width="14px" height="14px" viewBox="0 0 8 14">
                                                <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                   fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                    <g id="+UI2.-Главная"
                                                       transform="translate(-1207.000000, -591.000000)" stroke="#E35F46"
                                                       stroke-width="2">
                                                        <g id="Group-11" transform="translate(0.000000, 579.000000)">
                                                            <g id="Recommended">
                                                                <g id="Lenta"
                                                                   transform="translate(210.000000, 0.000000)">
                                                                    <g id="arrows"
                                                                       transform="translate(927.000000, 1.000000)">
                                                                        <g id="Group-37"
                                                                           transform="translate(56.000000, 0.000000)">
                                                                            <polyline id="arrow_right_black"
                                                                                      transform="translate(17.935389, 17.699575) scale(-1, 1) rotate(90.000000) translate(-17.935389, -17.699575) "
                                                                                      points="15 23.3991501 20.8707778 17.5283723 15.3424055 12"></polyline>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                            <svg class="hidden-md hidden-sm hidden-lg" width="10px" height="5px"
                                                 viewBox="0 0 10 5">
                                                <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                   fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                    <g id="UI3.-История-сделок---вы-арендуете"
                                                       transform="translate(-365.000000, -385.000000)" stroke="#3C3D3E">
                                                        <g id="FILTRES" transform="translate(0.000000, 315.000000)">
                                                            <g id="Group-19-Copy-4"
                                                               transform="translate(20.000000, 25.000000)">
                                                                <polyline id="Path-3-Copy-9"
                                                                          transform="translate(349.935000, 47.757144) scale(-1, 1) rotate(-270.000000) translate(-349.935000, -47.757144) "
                                                                          points="348 51.514287 351.87 47.6442871 348.225713 44"></polyline>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Дія</a>
                                            </li>
                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Інша
                                                    дія</a></li>

                                        </ul>
                                    </div>

                                    <input type="text" placeholder="Какую сделку вы хотите найти?">
                                </form>
                            </div>

                            <div class="allDeal row">
                                <div class="col-xs-12">
                                    <?= $this->render('blockDeal'); ?>
                                    <?= $this->render('blockDeal'); ?>
                                    <?= $this->render('blockDeal'); ?>
                                </div>

                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="yourProduct">

                            <div class="backgGrey">
                                <form action="" method="post" class="styleInput formForDeal">
                                    <div class="dropdown">
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                                                data-toggle="dropdown" aria-expanded="true">
                                            Сортировка по статусу&nbsp;
                                            <svg class="hidden-xs" width="14px" height="14px" viewBox="0 0 8 14">
                                                <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                   fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                    <g id="+UI2.-Главная"
                                                       transform="translate(-1207.000000, -591.000000)" stroke="#E35F46"
                                                       stroke-width="2">
                                                        <g id="Group-11" transform="translate(0.000000, 579.000000)">
                                                            <g id="Recommended">
                                                                <g id="Lenta"
                                                                   transform="translate(210.000000, 0.000000)">
                                                                    <g id="arrows"
                                                                       transform="translate(927.000000, 1.000000)">
                                                                        <g id="Group-37"
                                                                           transform="translate(56.000000, 0.000000)">
                                                                            <polyline id="arrow_right_black"
                                                                                      transform="translate(17.935389, 17.699575) scale(-1, 1) rotate(90.000000) translate(-17.935389, -17.699575) "
                                                                                      points="15 23.3991501 20.8707778 17.5283723 15.3424055 12"></polyline>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                            <svg class="hidden-md hidden-sm hidden-lg" width="10px" height="5px"
                                                 viewBox="0 0 10 5">
                                                <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                   fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                    <g id="UI3.-История-сделок---вы-арендуете"
                                                       transform="translate(-365.000000, -385.000000)" stroke="#3C3D3E">
                                                        <g id="FILTRES" transform="translate(0.000000, 315.000000)">
                                                            <g id="Group-19-Copy-4"
                                                               transform="translate(20.000000, 25.000000)">
                                                                <polyline id="Path-3-Copy-9"
                                                                          transform="translate(349.935000, 47.757144) scale(-1, 1) rotate(-270.000000) translate(-349.935000, -47.757144) "
                                                                          points="348 51.514287 351.87 47.6442871 348.225713 44"></polyline>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Дія</a>
                                            </li>
                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Інша
                                                    дія</a></li>

                                        </ul>
                                    </div>

                                    <input type="text" placeholder="Какую сделку вы хотите найти?">
                                </form>
                            </div>

                            <div class="allDeal row">
                                <div class="col-xs-12">
                                    <?= $this->render('blockDeal'); ?>
                                    <?= $this->render('blockDeal'); ?>
                                    <?= $this->render('blockDeal'); ?>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>

    <div class="container content detailsDeal" style="display: none">
        <div class="row topBlock">
            <div class="col-xs-6 text-left">
                <a href="#" class="red" id="prevPage">← Предыдущая страница</a>
            </div>
            <div class="col-xs-6  text-right">
                <a href="#" class="printing"><span>
                        <svg width="24px" height="24px" viewBox="0 0 24 24" >
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="+UI4.-Детали-сделки" transform="translate(-277.000000, -95.000000)">
            <g id="Group-3" transform="translate(278.000000, 96.000000)">
                <rect id="Rectangle-path" x="0" y="0" width="22" height="22"></rect>
                <circle id="Oval" fill="#E35F46" fill-rule="nonzero" cx="18.92" cy="9.24" r="1"></circle>
                <polyline id="Shape" stroke="#E35F46" stroke-linecap="round" points="17.6 6.6 17.6 0.44 4.4 0.44 4.4 6.6"></polyline>
                <polygon id="Shape" stroke="#E35F46" stroke-linecap="round" points="17.6 12.76 17.6 21.56 4.4 21.56 4.4 12.76"></polygon>
                <path d="M4.4,17.6 L1.32,17.6 C0.83424,17.6 0.44,17.20576 0.44,16.72 L0.44,7.48 C0.44,6.99424 0.83424,6.6 1.32,6.6 L20.68,6.6 C21.16576,6.6 21.56,6.99424 21.56,7.48 L21.56,16.72 C21.56,17.20576 21.16576,17.6 20.68,17.6 L17.6,17.6" id="Shape" stroke="#E35F46" stroke-linecap="round"></path>
            </g>
        </g>
    </g>
</svg></span> Распечатать</a>
            </div>
        </div>

        <div class="row idUser">
            <div class="col-xs-12 text-left">
                <p><strong>ID контракта: <span id="idUser">123456789</span></strong></p>
            </div>
        </div>

        <div class="row aboutUser">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-9 col-xs-12">
                        <div class="media">
                            <div class="media-left">
                                <a href="#"><img src="/img/account/foto.png" alt="..."></a>
                            </div>
                            <div class="media-body">
                                <h5 class="media-heading">Андрей Воробей</h5>
                                <p class="tel comunication">
                                    <svg class="imgCumunication" width="10px" height="18px" viewBox="0 0 10 18">
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="+UI4.-Детали-сделки" transform="translate(-458.000000, -273.000000)"
                                               fill-rule="nonzero" fill="#AFAFAF">
                                                <g id="REVIEWS-Copy" transform="translate(280.000000, 107.000000)">
                                                    <g id="Group-2" transform="translate(0.000000, 44.000000)">
                                                        <path d="M179.677409,122 C179.329599,122.000033 178.98366,122.134536 178.737721,122.366008 C178.491782,122.59748 178.348872,122.92307 178.348837,123.25042 L178.348837,138.672269 C178.348872,138.999621 178.491782,139.325209 178.737721,139.556683 C178.98366,139.788157 179.329599,139.922656 179.677409,139.922689 L186.320266,139.922689 C186.668076,139.922656 187.014015,139.788157 187.259953,139.556683 C187.505892,139.325209 187.648802,138.999621 187.648837,138.672269 L187.648837,123.25042 C187.648802,122.92307 187.505892,122.59748 187.259953,122.366008 C187.014015,122.134536 186.668076,122.000033 186.320266,122 L179.677409,122 Z M182.302721,122.833613 C182.30595,122.833547 182.30918,122.833547 182.312409,122.833613 C182.319785,122.833266 182.327175,122.833266 182.334552,122.833613 L183.663123,122.833613 C183.742978,122.832551 183.817255,122.872038 183.857512,122.936955 C183.89777,123.001872 183.89777,123.082161 183.857512,123.147079 C183.817255,123.211996 183.742978,123.251483 183.663123,123.25042 L182.334552,123.25042 C182.21226,123.258693 182.105998,123.172094 182.097208,123.056996 C182.088418,122.941898 182.18043,122.841886 182.302721,122.833613 L182.302721,122.833613 Z M178.791694,124.084034 L187.20598,124.084034 L187.20598,137.630252 L178.791694,137.630252 L178.791694,124.084034 Z M182.998837,138.047059 C183.365712,138.047059 183.663123,138.326975 183.663123,138.672269 C183.663123,139.017564 183.365712,139.297479 182.998837,139.297479 C182.631962,139.297479 182.334552,139.017564 182.334552,138.672269 C182.334552,138.326975 182.631962,138.047059 182.998837,138.047059 Z"
                                                              id="rect3010"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>&nbsp;Телефон: <span class="green">+373 (68) 407 773</span></p>
                                <p class="viber comunication">
                                    <svg class="imgCumunication" width="14px" height="14px" viewBox="0 0 14 14">
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="+UI4.-Детали-сделки" transform="translate(-457.000000, -301.000000)"
                                               fill="#7B519D">
                                                <g id="REVIEWS-Copy" transform="translate(280.000000, 107.000000)">
                                                    <g id="Group-2" transform="translate(0.000000, 44.000000)">
                                                        <g id="g15" transform="translate(177.000000, 150.000000)">
                                                            <path d="M12.859092,10.034413 C12.4267485,9.69474206 11.9646965,9.38984214 11.5099042,9.07835984 C10.6028427,8.45665143 9.77321886,8.40836388 9.0963595,9.40858434 C8.71627615,9.97014688 8.18420065,9.99471775 7.62787436,9.7483558 C6.09410262,9.06971733 4.90977896,8.02397292 4.21603159,6.50263891 C3.90916397,5.82957787 3.91318002,5.22610918 4.63122966,4.75006736 C5.0113645,4.49832896 5.39438266,4.2004134 5.36374739,3.65030723 C5.32374132,2.93302866 3.53917224,0.535936472 2.83445796,0.282941897 C2.54283074,0.178226764 2.25243923,0.185010134 1.95576621,0.282338931 C0.303263471,0.824656795 -0.381164597,2.15093123 0.274327296,3.7213065 C2.22983606,8.40645449 5.67128435,11.6680997 10.408579,13.658843 C10.6786328,13.7722006 10.978704,13.8175236 11.1306447,13.8581735 C12.2091608,13.8687757 13.4726213,12.854687 13.8375157,11.8482861 C14.1888174,10.8800731 13.4463625,10.4957324 12.859092,10.034413 L12.859092,10.034413 Z"
                                                                  id="path17-6"></path>
                                                            <path d="M7.54739885,0.763254735 C11.010678,1.28301161 12.6078828,2.89011757 13.0489277,6.29426384 C13.0898091,6.60871072 12.9697909,7.08183821 13.4294745,7.09058122 C13.9099077,7.09952522 13.794163,6.63338209 13.798179,6.31863372 C13.8388029,3.11256185 10.9742761,0.140390634 7.64754273,0.0308015242 C7.39653943,0.0660248007 6.87718143,-0.138129511 6.84479556,0.410871229 C6.82322215,0.781042239 7.26045702,0.720192898 7.54739885,0.763254735 L7.54739885,0.763254735 Z"
                                                                  id="path19-1"></path>
                                                            <path d="M8.22482457,1.69664644 C7.89169814,1.65750388 7.45194036,1.50430021 7.37409072,1.95541944 C7.29284289,2.42884841 7.78295579,2.38076186 8.097907,2.44955025 C10.2369185,2.91609536 10.9816903,3.67588304 11.3338673,5.74651928 C11.3853036,6.04840436 11.2831002,6.51826578 11.8087912,6.44038265 C12.1983998,6.38249789 12.0576835,5.97971646 12.0902753,5.74445914 C12.1075238,3.75652977 10.3619825,1.94707841 8.22482457,1.69664644 L8.22482457,1.69664644 Z"
                                                                  id="path21-2"></path>
                                                            <path d="M8.42125074,3.32224336 C8.19887469,3.32761981 7.98061766,3.35108524 7.89890644,3.582976 C7.77615939,3.92958108 8.03437099,4.0122377 8.29721649,4.05344039 C9.17467253,4.19091669 9.63646711,4.69585064 9.72409944,5.5497513 C9.74778386,5.78154156 9.89848881,5.96931529 10.1279187,5.94288527 C10.4458047,5.90590335 10.4745349,5.62974482 10.4648037,5.36750476 C10.4805075,4.40813526 9.3693996,3.29928039 8.42125074,3.32224336 L8.42125074,3.32224336 Z"
                                                                  id="path23"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>&nbsp;Viber: <span class="green">+373 (68) 407 773</span></p>
                                <p class="telegram comunication">
                                    <svg class="imgCumunication" width="15px" height="12px" viewBox="0 0 15 12">
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="+UI4.-Детали-сделки" transform="translate(-456.000000, -327.000000)"
                                               fill-rule="nonzero" fill="#64A9DC">
                                                <g id="REVIEWS-Copy" transform="translate(280.000000, 107.000000)">
                                                    <g id="Group-2" transform="translate(0.000000, 44.000000)">
                                                        <g id="1486933128_telegram"
                                                           transform="translate(176.000000, 176.000000)">
                                                            <g id="Layer_1">
                                                                <g id="g3885" transform="translate(0.007888, 0.000000)">
                                                                    <path d="M13.6455466,0.185242242 L1.24710646,4.98602595 C0.400892965,5.32686906 0.40591144,5.8006935 1.0928647,6.01197545 L4.18507312,6.98088675 L5.36822462,10.622508 C5.51206111,11.0210826 5.44115592,11.179163 5.85807025,11.179163 C6.17981465,11.179163 6.32254639,11.031899 6.50174341,10.8561618 C6.61569812,10.7442118 7.29229118,10.0838336 8.0477558,9.34645041 L11.2642326,11.7322127 C11.8561076,12.0600206 12.2834734,11.8902001 12.4308552,11.1803647 L14.5422226,1.1919172 C14.7583918,0.321875539 14.211822,-0.0727703203 13.6455466,0.185242242 Z M4.67036023,6.75828053 L11.6402151,2.34377625 C11.9881573,2.13193978 12.307185,2.24583147 12.0452496,2.4792538 L6.07723179,7.88494811 L5.84485625,10.3730928 L4.67036023,6.75828053 L4.67036023,6.75828053 Z"
                                                                          id="path9"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>&nbsp;Telegram: <span class="green">+373 (68) 407 773</span></p>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12 text-right">
                        <a href="/profile" class="btn btn-block showProf ">Посмотреть профиль</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row aboutProductDeal">
            <div class="col-xs-12">
                <div class="media">
                    <div class="media-left hidden-xs">
                        <a href="#">
                            <img src="/img/Bitmap.jpg" alt="...">
                        </a>

                    </div>
                    <div class="media-body ">

                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-9 col-xs-12">
                                    <h4 class="media-heading">Длинное название товара, длинное название товара и не
                                        только</h4>
                                    <p class="status">
                                        <svg width="19px" height="18px" viewBox="0 0 19 18">
                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                               fill-rule="evenodd">
                                                <g id="+UI2.-Главная" transform="translate(-210.000000, -3421.000000)"
                                                   fill-rule="nonzero">
                                                    <g id="Group-11" transform="translate(0.000000, 579.000000)">
                                                        <g id="Group-7" transform="translate(0.000000, 2726.000000)">
                                                            <g id="Group-6"
                                                               transform="translate(210.000000, 58.000000)">
                                                                <g id="Group-3">
                                                                    <g id="Group-Copy-4"
                                                                       transform="translate(0.000000, 58.000000)">
                                                                        <path d="M9.4,1.4 C13.7,1.4 17.2,4.9 17.2,9.2 C17.2,13.5 13.7,17 9.4,17 C5.1,17 1.6,13.5 1.6,9.2 C1.6,4.9 5.1,1.4 9.4,1.4 L9.4,1.4 Z M9.4,0.4 C4.6,0.4 0.6,4.3 0.6,9.2 C0.6,14.1 4.5,18 9.4,18 C14.3,18 18.2,14.1 18.2,9.2 C18.2,4.3 14.2,0.4 9.4,0.4 L9.4,0.4 Z"
                                                                              id="Shape" fill="#afafaf"></path>
                                                                        <path d="M8.9,11.6 C8.8,11.6 8.6,11.5 8.5,11.4 L6.4,9.1 C6.2,8.9 6.2,8.6 6.4,8.4 C6.6,8.2 6.9,8.2 7.1,8.4 L8.9,10.3 L12.1,6.5 C12.3,6.3 12.6,6.3 12.8,6.4 C13,6.6 13,6.9 12.9,7.1 L9.3,11.3 C9.2,11.5 9.1,11.6 8.9,11.6 L8.9,11.6 Z"
                                                                              id="Shape" fill="#4CDB58"></path>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>&nbsp;Прошло проверку
                                    </p>
                                </div>
                                <div class="col-xs-12 hidden-sm hidden-md hidden-lg  text-center imgProdMob">
                                    <img src="/img/Bitmap.jpg" class="img-responsive" alt="">
                                </div>

                                <div class="col-sm-3 col-xs-12 text-right showProductBtn">
                                    <a href="#" class="btn  btn-block">Карточка товара</a>
                                </div>

                                <div class="clearfix"></div>

                                <div class="col-sm-7 col-xs-12 text-left detail">
                                    <h5>Детали:</h5>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr>
                                                <td class="grey">Начало сделки:</td>
                                                <td class="leftText">01.01.2017 <span class="grey">10:00</span></td>
                                            </tr>
                                            <tr>
                                                <td class="grey">Завершение сделки:</td>
                                                <td class="leftText">01.01.2017 <span class="grey">10:00</span></td>
                                            </tr>
                                            <tr class="hidden-xs">
                                                <td></td>
                                                <td><a href="#" class="btn">Изменение срока аренды</a></td>
                                            </tr>
                                            <tr>
                                                <td class="grey">Доставка:</td>
                                                <td class="red leftText">Без доставки</td>
                                            </tr>
                                            <tr>
                                                <td class="grey">Адрес:</td>
                                                <td class="leftText"><a href="#" class="btn hidden-sm hidden-md hidden-lg pointerMaps">
                                                        <svg width="16px" height="16px" viewBox="0 0 16 16">
                                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <g class="re" transform="translate(-205.000000, -1007.000000)" fill-rule="nonzero">
                                                                    <g id="KACCA" transform="translate(0.000000, 140.000000)">
                                                                        <g id="Group">
                                                                            <g id="Group-5" transform="translate(20.000000, 778.000000)">
                                                                                <g id="Group-22" transform="translate(175.000000, 79.000000)">
                                                                                    <g id="Group-41" transform="translate(10.000000, 10.000000)">
                                                                                        <path d="M14.724561,7.02456123 L13.6438593,7.02456123 C13.3736839,4.18771919 11.0771927,2.02631574 8.37543839,1.75614031 L8.37543839,0.67543858 C8.37543839,0.270175432 8.10526296,8.8817842e-16 7.69999981,8.8817842e-16 C7.29473666,8.8817842e-16 7.02456123,0.270175432 7.02456123,0.67543858 L7.02456123,1.75614031 C4.18771919,2.02631574 2.02631574,4.32280691 1.75614031,7.02456123 L0.67543858,7.02456123 C0.270175432,7.02456123 0,7.29473666 0,7.69999981 C0,8.10526296 0.270175432,8.37543839 0.67543858,8.37543839 L1.75614031,8.37543839 C2.02631574,11.2122804 4.32280691,13.3736839 7.02456123,13.6438593 L7.02456123,14.724561 C7.02456123,15.1298242 7.29473666,15.3999996 7.69999981,15.3999996 C8.10526296,15.3999996 8.37543839,15.1298242 8.37543839,14.724561 L8.37543839,13.6438593 C11.2122804,13.3736839 13.3736839,11.0771927 13.6438593,8.37543839 L14.724561,8.37543839 C15.1298242,8.37543839 15.3999996,8.10526296 15.3999996,7.69999981 C15.3999996,7.29473666 15.1298242,7.02456123 14.724561,7.02456123 Z M8.37543839,12.2929822 L8.37543839,11.3473681 C8.37543839,10.942105 8.10526296,10.6719296 7.69999981,10.6719296 C7.29473666,10.6719296 7.02456123,10.942105 7.02456123,11.3473681 L7.02456123,12.2929822 C4.99824549,12.0228067 3.3771929,10.4017541 3.10701747,8.37543839 L4.05263148,8.37543839 C4.45789463,8.37543839 4.72807006,8.10526296 4.72807006,7.69999981 C4.72807006,7.29473666 4.45789463,7.02456123 4.05263148,7.02456123 L3.10701747,7.02456123 C3.3771929,4.99824549 4.99824549,3.3771929 7.02456123,3.10701747 L7.02456123,4.05263148 C7.02456123,4.45789463 7.29473666,4.72807006 7.69999981,4.72807006 C8.10526296,4.72807006 8.37543839,4.45789463 8.37543839,4.05263148 L8.37543839,3.10701747 C10.4017541,3.3771929 12.0228067,4.99824549 12.2929822,7.02456123 L11.3473681,7.02456123 C10.942105,7.02456123 10.6719296,7.29473666 10.6719296,7.69999981 C10.6719296,8.10526296 10.942105,8.37543839 11.3473681,8.37543839 L12.2929822,8.37543839 C12.0228067,10.4017541 10.4017541,12.0228067 8.37543839,12.2929822 Z" id="Shape"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg></a>2641
                                                    Tunlaw Road, N.W.<br> Washington, D.C. 20007
                                                </td>
                                            </tr>
                                            <tr class="hidden-xs">
                                                <td></td>
                                                <td class="leftText"><a href="#" class="btn"><svg width="16px" height="16px" viewBox="0 0 16 16">
                                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <g class="re" transform="translate(-205.000000, -1007.000000)" fill-rule="nonzero" >
                                                                    <g id="KACCA" transform="translate(0.000000, 140.000000)">
                                                                        <g id="Group">
                                                                            <g id="Group-5" transform="translate(20.000000, 778.000000)">
                                                                                <g id="Group-22" transform="translate(175.000000, 79.000000)">
                                                                                    <g id="Group-41" transform="translate(10.000000, 10.000000)">
                                                                                        <path d="M14.724561,7.02456123 L13.6438593,7.02456123 C13.3736839,4.18771919 11.0771927,2.02631574 8.37543839,1.75614031 L8.37543839,0.67543858 C8.37543839,0.270175432 8.10526296,8.8817842e-16 7.69999981,8.8817842e-16 C7.29473666,8.8817842e-16 7.02456123,0.270175432 7.02456123,0.67543858 L7.02456123,1.75614031 C4.18771919,2.02631574 2.02631574,4.32280691 1.75614031,7.02456123 L0.67543858,7.02456123 C0.270175432,7.02456123 0,7.29473666 0,7.69999981 C0,8.10526296 0.270175432,8.37543839 0.67543858,8.37543839 L1.75614031,8.37543839 C2.02631574,11.2122804 4.32280691,13.3736839 7.02456123,13.6438593 L7.02456123,14.724561 C7.02456123,15.1298242 7.29473666,15.3999996 7.69999981,15.3999996 C8.10526296,15.3999996 8.37543839,15.1298242 8.37543839,14.724561 L8.37543839,13.6438593 C11.2122804,13.3736839 13.3736839,11.0771927 13.6438593,8.37543839 L14.724561,8.37543839 C15.1298242,8.37543839 15.3999996,8.10526296 15.3999996,7.69999981 C15.3999996,7.29473666 15.1298242,7.02456123 14.724561,7.02456123 Z M8.37543839,12.2929822 L8.37543839,11.3473681 C8.37543839,10.942105 8.10526296,10.6719296 7.69999981,10.6719296 C7.29473666,10.6719296 7.02456123,10.942105 7.02456123,11.3473681 L7.02456123,12.2929822 C4.99824549,12.0228067 3.3771929,10.4017541 3.10701747,8.37543839 L4.05263148,8.37543839 C4.45789463,8.37543839 4.72807006,8.10526296 4.72807006,7.69999981 C4.72807006,7.29473666 4.45789463,7.02456123 4.05263148,7.02456123 L3.10701747,7.02456123 C3.3771929,4.99824549 4.99824549,3.3771929 7.02456123,3.10701747 L7.02456123,4.05263148 C7.02456123,4.45789463 7.29473666,4.72807006 7.69999981,4.72807006 C8.10526296,4.72807006 8.37543839,4.45789463 8.37543839,4.05263148 L8.37543839,3.10701747 C10.4017541,3.3771929 12.0228067,4.99824549 12.2929822,7.02456123 L11.3473681,7.02456123 C10.942105,7.02456123 10.6719296,7.29473666 10.6719296,7.69999981 C10.6719296,8.10526296 10.942105,8.37543839 11.3473681,8.37543839 L12.2929822,8.37543839 C12.0228067,10.4017541 10.4017541,12.0228067 8.37543839,12.2929822 Z" id="Shape"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>&nbsp;Показать на карте</a></td>
                                            </tr>
                                        </table>
                                    </div>

                                    <a href="#" class="btn changeRent btn-block hidden-sm hidden-lg hidden-md">Изменение срока аренды</a>

                                </div>
                                <div class="col-sm-3 col-sm-offset-2 col-xs-12 text-right priceCalc">
                                    <h5>Расчет стоимости:</h5>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr>
                                                <td class="grey">Ставка:</td>
                                                <td>...</td>
                                            </tr>
                                            <tr>
                                                <td class="grey">Экстра опции:</td>
                                                <td>нет</td>
                                            </tr>
                                            <tr>
                                                <td class="grey">Сумма:</td>
                                                <td class="green">$100</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <a href="#" class="btn btn-block showCheck " type="button" data-toggle="modal" data-target="#receipt">Посмотреть квитанцию</a>
                                </div>
                            </div>


                        </div>


                    </div>
                </div>
            </div>

        </div>

        <div class="row support">
            <div class="col-xs-12 text-center">
                <div class="supportImg text-center">
                    <svg width="187px" height="156px" viewBox="0 0 187 156" >
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                            <g id="+UI4.-Детали-сделки" transform="translate(-696.000000, -836.000000)">
                                <g id="REVIEWS-Copy" transform="translate(280.000000, 107.000000)">
                                    <g id="Group-4" transform="translate(0.000000, 700.000000)">
                                        <g id="support---illustration" transform="translate(417.000000, 29.000000)">
                                            <path d="M134,149 L39,149 L39,95.1 C39,93.4 40.3,92.1 42,92.1 L131,92.1 C132.7,92.1 134,93.4 134,95.1 L134,149 Z" id="Shape" stroke="#A0A0A0"></path>
                                            <path d="M93.4,133.5 C89.1,135.8 83.9,135.8 79.6,133.5" id="Shape" stroke="#FC988D"></path>
                                            <path d="M99.4,113.6 C101.8,117.9 101.8,123.2 99.4,127.6" id="Shape" stroke="#FC988D"></path>
                                            <path d="M79.5,107.7 C83.9,105.3 89.2,105.3 93.5,107.7" id="Shape" stroke="#FC988D"></path>
                                            <path d="M73.6,127.5 C71.2,123.1 71.3,117.8 73.7,113.5" id="Shape" stroke="#FC988D"></path>
                                            <path d="M139.7,139 L139.7,145.7 C139.7,151 134.2,155.4 128.8,155.4 L43,155.4 C37.7,155.4 33.3,151.1 33.3,145.7 L33.3,139" id="Shape" stroke="#A0A0A0"></path>
                                            <path d="M21.7,155.3 L0.2,155.3 L0.2,152.6 C0.2,147.4 4.4,143.3 9.5,143.3 L12.4,143.3 C17.6,143.3 21.7,147.5 21.7,152.6 L21.7,155.3 Z" id="Shape" stroke="#A0A0A0"></path>
                                            <path d="M176.5,5.3 L143,5.3 C138,5.3 134,9.3 134,14.3 L134,35.2 C134,40.2 138,44.2 143,44.2 L145.4,44.2 L143.3,52.9 L152.9,44.2 L176.5,44.2 C181.5,44.2 185.5,40.2 185.5,35.2 L185.5,14.3 C185.5,9.3 181.5,5.3 176.5,5.3 Z" id="Shape" stroke="#4DC97B"></path>
                                            <path d="M78.1,117.6 L73.4,112.9 C72.9,112.4 72.9,111.5 73.4,110.9 L76.8,107.5 C77.3,107 78.2,107 78.8,107.5 L83.5,112.2" id="Shape" stroke="#4B97E2"></path>
                                            <path d="M94.8,123.4 L99.6,128.2 C100.1,128.7 100.1,129.6 99.6,130.2 L96.2,133.6 C95.7,134.1 94.8,134.1 94.2,133.6 L89.4,128.8" id="Shape" stroke="#4B97E2"></path>
                                            <path d="M89.5,112.2 L94.2,107.5 C94.7,107 95.6,107 96.2,107.5 L99.6,110.9 C100.1,111.4 100.1,112.3 99.6,112.9 L94.9,117.6" id="Shape" stroke="#4B97E2"></path>
                                            <path d="M83.5,128.9 L78.7,133.7 C78.2,134.2 77.3,134.2 76.7,133.7 L73.3,130.3 C72.8,129.8 72.8,128.9 73.3,128.3 L78.1,123.5" id="Shape" stroke="#4B97E2"></path>
                                            <g id="Group-8" transform="translate(47.000000, 0.000000)" stroke="#A0A0A0">
                                                <path d="M52.2,60.5 L56.9,60.6 C68.7,60.6 78.2,70.1 78.2,81.9 L78.2,92" id="Shape"></path>
                                                <path d="M0.8,92 L0.8,81.9 C0.8,70.1 10.3,60.6 22.1,60.6 L26.8,60.5" id="Shape"></path>
                                                <path d="M25.3,52.7 C21.9,47.4 20,40.6 20,34.6 C20,22.6 21.1,5.3 39.5,5.3 C57.9,5.3 59,22.6 59,34.6 C59,46.6 51.5,61.7 39.5,61.7 C34.8,61.7 30.8,59.4 27.7,55.9" id="Shape"></path>
                                                <path d="M14.5,92.1 L14.5,83.1" id="Shape"></path>
                                                <path d="M64.5,92.1 L64.5,83.1" id="Shape"></path>
                                                <path d="M17.3,25.1 L17.3,23.1 C17.3,10.8 27.2,0.9 39.5,0.9 C51.8,0.9 61.7,10.8 61.7,23.1 L61.7,25.4" id="Shape"></path>
                                                <path d="M17.3,39.5 L17.3,42" id="Shape"></path>
                                                <path d="M17.3,41.6 C17.3,48.2 22.7,53.6 29.3,53.6 L34,53.6" id="Shape"></path>
                                                <path d="M20.5,40 L20.5,40 C16.6,40 13.4,36.8 13.4,32.9 L13.4,31.6 C13.4,27.7 16.6,24.5 20.5,24.5 L20.5,24.5" id="Shape"></path>
                                                <path d="M58.5,40 L58.5,40 C62.4,40 65.6,36.8 65.6,32.9 L65.6,31.6 C65.6,27.7 62.4,24.5 58.5,24.5 L58.5,24.5" id="Shape"></path>
                                                <polygon id="Shape" points="39.5 66.3 34.1 74.3 26.8 60.5"></polygon>
                                                <polygon id="Shape" points="39.5 66.3 44.9 74.3 52.2 60.5"></polygon>
                                                <path d="M47.9,85.6 L58.5,85.6" id="Shape"></path>
                                                <circle id="Oval" cx="36.7" cy="53.6" r="2.8"></circle>
                                            </g>
                                            <circle id="Oval" stroke="#FC988D" stroke-width="0.99999041" transform="translate(86.531410, 120.589810) rotate(-45.000000) translate(-86.531410, -120.589810) " cx="86.53141" cy="120.58981" r="8.49991848"></circle>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
                <p class="boldText">Возникли проблемы?</p>
                <p>Обратитесь в <a href="#" class="green">службу поддержки</a></p>
            </div>
        </div>
    </div>


</div>


<!-- Modal -->
<div class="modal fade" id="receipt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><svg viewBox="0 0 8 8" >
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
                            <g id="UI15.1----Отзывы-(личный-кабинет)---дефолт" transform="translate(-525.000000, -774.000000)" stroke="#FFFFFF" fill="#FFFFFF">
                                <g id="content" transform="translate(280.000000, 151.000000)">
                                    <g id="Group-22" transform="translate(20.000000, 89.000000)">
                                        <g id="Group-20">
                                            <g id="Group-19" transform="translate(0.000000, 204.000000)">
                                                <g id="Group-25" transform="translate(70.000000, 273.000000)">
                                                    <g id="close" transform="translate(151.000000, 53.000000)">
                                                        <g transform="translate(4.888889, 4.888889)" id="Combined-Shape">
                                                            <path d="M3.11111111,2.48257175 L0.832788199,0.204248838 C0.659221847,0.0306824861 0.37781519,0.0306824861 0.204248838,0.204248838 L0.204248838,0.832788199 L2.48257175,3.11111111 L0.151606014,5.44207685 C-0.0219603375,5.6156432 -0.0219603375,5.89704986 0.151606014,6.07061621 C0.325172366,6.24418256 0.606579024,6.24418256 0.780145375,6.07061621 L3.11111111,3.73965047 L5.44207685,6.07061621 C5.6156432,6.24418256 5.89704986,6.24418256 6.07061621,6.07061621 C6.24418256,5.89704986 6.24418256,5.6156432 6.07061621,5.44207685 L3.73965047,3.11111111 L6.01797338,0.832788199 C6.19153974,0.659221847 6.19153974,0.37781519 6.01797338,0.204248838 L5.38943402,0.204248838 L3.11111111,2.48257175 Z"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg></span>
                    </button>
                <h4 class="modal-title" id="idUser">ID контракта: 123456789</h4>
                <button type="button" class="close prevPage hidden-sm hidden-md hidden-lg"  data-dismiss="modal">← Предыдущая страница</button>
            </div>
            <div class="modal-body">
                <div class="row greyBackg">
                    <div class="col-sm-6 col-xs-12 aboutOwner">
                        <div class="media">
                            <div class="media-left">
                                <a class="" href="#">
                                    <img src="/img/account/foto.png" alt="...">
                                </a>
                            </div>

                            <div class="media-body">
                                <h5 class="media-heading">Ашот Равзанович Какашкин</h5>
                                <p>
                                    <svg width="10px" height="18px" viewBox="0 0 10 18" >
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="+UI4.-Детали-сделки" transform="translate(-458.000000, -273.000000)" fill-rule="nonzero" fill="#AFAFAF">
                                                <g id="REVIEWS-Copy" transform="translate(280.000000, 107.000000)">
                                                    <g id="Group-2" transform="translate(0.000000, 44.000000)">
                                                        <path d="M179.677409,122 C179.329599,122.000033 178.98366,122.134536 178.737721,122.366008 C178.491782,122.59748 178.348872,122.92307 178.348837,123.25042 L178.348837,138.672269 C178.348872,138.999621 178.491782,139.325209 178.737721,139.556683 C178.98366,139.788157 179.329599,139.922656 179.677409,139.922689 L186.320266,139.922689 C186.668076,139.922656 187.014015,139.788157 187.259953,139.556683 C187.505892,139.325209 187.648802,138.999621 187.648837,138.672269 L187.648837,123.25042 C187.648802,122.92307 187.505892,122.59748 187.259953,122.366008 C187.014015,122.134536 186.668076,122.000033 186.320266,122 L179.677409,122 Z M182.302721,122.833613 C182.30595,122.833547 182.30918,122.833547 182.312409,122.833613 C182.319785,122.833266 182.327175,122.833266 182.334552,122.833613 L183.663123,122.833613 C183.742978,122.832551 183.817255,122.872038 183.857512,122.936955 C183.89777,123.001872 183.89777,123.082161 183.857512,123.147079 C183.817255,123.211996 183.742978,123.251483 183.663123,123.25042 L182.334552,123.25042 C182.21226,123.258693 182.105998,123.172094 182.097208,123.056996 C182.088418,122.941898 182.18043,122.841886 182.302721,122.833613 L182.302721,122.833613 Z M178.791694,124.084034 L187.20598,124.084034 L187.20598,137.630252 L178.791694,137.630252 L178.791694,124.084034 Z M182.998837,138.047059 C183.365712,138.047059 183.663123,138.326975 183.663123,138.672269 C183.663123,139.017564 183.365712,139.297479 182.998837,139.297479 C182.631962,139.297479 182.334552,139.017564 182.334552,138.672269 C182.334552,138.326975 182.631962,138.047059 182.998837,138.047059 Z" id="rect3010"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>&nbsp;<span class="green tel">+373 (68) 407 773</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 dataRent">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <td class="grey">Начало сделки:</td>
                                    <td class="count">01.01.2017 <span class="grey"> 10-00</span></td>
                                </tr>
                                <tr>
                                    <td class="grey">Завершение сделки:</td>
                                    <td class="">01.01.2017 <span class="grey"> 10-00</span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-xs-12 product">
                        <h5>Товар</h5>
                        <div class="media">
                            <div class="media-left">
                                <a class="" href="#">
                                    <img src="/img/Bitmap.jpg" alt="...">
                                </a>
                            </div>

                            <div class="media-body">
                                <h4 class="media-heading">Длинное название товара, длинное название товара и не только</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 delivery">
                        <h5>Доставка / Самовывоз</h5>
                        <div class="media">
                            <div class="media-left">
                                <div class="maps"></div>
                            </div>

                            <div class="media-body">
                                <h4 class="media-heading grey">
                                    <svg width="13px" height="18px" viewBox="0 0 13 18" >
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="UI3.-История-сделок---вы-арендуете" transform="translate(-458.000000, -2208.000000)" fill-rule="nonzero" fill="#AFAFAF">
                                                <g id="REVIEWS-Copy" transform="translate(280.000000, 102.000000)">
                                                    <g id="Group-7" transform="translate(0.000000, 49.000000)">
                                                        <g id="Group-19-Copy-5" transform="translate(20.000000, 1998.000000)">
                                                            <g id="Group-12" transform="translate(149.000000, 0.000000)">
                                                                <g id="Group" transform="translate(9.000000, 57.000000)">
                                                                    <g id="Group-16" transform="translate(0.000000, 2.000000)">
                                                                        <path d="M6.5,17.5 L6.1,17.1 C5.9,16.8 0.4,10.6 0.4,6.4 C0.4,3 3.1,0.3 6.5,0.3 C9.9,0.3 12.6,3 12.6,6.4 C12.6,10.6 7.1,16.8 6.9,17.1 L6.5,17.5 Z M6.5,1.3 C3.7,1.3 1.4,3.6 1.4,6.4 C1.4,9.7 5.2,14.5 6.5,16 C7.7,14.5 11.6,9.7 11.6,6.4 C11.6,3.6 9.3,1.3 6.5,1.3 Z" id="Shape"></path>
                                                                        <path d="M6.5,9.6 C4.7,9.6 3.2,8.1 3.2,6.3 C3.2,4.5 4.7,3 6.5,3 C8.3,3 9.8,4.5 9.8,6.3 C9.8,8.1 8.3,9.6 6.5,9.6 Z M6.5,4 C5.2,4 4.2,5 4.2,6.3 C4.2,7.6 5.2,8.6 6.5,8.6 C7.8,8.6 8.8,7.6 8.8,6.3 C8.8,5.1 7.8,4 6.5,4 Z" id="Shape"></path>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>&nbsp;Адрес:</h4>
                                <p class="address">2641 Tunlaw Road, N.W. Washington, D.C. 20007</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4 col-xs-12 price">
                        <h5>Стоимость</h5>
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <td class="grey">Ставка:</td>
                                    <td>...</td>
                                </tr>
                                <tr>
                                    <td class="grey">Экстра опции:</td>
                                    <td>нет</td>
                                </tr>
                                <tr>
                                    <td class="grey">Сумма:</td>
                                    <td class="green">$300</td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div class="col-sm-6 col-sm-offset-2 col-xs-12 paycard">
                        <h5>Платеж</h5>
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <td class="grey">К оплате:</td>
                                    <td class="green">$100</td>
                                </tr>
                                <tr>
                                    <td class="grey">Статус:</td>
                                    <td class="green">
                                        <svg width="19px" height="18px" viewBox="0 0 19 18" >
                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <g id="+UI2.-Главная" transform="translate(-210.000000, -3421.000000)" fill-rule="nonzero">
                                                    <g id="Group-11" transform="translate(0.000000, 579.000000)">
                                                        <g id="Group-7" transform="translate(0.000000, 2726.000000)">
                                                            <g id="Group-6" transform="translate(210.000000, 58.000000)">
                                                                <g id="Group-3">
                                                                    <g id="Group-Copy-4" transform="translate(0.000000, 58.000000)">
                                                                        <path d="M9.4,1.4 C13.7,1.4 17.2,4.9 17.2,9.2 C17.2,13.5 13.7,17 9.4,17 C5.1,17 1.6,13.5 1.6,9.2 C1.6,4.9 5.1,1.4 9.4,1.4 L9.4,1.4 Z M9.4,0.4 C4.6,0.4 0.6,4.3 0.6,9.2 C0.6,14.1 4.5,18 9.4,18 C14.3,18 18.2,14.1 18.2,9.2 C18.2,4.3 14.2,0.4 9.4,0.4 L9.4,0.4 Z" id="Shape" fill="#4DC97B"></path>
                                                                        <path d="M8.9,11.6 C8.8,11.6 8.6,11.5 8.5,11.4 L6.4,9.1 C6.2,8.9 6.2,8.6 6.4,8.4 C6.6,8.2 6.9,8.2 7.1,8.4 L8.9,10.3 L12.1,6.5 C12.3,6.3 12.6,6.3 12.8,6.4 C13,6.6 13,6.9 12.9,7.1 L9.3,11.3 C9.2,11.5 9.1,11.6 8.9,11.6 L8.9,11.6 Z" id="Shape" fill="#4CDB58"></path>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg> &nbsp;Оплачено</td>
                                </tr>
                                <tr>
                                    <td class="img"><img src="" alt="Mastercard"></td>
                                    <td class="grey">5160 <span class="hiddenNumber">**** ****</span> 9876<br><span class="green">$200</span></td>
                                </tr>
                                <tr>
                                    <td class="img"><img src="" alt="Visa"></td>
                                    <td class="grey">5160 <span class="hiddenNumber">**** ****</span> 9876<br><span class="green">$200</span></td>
                                </tr>
                                <tr>
                                    <td class="grey">Дата оплаты:</td>
                                    <td class="dateChange">01.01.2017 <span class="grey">10-00</span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row greyBackg">
                    <div class="col-xs-12 canselRent">
                        <h5>Политики по отмене бронирования</h5>
                        <p>Уществует целый ряд различных факторов, оказывающих влияние на принятие решения покупателем. Одним из главных аспектов, влияющих на выбор пользователя, является описание товара, которое размещено на странице вашего интернет-магазина.</p>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <div class="row ">
                    <div class="col-xs-6 col-sm-4 text-left printing"><button type="button" class="btn bnt-default" >Распечатать</button>
                    </div>
                    <div class="col-xs-6 col-sm-8">
                        <button type="button" class="btn btn-default remove hidden-xs" data-dismiss="modal">Вернуться к деталям сделки</button>
                        <button type="button" class="btn mail btn-primary">
                            <svg width="24px" height="16px" viewBox="0 0 24 16" >
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="mail" transform="translate(-968.000000, -440.000000)" >
                                        <g id="SHARE" transform="translate(903.000000, 288.000000)">
                                            <g id="button-copy" transform="translate(20.000000, 142.000000)">
                                                <g id="button">
                                                    <g id="Group-25" transform="translate(45.000000, 8.000000)">
                                                        <path d="M1.23926278,3.20054155 C1.05648733,3.37344812 0.942399979,3.61876503 0.942399979,3.89125116 L0.942399979,15.1871485 C0.942399979,15.4601935 1.05469603,15.7063015 1.23747189,15.8794155 L1.23747189,15.8794155 L8.52871981,9.53919983 L1.23926278,3.20054155 L1.23926278,3.20054155 L1.23926278,3.20054155 L1.23926278,3.20054155 Z M22.3225276,3.19898425 C22.5053035,3.3720982 22.6175995,3.61820617 22.6175995,3.89125116 L22.6175995,15.1871485 C22.6175995,15.4596346 22.5035122,15.7049516 22.3207367,15.8778581 L15.0312797,9.53919983 L22.3225276,3.19898425 L22.3225276,3.19898425 L22.3225276,3.19898425 Z M14.326204,10.1523091 L21.2039995,16.1359997 L2.35599995,16.1359997 L9.23379548,10.1523091 L11.7799997,12.3663998 L14.326204,10.1523091 L14.326204,10.1523091 L14.326204,10.1523091 Z M1.88818775,2 C0.845370449,2 0,2.84298975 0,3.87920237 L0,15.1991973 C0,16.2370521 0.838512858,17.0783997 1.88818775,17.0783997 L21.6718117,17.0783997 C22.714629,17.0783997 23.5599995,16.2354099 23.5599995,15.1991973 L23.5599995,3.87920237 C23.5599995,2.84134756 22.7214866,2 21.6718117,2 L1.88818775,2 L1.88818775,2 L1.88818775,2 Z M11.7799997,11.1412913 L21.2039995,2.94239998 L2.35599995,2.94239998 L11.7799997,11.1412913 L11.7799997,11.1412913 L11.7799997,11.1412913 Z" id="mail-envelope-closed"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>&nbsp;Отправить на email</button>
                    </div>
                    <div class="col-xs-12 hidden-sm hidden-md hidden-lg">
                        <button type="button" class="btn remove btn-default btn-block" data-dismiss="modal">Вернуться к деталям сделки</button>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
