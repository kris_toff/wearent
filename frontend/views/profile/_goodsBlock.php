<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 07.08.2017
 * Time: 13:54
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/** @var \yii\web\View $this */
/** @var \common\models\Product $model */

?>

<div class="media">
    <div class="media-left hidden-xs">
        <?= Html::a(Html::img(ArrayHelper::getValue($model, 'media.0.link', '/img/category/imgCategory/defolt.svg')),
            "/{$model['slug']}"); ?>
    </div>
    <div class="media-body">
        <div class="row top">
            <div class="col-sm-8 col-xs-12">
                <h4 class="nameGoods"><?= Html::encode($model['name']); ?> </h4>
                <span class="category"><?= ArrayHelper::getValue($model, 'category.name', ''); ?></span>

                <div class="imgProduct hidden-sm hidden-md hidden-lg">
                    <?= Html::img(ArrayHelper::getValue($model, 'media.0.link',
                        '/img/category/imgCategory/defolt.svg')); ?>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12 text-right prise">
                <!--                <p class="forEra"><span class="green">$ 300</span>/сутки</p>-->
                <p class="forEra"><span class="green"><?= \frontend\models\ProductCurrency::formatPrice($model['price'],
                            $model['currency_id']); ?></span>/<?= \Yii::t('UI9.1',
                        '$PAYMENT_PERIOD_' . strtoupper($model->leasing) . '$'); ?></p>
                <p class="pladge">
                    <span class="green"><?= \Yii::$app->formatter->asCurrency($model['pledge_price'], $model['currency']['code']); ?></span>/<?= \Yii::t('UI6', '$ITEM_PLEDGE_LABEL$'); ?></p>
            </div>
        </div>

        <div class="row bottom">
            <div class="col-sm-6 col-xs-12">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td class="grey"><?= \Yii::t('UI6', '$ITEM_PROFIT_LABEL$'); ?>:</td>
                            <td><strong>???</strong> руб</td>
                        </tr>
                        <tr class="status">
                            <td class="grey"><?= \Yii::t('UI6', '$ITEM_STATUS_LABEL$'); ?>:</td>
                            <td>
                                <?php switch ($model['status']): ?>
<?php case \common\models\Product::PROCESSED: ?>
                                    <span class="audit blue"><?= \Yii::t('UI6', '$ITEM_STATUS_PROCESSED$'); ?></span>
                                    <?php break;
                                case \common\models\Product::OPEN: ?>
                                    <span class="checked green">
                                    <svg width="19px" height="18px" viewBox="0 0 19 18">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="+UI2.-Главная" transform="translate(-210.000000, -3421.000000)" fill-rule="nonzero">
            <g id="Group-11" transform="translate(0.000000, 579.000000)">
                <g id="Group-7" transform="translate(0.000000, 2726.000000)">
                    <g id="Group-6" transform="translate(210.000000, 58.000000)">
                        <g id="Group-3">
                            <g id="Group-Copy-4" transform="translate(0.000000, 58.000000)">
                                <path d="M9.4,1.4 C13.7,1.4 17.2,4.9 17.2,9.2 C17.2,13.5 13.7,17 9.4,17 C5.1,17 1.6,13.5 1.6,9.2 C1.6,4.9 5.1,1.4 9.4,1.4 L9.4,1.4 Z M9.4,0.4 C4.6,0.4 0.6,4.3 0.6,9.2 C0.6,14.1 4.5,18 9.4,18 C14.3,18 18.2,14.1 18.2,9.2 C18.2,4.3 14.2,0.4 9.4,0.4 L9.4,0.4 Z"
                                        id="Shape" fill="#90949B"></path>
                                <path d="M8.9,11.6 C8.8,11.6 8.6,11.5 8.5,11.4 L6.4,9.1 C6.2,8.9 6.2,8.6 6.4,8.4 C6.6,8.2 6.9,8.2 7.1,8.4 L8.9,10.3 L12.1,6.5 C12.3,6.3 12.6,6.3 12.8,6.4 C13,6.6 13,6.9 12.9,7.1 L9.3,11.3 C9.2,11.5 9.1,11.6 8.9,11.6 L8.9,11.6 Z"
                                        id="Shape" fill="#4CDB58"></path>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg> <?= \Yii::t('UI6', '$ITEM_STATUS_CONFIRMED$'); ?></span>
                                    <?php break;
                                case \common\models\Product::BLOCKED: ?>
                                    <span class="blocked red">
                                    <svg width="20px" height="20px" viewBox="0 0 20 20">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="UI14.2-------Дизайн-подсказок-по-ховеру" transform="translate(-689.000000, -4402.000000)"
                fill-rule="nonzero">
            <g id="Group-15-Copy-5" transform="translate(280.000000, 3102.000000)">
                <g id="1491442848_payment_method_card_visa" transform="translate(250.000000, 1291.000000)">
                    <g id="Group" transform="translate(160.000000, 10.000000)">
                        <path d="M8.8,0 C4,0 0,3.9 0,8.8 C0,13.7 3.9,17.6 8.8,17.6 C13.7,17.6 17.6,13.7 17.6,8.8 C17.6,3.9 13.6,0 8.8,0 L8.8,0 Z"
                                id="Shape" stroke="#AFAFAF"></path>
                        <path d="M9.65,8.95 L11.75,6.85 C11.95,6.65 11.95,6.35 11.75,6.15 C11.55,5.95 11.25,5.95 11.05,6.15 L8.95,8.25 L6.85,6.15 C6.65,5.95 6.35,5.95 6.15,6.15 C5.95,6.35 5.95,6.65 6.15,6.85 L8.25,8.95 L6.15,11.05 C5.95,11.25 5.95,11.55 6.15,11.75 C6.25,11.85 6.35,11.85 6.55,11.85 C6.75,11.85 6.85,11.85 6.95,11.75 L9.05,9.65 L11.15,11.75 C11.25,11.85 11.35,11.85 11.55,11.85 C11.75,11.85 11.85,11.85 11.95,11.75 C12.15,11.55 12.15,11.25 11.95,11.05 L9.65,8.95 Z"
                                id="Shape" fill="#E35F46"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg> <?= \Yii::t('UI6', '$ITEM_STATUS_BLOCKED$'); ?></span>
                                    <?php break;
                                default: ?>
                                    <span class="draft blue">
                                    <svg width="20px" height="17px" viewBox="0 0 20 17">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="+UI6.-Список-моих-товаров" transform="translate(-583.000000, -402.000000)" stroke="#AFAFAF">
            <g id="Group-7" transform="translate(280.000000, 151.000000)">
                <g id="Group-19-Copy" transform="translate(0.000000, 132.000000)">
                    <g id="Group-12">
                        <g id="draft_icon" transform="translate(303.000000, 119.000000)">
                            <path d="M0.5,6.00247329 C0.5,5.834831 0.46125679,5.85706529 0.602118602,5.94103612 L8.9508904,10.9179229 C9.58691139,11.297069 10.5462598,11.2934939 11.1878183,10.9060383 L19.3942015,5.94996741 C19.5367449,5.86388137 19.5,5.84324881 19.5,6.00247329 L19.5,15.9975267 C19.5,16.2714577 19.2721171,16.5 19.0081969,16.5 L0.99180311,16.5 C0.724293522,16.5 0.5,16.2722096 0.5,15.9975267 L0.5,6.00247329 Z"
                                    id="Rectangle-2"></path>
                            <path d="M0.597071571,5.8175801 L9.2591363,1.10096476 C9.74550642,0.836129423 10.5287105,0.838094281 11.0173125,1.1102784 L19.4674645,5.8175801"
                                    id="Path-2"></path>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg> <?= \Yii::t('UI6', '$ITEM_STATUS_DRAFT$'); ?></span>
                                <?php endswitch; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="grey">Заполнено на:</td>
                            <td><strong>??</strong>%</td>
                        </tr>
                    </table>

                    <?php if ((integer)$model['status']): ?>
                        <?= Html::a(\Yii::t('UI6', '$ITEM_PUBLISH_BTN$'), '#', ['class' => 'btn greenBtn']); ?>
                    <?php else: ?>
                        <?= Html::a(\Yii::t('UI6', '$ITEM_HIDE_BTN$'), '#', ['class' => 'btn redBtn']); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-sm-6 blockBtn col-xs-12 text-right">
                <a href="<?= "/red/{$model['slug']}"; ?>" class="btn editBtn">
                    <svg width="18px" height="18px" viewBox="0 0 18 18">
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g class="re" transform="translate(-1088.000000, -291.000000)" fill-rule="nonzero">
                                <g id="content" transform="translate(280.000000, 151.000000)">
                                    <g id="Group-22" transform="translate(20.000000, 89.000000)">
                                        <g id="Group-20">
                                            <g id="Group-4">
                                                <g id="Group-24" transform="translate(770.000000, 26.000000)">
                                                    <g id="edit_icon" transform="translate(18.000000, 25.000000)">
                                                        <path d="M15.6,9.6 C15.3,9.6 15.1,9.8 15.1,10.1 L15.1,16.5 L1.7,16.5 L1.7,3.1 L8.2,3.1 C8.5,3.1 8.7,2.9 8.7,2.6 C8.7,2.3 8.5,2.1 8.2,2.1 L1.2,2.1 C0.9,2.1 0.7,2.3 0.7,2.6 L0.7,17 C0.7,17.3 0.9,17.5 1.2,17.5 L15.6,17.5 C15.9,17.5 16.1,17.3 16.1,17 L16.1,10.1 C16.1,9.9 15.9,9.6 15.6,9.6 Z"
                                                                id="Shape"></path>
                                                        <path d="M17.7,3.8 L14.5,0.6 C14.3,0.4 14,0.4 13.8,0.6 L5.4,9 C5.3,9.1 5.3,9.2 5.3,9.3 L4.7,13 C4.7,13.2 4.7,13.3 4.8,13.4 C4.9,13.5 5,13.5 5.2,13.5 L5.3,13.5 L9,12.9 C9.1,12.9 9.2,12.8 9.3,12.8 L17.7,4.4 C17.9,4.3 17.9,4 17.7,3.8 Z M8.7,12 L5.8,12.4 L6.2,9.5 L11.8,3.9 L14.2,6.3 L8.7,12 Z M15.1,5.7 L12.7,3.3 L14.3,1.7 L16.7,4.1 L15.1,5.7 Z"
                                                                id="Shape"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg> <?= \Yii::t('UI6', '$ITEM_EDIT_PRODUCT_BTN$'); ?></a><br>
                <a href="<?= "/{$model['slug']}"; ?>" class="btn showBtn" target="_blank">
                    <svg width="17px" height="17px" viewBox="0 0 17 17">
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g class="re" transform="translate(-42.000000, -1004.000000)" fill-rule="nonzero">
                                <g id="KACCA" transform="translate(0.000000, 323.000000)">
                                    <g id="Group">
                                        <g id="Group-13" transform="translate(22.000000, 600.000000)">
                                            <g id="button-copy-2" transform="translate(0.000000, 66.000000)">
                                                <g id="button">
                                                    <g id="Group-6" transform="translate(20.000000, 14.000000)">
                                                        <g id="Group-3" transform="translate(0.000000, 1.000000)">
                                                            <path d="M16.3,15.5 L14.2,13.4 C15.5,12 16.2,10.1 16.2,8.1 C16.2,3.6 12.6,0 8.1,0 C3.6,0 0,3.6 0,8.1 C0,12.6 3.6,16.2 8.1,16.2 C10.2,16.2 12,15.4 13.4,14.2 L15.5,16.3 C15.6,16.4 15.7,16.4 15.9,16.4 C16.1,16.4 16.2,16.4 16.3,16.3 C16.5,16.1 16.5,15.7 16.3,15.5 Z M1,8 C1,4.1 4.2,0.9 8.1,0.9 C12,0.9 15.2,4.1 15.2,8 C15.2,11.9 12,15.1 8.1,15.1 C4.2,15.1 1,12 1,8 Z"
                                                                    id="Shape"></path>
                                                            <path d="M9.5,11.2 L8.6,11.2 L8.6,7.4 C8.6,7.1 8.4,6.9 8.1,6.9 L6.8,6.9 C6.5,6.9 6.3,7.1 6.3,7.4 C6.3,7.7 6.5,7.9 6.8,7.9 L7.6,7.9 L7.6,11.2 L6.8,11.2 C6.5,11.2 6.3,11.4 6.3,11.7 C6.3,12 6.5,12.2 6.8,12.2 L9.4,12.2 C9.7,12.2 9.9,12 9.9,11.7 C9.9,11.4 9.7,11.2 9.5,11.2 Z"
                                                                    id="Shape"></path>
                                                            <circle id="Oval" cx="7.9" cy="4.4" r="1.1"></circle>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                    <?= \Yii::t('UI6', '$ITEM_SHOW_PRODUCT_BTN$'); ?></a><br>
                <a href="#" class="btn historyBtn">
                    <svg width="18px" height="19px" viewBox="0 0 18 19">
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g class="re" transform="translate(-35.000000, -250.000000)" fill-rule="nonzero">
                                <g id="MENU_LEFT" transform="translate(0.000000, 80.000000)">
                                    <g id="Group" transform="translate(34.000000, 124.000000)">
                                        <g id="Group-14" transform="translate(1.000000, 46.000000)">
                                            <path d="M14.9,15.1 C14.9,15.1 14.9,15 14.9,15.1 C14.9,15 14.9,15 14.8,14.9 C14.8,14.9 14.8,14.8 14.7,14.8 L14.7,14.8 L14.7,14.8 C14.7,14.8 14.6,14.8 14.6,14.7 L14.5,14.7 C14.5,14.7 14.5,14.7 14.4,14.7 L11.3,14.3 C11,14.3 10.8,14.5 10.7,14.7 C10.7,15 10.9,15.2 11.1,15.3 L13.1,15.6 C11.8,16.5 10.3,17.1 8.7,17.1 C6.7,17.1 4.9,16.3 3.5,14.9 C1.2,12.6 0.7,9 2.2,6.2 C2.3,6 2.2,5.7 2,5.5 C1.8,5.4 1.5,5.5 1.3,5.7 C-0.4,8.9 0.2,13 2.8,15.6 C4.4,17.2 6.5,18.1 8.7,18.1 C10.5,18.1 12.2,17.5 13.6,16.5 L13.4,18.3 C13.4,18.6 13.6,18.8 13.8,18.9 L13.9,18.9 C14.1,18.9 14.4,18.7 14.4,18.5 L14.8,15.4 C14.9,15.2 14.9,15.2 14.9,15.1 C14.9,15.2 14.9,15.1 14.9,15.1 Z"
                                                    id="Shape"></path>
                                            <path d="M14.4239157,3.28513728 C12.4572823,1.58422617 10.9300839,1.17481512 8.7300839,1.17481512 C6.9300839,1.17481512 5.3,1.8 3.9,2.8 L4.1,1 C4.1,0.7 3.9,0.5 3.7,0.4 C3.4,0.4 3.2,0.6 3.1,0.8 L2.7,3.9 L2.7,4 L2.7,4.1 L2.7,4.2 C2.7,4.2 2.7,4.3 2.8,4.3 L2.9,4.3 L3,4.3 L3.1,4.3 L6.2,4.7 L6.3,4.7 C6.5,4.7 6.8,4.5 6.8,4.3 C6.8,4 6.6,3.8 6.4,3.7 L4.4,3.4 C7.3,1.3 11.4,1.5 14,4.1 C16.3,6.4 16.8,10 15.3,12.8 C15.2,13 15.3,13.3 15.5,13.5 C15.6,13.5 15.7,13.6 15.7,13.6 C15.9,13.6 16.1,13.5 16.1,13.3 C17.9,10.3 17.3152539,5.80994282 14.4239157,3.28513728 Z"
                                                    id="Shape"></path>
                                            <path d="M8.8,6.5 C8.5,6.5 8.3,6.7 8.3,7 L8.3,10.3 C8.3,10.6 8.5,10.8 8.8,10.8 L11.2,10.8 C11.5,10.8 11.7,10.6 11.7,10.3 C11.7,10 11.5,9.8 11.2,9.8 L9.3,9.8 L9.3,7 C9.3,6.7 9,6.5 8.8,6.5 Z"
                                                    id="Shape"></path>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg> <?= \Yii::t('UI6', '$ITEM_HISTORY_PRODUCT_BTN$'); ?></a>
            </div>
        </div>
    </div>
</div>
