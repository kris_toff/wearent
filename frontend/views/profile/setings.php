<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 09.06.2017
 * Time: 16:41
 */
use yii\bootstrap\Html;

/** @var \yii\web\View $this */

\yii\bootstrap\BootstrapPluginAsset::register($this);
\frontend\assets\ReadmoreAsset::register($this);
\frontend\assets\SliderAsset::register($this);
\frontend\assets\ProductAsset::register($this);
\frontend\assets\yyAsset::register($this);

?>

<div class="marg">

</div>


<div class="row setings">
    <div class="container sidebarEditLeft">
        <div class="row">
            <div class="col-sm-12">
                <?= $this->render('left-menu') ?>
            </div>
        </div>
    </div>


    <div class="container content">
        <div class="row">
            <div class="col-sm-12">
                <div class="titleEdit">
                    <h4 class="text-left">Настройки аккаунта</h4>
                </div>


                <div class="wrapPanel">
                    <div role="tabpanel" class="setingsPanel">

                        <!-- Навігаційні елементи вкладок -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#masseg" aria-controls="home" role="tab" data-toggle="tab">Настройки уведомлений</a></li>
                            <li role="presentation"><a href="#secyrity" aria-controls="profile" role="tab" data-toggle="tab">Настройки безопасности</a></li>
                            <li role="presentation"><a href="#lost" aria-controls="messages" role="tab" data-toggle="tab">Прочее</a></li>

                        </ul>

                        <!-- Вкладки панелі -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="masseg">Настройки уведомлений</div>
                            <div role="tabpanel" class="tab-pane" id="secyrity">Настройки безопасности</div>
                            <div role="tabpanel" class="tab-pane" id="lost">Прочее</div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

