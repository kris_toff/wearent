<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 15.08.2017
 * Time: 14:12
 */

use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

/** @var \yii\web\View $this */
/** @var array $model */


if (!count($model['minislide'])) {
    $model['minislide'][] = [
        'src' => '/img/category/imgCategory/defolt.svg',
        'class' => ['defoltImg']
    ];
}

$countMiniSlides = count($model['minislide']);
$i = 1;

?>


<div class="col-sm-6 col-md-4 paddSlide">
    <div class="thumbnail pageProduct">
        <div class="slider1">

            <?php foreach ($model['minislide'] as $item): ?>
                <div class="sl">
                        <span class="glyphiconHeart">
                             <svg width="21px" height="19px" viewBox="0 0 19 18">
    <g id="Page-1" stroke="none" fill="none" fill-rule="evenodd">
        <g id="likeTop" transform="translate(-1235.000000, -523.000000)">
            <g id="content" transform="translate(280.000000, 151.000000)">
                <g id="Group-22" transform="translate(20.000000, 89.000000)">
                    <g id="Group-20">
                        <g id="Group-19" transform="translate(0.000000, 204.000000)">
                            <g id="like_icon-copy" transform="translate(935.000000, 79.000000)">
                                <path d="M9.84718182,17.6587043 L9.5,17.8116311 L9.15281818,17.6587043 C4.69040909,14.9064558 0,8.64166921 0,5.16214939 C0.0151136364,2.31301829 2.33354545,3.85870198e-16 5.18181818,3.85870198e-16 C6.98206818,3.85870198e-16 8.57115909,0.924512195 9.5,2.32387957 C10.4288409,0.924512195 12.0179318,0 13.8181818,0 C16.6664545,0 18.9848864,2.31301829 19,5.16214939 C19,8.64166921 14.3095909,14.9064558 9.84718182,17.6587043 L9.84718182,17.6587043 Z M13.8181818,0.864992378 C12.3698636,0.864992378 11.02475,1.58922256 10.2189773,2.80264482 L9.5,3.88573171 L8.78102273,2.80221037 C7.97525,1.58922256 6.63013636,0.864992378 5.18181818,0.864992378 C2.81372727,0.864992378 0.876159091,2.7948247 0.863636364,5.16214939 C0.863636364,8.2311128 5.28286364,14.2022104 9.5,16.8562729 C13.7171364,14.2022104 18.1363636,8.23154726 18.1363636,5.1664939 C18.1238409,2.7948247 16.1862727,0.864992378 13.8181818,0.864992378 L13.8181818,0.864992378 Z"
                                      id="Shape"></path>
                        </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                        </span>
                    <?= Html::a(Html::img($item['src'], ['alt' => '', 'class' => implode(' ', array_merge(['imgSl'], ArrayHelper::getValue($item, 'class', [])))]), ["/cargo/{$model['slug']}"]) ?>
                    <div class="textSl">
                        <p class="pSl">
                            <?= \Yii::t('frontend', 'Фото {index} з {count}', [
                                'index' => $i,
                                'count' => $countMiniSlides,
                            ]) ?>
                        </p>
                    </div>
                </div>

                <?php $i++; endforeach; ?>

        </div>

        <div class="caption">
            <?= Html::a(Html::tag('h3', Html::encode($model['product_name']), ['class' => 'longName']),
                ["/cargo/{$model['slug']}"]) ?>

            <?= Html::a(Html::encode($model['category']), ['/category/repel', 'fields' => $model['category']],
                ['class' => 'btn btnTrans', 'role' => 'button']) ?>

            <a href="#" hidden class="advertising">Рекламма</a>
            <table class="table-block table-responsive">
                <tr>
                    <td><?= \Yii::t('frontend', 'Цена за сутки') ?></td>
                    <td class="text-right">
                        <?= \Yii::$app->formatter->asCurrency($model['price'], $model['currency']) ?>
                    </td>
                </tr>
                <tr>
                    <td>Сумма залога</td>
                    <td class="text-right"><?= \Yii::$app->formatter->asCurrency($model['price'], $model['currency']) ?></td>
                </tr>
                <tr>
                    <td>Мин. срок аренды</td>
                    <td class="text-right"><?= "{$model['min_period']} {$model['step']}" ?> </td>
                </tr>
                <tr>
                    <td>Оценка товара</td>
                    <td class="text-right">
                        <div class="rateyoHome" data-rating="1"></div>
                    </td>
                </tr>

            </table>


            <div class="row margg">
                <div class="col-xs-12 rowBottom">
                    <div class="margLeft">
                        <span class="face">
                            <a href="#">
                                <img class="menFase" src="/img/category/man.png" alt="">
                            </a>
                        </span>
                        <span class="mail">
                            <img class="menFase" src="img/home/mail.svg" alt="">&nbsp; <?= $model['countComments'] ?>
                        </span>
                    </div>

                    <div hidden class="markProduct">
                        <span class="count">25</span>
                    </div>

                    <span class="leftRightBtn"></span>
                </div>
            </div>
        </div>
    </div>
</div>
