<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 26.07.2017
 * Time: 12:14
 */

/** @var \yii\web\View $this */

?>

<div class="media blockDeal">
    <a class="media-left imgProductRent hidden-xs" href="#">
        <img src="/img/Bitmap.jpg" alt="Bitmap.jpg">
    </a>
    <div class="media-body">
        <div class="row borderBottom">
            <div class="nameProduct col-sm-9 col-xs-12">
                <h4 class="media-heading">Длинное название товара, длинное название товара и не только</h4>
                <div class="statusRent">
                    <p class="rentOk">
                        <svg width="19px" height="18px" viewBox="0 0 19 18">
                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g id="+UI2.-Главная" transform="translate(-210.000000, -3481.000000)" fill-rule="nonzero">
                                    <g id="Group-11" transform="translate(0.000000, 579.000000)">
                                        <g id="Group-7" transform="translate(0.000000, 2726.000000)">
                                            <g id="Group-6" transform="translate(210.000000, 58.000000)">
                                                <g id="Group-3">
                                                    <g id="Group-Copy-6" transform="translate(0.000000, 118.000000)">
                                                        <path d="M9.4,1.4 C13.7,1.4 17.2,4.9 17.2,9.2 C17.2,13.5 13.7,17 9.4,17 C5.1,17 1.6,13.5 1.6,9.2 C1.6,4.9 5.1,1.4 9.4,1.4 L9.4,1.4 Z M9.4,0.4 C4.6,0.4 0.6,4.3 0.6,9.2 C0.6,14.1 4.5,18 9.4,18 C14.3,18 18.2,14.1 18.2,9.2 C18.2,4.3 14.2,0.4 9.4,0.4 L9.4,0.4 Z"
                                                              id="Shape" fill="#4DC97B"></path>
                                                        <path d="M8.9,11.6 C8.8,11.6 8.6,11.5 8.5,11.4 L6.4,9.1 C6.2,8.9 6.2,8.6 6.4,8.4 C6.6,8.2 6.9,8.2 7.1,8.4 L8.9,10.3 L12.1,6.5 C12.3,6.3 12.6,6.3 12.8,6.4 C13,6.6 13,6.9 12.9,7.1 L9.3,11.3 C9.2,11.5 9.1,11.6 8.9,11.6 L8.9,11.6 Z"
                                                              id="Shape" fill="#4CDB58"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        Идет аренда, вам передан товар
                    </p>
                    <p class="waitAnswer" style="display: none">
                        ! Вы запросил у вас разрешение на аренду, ожидание ответа
                    </p>
                    <p class="payRent" style="display: none">
                        @ Дмитрий Доба подтвердил сделку, оплатите аренду до <span class="countDate">03.12.1016</span>
                    </p>

                </div>
                    <p class="delivery">
                        <svg width="13px" height="18px">
                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g id="+UI5.-Квитанция-об-оплате" transform="translate(-973.000000, -382.000000)"
                                   fill-rule="nonzero" fill="#AFAFAF">
                                    <g id="Group-13" transform="translate(366.000000, 142.000000)">
                                        <g id="Group-11" transform="translate(20.000000, 208.000000)">
                                            <g id="Group" transform="translate(587.000000, 30.000000)">
                                                <g id="Group-16" transform="translate(0.000000, 2.000000)">
                                                    <path d="M6.5,17.5 L6.1,17.1 C5.9,16.8 0.4,10.6 0.4,6.4 C0.4,3 3.1,0.3 6.5,0.3 C9.9,0.3 12.6,3 12.6,6.4 C12.6,10.6 7.1,16.8 6.9,17.1 L6.5,17.5 Z M6.5,1.3 C3.7,1.3 1.4,3.6 1.4,6.4 C1.4,9.7 5.2,14.5 6.5,16 C7.7,14.5 11.6,9.7 11.6,6.4 C11.6,3.6 9.3,1.3 6.5,1.3 Z"
                                                          id="Shape"></path>
                                                    <path d="M6.5,9.6 C4.7,9.6 3.2,8.1 3.2,6.3 C3.2,4.5 4.7,3 6.5,3 C8.3,3 9.8,4.5 9.8,6.3 C9.8,8.1 8.3,9.6 6.5,9.6 Z M6.5,4 C5.2,4 4.2,5 4.2,6.3 C4.2,7.6 5.2,8.6 6.5,8.6 C7.8,8.6 8.8,7.6 8.8,6.3 C8.8,5.1 7.8,4 6.5,4 Z"
                                                          id="Shape"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>&nbsp;С доставкой:<span class="place"> г. Москва, ул Проектируемсяй проезд № 4158, д 12, корп. 111</span>
                    </p>
                </div>
                <div class="blockPrice col-sm-3 text-right hidden-xs">
                    <span class="price">$3 000.00</span><br>
                    <span class="pladge"><i class="grey">Залог:</i> $300</span>
                </div>
            </div>


            <div class="clearfix"></div>

            <div class="row borderBottom">
                <div class="media infAboutOwner hidden-xs col-sm-6">
                    <a class="media-left imgOwner" href="#">
                        <img src="/img/account/foto.png" alt="Зображення користувача">
                    </a>
                    <div class="media-body">
                        <span>Хозяин</span>
                        <h5 class="media-heading">Доба Дмитрий</h5>
                        <a href="#" class="btn startMassage hidden-lg hidden-md hidden-sm">Начатьfda;vkf переписку</a>
                    </div>

                </div>

                <div class="tableDateRent col-sm-6 col-xs-12 text-right">
                    <table class="table">
                        <tr>
                            <td class="grey">Начало аренды:</td>
                            <td class="right">01.03.2017 <span class="grey">10:00</span></td>
                        </tr>
                        <tr>
                            <td class="grey">Завершение аренды:</td>
                            <td class="right">01.03.2017 <span class="grey">10:00</span></td>
                        </tr>
                    </table>
                </div>

                <div class="imgProductMob text-center">
                    <img src="/img/Bitmap.jpg" class="img-responsive hidden-sm hidden-md hidden-lg" alt="Bitmap.jpg">
                </div>

                <div class="media infAboutOwner hidden-sm hidden-md hidden-lg col-xs-12">
                    <a class="media-left imgOwner" href="#">
                        <img src="/img/account/foto.png" alt="Зображення користувача">
                    </a>
                    <div class="media-body">
                        <span>Хозяин</span>
                        <h5 class="media-heading">Доба Дмитрий</h5>

                    </div>
                    <a href="#" class="btn startMassage hidden-lg hidden-md hidden-sm">Начать переписку</a>
                </div>

            </div>


            <div class="blockPrice col-xs-12 hidden-sm hidden-md hidden-lg">
                <span class="pladge"><i class="grey">Залог:</i> $300.00</span>
                <span class="price">$3 000.00</span>
            </div>

            <div class="row borderBottom">
                <div class="massage col-xs-12">
                    <h5>Ваше сообщение:</h5>
                    <p>Сергей Попков и Андрей Абрамов обсуждают одну из ключевых сторон работы дизайнера — презентацию
                        концепции. Kакой стратегии придерживаться, как правильно рассказать о целях проекта, стоит ли
                        углубляться в детали и какой инструмент для создания презентации выбрать. </p>
                </div>
            </div>


            <div class="blockBtn col-xs-12 text-right">
                <a href="#" class="btn red">Договор аренды</a>
                <a href="#" class="btn red datailDeal">Детали сделки</a>

                <a href="#" class="btn canselRequest hidden">Отменить запрос</a>
                <a href="#" class="btn changeDate hidden">Изменить дату аренды</a>

                <a href="#" class="btn red massageStart hidden-xs">Начать переписку</a>
            </div>


        </div>

    </div>
