<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.09.2017
 * Time: 10:38
 */

use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \common\models\UserProfile $model */
/** @var \yii\widgets\ActiveForm $form */
?>

    <div class="row contact allBlockContent">
        <div class="col-sm-12" ng-form="contactsForm">
            <div class="email styleInput">
                <h4 ng-init="confirm_email_title='<?= \Yii::t('UI14.4',
                    '$FIELD_EMAIL_CONFIRM_REMOVE_BOX_TITLE$') ?>';confirm_email_question='<?= \Yii::t('UI14.4',
                    '$FIELD_EMAIL_CONFIRM_REMOVE_BOX_QUESTION$') ?>';confirm_email_ok_btn='<?= \Yii::t('UI14.4',
                    '$FIELD_EMAIL_CONFIRM_REMOVE_BOX_OK_BTN$') ?>';confirm_email_cancel_btn='<?= \Yii::t('UI14.4',
                    '$FIELD_EMAIL_CONFIRM_REMOVE_BOX_CANCEL_BTN$') ?>';"><?= \Yii::t('UI14.4',
                        '$FIELDS_EMAIL_TITLE$') ?></h4>

                <email-field-edit class="emailForm"
                        ng-repeat="o in emails track by o.id"
                        model="o"
                        index="$index"
                        pef="contactsForm"
                        on-delete="removeEmail(idx)"
                        on-save="saveEmail(idx, model)"
                        on-make-login="makeLogin(idx)"
                ></email-field-edit>

                <div class="wrapperNewContact">
                    <a class="md-accent addNewContact" ng-click="addNewEmail()">+ <?= \Yii::t('UI14.4',
                            '$ADD_NEW_EMAIL_BTN$') ?></a>
                </div>
            </div>
            <div class="phone styleInput">
                <h4 ng-init="confirm_phone_title='<?= \Yii::t('UI14.4',
                    '$FIELD_PHONE_CONFIRM_REMOVE_BOX_TITLE$') ?>';confirm_phone_question='<?= \Yii::t('UI14.4',
                    '$FIELD_PHONE_CONFIRM_REMOVE_BOX_QUESTION$') ?>';confirm_phone_ok_btn='<?= \Yii::t('UI14.4',
                    '$FIELD_PHONE_CONFIRM_REMOVE_BOX_OK_BTN$') ?>';confirm_phone_cancel_btn='<?= \Yii::t('UI14.4',
                    '$FIELD_PHONE_CONFIRM_REMOVE_BOX_CANCEL_BTN$') ?>';"><?= \Yii::t('UI14.4',
                        '$FIELD_PHONE_TITLE$') ?></h4>

                <phone-field-edit class="phoneForm"
                        ng-repeat="o in phones track by o.id"
                        model="o"
                        index="$index"
                        pef="contactsForm"
                        count-public="amountPublic()"
                        on-delete="removePhone(idx)"
                        on-save="savePhone(idx, model)"
                        on-ftr="setTrustedPhone(id)"
                ></phone-field-edit>

                <div class="wrapperNewContact">
                    <a class="md-accent addNewContact" ng-click="addNewPhone()">+ <?= \Yii::t('UI14.4',
                            '$ADD_NEW_PHONE_BTN$') ?></a>
                </div>

            </div>
        </div>
        <div class="col-sm-12">
            <div class="styleInput userNameBlock">
                <?= $form->field($model->user, 'username', [
                    'enableAjaxValidation' => true,
                    'validateOnType'       => true,
                    'validationDelay'      => 1000,
                    'inputTemplate'        => '<div class="input-group"><span class="input-group-addon" ng-hide="isSupportCopy()">'
                        . \Yii::$app->request->hostInfo
                        . '/</span>{input}</div>',
                    'errorOptions'         => [
                        'encode' => false,
                    ],
                ])
                    ->textInput([
                        'placeholder' => \Yii::t('UI14.4', '$FIELD_USERNAME_PLACEHOLDER$'),
                        'data'        => [
                            'toggle'    => 'popover',
                            'content'   => \Yii::t('UI14.4', '$FIELD_USERNAME_HINT_POPOVER$'),
                            'placement' => 'top',
                            'html'      => '1',
                            'trigger'   => 'focus',
                        ],
                        'ng-model'    => 'profile.username',
                        'ng-value'    => 'profile.username',
                        'ng-init'     => "profile.username = '{$model->user->username}';",
                    ])
                    ->label(\Yii::t('UI14.4', '$FIELD_USERNAME_LABEL$')) ?>
            </div>
            <div ng-show="isSupportCopy()" ng-class="{done: hasCopied}">
                <span>{{_location.protocol + '//' + _location.host + '/'}}<strong>{{profile.username }}</strong></span>
                <md-button ng-click="copyUserLink($event)" ng-switch on="hasCopied">
                    <span ng-switch-when="false">copy</span>
                    <span ng-switch-when="true">done</span>
                </md-button>
            </div>
        </div>
        <div class="col-sm-12">
            <md-switch ng-model="isTest">Браузер <span ng-hide="isTest">не </span>поддерживает копирование (пример)
            </md-switch>
        </div>
    </div>


<?php
$this->registerCss(<<<CSS
email-field-edit,
phone-field-edit{
    display: block;
}
.input-box{
    position: relative;
}
.input-box .email_verify_icon{
    position: absolute;
    right: 2px;
    top: 3px;
    pointer-events: none;
}
.md-panel #newpass_panel_box{
    background-color: #fff;
}
.md-panel:not(._md-panel-fullscreen) #newpass_panel_box{
    max-width: 410px;
}
CSS
);