<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.09.2017
 * Time: 23:57
 */

use yii\bootstrap\Html;
use yii\helpers\Json;

use common\models\Country;

/** @var \yii\web\View $this */
/** @var \common\models\UserProfile $model */

$countries = Country::getList();

array_walk($countries, function(&$v) {
    $v['name'] = \Yii::t('UI14.3-countries', "\$COUNTRY_NAME_{$v['code']}$");
});
usort($countries, function($a, $b) {
    return strcasecmp($a['name'], $b['name']);
});

?>

    <div class="row addInf allBlockContent">
        <div class="col-sm-12">
            <div class="leave">
                <h4><?= \Yii::t('UI14.3', '$LOCATION_TITLE$') ?></h4>

                <div class="form-group <?= 'field-' . Html::getInputId($model, 'country') ?>">
                    <?= Html::activeLabel($model, 'country', [
                        'label' => \Yii::t('UI14.3', '$LOCATION_COUNTRY_LABEL$'),
                        'class' => 'control-label',
                    ]) ?>
                    <?= Html::activeHiddenInput($model, 'country', [
                        'ng-value' => 'loc_country',
                        'ng-init'  => 'loc_country = "'
                            . ($model['country'] ?: '')
                            . '";countries = '
                            . Json::encode($countries),
                    ]) ?>

                    <md-select class="md-no-underline"
                            ng-model="loc_country"
                            placeholder="<?= \Yii::t('UI14.3', '$FIELD_LOCATION_COUNTRY_PLACEHOLDER$') ?>" ng-cloak>
                        <md-option ng-value="n.id" ng-repeat="n in countries">{{n.name}}</md-option>
                    </md-select>

                    <?= Html::error($model, 'country', ['class' => 'error-block']) ?>
                </div>

                <div class="form-group <?= 'field-' . Html::getInputId($model, 'city') ?>">
                    <?= Html::activeLabel($model, 'city', [
                        'label' => \Yii::t('UI14.3', '$LOCATION_CITY_LABEL$'),
                        'class' => 'control-label',
                    ]) ?>

                    <?= Html::activeTextInput($model, 'city',
                        ['placeholder' => \Yii::t('UI14.3', '$FIELD_LOCATION_CITY_PLACEHOLDER$')]) ?>
                    <?= Html::error($model, 'city', ['class' => 'help-block']) ?>
                </div>
            </div>

            <div class="work">
                <h4><?= \Yii::t('UI14.3', '$WORK_TITLE$') ?></h4>
                <div class="form-group <?= 'field-' . Html::getInputId($model, 'company') ?>">
                    <?= Html::activeLabel($model, 'company', [
                        'label' => \Yii::t('UI14.3', '$COMPANY_NAME_LABEL$'),
                        'class' => 'control-label',
                    ]) ?>
                    <?= Html::activeTextInput($model, 'company', [
                        'placeholder' => \Yii::t('UI14.3', '$FIELD_COMPANY_NAME_PLACEHOLDER$'),
                    ]) ?>

                    <?= Html::error($model, 'company', ['class' => 'help-block']) ?>
                </div>
            </div>

            <div class="language">
                <h4><?= \Yii::t('UI14.3', '$LANGUAGE_TITLE$') ?></h4>

                <div class="form-group">
                    <?= Html::activeHiddenInput($model, 'language', [
                        'ng-value' => 'LanguageList.join(",")',
                        'ng-init'  => 'LanguageList = ' . Json::encode(array_filter(explode(',', $model->language))),
                    ]) ?>
                    <?= Html::activeLabel($model, 'langs', [
                        'label' => \Yii::t('UI14.3', '$FIELD_LANGUAGE_LABEL$'),
                        'class' => 'control-label',
                    ]) ?>

                    <div id="chips_languages" ng-cloak>
                        <md-chips ng-model="LanguageList">
                            <md-chip-template>
                                <span>{{languages[$chip]}}</span>
                            </md-chip-template>

                            <input type="text" disabled />
                        </md-chips>
                        <md-button ng-click="openLangBox()">+ <?= \Yii::t('UI14.3',
                                '$FIELD_LANGUAGE_OPEN_LIST_BOX_BTN$') ?></md-button>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php
$this->registerCss(<<<CSS
.md-panel-outer-wrapper{
    position: fixed;
}

#chips_languages .md-chip-input-container{
    display: none;
}

#language_box{
    background-color: #fff;
}
CSS
);
