<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 25.09.2017
 * Time: 0:42
 */

use yii\bootstrap\Html;

?>


<h4><?= \Yii::t('UI14.7', '$BLOCK_RECOMMENDATION_TITLE$') ?></h4>

<p><?= \Yii::t('UI14.7', '$BLOCK_RECOMMENDATION_TEXT$') ?></p>

<div class="row invite">
    <div class="col-sm-6"><span class="leftText"><?= \Yii::t('UI14.7', '$BLOCK_INVITE_FRIENDS_TITLE$') ?></span></div>
    <div class="clearfix"></div>

    <div class="col-sm-6">
        <?php \kartik\social\GooglePlugin::widget([
            'type'      => \kartik\social\GooglePlugin::BADGE_PERSON,
            'profileId' => '112011308023498994860',
            'settings'  => ['layout' => 'portrait'],
        ]) ?>
    </div>
    <div class="clearfix"></div>

    <div class="col-sm-6">
        <label><?= \Yii::t('UI14.7', '$BLOCK_INVITE_FRIENDS_MANUAL_LABEL$') ?></label>

        <md-chips name="iemails" ng-model="iemails" md-separator-keys="codes" placeholder="Type new email" secondary-placeholder="+ one more" md-transform-chip="transform($chip)">
        </md-chips>
    </div>
</div>

<div class="row">
    <h4><?= \Yii::t('UI14.7', '$BLOCK_INVITE_MY_LINK$') ?></h4>
    <div>
        <?= Html::input('text', 'my_link', \Yii::$app->request->hostInfo . '/invite' . \Yii::$app->user->id) ?>

        <md-button ng-click="copyLink()" class="md-raised md-primary"><?= \Yii::t('UI14.7',
                '$BLOCK_INVITE_MY_LINK_COPY_LABEL_BTN$') ?></md-button>
    </div>
</div>

<div class="row frend">
    <h4 class="textfrends"><?= \Yii::t('UI14.7', '$BLOCK_MY_FRIENDS_TEXT$') ?></h4>

    <div class="col-sm-6">

    </div>
</div>