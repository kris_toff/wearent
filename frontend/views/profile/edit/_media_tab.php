<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 08.09.2017
 * Time: 20:34
 */

use kartik\file\FileInput;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;

/** @var \yii\web\View $this */
/** @var \common\models\UserProfile $model */

\frontend\assets\CropperAsset::register($this);
\kartik\growl\GrowlAsset::register($this);


$video = $model->getVideo()->where(['is_deleted' => 0])->orderBy(['is_main' => SORT_DESC])->limit(1)->one();
?>

    <div class="row fotoVideo allBlockContent">
        <div class="col-sm-12">
            <h4><?= \Yii::t('UI14.5', '$BLOCK_PHOTO_TITLE$') ?> <span class="popupHover"
                        data-toggle="popover"
                        data-trigger="hover"
                        data-container="body"
                        data-placement="bottom auto"
                        data-html="true"
                        data-content="<?= \Yii::t('UI14.5', '$TAB_TITLE_INFO_HOVER$') ?>"
                        data-template='<div class="popover speedBlockHover fotoBlockHover" role="tooltip">
                                 <div class="arrow" style="top: 25%;"></div>
                                 <div class="popover-content"></div>
                                 </div>'><md-icon md-svg-src="/img/editProfile/infIcon.svg"></md-icon></span></h4>

        </div>

        <div class="col-sm-12">
            <div layout="row" style="max-width: 445px;" ng-show="sizeAvatars()">
                <div class="col-xs-12 blockForImgs">
                    <div class="row">
                        <div class="col-xs-6 col-md-4 loatedFoto"
                                ng-repeat="m in avatars | orderBy:'created_at' track by m.id">
                            <div class="blockImg center-block">
                                <img src="{{m.path}}" ng-class="{main: +m.is_main}" class="img-responsive img-circle" />

                                <span class="blockHoverImg">
                            <md-button class="md-icon-button" ng-click="removeAvatar(m.id)"><md-icon>clear</md-icon></md-button>
                            <a ng-click="makeAvatarDefault(m.id)"
                                    class="md-raised md-primary" ng-if="!+m.is_main"><?= \Yii::t('UI14.5',
                                    '$BLOCK_PHOTO_AVATAR_MAKE_DEFAULT_LINK$') ?></a>
                        </span>
                                <span class="basicFoto" ng-if="+m.is_main">
<!--                        --><? //= \Yii::t('UI14.5', '$BLOCK_PHOTO_AVATAR_DEFAULT_LABEL$') ?>
                        </span>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div>
                            <?= Html::button(\Yii::t('UI14.5', '$BLOCK_PHOTO_ADD_AVATAR_BTN_LABEL$'),
                                [
                                    'data-toggle' => 'modal',
                                    'data-target' => '#createPhotoBox',
                                    'class'       => 'btnFoto btn btn-block text-center',
                                ]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div layout="row" ng-hide="sizeAvatars()">
                <div class="col-xs-12">
                    <div class="blockAddFoto text-center" data-toggle="modal" data-target="#createPhotoBox">
                        <a>
                            <md-icon md-svg-icon="/img/editProfile/resizeImg/addNewFoto.svg"></md-icon>
                            <br> <?= \Yii::t('UI14.5', '$BLOCK_PHOTO_NO_FILES_TEXT$') ?></a>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div>
                    <?= Html::button(\Yii::t('UI14.5', '$BLOCK_PHOTO_ADD_AVATAR_BTN_LABEL$'),
                        [
                            'data-toggle' => 'modal',
                            'data-target' => '#createPhotoBox',
                            'class'       => 'btnFoto btn btn-block text-center',
                        ]) ?>
                </div>
            </div>
        </div>

    </div>
    <div class="row video">
        <div class="col-xs-12">
            <h4><?= \Yii::t('UI14.5', '$BLOCK_VIDEO_TITLE$') ?></h4>
        </div>


        <div class="col-xs-12">
            <!-- video -->
            <div id="user_media_video"
                    class="<?= empty($video) ? 'hidden ' : '' ?>embed-responsive embed-responsive-16by9">
                <?php if (!empty($video)): ?>
                    <?php
                    $options = ['controls' => 'controls'];
                    if (!empty($video['poster_path'])) {
                        $options['poster'] = $video['poster_path'];
                    }
                    print Html::beginTag('video', $options);
                    ?>

                    <?= Html::tag('source', '', ['src' => $video['path'], 'type' => $video['type']]) ?>

                    <?= Html::endTag('video'); ?>
                <?php endif; ?>
            </div>

            <?php if (empty($video)): ?>
                <div class="blockAddVideo text-center" data-toggle="modal" data-target="#createVideoBox">
                    <a>
                        <md-icon md-svg-icon="/img/editProfile/resizeImg/addNewFoto.svg"></md-icon>
                        <br> <?= \Yii::t('UI14.5',
                            '$BLOCK_VIDEO_NO_FILE_TEXT$') ?></a>
                </div>
            <?php endif; ?>

            <div class="btnVideo">
                <a
                        class="btn btn-block web"
                        data-toggle="modal"
                        data-target="#createVideoBox">
                    <?= \Yii::t('UI14.5',
                        '$BLOCK_VIDEO_REPLACE_FILE_BTN_LABEL$') ?>
                </a>
            </div>
        </div>
    </div>


    <!-- modal for load new avatars -->
    <div class="modal fade createFoto" id="createPhotoBox" tabindex="-1" role="dialog"
            aria-labelledby="createFotoLabel"
            aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">
                            <svg width="8px" height="8px" viewBox="0 0 8 8">
        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
            <g id="UI15.1----Отзывы-(личный-кабинет)---дефолт" transform="translate(-525.000000, -774.000000)"
                    stroke="#FFFFFF" fill="#FFFFFF">
                <g id="content" transform="translate(280.000000, 151.000000)">
                    <g id="Group-22" transform="translate(20.000000, 89.000000)">
                        <g id="Group-20">
                            <g id="Group-19" transform="translate(0.000000, 204.000000)">
                                <g id="Group-25" transform="translate(70.000000, 273.000000)">
                                    <g id="close" transform="translate(151.000000, 53.000000)">
                                        <g transform="translate(4.888889, 4.888889)" id="Combined-Shape">
                                            <path d="M3.11111111,2.48257175 L0.832788199,0.204248838 C0.659221847,0.0306824861 0.37781519,0.0306824861 0.204248838,0.204248838 L0.204248838,0.832788199 L2.48257175,3.11111111 L0.151606014,5.44207685 C-0.0219603375,5.6156432 -0.0219603375,5.89704986 0.151606014,6.07061621 C0.325172366,6.24418256 0.606579024,6.24418256 0.780145375,6.07061621 L3.11111111,3.73965047 L5.44207685,6.07061621 C5.6156432,6.24418256 5.89704986,6.24418256 6.07061621,6.07061621 C6.24418256,5.89704986 6.24418256,5.6156432 6.07061621,5.44207685 L3.73965047,3.11111111 L6.01797338,0.832788199 C6.19153974,0.659221847 6.19153974,0.37781519 6.01797338,0.204248838 L5.38943402,0.204248838 L3.11111111,2.48257175 Z"></path>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
                        </span>
                    </button>
                    <h4 class="modal-title"><?= \Yii::t('UI14.5.1', '$BOX_LOAD_AVATAR_TITLE$') ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6 leftBlock">
                            <div id="avatar_preview_container">
                                <div id="avatar_preview" class="editFoto"></div>

                                <div class="pull-down" ng-hide="isEmpty">
                                    <md-button class="pull-left md-icon-button" ng-click="rotateAvatar(-90)">
                                        <md-icon>reply</md-icon>
                                    </md-button>
                                    <md-button class="pull-right md-icon-button" ng-click="rotateAvatar(90)">
                                        <md-icon>redo</md-icon>
                                    </md-button>
                                </div>
                            </div>

                            <div ng-show="isSend">
                                    <span class="pull-right">{{percentProgress}} %</span>

                                    <md-icon>attach_file</md-icon>
                                    <span>{{fileName}}</span>
                                    <span><strong>{{fileSize}} Mb</strong></span>

                                    <div class="clearfix"></div>

                                    <md-progress-linear md-mode="determinate"
                                            ng-value="percentProgress"></md-progress-linear>
                            </div>

                            <div ng-hide="isSend">
                                <?= FileInput::widget([
                                    'id'            => 'selecter_new_file-input',
                                    'name'          => 'test',
                                    'pluginOptions' => [
                                        'showCaption' => false,
                                        'showRemove'  => false,
                                        'showUpload'  => false,
                                        'showPreview' => false,
                                        'browseLabel' => \Yii::t('UI14.5.1', '$BOX_LOAD_AVATAR_SELECT_NEW_PHOTO_BTN$'),
                                        'browseClass' => 'btn btn-block red loadVideo',
                                        'browseIcon'  => '',
                                    ],
                                    'options'       => [
                                        'accept' => 'image/*',
                                    ],
                                ]) ?>
                            </div>


                        </div>
                        <div class="col-sm-6 rightBlock">
                            <div class="webFoto">
                                <div class="yourFoto">

                                    <p class="text-center"><?= \Yii::t('UI14.5.1',
                                            '$BOX_LOAD_AVATAR_FINAL_PICTURE_TITLE$') ?></p>
                                    <div class="text-center avatar">
                                        <span ng-show="isEmpty">
                                        <svg width="76px" height="95px" viewBox="0 0 76 95">
                                            <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                                    fill-rule="evenodd" stroke-linecap="round"
                                                    stroke-linejoin="round">
                                                <g id="UI14.7-----Загрузка-и-редактирование-фото"
                                                        transform="translate(-801.000000, -753.000000)"
                                                        stroke="#A0A0A0">
                                                    <g id="Group-7"
                                                            transform="translate(-1.000000, 0.000000)">
                                                        <g id="product-copy-5"
                                                                transform="translate(335.000000, 638.000000)">
                                                            <g id="Group"
                                                                    transform="translate(445.000000, 94.000000)">
                                                                <g id="avatar_icon"
                                                                        transform="translate(23.000000, 22.000000)">
                                                                    <path d="M48.2230932,52.5831579 L54.0006356,52.5831579 C65.1705508,52.5831579 74.2220339,61.5774737 74.2220339,72.6768421 L74.2220339,87.0294737"
                                                                            id="Shape"></path>
                                                                    <path d="M0.462076271,87.2208421 L0.462076271,72.8682105 C0.462076271,61.7688421 9.51355932,52.7745263 20.6834746,52.7745263 L26.4610169,52.7745263"
                                                                            id="Shape"></path>
                                                                    <path d="M47.4527542,49.904 C48.0305085,51.2435789 48.415678,52.5831579 48.415678,54.1141053 C48.415678,60.0465263 43.6010593,64.8307368 37.6309322,64.8307368 C31.6608051,64.8307368 26.8461864,60.0465263 26.8461864,54.1141053 C26.8461864,52.7745263 27.0387712,51.2435789 27.6165254,50.0953684"
                                                                            id="Shape"></path>
                                                                    <path d="M56.1190678,27.8966316 C56.1190678,39.1873684 48.9934322,53.54 37.6309322,53.54 C26.2684322,53.54 19.1427966,39.1873684 19.1427966,27.8966316 C19.1427966,16.6058947 20.1057203,0.148210526 37.6309322,0.148210526 C54.9635593,0.148210526 56.1190678,16.6058947 56.1190678,27.8966316 Z"
                                                                            id="Shape"></path>
                                                                    <path d="M13.557839,93.9187368 L13.557839,79.7574737"
                                                                            id="Shape"></path>
                                                                    <path d="M61.3188559,93.9187368 L61.3188559,79.7574737"
                                                                            id="Shape"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                        </span>

                                        <div id="real_avatar_preview"></div>
                                    </div>
                                    <p class="text-center grey"><?= \Yii::t('UI14.5.1',
                                            '$BOX_LOAD_AVATAR_FINAL_PICTURE_HINT$') ?></p>
                                </div>

                                <a href="#" class="btn btn-block red" ng-click="mksnap_webcam()">
                                    <span ng-hide="mkwebcam"><?= \Yii::t('UI14.5.1',
                                            '$BOX_LOAD_AVATAR_START_WEBCAM_BTN$') ?></span>
                                    <span ng-show="mkwebcam"><?= \Yii::t('UI14.5.1',
                                            '$BOX_LOAD_AVATAR_SNAP_WEBCAM_BTN$') ?></span>
                                </a>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default cloce" data-dismiss="modal" ng-disabled="isSend">
                        <?= \Yii::t('UI14.5.1', '$BOX_FOOTER_CANCEL_BTN$') ?>
                    </button>
                    <button type="button" class="btn btn-primary send" ng-click="sendNewAvatar()" ng-disabled="isSend">
                        <?= \Yii::t('UI14.5.1', '$BOX_FOOTER_SAVE_BTN$') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>


    <!-- modal for load video -->
    <div class="modal fade createVideo" id="createVideoBox" tabindex="-1" role="dialog"
            aria-labelledby="createVideoLabel"
            aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">
                            <svg class="hidden-xs" width="8px" height="8px" viewBox="0 0 8 8">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
        <g id="UI15.1----Отзывы-(личный-кабинет)---дефолт" transform="translate(-525.000000, -774.000000)"
                stroke="#FFFFFF" fill="#FFFFFF">
            <g id="content" transform="translate(280.000000, 151.000000)">
                <g id="Group-22" transform="translate(20.000000, 89.000000)">
                    <g id="Group-20">
                        <g id="Group-19" transform="translate(0.000000, 204.000000)">
                            <g id="Group-25" transform="translate(70.000000, 273.000000)">
                                <g id="close" transform="translate(151.000000, 53.000000)">
                                    <g transform="translate(4.888889, 4.888889)" id="Combined-Shape">
                                        <path d="M3.11111111,2.48257175 L0.832788199,0.204248838 C0.659221847,0.0306824861 0.37781519,0.0306824861 0.204248838,0.204248838 L0.204248838,0.832788199 L2.48257175,3.11111111 L0.151606014,5.44207685 C-0.0219603375,5.6156432 -0.0219603375,5.89704986 0.151606014,6.07061621 C0.325172366,6.24418256 0.606579024,6.24418256 0.780145375,6.07061621 L3.11111111,3.73965047 L5.44207685,6.07061621 C5.6156432,6.24418256 5.89704986,6.24418256 6.07061621,6.07061621 C6.24418256,5.89704986 6.24418256,5.6156432 6.07061621,5.44207685 L3.73965047,3.11111111 L6.01797338,0.832788199 C6.19153974,0.659221847 6.19153974,0.37781519 6.01797338,0.204248838 L5.38943402,0.204248838 L3.11111111,2.48257175 Z"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>

                            <svg class="hidden visible-xs" width="22px" height="21px" viewBox="0 0 22 21">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
        <g id="UI1.6.2-Модал-категориии" transform="translate(-373.000000, -25.000000)" stroke="#3C3D3E"
                stroke-width="2">
            <g id="Group-4">
                <g id="close---icon---grey" transform="translate(374.000000, 26.000000)">
                    <path d="M0.318645628,18.83413 L19.9895953,0.203126916" id="Line"></path>
                    <path d="M0.318645628,18.83413 L19.9895953,0.203126916" id="Line-Copy-2"
                            transform="translate(10.154120, 9.518628) scale(-1, 1) translate(-10.154120, -9.518628) "></path>
                </g>
            </g>
        </g>
    </g>
</svg>
                        </span>
                    </button>
                    <h4 class="modal-title"><?= \Yii::t('UI14.5.2', '$BOX_LOAD_VIDEO_TITLE$') ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6 leftBlock">
                            <div id="video_preview"
                                    class="editFoto embed-responsive embed-responsive-16by9"
                                    ng-hide="isNewVideo"></div>

                            <div class="notVideo text-center" ng-show="isNewVideo">
                                <p class="grey">
                                    <md-icon md-svg-icon="/img/editProfile/resizeImg/addNewFoto.svg"></md-icon>
                                    <br></p>
                                <p class="grey"><?= \Yii::t('UI14.5.2', '$BOX_LOAD_VIDEO_NO_FILE_TEXT$') ?></p>
                            </div>

                            <div class="row" ng-show="isSend">
                                <div class="col-sm-12">
                                    <span class="pull-right">{{percentProgress}} %</span>

                                    <md-icon>attach_file</md-icon>
                                    <span>{{fileName}}</span>
                                    <span><strong>{{fileSize}} Mb</strong></span>

                                    <div class="clearfix"></div>

                                    <md-progress-linear md-mode="determinate"
                                            ng-value="percentProgress"></md-progress-linear>
                                </div>
                            </div>


                            <div class="row" ng-hide="isSend">
                                <div class="col-sm-6 col-xs-6">
                                    <?= FileInput::widget([
                                        'id'            => 'selecter_new_videofile-input',
                                        'name'          => 'test2',
                                        'pluginOptions' => [
                                            'showCaption' => false,
                                            'showRemove'  => false,
                                            'showUpload'  => false,
                                            'showPreview' => false,
                                            'browseLabel' => \Yii::t('UI14.5.2',
                                                '$BOX_LOAD_VIDEO_SELECT_NEW_VIDEO_BTN$'),
                                            'browseClass' => 'btn  btn-block red loadVideo',
                                            'browseIcon'  => '',
                                        ],
                                    ]) ?>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <a class="btn btn-block green"
                                            ng-click="recordNewVideoFromWebcam()"
                                            ng-switch="webcamVideoStatus">
                                        <span ng-switch-when="0"><?= \Yii::t('UI14.5.2',
                                                '$BOX_LOAD_VIDEO_WEBCAM_BTN_INIT_RECORD$') ?></span>
                                        <span ng-switch-when="1"><?= \Yii::t('UI14.5.2',
                                                '$BOX_LOAD_VIDEO_WEBCAM_BTN_START_RECORD$') ?></span>
                                        <span ng-switch-when="2"><?= \Yii::t('UI14.5.2',
                                                '$BOX_LOAD_VIDEO_WEBCAM_BTN_STOP_RECORD$') ?></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 rightBlock">

                            <md-tabs md-no-ink="0" md-no-ink-bar="0" md-no-pagination="1">
                                <md-tab label="<?= \Yii::t('UI14.5.2', '$MINI_ADVICE_TAB_LABEL$') ?>" md-active="true">
                                    <h5><?= \Yii::t('UI14.5.2', '$MINI_ADVICE_TAB_TITLE$') ?></h5>

                                    <?= \Yii::t('UI14.5.2', '$MINI_ADVICE_TAB_LISTTEXT$') ?>
                                </md-tab>
                                <md-tab label="<?= \Yii::t('UI14.5.2', '$MINI_TELL_TAB_LABEL$') ?>">
                                    <h5><?= \Yii::t('UI14.5.2', '$MINI_TELL_TAB_TITLE$') ?></h5>
                                    <p><?= \Yii::t('UI14.5.2', '$MINI_TELL_TAB_TEXT$') ?></p>

                                    <?= \Yii::t('UI14.5.2', '$MINI_TELL_TAB_LISTTEXT$') ?>
                                </md-tab>
                            </md-tabs>

                        </div>
                    </div>

                </div>
                <div class="modal-footer hidden-xs">
                    <button type="button" class="btn btn-default cloce" data-dismiss="modal" ng-disabled="isSend">
                        <?= \Yii::t('UI14.5.2', '$BOX_FOOTER_CANCEL_BTN$') ?>
                    </button>
                    <button type="button" class="btn btn-primary send" ng-click="saveVideo()" ng-disabled="isSend">
                        <?= \Yii::t('UI14.5.2', '$BOX_FOOTER_SAVE_BTN$') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

<?php
$this->registerJsFile('/js/webcamjs-master/webcam.min.js', [
    'position' => \yii\web\View::POS_END,
    'depends'  => [
        'frontend\assets\AppAsset',
    ],
]);
$this->registerJsFile('//vjs.zencdn.net/5.19/video.min.js', [
    'position' => \yii\web\View::POS_END,
    'depends'  => [
        'frontend\assets\AppAsset',
    ],
]);
$this->registerJsFile('//cdn.WebRTC-Experiment.com/RecordRTC.js', [
    'position' => \yii\web\View::POS_END,
    'depends'  => [
        'frontend\assets\AppAsset',
    ],
]);
$this->registerJsFile('/js/videojs-record/dist/videojs.record.min.js', [
    'position' => \yii\web\View::POS_END,
    'depends'  => [
        'frontend\assets\AppAsset',
    ],
]);
$this->registerCssFile('//vjs.zencdn.net/5.19/video-js.min.css');
$this->registerCssFile('/js/videojs-record/dist/css/videojs.record.min.css');


$this->registerCss(<<<CSS
#avatar_preview_container{
    margin-bottom: 30px;
    position: relative;
}
#video_preview{
    padding-top: 0;
    margin-bottom: 30px;
}
#avatar_preview{
    padding-top: 0;
    height: 250px;
}
#avatar_preview_container .pull-down{
    position: absolute;
    width: 100%;
    bottom: 0;
}
#webcam_preview{
    margin-bottom: 30px;
}
#createPhotoBox .yourFoto .avatar{
    position: relative;
    overflow: hidden;
}
#real_avatar_preview{
    width: 100%;
    height: 100%;
}
#avatar_preview .cropper-view-box{
    border-radius: 50%;
    border: 1px solid #fff;
    outline: none;
}
#avatar_preview .cropper-face{
    border-radius: 50%;
}
#avatar_preview .cropper-line,#avatar_preview span.cropper-point{
    display: none;
}
#avatar_preview .resizable-angle-picture {
    position: absolute;
    top: calc(15% - 10px);
    right: calc(15% - 10px);
    width: 20px;
    height: 20px;
    z-index: 3;
}
#avatar_preview img.cropper-point{
    opacity: 1;
    background-color: transparent;
}

md-progress-linear .md-container{
    background-color: #d8d8d8;
}
md-progress-linear .md-bar{
    background-color: #4dc97b;
}
CSS
);