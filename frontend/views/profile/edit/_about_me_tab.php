<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 02.09.2017
 * Time: 21:28
 */

use yii\bootstrap\Html;

/** @var \yii\web\View $this */
/** @var \yii\bootstrap\ActiveForm $form */
/** @var \common\models\UserProfile $model */

?>


    <div class="row obligatoryInf allBlockContent">
        <div class="col-sm-12">
            <h4><?= \Yii::t('UI14.1', '$TAB_TITLE$') ?></h4>

            <div class="form-group <?= 'field-' . Html::getInputId($model, 'firstname') ?>">
                <?= Html::activeLabel($model, 'firstname', [
                    'label' => \Yii::t('UI14.1', '$FIELD_NAME_LABEL$'),
                    'class' => 'control-label',
                ]) ?>
                <div>
                    <?= Html::activeTextInput($model, 'firstname', [
                        'placeholder' => \Yii::t('UI14.1', '$FIELD_NAME_PLACEHOLDER$'),
                    ]) ?>
                </div>

                <?= Html::error($model, 'firstname', ['class' => 'help-block']) ?>
            </div>

            <div class="form-group <?= 'field-' . Html::getInputId($model, 'lastname') ?>">
                <?= Html::activeLabel($model, 'lastname', [
                    'label' => \Yii::t('UI14.1', '$FIELD_LASTNAME_LABEL$'),
                    'class' => 'control-label',
                ]) ?>

                <div>
                    <?= Html::activeTextInput($model, 'lastname', [
                        'placeholder' => \Yii::t('UI14.1', '$FIELD_LASTNAME_PLACEHOLDER$'),
                    ]) ?>

                    <md-icon md-svg-src="/img/icons/ic_visibility_off.svg" ng-cloak>
                        <md-tooltip><?= \Yii::t('UI14.1', '$FIELD_ICON_EYE_TOOLTIP$') ?></md-tooltip>
                    </md-icon>
                </div>

                <?= Html::error($model, 'lastname', ['class' => 'help-block']) ?>
            </div>

            <div class="form-group <?= 'field-' . Html::getInputId($model, 'gender') ?>">
                <?= Html::activeHiddenInput($model, 'gender', [
                    'ng-value' => 'genderValue',
                    'ng-init'  => 'genderValue = '
                        . ((int)$model['gender'] ?: '""')
                        . ';genderTextList = ["'
                        . \Yii::t('UI14.1', '$FIELD_GENDER_INCOMER$') . '", "'
//                        . \Yii::t('UI14.1', '$FIELD_GENDER_PLACEHOLDER$') . '", "'
                        . \Yii::t('UI14.1', '$FIELD_GENDER_DROPDOWN_ITEM_MALE$')
                        . '", "'
                        . \Yii::t('UI14.1', '$FIELD_GENDER_DROPDOWN_ITEM_FEMALE$')
                        . '"];',
                ]) ?>

                <?= Html::activeLabel($model, 'gender', [
                    'label' => \Yii::t('UI14.1', '$FIELD_GENDER_LABEL$'),
                    'class' => 'control-label',
                ]) ?>

                <div ng-cloak>
                    <md-select class="md-no-underline"
                            ng-model="genderValue"
                            placeholder="<?= \Yii::t('UI14.1', '$FIELD_GENDER_PLACEHOLDER$') ?>">
                        <md-option ng-value="n" ng-repeat="n in [0, 1,2]">{{genderTextList[n]}}</md-option>
                    </md-select>

                    <div class="btn-group" uib-dropdown hide>
                        <button type="button" class="btn btn-default btnForInput" uib-dropdown-toggle>{{genderText()}}
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" uib-dropdown-menu role="menu">
                            <li role="menuitem" ng-click="genderValue = 1;">
                                <a>{{genderTextList[1]}}</a>
                            </li>
                            <li role="menuitem" ng-click="genderValue = 2;">
                                <a>{{genderTextList[2]}}</a>
                            </li>
                        </ul>
                    </div>

                    <md-icon md-svg-src="/img/icons/ic_visibility_off.svg">
                        <md-tooltip><?= \Yii::t('UI14.1', '$FIELD_ICON_EYE_TOOLTIP$') ?></md-tooltip>
                    </md-icon>
                </div>

                <?= Html::error($model, 'gender', ['class' => 'help-block']) ?>
            </div>

            <div class="form-group birthDay" ng-cloak>
                <?= Html::activeHiddenInput($model, 'birthday_day', [
                    'ng-value' => 'birthdayDay',
                    'ng-init'  => 'birthdayDay = ' . ($model['birthday_day'] ?: '""'),
                ]) ?>
                <?= Html::activeHiddenInput($model, 'birthday_month', [
                    'ng-value' => 'birthdayMonth',
                    'ng-init'  => 'birthdayMonth = ' . ($model['birthday_month'] ?: '""'),
                ]) ?>
                <?= Html::activeHiddenInput($model, 'birthday_year', [
                    'ng-value' => 'birthdayYear',
                    'ng-init'  => 'birthdayYear = ' . ($model['birthday_year'] ?: '""'),
                ]) ?>

                <div class="dayBlock">
                    <?= Html::label(\Yii::t('UI14.1', '$FIELD_BIRTHDAY_DAY_LABEL$'), null,
                        ['class' => 'control-label']) ?>

                    <br />

                    <md-select class="md-no-underline" ng-model="birthdayDay">
                        <md-option ng-value="n" ng-repeat="n in days">{{n}}</md-option>
                    </md-select>

                    <div class="btn-group" uib-dropdown hide>
                        <button type="button" class="btn btn-default btnForInput" uib-dropdown-toggle>{{birthdayDay}}
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" uib-dropdown-menu role="menu">
                            <li role="menuitem" ng-repeat="n in days" ng-click="setDay(n)">
                                <a>{{n}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="monthBlock">
                    <?= Html::label(\Yii::t('UI14.1', '$FIELD_BIRTHDAY_MONTH_LABEL$'), null,
                        ['class' => 'control-label']) ?>

                    <br />

                    <md-select class="md-no-underline" ng-model="birthdayMonth">
                        <md-option ng-value="1+ $index" ng-repeat="n in months">{{n}}</md-option>
                    </md-select>

                    <div class="btn-group" uib-dropdown hide>
                        <button type="button" class="btn btn-default btnForInput" uib-dropdown-toggle>{{birthdayMonth}}
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" uib-dropdown-menu role="menu">
                            <md-virtual-repeat-container>
                                <li role="menuitem" md-virtual-repeat="n in months"
                                        ng-click="setMonth(n)" md-item-size="20">
                                    <a>{{n}}</a>
                                </li>
                            </md-virtual-repeat-container>

                        </ul>
                    </div>
                </div>
                <div class="yearBlock">
                    <?= Html::label(\Yii::t('UI14.1', '$FIELD_BIRTHDAY_YEAR_LABEL$'), null,
                        ['class' => 'control-label']) ?>

                    <br />

                    <md-select class="md-no-underline" ng-model="birthdayYear">
                        <md-option ng-value="n" ng-repeat="n in years">{{n}}</md-option>
                    </md-select>

                    <div class="btn-group" uib-dropdown hide>
                        <button type="button" class="btn btn-default btnForInput" uib-dropdown-toggle>{{birthdayYear}}
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" uib-dropdown-menu role="menu">
                            <li role="menuitem" ng-repeat="n in years" ng-click="setYear(n)">
                                <a>{{n}}</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <md-icon md-svg-src="/img/icons/ic_visibility_off.svg">
                    <md-tooltip><?= \Yii::t('UI14.1', '$FIELD_ICON_EYE_TOOLTIP$') ?></md-tooltip>
                </md-icon>
            </div>

            <div class="form-group aboutSelfBlock <?= 'field-' . Html::getInputId($model, 'description') ?>">

                <?= Html::activeLabel($model, 'description', [
                    'class' => 'control-label',
                    'label' => \Yii::t('UI14.1', '$FIELD_ABOUT_ME_LABEL$'),
                ]) ?>

                <div>
                    <md-input-container md-no-float class="md-block" style="margin: 0;">
                        <?= Html::activeTextarea($model, 'description', [
                            'rows'        => 4,
                            'max-rows'    => 12,
//                            'md-detect-hidden' => '1',
                            'placeholder' => \Yii::t('UI14.1', '$FIELD_ABOUT_ME_PLACEHOLDER$'),
                            'data'        => [
                                'toggle'    => 'popover',
                                'placement' => '{{_mdMedia(\'gt-sm\') ? \'right\' : \'bottom\'}}',
                                'trigger'   => 'focus',
                                'content'   => \Yii::t('UI14.1', '$FIELD_TEXTAREA_ABOUT_ME_TEXT$'),
                                'html'      => 1,
                                'container' => 'body',
                            ],
                        ]) ?>
                    </md-input-container>
                </div>
            </div>
        </div>
    </div>

<?php
$this->registerCss(<<<CSS
#userprofile-description + .popover ul{
    margin-left: 0;
    padding-left: 16px;
}
#userprofile-description + .popover ul li{
    padding: 6px 0;
}
CSS
);
$this->registerJs(<<<JS
jQuery("#update_info_form-tabs-container").find(".tab-content .tab-pane:not(.active) textarea").each(function(index, element){
    jQuery(element).css("minHeight",  jQuery(element).outerHeight());
});
jQuery("#update_info_form-tabs-container").on("show.bs.tab", function(event){
    jQuery( jQuery(event.target).attr("href") ).find("textarea").css("height", function(index, value){
        return jQuery(this).outerHeight();
    }).css("minHeight", "");
});
JS
);
