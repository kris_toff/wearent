<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 02.09.2017
 * Time: 16:26
 */

use kartik\tabs\TabsX;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/** @var \yii\web\View $this */
/** @var \common\models\UserProfile $model */

?>

    <div class="row marg hidden-xs"></div>

    <div class="row edit contentBody">
        <div class="container sidebarEditLeft hidden-xs hidden-sm">
            <div class="row">
                <div class="col-sm-12">
                    <?= $this->render('_left-menu') ?>
                </div>
            </div>
        </div>

        <!--    контент сторінки   -->
        <div class="container content" ng-controller="ProfileController as PCtrl">
            <div class="row">
                <div class="col-sm-12">
                    <div class="titleEdit">
                        <h4 class="text-left"><?= \Yii::t('UI14',
                                $model->isBusiness ? '$TITLE_PAGE_BUSINESS$' : '$TITLE_PAGE$') ?></h4>

                        <a href="/profile" class="showProfil">
                            <svg width="16px" height="21px" viewBox="0 0 16 21" version="1.1">
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="user" transform="translate(-1206.000000, -103.000000)" fill-rule="nonzero">
                                        <g id="product" transform="translate(1170.000000, 73.000000)">
                                            <path d="M46.5,39.5 C47.6,38.5 48.3,36.9 48.3,35.4 C48.3,32.9 46.3,30 43.7,30 C41.1,30 39.1,32.9 39.1,35.4 C39.1,36.9 39.8,38.4 40.9,39.5 C36.9,40.3 36,42.9 36,45 L36,50.4 C36,50.5 36.1,50.7 36.1,50.8 C36.1,50.9 36.3,50.9 36.5,50.9 L50.9,50.9 C51.2,50.9 51.4,50.7 51.4,50.4 L51.4,45 C51.3,42.9 50.4,40.3 46.5,39.5 Z M50.3,49.9 L36.9,49.9 L36.9,45 C36.9,42.2 38.6,40.7 42,40.3 C42.2,40.3 42.4,40.1 42.4,39.9 C42.4,39.7 42.3,39.5 42.2,39.4 C40.9,38.7 40,37 40,35.4 C40,33.4 41.6,31 43.6,31 C45.6,31 47.2,33.4 47.2,35.4 C47.2,37 46.3,38.6 45,39.4 C44.8,39.5 44.7,39.7 44.8,39.9 C44.9,40.1 45,40.3 45.2,40.3 C48.7,40.7 50.3,42.2 50.3,45 L50.3,49.9 L50.3,49.9 Z"
                                                    id="profile_icon"></path>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                            <?= \Yii::t('UI14', '$PROFILE_PREVIEW_LINK$') ?>
                        </a>
                    </div>

                    <div class="wrapPanel">
                        <?php $form = ActiveForm::begin([
                            'id'      => 'update_info-form',
                            'options' => [
                                'class'      => 'styleInput',
                                'novalidate' => '',
                                'name'       => 'profileEditForm',
                            ],
                        ]) ?>

                        <?= TabsX::widget([
                            'id'                => 'update_info_form-tabs',
                            'items'             => [
                                [
                                    'label'   => \Yii::t('UI14', '$ABOUT_ME_TAB_LABEL$'),
                                    'content' => $this->render('_about_me_tab', [
                                        'form'  => $form,
                                        'model' => $model,
                                    ]),
                                    'options' => [
                                        'ng-controller' => 'ProfileAboutMeTabController as PAMTC',
                                        'id'            => 'about_me_tab',
                                    ],
                                ],
                                [
                                    'label'   => \Yii::t('UI14', '$TRIBUTE_TAB_LABEL$'),
                                    'content' => $this->render('_statistic_tab', [
                                        'model' => $model,
                                    ]),
                                    'options' => [
                                        'id' => 'tributes_tab',
                                    ],
                                ],
                                [
                                    'label'   => \Yii::t('UI14', '$ADDITION_ABOUT_ME_TAB_LABEL$'),
                                    'content' => $this->render('_additional_about_me_tab', [
                                        'form'  => $form,
                                        'model' => $model,
                                    ]),
                                    'options' => [
                                        'ng-controller' => 'ProfileMoreAboutMeTabController as PMAMTC',
                                        'id'            => 'additional_about_me_tab',
                                    ],
                                ],
                                [
                                    'label'   => \Yii::t('UI14', '$CONTACTS_TAB_LABEL$'),
                                    'content' => $this->render('_contacts_tab', [
                                        'model' => $model,
                                        'form'  => $form,
                                    ]),
                                    'options' => [
                                        'ng-controller' => 'ProfileContactsTabController as PCTC',
                                        'id'            => 'contacts_tab',
                                    ],
                                ],
                                [
                                    'label'   => \Yii::t('UI14', '$MEDIA_TAB_LABEL$'),
                                    'content' => $this->render('_media_tab', [
                                        'model' => $model,
                                    ]),
                                    'options' => [
                                        'id'            => 'media_tab',
                                        'ng-controller' => 'ProfileMediaTabController as PMTC',
                                    ],
                                ],
                                [
                                    'label'   => \Yii::t('UI14', '$VERIFICATION_TAB_LABEL$'),
                                    'content' => $this->render('_verification_tab', [
                                        'model' => $model,
                                    ]),
                                    'options' => [
                                        'id'            => 'verification_tab',
                                        'ng-controller' => 'ProfileVerificationTabController as PVTC',
                                    ],
                                ],
                                [
                                    'label'   => \Yii::t('UI14', '$RECOMMENDATION_TAB_LABEL$'),
                                    'content' => $this->render('_recommendation_tab', []),
                                    'options' => [
                                        'id'            => 'recommendation_tab',
                                        'ng-controller' => 'ProfileRecommendationTabController as PRTC',
                                    ],
                                ],
                            ],
                            'containerOptions'  => [
                                'class' => 'tabPaneledit',
                                'role'  => 'tabpanel',
                            ],
                            'enableStickyTabs'  => true,
                            'stickyTabsOptions' => [
                                'backToTop' => false,
                                //                                'selectorAttribute' => '',
                            ],
                        ]) ?>

                        <div class="blockTwoBtn">
                            <?= Html::submitButton(\Yii::t('UI14', '$FORM_SUBMIT_BTN$'), ['class' => 'btn greenBtn']) ?>
                            <?= Html::a(\Yii::t('UI14', '$FORM_CANCEL_BTN$'), ['/profile'],
                                ['class' => 'btn redBtn']) ?>
                        </div>

                        <?php ActiveForm::end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php
$this->registerJsFile('/js/aprofile.js', [
    'position' => \yii\web\View::POS_END,
    'depends'  => [
        'frontend\assets\AppAsset',
    ],
]);
$this->registerCss(<<<CSS
.md-panel.md-tooltip{
    background-color: #fff;
    color: #444546;
    box-shadow: 0 6px 12px 2px rgba(69, 69, 69, .25);
}
CSS
);
