<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 16.09.2017
 * Time: 1:05
 */

use yii\authclient\widgets\AuthChoice;
use yii\helpers\ArrayHelper;

/** @var \yii\web\View $this */
/** @var \common\models\UserProfile $model */

//print_r($model->social);
$social = ArrayHelper::index($model->social, 'client');
?>

    <div class="row trust allBlockContent">
        <div class="col-xs-12">
            <div class="topTrust">
                <h4 class="text-left"><?= \Yii::t('UI14.6', '$BLOCK_TRUSTVERIFICATION_TITLE$') ?></h4>
                <p class="subText"><?= \Yii::t('UI14.6', '$BLOCK_TRUSTVERIFICATION_TEXT$') ?></p>
                <a href="#" class="redLink"><?= \Yii::t('UI14.6', '$BLOCK_TRUSTVERIFICATION_LINK$') ?></a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-9">
            <div class="styp">
                <h4 class="textStyp"><?= \Yii::t('UI14.6', '$BLOCK_TRUST_TITLE$') ?></h4>
                <div class="">
                    <p class="boldTopText">{pc}</p>

                    <div class="lineRating"></div>

                    <p class="textForAccordion"><?= \Yii::t('UI14.6', '$BLOCK_TRUST_MAXCONDITION_LIST_LABEL$') ?></p>

                    <?php $socialClient = AuthChoice::begin([
                        'id'          => 'social_render',
                        'baseAuthUrl' => ['/auth/social'],
                        'autoRender'  => false,
                    ]);
                    $clients            = $socialClient->getClients();
                    ?>

                    <!-- according -->
                    <?= \yii\jui\Accordion::widget([
                        'id'            => 'according_user_verification',
                        'items'         => [
                            [
                                'header'  => '<md-icon class="pull-right" md-svg-src="/img/editProfile/statusIcon/errorIcon.svg"></md-icon><span class="leftTitle">'
                                    . \Yii::t('UI14.6',
                                        '$ACCORD_EMAIL_HEADER$')
                                    . '</span><span class="redText">10%</span>',
                                'content' => $this->render('part/verification/list-eh', [
                                    'slist' => 'emails',
                                    'sitem' => 'email',
                                ]),
                                'options' => [
                                    'class' => 'link_to_contacts',
                                ],
                            ],
                            [
                                'header'  => '<md-icon class="pull-right" md-svg-src="/img/editProfile/statusIcon/errorIcon.svg"></md-icon><span class="leftTitle">'
                                    . \Yii::t('UI14.6',
                                        '$ACCORD_PHONE_HEADER$')
                                    . '</span><span class="redText">10%</span>',
                                'content' => $this->render('part/verification/list-eh', [
                                    'slist' => 'phones',
                                    'sitem' => 'phone',
                                ]),
                                'options' => [
                                    'class' => 'link_to_contacts',
                                ],
                            ],
                            [
                                'header'  => '<md-icon class="pull-right" md-svg-src="/img/editProfile/statusIcon/errorIcon.svg"></md-icon><span class="leftTitle">'
                                    . \Yii::t('UI14.6',
                                        '$ACCORD_GOOGLE_PLUS_HEADER$')
                                    . '</span><span class="redText">10%</span>',
                                'content' => $this->render('part/verification/socials', [
                                    'social' => $socialClient,
                                    'client' => ArrayHelper::getValue($clients, 'google'),
                                    'model'  => ArrayHelper::getValue($social, 'google'),
                                    'name'   => 'Google',
                                ]),
                            ],
                            [
                                'header'  => '<md-icon class="pull-right" md-svg-src="/img/editProfile/statusIcon/errorIcon.svg"></md-icon><span class="leftTitle">'
                                    . \Yii::t('UI14.6',
                                        '$ACCORD_FACEBOOK_HEADER$')
                                    . '</span><span class="redText">10%</span>',
                                'content' => $this->render('part/verification/socials', [
                                    'social' => $socialClient,
                                    'client' => ArrayHelper::getValue($clients, 'facebook'),
                                    'model'  => ArrayHelper::getValue($social, 'facebook'),

                                    'name' => 'Facebook',
                                ]),
                            ],
                            [
                                'header'  => '<md-icon class="pull-right" md-svg-src="/img/editProfile/statusIcon/errorIcon.svg"></md-icon><span class="leftTitle">'
                                    . \Yii::t('UI14.6', '$ACCORD_TWITTER_HEADER$')
                                    . '</span><span class="redText">10%</span>',
                                'content' => $this->render('part/verification/socials', [
                                    'social' => $socialClient,
                                    'client' => ArrayHelper::getValue($clients, 'twitter'),
                                    'model'  => ArrayHelper::getValue($social, 'twitter'),
                                    'name'   => 'Twitter',
                                ]),
                            ],
                            [
                                'header'  => '<md-icon class="pull-right" md-svg-src="/img/editProfile/statusIcon/errorIcon.svg"></md-icon><span class="leftTitle">'
                                    . \Yii::t('UI14.6',
                                        '$ACCORD_PAYMENT_HEADER$')
                                    . '</span><span class="redText">10%</span>',
                                'content' => $this->render('part/verification/payment'),
                            ],
                            [
                                'header'  => '<md-icon class="pull-right" md-svg-src="/img/editProfile/statusIcon/errorIcon.svg"></md-icon><span class="leftTitle">'
                                    . \Yii::t('UI14.6',
                                        '$ACCORD_DOCUMENT_HEADER$')
                                    . '</span><span class="redText">10%</span>',
                                'content' => $this->render('part/verification/document'),
                            ],
                        ],
                        'clientOptions' => [
                            'icons'       => [
                                'header'       => 'ui-icon-plus',
                                'activeHeader' => 'ui-icon-minus',
                            ],
                            'heightStyle' => 'content',
                            'active'      => false,
                            'collapsible' => true,
                        ],
                    ]) ?>

                    <?php AuthChoice::end() ?>
                </div>
            </div>
        </div>


    </div>

<?php
$this->registerJsFile('//www.paypalobjects.com/api/checkout.js', [
    'position' => \yii\web\View::POS_BEGIN,
]);
