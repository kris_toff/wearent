<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.10.2017
 * Time: 12:25
 */

use kartik\file\FileInput;
use yii\helpers\Html;

/** @var \yii\web\View $this */

/** @var string $type */
$type = strtolower($type);

?>

<p class="greyText"><?= \Yii::t('UI14.6', '$ACCORD_DOCUMENT_LIST_' . strtoupper($type) . '_TEXT$') ?></p>

<?= FileInput::widget([
    'id'            => "file_{$type}_input_btn",
    'name'          => "file_{$type}_input",
    'pluginOptions' => [
        'showPreview'      => false,
        'showCaption'      => false,
        'showRemove'       => false,
        'showUpload'       => false,
        'browseLabel'      => \Yii::t('UI14.6', '$ACCORD_DOCUMENT_UPLOAD_FILES_BTN$'),
        'browseIcon'       => '<i class="glyphicon glyphicon-paperclip"></i>',
        'browseClass'      => 'btn btn-block',
        'allowedFileTypes' => ['image'],
        'uploadAsync'      => false,
        'maxFileSize'      => 10240,
        'maxFileCount'     => 5,
    ],
    'options'       => [
        'multiple' => true,
        'accept'   => 'image/*',
        'data'     => [
            'type' => $type,
        ],
    ],
]) ?>


<div class="file-list">
    <?= Html::beginTag('div', [
        'ng-repeat'    => "file in files | filter:{type: '{$type}'} track by file.id",
        'layout'       => 'row',
        'layout-align' => 'start center',
    ]) ?>
    <md-icon flex="none">attach_file</md-icon>
    <span flex="nogrow" md-truncate="1">{{file.name}}</span>
    <span flex="none">{{getSize(file.size)}}Mb</span>

    <md-button flex="none"
            class="md-icon-button"
            ng-click="removeDocument($event, file.id)"
            ng-show="file.status == null"
            uib-popover="<?= \Yii::t('UI14.6', '$ACCORD_DOCUMENT_ITEM_STATE_ICON_WAITING$') ?>"
            popover-placement="right"
            popover-trigger="{'mouseenter': 'mouseleave'}"
            popover-append-to-body="1">
        <md-icon>highlight_off</md-icon>
    </md-button>
    <span flex></span>

    <md-icon ng-show="file.status == 0"
            uib-popover="<?= \Yii::t('UI14.6', '$ACCORD_DOCUMENT_ITEM_STATE_ICON_PROCESSED$') ?>"
            popover-placement="right"
            popover-trigger="{'mouseenter': 'mouseleave'}"
            popover-append-to-body="1">explore
    </md-icon>
    <md-icon ng-show="file.status == 1"
            uib-popover="'<?= \Yii::t('UI14.6', '$ACCORD_DOCUMENT_ITEM_STATE_ICON_APPROVED$') ?>"
            popover-placement="right"
            popover-trigger="{'mouseenter': 'mouseleave'}"
            popover-appen-to-body="1">verified_user
    </md-icon>
    <md-icon ng-show="file.status == -1"
            uib-popover="{{::'<?= \Yii::t('UI14.6', '$ACCORD_DOCUMENT_ITEM_STATE_ICON_FAILURE$') ?>:' + file.cause}}"
            popover-placement="right"
            popover-trigger="{'mouseenter': 'mouseleave'}"
            popover-append-to-body="1">not_interested
    </md-icon>
    <?= Html::endTag('div') ?>

    <?= Html::beginTag('div', ['ng-repeat' => "file in tempfiles['{$type}'] track by file.size"]) ?>
    <div layout="row" layout-align="start center">
        <md-icon flex="none">attach_file</md-icon>
        <span flex="nogrow" md-truncate="1">{{file.name}}</span>
        <span flex="none">{{getSize(file.size)}}Mb</span>

        <span flex></span>
        <?= Html::tag('span', "<strong>{{getSizePercent(\$index,'{$type}')}}%</strong>",
            ['class' => 'pull-right', 'flex' => 'none']) ?>
    </div>

    <?= Html::tag('md-progress-linear', '',
        ['md-mode' => 'determinate', 'ng-value' => "getSizePercent(\$index,'{$type}')"]) ?>
    <?= Html::endTag('div') ?>
</div>


