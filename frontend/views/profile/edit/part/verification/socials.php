<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.10.2017
 * Time: 14:02
 */

use yii\bootstrap\Html;

/** @var \yii\web\View $this */
/** @var \yii\authclient\widgets\AuthChoice $social */
/** @var \yii\authclient\clients\Google|\yii\authclient\clients\Facebook|\yii\authclient\clients\Twitter|null $client */
/** @var \frontend\models\UserSocial|null $model */

/** @var string $name */

$isSetUp = !is_null($model);
$name    = strtolower($name);
?>


<p><?= \Yii::t('UI14.6', '$ACCORD_SOCIAL_' . strtoupper($name) . '_PLUS_GOAL$') ?></p>

<?= Html::beginTag('div', [
    'ng-show' => 'is' . ucfirst($name),
    'ng-init' => 'is' . ucfirst($name) . ' = ' . ($isSetUp ? 'true' : 'false'),
]); ?>
<!-- exists social client -->
<div class="blockResultSeccess">
    <span class="successReg"><?= \Yii::t('UI14.6', '$ACCORD_SOCIAL_ASSIGN_TEXT$') ?>:</span>
    <span class="email"> <strong><?= $isSetUp ? Html::encode($model->name) : '' ?></strong></span>
</div>
<div class="inlineButt">
    <?= Html::a(\Yii::t('UI14.6', '$ACCORD_SOCIAL_ASSIGN_CHANGE_LINK$'), $social->createClientUrl($client),
        ['title' => ucfirst($name), 'class' => $name . ' auth-link']) ?>
    <?= Html::a(\Yii::t('UI14.6', '$ACCORD_SOCIAL_ASSIGN_VIEW_LINK$'), $isSetUp ? $model->url : '',
        ['title' => ucfirst($name), 'target' => '_blank']) ?>
    <?= Html::a(\Yii::t('UI14.6', '$ACCORD_SOCIAL_ASSIGN_DISCARD_LINK$'), '#',
        ['title' => ucfirst($name), 'ng-click' => "discard(\$event, '{$name}')"]) ?>
</div>
<?= Html::endTag('div') ?>

<?= Html::beginTag('div', ['ng-hide' => 'is' . ucfirst($name), 'class'=>'blockResultError']) ?>
<span><?= \Yii::t('UI14.6', '$ACCORD_SOCIAL_BLANK_' . strtoupper($name) . '_TEXT$') ?></span>
<?= Html::a('link', $social->createClientUrl($client),
    ['title' => ucfirst($name), 'class' => $name . ' auth-link btn greenBtn']) ?>
<?= Html::endTag('div') ?>

