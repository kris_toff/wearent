<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 08.10.2017
 * Time: 14:03
 */

use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var string $slist */
/** @var string $sitem */

?>

<?= Html::beginTag('div', ['class' => 'form-group', 'ng-repeat' => "file in {$slist} | filter:{is_verify: 1}"]) ?>
<?= Html::label(\Yii::t('UI14.6', '$ACCORD_COMMUNICATION_' . strtoupper($sitem) . '_LABEL$'),
    '{{\'communicate_' . strtolower($sitem) . '\' + file.id}}', ['class' => 'control-label']) ?>

<?= Html::input('text', 'q', "{{file.{$sitem}}}",
    [
        'class'    => 'form-control',
        'readonly' => 'readonly',
        'disabled' => 'disabled',
        'id'       => '{{\'communicate_' . strtolower($sitem) . '\' + file.id}}',
    ]) ?>

<?= Html::endTag('div') ?>

<p><?= \Yii::t('UI14.6', '$ACCORD_COMMUNICATION_' . strtoupper($sitem) . '_LINK_TO_CONTACTS$') ?></p>
