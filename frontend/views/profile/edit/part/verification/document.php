<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 18.09.2017
 * Time: 23:44
 */

use yii\bootstrap\Collapse;

/** @var \yii\web\View $this */

?>

<div>
    <?= \Yii::t('UI14.6', '$ACCORD_DOCUMENT_TEXT$') ?>
</div>

<div><?= \Yii::t('UI14.6', '$ACCORD_DOCUMENT_LISTTEXT$') ?></div>
<?= Collapse::widget([
    'id'           => 'document_verification',
    'encodeLabels' => false,
    'items'        => [
        [
            'label'   => '<span class="leftTitle">'
                . \Yii::t('UI14.6', '$ACCORD_DOCUMENT_LIST_ITEM1$')
                . '</span><md-icon hide md-svg-src="/img/editProfile/statusIcon/errorIcon.svg"></md-icon>',
            'content' => $this->render('documents/person_paper', ['type' => 'passport']),
        ],
        [
            'label'   => '<span class="leftTitle">' . \Yii::t('UI14.6', '$ACCORD_DOCUMENT_LIST_ITEM2$') .'</span><md-icon hide md-svg-src="/img/editProfile/statusIcon/errorIcon.svg"></md-icon>',
            'content' => $this->render('documents/person_paper', ['type' => 'registration']),
        ],
        [
            'label'   => '<span class="leftTitle">' .\Yii::t('UI14.6', '$ACCORD_DOCUMENT_LIST_ITEM3$') . '</span><md-icon hide md-svg-src="/img/editProfile/statusIcon/errorIcon.svg"></md-icon>',
            'content' => $this->render('documents/person_paper', ['type' => 'other']),
        ],
    ],
]) ?>
