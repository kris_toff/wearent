<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.09.2017
 * Time: 14:33
 */

/** @var \yii\web\View $this */


?>


<div id="language_box" class="md-panel-container">
    <div class="box_header hidden visible-xs">
        <p class="text-center"><?= \Yii::t('UI14.3.1', '$LANGUAGE_BOX_MOBILE_HEADER_TITLE$') ?></p>
    </div>

    <md-button class="md-icon-button closeBtn" ng-click="ctrl.close()">
        <md-icon class="hidden visible-xs" md-svg-src="/img/editProfile/mobile/closeIcon.svg"></md-icon>
        <md-icon class="hidden-xs">clear</md-icon>
    </md-button>

    <div class="description">
        <h5><?= \Yii::t('UI14.3.1', '$LANGUAGE_BOX_TEXT_BOLD$') ?></h5>
        <p><?= \Yii::t('UI14.3.1', '$LANGUAGE_BOX_TEXT_SIMPLE$') ?></p>
    </div>

    <md-input-container md-no-float
            class="md-block styleInput hidden visible-xs">

        <div layout="row" layout-align="start center">
            <input type="text"
                    class="form-control"
                    placeholder="<?= \Yii::t('UI14.3.1', '$FILTER_INPUT_PLACEHOLDER$') ?>"
                    ng-model="filterWord" flex="grow" />
        </div>
    </md-input-container>

    <div layout="row" layout-wrap class="panel-content">
            <div ng-repeat="(k, v) in ctrl.lng | levelDeepO:filterWord" flex-gt-sm="33" flex="grow">
                <md-checkbox ng-checked="isSelected(k)" ng-click="toggle(k)">{{v}}</md-checkbox>
            </div>
    </div>

    <div class="box_footer text-right">
        <md-button class="cancel" ng-click="ctrl.close()">
            <?= \Yii::t('UI14.3.1', '$LANGUAGE_BOX_CANCEL_BTN$') ?>
        </md-button>
        <md-button class="send" ng-click="ctrl.save()">
            <?= \Yii::t('UI14.3.1', '$LANGUAGE_BOX_SAVE_BTN$') ?>
        </md-button>
    </div>
</div>