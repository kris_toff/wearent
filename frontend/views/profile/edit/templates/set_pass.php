<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 30.09.2017
 * Time: 13:44
 */

?>

<div id="newpass_panel_box" ng-form="setPasswordForm">
    <h4>Укажите пароль</h4>

    <p>Ранее вы регистрировались используя социальные сети. Сейчас нужно дополнительно задать пароль для возможности
        использовать почту <b>{{$ctrl.email}}</b> для входа на сайт.</p>

    <md-input-container class="md-block" md-no-float="1">
        <label for="sourcePassword">пароль</label>
        <input id="sourcePassword"
                type="password"
                name="sp"
                placeholder="password"
                minlength="8"
                maxlength="45"
                required="required"
                ng-model="rack.sp"
                ng-model-options="{updateOn: 'blur'}"
                md-no-asterisk="1"
                ui-validate="'checkReliablePass($modelValue)'"
        />

        <div ng-messages="setPasswordForm.sp.$error" role="alert">
            <div ng-message="required">this field required</div>
            <div ng-message="minlength">password must contains at least 8 chars</div>
            <div ng-message="validator">must be at least upper char, digit and spesial symbol</div>
        </div>
    </md-input-container>

    <md-input-container class="md-block" md-no-float="1">
        <label for="repeatPassword">повторите пароль</label>
        <input id="repeatPassword"
                type="password"
                name="rp"
                placeholder="repeat pass"
                required="required"
                ng-model="rack.rp"
                ng-model-options="{updateOn: 'blur'}"
                ui-validate="'$modelValue===rack.sp'"
                ui-validate-watch="'rack.sp'"
                md-no-asterisk="1"
        />

        <div ng-messages="setPasswordForm.rp.$error" role="alert">
            <div ng-message="required">this field required</div>
            <div ng-message="validator">compate input failed</div>
        </div>
    </md-input-container>

    <md-button ng-click="submitSetPassForm()">save</md-button>
    <md-button ng-click="close()">close</md-button>
</div>
