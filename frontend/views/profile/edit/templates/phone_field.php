<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 08.09.2017
 * Time: 10:58
 */

use yii\helpers\Html;

/** @var \yii\web\View $this */

?>

<md-input-container>
    <label class="control-label" for="{{'phonefield-' + $ctrl.model.id}}"><?= \Yii::t('UI14.4',
            '$FIELD_PHONE_LABEL$') ?></label>

    <div layout="row">
        <div class="input-box">
            <md-icon ng-if="!+$ctrl.model.is_verify && !$ctrl.model.is_new"
                    class="email_verify_icon"
                    md-svg-src="/img/editProfile/statusIcon/verification_icon.svg">
                <md-tooltip><?= \Yii::t('UI14.4', '$FIELD_PHONE_ICON_WARNING_HINT$') ?></md-tooltip>
            </md-icon>
            <md-icon ng-if="+$ctrl.model.is_verify"
                    class="email_verify_icon"
                    md-svg-src="/img/editProfile/statusIcon/ok_icon.svg">
                <md-tooltip><?= \Yii::t('UI14.4', '$FIELD_PHONE_ICON_SUCCESS_HINT$') ?></md-tooltip>
            </md-icon>
            <md-icon ng-if="0" class="email_verify_icon" md-svg-src="/img/editProfile/statusIcon/errorIcon.svg">
            </md-icon>

            <input id="{{'phonefield-' +$ctrl.model.id}}"
                    type="text"
                    ng-model="data.phone"
                    ng-model-options="{debounce: {default: 650, blur: 0}}"
                    ng-readonly="+$ctrl.model.is_verify"
                    ng-disabled="+$ctrl.model.is_verify"
                    placeholder="<?= \Yii::t('UI14.4', '$FIELD_PHONE_PLACEHOLDER$') ?>"
                    name="{{'phonefield' + $ctrl.model.id}}"
                    required="1"
                    md-no-asterisk="1"
                    ui-validate-async="'uniquePhone($modelValue)'"

                    uib-popover-html="'<?= \Yii::t('UI14.4', '$FIELD_PHONE_ERROR_REQUIRED$') ?>'"
                    popover-trigger="{'focus': 'blur'}"
                    popover-placement="top-left"
                    popover-append-to-body="1"
            />
        </div>

            <md-button class="md-icon-button" ng-click="removeItem()">
                <md-tooltip><?= \Yii::t('UI14.4',
                        '$FIELD_PHONE_DELETE_BTN_HINT$') ?></md-tooltip>
                <md-icon md-svg-src="/img/editProfile/svgForProfile/delete_icon.svg"></md-icon>
            </md-button>
            <md-button class="md-icon-button lockBtn" ng-click="toggleLock()" ng-switch="+data.is_public">
                <md-tooltip ng-switch-when="1">
                    <?= \Yii::t('UI14.4',
                        '$FIELD_PHONE_OPEN_STATUS_BTN_HINT$') ?>

                </md-tooltip>
                <md-icon ng-switch-when="1" md-svg-src="/img/editProfile/svgForProfile/lockOpen.svg">
                </md-icon>

                <md-tooltip ng-switch-when="0">
                    <?= \Yii::t('UI14.4', '$FIELD_PHONE_CLOSE_STATUS_BTN_HINT$') ?>
                </md-tooltip>

                <md-icon ng-switch-when="0" md-svg-src="/img/icons/ic_visibility_off.svg"></md-icon>
            </md-button>

    </div>
    <div ng-messages="getField().$error" role="alert" ng-if="getField().$invalid">
        <div class="row">
            <div class="col-sm-6">
                <div ng-message="minlength"><?= \Yii::t('UI14.4', '$FIELD_PHONE_ERROR_MINLENGTH$', ['n' => 6]) ?></div>
<!--                <div ng-message="required">--><?//= \Yii::t('UI14.4', '$FIELD_PHONE_ERROR_REQUIRED$') ?><!--</div>-->
                <div ng-message="validatorAsync"><?= \Yii::t('UI14.4',
                        '$FIELD_PHONE_ALREADY_EXISTS') ?></div>
            </div>
        </div>
    </div>

    <div class="verificationCode" ng-hide="+$ctrl.model.is_verify || getField().$invalid">
        <a ng-click="save($event)"><?= \Yii::t('UI14.4',
                '$FIELD_PHONE_SAVE_AND_SEND_VERIFY_CODE_LINK$') ?></a>
    </div>
    <div class="socialCheck">
        <?= \Yii::t('UI14.4', '$FIELD_PHONE_MESSENGER_LABEL$') ?>

        <div layout="column" layout-gt-sm="row" layout-wrap dir="rtl">
            <md-checkbox ng-model="data.is_viber" ng-true-value="1" ng-false-value="0" ng-change="saveInfo()">
                <md-icon md-svg-src="/img/editProfile/socialIcon/viber.svg"></md-icon>
                Viber
            </md-checkbox>

            <md-checkbox ng-model="data.is_whatsapp" ng-true-value="1" ng-false-value="0" ng-change="saveInfo()">
                <md-icon md-svg-src="/img/editProfile/socialIcon/whatsup.svg"></md-icon>
                Whats app
            </md-checkbox>

            <md-checkbox ng-model="data.is_telegram" ng-true-value="1" ng-false-value="0" ng-change="saveInfo()">
                <md-icon md-svg-src="/img/editProfile/socialIcon/telegram.svg"></md-icon>
                Telegram
            </md-checkbox>
        </div>
    </div>
</md-input-container>
