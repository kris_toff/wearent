<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.09.2017
 * Time: 15:58
 */

use yii\helpers\Html;

/** @var \yii\web\View $this */
?>

<md-input-container ng-class="{main: +$ctrl.model.is_main}">
    <label class="control-label" for="{{'emailfield-' + $ctrl.model.id}}">
        <?= \Yii::t('UI14.4', '$FIELD_EMAIL_LABEL$') ?>
    </label>

    <div layout="row" layout-align="start start">
        <div class="wrappEmailInput">
            <div class="input-box">
                <md-icon ng-if="+$ctrl.model.is_verify"
                         class="email_verify_icon"
                         md-svg-src="/img/editProfile/statusIcon/ok_icon.svg">
                    <md-tooltip><?= \Yii::t('UI14.4', '$FIELD_EMAIL_ICON_SUCCESS_HINT$') ?></md-tooltip>
                </md-icon>
                <md-icon ng-if="!+$ctrl.model.is_verify && !+$ctrl.model.is_new"
                         class="email_verify_icon"
                         md-svg-src="/img/editProfile/statusIcon/verification_icon.svg">
                    <md-tooltip><?= \Yii::t('UI14.4', '$FIELD_EMAIL_ICON_WARNING_HINT$') ?></md-tooltip>
                </md-icon>

                <input id="{{'emailfield-' + $ctrl.model.id}}"
                       ng-readonly="+$ctrl.model.is_verify"
                       ng-disabled="+$ctrl.model.is_verify"
                       type="email"
                       name="{{'emailField' + $ctrl.index}}"
                       ng-model="data.email"
                       ng-model-options="{debounce: {default: 650, blur: 0}}"
                       placeholder="<?= \Yii::t('UI14.4', '$FIELD_EMAIL_PLACEHOLDER$') ?>"
                       required="1"
                       md-no-asterisk="1"
                       ui-validate-async="'uniqueEmail($modelValue)'"

                       uib-popover-html="'<?= \Yii::t('UI14.4', '$FIELD_EMAIL_ERROR_REQUIRED$') ?>'"
                       popover-trigger="{'focus': 'blur'}"
                       popover-placement="top-left"
                       popover-append-to-body="1"
                />
            </div>

            <md-button class="md-icon-button" ng-click="removeItem()" ng-hide="+$ctrl.model.is_main">
                <md-tooltip><?= \Yii::t('UI14.4',
                        '$FIELD_EMAIL_DELETE_BTN_HINT$') ?></md-tooltip>
                <md-icon md-svg-src="/img/editProfile/svgForProfile/delete_icon.svg"></md-icon>
            </md-button>
            <md-icon md-svg-src="/img/icons/ic_visibility_off.svg">
                <md-tooltip><?= \Yii::t('UI14.4', '$FIELD_EMAIL_EYE_HINT$') ?></md-tooltip>
            </md-icon>
        </div>



        <span class="defoltLogin" ng-show="+$ctrl.model.is_main"><?= \Yii::t('UI14.4',
                '$FIELD_EMAIL_SIGNED_ACTUAL_ACCOUNT$') ?></span>

        <a class="makeMain"
           ng-hide="+$ctrl.model.is_main || isDirty() || !+$ctrl.model.is_verify"
           ng-click="setLogin()">
            <?= \Yii::t('UI14.4', '$FIELD_EMAIL_SET_ACCOUNT$') ?>
        </a>

    </div>


    <div ng-messages="getField().$error" role="alert" ng-if="getField().$invalid">
        <div class="row">
            <div class="col-sm-6">
                <div ng-message="email"><?= \Yii::t('UI14.4', '$FIELD_EMAIL_ERROR_WRONG_EMAIL$') ?></div>
                <!--                <div ng-message="required">-->
                <? //= \Yii::t('UI14.4', '$FIELD_EMAIL_ERROR_REQUIRED$') ?><!--</div>-->
                <div ng-message="validatorAsync"><?= \Yii::t('UI14.4', '$FIELD_EMAIL_ERROR_ALREADY_EXISTS$') ?></div>
            </div>
        </div>
    </div>

    <a
            class="saveBtn"
            ng-show="isDirty() || +$ctrl.model.is_new"
            ng-click="saveEmail()">
        <?= \Yii::t('UI14.4', '$FIELD_EMAIL_SAVE_AND_SEND_VERIFY_BTN$') ?>
    </a>
</md-input-container>

