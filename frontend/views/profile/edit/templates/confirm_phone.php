<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.09.2017
 * Time: 17:41
 */

/** @var \yii\web\View $this */

?>

<div role="dialog" class="confirmPhoneWindow">
    <h4 class="hidden visible-xs titleWindow">
        <?= \Yii::t('UI14.4', '$FIELD_PHONE_CONFIRM_BOX_MOBILE_CODE_HEADER$') ?>
        <md-button class="md-icon-button closeWindow" ng-click="close()">
            <md-icon
                    md-svg-src="/img/editProfile/mobile/closeIcon.svg"></md-icon>
        </md-button>

    </h4>

    <div class="contentConfirm" ng-form="confirmPhoneForm">
        <md-input-container md-no-float="1" class="md-block styleInput">
            <label class="control-label" for="confirm_phone_box_code"><?= \Yii::t('UI14.4',
                    '$FIELD_PHONE_CONFIRM_BOX_CODE_LABEL$') ?></label>
            <input type="text"
                    name="confirm_code"
                    id="confirm_phone_box_code"
                    placeholder="<?= \Yii::t('UI14.4', '$FIELD_PHONE_CONFIRM_BOX_CODE_PLACEHOLDER$') ?>"
                    minlength="6"
                    maxlength="6"
                    ng-model="code"
                    ng-model-options="{debounce: {default: 650, blur: 0}, updateOn: 'blur'}"
                    ng-keydown="apply($event)"
                    ui-mask-placeholder=""
                    ui-mask="999999"
                    ui-mask-placeholder-char="space"
                    ui-options="{clearOnBlur: 0, clearOnBlurPlaceholder: 1, allowInvalidValue: 1}"
            />

            <div ng-messages="confirmPhoneForm.confirm_code.$error">
                <div ng-message="minlength"><?= \Yii::t('UI14.4', '$FIELD_PHONE_CONFIRM_BOX_ERROR_WRONG_LENGTH_CODE$') ?></div>
                <div ng-message="wrong_code"><?= \Yii::t('UI14.4', '$FIELD_PHONE_CONFIRM_BOX_ERROR_WRONG_CODE$') ?></div>
            </div>
        </md-input-container>

        <div ng-switch="isActiveResend()"
                ng-init="resendText = '<?= \Yii::t('UI14.4',
                    '$FIELD_PHONE_CONFIRM_BOX_RESEND_CODE_BTN$') ?>';remainingText = '<?= \Yii::t('UI14.4',
                    '$FIELD_PHONE_CONFIRM_BOX_RESEND_CODE_REMAINING_TIME$') ?>';">
            <span ng-switch-when="0">{{resendText + ' ' + $ctrl.remaining()}}</span>
            <a ng-click="resendSms()" ng-switch-when="1">{{resendText}}</a>
        </div>

        <md-button class="btn btn-block sendBtn" ng-click="apply()"><?= \Yii::t('UI14.4',
                '$FIELD_PHONE_CONFIRM_BOX_APPLY_CODE_BTN$') ?></md-button>
    </div>
</div>
