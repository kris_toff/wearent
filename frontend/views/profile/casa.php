<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 09.06.2017
 * Time: 15:58
 */

use yii\bootstrap\Html;

/** @var \yii\web\View $this */

\yii\bootstrap\BootstrapPluginAsset::register($this);
\frontend\assets\ReadmoreAsset::register($this);
\frontend\assets\SliderAsset::register($this);
\frontend\assets\ProductAsset::register($this);
\frontend\assets\yyAsset::register($this);

?>

<div class="marg">

</div>
<div class="row casa">
    <div class="container sidebarEditLeft">
        <div class="row">
            <div class="col-sm-12">
                <?= $this->render('left-menu') ?>
            </div>
        </div>
    </div>


    <div class="container content">
        <div class="row">
            <div class="col-sm-12">
                <div class="titleEdit">
                    <h4 class="text-left">Касса</h4>
                </div>


                <div class="wrapPanel">
                    <div role="tabpanel" class="panelCasa">

                        <!-- Навігаційні елементи вкладок -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active text-center"><a href="#balance"
                                                                                  aria-controls="balance" role="tab"
                                                                                  data-toggle="tab">Баланс в системе</a>
                            </li>
                            <li role="presentation" class="text-center"><a href="#transactions"
                                                                           aria-controls="transactions" role="tab"
                                                                           data-toggle="tab">История транзакций</a></li>

                            <li role="presentation" class="text-center"><a href="#cupon" aria-controls="cupon"
                                                                           role="tab" data-toggle="tab">Ваши купоны</a>
                            </li>

                        </ul>

                        <!-- Вкладки панелі -->
                        <div class="tab-content">
                            <!--                            first panel-->
                            <div role="tabpanel" class="tab-pane active" id="balance">
                                <div class="row">
                                    <div class="col-md-3 col-sm-6">
                                        <?= $this->render('manu-media') ?>
                                        <?= $this->render('manu-media') ?>
                                        <?= $this->render('manu-media') ?>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <?= $this->render('manu-media') ?>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <?= $this->render('manu-media') ?>
                                        <?= $this->render('manu-media') ?>
                                        <?= $this->render('manu-media') ?>
                                        <?= $this->render('manu-media') ?>
                                    </div>
                                    <div class="col-md-3 col-sm-6 twoBtn">
                                        <a href="#" class="btn btn-block btn-success">Пополнить счет</a>
                                        <a href="#" class="btn btn-block btn-danger">Вывести средства</a>
                                    </div>
                                </div>

                                <div class="bottomBlock">
                                    <h5>Платежные инструменты:</h5>

                                    <?= $this->render('card-media') ?>
                                    <?= $this->render('card-media') ?>


                                    <div class="card cardAdd">
                                        <div class="cardLeft">
                                            <p class="text-center">Добавить новую</p>
                                        </div>

                                    </div>
                                </div>


                            </div>


                            <div role="tabpanel" class="tab-pane transactions" id="transactions">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <th>Дата</th>
                                            <th>Категория</th>
                                            <th>Статус</th>
                                            <th>Price</th>
                                            <th>Pce</th>
                                        </tr>
                                        <tr>
                                            <td class="data">1 августа, 2016 <br><span>10:23</span></td>
                                            <td class="type">Оплата Бизнес аккаунта</td>
                                            <td class="statusBlue">В обработке</td>
                                            <td class="priseRed">- $100</td>
                                            <td class="inf">
                                                <svg class="infImg" width="17px" height="17px" viewBox="0 0 17 17">
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="infImg" transform="translate(-1068.000000, -837.000000)" fill-rule="nonzero">
                                                            <g id="FORM" transform="translate(903.000000, 624.000000)">
                                                                <g id="Group-16" transform="translate(20.000000, 213.000000)">
                                                                    <g id="Group-14">
                                                                        <g id="Group-3" transform="translate(145.000000, 0.000000)">
                                                                            <path d="M14.2,13.4 C15.5,12 16.2,10.1 16.2,8.1 C16.2,3.6 12.6,0 8.1,0 C3.6,0 0,3.6 0,8.1 C0,12.6 3.6,16.2 8.1,16.2 C10.2,16.2 12,15.4 13.4,14.2 L14.2,13.4 Z M1,8 C1,4.1 4.2,0.9 8.1,0.9 C12,0.9 15.2,4.1 15.2,8 C15.2,11.9 12,15.1 8.1,15.1 C4.2,15.1 1,12 1,8 Z" id="Shape"></path>
                                                                            <path d="M9.5,11.2 L8.6,11.2 L8.6,7.4 C8.6,7.1 8.4,6.9 8.1,6.9 L6.8,6.9 C6.5,6.9 6.3,7.1 6.3,7.4 C6.3,7.7 6.5,7.9 6.8,7.9 L7.6,7.9 L7.6,11.2 L6.8,11.2 C6.5,11.2 6.3,11.4 6.3,11.7 C6.3,12 6.5,12.2 6.8,12.2 L9.4,12.2 C9.7,12.2 9.9,12 9.9,11.7 C9.9,11.4 9.7,11.2 9.5,11.2 Z" id="Shape"></path>
                                                                            <circle id="Oval" cx="7.9" cy="4.4" r="1.1"></circle>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="data">1 августа, 2016 <br><span>10:23</span></td>
                                            <td class="type">Оплата Бизнес аккаунта</td>
                                            <td class="statusBlue">В обработке</td>
                                            <td class="priseRed">- $100</td>
                                            <td class="inf">
                                                <svg class="infImg" width="17px" height="17px" viewBox="0 0 17 17">
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="infImg" transform="translate(-1068.000000, -837.000000)" fill-rule="nonzero">
                                                            <g id="FORM" transform="translate(903.000000, 624.000000)">
                                                                <g id="Group-16" transform="translate(20.000000, 213.000000)">
                                                                    <g id="Group-14">
                                                                        <g id="Group-3" transform="translate(145.000000, 0.000000)">
                                                                            <path d="M14.2,13.4 C15.5,12 16.2,10.1 16.2,8.1 C16.2,3.6 12.6,0 8.1,0 C3.6,0 0,3.6 0,8.1 C0,12.6 3.6,16.2 8.1,16.2 C10.2,16.2 12,15.4 13.4,14.2 L14.2,13.4 Z M1,8 C1,4.1 4.2,0.9 8.1,0.9 C12,0.9 15.2,4.1 15.2,8 C15.2,11.9 12,15.1 8.1,15.1 C4.2,15.1 1,12 1,8 Z" id="Shape"></path>
                                                                            <path d="M9.5,11.2 L8.6,11.2 L8.6,7.4 C8.6,7.1 8.4,6.9 8.1,6.9 L6.8,6.9 C6.5,6.9 6.3,7.1 6.3,7.4 C6.3,7.7 6.5,7.9 6.8,7.9 L7.6,7.9 L7.6,11.2 L6.8,11.2 C6.5,11.2 6.3,11.4 6.3,11.7 C6.3,12 6.5,12.2 6.8,12.2 L9.4,12.2 C9.7,12.2 9.9,12 9.9,11.7 C9.9,11.4 9.7,11.2 9.5,11.2 Z" id="Shape"></path>
                                                                            <circle id="Oval" cx="7.9" cy="4.4" r="1.1"></circle>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="data">1 августа, 2016 <br><span>10:23</span></td>
                                            <td class="type">Оплата Бизнес аккаунта</td>
                                            <td class="statusBlue">В обработке</td>
                                            <td class="priseRed">- $100</td>
                                            <td class="inf">
                                                <svg class="infImg" width="17px" height="17px" viewBox="0 0 17 17">
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="infImg" transform="translate(-1068.000000, -837.000000)" fill-rule="nonzero">
                                                            <g id="FORM" transform="translate(903.000000, 624.000000)">
                                                                <g id="Group-16" transform="translate(20.000000, 213.000000)">
                                                                    <g id="Group-14">
                                                                        <g id="Group-3" transform="translate(145.000000, 0.000000)">
                                                                            <path d="M14.2,13.4 C15.5,12 16.2,10.1 16.2,8.1 C16.2,3.6 12.6,0 8.1,0 C3.6,0 0,3.6 0,8.1 C0,12.6 3.6,16.2 8.1,16.2 C10.2,16.2 12,15.4 13.4,14.2 L14.2,13.4 Z M1,8 C1,4.1 4.2,0.9 8.1,0.9 C12,0.9 15.2,4.1 15.2,8 C15.2,11.9 12,15.1 8.1,15.1 C4.2,15.1 1,12 1,8 Z" id="Shape"></path>
                                                                            <path d="M9.5,11.2 L8.6,11.2 L8.6,7.4 C8.6,7.1 8.4,6.9 8.1,6.9 L6.8,6.9 C6.5,6.9 6.3,7.1 6.3,7.4 C6.3,7.7 6.5,7.9 6.8,7.9 L7.6,7.9 L7.6,11.2 L6.8,11.2 C6.5,11.2 6.3,11.4 6.3,11.7 C6.3,12 6.5,12.2 6.8,12.2 L9.4,12.2 C9.7,12.2 9.9,12 9.9,11.7 C9.9,11.4 9.7,11.2 9.5,11.2 Z" id="Shape"></path>
                                                                            <circle id="Oval" cx="7.9" cy="4.4" r="1.1"></circle>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <a href="#" class="btn btn-block showTransactions">Посмотреть еще 10 транзакций</a>
                            </div>

                            <div role="tabpanel" class="tab-pane cupon" id="cupon">Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit. Dignissimos earum libero molestias mollitia nam possimus quas quod
                                sequi sunt temporibus! Assumenda aut enim perspiciatis? At ex illum inventore itaque
                                obcaecati.
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
