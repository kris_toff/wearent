<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 29.05.2017
 * Time: 13:44
 */

use yii\helpers\Html;

/** @var \yii\web\View $this */


\frontend\assets\yyAsset::register($this);
\yii\bootstrap\BootstrapPluginAsset::register($this);
\frontend\assets\RateAsset::register($this);
\frontend\assets\ScrollAsset::register($this);
?>


<div class="row wrapp">
    <div class="container">
        <div class="row service">
            <h2 class="text-center">Service</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis consectetur dolorum, earum et fugit hic laudantium nam quis repellat? Corporis doloribus enim eum fuga magni reprehenderit? Fuga, illo, incidunt! Adipisci amet consectetur dignissimos dolor ducimus ea, earum eligendi excepturi facilis fugiat illo ipsam iste libero modi, necessitatibus nemo odit recusandae repellat reprehenderit voluptatibus. A at autem, cupiditate dicta doloribus dolorum ducimus eius eligendi ex, excepturi hic illo laudantium maiores modi molestiae non nulla perferendis provident quidem quo saepe sit ullam, vel voluptas voluptatibus. Ad aliquid blanditiis consequuntur corporis cum cumque deleniti deserunt dolorem dolores dolorum ex facere fuga, id inventore ipsam iste iure laboriosam laborum libero nam necessitatibus neque nesciunt numquam odio officiis pariatur possimus quis quod, ratione sapiente sed sequi similique sunt unde ut vel voluptatum? Culpa ex, impedit odit praesentium quaerat recusandae? Dolorem iste natus non tempore! Culpa fugit labore minima modi numquam officiis quam quo rem repellat, repellendus. At consectetur est et eveniet excepturi, expedita hic incidunt ipsa laudantium maxime mollitia non provident quae reiciendis sed, similique veniam. Ab aspernatur cum ea eum ex fugiat fugit illum ipsam laudantium modi molestias nam nemo nihil, nostrum, obcaecati possimus quae repellendus saepe sequi sit veniam veritatis vero voluptas? At deleniti dolor dolore doloremque eos id illo ipsam iusto labore libero nemo nulla odit, quaerat quos similique, tempore veritatis. Autem est hic ipsam mollitia perferendis placeat quos tenetur vel veniam vitae. Commodi culpa debitis distinctio dolores doloribus ea eius, eligendi eos est impedit ipsum minima molestiae nulla optio praesentium quidem, tempora totam vero! Debitis, ipsa, quos. A aliquid beatae culpa cupiditate dolores ea eius expedita fugiat hic incidunt ipsa itaque iure laborum maiores molestias natus nemo nesciunt nisi non odio odit, optio perspiciatis porro praesentium quos reiciendis rem sapiente sit sunt tempora tempore unde ut voluptates. Aliquid amet, animi at consequuntur dolorum earum error eum fugit id in incidunt inventore libero, nemo nisi officia quae quam quia quos repellat repellendus repudiandae rerum sed sunt tempora totam vero voluptates. Eligendi excepturi fuga odio saepe similique! Aliquid aperiam architecto consequatur culpa cumque debitis delectus deleniti dignissimos distinctio dolorem dolores earum eligendi esse expedita fugiat harum hic, id illum ipsam magni nam natus nemo, neque perferendis perspiciatis placeat, porro quasi quisquam rem repellendus repudiandae rerum sapiente sed sequi sint ullam voluptas. Blanditiis exercitationem, ipsum iste laboriosam libero natus quaerat saepe voluptates. Accusantium aliquam animi atque consectetur culpa debitis delectus dignissimos dolor dolore dolores doloribus, ea exercitationem illum impedit in molestias nam nulla odit perspiciatis quae quam quis reiciendis sequi sit suscipit temporibus ut voluptate. Alias aut dignissimos eius fugiat, illo ipsum iste laudantium perspiciatis possimus quo tempore vel voluptate. Consectetur deleniti eaque fuga. Accusamus, aspernatur culpa, dolore eius fuga fugiat magnam nihil praesentium, quidem quis repellat similique temporibus unde. Ab aut distinctio, eaque, ex fuga ipsam maxime minus molestiae nam nihil nobis provident quia quos repellendus tempora veritatis voluptates. Dolor in nemo quasi quod ut voluptates? Animi, consequatur deleniti esse ex id ipsum necessitatibus neque nostrum quaerat quam repudiandae saepe sapiente sequi voluptate?</p>
        </div>
    </div>
</div>
