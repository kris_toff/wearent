<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 15.04.2017
 * Time: 21:47
 */

use yii\bootstrap\Html;

/** @var \yii\web\View $this */
/** @var array $model */
/** @var integer $key */
/** @var integer $index */
/** @var \yii\widgets\ListView $widget */

$product = \yii\helpers\ArrayHelper::getValue($model, 'product', $model);
?>

<div class="col-md-6 col-lg-4">
    <a class="thumbnail" href="/cargo/<?= $model['slug'] ?>">
        <?= Html::img(\yii\helpers\ArrayHelper::getValue($product, 'photo.0.link', '#')) ?>
        <div class="caption">
            <h3><?= $model['name'] ?></h3>
        </div>
    </a>
</div>
