<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 15.04.2017
 * Time: 14:36
 */

use yii\widgets\ListView;
use yii\bootstrap\Html;

/** @var \yii\web\View $this */
/** @var \common\models\Product[] $productProvider */
/** @var \backend\models\ProductAdvertisement[] $topProductProvider */
/** @var \yiimodules\categories\models\Categories $category */

\yiimodules\categories\ModuleAsset::register($this);
\frontend\assets\yyAsset::register($this);
\yii\bootstrap\BootstrapPluginAsset::register($this);
\frontend\assets\SlickAsset::register($this);

$this->title = Html::encode($category->meta_title ?: $category->name);
$this->registerMetaTag([
    'name'    => 'description',
    'content' => $category->meta_description ?: $category->description,
]);
$this->registerMetaTag([
    'name'    => 'keywords',
    'content' => $category->meta_keywords,
]);

?>

    <div class="row head wrapp">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div id="categories-jstree">
                        <?= \yiimodules\categories\models\Categories::createTreeList(null, $category->id); ?>
                    </div>
                </div>
                <div class="col-sm-9">
                    <h1><?= Html::encode($category->meta_title ?: $category->name) ?></h1>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                TOP
                            </div>
                        </div>
                        <div class="panel-body">
                            <?= ListView::widget([
                                'dataProvider' => $topProductProvider,
                                'itemView'     => '_product-item',
                                'itemOptions'  => [
                                    'class' => 'top-item',
                                ],
                            ]) ?>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                Products
                            </div>
                        </div>
                        <div class="panel-body">
                            <?= ListView::widget([
                                'dataProvider' => $productProvider,
                                'itemView'     => '_product-item',
                                'viewParams'   => [
                                    'isTop' => true,
                                ],
                            ]) ?>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>


<?php
$this->registerJs(<<<JS
    var el = $("#categories-jstree");
    el.jstree();
    el.on("select_node.jstree", function(e, data){
        var url = data.node.a_attr.href;
        var params = url.match(/^.+(\?[^?]*)$/i);
        window.location.href = "/catalog"+params[1];
    });
JS
    , \yii\web\View::POS_READY);

$this->registerJsFile('/js/grom.js', [
    'position' => \yii\web\View::POS_END,
    'depends'  => [
        'frontend\assets\AppAsset',
    ],
]);