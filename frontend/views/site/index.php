<?php


use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \yiimodules\categories\models\Categories[] $categories список категорий */
/** @var \common\models\Product[] $viewed_products список недавно просмотренных товаров */
/** @var \common\models\Product[] $team_selection */
/** @var array $sliderLine1 */
/** @var array $sliderLine2 */
/** @var array $sliderAmazing */
/** @var integer $product_count */

\frontend\assets\yyAsset::register($this);
\yii\bootstrap\BootstrapPluginAsset::register($this);
\frontend\assets\RateAsset::register($this);
\frontend\assets\ScrollAsset::register($this);
\frontend\assets\FrontendAsset::register($this);
\frontend\assets\ScrollAsset::register($this);
\frontend\assets\CheckboxAsset::register($this);
\frontend\assets\DateRangePickerAsset::register($this);


$searchForm = \yii\helpers\ArrayHelper::getValue($this->params, 'searchForm', new \frontend\models\ProductSearch());


$a = \frontend\models\CategoryList::tree();

if (\Yii::$app->user->isGuest && is_null(\Yii::$app->request->userIP)) {
    $dataProviderHistory = new \yii\data\ArrayDataProvider(['allModels' => []]);
} else {
    $dataProviderHistory = new \yii\data\ActiveDataProvider([
        'key' => function ($model) {
            return md5("{$model->user_id}={$model->ip}={$model->created_at}");
        },
        'query' => \frontend\models\HistorySearch::find()
            ->filterWhere(['user_id' => \Yii::$app->user->isGuest ? null : \Yii::$app->user->id])
            ->orFilterWhere(['ip' => \Yii::$app->request->userIP]),
        'pagination' => [
            'defaultPageSize' => 6,
            'page' => 0,
            'forcePageParam' => false,
            'pageSize' => 6,
            'validatePage' => true,
        ],
        'sort' => [
            'defaultOrder' => [
                'created_at' => SORT_DESC,
                'user_id' => SORT_DESC,
            ],
            'enableMultiSort' => true,
        ],
    ]);
}
//print_r( $dataProviderHistory->getModels() );exit;

$listPopup = [

    1 => [
        'id' => 1,
        'name' => 'Музыка',
        'img' => '/img/category/music.svg',
    ],

    3 => [
        'id' => 3,
        'name' => 'Спорт, Отдых',
        'node' => [
            101 => [
                'id' => 3301,
                'name' => 'Велосипеды, гироскутеры, ролики, скейты',
            ],
            1011 => [
                'id' => 3302,
                'name' => 'Лыжи, коньки, санки',
            ],
            102 => [
                'id' => 3302,
                'name' => 'Пикник, кемпинг, пляж',
            ],
            103 => [
                'id' => 103,
                'name' => 'Туризм, путешествия',
                'node' => [
                    3320 => [
                        'id' => 3332,
                        'name' => 'Велосипеды, гироскутеры, ролики, скейты',
                    ],
                    3321 => [
                        'id' => 3333,
                        'name' => 'Лыжи, коньки, санки',
                    ],
                    3322 => [
                        'id' => 3334,
                        'name' => 'Пикник, кемпинг, пляж',
                    ],
                    3323 => [
                        'id' => 3335,
                        'name' => 'Туризм, путешествия',
                    ],
                    3324 => [
                        'id' => 3336,
                        'name' => 'Спортивные игры',
                    ],
                    3325 => [
                        'id' => 3337,
                        'name' => 'Дайвинг, водный спорт',
                    ],
                    3326 => [
                        'id' => 3338,
                        'name' => 'Услуги инструкторов',
                    ],
                    3327 => [
                        'id' => 3339,
                        'name' => 'Аренда спортивных помещений',
                    ],
                    3328 => [
                        'id' => 3340,
                        'name' => 'Картинг',
                    ],
                    3329 => [
                        'id' => 3341,
                        'name' => 'Тренажеры, фитнес, единоборства',
                    ],
                    3330 => [
                        'id' => 3342,
                        'name' => 'Пневматика и снаряжение',
                    ],

                ],
            ],
            104 => [
                'id' => 3304,
                'name' => 'Спортивные игры',
            ],
            105 => [
                'id' => 3305,
                'name' => 'Дайвинг, водный спорт',
            ],
            106 => [
                'id' => 3306,
                'name' => 'Услуги инструкторов',
            ],
            107 => [
                'id' => 3307,
                'name' => 'Аренда спортивных помещений',
            ],
            108 => [
                'id' => 3308,
                'name' => 'Картинг',
            ],
            109 => [
                'id' => 3309,
                'name' => 'Тренажеры, фитнес, единоборства',
            ],
            1010 => [
                'id' => 3310,
                'name' => 'Пневматика и снаряжение',
            ],

        ],
        'img' => '/img/category/backpack.svg',
    ],

    4 => [
        'id' => 331,
        'name' => 'Фото, Видео',
        'img' => '/img/category/foto.svg',
    ],

    5 => [
        'id' => 332,
        'name' => 'Праздники и события',
        'img' => '/img/category/music.svg',
    ],

    6 => [
        'id' => 333,
        'name' => 'Инструменты и спец. техника',
        'img' => '/img/category/music.svg',
    ],

    2 => [
        'id' => 334,
        'name' => 'Детские товары',
        'img' => '/img/category/music.svg',
    ],

    7 => [
        'id' => 334,
        'name' => 'Авто Мото',
        'img' => '/img/category/auto.svg',
    ],

    8 => [
        'id' => 335,
        'name' => 'Красота и здоровье',
        'img' => '/img/category/music.svg',
    ],

    9 => [
        'id' => 336,
        'name' => 'Бизнес',
        'img' => '/img/category/tv.svg',
    ],

    10 => [
        'id' => 337,
        'name' => 'Одежда, обувь, акссесуары',
        'img' => '/img/category/stuff.svg',
    ],

    11 => [
        'id' => 337,
        'name' => 'Бытовая техника',
        'img' => '/img/category/drill.svg',
    ],

    12 => [
        'id' => 337,
        'name' => 'Все для дома',
        'img' => '/img/category/bett.svg',
    ],

    13 => [
        'id' => 337,
        'name' => 'Дача, сад',
        'img' => '/img/category/music.svg',
    ],


];
?>
    <div class="contentBody" ng-controller="HomePageController as HPC">
        <div class="row head">
            <div class="container">
                <div class="row body">
                    <div class="title">
                        <div class="col-sm-12">
                            <h1 class="text-center"><span>Wearent</span> <?= \Yii::t('UI2', '$MAIN_SLOGAN$') ?></h1>
                            <h3 class="text-center"><?= \Yii::t('UI2', '$UNDER_SLOGAN_TEXT$') ?></h3>
                        </div>
                    </div>

                    <div class="col-sm-12 headSearch hidden-sm hidden-xs" ng-controller="SearchBarController as SBC">
                        <?= Html::beginForm(['/search'], 'get', ['autocomplete' => 'off', 'class' => 'titleSearch']) ?>
                        <div class="left" style="position: relative;">
                            <div class="leftBlockLeft">
                                <?= Html::activeHiddenInput($searchForm, 'category_id', ['ng-value' => 'categoryId']) ?>

                                <a href="#" class="aTopLeft "><strong><?= \Yii::t('UI2',
                                            '$SEARCH_BAR_FIELD_WHAT_LABEL$') ?></strong></a>
                                <?= Html::activeTextInput($searchForm, 'note', [
                                    'id' => 'whoSearchLeft',
                                    'class' => 'inputSearchHeadLeft focusInput1',
                                    'placeholder' => \Yii::t('UI2', '$SEARCH_BAR_FIELD_WHAT_PLACEHOLDER$'),
                                    'ng-keypress' => 'enterWord($event)',
                                    'ng-focus' => 'isWhatFocus = true',
                                    'ng-blur' => 'isWhatFocus = false',
                                ]) ?>
                            </div>

                            <div class="rightBlockLeft">
                                <a href="#" onclick="return false;" class="btnBig">
                                    <svg width="14px" height="14px" viewBox="0 0 8 14" version="1.1">
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"
                                           stroke-linecap="round" stroke-linejoin="round">
                                            <g id="btnLeft" transform="translate(-1207.000000, -591.000000)"
                                               stroke-width="2">
                                                <g id="Group-11" transform="translate(0.000000, 579.000000)">
                                                    <g id="Recommended">
                                                        <g id="Lenta" transform="translate(210.000000, 0.000000)">
                                                            <g id="arrows" transform="translate(927.000000, 1.000000)">
                                                                <g id="Group-37"
                                                                   transform="translate(56.000000, 0.000000)">
                                                                    <polyline id="arrow_right_black"
                                                                              transform="translate(17.935389, 17.699575) scale(-1, 1) rotate(90.000000) translate(-17.935389, -17.699575) "
                                                                              points="15 23.3991501 20.8707778 17.5283723 15.3424055 12"></polyline>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>

                            <div class="autocomplete_searchbar" ng-show="asyncCompleteCategory.length && isWhatFocus">
                                <div class="text-left scroll-1">
                                    <ul class="list-unstyled">
                                        <li ng-repeat="a in asyncCompleteCategory track by a.id">
                                            <a href="/search?PS[category_id]={{a.id}}"
                                               ng-bind-html="a.name"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="center">
                            <?= Html::activeHiddenInput($searchForm, 'location', [
                                'ng-value' => 'location',
                                'ng-init' => 'location = '
                                    . ($searchForm['location'] ? "'{$searchForm['location']}'" : 'null'),
                            ]) ?>

                            <a href="#" class="aTop"><strong><?= \Yii::t('UI2',
                                        '$SEARCH_BAR_FIELD_WHERE_LABEL$') ?></strong></a>
                            <?= Html::activeTextInput($searchForm, 'location_string', [
                                'id' => 'whoSearchCenter',
                                'class' => 'inputSearchHead search-autocomplete',
                                'placeholder' => \Yii::t('UI2', '$SEARCH_BAR_FIELD_WHERE_PLACEHOLDER$'),
                            ]) ?>
                        </div>

                        <div class="right">
                            <div class="leftBlockRight">
                                <a href="#" class="aTopRight"><strong><?= \Yii::t('UI2',
                                            '$SEARCH_BAR_FIELD_WHEN_LABEL$') ?></strong></a>

                                <?= Html::activeHiddenInput($searchForm, 'period_from', [
                                    'ng-value' => 'period_from',
                                    'ng-init' => 'period_from = '
                                        . ($searchForm['period_from'] ? "'{$searchForm['period_from']}'" : 'undefined'),
                                ]) ?>
                                <?= Html::activeHiddenInput($searchForm, 'period_to', [
                                    'ng-value' => 'period_to',
                                    'ng-init' => 'period_to = '
                                        . ($searchForm['period_to'] ? "'{$searchForm['period_to']}'" : 'undefined'),
                                ]) ?>

                                <div class="someBlock">
                                    <div class="leftSomeBlock">
                                        <?= Html::textInput('period_from', null, [
                                            'class' => 'getInput inputDate',
                                            'id' => 'getDate',
                                            'role' => 'datepicker1',
                                            'placeholder' => \Yii::t('UI2', '$SEARCH_BAR_FIELD_WHEN_FROM_PLACEHOLDER$'),
                                        ]) ?>
                                    </div>

                                    <div class="rightSomeBlock">
                                        <?= Html::textInput('period_to', null, [
                                            'class' => 'returnInput inputDate',
                                            'id' => 'returnDate',
                                            'role' => 'datepicker1',
                                            'placeholder' => \Yii::t('UI2', '$SEARCH_BAR_FIELD_WHEN_TO_PLACEHOLDER$'),
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="rightBlockRight">
                                <?= Html::submitButton(\Yii::t('UI2', '$SEARCH_BAR_SUBMIT_BTN_LABEL$'),
                                    ['class' => 'btnSearch']) ?>
                            </div>
                        </div>
                        <?= Html::endForm() ?>
                    </div>

                    <div class="text-center">
                        <a href="/service" class="btn btnHead"><?= \Yii::t('UI2', '$ABOUT_SERVICE_BTN$') ?></a>
                    </div>
                </div>
            </div>
        </div>

        <?php if (count($team_selection)): ?>
            <div class="row team">
                <div class="container">
                    <div class="row body">

                        <?= $this->render('//blocks/product-slider', [
                            'title' => 'Выбор команды',
                            'slides' => $team_selection,
                        ]) ?>

                        <div class="col-xs-12 allBlock ">
                            <a href="#" class="btn  btn-block btnAllBlock text-center" onclick="return false;">Показать
                                все
                                товары</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="row category wrapp">
            <div class="container">
                <div class="row body">
                    <div class="col-sm-12 content">
                        <!--    TITLE PAGE-->
                        <div class="title">
                            <div class="col-sm-6 col-xs-10 titleCategory text-left">
                                <h3>Категории товаров</h3>
                            </div>
                            <div class="col-sm-6 col-md-5 col-xs-2 col-md-offset- text-right form allBlockXs"
                                 ng-controller="CategoryController as CC">
                                <form method="get" action="/category/search" id="search-category-form"
                                      autocomplete="off">
                                    <?= Html::input('text', 'fields', null, [
                                        'class' => 'subCategorySearch',
                                        'placeholder' => \Yii::t('UI7', '$FIELD_AUTOCOMPLETE_PLACEHOLDER$'),
                                        'ng-keypress' => 'scanAll($event)',
                                    ]) ?>

                                </form>
                                <div class="wrappSearch" style="display: none">
                                    <div class="listCategorySearch text-left scroll-1">
                                        <ul class="list-unstyled" id="scrollList">
                                            <li ng-repeat="c in outcome">
                                                <a href="{{'/search?PS[category_id]=' + c.id + '&PS[note]=' + c.name}}"
                                                   ng-bind-html="c.label"></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--    END TITLE PAGE-->


                        <!--                slider-->
                        <div class="slider col-sm-12" id="category-links">
                            <div class="row">

                                <?php
                                $i = 1;
                                foreach ($categories as $item):
                                    $options = ['class' => 'col-md-3 col-sm-4 col-xs-6 allCategoryBlock'];
                                    if ($i > 8) {
                                        Html::addCssClass($options, ['hidden-md', 'hidden-lg']);
                                    }
                                    if ($i > 6) {
                                        Html::addCssClass($options, ['hidden-sm']);
                                    }
                                    if ($i > 6) {
                                        Html::addCssClass($options, ['hidden-xs']);
                                    }

                                    print Html::beginTag('div', $options);
                                    ?>

                                    <a href="<?= \yii\helpers\Url::to(['/search', 'PS' => ['category_id' => $item['id'], 'isOnlyCategory' => 1]]) ?>"
                                       class="thumbnail">
                                        <?= Html::img("/uploads/category-images/small/{$item['image']}",
                                            ['alt' => $item['name']]) ?>
                                        <span class="borderCateg"></span>
                                        <h4 class="text-center hovCateg"><?= $item['name'] ?></h4>
                                    </a>

                                    <?php
                                    print Html::endTag('div');

                                    if (!($i % 4)) {
                                        print Html::tag('div', '', ['class' => 'visible-md-block visible-sm-block']);
                                    } elseif (!($i % 2)) {
                                        print Html::tag('div', '', ['class' => 'visible-sm-block']);
                                    }
                                    ?>
                                    <?php
                                    $i++;
                                endforeach; ?>
                            </div>

                            <!--                end slider-->
                            <div class="col-sm-12 btnCategoty text-center">
                                <a href="/category" class="btn showAllHome">Показать все <?= count($categories) ?>
                                    категорий
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row visitedProducts">
            <?php if (!empty($viewed_products)): ?>
                <div class="container">
                    <div class="row body ">

                        <?= $this->render('//blocks/product-slider', [
                            'title' => 'Вы просматривали',
                            'slides' => $viewed_products,
                        ]) ?>
                        <div class="col-sm-12 allBlock">
                            <a href="#" class="btn btn-block btnAllBlock text-center" onclick="return false;">Показать
                                все
                                ваши просмотренные товары</a>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>


        <div class="row news">
            <div class="container">
                <div class="row body">
                    <div class="col-sm-12 content">
                        <!--    TITLE PAGE-->
                        <div class="title titleCategory">
                            <div class="text-left">
                                <h3>Популярные посты</h3>
                            </div>
                            <div class="leftSlider text-right form">
                            </div>
                        </div>
                        <!--    END TITLE PAGE-->


                        <!--                block News-->
                        <div class="row blockNews slider2">
                            <div class=" col-sm-6 col-md-4">
                                <div class="thumbnail pageProduct">
                                <span class="glyphiconHeart">
                                     <svg xmlns="http://www.w3.org/2000/svg" width="21" height="19"
                                          viewBox="-1 -1 22 20">
                              <path fill-rule="evenodd"
                                    d="M9.84718182,17.6587043 L9.5,17.8116311 L9.15281818,17.6587043 C4.69040909,14.9064558 0,8.64166921 0,5.16214939 C0.0151136364,2.31301829 2.33354545,3.85870198e-16 5.18181818,3.85870198e-16 C6.98206818,3.85870198e-16 8.57115909,0.924512195 9.5,2.32387957 C10.4288409,0.924512195 12.0179318,0 13.8181818,0 C16.6664545,0 18.9848864,2.31301829 19,5.16214939 C19,8.64166921 14.3095909,14.9064558 9.84718182,17.6587043 L9.84718182,17.6587043 Z"/>
                            </svg>
                                </span>
                                    <img class="img-responsive" src="/img/Bitmap.jpg" alt="">
                                    <div class="caption">
                                        <h3 class="longName">Длинное название поста</h3>
                                        <p>Яркая, легкая, универсальная модель для детей и подростков классического,
                                            спортивного стиля оригинального, красочного дизайна с низким профилем,
                                            отличной
                                            амортизацией и стабильностью. Идеально подойдет для занятий спортом, похода
                                            в
                                            школу, прогулок и путешествий…</p>

                                        <div class="row margg">
                                            <div class="col-xs-12 rowBottom">
                                                <p class="mailNews"><img src="img/home/mail.svg" alt="">&nbsp; 5</p>
                                                <p>20 января, 2017</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-sm-6 col-md-4">
                                <div class="thumbnail pageProduct">
                                <span class="glyphiconHeart">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="21" height="19"
                                         viewBox="-1 -1 22 20">
                              <path fill-rule="evenodd"
                                    d="M9.84718182,17.6587043 L9.5,17.8116311 L9.15281818,17.6587043 C4.69040909,14.9064558 0,8.64166921 0,5.16214939 C0.0151136364,2.31301829 2.33354545,3.85870198e-16 5.18181818,3.85870198e-16 C6.98206818,3.85870198e-16 8.57115909,0.924512195 9.5,2.32387957 C10.4288409,0.924512195 12.0179318,0 13.8181818,0 C16.6664545,0 18.9848864,2.31301829 19,5.16214939 C19,8.64166921 14.3095909,14.9064558 9.84718182,17.6587043 L9.84718182,17.6587043 Z"/>
                            </svg>
                                </span>
                                    <img class="img-responsive" src="/img/Bitmap.jpg" alt="">
                                    <div class="caption">
                                        <h3 class="longName">Длинное название поста</h3>
                                        <p>Яркая, легкая, универсальная модель для детей и подростков классического,
                                            спортивного стиля оригинального, красочного дизайна с низким профилем,
                                            отличной
                                            амортизацией и стабильностью. Идеально подойдет для занятий спортом, похода
                                            в
                                            школу, прогулок и путешествий…</p>

                                        <div class="row margg">
                                            <div class="col-xs-12 rowBottom">
                                                <p class="mailNews"><img src="img/home/mail.svg" alt="">&nbsp; 5</p>
                                                <p>20 января, 2017</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-sm-6 col-md-4">
                                <div class="thumbnail pageProduct">
                                   <span class="glyphiconHeart">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="21" height="19"
                                         viewBox="-1 -1 22 20">
                              <path fill-rule="evenodd"
                                    d="M9.84718182,17.6587043 L9.5,17.8116311 L9.15281818,17.6587043 C4.69040909,14.9064558 0,8.64166921 0,5.16214939 C0.0151136364,2.31301829 2.33354545,3.85870198e-16 5.18181818,3.85870198e-16 C6.98206818,3.85870198e-16 8.57115909,0.924512195 9.5,2.32387957 C10.4288409,0.924512195 12.0179318,0 13.8181818,0 C16.6664545,0 18.9848864,2.31301829 19,5.16214939 C19,8.64166921 14.3095909,14.9064558 9.84718182,17.6587043 L9.84718182,17.6587043 Z"/>
                            </svg>
                                </span>
                                    <img class="img-responsive" src="/img/Bitmap.jpg" alt="">
                                    <div class="caption">
                                        <h3 class="longName">Длинное название поста</h3>
                                        <p>Яркая, легкая, универсальная модель для детей и подростков классического,
                                            спортивного стиля оригинального, красочного дизайна с низким профилем,
                                            отличной
                                            амортизацией и стабильностью. Идеально подойдет для занятий спортом, похода
                                            в
                                            школу, прогулок и путешествий…</p>

                                        <div class="row margg">
                                            <div class="col-xs-12 rowBottom">
                                                <p class="mailNews"><img src="img/home/mail.svg" alt="">&nbsp; 5</p>
                                                <p>20 января, 2017</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-sm-6 col-md-4">
                                <div class="thumbnail pageProduct">
                                <span class="glyphiconHeart">
                                     <svg xmlns="http://www.w3.org/2000/svg" width="21" height="19"
                                          viewBox="-1 -1 22 20">
                              <path fill-rule="evenodd"
                                    d="M9.84718182,17.6587043 L9.5,17.8116311 L9.15281818,17.6587043 C4.69040909,14.9064558 0,8.64166921 0,5.16214939 C0.0151136364,2.31301829 2.33354545,3.85870198e-16 5.18181818,3.85870198e-16 C6.98206818,3.85870198e-16 8.57115909,0.924512195 9.5,2.32387957 C10.4288409,0.924512195 12.0179318,0 13.8181818,0 C16.6664545,0 18.9848864,2.31301829 19,5.16214939 C19,8.64166921 14.3095909,14.9064558 9.84718182,17.6587043 L9.84718182,17.6587043 Z"/>
                            </svg>
                                </span>
                                    <img class="img-responsive" src="/img/Bitmap.jpg" alt="">
                                    <div class="caption">
                                        <h3 class="longName">Длинное название поста</h3>
                                        <p>Яркая, легкая, универсальная модель для детей и подростков классического,
                                            спортивного стиля оригинального, красочного дизайна с низким профилем,
                                            отличной
                                            амортизацией и стабильностью. Идеально подойдет для занятий спортом, похода
                                            в
                                            школу, прогулок и путешествий…</p>

                                        <div class="row margg">
                                            <div class="col-xs-12 rowBottom">
                                                <p class="mailNews"><img src="img/home/mail.svg" alt="">&nbsp; 5</p>
                                                <p>20 января, 2017</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-sm-6 col-md-4">
                                <div class="thumbnail pageProduct">
                                <span class="glyphiconHeart">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="21" height="19"
                                         viewBox="-1 -1 22 20">
                              <path fill-rule="evenodd"
                                    d="M9.84718182,17.6587043 L9.5,17.8116311 L9.15281818,17.6587043 C4.69040909,14.9064558 0,8.64166921 0,5.16214939 C0.0151136364,2.31301829 2.33354545,3.85870198e-16 5.18181818,3.85870198e-16 C6.98206818,3.85870198e-16 8.57115909,0.924512195 9.5,2.32387957 C10.4288409,0.924512195 12.0179318,0 13.8181818,0 C16.6664545,0 18.9848864,2.31301829 19,5.16214939 C19,8.64166921 14.3095909,14.9064558 9.84718182,17.6587043 L9.84718182,17.6587043 Z"/>
                            </svg>
                                </span>
                                    <img class="img-responsive" src="/img/Bitmap.jpg" alt="">
                                    <div class="caption">
                                        <h3 class="longName">Длинное название поста</h3>
                                        <p>Яркая, легкая, универсальная модель для детей и подростков классического,
                                            спортивного стиля оригинального, красочного дизайна с низким профилем,
                                            отличной
                                            амортизацией и стабильностью. Идеально подойдет для занятий спортом, похода
                                            в
                                            школу, прогулок и путешествий…</p>

                                        <div class="row margg">
                                            <div class="col-xs-12 rowBottom">
                                                <p class="mailNews"><img src="img/home/mail.svg" alt="">&nbsp; 5</p>
                                                <p>20 января, 2017</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-sm-6 col-md-4">
                                <div class="thumbnail pageProduct">
                               <span class="glyphiconHeart">
                                     <svg xmlns="http://www.w3.org/2000/svg" width="21" height="19"
                                          viewBox="-1 -1 22 20">
                                      <path fill-rule="evenodd"
                                            d="M9.84718182,17.6587043 L9.5,17.8116311 L9.15281818,17.6587043 C4.69040909,14.9064558 0,8.64166921 0,5.16214939 C0.0151136364,2.31301829 2.33354545,3.85870198e-16 5.18181818,3.85870198e-16 C6.98206818,3.85870198e-16 8.57115909,0.924512195 9.5,2.32387957 C10.4288409,0.924512195 12.0179318,0 13.8181818,0 C16.6664545,0 18.9848864,2.31301829 19,5.16214939 C19,8.64166921 14.3095909,14.9064558 9.84718182,17.6587043 L9.84718182,17.6587043 Z"/>
                                        </svg>
                                </span>
                                    <img class="img-responsive" src="/img/Bitmap.jpg" alt="">
                                    <div class="caption">
                                        <h3 class="longName">Длинное название поста</h3>
                                        <p>Яркая, легкая, универсальная модель для детей и подростков классического,
                                            спортивного стиля оригинального, красочного дизайна с низким профилем,
                                            отличной
                                            амортизацией и стабильностью. Идеально подойдет для занятий спортом, похода
                                            в
                                            школу, прогулок и путешествий…</p>

                                        <div class="row margg">
                                            <div class="col-xs-12 rowBottom">
                                                <p class="mailNews"><img src="img/home/mail.svg" alt="">&nbsp; 5</p>
                                                <p>20 января, 2017</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--                block News-->
                    </div>

                    <!--                btnNews-->
                    <div class="col-xs-12 allBlockNews">
                        <a href="/news" class="btn btnAllBlock btn-block btnNews">Показать все посты</a>
                    </div>
                    <!--                btnNews-->
                </div>
            </div>
        </div>
        <div class="row subscription">

            <?= $this->render('//subscription/subscription') ?>
        </div>

        <?php if (count($sliderLine1['slides'])): ?>
            <div class="row buildingMaterial ">
                <div class="container">
                    <div class="row body">
                        <!-- слайдер с товарами -->
                        <?= $this->render('//blocks/product-slider', [
                            'title' => $sliderLine1['title'],
                            'slides' => $sliderLine1['slides'],
                        ]) ?>

                        <div class="col-sm-12 allBlock">
                            <?= Html::a('Посмотреть категорию', ['/search', 'category' => $sliderLine1['id']],
                                ['class' => 'btn btn-block btnAllBlock']) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="row earn wrapp">
            <div class="container">
                <div class="row body">
                    <div class="col-sm-6 col-xs-12 rightBlock col-sm-push-6">
                        <span><img src="/img/category/each.svg" alt=""></span>
                    </div>
                    <div class="col-sm-6 col-xs-12 text-left leftBlock col-sm-pull-6">
                        <h3>Зарабатывай с <span>Wearent</span></h3>
                        <p>Рассчитай сколько ты можешь заработать на сдаче<br> в аренду вашего товара.</p>
                        <a href="#" class="text-center btn btn-lg">Посмотреть калькулятор</a>
                    </div>
                </div>
            </div>
        </div>

        <?php if (count($sliderLine2['slides'])): ?>
            <div class="row autoMoto">
                <div class="container">
                    <div class="row body">
                        <?= $this->render('//blocks/product-slider', [
                            'title' => $sliderLine2['title'],
                            'slides' => $sliderLine2['slides'],
                        ]) ?>

                        <div class="col-sm-12 allBlock">
                            <?= Html::a('Посмотреть категорию', ['/search', 'category' => $sliderLine2['id']],
                                ['class' => 'btn btn-block btnAllBlock text-center']) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if (count($sliderAmazing['slides'])): ?>
            <div class="row unusualGoods ">
                <div class="container">
                    <div class="row body">

                        <?= $this->render('//blocks/product-slider', [
                            'title' => $sliderAmazing['title'],
                            'slides' => $sliderAmazing['slides'],
                        ]) ?>

                        <div class="col-xs-12 hidden-xs allBlock">
                            <a href="/amazing" class="btn btn-block btnAllBlock text-center">Посмотреть
                                категорию</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="row statistics">
            <div class="container">
                <div class="row body">
                    <div class="col-sm-12 content">
                        <div class="row  title">
                            <div class="col-sm-12 text-left">
                                <h3>Живая статистика сделок</h3>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <tr class="caption">
                                    <td class="tdStrong one">Кто</td>
                                    <td class="two">Тип сделки</td>
                                    <td class="tre">Какой товар</td>
                                    <td>Срок аренды</td>
                                    <td>Когда</td>
                                </tr>
                                <tr>
                                    <td class="tdStrong one">Дмитрий Д.</td>
                                    <td class="two"><span class="took ">
                                        <img src="/img/statistic/took.svg" alt="">
                                    </span>взял в аренду
                                    </td>
                                    <td class="tdStrong tre">Труповозка</td>
                                    <td class="tdStrong">1 месяц</td>
                                    <td>1 минуту назад</td>
                                </tr>
                                <tr>
                                    <td class="tdStrong one">Виталий Р.</td>
                                    <td class="two"><span class="gave">
                                        <img src="/img/statistic/gave.svg" alt="">
                                    </span>сдал в аренду
                                    </td>
                                    <td class="tdStrong tre">Телевизор LG</td>
                                    <td class="tdStrong">1 неделя</td>
                                    <td>1 минуту назад</td>
                                </tr>
                                <tr>
                                    <td class="tdStrong one">Станислав Г</td>
                                    <td class="two"><span class="gave">
                                        <img src="/img/statistic/gave.svg" alt="">
                                    </span>сдал в аренду
                                    </td>
                                    <td class="tdStrong tre">Dacia Logan</td>
                                    <td class="tdStrong">2 года</td>
                                    <td>1 минуту назад</td>
                                </tr>
                                <tr>
                                    <td class="tdStrong one">Андрей Х.</td>
                                    <td class="two"><span class="took">
                                        <img src="/img/statistic/took.svg" alt="">
                                    </span>взял в аренду
                                    </td>
                                    <td class="tdStrong tre">Жена соседа</td>
                                    <td class="tdStrong">3 дня</td>
                                    <td>1 минуту назад</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row number">
            <div class="container">
                <div class="row body">
                    <div class="col-sm-12 numberBlock">
                        <div class="leftNumberBlock column">
                            <span><img src="/img/category/leftNumber.svg" alt=""></span>
                            <div class="blockText">
                                <p class=" bigText text-left"><?= $product_count ?>+</p>
                                <p class="smallText text-left">Всего товаров<br> на сайте</p>
                            </div>
                        </div>

                        <div class="centerNumberBlock column">
                            <span><img src="/img/category/centerNumber.svg" alt=""></span>
                            <div class="blockText">
                                <p class=" bigText text-left">100+</p>
                                <p class="smallText text-left">Всего сделок<br> на сайте</p>
                            </div>
                        </div>

                        <div class="rightNumberBlock column">
                            <span><img src="/img/category/rightNumber.svg" alt=""></span>
                            <div class="blockText">
                                <p class=" bigText text-left">100+</p>
                                <p class="smallText text-left">Недовольных<br> пользователей</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--        HOVER НА ПЕРШИЙ ІНПУТ І ВІДКРИВАЄ ІСТОРІЮ-->
        <?php
        $cache = \Yii::$app->cache;
        if ($dataProviderHistory->getCount()): ?>
            <div id="WRAPPANELhead" style="display: none">
                <div class="col-sm-10 history scroll-1">

                    <?= \yii\widgets\ListView::widget([
                        'beforeItem' => function ($model, $key, $index, $widget) use ($cache) {
                            $c = (int)$model->field_category_id;
                            if ($c > 0) {
                                $model->field_what = $cache->getOrSet('HistorySearch' . $c,
                                    function () use ($c) {
                                        return \yiimodules\categories\models\Categories::find()
                                            ->select('name')
                                            ->where(['id' => $c])
                                            ->scalar();
                                    });
                            }
                        },
                        'dataProvider' => $dataProviderHistory,
                        'emptyText' => 'нет истории',
                        'itemView' => '/search/_history-panel',
                        'itemOptions' => [
                            'class' => 'liText',
                        ],
                        'layout' => '{items}',
                    ]) ?>

                </div>
            </div>
        <?php endif; ?>

        <!--        КЛІК НА КНОПКУ В ПОЛІ ШТО І ВІДКРИВАЄ СПИСОК-->
        <div class="col-sm-9 selectProductTitle" style="display: none;">
            <div class="row">
                <div class="leftSelect">
                    <ul class="list-unstyled">
                        <?php foreach ($a as $k => $i): ?>
                            <?= Html::tag('li', $i['name'], ['id' => $k]) ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="leftSelectDisplay">
                    <span><img src="/img/popup/mail.svg" alt=""></span>
                    <span>Выберите категорию товара</span>
                </div>
                <div class="centerSelect scroll-1">
                    <ul class="list-unstyled " style="display: none"></ul>
                </div>
                <div class="rightSelect scroll-1">
                    <ul class="list-unstyled" style="display: none"></ul>
                </div>
            </div>
        </div>

        <!--        HOVER  НА ІНПУТ ГДЕ-->
        <div class="col-sm-5 whenBlockTitle" style="display: none">
            <ul class="list-unstyled scrollbar1">
                <?php foreach ([['name' => 'test']] as $k => $i): ?>
                    <?= Html::beginTag('li', ['data-id' => $k]) ?>
                    <svg class="sityImg" width="14px" height="14px" viewBox="0 0 14 14" version="1.1">
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="sityImg" transform="translate(-539.000000, -140.000000)" fill-rule="nonzero">
                                <g id="product" transform="translate(519.000000, 71.000000)">
                                    <path d="M32.8122803,75.1122805 L31.8719295,75.1122805 C31.6368418,72.6438595 29.6385962,70.7631578 27.2877191,70.5280701 L27.2877191,69.5877193 C27.2877191,69.2350877 27.0526314,69 26.6999998,69 C26.3473682,69 26.1122805,69.2350877 26.1122805,69.5877193 L26.1122805,70.5280701 C23.6438595,70.7631578 21.7631578,72.7614034 21.5280701,75.1122805 L20.5877193,75.1122805 C20.2350877,75.1122805 20,75.3473682 20,75.6999998 C20,76.0526314 20.2350877,76.2877191 20.5877193,76.2877191 L21.5280701,76.2877191 C21.7631578,78.7561401 23.7614034,80.6368418 26.1122805,80.8719295 L26.1122805,81.8122803 C26.1122805,82.1649119 26.3473682,82.3999996 26.6999998,82.3999996 C27.0526314,82.3999996 27.2877191,82.1649119 27.2877191,81.8122803 L27.2877191,80.8719295 C29.7561401,80.6368418 31.6368418,78.6385962 31.8719295,76.2877191 L32.8122803,76.2877191 C33.1649119,76.2877191 33.3999996,76.0526314 33.3999996,75.6999998 C33.3999996,75.3473682 33.1649119,75.1122805 32.8122803,75.1122805 Z M27.2877191,79.6964909 L27.2877191,78.8736839 C27.2877191,78.5210524 27.0526314,78.2859646 26.6999998,78.2859646 C26.3473682,78.2859646 26.1122805,78.5210524 26.1122805,78.8736839 L26.1122805,79.6964909 C24.3491227,79.4614032 22.9385964,78.0508769 22.7035087,76.2877191 L23.5263157,76.2877191 C23.8789473,76.2877191 24.114035,76.0526314 24.114035,75.6999998 C24.114035,75.3473682 23.8789473,75.1122805 23.5263157,75.1122805 L22.7035087,75.1122805 C22.9385964,73.3491227 24.3491227,71.9385964 26.1122805,71.7035087 L26.1122805,72.5263157 C26.1122805,72.8789473 26.3473682,73.114035 26.6999998,73.114035 C27.0526314,73.114035 27.2877191,72.8789473 27.2877191,72.5263157 L27.2877191,71.7035087 C29.0508769,71.9385964 30.4614032,73.3491227 30.6964909,75.1122805 L29.8736839,75.1122805 C29.5210524,75.1122805 29.2859646,75.3473682 29.2859646,75.6999998 C29.2859646,76.0526314 29.5210524,76.2877191 29.8736839,76.2877191 L30.6964909,76.2877191 C30.4614032,78.0508769 29.0508769,79.4614032 27.2877191,79.6964909 Z"
                                          id="Shape-Copy"></path>
                                </g>
                            </g>
                        </g>
                    </svg>
                    <?= $i['name'] ?>

                    <?= Html::endTag('li') ?>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>


<?php
$this->registerJsFile('/js/grom.js', [
    'position' => \yii\web\View::POS_END,
    'depends' => ['frontend\assets\AppAsset', 'frontend\assets\FrontendAsset'],
]);
$this->registerJsFile('/js/hideRent.js', [
    'position' => \yii\web\View::POS_END,
    'depends' => ['frontend\assets\AppAsset'],
]);
$this->registerJsFile('/js/ahome.js', [
    'position' => \yii\web\View::POS_END,
    'depends' => ['frontend\assets\AppAsset'],
]);
$this->registerJsFile('/js/acategory.js', [
    'position' => \yii\web\View::POS_END,
    'depends' => ['frontend\assets\AppAsset', 'frontend\assets\FrontendAsset'],
]);

$listCategoryPopup = \yii\helpers\Json::encode($listPopup);
$list = \yii\helpers\Json::encode($list);

$this->registerJs(<<<JS
var categoryNestedList1 = {$listCategoryPopup};
var categoryProductGroup = $list;
JS
    , \yii\web\View::POS_BEGIN);

$this->registerJsFile('/js/aheadPanel.js', [
    'position' => \yii\web\View::POS_END,
    'depends' => [
        'frontend\assets\AppAsset',
    ],
]);

$this->registerJs(<<<JS
jQuery("#search-category-form").on("focus", "input", function(event){
    
    if ($(window).width() <= '850'){
        jQuery("body,html").animate({scrollTop: jQuery(event.target).offset().top - 2}, 350);
    }
});
JS
);
