<?php


use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \yiimodules\categories\models\Categories[] $categories список категорий */
/** @var \common\models\Product[] $viewed_products список недавно просмотренных товаров */

\frontend\assets\SlickAsset::register($this);
\frontend\assets\yyAsset::register($this);
\yii\bootstrap\BootstrapPluginAsset::register($this);


$listPopup = [

    1 => [
        'id'   => 1,
        'name' => 'Музыка',
        'img'  => '/img/category/music.svg',
    ],

    3 => [
        'id'   => 3,
        'name' => 'Спорт, Отдых',
        'node' => [
            101  => [
                'id'   => 3301,
                'name' => 'Велосипеды, гироскутеры, ролики, скейты',
            ],
            1011 => [
                'id'   => 3302,
                'name' => 'Лыжи, коньки, санки',
            ],
            102  => [
                'id'   => 3302,
                'name' => 'Пикник, кемпинг, пляж',
            ],
            103  => [
                'id'   => 103,
                'name' => 'Туризм, путешествия',
                'node' => [
                    3320 => [
                        'id'   => 3332,
                        'name' => 'Велосипеды, гироскутеры, ролики, скейты',
                    ],
                    3321 => [
                        'id'   => 3333,
                        'name' => 'Лыжи, коньки, санки',
                    ],
                    3322 => [
                        'id'   => 3334,
                        'name' => 'Пикник, кемпинг, пляж',
                    ],
                    3323 => [
                        'id'   => 3335,
                        'name' => 'Туризм, путешествия',
                    ],
                    3324 => [
                        'id'   => 3336,
                        'name' => 'Спортивные игры',
                    ],
                    3325 => [
                        'id'   => 3337,
                        'name' => 'Дайвинг, водный спорт',
                    ],
                    3326 => [
                        'id'   => 3338,
                        'name' => 'Услуги инструкторов',
                    ],
                    3327 => [
                        'id'   => 3339,
                        'name' => 'Аренда спортивных помещений',
                    ],
                    3328 => [
                        'id'   => 3340,
                        'name' => 'Картинг',
                    ],
                    3329 => [
                        'id'   => 3341,
                        'name' => 'Тренажеры, фитнес, единоборства',
                    ],
                    3330 => [
                        'id'   => 3342,
                        'name' => 'Пневматика и снаряжение',
                    ],

                ],
            ],
            104  => [
                'id'   => 3304,
                'name' => 'Спортивные игры',
            ],
            105  => [
                'id'   => 3305,
                'name' => 'Дайвинг, водный спорт',
            ],
            106  => [
                'id'   => 3306,
                'name' => 'Услуги инструкторов',
            ],
            107  => [
                'id'   => 3307,
                'name' => 'Аренда спортивных помещений',
            ],
            108  => [
                'id'   => 3308,
                'name' => 'Картинг',
            ],
            109  => [
                'id'   => 3309,
                'name' => 'Тренажеры, фитнес, единоборства',
            ],
            1010 => [
                'id'   => 3310,
                'name' => 'Пневматика и снаряжение',
            ],

        ],
        'img'  => '/img/category/backpack.svg',
    ],

    4 => [
        'id'   => 331,
        'name' => 'Фото, Видео',
        'img'  => '/img/category/foto.svg',
    ],

    5 => [
        'id'   => 332,
        'name' => 'Праздники и события',
        'img'  => '/img/category/music.svg',
    ],

    6 => [
        'id'   => 333,
        'name' => 'Инструменты и спец. техника',
        'img'  => '/img/category/music.svg',
    ],

    2 => [
        'id'   => 334,
        'name' => 'Детские товары',
        'img'  => '/img/category/music.svg',
    ],

    7 => [
        'id'   => 334,
        'name' => 'Авто Мото',
        'img'  => '/img/category/auto.svg',
    ],

    8 => [
        'id'   => 335,
        'name' => 'Красота и здоровье',
        'img'  => '/img/category/music.svg',
    ],

    9 => [
        'id'   => 336,
        'name' => 'Бизнес',
        'img'  => '/img/category/tv.svg',
    ],

    10 => [
        'id'   => 337,
        'name' => 'Одежда, обувь, акссесуары',
        'img'  => '/img/category/stuff.svg',
    ],

    11 => [
        'id'   => 337,
        'name' => 'Бытовая техника',
        'img'  => '/img/category/drill.svg',
    ],

    12 => [
        'id'   => 337,
        'name' => 'Все для дома',
        'img'  => '/img/category/bett.svg',
    ],

    13 => [
        'id'   => 337,
        'name' => 'Дача, сад',
        'img'  => '/img/category/music.svg',
    ],


];
?>

    <div class="row head wrapp">

        <div class="container">
            <div class="row body">

                <div class="title">
                    <div class="col-sm-12">
                        <h1 class="text-center"><span>Wearent</span> получай новые впечатления,<br> арендуя уникальные
                            вещи</h1>
                        <h3 class="text-center">Зарабатывайте на безопасной сдаче вещей</h3>
                    </div>
                </div>


                <div class="col-sm-12 headSearch">
                    <form action="" method="post" class="titleSearch">
                        <div class="left">
                            <div class="leftBlockLeft">
                                <a href="#" class="aTop"><strong>Что</strong></a>
                                <label for="whoSearchLeft" class="labelForSearch">Световой меч</label>
                                <input type="text" class="inputSearchHead" id="whoSearchLeft">

                            </div>
                            <div class="rightBlockLeft">
                                <a href="#" class="btnBig"><img src="/img/category/droppMenu.svg" alt=""></a>
                            </div>
                        </div>

                        <div class=" center">
                            <a href="#" class="aTop"><strong>Где</strong></a>
                            <label for="whoSearchCenter" class="labelForSearch">Татуин, ул. Энакина, дом 2</label>
                            <input type="text" class="inputSearchHead" id="whoSearchCenter">
                        </div>

                        <div class=" right">
                            <div class="leftBlockRight">
                                <a href="#" class="aTop"><strong>Когда</strong></a>
                                <div class="someBlock">

                                    <label class="labelGet labelDate" for="getDate">Получить</label>
                                    <input type="text" id="getDate" class="getInput inputDate" role="datepicker1">

                                    <label class="labelGet labelDate" for="returnDate">Вернуть</label>
                                    <input type="text" id="returnDate" class="returnInput inputDate" role="datepicker1">
                                </div>
                            </div>
                            <div class="rightBlockRight">
                                <button type="submit" class="btnSearch">Найти</button>
                            </div>
                        </div>
                    </form>


                </div>


                <div class=" text-center">
                    <a href="#" class="btn btnHead">Узнай больше о сервисе</a>
                </div>

                <div class="col-sm-12 content">

                    <div class=" row team">
                        <div class="col-sm-12">

                            <div class="row  titleTeam">
                                <div class="col-sm-7 text-left">
                                    <h3>Выбор команды <span>Wearent</span></h3>
                                </div>
                                <div class="col-sm-5 hidden-xs leftSlider text-right form">

                                </div>
                            </div>


                            <div class="media">
                                <div class="row slider2">
                                    <div class="col-sm-6 col-md-4 paddSlide">

                                        <div class="thumbnail">
                                            <div class="slider1">
                                                <div class="sl">
                                                    <span class="glyphicon glyphicon-heart"></span>
                                                    <img class="imgSl" src="/img/Bitmap.jpg"
                                                         alt="">
                                                    <div class="textSl">

                                                        <p class="pSl">Фото 1 из 4</p>

                                                    </div>
                                                </div>

                                                <div class="sl">
                                                    <span class="glyphicon glyphicon-heart"></span>
                                                    <img class="imgSl" src="/img/mitsubishi1.jpg"
                                                         alt="">
                                                    <div class="textSl">
                                                        <p class="pSl">Фото 1 из 4</p>
                                                    </div>
                                                </div>

                                                <div class="sl">
                                                    <span class="glyphicon glyphicon-heart"></span>
                                                    <img class="imgSl" src="/img/mitsubishi2.jpg"
                                                         alt="">
                                                    <div class="textSl">
                                                        <p class="pSl">Фото 1 из 4</p>
                                                    </div>
                                                </div>


                                            </div>


                                            <div class="caption">
                                                <h3 class="longName">Длинное название товара</h3>
                                                <a href="#" class="btn btnTrans" role="button">Транспорт</a>
                                                <table class="table-block table-responsive">
                                                    <tr>
                                                        <td>Цена за сутки</td>
                                                        <td class="text-right"><strong>100 руб.</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Мин. срок аренды</td>
                                                        <td class="text-right"><strong>1 день</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Оценка товара</td>
                                                        <td class="text-right">
                                                            <div class="star-rating">
                                                                <input type="radio" name="example" class="rating"
                                                                       value="1"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="2"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="3"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="4"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="5"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="row marg">
                                                    <div class="col-xs-12 rowBottom">
                                                        <div class="margLeft">
                                                            <span class="face">
                                                            <a href="#">
                                                                <img class="menFase"
                                                                     src="/img/category/man.png"
                                                                     alt="">
                                                            </a>
                                                            </span>
                                                            <span class="mail">

                                                                <img class="menFase"
                                                                     src="img/home/mail.svg" alt="">&nbsp; 5

                                                            </span>
                                                        </div>


                                                        <span class="leftRightBtn"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 paddSlide">

                                        <div class="thumbnail">
                                            <div class="slider1">
                                                <div class="sl">
                                                    <span class="glyphicon glyphicon-heart"></span>
                                                    <img class="imgSl" src="/img/Bitmap.jpg"
                                                         alt="">
                                                    <div class="textSl">

                                                        <p class="pSl">Фото 1 из 4</p>

                                                    </div>
                                                </div>

                                                <div class="sl">
                                                    <span class="glyphicon glyphicon-heart"></span>
                                                    <img class="imgSl" src="/img/mitsubishi1.jpg"
                                                         alt="">
                                                    <div class="textSl">
                                                        <p class="pSl">Фото 1 из 4</p>
                                                    </div>
                                                </div>

                                                <div class="sl">
                                                    <span class="glyphicon glyphicon-heart"></span>
                                                    <img class="imgSl" src="/img/mitsubishi2.jpg"
                                                         alt="">
                                                    <div class="textSl">
                                                        <p class="pSl">Фото 1 из 4</p>
                                                    </div>
                                                </div>


                                            </div>


                                            <div class="caption">
                                                <h3 class="longName">Длинное название товара</h3>
                                                <a href="#" class="btn btnTrans" role="button">Транспорт</a>
                                                <table class="table-block table-responsive">
                                                    <tr>
                                                        <td>Цена за сутки</td>
                                                        <td class="text-right"><strong>100 руб.</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Мин. срок аренды</td>
                                                        <td class="text-right"><strong>1 день</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Оценка товара</td>
                                                        <td class="text-right">
                                                            <div class="star-rating">
                                                                <input type="radio" name="example" class="rating"
                                                                       value="1"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="2"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="3"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="4"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="5"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="row marg">
                                                    <div class="col-xs-12 rowBottom">
                                                        <div class="margLeft">
                                                            <span class="face">
                                                            <a href="#">
                                                                <img class="menFase"
                                                                     src="/img/category/man.png"
                                                                     alt="">
                                                            </a>
                                                            </span>
                                                            <span class="mail">

                                                                <img class="menFase"
                                                                     src="img/home/mail.svg" alt="">&nbsp; 5

                                                            </span>
                                                        </div>


                                                        <span class="leftRightBtn"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 paddSlide">

                                        <div class="thumbnail">
                                            <div class="slider1">
                                                <div class="sl">
                                                    <span class="glyphicon glyphicon-heart"></span>
                                                    <img class="imgSl" src="/img/Bitmap.jpg"
                                                         alt="">
                                                    <div class="textSl">

                                                        <p class="pSl">Фото 1 из 4</p>

                                                    </div>
                                                </div>

                                                <div class="sl">
                                                    <span class="glyphicon glyphicon-heart"></span>
                                                    <img class="imgSl" src="/img/mitsubishi1.jpg"
                                                         alt="">
                                                    <div class="textSl">
                                                        <p class="pSl">Фото 1 из 4</p>
                                                    </div>
                                                </div>

                                                <div class="sl">
                                                    <span class="glyphicon glyphicon-heart"></span>
                                                    <img class="imgSl" src="/img/mitsubishi2.jpg"
                                                         alt="">
                                                    <div class="textSl">
                                                        <p class="pSl">Фото 1 из 4</p>
                                                    </div>
                                                </div>


                                            </div>


                                            <div class="caption">
                                                <h3 class="longName">Длинное название товара</h3>
                                                <a href="#" class="btn btnTrans" role="button">Транспорт</a>
                                                <table class="table-block table-responsive">
                                                    <tr>
                                                        <td>Цена за сутки</td>
                                                        <td class="text-right"><strong>100 руб.</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Мин. срок аренды</td>
                                                        <td class="text-right"><strong>1 день</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Оценка товара</td>
                                                        <td class="text-right">
                                                            <div class="star-rating">
                                                                <input type="radio" name="example" class="rating"
                                                                       value="1"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="2"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="3"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="4"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="5"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="row marg">
                                                    <div class="col-xs-12 rowBottom">
                                                        <div class="margLeft">
                                                            <span class="face">
                                                            <a href="#">
                                                                <img class="menFase"
                                                                     src="/img/category/man.png"
                                                                     alt="">
                                                            </a>
                                                            </span>
                                                            <span class="mail">

                                                                <img class="menFase"
                                                                     src="img/home/mail.svg" alt="">&nbsp; 5

                                                            </span>
                                                        </div>


                                                        <span class="leftRightBtn"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 paddSlide">

                                        <div class="thumbnail">
                                            <div class="slider1">
                                                <div class="sl">
                                                    <span class="glyphicon glyphicon-heart"></span>
                                                    <img class="imgSl" src="/img/Bitmap.jpg"
                                                         alt="">
                                                    <div class="textSl">

                                                        <p class="pSl">Фото 1 из 4</p>

                                                    </div>
                                                </div>

                                                <div class="sl">
                                                    <span class="glyphicon glyphicon-heart"></span>
                                                    <img class="imgSl" src="/img/mitsubishi1.jpg"
                                                         alt="">
                                                    <div class="textSl">
                                                        <p class="pSl">Фото 1 из 4</p>
                                                    </div>
                                                </div>

                                                <div class="sl">
                                                    <span class="glyphicon glyphicon-heart"></span>
                                                    <img class="imgSl" src="/img/mitsubishi2.jpg"
                                                         alt="">
                                                    <div class="textSl">
                                                        <p class="pSl">Фото 1 из 4</p>
                                                    </div>
                                                </div>


                                            </div>


                                            <div class="caption">
                                                <h3 class="longName">Длинное название товара</h3>
                                                <a href="#" class="btn btnTrans" role="button">Транспорт</a>
                                                <table class="table-block table-responsive">
                                                    <tr>
                                                        <td>Цена за сутки</td>
                                                        <td class="text-right"><strong>100 руб.</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Мин. срок аренды</td>
                                                        <td class="text-right"><strong>1 день</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Оценка товара</td>
                                                        <td class="text-right">
                                                            <div class="star-rating">
                                                                <input type="radio" name="example" class="rating"
                                                                       value="1"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="2"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="3"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="4"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="5"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="row marg">
                                                    <div class="col-xs-12 rowBottom">
                                                        <div class="margLeft">
                                                            <span class="face">
                                                            <a href="#">
                                                                <img class="menFase"
                                                                     src="/img/category/man.png"
                                                                     alt="">
                                                            </a>
                                                            </span>
                                                            <span class="mail">

                                                                <img class="menFase"
                                                                     src="img/home/mail.svg" alt="">&nbsp; 5

                                                            </span>
                                                        </div>


                                                        <span class="leftRightBtn"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 paddSlide">

                                        <div class="thumbnail">
                                            <div class="slider1">
                                                <div class="sl">
                                                    <span class="glyphicon glyphicon-heart"></span>
                                                    <img class="imgSl" src="/img/Bitmap.jpg"
                                                         alt="">
                                                    <div class="textSl">

                                                        <p class="pSl">Фото 1 из 4</p>

                                                    </div>
                                                </div>

                                                <div class="sl">
                                                    <span class="glyphicon glyphicon-heart"></span>
                                                    <img class="imgSl" src="/img/mitsubishi1.jpg"
                                                         alt="">
                                                    <div class="textSl">
                                                        <p class="pSl">Фото 1 из 4</p>
                                                    </div>
                                                </div>

                                                <div class="sl">
                                                    <span class="glyphicon glyphicon-heart"></span>
                                                    <img class="imgSl" src="/img/mitsubishi2.jpg"
                                                         alt="">
                                                    <div class="textSl">
                                                        <p class="pSl">Фото 1 из 4</p>
                                                    </div>
                                                </div>


                                            </div>


                                            <div class="caption">
                                                <h3 class="longName">Длинное название товара</h3>
                                                <a href="#" class="btn btnTrans" role="button">Транспорт</a>
                                                <table class="table-block table-responsive">
                                                    <tr>
                                                        <td>Цена за сутки</td>
                                                        <td class="text-right"><strong>100 руб.</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Мин. срок аренды</td>
                                                        <td class="text-right"><strong>1 день</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Оценка товара</td>
                                                        <td class="text-right">
                                                            <div class="star-rating">
                                                                <input type="radio" name="example" class="rating"
                                                                       value="1"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="2"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="3"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="4"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="5"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="row marg">
                                                    <div class="col-xs-12 rowBottom">
                                                        <div class="margLeft">
                                                            <span class="face">
                                                            <a href="#">
                                                                <img class="menFase"
                                                                     src="/img/category/man.png"
                                                                     alt="">
                                                            </a>
                                                            </span>
                                                            <span class="mail">

                                                                <img class="menFase"
                                                                     src="img/home/mail.svg" alt="">&nbsp; 5

                                                            </span>
                                                        </div>


                                                        <span class="leftRightBtn"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <!--        END TEAM WEARENT-->
                <div class="col-xs-12 allBlock ">
                    <a href="#" class="btn  btn-block showAll text-center">Показать все товар</a>
                </div>


            </div>
        </div>
    </div>


    <div class="row category wrapp">

    <div class="container">
        <div class="row body">

            <div class="col-sm-12 content">
                <!--    TITLE PAGE-->
                <div class="  title">
                    <div class="col-sm-7 text-left">
                        <h3>Категории товаров</h3>
                    </div>
                    <div class="col-sm-5 text-right form">
                        <input type="email" class="form-control " placeholder="Поиск в категории товаров">

                    </div>
                </div>

                <!--    END TITLE PAGE-->


                <!--                slider-->
                <div class="slider col-sm-12">
                    <div class="row">

                        <?php
                        $i = 1;
                        foreach ($categories as $item):
                            $options = ['class' => 'col-md-3 col-sm-6 col-xs-12 allCategoryBlock'];
                            if ($i > 8) {
                                Html::addCssClass($options, ['hidden-md', 'hidden-lg']);
                            }
                            if ($i > 6) {
                                Html::addCssClass($options, ['hidden-sm']);
                            }
                            if ($i > 4) {
                                Html::addCssClass($options, ['hidden-xs']);
                            }

                            print Html::beginTag('div', $options);
                            ?>

                            <a href="<?= \yii\helpers\Url::to(['/category/repel', 'fields' => $item['name']]) ?>"
                               class="thumbnail">
                                <?= Html::img($item['image'], ['alt' => $item['name']]) ?>
                                <span class="borderCateg"></span>
                                <h4 class="text-center hovCateg"><?= $item['name'] ?></h4>
                            </a>

                            <?php
                            print Html::endTag('div');

                            if (!($i % 4)) {
                                print Html::tag('div', '', ['class' => 'visible-md-block visible-sm-block']);
                            } elseif (!($i % 2)) {
                                print Html::tag('div', '', ['class' => 'visible-sm-block']);
                            }
                            ?>
                            <?php
                            $i++;
                        endforeach; ?>
                    </div>
                    <!--                end slider-->

                    <div class="col-sm-12 btnCategoty text-center">
                        <a href="#" class="btn showAllHome" >Показать все <?= count($categories) ?> категорий товаров</a>
                        <a href="#" class="btn hideAllHome" style="display: none">close</a>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="row visitedProducts wrapp">
        <?php if (!empty($viewed_products)): ?>
        <div class="container">
            <div class="row body ">

                <?= $this->render('//blocks/product-slider', [
                        'title' => 'Вы просматривали',
                        'slides' => $viewed_products,
]) ?>
                <div class="col-sm-12 allBlock">
                    <a href="#" class="btn btn-block btnAllBlock text-center" onclick="return false;">Показать все ваши просмотренные товары</a>
                </div>


            </div>
        </div>
        <?php endif; ?>
    </div>


    <div class="row news ">


        <div class="container">
            <div class="row body">

                <div class="col-sm-12 content">
                    <!--    TITLE PAGE-->
                    <div class="row  title">
                        <div class="col-sm-7 text-left">
                            <h3>Популярные посты</h3>
                        </div>
                        <div class="col-sm-5 hidden-xs leftSlider text-right form">
                        </div>
                    </div>
                    <!--    END TITLE PAGE-->


                    <!--                block News-->
                    <div class="row blockNews slider2 ">
                        <div class=" col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <span class="glyphicon glyphicon-heart"></span>
                                <img class="img-responsive" src="/img/Bitmap.jpg" alt="">
                                <div class="caption">
                                    <h3 class="longName">Длинное название поста</h3>
                                    <p>Яркая, легкая, универсальная модель для детей и подростков классического,
                                        спортивного стиля оригинального, красочного дизайна с низким профилем, отличной
                                        амортизацией и стабильностью. Идеально подойдет для занятий спортом, похода в
                                        школу, прогулок и путешествий…</p>

                                    <div class="row marg">
                                        <div class="col-xs-12 rowBottom">
                                            <p class="mailNews"><img src="img/home/mail.svg" alt="">&nbsp; 5</p>
                                            <p>20 января, 2017</p>
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                        <div class=" col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <span class="glyphicon glyphicon-heart"></span>
                                <img class="img-responsive" src="/img/Bitmap.jpg" alt="">
                                <div class="caption">
                                    <h3 class="longName">Длинное название поста</h3>
                                    <p>Яркая, легкая, универсальная модель для детей и подростков классического,
                                        спортивного стиля оригинального, красочного дизайна с низким профилем, отличной
                                        амортизацией и стабильностью. Идеально подойдет для занятий спортом, похода в
                                        школу, прогулок и путешествий…</p>

                                    <div class="row marg">
                                        <div class="col-xs-12 rowBottom">
                                            <p class="mailNews"><img src="img/home/mail.svg" alt="">&nbsp; 5</p>
                                            <p>20 января, 2017</p>
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                        <div class=" col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <span class="glyphicon glyphicon-heart"></span>
                                <img class="img-responsive" src="/img/Bitmap.jpg" alt="">
                                <div class="caption">
                                    <h3 class="longName">Длинное название поста</h3>
                                    <p>Яркая, легкая, универсальная модель для детей и подростков классического,
                                        спортивного стиля оригинального, красочного дизайна с низким профилем, отличной
                                        амортизацией и стабильностью. Идеально подойдет для занятий спортом, похода в
                                        школу, прогулок и путешествий…</p>

                                    <div class="row marg">
                                        <div class="col-xs-12 rowBottom">
                                            <p class="mailNews"><img src="img/home/mail.svg" alt="">&nbsp; 5</p>
                                            <p>20 января, 2017</p>
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                        <div class=" col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <span class="glyphicon glyphicon-heart"></span>
                                <img class="img-responsive" src="/img/Bitmap.jpg" alt="">
                                <div class="caption">
                                    <h3 class="longName">Длинное название поста</h3>
                                    <p>Яркая, легкая, универсальная модель для детей и подростков классического,
                                        спортивного стиля оригинального, красочного дизайна с низким профилем, отличной
                                        амортизацией и стабильностью. Идеально подойдет для занятий спортом, похода в
                                        школу, прогулок и путешествий…</p>

                                    <div class="row marg">
                                        <div class="col-xs-12 rowBottom">
                                            <p class="mailNews"><img src="img/home/mail.svg" alt="">&nbsp; 5</p>
                                            <p>20 января, 2017</p>
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                        <div class=" col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <span class="glyphicon glyphicon-heart"></span>
                                <img class="img-responsive" src="/img/Bitmap.jpg" alt="">
                                <div class="caption">
                                    <h3 class="longName">Длинное название поста</h3>
                                    <p>Яркая, легкая, универсальная модель для детей и подростков классического,
                                        спортивного стиля оригинального, красочного дизайна с низким профилем, отличной
                                        амортизацией и стабильностью. Идеально подойдет для занятий спортом, похода в
                                        школу, прогулок и путешествий…</p>

                                    <div class="row marg">
                                        <div class="col-xs-12 rowBottom">
                                            <p class="mailNews"><img src="img/home/mail.svg" alt="">&nbsp; 5</p>
                                            <p>20 января, 2017</p>
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                        <div class=" col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <span class="glyphicon glyphicon-heart"></span>
                                <img class="img-responsive" src="/img/Bitmap.jpg" alt="">
                                <div class="caption">
                                    <h3 class="longName">Длинное название поста</h3>
                                    <p>Яркая, легкая, универсальная модель для детей и подростков классического,
                                        спортивного стиля оригинального, красочного дизайна с низким профилем, отличной
                                        амортизацией и стабильностью. Идеально подойдет для занятий спортом, похода в
                                        школу, прогулок и путешествий…</p>

                                    <div class="row marg">
                                        <div class="col-xs-12 rowBottom">
                                            <p class="mailNews"><img src="img/home/mail.svg" alt="">&nbsp; 5</p>
                                            <p>20 января, 2017</p>
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>


                    </div>
                    <!--                block News-->

                </div>

                <!--                btnNews-->
                <div class="col-xs-12 allBlockNews">
                    <a href="#" class="btn btnAllBlock btn-block btnNews">Показать все посты</a>
                </div>
                <!--                btnNews-->


            </div>
        </div>
    </div>


    <div class="row subscription ">


        <div class="container">
            <div class="row body">

                <div class="col-sm-6 content">
                    <!--    TITLE PAGE-->

                    <h3>Оформите подписку</h3>
                    <ul class="list-unstyled">
                        <li><img src="/img/category/check.svg" alt=""> Получате на почту новые объявления
                        </li>
                        <li><img src="/img/category/check.svg" alt=""> Читайте наш блог с почты</li>
                        <li><img src="/img/category/check.svg" alt=""> Пулучайте уведомления о активности
                            пользователей
                        </li>
                    </ul>
                </div>
                <div class="col-sm-5 col-sm-offset-1 text-right">
                    <form role="form">
                        <div class="form-group">
                            <input type="email" class="form-control" id="exampleInputEmail1"
                                   placeholder="example_mail@news.com">
                        </div>


                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Я согласен на рассылку
                            </label>
                        </div>
                        <button type="submit" class="btn btn-default
     btn-lg btnSubscript">Подписаться
                        </button>
                    </form>
                </div>


            </div>

        </div>
    </div>


    <div class="row buildingMaterial wrapp">


        <div class="container">
            <div class="row body">

                <?= $this->render('//blocks/product-slider', [
                    'title'  => 'Строительные материалы',
                    'slides' => [
                        [
                            'minislide'     => [
                                ['src' => '/img/Bitmap.jpg',],
                                ['src' => '/img/mitsubishi1.jpg'],
                                ['src' => '/img/mitsubishi2.jpg'],
                            ],
                            'product_name'  => 'Длинное название товара',
                            'category'      => 'Транспорт',
                            'price'         => 100,
                            'currency'      => 'rub',
                            'min_period'    => 1,
                            'step'          => 'Day',
                            'countComments' => 5,
                        ],
                        [
                            'minislide'     => [
                                ['src' => '/img/Bitmap.jpg',],
                                ['src' => '/img/mitsubishi1.jpg'],
                                ['src' => '/img/mitsubishi2.jpg'],
                            ],
                            'product_name'  => 'Длинное название товара',
                            'category'      => 'Транспорт',
                            'price'         => 200,
                            'currency'      => 'usd',
                            'min_period'    => 1,
                            'step'          => 'Day',
                            'countComments' => 5,
                        ],
                        [
                            'minislide'     => [
                                ['src' => '/img/Bitmap.jpg',],
                                ['src' => '/img/mitsubishi1.jpg'],
                                ['src' => '/img/mitsubishi2.jpg'],
                            ],
                            'product_name'  => 'Оооооочень длинное название товара',
                            'category'      => 'Транспорт',
                            'price'         => 100,
                            'currency'      => 'rub',
                            'min_period'    => 1,
                            'step'          => 'Day',
                            'countComments' => 5,
                        ],
                        [
                            'minislide'     => [
                                ['src' => '/img/Bitmap.jpg',],
                                ['src' => '/img/mitsubishi1.jpg'],
                                ['src' => '/img/mitsubishi2.jpg'],
                            ],
                            'product_name'  => 'Длинное название товара',
                            'category'      => 'Транспорт',
                            'price'         => 100,
                            'currency'      => 'rub',
                            'min_period'    => 1,
                            'step'          => 'Week',
                            'countComments' => 12,
                        ],
                    ],
                ]) ?>

                <div class="col-sm-12 allBlock">
                    <a href="#" class="btn btn-block btnAllBlock text-center">Посмотреть категорию</a>
                </div>

            </div>
        </div>
    </div>


    <div class="row earn wrapp">


        <div class="container">
            <div class="row body">

                <div class="col-sm-6 col-xs-12 leftBlock">
                    <h3>Зарабатывай с <span>Wearent</span></h3>
                    <p>Рассчитай сколько ты можешь заработать на сдаче<br> в аренду вашего товара.</p>
                    <a href="#" class="btn btn-lg">Посмотреть калькулятор</a>
                </div>
                <div class="col-sm-6 col-xs-12 rightBlock text-right">
                    <span><img src="/img/category/each.svg" alt=""></span>
                </div>

            </div>
        </div>
    </div>


    <div class="row autoMoto wrapp">


        <div class="container">
            <div class="row body">


                <div class="col-sm-12 content">
                    <!--    TITLE PAGE-->
                    <div class="row  title">
                        <div class="col-sm-7 text-left">
                            <h3>Авто мото</h3>
                        </div>
                        <div class="col-sm-5 hidden-xs leftSlider text-right form">
                        </div>
                    </div>
                    <!--    END TITLE PAGE-->

                    <div class=" row visited">
                        <div class="col-sm-12">

                            <div class="media">
                                <div class="row slider2">

                                    <div class="col-sm-6 col-md-4 ">
                                        <div class="thumbnail">
                                            <span class="glyphicon glyphicon-heart"></span>
                                            <img src="/img/Bitmap.jpg" alt="fon">
                                            <div class="caption">
                                                <h3 class="longName">Длинное название товара</h3>
                                                <a href="#" class="btn" role="button">Транспорт</a>
                                                <table class="table-block table-responsive">
                                                    <tr>
                                                        <td>Цена за сутки</td>
                                                        <td class="text-right"><strong>100 руб.</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Мин. срок аренды</td>
                                                        <td class="text-right"><strong>1 день</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Оценка товара</td>
                                                        <td class="text-right">
                                                            <div class="star-rating">
                                                                <input type="radio" name="example" class="rating"
                                                                       value="1"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="2"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="3"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="4"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="5"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="row marg">
                                                    <div class="col-xs-12 rowBottom">
                                                        <div class="margLeft">
                                                            <span class="face">
                                                            <a href="#">
                                                                <img class="menFase"
                                                                     src="/img/category/man.png"
                                                                     alt="">
                                                            </a>
                                                            </span>
                                                            <span class="mail">

                                                                <img class="menFase"
                                                                     src="img/home/mail.svg" alt="">&nbsp; 5

                                                            </span>
                                                        </div>


                                                        <span class="leftRightBtn"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-4 ">
                                        <div class="thumbnail">
                                            <span class="glyphicon glyphicon-heart"></span>
                                            <img src="/img/Bitmap.jpg" alt="fon">
                                            <div class="caption">
                                                <h3 class="longName">Длинное название товара</h3>
                                                <a href="#" class="btn" role="button">Транспорт</a>
                                                <table class="table-block table-responsive">
                                                    <tr>
                                                        <td>Цена за сутки</td>
                                                        <td class="text-right"><strong>100 руб.</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Мин. срок аренды</td>
                                                        <td class="text-right"><strong>1 день</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Оценка товара</td>
                                                        <td class="text-right">
                                                            <div class="star-rating">
                                                                <input type="radio" name="example" class="rating"
                                                                       value="1"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="2"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="3"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="4"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="5"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="row marg">
                                                    <div class="col-xs-12 rowBottom">
                                                        <div class="margLeft">
                                                            <span class="face">
                                                            <a href="#">
                                                                <img class="menFase"
                                                                     src="/img/category/man.png"
                                                                     alt="">
                                                            </a>
                                                            </span>
                                                            <span class="mail">

                                                                <img class="menFase"
                                                                     src="img/home/mail.svg" alt="">&nbsp; 5

                                                            </span>
                                                        </div>


                                                        <span class="leftRightBtn"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-4 ">
                                        <div class="thumbnail">
                                            <span class="glyphicon glyphicon-heart"></span>
                                            <img src="/img/Bitmap.jpg" alt="fon">
                                            <div class="caption">
                                                <h3 class="longName">Длинное название товара</h3>
                                                <a href="#" class="btn" role="button">Транспорт</a>
                                                <table class="table-block table-responsive">
                                                    <tr>
                                                        <td>Цена за сутки</td>
                                                        <td class="text-right"><strong>100 руб.</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Мин. срок аренды</td>
                                                        <td class="text-right"><strong>1 день</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Оценка товара</td>
                                                        <td class="text-right">
                                                            <div class="star-rating">
                                                                <input type="radio" name="example" class="rating"
                                                                       value="1"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="2"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="3"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="4"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="5"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="row marg">
                                                    <div class="col-xs-12 rowBottom">
                                                        <div class="margLeft">
                                                            <span class="face">
                                                            <a href="#">
                                                                <img class="menFase"
                                                                     src="/img/category/man.png"
                                                                     alt="">
                                                            </a>
                                                            </span>
                                                            <span class="mail">

                                                                <img class="menFase"
                                                                     src="img/home/mail.svg" alt="">&nbsp; 5

                                                            </span>
                                                        </div>


                                                        <span class="leftRightBtn"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-4 ">
                                        <div class="thumbnail">
                                            <span class="glyphicon glyphicon-heart"></span>
                                            <img src="/img/Bitmap.jpg" alt="fon">
                                            <div class="caption">
                                                <h3 class="longName">Длинное название товара</h3>
                                                <a href="#" class="btn" role="button">Транспорт</a>
                                                <table class="table-block table-responsive">
                                                    <tr>
                                                        <td>Цена за сутки</td>
                                                        <td class="text-right"><strong>100 руб.</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Мин. срок аренды</td>
                                                        <td class="text-right"><strong>1 день</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Оценка товара</td>
                                                        <td class="text-right">
                                                            <div class="star-rating">
                                                                <input type="radio" name="example" class="rating"
                                                                       value="1"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="2"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="3"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="4"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="5"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="row marg">
                                                    <div class="col-xs-12 rowBottom">
                                                        <div class="margLeft">
                                                            <span class="face">
                                                            <a href="#">
                                                                <img class="menFase"
                                                                     src="/img/category/man.png"
                                                                     alt="">
                                                            </a>
                                                            </span>
                                                            <span class="mail">

                                                                <img class="menFase"
                                                                     src="img/home/mail.svg" alt="">&nbsp; 5

                                                            </span>
                                                        </div>


                                                        <span class="leftRightBtn"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-4 ">
                                        <div class="thumbnail">
                                            <span class="glyphicon glyphicon-heart"></span>
                                            <img src="/img/Bitmap.jpg" alt="fon">
                                            <div class="caption">
                                                <h3 class="longName">Длинное название товара</h3>
                                                <a href="#" class="btn" role="button">Транспорт</a>
                                                <table class="table-block table-responsive">
                                                    <tr>
                                                        <td>Цена за сутки</td>
                                                        <td class="text-right"><strong>100 руб.</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Мин. срок аренды</td>
                                                        <td class="text-right"><strong>1 день</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Оценка товара</td>
                                                        <td class="text-right">
                                                            <div class="star-rating">
                                                                <input type="radio" name="example" class="rating"
                                                                       value="1"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="2"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="3"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="4"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="5"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="row marg">
                                                    <div class="col-xs-12 rowBottom">
                                                        <div class="margLeft">
                                                            <span class="face">
                                                            <a href="#">
                                                                <img class="menFase"
                                                                     src="/img/category/man.png"
                                                                     alt="">
                                                            </a>
                                                            </span>
                                                            <span class="mail">

                                                                <img class="menFase"
                                                                     src="img/home/mail.svg" alt="">&nbsp; 5

                                                            </span>
                                                        </div>


                                                        <span class="leftRightBtn"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>

                        </div>
                    </div>

                </div>
                <div class="col-sm-12 allBlock">
                    <a href="#" class="btn btn-block btnAllBlock text-center">Посмотреть категорию</a>
                </div>


            </div>
        </div>
    </div>


    <div class="row unusualGoods wrapp">


        <div class="container">
            <div class="row body">


                <div class="col-sm-12 content">
                    <!--    TITLE PAGE-->
                    <div class="row  title">
                        <div class="col-sm-7 text-left">
                            <h3>Необычные товары</h3>
                        </div>
                        <div class="col-sm-5 hidden-xs leftSlider text-right form">
                        </div>
                    </div>
                    <!--    END TITLE PAGE-->

                    <div class=" row visited">
                        <div class="col-sm-12">

                            <div class="media">
                                <div class="row slider2">
                                    <div class="col-sm-6 col-md-4 ">
                                        <div class="thumbnail">
                                            <span class="glyphicon glyphicon-heart"></span>
                                            <img src="/img/Bitmap.jpg" alt="fon">
                                            <div class="caption">
                                                <h3 class="longName">Длинное название товара</h3>
                                                <a href="#" class="btn" role="button">Транспорт</a>
                                                <table class="table-block table-responsive">
                                                    <tr>
                                                        <td>Цена за сутки</td>
                                                        <td class="text-right"><strong>100 руб.</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Мин. срок аренды</td>
                                                        <td class="text-right"><strong>1 день</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Оценка товара</td>
                                                        <td class="text-right">
                                                            <div class="star-rating">
                                                                <input type="radio" name="example" class="rating"
                                                                       value="1"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="2"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="3"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="4"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="5"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="row marg">
                                                    <div class="col-xs-12 rowBottom">
                                                        <div class="margLeft">
                                                            <span class="face">
                                                            <a href="#">
                                                                <img class="menFase"
                                                                     src="/img/category/man.png"
                                                                     alt="">
                                                            </a>
                                                            </span>
                                                            <span class="mail">

                                                                <img class="menFase"
                                                                     src="img/home/mail.svg" alt="">&nbsp; 5

                                                            </span>
                                                        </div>


                                                        <span class="leftRightBtn"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-4 ">
                                        <div class="thumbnail">
                                            <span class="glyphicon glyphicon-heart"></span>
                                            <img src="/img/Bitmap.jpg" alt="fon">
                                            <div class="caption">
                                                <h3 class="longName">Длинное название товара</h3>
                                                <a href="#" class="btn" role="button">Транспорт</a>
                                                <table class="table-block table-responsive">
                                                    <tr>
                                                        <td>Цена за сутки</td>
                                                        <td class="text-right"><strong>100 руб.</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Мин. срок аренды</td>
                                                        <td class="text-right"><strong>1 день</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Оценка товара</td>
                                                        <td class="text-right">
                                                            <div class="star-rating">
                                                                <input type="radio" name="example" class="rating"
                                                                       value="1"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="2"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="3"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="4"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="5"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="row marg">
                                                    <div class="col-xs-12 rowBottom">
                                                        <div class="margLeft">
                                                            <span class="face">
                                                            <a href="#">
                                                                <img class="menFase"
                                                                     src="/img/category/man.png"
                                                                     alt="">
                                                            </a>
                                                            </span>
                                                            <span class="mail">

                                                                <img class="menFase"
                                                                     src="img/home/mail.svg" alt="">&nbsp; 5

                                                            </span>
                                                        </div>


                                                        <span class="leftRightBtn"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-4 ">
                                        <div class="thumbnail">
                                            <span class="glyphicon glyphicon-heart"></span>
                                            <img src="/img/Bitmap.jpg" alt="fon">
                                            <div class="caption">
                                                <h3 class="longName">Длинное название товара</h3>
                                                <a href="#" class="btn" role="button">Транспорт</a>
                                                <table class="table-block table-responsive">
                                                    <tr>
                                                        <td>Цена за сутки</td>
                                                        <td class="text-right"><strong>100 руб.</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Мин. срок аренды</td>
                                                        <td class="text-right"><strong>1 день</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Оценка товара</td>
                                                        <td class="text-right">
                                                            <div class="star-rating">
                                                                <input type="radio" name="example" class="rating"
                                                                       value="1"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="2"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="3"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="4"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="5"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="row marg">
                                                    <div class="col-xs-12 rowBottom">
                                                        <div class="margLeft">
                                                            <span class="face">
                                                            <a href="#">
                                                                <img class="menFase"
                                                                     src="/img/category/man.png"
                                                                     alt="">
                                                            </a>
                                                            </span>
                                                            <span class="mail">

                                                                <img class="menFase"
                                                                     src="img/home/mail.svg" alt="">&nbsp; 5

                                                            </span>
                                                        </div>


                                                        <span class="leftRightBtn"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-4 ">
                                        <div class="thumbnail">
                                            <span class="glyphicon glyphicon-heart"></span>
                                            <img src="/img/Bitmap.jpg" alt="fon">
                                            <div class="caption">
                                                <h3 class="longName">Длинное название товара</h3>
                                                <a href="#" class="btn" role="button">Транспорт</a>
                                                <table class="table-block table-responsive">
                                                    <tr>
                                                        <td>Цена за сутки</td>
                                                        <td class="text-right"><strong>100 руб.</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Мин. срок аренды</td>
                                                        <td class="text-right"><strong>1 день</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Оценка товара</td>
                                                        <td class="text-right">
                                                            <div class="star-rating">
                                                                <input type="radio" name="example" class="rating"
                                                                       value="1"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="2"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="3"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="4"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="5"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="row marg">
                                                    <div class="col-xs-12 rowBottom">
                                                        <div class="margLeft">
                                                            <span class="face">
                                                            <a href="#">
                                                                <img class="menFase"
                                                                     src="/img/category/man.png"
                                                                     alt="">
                                                            </a>
                                                            </span>
                                                            <span class="mail">

                                                                <img class="menFase"
                                                                     src="img/home/mail.svg" alt="">&nbsp; 5

                                                            </span>
                                                        </div>


                                                        <span class="leftRightBtn"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-4 ">
                                        <div class="thumbnail">
                                            <span class="glyphicon glyphicon-heart"></span>
                                            <img src="/img/Bitmap.jpg" alt="fon">
                                            <div class="caption">
                                                <h3 class="longName">Длинное название товара</h3>
                                                <a href="#" class="btn" role="button">Транспорт</a>
                                                <table class="table-block table-responsive">
                                                    <tr>
                                                        <td>Цена за сутки</td>
                                                        <td class="text-right"><strong>100 руб.</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Мин. срок аренды</td>
                                                        <td class="text-right"><strong>1 день</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Оценка товара</td>
                                                        <td class="text-right">
                                                            <div class="star-rating">
                                                                <input type="radio" name="example" class="rating"
                                                                       value="1"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="2"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="3"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="4"/>
                                                                <input type="radio" name="example" class="rating"
                                                                       value="5"/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="row marg">
                                                    <div class="col-xs-12 rowBottom">
                                                        <div class="margLeft">
                                                            <span class="face">
                                                            <a href="#">
                                                                <img class="menFase"
                                                                     src="/img/category/man.png"
                                                                     alt="">
                                                            </a>
                                                            </span>
                                                            <span class="mail">

                                                                <img class="menFase"
                                                                     src="img/home/mail.svg" alt="">&nbsp; 5

                                                            </span>
                                                        </div>


                                                        <span class="leftRightBtn"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>

                </div>
                <div class="col-sm-12 allBlock">
                    <a href="#" class="btn btn-block btnAllBlock text-center">Посмотреть категорию</a>
                </div>


            </div>
        </div>
    </div>


    <div class="row statistics">


        <div class="container">
            <div class="row body">

                <div class="col-sm-12 content">
                    <div class="row  title">
                        <div class="col-sm-12 text-left">
                            <h3>Живая статистика сделок</h3>
                        </div>

                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <tr class="caption">
                                <td>Кто</td>
                                <td>Тип сделки</td>
                                <td>Какой товар</td>
                                <td>Срок аренды</td>
                                <td>Когда</td>
                            </tr>
                            <tr>
                                <td class="tdStrong">Дмитрий Д.</td>
                                <td><span class="took ">
                                        <img src="/img/statistic/took.svg" alt="">
                                    </span> взял в аренду
                                </td>
                                <td class="tdStrong">Труповозка</td>
                                <td class="tdStrong">1 месяц</td>
                                <td>1 минуту назад</td>
                            </tr>
                            <tr>
                                <td class="tdStrong">Виталий Р.</td>
                                <td><span class="gave">
                                        <img src="/img/statistic/gave.svg" alt="">
                                    </span> сдал в аренду
                                </td>
                                <td class="tdStrong">Телевизор LG</td>
                                <td class="tdStrong">1 неделя</td>
                                <td>1 минуту назад</td>
                            </tr>
                            <tr>
                                <td class="tdStrong">Станислав Г</td>
                                <td><span class="gave">
                                        <img src="/img/statistic/gave.svg" alt="">
                                    </span> сдал в аренду
                                </td>
                                <td class="tdStrong">Dacia Logan</td>
                                <td class="tdStrong">2 года</td>
                                <td>1 минуту назад</td>
                            </tr>
                            <tr>
                                <td class="tdStrong">Андрей Х.</td>
                                <td><span class="took ">
                                        <img src="/img/statistic/took.svg" alt="">
                                    </span> взял в аренду
                                </td>
                                <td class="tdStrong">Жена соседа</td>
                                <td class="tdStrong">3 дня</td>
                                <td>1 минуту назад</td>
                            </tr>

                        </table>
                    </div>

                </div>

            </div>
        </div>
    </div>


    <div class="row number ">


        <div class="container">
            <div class="row body">

                <div class="col-sm-12 numberBlock ">

                    <div class="leftNumberBlock column">
                        <span><img src="/img/category/leftNumber.svg" alt=""></span>
                        <div class="blockText">
                            <p class=" bigText text-left">100+</p>
                            <p class="smallText text-left">Всего товаров<br> на сайте</p>
                        </div>
                    </div>

                    <div class="centerNumberBlock column">
                        <span><img src="/img/category/centerNumber.svg" alt=""></span>
                        <div class="blockText">
                            <p class=" bigText text-left">100+</p>
                            <p class="smallText text-left">Всего сделок<br> на сайте</p>
                        </div>
                    </div>

                    <div class="rightNumberBlock column">
                        <span><img src="/img/category/rightNumber.svg" alt=""></span>
                        <div class="blockText">
                            <p class=" bigText text-left">100+</p>
                            <p class="smallText text-left">Недовольных<br> пользователей</p>
                        </div>
                    </div>


                </div>


            </div>

        </div>
    </div>




<?php
$this->registerJsFile('/js/grom.js', [
    'position' => \yii\web\View::POS_END,
    'depends'  => ['frontend\assets\AppAsset',],
]);

$listCategoryPopup = \yii\helpers\Json::encode($listPopup);

$this->registerJs(<<<JS
var categoryNestedList1 = {$listCategoryPopup};
JS
    , \yii\web\View::POS_BEGIN);
