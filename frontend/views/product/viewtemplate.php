<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 16.04.2017
 * Time: 0:18
 */

use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;

/** @var \yii\web\View $this */
/** @var \common\models\Product $model */

\frontend\assets\FrontendAsset::register($this);
\yii\bootstrap\BootstrapPluginAsset::register($this);
\frontend\assets\ReadmoreAsset::register($this);
\frontend\assets\yyAsset::register($this);
\frontend\assets\ProductAsset::register($this);
\frontend\assets\RateAsset::register($this);


$a           = array_filter([$model->seo_title, $model->seo_h1, $model->name], function($v) {
    return !empty($v);
});
$this->title = Html::encode(array_shift($a));

$this->registerMetaTag([
    'name'    => 'description',
    'content' => $model->seo_description,
]);
$this->registerMetaTag([
    'name'    => 'keywords',
    'content' => $model->seo_keywords,
]);

$a = [

    1 => [
        'id'   => 1,
        'name' => 'Музыка',
    ],

    3 => [
        'id'   => 3,
        'name' => 'Спорт, Отдых',
        'node' => [
            101  => [
                'id'   => 101,
                'name' => 'Велосипеды, гироскутеры, ролики, скейты',
            ],
            1011 => [
                'id'   => 3302,
                'name' => 'Лыжи, коньки, санки',
            ],
            102  => [
                'id'   => 3302,
                'name' => 'Пикник, кемпинг, пляж',
            ],
            103  => [
                'id'   => 103,
                'name' => 'Туризм, путешествия',
                'node' => [
                    3320 => [
                        'id'   => 3332,
                        'name' => 'Велосипеды, гироскутеры, ролики, скейты',
                    ],
                    3321 => [
                        'id'   => 3333,
                        'name' => 'Лыжи, коньки, санки',
                    ],
                    3322 => [
                        'id'   => 3334,
                        'name' => 'Пикник, кемпинг, пляж',
                    ],
                    3323 => [
                        'id'   => 3335,
                        'name' => 'Туризм, путешествия',
                    ],
                    3324 => [
                        'id'   => 3336,
                        'name' => 'Спортивные игры',
                    ],
                    3325 => [
                        'id'   => 3337,
                        'name' => 'Дайвинг, водный спорт',
                    ],
                    3326 => [
                        'id'   => 3338,
                        'name' => 'Услуги инструкторов',
                    ],
                    3327 => [
                        'id'   => 3339,
                        'name' => 'Аренда спортивных помещений',
                    ],
                    3328 => [
                        'id'   => 3340,
                        'name' => 'Картинг',
                    ],
                    3329 => [
                        'id'   => 3341,
                        'name' => 'Тренажеры, фитнес, единоборства',
                    ],
                    3330 => [
                        'id'   => 3342,
                        'name' => 'Пневматика и снаряжение',
                    ],

                ],
            ],
            104  => [
                'id'   => 3304,
                'name' => 'Спортивные игры',
            ],
            105  => [
                'id'   => 3305,
                'name' => 'Дайвинг, водный спорт',
            ],
            106  => [
                'id'   => 3306,
                'name' => 'Услуги инструкторов',
            ],
            107  => [
                'id'   => 3307,
                'name' => 'Аренда спортивных помещений',
            ],
            108  => [
                'id'   => 3308,
                'name' => 'Картинг',
            ],
            109  => [
                'id'   => 3309,
                'name' => 'Тренажеры, фитнес, единоборства',
            ],
            1010 => [
                'id'   => 3310,
                'name' => 'Пневматика и снаряжение',
            ],

        ],
    ],

    4 => [
        'id'   => 331,
        'name' => 'Фото, Видео',
    ],

    5 => [
        'id'   => 332,
        'name' => 'Праздники и события',
    ],

    6 => [
        'id'   => 333,
        'name' => 'Инструменты и спец. техника',
    ],

    2 => [
        'id'   => 334,
        'name' => 'Детские товары',
    ],

    7 => [
        'id'   => 334,
        'name' => 'Авто Мото',
    ],

    8 => [
        'id'   => 335,
        'name' => 'Красота и здоровье',
    ],

    9 => [
        'id'   => 336,
        'name' => 'Бизнес',
    ],

    10 => [
        'id'   => 337,
        'name' => 'Одежда, обувь, акссесуары',
    ],

    11 => [
        'id'   => 337,
        'name' => 'Бытовая техника',
    ],

    12 => [
        'id'   => 337,
        'name' => 'Все для дома',
    ],

    13 => [
        'id'   => 337,
        'name' => 'Дача, сад',
    ],


];

$when = [
    1 => [
        'id'   => 1,
        'name' => 'Россия, Москва',
    ],

    2 => [
        'id'   => 2,
        'name' => 'Московская область, Мытищи улица Новосельева',
    ],
    3 => [
        'id'   => 3,
        'name' => 'Россия, Москва',
    ],
    4 => [
        'id'   => 4,
        'name' => 'Московская область, Мытищи улица Новосельева',
    ],
    5 => [
        'id'   => 5,
        'name' => 'Россия, Москва',
    ],
    6 => [
        'id'   => 6,
        'name' => 'Московская область, Мытищи улица Новосельева',
    ],
];

?>

<div class="row wrapp ">
    <div class="container-fluid">
        <div class="row imgProduct">
            <div class="windowProfil">
                <div class="topTitleWindow">
                    <img class="imgProfil" src="/img/product/fotoFace.jpg" alt="">
                    <span class="greenInputFoto"></span>
                    <div class="dropdown" id="iReadAll">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                                data-toggle="dropdown" aria-expanded="true">
                            О хозяине
                            <span>
                                    <svg class="rotateBtn" width="10px" height="6px" viewBox="0 0 10 6" version="1.1">

    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"
            stroke-linejoin="round">
        <g id="#rotateBtn" transform="translate(-365.000000, -1307.000000)" stroke-width="2">
            <g id="DESCRIBTION" transform="translate(210.000000, 624.000000)">
                <g id="about" transform="translate(0.000000, 315.000000)">
                    <g id="Group-38" transform="translate(22.000000, 359.000000)">
                        <g id="Group-6">
                            <polyline id="Path-3-Copy"
                                    transform="translate(137.757144, 11.935000) scale(-1, 1) rotate(-270.000000) translate(-137.757144, -11.935000) "
                                    points="135.822144 15.6921435 139.692143 11.8221436 136.047856 8.17785643"></polyline>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                </span>
                        </button>
                    </div>
                </div>

                <div class="confirmed">
                    <ul class="list-unstyled">
                        <li class="frequency">Подтвержден на: <span>96%</span></li>
                        <li class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="96"
                                    aria-valuemin="0" aria-valuemax="100" style="width: 96%;">
                            </div>
                        </li>
                    </ul>
                </div>

            </div>
            <button type="button" class="btn showAll" data-toggle="modal" data-target="#myModal">Посмотреть 12 фото
            </button>
            <img class="img-responsive imgWrap" src="/img/KTC.jpg" alt="">
        </div>

        <div class="row ">
            <div class="container contentProduct">
                <div class="content col-sm-12">
                    <div class="col-sm-8 leftContent">
                        <div class="aboutProduct">
                            <h4 class="text-left titlePageProduct">Студия звукозаписи имени С. Шнурова, не
                                беспокоить в течении дня</h4>

                            <div class="informationProduct">
                                <div class="row">
                                    <div class="leftInformation col-sm-8">
                                        <ul class="list-unstyled list">
                                            <li class="inputList">Отдельный вход со стороны мясокомбината</li>
                                            <li class="inputList">Бабушка вахтерша</li>

                                            <li class="inputList">При входе не надо протирать ноги</li>
                                            <li class="inputList">И мыть тоже не надо</li>
                                            <li class="inputList">Можно приводить друзей</li>
                                            <li class="inputList">Отдельный вход со стороны мясокомбината</li>
                                            <li class="inputList">Бабушка вахтерша</li>

                                            <li class="inputList">При входе не надо протирать ноги</li>
                                            <li class="inputList">И мыть тоже не надо</li>
                                            <li class="inputList">Можно приводить друзей</li>
                                        </ul>

                                    </div>
                                    <div class="col-sm-4 rating">
                                        <span class="nameRating">Общая оценка</span><br>
                                        <span class="numberRating text-right">4,2</span>
                                        <span class="rateyoProd"></span>
                                    </div>
                                    <div class="col-sm-12 aboutProduct2">
                                        <h4 class="text-left">О товаре</h4>
                                        <div class="blockTexxt readmoreProduct">
                                            <p>Существует целый ряд различных факторов, оказывающих влияние на принятие
                                                решения
                                                покупателем. Одним из главных аспектов, влияющих на выбор пользователя,
                                                является
                                                описание товара, которое размещено на странице вашего
                                                интернет-магазина.</p>
                                            <p>Какой бы продукцией не торговал магазин, все его товары должны
                                                сопровождаться
                                                описаниями, так как интернет-магазин – это в первую очередь каталог:</p>
                                            <ul class="ulAboutProduct">
                                                <li class="listAboutProduct">Из которого покупатель ожидает получить всю
                                                    необходимую информацию о товаре
                                                </li>
                                                <li class="listAboutProduct">Часто владельцы таких магазинов акцентируют
                                                    свое
                                                    внимание
                                                </li>
                                                <li class="listAboutProduct">Конечно, изображения товара очень важны
                                                </li>
                                            </ul>
                                            <p class="lastP">Кроме того, они в первую очередь привлекают внимание
                                                посетителей. Тем не
                                                менее,
                                                на следующем этапе нахождения на сайте клиент переходит к изучению
                                                описания
                                                товара. <br> Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                                Ad aliquam delectus dolores esse, est eum illum ipsam nemo, nihil
                                                perferendis porro quo, repudiandae saepe temporibus ut! Aliquid
                                                assumenda dolore dolorem eligendi mollitia natus non nostrum officiis
                                                omnis quisquam quo quos, recusandae sunt? Accusantium aliquid corporis
                                                doloribus earum hic maiores porro quos repellendus! Ab, aliquam amet at
                                                aut consectetur, doloremque iure minus officia placeat reprehenderit
                                                ullam, voluptas. Alias animi architecto dicta in numquam quidem vero!
                                                Consectetur corporis eos eum fuga, harum iure laborum libero minima
                                                mollitia nemo praesentium quae quia soluta velit voluptates? Dolorem
                                                eaque hic impedit ipsam quia voluptatem voluptatibus?</p>
                                        </div>

                                        <span class="borderAboutProduct"></span>
                                        <div class="row bottomBlock">
                                            <div class="col-sm-6 minRent">Минимальный срок аренды: <strong>1
                                                    день</strong>
                                            </div>
                                            <div class="col-sm-6 datePickerProduct">
                                                <a href="#" class="datePickerProductA">
                                                    <svg class="imgButtDate" width="18px" height="20px"
                                                            viewBox="0 0 18 20" version="1.1"
                                                            xmlns="http://www.w3.org/2000/svg">


                                                        <path d="M17.2,1.9 L14.2,1.9 L14.2,0.6 C14.2,0.3 14,0.1 13.7,0.1 C13.4,0.1 13.2,0.3 13.2,0.6 L13.2,1.9 L4.6,1.9 L4.6,0.6 C4.6,0.3 4.4,0.1 4.1,0.1 C3.8,0.1 3.6,0.3 3.6,0.6 L3.6,1.9 L0.6,1.9 C0.3,1.9 0.1,2.1 0.1,2.4 L0.1,19.1 C0.1,19.4 0.3,19.6 0.6,19.6 L17.3,19.6 C17.6,19.6 17.8,19.4 17.8,19.1 L17.8,2.4 C17.7,2.2 17.5,1.9 17.2,1.9 Z M3.6,2.9 L3.6,4.2 C3.6,4.5 3.8,4.7 4.1,4.7 C4.4,4.7 4.6,4.5 4.6,4.2 L4.6,2.9 L13.2,2.9 L13.2,4.2 C13.2,4.5 13.4,4.7 13.7,4.7 C14,4.7 14.2,4.5 14.2,4.2 L14.2,2.9 L16.7,2.9 L16.7,7.1 L1,7.1 L1,2.9 L3.6,2.9 Z M1,18.6 L1,8.1 L16.7,8.1 L16.7,18.6 L1,18.6 Z"
                                                                id="Shape"></path>
                                                        <rect id="Rectangle-path" x="3.2" y="10.1" width="1.9"
                                                                height="1.9"></rect>
                                                        <rect id="Rectangle-path" x="12.7" y="10.1" width="1.9"
                                                                height="1.9"></rect>
                                                        <rect id="Rectangle-path" x="8" y="10.1" width="1.9"
                                                                height="1.9"></rect>
                                                        <rect id="Rectangle-path" x="3.2" y="13.9" width="1.9"
                                                                height="1.9"></rect>
                                                        <rect id="Rectangle-path" x="8" y="13.9" width="1.9"
                                                                height="1.9"></rect>

                                                    </svg>
                                                    Посмотреть свободные даты</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="ratingProductBlock">
                                <div class="row">
                                    <h4 class="nameBlock">Рейтинг товара</h4>
                                    <div class="col-sm-8 ratingBlockLeft">
                                        <table class="table-responsive">
                                            <tr class="
provider">
                                                <td class="fitrsTd">Рейтинг поставщика:</td>
                                                <td class="twoTd">5</td>
                                                <td>
                                                    <div class="rateyo"></div>
                                                </td>
                                                <td class="lastTd">
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                                aria-valuenow="60"
                                                                aria-valuemin="0" aria-valuemax="100"
                                                                style="width: 100%;">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="product">
                                                <td class="fitrsTd">Качество товара:</td>
                                                <td class="twoTd">4</td>
                                                <td>
                                                    <div class="rateyo"></div>
                                                </td>
                                                <td class="lastTd">
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                                aria-valuenow="60"
                                                                aria-valuemin="0" aria-valuemax="100"
                                                                style="width: 80%;">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="description">
                                                <td class="fitrsTd">Корректность описания:</td>
                                                <td class="twoTd">4</td>
                                                <td>
                                                    <div class="rateyo"></div>
                                                </td>
                                                <td class="lastTd">
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                                aria-valuenow="60"
                                                                aria-valuemin="0" aria-valuemax="100"
                                                                style="width: 80%;">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="
other">
                                                <td class="fitrsTd">Еще что нибудь:</td>
                                                <td class="twoTd">3</td>
                                                <td>
                                                    <div class="rateyo"></div>
                                                </td>
                                                <td class="lastTd">
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                                aria-valuenow="60"
                                                                aria-valuemin="0" aria-valuemax="100"
                                                                style="width: 60%;">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>


                                    </div>
                                    <div class="col-sm-4 text-right ratingBlockRight">
                                        <span class="nameRating">Общая оценка</span><br>
                                        <span class="numberRating">4,2</span>
                                        <span class="rateyoProd"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="reviews">
                                <div class="row">

                                    <div class="col-sm-12 titleReviews">
                                        <h4>Отзывы о товаре <span>(34)</span></h4>
                                    </div>
                                    <!-- first reviews  -->
                                    <div class="col-sm-12 reviewsBlock">
                                        <div class="row">

                                            <div class="col-sm-1 ">
                                                <img class="fotoMan" src="/img/product/fotoFace.jpg" alt="">
                                                <span class="greenInputFoto"></span>
                                            </div>

                                            <div class="col-sm-11 textReviews">
                                                <div class="name inlineBlock">
                                                    <div class="leftInlineBlock">
                                                        <h5 class="nameManReviews">Виталий Разгулин</h5>
                                                        <span class="time">• 1 час назад</span>
                                                    </div>
                                                    <div class="rightInineBlock dropdown">

                                                        <button id="dLabel" type="button" data-toggle="dropdown"
                                                                aria-haspopup="true" role="button"
                                                                aria-expanded="false">
                                                            <span class="circle"></span>
                                                            <span class="circle"></span>
                                                            <span class="circle"></span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu"
                                                                aria-labelledby="dLabel">
                                                            <li><a href="#"> Редактировать</a></li>
                                                            <li><a href="#">Сообщить о спаме</a></li>
                                                        </ul>

                                                    </div>
                                                </div>
                                                <p class="massage readmoreFedback">Существует целый ряд различных
                                                    факторов, оказывающих влияние на принятие решения покупателем.<br>Существует
                                                    целый ряд различных факторов, оказывающих влияние на принятие
                                                    решения покупателем.</p>
                                                <div class="bottomBlockReview inlineBlock">
                                                    <span></span>
                                                    <span class="like12">
<svg width="19px" height="18px" viewBox="0 0 19 18" version="1.1" xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink">

    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g class="borderHeard" transform="translate(-819.000000, -1976.000000)" fill="#d8d8d8">
            <g id="REVIEWS-Copy" transform="translate(211.000000, 1664.000000)">
                <path d="M617.847182,329.658704 L617.5,329.811631 L617.152818,329.658704 C612.690409,326.906456 608,320.641669 608,317.162149 C608.015114,314.313018 610.333545,312 613.181818,312 C614.982068,312 616.571159,312.924512 617.5,314.32388 C618.428841,312.924512 620.017932,312 621.818182,312 C624.666455,312 626.984886,314.313018 627,317.162149 C627,320.641669 622.309591,326.906456 617.847182,329.658704 L617.847182,329.658704 Z M621.818182,312.864992 C620.369864,312.864992 619.02475,313.589223 618.218977,314.802645 L617.5,315.885732 L616.781023,314.80221 C615.97525,313.589223 614.630136,312.864992 613.181818,312.864992 C610.813727,312.864992 608.876159,314.794825 608.863636,317.162149 C608.863636,320.231113 613.282864,326.20221 617.5,328.856273 C621.717136,326.20221 626.136364,320.231547 626.136364,317.166494 C626.123841,314.794825 624.186273,312.864992 621.818182,312.864992 L621.818182,312.864992 Z"
                        id="Shape"></path>
            </g>
        </g>
    </g>
</svg> 22</span>
                                                </div>
                                            </div>
                                            <span class="borderAboutProduct"></span>
                                        </div>
                                    </div>
                                    <!--                                secong reviews -->
                                    <span class="borderReviewsBlock"></span>
                                    <div class="col-sm-12 reviewsBlock">
                                        <div class="row">

                                            <div class="col-sm-1">
                                                <img class="fotoMan" src="/img/product/fotoFace.jpg" alt="">
                                                <span class="greenInputFoto"></span>
                                            </div>

                                            <div class="col-sm-11 textReviews">
                                                <div class="name inlineBlock">
                                                    <div class="leftInlineBlock">
                                                        <h5 class="nameManReviews">Доба Дмитрий</h5>
                                                        <span class="time">• 1 час назад</span>
                                                    </div>
                                                    <div class="rightInineBlock dropdown">

                                                        <button id="dLabel" type="button" data-toggle="dropdown"
                                                                aria-haspopup="true" role="button"
                                                                aria-expanded="false">
                                                            <span class="circle"></span>
                                                            <span class="circle"></span>
                                                            <span class="circle"></span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu"
                                                                aria-labelledby="dLabel">
                                                            <li><a href="#"> Редактировать</a></li>
                                                            <li><a href="#">Сообщить о спаме</a></li>
                                                        </ul>

                                                    </div>
                                                </div>
                                                <p class="massage readmoreFedback">Существует целый ряд различных
                                                    факторов,
                                                    оказывающих
                                                    влияние
                                                    на принятие решения покупателем.<br>Существует целый ряд различных
                                                    факторов,
                                                    оказывающих
                                                    влияние
                                                    на принятие решения покупателем</p>
                                                <div class="bottomBlockReview inlineBlock">
                                                    <span></span>
                                                    <span class="like"><svg width="19px" height="18px"
                                                                viewBox="0 0 19 18" version="1.1"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                xmlns:xlink="http://www.w3.org/1999/xlink">

    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g class="borderHeard" transform="translate(-819.000000, -1976.000000)" fill="#d8d8d8">
            <g id="REVIEWS-Copy" transform="translate(211.000000, 1664.000000)">
                <path d="M617.847182,329.658704 L617.5,329.811631 L617.152818,329.658704 C612.690409,326.906456 608,320.641669 608,317.162149 C608.015114,314.313018 610.333545,312 613.181818,312 C614.982068,312 616.571159,312.924512 617.5,314.32388 C618.428841,312.924512 620.017932,312 621.818182,312 C624.666455,312 626.984886,314.313018 627,317.162149 C627,320.641669 622.309591,326.906456 617.847182,329.658704 L617.847182,329.658704 Z M621.818182,312.864992 C620.369864,312.864992 619.02475,313.589223 618.218977,314.802645 L617.5,315.885732 L616.781023,314.80221 C615.97525,313.589223 614.630136,312.864992 613.181818,312.864992 C610.813727,312.864992 608.876159,314.794825 608.863636,317.162149 C608.863636,320.231113 613.282864,326.20221 617.5,328.856273 C621.717136,326.20221 626.136364,320.231547 626.136364,317.166494 C626.123841,314.794825 624.186273,312.864992 621.818182,312.864992 L621.818182,312.864992 Z"
                        id="Shape"></path>
            </g>
        </g>
    </g>
</svg> 22</span>
                                                </div>
                                                <span class="borderAboutProduct"></span>

                                                <!--                                            коментрар до відгуку -->
                                                <div class="row commentReviews">
                                                    <div class="col-sm-2 ">
                                                        <img class="fotoMan" src="/img/product/fotoFace.jpg" alt="">
                                                        <span class="greenInputFoto"></span>
                                                    </div>
                                                    <div class="col-sm-10 textReviews">
                                                        <div class="name inlineBlock">
                                                            <div class="leftInlineBlock">
                                                                <h5 class="nameManReviews">Виталий Разгулин</h5>
                                                                <span class="time">• 1 час назад</span>
                                                            </div>
                                                            <div class="rightInineBlock dropdown">

                                                                <button id="dLabel" type="button" data-toggle="dropdown"
                                                                        aria-haspopup="true" role="button"
                                                                        aria-expanded="false">
                                                                    <span class="circle"></span>
                                                                    <span class="circle"></span>
                                                                    <span class="circle"></span>
                                                                </button>
                                                                <ul class="dropdown-menu" role="menu"
                                                                        aria-labelledby="dLabel">
                                                                    <li><a href="#"> Редактировать</a></li>
                                                                    <li><a href="#">Сообщить о спаме</a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                        <p class="massage">Существует целый ряд различных факторов,
                                                            оказывающих влияние</p>
                                                        <img src="/img/product/fotoComment.jpg" alt="comment"
                                                                class="fotoProduct">
                                                        <div class="bottomBlockReview inlineBlock">
                                                            <span class="like text-right"> <svg width="19px"
                                                                        height="18px"
                                                                        viewBox="0 0 19 18"
                                                                        version="1.1"
                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                        xmlns:xlink="http://www.w3.org/1999/xlink">

    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g class="borderHeard" transform="translate(-819.000000, -1976.000000)" fill="#d8d8d8">
            <g id="REVIEWS-Copy" transform="translate(211.000000, 1664.000000)">
                <path d="M617.847182,329.658704 L617.5,329.811631 L617.152818,329.658704 C612.690409,326.906456 608,320.641669 608,317.162149 C608.015114,314.313018 610.333545,312 613.181818,312 C614.982068,312 616.571159,312.924512 617.5,314.32388 C618.428841,312.924512 620.017932,312 621.818182,312 C624.666455,312 626.984886,314.313018 627,317.162149 C627,320.641669 622.309591,326.906456 617.847182,329.658704 L617.847182,329.658704 Z M621.818182,312.864992 C620.369864,312.864992 619.02475,313.589223 618.218977,314.802645 L617.5,315.885732 L616.781023,314.80221 C615.97525,313.589223 614.630136,312.864992 613.181818,312.864992 C610.813727,312.864992 608.876159,314.794825 608.863636,317.162149 C608.863636,320.231113 613.282864,326.20221 617.5,328.856273 C621.717136,326.20221 626.136364,320.231547 626.136364,317.166494 C626.123841,314.794825 624.186273,312.864992 621.818182,312.864992 L621.818182,312.864992 Z"
                        id="Shape"></path>
            </g>
        </g>
    </g>
</svg> 22</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                             кінець коментаря до відгуку -->
                                            </div>

                                        </div>
                                    </div>


                                    <span class="borderAboutProduct"></span>
                                    <a href="#" class="btn allComment text-center col-sm-12">Посмотреть еще 10
                                        отзывов</a>
                                </div>

                            </div>

                            <div class="accountMan">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4 class="titleAccount">О хозяине</h4>
                                    </div>
                                    <div class="col-sm-12 borderProfil">
                                        <div class="row">
                                            <div class="col-sm-1">
                                                <img class="imgProfil" src="/img/product/fotoFace.jpg" alt="">
                                                <span class="greenInputFoto"></span>
                                            </div>
                                            <div class="col-sm-6 aboutProfil">
                                                <ul class="list-unstyled">
                                                    <li class="titleProfil"><h5>Привет! Меня зовут Аркадий и я
                                                            алкоголик</h5></li>
                                                    <li class="address">Россия, г. Москвa</li>
                                                    <li class="member">Участник с 1 августа 2016</li>
                                                    <li class="btnProfil"><a href="#">Посмотреть профиль</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-4 statisticsProfil">
                                                <ul class="list-unstyled">
                                                    <li class="frequency">Частота ответов: <span>96%</span></li>
                                                    <li class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                                aria-valuenow="60"
                                                                aria-valuemin="0" aria-valuemax="100"
                                                                style="width: 96%;">
                                                            <span class="sr-only">60% Завершено</span>
                                                        </div>
                                                    </li>
                                                    <li class="time">Время ответа: <span>5 часов</span></li>
                                                </ul>

                                            </div>
                                            <span class="borderAboutProduct"></span>
                                        </div>
                                    </div>
                                    <div class="data">
                                        <div class="col-sm-4 col-md-3">
                                            <div class="thumbnail">
                                                <img data-src="" class="text-center" alt="...">
                                                <div class="caption">
                                                    <h5 class="text-center">Не подтвержден</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-md-3">
                                            <div class="thumbnail">
                                                <img data-src="" class="text-center" alt="...">
                                                <div class="caption">
                                                    <h5 class="text-center">Лучший хозяин</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-md-3">
                                            <div class="thumbnail">
                                                <img data-src="" class="text-center" alt="...">
                                                <div class="caption">
                                                    <h5 class="text-center">Активный участник сообщества</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-md-3">
                                            <div class="thumbnail">
                                                <img data-src="" class="text-center" alt="...">
                                                <div class="caption ">
                                                    <h5 class="text-center">Награда от компании <span>Wearent</span>
                                                    </h5>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="borderAboutProduct"></span>
                                    </div>
                                    <div class="aboutMe col-sm-12">
                                        <h5 class="titleAboutMe">Немного обо мне</h5>
                                        <div class="blockaboutMe ">
                                            <p class="textAboutMe">Diana was already waiting for us and ready to welcome
                                                us.
                                                Diana had got maps and leaflets for things to do around the area. We
                                                asked
                                                for some suggestions and Diana kindly marked them out on the map for us.
                                                Showed us around the apartment and just remembered us of house
                                                requirements,
                                                which are stated in the description on the apartment on the website. In
                                                a
                                                great location, metro stations are close and</p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus
                                                adipisci asperiores blanditiis dolorem earum excepturi in ipsum, iste
                                                labore magnam nobis obcaecati officiis quae ratione reiciendis
                                                repudiandae sit voluptatem.</p>
                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-sm-4 sidebar">

                        <div class="topBlock">
                            <h4 class="title text-center">$55/за сутки</h4>

                            <div class="input-floating  whBlock">
                                <label class="labelWhat labelWhere" for="inputWhenRent">Когда арендовать?</label>
                                <label class="labelWhat infocus" for="inputWhenRent">Когда арендовать:</label>
                                <span class="datePickerProduct"><img src="/img/product/datePGrey.svg" alt=""></span>
                                <input name="fieldWhere" type="text" id="inputWhenRent"
                                        class="dateOne1 ">
                            </div>


                            <div class="input-floating whenBack whBlock">
                                <label class="labelWhat labelWhere" for="inputWhenBack">Когда вернуть?</label>
                                <label class="labelWhat infocus" for="inputWhenBack">Когда вернуть:</label>
                                <span class="datePickerProduct"><img src="/img/product/datePGrey.svg" alt=""></span>
                                <input name="fieldWhere" type="text" id="inputWhenBack"
                                        class="dateOne2">
                            </div>


                            <div class="addDetail">
                                <form action="" method="post">
                                    <input type="checkbox" id="checkInf" class="checkInf">
                                    <label for="checkInf">Дополнительные детали</label>
                                </form>


                            </div>

                            <!--                            форма для заповнення інфи про оренду-->
                            <div class="rentSidebar" style="display: none">
                                <form action="" method="post" class="formAddress ">
                                    <input type="text" placeholder="Укажите адрес доставки" class="form-control">
                                </form>

                                <span class="greenCheck greeyText"><img src="/img/product/greenBtnRight.svg"
                                            alt="Доставка"> Доставка включена в счет</span>

                                <div class="priceSidebar">

                                    <div class="inlinePrice paddBlocks inFlex">
                                        <span class="greeyText left">$55 х 2 сутки</span>
                                        <span class="boldText  right">$100</span>
                                    </div>

                                    <div class="serviceSidebar paddBlocks inFlex">
                                        <span class="greeyText left">Сбор за услуги сервиса</span>
                                        <span class="boldText right">$10</span>
                                    </div>

                                    <div class="discount paddBlocks">
                                        <div class="topDiscount inFlex ">
                                            <span class="yourDiscout greeyText">Ваша текущая скидка &nbsp;<svg
                                                        class="greyBtnDown" width="10px" height="6px"
                                                        viewBox="0 0 10 6">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"
            stroke-linejoin="round">
        <g id="greyBtnDown" transform="translate(-360.000000, -3196.000000)" stroke-width="2">
            <g id="DESCRIBTION-Copy" transform="translate(211.000000, 2623.000000)">
                <g id="Group-5">
                    <g id="Group-16" transform="translate(20.000000, 393.000000)">
                        <g id="Group-38" transform="translate(0.000000, 171.000000)">
                            <polyline id="Path-3-Copy"
                                    transform="translate(133.757144, 11.935000) scale(-1, 1) rotate(-270.000000) translate(-133.757144, -11.935000) "
                                    points="131.822144 15.6921435 135.692143 11.8221436 132.047856 8.17785643"></polyline>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span>
                                            <span class="result boldText">$10</span>
                                        </div>
                                        <div class="bottomDiscount inFlex" style="display:none;">
                                            <ul>
                                                <li>За хороший рейтинг 10% <span>$5</span></li>
                                                <li>При аренде на неделю 10%</li>
                                                <li>При аренде на месяц 25%</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="allPrice inFlex ">
                                        <span class="textPrice">Итоговая цена</span>
                                        <span class="numberPrice">$100</span>
                                    </div>
                                    <div class="pledge">
                                        <div class="topPledge inFlex">
                                            <span class="pledgeText greeyText">Сумма залога&nbsp;<svg
                                                        class="greyBtnDown" width="10px" height="6px"
                                                        viewBox="0 0 10 6">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"
            stroke-linejoin="round">
        <g id="greyBtnDown2" transform="translate(-360.000000, -3196.000000)" stroke-width="2">
            <g id="DESCRIBTION-Copy" transform="translate(211.000000, 2623.000000)">
                <g id="Group-5">
                    <g id="Group-16" transform="translate(20.000000, 393.000000)">
                        <g id="Group-38" transform="translate(0.000000, 171.000000)">
                            <polyline id="Path-3-Copy"
                                    transform="translate(133.757144, 11.935000) scale(-1, 1) rotate(-270.000000) translate(-133.757144, -11.935000) "
                                    points="131.822144 15.6921435 135.692143 11.8221436 132.047856 8.17785643"></polyline>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span>
                                            <span class="rightText boldText">$50</span>
                                        </div>
                                        <div class="bottomPredge" style="display: none">
                                            <ul>
                                                <li>По умолчанию - <span>$50</span></li>
                                                <li>Хороший рейтинг - $45</li>
                                            </ul>

                                        </div>
                                    </div>

                                </div>
                            </div>


                            <a href="#" class="btn btnTopBlock" data-toggle="modal" data-target="#rentModal">Сделать
                                запрос на бронирование</a>
                        </div>
                        <div class="bottomBlock">
                            <a href="#" class="btn add">Добавить в избранное</a>
                            <div class="input-group notClassic">
                                <label for="notClassic">Необычный товар</label>
                                <input type="text" id="notClassic" class="form-control text-center">
                                <span class="input-group-addon"></span>
                            </div>
                            <a href="#" class="btn question add">Задать вопрос хозяину</a>
                            <a href="#" class="btn violation">Сообщить о нарушении</a>
                            <span class="borderAboutProduct"></span>
                            <div class="social">
                                <span class="text-right">Поделиться с друзьями</span>
                                <div class="socialBlock">
                                    <a href="#"><img src="/img/product/mail.svg" class="mail" alt="Google+"></a>
                                    <a href="#"><img src="/img/product/Facebook_3_.svg" class="faseboock"
                                                alt="Faseboock"></a>
                                    <a href="#"><img src="/img/product/Vk.svg" class="vkontakte" alt="Vk"></a>
                                    <a href="#"><img src="/img/product/instagram.svg" class="instagram"
                                                alt="Instagram"></a>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

        </div>

        <div class=" maps">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d219657.89606273442!2d37.473895182882686!3d55.72366467267075!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54afc73d4b0c9%3A0x3d44d6cc5757cf4c!2z0JzQvtGB0LrQstCwLCDQoNC-0YHRltGP!5e0!3m2!1suk!2sua!4v1493901562816"
                    frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

        <div class="row sliderProduct">
            <div class="container">
                <div class="row body">

                    <?= $this->render('//blocks/product-slider', [
                        'title'  => 'Похожие товары',
                        'slides' => [
                            [
                                'minislide'     => [
                                    ['src' => '/img/Bitmap.jpg',],
                                    ['src' => '/img/mitsubishi1.jpg'],
                                    ['src' => '/img/mitsubishi2.jpg'],
                                ],
                                'product_name'  => 'Длинное название товара',
                                'category'      => 'Транспорт',
                                'slug'          => 'ghg',
                                'price'         => 100,
                                'currency'      => 'rub',
                                'min_period'    => 1,
                                'step'          => 'Day',
                                'countComments' => 5,
                            ],
                            [
                                'minislide'     => [
                                    ['src' => '/img/Bitmap.jpg',],
                                    ['src' => '/img/mitsubishi1.jpg'],
                                    ['src' => '/img/mitsubishi2.jpg'],
                                ],
                                'product_name'  => 'Длинное название товара',
                                'category'      => 'Транспорт',
                                'price'         => 200,
                                'currency'      => 'usd',
                                'slug'          => 'ghg',
                                'min_period'    => 1,
                                'step'          => 'Day',
                                'countComments' => 5,
                            ],
                            [
                                'minislide'     => [
                                    ['src' => '/img/Bitmap.jpg',],
                                    ['src' => '/img/mitsubishi1.jpg'],
                                    ['src' => '/img/mitsubishi2.jpg'],
                                ],
                                'product_name'  => 'Оооооочень длинное название товара',
                                'category'      => 'Транспорт',
                                'price'         => 100,
                                'currency'      => 'rub',
                                'slug'          => 'ghg',
                                'min_period'    => 1,
                                'step'          => 'Day',
                                'countComments' => 5,
                            ],
                            [
                                'minislide'     => [
                                    ['src' => '/img/Bitmap.jpg',],
                                    ['src' => '/img/mitsubishi1.jpg'],
                                    ['src' => '/img/mitsubishi2.jpg'],
                                ],
                                'product_name'  => 'Длинное название товара',
                                'category'      => 'Транспорт',
                                'price'         => 100,
                                'slug'          => 'ghg',
                                'currency'      => 'rub',
                                'min_period'    => 1,
                                'step'          => 'Week',
                                'countComments' => 12,
                            ],
                        ],
                    ]) ?>

                </div>
            </div>
        </div>

        <?php Modal::begin([
            'id'     => 'myModal',
            'header' => <<<HEADER
<div class="row">
                        <div class="col-sm-6 text-left leftHeader">
                            <span>Поделиться:</span>
                            <div class="blockSvg">
                                <a href="#" class="mailP"><svg width="24px" height="16px" viewBox="0 0 24 16" version="1.1">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="UI13.8-и-13.9-------Галерея---как-в-карточке-товара-" transform="translate(-394.000000, -37.000000)" fill="#FFFFFF">
            <g id="share" transform="translate(293.000000, 35.000000)">
                <g id="Group-2" transform="translate(101.000000, 0.000000)">
                    <path d="M1.23926278,3.20054155 C1.05648733,3.37344812 0.942399979,3.61876503 0.942399979,3.89125116 L0.942399979,15.1871485 C0.942399979,15.4601935 1.05469603,15.7063015 1.23747189,15.8794155 L1.23747189,15.8794155 L8.52871981,9.53919983 L1.23926278,3.20054155 L1.23926278,3.20054155 L1.23926278,3.20054155 L1.23926278,3.20054155 Z M22.3225276,3.19898425 C22.5053035,3.3720982 22.6175995,3.61820617 22.6175995,3.89125116 L22.6175995,15.1871485 C22.6175995,15.4596346 22.5035122,15.7049516 22.3207367,15.8778581 L15.0312797,9.53919983 L22.3225276,3.19898425 L22.3225276,3.19898425 L22.3225276,3.19898425 Z M14.326204,10.1523091 L21.2039995,16.1359997 L2.35599995,16.1359997 L9.23379548,10.1523091 L11.7799997,12.3663998 L14.326204,10.1523091 L14.326204,10.1523091 L14.326204,10.1523091 Z M1.88818775,2 C0.845370449,2 0,2.84298975 0,3.87920237 L0,15.1991973 C0,16.2370521 0.838512858,17.0783997 1.88818775,17.0783997 L21.6718117,17.0783997 C22.714629,17.0783997 23.5599995,16.2354099 23.5599995,15.1991973 L23.5599995,3.87920237 C23.5599995,2.84134756 22.7214866,2 21.6718117,2 L1.88818775,2 L1.88818775,2 L1.88818775,2 Z M11.7799997,11.1412913 L21.2039995,2.94239998 L2.35599995,2.94239998 L11.7799997,11.1412913 L11.7799997,11.1412913 L11.7799997,11.1412913 Z" id="mail-envelope-closed"></path>
                </g>
            </g>
        </g>
    </g>
</svg></a>
                                <a href="#" class="fbP"><svg width="9px" height="20px" viewBox="0 0 9 20" version="1.1" >
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="nonzero">
        <g id="UI14.2-------Дизайн-подсказок-по-ховеру" transform="translate(-529.000000, -5471.000000)" fill="#FFFFFF">
            <g id="Group-15-Copy-6" transform="translate(280.000000, 5286.000000)">
                <g id="Group-16">
                    <g id="Group-17" transform="translate(227.000000, 177.000000)">
                        <path d="M23.945245,11.6822896 L23.945245,14.2993864 L22,14.2993864 L22,17.5002088 L23.945245,17.5002088 L23.945245,27.0099224 L27.942474,27.0099224 L27.942474,17.5002088 L30.6235868,17.5002088 C30.6235868,17.5002088 30.8743072,15.9650202 30.9963423,14.2872596 L27.9567723,14.2872596 L27.9567723,12.0988619 C27.9567723,11.7720935 28.3920417,11.3319231 28.8229883,11.3319231 L31,11.3319231 L31,8 L28.0405675,8 C23.8468189,8.00032775 23.945245,11.2037722 23.945245,11.6822896 L23.945245,11.6822896 Z" id="Facebook_3_"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></a>
                                <a href="#" class="vkP"><svg width="24px" height="14px" viewBox="0 0 24 14" version="1.1" >
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="UI13.8-и-13.9-------Галерея---как-в-карточке-товара-" transform="translate(-477.000000, -38.000000)" stroke="#FFFFFF">
            <g id="share" transform="translate(293.000000, 35.000000)">
                <g id="Group-2" transform="translate(101.000000, 0.000000)">
                    <g id="1482090935_038_018_vkontakte_vk_social_network_android_material" transform="translate(84.000000, 4.000000)">
                        <g id="Layer_21">
                            <path d="M21.0694894,0.546667685 L17.5108173,0.546667685 C17.2148724,0.546665768 16.9428928,0.709374846 16.8029503,0.970141969 C16.8029503,0.970141969 15.383615,3.584065 14.9277064,4.46448617 C13.7012143,6.83291827 12.9069783,6.08938326 12.9069783,4.99017353 L12.9069783,1.20008085 C12.9069783,0.540561232 12.372332,0.00591494631 11.7128124,0.00591494631 L9.03716781,0.00591494631 C8.29629836,-0.0491696322 7.5797405,0.283327951 7.14345172,0.884638146 C7.14345172,0.884638146 8.50210379,0.664962754 8.50210379,2.49562708 C8.50210379,2.94957818 8.52548594,4.25548523 8.5467916,5.35080153 C8.55501913,5.68140665 8.35616627,5.98205148 8.04870651,6.10385433 C7.74124676,6.22565718 7.39045062,6.14276057 7.17003513,5.89621556 C6.07485248,4.37300895 5.16767603,2.72308586 4.46821814,0.982298091 C4.34555254,0.716381927 4.07927107,0.546241577 3.78642627,0.546667685 L0.553146685,0.546667685 C0.372612602,0.545439173 0.20299664,0.632984429 0.0994179408,0.780854363 C-0.00416075803,0.928724297 -0.0284814128,1.1180448 0.0343593226,1.28729345 C1.01613918,3.98030698 5.2453339,12.4432279 10.0725363,12.4432279 L12.1043065,12.4432279 C12.5476116,12.443222 12.9069783,12.0838505 12.9069783,11.6405454 L12.9069783,10.4136964 C12.9069806,10.099148 13.0938758,9.81467159 13.3825709,9.68978841 C13.6712661,9.56490523 14.0065631,9.62349274 14.2358024,9.83887622 L16.6676864,12.1237728 C16.8860894,12.3289755 17.1744961,12.443215 17.4741759,12.4432279 L20.6676561,12.4432279 C22.2078821,12.4432279 22.2078821,11.374441 21.3681255,10.5474137 C20.7771369,9.96537992 18.6445487,7.71751405 18.6445487,7.71751405 C18.2672055,7.32659523 18.2312217,6.71893833 18.5597911,6.28621725 C19.2491102,5.37909372 20.3765905,3.89419751 20.8547457,3.25807762 C21.5083319,2.38861211 22.6912285,0.546667685 21.0694894,0.546667685 L21.0694894,0.546667685 Z" id="Shape"></path>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></a>
                                <a href="#" class="instP"><svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="UI13.8-и-13.9-------Галерея---как-в-карточке-товара-" transform="translate(-529.000000, -35.000000)">
            <g id="share" transform="translate(293.000000, 35.000000)">
                <g id="Group-2" transform="translate(101.000000, 0.000000)">
                    <g id="Group-7" transform="translate(136.000000, 1.000000)">
                        <rect id="Rectangle-7" stroke="#FFFFFF" x="0" y="0" width="18" height="18" rx="5"></rect>
                        <circle id="Oval-3" stroke="#FFFFFF" cx="9" cy="9" r="4"></circle>
                        <ellipse id="XMLID_143_" fill="#FFFFFF" cx="13.7838462" cy="4.1976834" rx="1.07307692" ry="1.07722008"></ellipse>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></a>
                            </div>

                        </div>
                        <div class="col-sm-6 text-right rightHeader">
                            <p><span>$55</span>/за сутки</p>
                            <a href="#" class="btn greenBtnHeader">Посмотреть свободные даты</a>
                        </div>
</div>
HEADER
            ,
            'footer' => <<<FOOTER
        <div class="slickItemDown"></div>
FOOTER
            ,

            'options' => [
//                'class' => 'container-fluid',
            ],
            'size'    => Modal::SIZE_LARGE,
        ]) ?>

        <div class="contentBody">
            <div class="slickItem">
                <img class="img-responsive" src="/img/product/popupSlider/ImgSl.jpg" alt="img1">

            </div>

        </div>
        <div class="bottomPathBody">
            <div class="blockBtnBody">
                <a href="#" class="foto">Фото</a>
                <a href="#" class="video">Видео</a>
            </div>

        </div>


        <?php Modal::end() ?>

    </div>


</div>


<?php
$this->registerJsFile('/js/grom.js', [
    'position' => \yii\web\View::POS_END,
    'depends'  => ['frontend\assets\AppAsset',],
]);

$ja = \yii\helpers\Json::encode($a);
$mw = \yii\helpers\Json::encode($when);

$this->registerJs(<<<JS
var categoryNestedList =$ja;
var categoryWhenList =$mw;

JS
    , \yii\web\View::POS_BEGIN);
?>
