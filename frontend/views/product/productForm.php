<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 27.06.2017
 * Time: 15:26
 */

use yii\bootstrap\Html;
use yii\bootstrap\Tabs;
use yii\bootstrap\ActiveForm;
use kartik\growl\Growl;
use kartik\growl\GrowlAsset;
use yii\bootstrap\ToggleButtonGroup;

/** @var \yii\web\View $this */
/** @var \frontend\models\ProductForm $model */
/** @var string $backUrl */
/** @var array[] $media */
/** @var \common\models\Currency[] $currencies */
/** @var array $categories */
/** @var array $ctgList */
/** @var array $placeholders */

\frontend\assets\FrontendAsset::register($this);
\yii\bootstrap\BootstrapPluginAsset::register($this);
\frontend\assets\ReadmoreAsset::register($this);
\frontend\assets\yyAsset::register($this);
\frontend\assets\DateRangePickerAsset::register($this);
\frontend\assets\CheckboxAsset::register($this);
\frontend\assets\ProductAsset::register($this);
\frontend\assets\RateAsset::register($this);

GrowlAsset::register($this);
?>

    <div class="row vievCreateProduct hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 tableInf">
                    <table class="table-responsive tablePrice">
                        <tr>
                            <td>Просмотры:</td>
                            <td><span class="count">345</span><br><span class="forEra">за сутки</span></td>
                            <td><span class="count">3454</span><br><span class="forWeek">за неделю</span></td>
                            <td><span class="count">34 423</span><br><span class="forMonth">за месяц</span></td>
                            <td class="lastTd"><span class="count">34 423 руб</span><br><span class="forAll">Общий доход от объявлений</span>
                            </td>

                        </tr>
                    </table>
                </div>
                <div class="col-sm-4 text-right inlineBlockBtn">
                    <a href="#" class="btn" type="button" data-toggle="modal" data-target="#addRatingWindow">Я хочу
                        больше просмотров</a>
                    <a href="#" class="inf" type="button" data-toggle="modal" data-target="#scheduleOfVisits">
                        <svg width="17px" height="17px" viewBox="0 0 17 17">
                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g class="re" transform="translate(-377.000000, -552.000000)" fill-rule="nonzero">
                                    <g id="KACCA" transform="translate(0.000000, 259.000000)">
                                        <g id="Group" transform="translate(0.000000, 56.000000)">
                                            <g id="Tr-1-Copy" transform="translate(20.000000, 235.000000)">
                                                <g id="Group-3-Copy" transform="translate(357.000000, 2.000000)">
                                                    <path d="M16.3,15.5 L14.2,13.4 C15.5,12 16.2,10.1 16.2,8.1 C16.2,3.6 12.6,0 8.1,0 C3.6,0 0,3.6 0,8.1 C0,12.6 3.6,16.2 8.1,16.2 C10.2,16.2 12,15.4 13.4,14.2 L15.5,16.3 C15.6,16.4 15.7,16.4 15.9,16.4 C16.1,16.4 16.2,16.4 16.3,16.3 C16.5,16.1 16.5,15.7 16.3,15.5 Z M1,8 C1,4.1 4.2,0.9 8.1,0.9 C12,0.9 15.2,4.1 15.2,8 C15.2,11.9 12,15.1 8.1,15.1 C4.2,15.1 1,12 1,8 Z"
                                                            id="Shape"></path>
                                                    <path d="M9.5,11.2 L8.6,11.2 L8.6,7.4 C8.6,7.1 8.4,6.9 8.1,6.9 L6.8,6.9 C6.5,6.9 6.3,7.1 6.3,7.4 C6.3,7.7 6.5,7.9 6.8,7.9 L7.6,7.9 L7.6,11.2 L6.8,11.2 C6.5,11.2 6.3,11.4 6.3,11.7 C6.3,12 6.5,12.2 6.8,12.2 L9.4,12.2 C9.7,12.2 9.9,12 9.9,11.7 C9.9,11.4 9.7,11.2 9.5,11.2 Z"
                                                            id="Shape"></path>
                                                    <circle id="Oval" cx="7.9" cy="4.4" r="1.1"></circle>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </a>

                </div>
            </div>
            <div class="row advertising">
                <div class="col-sm-8 leftBlock">
                    <span class="grey">Продвижение:</span>
                    <div class="progress">
                        <span class="pointer">2 000</span>
                    </div>

                    <p class="maxPrise">Макс: <span class="bold">12 000 руб</span></p>
                </div>
                <div class="col-sm-4 rightBlock text-right">
                    <p class="yourMany">На вашем счету: <span class="amountOfManey">122 000 руб</span></p>
                    <button class="btn balance">Пополнить баланс</button>
                </div>
            </div>
        </div>
    </div>


    <div class="row editProdct">
        <div class="container">
            <div class="row topRow">
                <div class="col-sm-6 col-xs-12">
                    <?php if (!empty($backUrl)): ?>
                        <p class="text-left">
                            <?= Html::a('← ' . \Yii::t('UI10', '$PAGE_PREVIOUS_REFERRER_LINK$'), $backUrl) ?>
                        </p>
                    <?php endif; ?>
                </div>
                <div class="col-sm-6 hidden-xs">
                    <p class="text-right">
                        <?= Html::beginTag('a', [
                            'class'  => 'preview_page',
                            //                            'title'  => 'Откроется в новом окне',
                            'href'   => '#',
                            'target' => '_blank',
                        ]) ?>
                        <svg width="19px" height="19px" viewBox="0 0 19 19">
                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g id="UI10.3--Создание-товара3--Календарь-свободных-дней"
                                        transform="translate(-20.000000, -423.000000)"
                                        fill-rule="nonzero"
                                        fill="#E35F46">
                                    <g id="PROF" transform="translate(0.000000, 396.000000)">
                                        <g id="Group-10" transform="translate(13.000000, 26.000000)">
                                            <g id="Group" transform="translate(7.000000, 1.000000)">
                                                <path d="M3.8,9.5 L8.4,9.5 C8.7,9.5 8.9,9.3 8.9,9 L8.9,4.4 C8.9,4.1 8.7,3.9 8.4,3.9 L3.8,3.9 C3.5,3.9 3.3,4.1 3.3,4.4 L3.3,9 C3.3,9.3 3.5,9.5 3.8,9.5 Z M4.3,4.9 L7.9,4.9 L7.9,8.5 L4.3,8.5 L4.3,4.9 Z"
                                                        id="Shape"></path>
                                                <path d="M18.3,0.8 L1,0.8 C0.7,0.8 0.5,1 0.5,1.3 L0.5,18.2 C0.5,18.5 0.7,18.7 1,18.7 L5.3,18.7 L9.6,18.7 L14,18.7 C14.3,18.7 14.5,18.5 14.5,18.2 L14.5,14.8 L18.3,14.8 C18.6,14.8 18.8,14.6 18.8,14.3 L18.8,12 L18.8,1.3 C18.8,1.1 18.6,0.8 18.3,0.8 Z M17.8,1.8 L17.8,11.6 L1.5,11.6 L1.5,1.8 L17.8,1.8 Z M9.2,17.7 L5.9,17.7 L5.9,12.6 L9.2,12.6 L9.2,17.7 Z M1.5,12.6 L4.8,12.6 L4.8,17.7 L1.5,17.7 L1.5,12.6 Z M13.5,17.7 L10.2,17.7 L10.2,12.6 L13.5,12.6 L13.5,17.7 Z M14.5,13.9 L14.5,12.6 L17.8,12.6 L17.8,13.9 L14.5,13.9 Z"
                                                        id="Shape"></path>
                                                <path d="M11.3,5.1 L15.8,5.1 C16.1,5.1 16.3,4.9 16.3,4.6 C16.3,4.3 16.1,4.1 15.8,4.1 L11.3,4.1 C11,4.1 10.8,4.3 10.8,4.6 C10.8,4.9 11,5.1 11.3,5.1 Z"
                                                        id="Shape"></path>
                                                <path d="M11.3,7.4 L14.3,7.4 C14.6,7.4 14.8,7.2 14.8,6.9 C14.8,6.6 14.6,6.4 14.3,6.4 L11.3,6.4 C11,6.4 10.8,6.6 10.8,6.9 C10.8,7.2 11,7.4 11.3,7.4 Z"
                                                        id="Shape"></path>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        <?= \Yii::t('UI10', '$SHOW_PREVIEW_PRODUCT_LINK$'); ?>
                        <?= Html::endTag('a') ?>
                    </p>
                </div>
            </div>

            <div class="row content">

                <?php $form = ActiveForm::begin([
                    'method'        => 'post',
                    'id'            => 'new-product-form',
                    'options'       => [
                        'class'        => 'formCreateProduct',
                        'autocomplete' => 'off',
                    ],
                    'scrollToError' => false,
                ]) ?>

                <?= Html::activeHiddenInput($model, 'id') ?>
                <?= Html::activeHiddenInput($model, 'category') ?>

                <div class="tabEditProdct">

                    <?= Tabs::widget([
                        'id'           => 'new-product-form-tab',
                        'items'        => [
                            [
                                'label'   => createLabel(1, '$TAB_ABOUT$'),
                                'content' => $this->render('create/_form-1', [
                                    'form'                => $form,
                                    'model'               => $model,
                                    'categories'          => $categories,
                                    'categoryBreadCrumbs' => $ctgList,
                                    'placeholders'        => $placeholders,
                                ]),
                                'options' => [
                                    'id' => 'about',
                                ],
                            ],
                            [
                                'label'   => createLabel(2, '$TAB_MEDIA$'),
                                'content' => $this->render('create/_form-2', [
                                    'form'         => $form,
                                    'model'        => $model,
                                    'initialMedia' => $media,
                                    'placeholders' => $placeholders,
                                ]),
                                'options' => [
                                    'id' => 'fotoVideo',
                                ],
                            ],
                            [
                                'label'   => createLabel(3, '$TAB_FREEDAYS$'),
                                'content' => $this->render('create/_form-3', [
                                    'form'  => $form,
                                    'model' => $model,
                                ]),
                                'options' => [
                                    'id' => 'date',
                                ],
                            ],
                            [
                                'label'   => createLabel(4, '$TAB_SETTINGS$'),
                                'content' => $this->render('create/_form-4', [
                                    'form'       => $form,
                                    'model'      => $model,
                                    'currencies' => $currencies,
                                ]),
                                'options' => [
                                    'id' => 'settings',
                                ],
                            ],
                        ],
                        'encodeLabels' => false,
                        'clientEvents' => [
                            'hide.bs.tab' => 'function(event){
var m = jQuery(event.target);

                                m.find(".dich").html(\'<img src="/img/editProfile/chat/oneWhite.svg" />\');
                                m.parent().addClass("fill");
                            }',
                        ],
                    ]) ?>

                </div>

                <?php
                if ($form->enableClientScript && $form->enableClientValidation) {
                    foreach ([
                        'category',
                        'minRent',
                        'maxRent',
                        'minRentVal',
                        'maxRentVal',
                        'dealTimeFrom',
                        'dealTimeTo',
                        'timeZone',
                        'currency',
                        'price',
                        'guaranty',
                        'discountGuaranty',
                        'discountPrice',
                        'deliverydistance',
                        'deliverycost',
                        'autoOrder',
                        'autoRate',
                    ] as $inputs) {
                        $clientOptions = getClientOptions($inputs, $model, $form);
                        if (!empty($clientOptions)) {
                            $form->attributes[] = $clientOptions;
                        }
                    }
                }
                ?>

                <?php ActiveForm::end() ?>

                <!-- для замены категории -->
                <?php
                $action = \Yii::$app->request->get();
                array_unshift($action, 'create-new');

                $form = ActiveForm::begin([
                    'id'     => 'change_category-form',
                    'action' => $action,
                    'method' => 'post',
                ]) ?>

                <?= Html::activeHiddenInput($model, 'id') ?>
                <?= Html::activeHiddenInput($model, 'category') ?>

                <?php ActiveForm::end() ?>

            </div>
        </div>
    </div>

    <div class="modal fade" id="scheduleOfVisits" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">
                            <svg class="closeImg" width="8px" height="8px" viewBox="0 0 8 8">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
        <g id="U10.6-Просмотр---статистика-без-рекламы-подробно" transform="translate(-1034.000000, -272.000000)"
                stroke="#FFFFFF" fill="#FFFFFF">
            <g id="Group-28">
                <g id="Group-23" transform="translate(385.000000, 258.000000)">
                    <g id="close" transform="translate(645.000000, 10.000000)">
                        <g transform="translate(4.888889, 4.888889)" id="Combined-Shape">
                            <path d="M3.11111111,2.48257175 L0.832788199,0.204248838 C0.659221847,0.0306824861 0.37781519,0.0306824861 0.204248838,0.204248838 L0.204248838,0.832788199 L2.48257175,3.11111111 L0.151606014,5.44207685 C-0.0219603375,5.6156432 -0.0219603375,5.89704986 0.151606014,6.07061621 C0.325172366,6.24418256 0.606579024,6.24418256 0.780145375,6.07061621 L3.11111111,3.73965047 L5.44207685,6.07061621 C5.6156432,6.24418256 5.89704986,6.24418256 6.07061621,6.07061621 C6.24418256,5.89704986 6.24418256,5.6156432 6.07061621,5.44207685 L3.73965047,3.11111111 L6.01797338,0.832788199 C6.19153974,0.659221847 6.19153974,0.37781519 6.01797338,0.204248838 L5.38943402,0.204248838 L3.11111111,2.48257175 Z"></path>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span></button>
                    <h4 class="modal-title" id="myModalLabel">Графики посещений и дохода вашего объявления</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="red"> Просмотров</p>

                    <!--                    --><? //= ToggleButtonGroup::widget([
                    //                        'name' => 'tbg',
                    //                        'type' => 'radio',
                    //                        'items' => [
                    //                            'day' => 'Day',
                    //                            'week' => 'Week',
                    //                            'month' => 'Month',
                    //                            'year' => 'Year',
                    //                            'p' => 'D',
                    //                        ]
                    //                    ]) ?>

                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-primary active">
                            <input type="radio" name="options" id="option1" autocomplete="off" checked>За день
                        </label>
                        <label class="btn btn-primary">
                            <input type="radio" name="options" id="option2" autocomplete="off">За неделю
                        </label>
                        <label class="btn btn-primary">
                            <input type="radio" name="options" id="option3" autocomplete="off">За месяц
                        </label>
                        <label class="btn btn-primary">
                            <input type="radio" name="options" id="option3" autocomplete="off">За год
                        </label>
                        <label class="btn btn-primary">
                            <input type="radio" name="options" id="option3" autocomplete="off">Доход
                        </label>
                    </div>


                    <table class="table-responsive tableStatistyck">
                        <tr>
                            <td class="onePeriod">
                                <p class="countNumber">324</p>
                                <p class="month">Янв</p>
                                <p class="diagramm"></p>
                            </td>
                            <td class="onePeriod">
                                <p class="countNumber">324</p>
                                <p class="month">Янв</p>
                                <p class="diagramm"></p>
                            </td>
                            <td class="onePeriod">
                                <p class="countNumber">324</p>
                                <p class="month">Янв</p>
                                <p class="diagramm"></p>
                            </td>
                            <td class="onePeriod">
                                <p class="countNumber">324</p>
                                <p class="month">Янв</p>
                                <p class="diagramm"></p>
                            </td>
                            <td class="onePeriod">
                                <p class="countNumber">324</p>
                                <p class="month">Янв</p>
                                <p class="diagramm"></p>
                            </td>
                            <td class="onePeriod">
                                <p class="countNumber">324</p>
                                <p class="month">Янв</p>
                                <p class="diagramm"></p>
                            </td>
                            <td class="onePeriod">
                                <p class="countNumber">324</p>
                                <p class="month">Янв</p>
                                <p class="diagramm"></p>
                            </td>
                            <td class="onePeriod">
                                <p class="countNumber">324</p>
                                <p class="month">Янв</p>
                                <p class="diagramm"></p>
                            </td>
                            <td class="onePeriod">
                                <p class="countNumber">324</p>
                                <p class="month">Янв</p>
                                <p class="diagramm"></p>
                            </td>
                            <td class="onePeriod">
                                <p class="countNumber">324</p>
                                <p class="month">Янв</p>
                                <p class="diagramm"></p>
                            </td>
                            <td class="onePeriod">
                                <p class="countNumber">324</p>
                                <p class="month">Янв</p>
                                <p class="diagramm"></p>
                            </td>
                            <td class="onePeriod">
                                <p class="countNumber">324</p>
                                <p class="month">Янв</p>
                                <p class="diagramm"></p>
                            </td>


                        </tr>
                    </table>

                    <div class="promoution text-center hidden">
                        <p class="titlePromoution">Продвижение:</p>
                        <div class="inlineblock">
                            <span class="left"></span>
                            <span class="right">Макс: <strong>12 000 руб</strong></span>
                        </div>
                        <p class="strongText">На вашем счету: <span class="countInScore green">122 000 руб</span></p>
                        <a href="#">Пополнить баланс</a>
                    </div>


                </div>
                <div class="modal-footer text-center">
                    <a href="#" class="moreShown btn">Я хочу больше просмотров</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade"
            id="addRatingWindow"
            tabindex="-1"
            role="dialog"
            aria-labelledby="myModalLabel"
            aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"> <svg class="closeImg"
                                        width="8px"
                                        height="8px"
                                        viewBox="0 0 8 8">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
        <g id="U10.6-Просмотр---статистика-без-рекламы-подробно"
                transform="translate(-1034.000000, -272.000000)"
                stroke="#FFFFFF"
                fill="#FFFFFF">
            <g id="Group-28">
                <g id="Group-23" transform="translate(385.000000, 258.000000)">
                    <g id="close" transform="translate(645.000000, 10.000000)">
                        <g transform="translate(4.888889, 4.888889)" id="Combined-Shape">
                            <path d="M3.11111111,2.48257175 L0.832788199,0.204248838 C0.659221847,0.0306824861 0.37781519,0.0306824861 0.204248838,0.204248838 L0.204248838,0.832788199 L2.48257175,3.11111111 L0.151606014,5.44207685 C-0.0219603375,5.6156432 -0.0219603375,5.89704986 0.151606014,6.07061621 C0.325172366,6.24418256 0.606579024,6.24418256 0.780145375,6.07061621 L3.11111111,3.73965047 L5.44207685,6.07061621 C5.6156432,6.24418256 5.89704986,6.24418256 6.07061621,6.07061621 C6.24418256,5.89704986 6.24418256,5.6156432 6.07061621,5.44207685 L3.73965047,3.11111111 L6.01797338,0.832788199 C6.19153974,0.659221847 6.19153974,0.37781519 6.01797338,0.204248838 L5.38943402,0.204248838 L3.11111111,2.48257175 Z"></path>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span></button>
                        <h4 class="modal-title" id="myModalLabel">Управление продвижением:</h4>
                    </div>

                    <div class="modal-body">

                        <div class="clarify">
                            <h4 class="titleClarify">Давайте кое что уточним</h4>
                            <p class="textClarify">Если вы воспользуетесь данной услугой - ваше объявление будет
                                показываться в топе, а при клике на него будет сниматься сумма, указанная вами ниже.</p>
                            <label for="inputClarify">Цена:</label><br>
                            <div class="inlineBlock">

                                <div class="input-group styleInput">
                                    <input type="text" class="form-control" id="inputClarify" placeholder="15">
                                    <div class="input-group-btn">
                                        <button type="button"
                                                class="btn dropdown-toggle"
                                                data-toggle="dropdown"
                                                aria-expanded="false">Рубли <span class="caret"></span></button>
                                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                            <li><a href="#">Дія</a></li>
                                            <li><a href="#">Інша дія</a></li>
                                            <li><a href="#">Тут щось ще</a></li>
                                        </ul>
                                    </div>

                                </div>
                                <span class="greyText">Рекомендуем указать 15 рублей за клик</span>
                            </div>


                        </div>

                        <div class="manuForPromotion styleInput">
                            <h4 class="titlePromoution">Какую сумму вы хотите потратить на продвижение?</h4>
                            <p class="text-left">Мы остановим продвижение объявления, после того как будет потрачен
                                указанный вами бюджет.</p>

                            <label for="stopData">Отключить продвижение, когда расход составит:
                                <input type="number" placeholder="12" id="stopNumber">
                            </label>

                            <div class="inlineBlock text-justify">
                                <span>Уже потрачено: <strong>434 руб.</strong></span>
                                <span>Ваш баланс:<strong>444руб.</strong>&nbsp;&nbsp;<a href="#">Пополнить</a></span>

                            </div>
                            <p class="text-center stupidBorder"><span>и / или</span></p>
                        </div>

                        <div class="stopPromoution">
                            <h4 class="titleStop">Когда остановить продвижение?</h4>
                            <p class="text-left">По вашему желанию, в выбранный вами день мы можем перестать показывать
                                рекламу. Остаток неизрасходованных средств, оплаченный ранее будет возвращен вам на
                                счет.</p>
                            <div class="inlineBlock styleInput ">
                                <label for="startData">Я хочу продвигать свое объявление с
                                    <input type="text" placeholder="24.05.2017" id="startData">
                                </label>
                                <label for="endData">до
                                    <input type="text" id="endData" placeholder="25.06.2017">
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn redText">Отказаться от продвижения</button>
                        <button type="button" class="btn redBorder">Приостановить рекламу</button>
                        <button type="button" class="btn green">Сохранить настройки</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <!-- Root element of PhotoSwipe. Must have class pswp. -->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

        <!-- Background of PhotoSwipe.
             It's a separate element as animating opacity is faster than rgba(). -->
        <div class="pswp__bg"></div>

        <!-- Slides wrapper with overflow:hidden. -->
        <div class="pswp__scroll-wrap">

            <!-- Container that holds slides.
                PhotoSwipe keeps only 3 of them in the DOM to save memory.
                Don't modify these 3 pswp__item elements, data is added later on. -->
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
            <div class="pswp__ui pswp__ui--hidden">

                <div class="pswp__top-bar">

                    <!--  Controls are self-explanatory. Order can be changed. -->

                    <div class="pswp__counter"></div>

                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                    <button class="pswp__button pswp__button--share" title="Share"></button>

                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                    <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                    <!-- element will get class pswp__preloader--active when preloader is running -->
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>

                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                </button>

                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                </button>

                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>

            </div>

        </div>

    </div>

<?php
/**
 * @param integer $n номер вкладки
 * @param string  $t название вкладки
 *
 * @return string
 */
function createLabel($n, $t) {
    return Html::tag('span', $n, ['class' => 'dich'])
        . Html::tag('span', \Yii::t('UI10', $t), ['class' => 'text'])
        . Html::tag('span', '12%', ['class' => 'count text-right']);
}

$cf = \Yii::t('product', 'Изменения не будут сохранены. Действительно продолжить?');

$this->registerJs(<<<JS
var tab = jQuery("#new-product-form").find(".tab-content");

// сохранить черновик
tab.find(".savedraft-btn").on("click", function(event) {
    event.preventDefault();
    
    var form = $("#new-product-form");
    // form.data("yiiActiveForm").submitting = true;
    // form.yiiActiveForm("validate");
    
    // form.one("afterValidate", function() {
    //     if (form.find(".form-group.has-error").length){
            // client validation error
            // showErrorGrowl();
        // } else {
            var data = form.serialize();
            
            // no client validation error
             jQuery.ajax("/product/create-new", {
                method: "post",
                timeout: 2500,
                data: data,
                cache: false
            })
            .done(function(response) {
                console.log(response);
                
                if (response.status === "success"){
                    form.trigger("saved_product", response.product);
                    
                     jQuery.notify({
                    title: "Success",
                    message: "Товар сохранился в черновики. Для полной публикации в каталоге нажмите кнопку справа."
                }, {
                    type: "info",
                    allow_dismiss: false,
                    showProgressbar: false,
                    placement: {
                        from: "bottom",
                        align: "right"
                    },
                    offset: 20
                });
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
                if (jqXHR.responseJSON.status === "invalid") {
                    // не прошла валидацию форма на стороне сервера
                    _.forOwn(jqXHR.responseJSON.errors, function(v, k) {
                       
                    });
                }
                
                showErrorGrowl();
            });
        // }
    // })
    form.on("beforeSubmit", function(){
        return false;
    });
    
    function showErrorGrowl() {
        jQuery.notify({
            title: "Error",
            message: "Некоторые поля формы не заполнены или содержат ошибку, их надо исправить."
        }, {
            type: "danger",
            allow_dismiss: false,
            showProgressbar: false,
            placement: {
                from: "bottom",
                align: "right"
            },
            offset: 20
        });
    }
    
});

// кнопка отменить создание товара
tab.find(".cancel-btn").on("click", function(event) {
    event.preventDefault();
    
    if ( confirm('{$cf}') ){
        // закрыть и удалить
        jQuery.ajax("/product/remove-product", {
            method: "get",
            data: {
                id: {$model->id}
            }
        });
    }
});

// кнопка на следующую вкладку
tab.find(".bottomBlock .next-btn").on("click", function(event){
    event.preventDefault();
    
    var item = jQuery("#new-product-form-tab").find("> li.active").next("li").children("a");
    item.tab("show");
   
    jQuery("html,body").animate({scrollTop: jQuery("#new-product-form").offset().top}, 350);
});

// кнопка предварительного просмотра
var previw;
var autoclose;
jQuery(".preview_page").on("click", function(event) {
    var fm = jQuery("#new-product-form");
    var la = fm.attr("action");
    
    fm
    .clone()
    .attr("target", "_blank")
    .attr("action", function(index, attr){ 
        var y = _.split(attr, "/");
        y.splice(-2, 1, "preview" );
        
        return _.join(y, "/");
     })
     .removeData( "yiiActiveForm" )
     .appendTo("body")
    .trigger("submit")
    .remove()
    // .attr({target: null, action: la})
    ;
});
jQuery("#new-product-form").on("saved_product", function(event, data) {
    clearTimeout(autoclose);

    if (preview && !preview.closed) {
        preview.location.href = window.location.protocol + "//" + window.location.hostname + "/preview" + jQuery("#productform-id").val();
    }
});

// проценты заполнения полей на вкладке
// var fields1 = jQuery("#about").find(".form-group:has(input:not([type='hidden']),textarea,select)"),
// fields2 = jQuery("#fotoVideo").find(".preview-thumbnails .thumbnail"),
// fields3,
// fields4;
var p1 = 0, p2 = 0, p3 = 0, p4 = 0;

// tab 1
var a1 = jQuery("#about").find(".form-group");
a1 = a1.has("input,textarea,select").not(":has(input[type='hidden'])").add(a1.has(".checkbox,.radio"));

a1.on("change", "input,textarea,select", calc1)
.on("click", "input[type='checkbox'],input[type='radio']", calc1);

// tab 2
jQuery("#productform-media")
.on("filebatchuploadcomplete file\:removesuccess", calc2);

// tab 3
var il = ["#productform-minrentval", "#productform-minrent", "#productform-advancebooking", "#productform-dealtimefrom", "#productform-dealtimeto", "#productform-timezone","#productform-restbetweenrest","#productform-futureplans"];

jQuery("#date").on("change", _.join(il), calc3);
jQuery("#date").find("#unit-of-time-dropdown-menu,#adv-mob-menu,#deal_time_from-dropdown-menu,#deal_time_to-dropdown-menu,#deal_timezone-dropdown-menu,#betweenrest-dropdown-menu,#futureplans-mob-menu").on("change.bs.dropdown", calc3);

// tab 4
jQuery("#settings").on("change", "#productform-price,#productform-address", calc4);
jQuery("#settings").on("change.bs.dropdown", "#currency-dropdown-menu", calc4);
jQuery("#settings").on("click", "#productform-autoorder", calc4);

var gradeColor = ["eb7e3a", "f4a02c", "f7aa29", "c7c93a", "96c955", "7bc963"];

function calc1(){
    var r = jQuery("#about").find(".form-group").filter(function(index, element){
        var t = jQuery(element);
        if (t.is(function(){ return jQuery("input[type='checkbox'],input[type='radio']", this).length; } )){
            return t.find("input:checked").length;
        } else {
            return (t.find("input,textarea,select").val() || "").length ;
        }
    });
    
    p1 = _.clamp(_.floor(r.length / a1.length * 100), 0, 100);
    jQuery("#new-product-form-tab").find("a[href='#about'] .count").text(p1 + "%");
    
    totalCalc();
}
function calc2(){
    var r = jQuery("#fotoVideo").find(".media-preview .preview-thumbnails").children(".thumbnail");
    
    p2 = _.clamp(_.floor(r.length / 10 * 100));
    jQuery("#new-product-form-tab").find("a[href='#fotoVideo'] .count").text(p2 + "%");
    
    totalCalc();
}
function calc3(){
    var r = jQuery("#date").find(_.join(il)).filter(function(index, element){
        var t = jQuery(element);
        if (t.is("input")){
            return (t.val() || "").length;
        } else if (t.has("input[type='radio']")){
            return t.find("input[type='radio']:checked").length;
        }
    });
    
    p3 = _.clamp(_.floor(r.length / _.size(il) * 100), 0, 100);
    jQuery("#new-product-form-tab").find("a[href='#date'] .count").text(p3 + "%");
    
    totalCalc();
}
function calc4(){
    var r = jQuery("#settings").find("#productform-price,#productform-currency,#productform-address,#productform-autoorder").filter(function(index, element){
        var t = jQuery(element);
        if (t.is("input")){
            return (t.val() || "").length;
        } else if (t.has("input[type='radio']")){
            return t.find("input[type='radio']:checked").length;
        }
    });
    
    p4 = _.clamp(_.floor(r.length / 4 * 100), 0, 100);
    jQuery("#new-product-form-tab").find("a[href='#settings'] .count").text(p4 + "%");
    
    totalCalc();
}
function totalCalc(){
    var pt = _.clamp(_.round(_.sum([p1, p2, p3, p4]) / 4));
    var ul = jQuery("#new-product-form-tab");
    ul.children("li").removeClass("success");
    
    jQuery.each([p1, p2, p3, p4], function(index, value){
        if (value >= 90){
            ul.children("li").eq(index).addClass("success");
        }
    });
    
    var cidx = _.findIndex(_.range(0, 120, 100 / 6), function(o){ return  pt < o; });
    
    jQuery("#new-product-form").find(".tab-pane .bottomBlock .topLine .count").text(pt + "%");
    jQuery("#new-product-form")
        .find(".tab-pane .bottomBlock .bottomLine")
        .each(function(index, element){
            jQuery(element)
                .children("span")
                .css("backgroundColor", "")
                .slice(0, cidx)
                .each(function(index, element){
                jQuery(element).css("backgroundColor", "#" + gradeColor[index]);
            });
        })
            
    
    // jQuery("#new-product-form-tab").
}

calc1();
calc2();
calc3();
calc4();

JS
);


function getClientOptions($attr, $modelTest, $form) {
    $attribute = Html::getAttributeName($attr);
    if (!in_array($attribute, $modelTest->activeAttributes(), true)) {
        return [];
    }

    $clientValidation = $form->enableClientValidation;
    $ajaxValidation   = false;

    if ($clientValidation) {
        $validators = [];
        foreach ($modelTest->getActiveValidators($attribute) as $validator) {
            /* @var $validator \yii\validators\Validator */
            $js = $validator->clientValidateAttribute($modelTest, $attribute, $form->getView());
            if ($validator->enableClientValidation && $js != '') {
                if ($validator->whenClient !== null) {
                    $js = "if (({$validator->whenClient})(attribute, value)) { $js }";
                }
                $validators[] = $js;
            }
        }
    }

    if (!$ajaxValidation && (!$clientValidation || empty($validators))) {
        return [];
    }

    $options = [];

    $inputID         = Html::getInputId($modelTest, $attr);
    $options['id']   = Html::getInputId($modelTest, $attr);
    $options['name'] = $attr;

    $options['container'] = ".field-$inputID";
    $options['input']     = "#$inputID";
    $options['error']     = '.' . implode('.', preg_split('/\s+/', 'help-block', -1, PREG_SPLIT_NO_EMPTY));

    $options['encodeError'] = true;
    foreach (['validateOnChange', 'validateOnBlur', 'validateOnType', 'validationDelay'] as $name) {
        $options[ $name ] = $form->$name;
    }

    if (!empty($validators)) {
        $options['validate'] = new \yii\web\JsExpression("function (attribute, value, messages, deferred, \$form) {"
            . implode('', $validators)
            . '}');
    }


    // only get the options that are different from the default ones (set in yii.activeForm.js)
    return array_diff_assoc($options, [
        'validateOnChange'  => true,
        'validateOnBlur'    => true,
        'validateOnType'    => false,
        'validationDelay'   => 500,
        'encodeError'       => true,
        'error'             => '.help-block',
        'updateAriaInvalid' => true,
    ]);
}

$this->registerCss(<<<CSS
#fotoVideo .od-define h5 a{
    color: inherit;
    cursor: pointer;
}
CSS
);