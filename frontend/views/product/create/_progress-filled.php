<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 13.07.2017
 * Time: 17:13
 */

use yii\bootstrap\Html;

/** @var \yii\web\View $this */
/** @var bool|null $submit */
?>


    <div class="col-sm-12 bottomBlock">
        <div class="row">
            <div class="col-md-6 col-xs-12 left">
                <div class="topLine">
                    <span class="text">Вы заполнили объявление на:</span>
                    <span class="count">0%</span>
                </div>
                <div class="bottomLine">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="col-md-6 right text-right">
                <a href="#" class="btn default cancel-btn hidden-xs"><?= \Yii::t('UI10', '$FORM_CANCEL_BTN$') ?></a>
                <a href="#" class="btn red savedraft-btn"><?= \Yii::t('UI10', '$FORM_SAVE_DRAFT_BTN$') ?></a>

                <?php
                $submit = isset($submit) ? $submit : 1;
                switch ($submit) {
                case 1:
                    print '<a href="#" class="btn green next-btn">' . \Yii::t('UI10', '$FORM_NEXT_STEP_BTN$') . '</a>';
                    break;
                case 2:
                    print Html::submitButton(\Yii::t('UI10', '$FORM_PUBLISH_BTN$'), ['class' => 'btn green']);
                    break;
                case 3:
                    print Html::submitButton(\Yii::t('UI10', '$FORM_SAVE_BTN$'), ['class' => 'btn green']);
                    break;
                }
                ?>

                <a href="#" class="btn default btn-block cancel-btn hidden-sm hidden-md hidden-lg"><?= \Yii::t('UI10',
                        '$FORM_CANCEL_BTN$') ?></a>
            </div>
        </div>
    </div>


<?php
$this->registerCss(<<<CSS
.tab-content .left .bottomLine span {
    width: 16.6%;
}
.tab-content .left .bottomLine span.active:nth-child(1){
    background-color: #eb7f3a;
}
.tab-content .left .bottomLine span.active:nth-child(2){
    background-color: #ed8437;
}
.tab-content .left .bottomLine span.active:nth-child(3){
    background-color: #f8af27;
}
.tab-content .left .bottomLine span.active:nth-child(4){
    background-color: #e8c929;
}
.tab-content .left .bottomLine span.active:nth-child(5){
    background-color: #95c955;
}
.tab-content .left .bottomLine span.active:nth-child(6){
    background-color: #7bc962;
}
CSS
);