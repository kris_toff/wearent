<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.07.2017
 * Time: 23:05
 */

use yii\helpers\Url;
use yii\bootstrap\Html;

/** @var \yii\web\View $this */
/** @var \yii\bootstrap\ActiveForm $form */
/** @var \frontend\models\ProductForm $model */
/** @var array[] $initialMedia */
/** @var array $placeholders */

\frontend\assets\CropperAsset::register($this);
\frontend\assets\FullScreenGalerryAsset::register($this);

?>

    <div class="col-sm-12 topBlock styleText">
        <p class="hidden-sm hidden-md hidden-lg beforeShow">
            <a href="#">
                <svg width="19px" height="19px" viewBox="0 0 19 19">
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="UI10.3--Создание-товара3--Календарь-свободных-дней"
                                transform="translate(-20.000000, -423.000000)"
                                fill-rule="nonzero"
                                fill="#E35F46">
                            <g id="PROF" transform="translate(0.000000, 396.000000)">
                                <g id="Group-10" transform="translate(13.000000, 26.000000)">
                                    <g id="Group" transform="translate(7.000000, 1.000000)">
                                        <path d="M3.8,9.5 L8.4,9.5 C8.7,9.5 8.9,9.3 8.9,9 L8.9,4.4 C8.9,4.1 8.7,3.9 8.4,3.9 L3.8,3.9 C3.5,3.9 3.3,4.1 3.3,4.4 L3.3,9 C3.3,9.3 3.5,9.5 3.8,9.5 Z M4.3,4.9 L7.9,4.9 L7.9,8.5 L4.3,8.5 L4.3,4.9 Z"
                                                id="Shape"></path>
                                        <path d="M18.3,0.8 L1,0.8 C0.7,0.8 0.5,1 0.5,1.3 L0.5,18.2 C0.5,18.5 0.7,18.7 1,18.7 L5.3,18.7 L9.6,18.7 L14,18.7 C14.3,18.7 14.5,18.5 14.5,18.2 L14.5,14.8 L18.3,14.8 C18.6,14.8 18.8,14.6 18.8,14.3 L18.8,12 L18.8,1.3 C18.8,1.1 18.6,0.8 18.3,0.8 Z M17.8,1.8 L17.8,11.6 L1.5,11.6 L1.5,1.8 L17.8,1.8 Z M9.2,17.7 L5.9,17.7 L5.9,12.6 L9.2,12.6 L9.2,17.7 Z M1.5,12.6 L4.8,12.6 L4.8,17.7 L1.5,17.7 L1.5,12.6 Z M13.5,17.7 L10.2,17.7 L10.2,12.6 L13.5,12.6 L13.5,17.7 Z M14.5,13.9 L14.5,12.6 L17.8,12.6 L17.8,13.9 L14.5,13.9 Z"
                                                id="Shape"></path>
                                        <path d="M11.3,5.1 L15.8,5.1 C16.1,5.1 16.3,4.9 16.3,4.6 C16.3,4.3 16.1,4.1 15.8,4.1 L11.3,4.1 C11,4.1 10.8,4.3 10.8,4.6 C10.8,4.9 11,5.1 11.3,5.1 Z"
                                                id="Shape"></path>
                                        <path d="M11.3,7.4 L14.3,7.4 C14.6,7.4 14.8,7.2 14.8,6.9 C14.8,6.6 14.6,6.4 14.3,6.4 L11.3,6.4 C11,6.4 10.8,6.6 10.8,6.9 C10.8,7.2 11,7.4 11.3,7.4 Z"
                                                id="Shape"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg> <?= \Yii::t('UI10', '$SHOW_PREVIEW_PRODUCT_LINK$'); ?></a>
        </p>
        <h3><?= \Yii::t('UI10.2', '$PAGE_TITLE$') ?></h3>
        <?= $form->field($model, 'media')->widget(\kartik\file\FileInput::className(), [
            'options'       => [
                'multiple' => true,
            ],
            'pluginOptions' => [
                'language'                 => substr(\Yii::$app->language, 0, 2),
                'maxFileCount'             => 30,
                'maxFileSize'              => 102400,    // KB
                'uploadAsync'              => true,
                'uploadUrl'                => Url::to('/product/load-media'),
                'uploadExtraData'          => [
                    Html::getInputName($model, 'id') => $model->id,
                ],
                'deleteUrl'                => Url::to('/product/delete-media'),
                'deleteExtraData'          => [
                    'pid' => $model->id,
                ],
                'browseOnZoneClick'        => false,
                'showCaption'              => false,
                'showPreview'              => true,
                'showRemove'               => false,
                'showUpload'               => false,
                'showCancel'               => false,
                'showClose'                => false,
                'browseLabel'              => \Yii::t('UI10.2', '$LOAD_FILES_BTN$'),
                'browseIcon'               => '',
                'browseClass'              => 'btn btn-default',
                //            'previewClass'             => '',
                'layoutTemplates'          => [
                    'main1'   => $this->render('media-options/layout-templates/main1'),
                    'main2'   => $this->render('media-options/layout-templates/main2', [
                        'iniMedia'     => $initialMedia,
                        'placeholders' => $placeholders,
                    ]),
                    'preview' => $this->render('media-options/layout-templates/preview', [
                        'iniMedia' => $initialMedia,
                    ]),
                    //                'fileIcon'     => '',
                    //                'size'         => '',
                    'caption' => '',
                    //                'modal'        => '',
                    //                    'progress' => '',
                    'footer'  => '',
                    //                    'actions' => $this->render('media-options/layout-templates/actions'),
                    //                'actionDelete' => '',
                    //                'actionUpload' => '',
                ],
                'previewTemplates'         => [
                    'image' => $this->render('media-options/preview-templates/image'),
                    'video' => $this->render('media-options/preview-templates/video'),
                ],
                'previewSettings'          => [
                    'image' => [
                        'width'  => '100%',
                        'height' => 'auto',
                    ],
                    'video' => [
                        'width'  => '100%',
                        'height' => 'auto',
                    ],
                ],
                'previewZoomButtonIcons'   => [
                    'toggleheader' => false,
                    'borderless'   => false,
                ],
                'defaultPreviewContent'    => '-',
                'initialPreview'           => $initialMedia['preview'],
                'initialPreviewConfig'     => $initialMedia['config'],
                'initialPreviewAsData'     => true,
                'initialPreviewShowDelete' => true,
                'initialPreviewCount'      => count($initialMedia['preview']),
                'overwriteInitial'         => false,
                'allowedFileTypes'         => ['image', 'video'],
                'allowedFileExtensions'    => ['png', 'jpg', 'mp4', 'avi'],
                'allowedPreviewTypes'      => ['image'],
            ],
            'pluginEvents'  => [
                'filebatchselected'       => 'function(event, files) {
//                console.log("select", event, files);
                if (!files.length) return;
                
                $(event.target).fileinput("upload");
            }',
                'filebatchuploadcomplete' => 'function(event, files, extra){
//                console.log("uploadcomplete", files, extra);
                
                var fv = $("#fotoVideo");
                fv.find("#mediafile-progress").addClass("hidden");
                fv.find(".btn-file").removeClass("hidden");
            }',
                'fileuploaded'            => 'function(event, data, previewId, index){
//                console.log("uploaded", data, previewId, index);
                
                var response = data.response;
                $("#fotoVideo").find(".media-preview .preview-thumbnails").append(response.html);
                
                var ims = jQuery("#mediafile-statistics");
                var cf = ims.find(".count-files");
                
                cf.find(".i").text(response.text.i);
                cf.find(".v").text(response.text.v);
                ims.find(".size-files").text(_.round(_.toInteger(response.size)/(1024*1024), 2) + " Mb");
                
                var pct = _.ceil(_.toInteger(response.count)/30*100);
                ims.find(".progress-files").text(pct + "%");
                
                tab.find(".topLine .preview-title .countfiles").text(response.text.a);
            }',
                'filepreajax'             => 'function(){
                    jQuery("#mediafile-progress").removeClass("hidden")
                        .find(".progress-bar").css("width", 0);
                    jQuery("#fotoVideo").find(".btn-file").addClass("hidden");
                }',
                'filepreupload'           => 'function(event, data, previewId, index){
                    
                }',
                'filesuccessremove'       => 'function(event, id){
//                console.log("remove",id);
            }',
                'filedeleted'             => 'function(event){
//                console.log("deleted");
            }',
                'filereset'               => 'function(){
//                    console.log("reset");
                }',
                'fileerror'               => 'function(){
                    console.log("error");
                }',
                'fileuploaderror'         => 'function(event, data, msg){
//                    console.log("upload error", data, msg);
                    jQuery.notify({
                        message: msg
                    }, {
                        type: "danger",
                        delay: 3500
                    });
                }',
            ],
        ])->label(false); ?>
    </div>

<?= $this->render('_progress-filled', [
    'submit' => !$model->product->status ? 1 : 3,
    'status' => !$model->product->isNewRecord || (integer)$model->product->status
]) ?>

<?php
$this->registerJs(<<<JS
var tab = jQuery("#fotoVideo");
var pvb = tab.find(".media-preview");
var th = pvb.find(".preview-thumbnails");
var ctT = tab.find(".file .topLine .countfiles");

// обрезка фото
var cropper;
tab.find(".preview-thumbnails a[data-target='crop']")
.on("click", function(event) {
    var thumbnail = jQuery(event.target).closest(".thumbnail");
    var img = thumbnail.find(".img img");
    
    thumbnail.find(".shadow").addClass("hidden");
    if (cropper){
        cropper.destroy();
    }
    
    cropper = new Cropper(img.get(0), {
        viewMode: 3,
        movable: false,
        rotatable: false,
        zoomable: false,
        zoomOnTouch: false,
        zoomOnWheel: false
    });
    
    thumbnail.find(".img .shadow").after('<div class="crop_actions" style="display: inline-block;"><button type="button" class="btn btn-success btn-sm" data-action="save"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button><button type="button" class="btn btn-info btn-sm" data-action="cancel"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div>');
});
tab.find(".preview-thumbnails").on("click", ".crop_actions", function(event) {
    var btn = jQuery(event.target).closest("button", event.currentTarget);
    if (!btn.length){
        return;
    }
    var thumbnail = jQuery(event.currentTarget).closest(".thumbnail");
    
    switch(btn.data("action")){
        case "save":
            jQuery.ajax("/product/crop", {
                method: "patch",
                timeout: 2000,
                data: jQuery.extend({}, cropper.getData(true), {id: thumbnail.data("id"), pid: {$model->id}})
            })
                .done(function(response) {
                    // console.log(response);
                    thumbnail.find(".img img").attr("src", response.src + "?v=" + moment());
                })
                .fail(function(){
                    jQuery.notify({
                        message: "Не удалось сохранить новое изображение."
                    }, {
                        type: "danger"
                    });
                })
                .always(function(){
                    cropper.destroy();
                    thumbnail.find(".shadow").removeClass("hidden");
                    thumbnail.find(".crop_actions").remove();
                });
            break;
        case "cancel":
            cropper.destroy();
            thumbnail.find(".shadow").removeClass("hidden");
            thumbnail.find(".crop_actions").remove();
            break;
    }
});
jQuery(document).on("click", function(event) {
    var ts = jQuery(event.target).closest(".thumbnail");
    
    if (!ts.length && cropper) {
        cropper.destroy();
    }
    
    th.find(".thumbnail").not(ts)
        .find(".shadow").removeClass("hidden")
        .end()
        .find(".crop_actions").remove();
});

// полноразмерные фото
th.on("click", ".kv-file-zoom", function(event) {
    var item = [];
    
    var all = th.find(".thumbnail img.file-preview-image");
    var current = jQuery(event.target).closest(".thumbnail").find("img.file-preview-image");
    
    all.each(function(index, element) {
        var img = jQuery(element);
        item.push({
            src: window.location.protocol + "//" + window.location.hostname + img.attr("src"),
            w: img.prop("naturalWidth"),
            h: img.prop("naturalHeight")
        });
    });
    
    var options = {
        index: all.index(current),
        history: false,
        focus: false,
        
        showAnimationDuration: 0,
        hideAnimationDuration: 0,
        
        shareEl: false
    };
    
    var gallery = new PhotoSwipe( jQuery(".pswp").first().get(0), PhotoSwipeUI_Default, item, options );
    gallery.init();
    gallery.ui.getFullscreenAPI().enter();
});

// удаление изображения/видео из списка
pvb.on("click", ".kv-file-remove", function(event){
    var thumb = jQuery(event.target).closest(".file-preview-frame");
    var id = thumb.data("id");
    
    jQuery.ajax("/product/delete-media", {
        method: "post",
        data: {
            key: id,
            pid: {$model->id}
        }
    })
        .done(function(response){
            thumb.remove();
            th.children(".clearfix");
            th.children(":nth-child(2n)").after('<div class="clearfix"></div>');
            
            var ims = jQuery("#mediafile-statistics");
            var cf = ims.find(".count-files");
            
            cf.find(".i").text(response.text.i);
            cf.find(".v").text(response.text.v);
            ims.find(".size-files").text(_.round(_.toInteger(response.size)/(1024*1024), 2) + " Mb");
            
            var pct = _.ceil(_.toInteger(response.count)/30*100);
            ims.find(".progress-files").text(pct + "%");
            
            tab.find(".topLine .preview-title .countfiles").text(response.text.a);
            jQuery("#mediafile-progress").find(".progress-bar").css("width", pct + "%");
            
            jQuery("#productform-media").trigger("file:removesuccess");
        });
    
    var count = countThumbnails();
    
    if (!count){
        tab.find(".od-define,.noneFile").removeClass("hidden");
        tab.find(".topLine .preview-title .countfiles").addClass("hidden");
    }
    
    jQuery("#mediafile-progress").find(".progress-bar").css("width", _.ceil(count / 30 * 100) + "%");
});

// новое описание под изображением/видео
pvb.on("change", ".file-thumbnail-description", function(event){
    var input = jQuery(event.target);
    var id = input.closest(".file-preview-frame").data("id");
    
    jQuery.ajax("/product/set-define", {
        method: "post",
        data: {
            pid: {$model->id},
            key: id,
            define: input.val()
        }
    });
});

// фильтры
tab.on("click", ".topLine .btn-group button", function(event){
    var btn = jQuery(event.target).closest("button");
    btn.addClass("active").siblings("button").removeClass("active");
    
    var f = btn.data("filter");
    var thumbs;
    
    switch(f){
        case "all":
            thumbs = th.children(".thumbnail");
            break;
        case "image":
            thumbs = th.children(".thumbnail[type='image']");
            break;
        case "video":
            thumbs = th.children(".thumbnail[type='video']");
            break;
    }
    
    thumbs.removeClass("hidden");
    th.children(".thumbnail").not(thumbs).addClass("hidden");
    
    th.children(".clearfix").remove();
    th.children(".thumbnail").not(".hidden").after(function(index){
        return ((1+index) % 2 === 0) ? '<div class="clearfix"></div>' : '';
    });
});

tab.find(".topLine .btn-group button:first").click();

function countThumbnails(){
    return th.children(".thumbnail").length;
}

// поворот миниатюр влево/вправо
th.on("click", "a[data-direction='left'],a[data-direction='right']", function(event) {
    var m = jQuery(event.target).closest(".thumbnail");
    
    jQuery.ajax("/product/rotate", {
        method: "patch",
        data: {
            angle: jQuery(event.currentTarget).data("direction") === "left" ? -90 : 90,
            id: m.data("id"),
            pid: {$model->id}
        }
    })
    .done(function(response){
        m.find("img").attr("src", response.src + "?v=" + moment());
    });
});

// воспроизведение видео
th.find(".thumbnail[type='video']").on("click", ".shadow .middleLine a", function(event) {
    console.log(event.currentTarget);
});

// сделать медиа основным
th.on("click", ".thumbnail .shadow .mk-default", function(event){
    var th = jQuery(event.target).closest(".thumbnail");
    var id = th.data("id");
    
    jQuery.ajax("/product/make-default", {
        method: "patch",
        data: {
            pid: {$model->id},
            id: id
        }
    })
        .done(function(response) {
            // console.log(response);
            if (response.status !== "success") return false;
            
            th.addClass("default")
                .siblings(".thumbnail").removeClass("default");
        });
});

th.find(".thumbnail[data-id='{$model->mediaDefault}']").addClass("default");

// отображение/скрытие колонок с описанием и ограничениями под зоной загрузки фото
jQuery("#fotoVideo .od-define h5").on("click", function(event){
   jQuery("#fotoVideo .od-define h5 + p").slideToggle();
});
JS
);

$this->registerCss(<<<CSS
#fotoVideo .thumbnail .v-default{
    position: absolute;
    top: 0;
    left: 30px;
    right: 30px;
    display: none;
}
#fotoVideo .thumbnail.default .v-default{
    display: block;
}
#fotoVideo .thumbnail.default .mk-default{
    display: none;
}
#fotoVideo .od-define h5 + p{
    overflow: hidden;
}
#fotoVideo .thumbnail .crop_actions{
    position: absolute;
    right: 2px;
    bottom: 2px;
}
CSS
);