<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 21.04.2017
 * Time: 10:35
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\widgets\Pjax;
use common\models\Product;

/** @var \yii\web\View $this */
/** @var Product|null $model */

if (!isset($model) || is_null($model)) $model = new Product();

$currencyList = \common\models\Currency::find()->all();
$currency     = \yii\helpers\ArrayHelper::map($currencyList, 'id', 'name');
?>


<?php Pjax::begin([
    'id' => 'new-product-form-pjax',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'timeout' => 3000
]) ?>
<?php //print_r($model->errors) ?>
<?php $form = ActiveForm::begin([
    'id'          => 'new-product-form',
    'action' => ['/product/create-new'],
    'options'     => [
        'class' => 'simple-form',
        'data-pjax' => '',
    ],
    'fieldConfig' => [
        'template' => "{label}\n{input}\n<span class='glyphicon glyphicon-ok form-control-feedback' aria-hidden='true'></span>\n<span class='glyphicon glyphicon-remove form-control-feedback' aria-hidden='true'></span>\n{hint}\n{error}\n",
        'options'  => [
            'class' => 'form-group has-feedback',
        ],
    ],
]) ?>

<p class="titleProduct">Информация о товаре:</p>
<?= Html::activeHiddenInput($model, 'category_id') ?>
<?= $form->field($model, 'name')->textInput() ?>
<?= $form->field($model, 'description')->textarea() ?>
<?= $form->field($model, 'country')->textInput()->hint('Страна словами') ?>
<?= $form->field($model,'city')->textInput()->hint('Желательно указывать полное название города') ?>
<?= $form->field($model, 'address')->textarea() ?>
<?= $form->field($model, 'delivery_status',
    ['template' => "<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n{error}\n{hint}\n</div>"])
    ->checkbox(['uncheck' => 0]) ?>
<?= $form->field($model, 'delivery_radius')->textInput() ?>
<?= $form->field($model, 'delivery_price')->textInput() ?>
<?= $form->field($model, 'price') ?>
<?= $form->field($model, 'currency_id', ['template' => "{label}\n{input}\n{hint}\n{error}\n"])
    ->dropDownList($currency) ?>
<?= $form->field($model, 'lease_time', ['template' => "{label}\n{input}\n{hint}\n{error}\n"])->dropDownList([
    Product::LEASING_DAY   => 'Day',
    Product::LEASING_WEEK  => 'Week',
    Product::LEASING_MONTH => 'Month',
]) ?>
<?= $form->field($model, 'pledge_price')->textInput() ?>
<?= $form->field($model, 'min_period')->textInput() ?>
<?= $form->field($model, 'max_period')->textInput() ?>

<?= Html::submitButton('Create', ['class' => 'hidden', 'id' => 'new-product-form-btn']) ?>
<?php ActiveForm::end() ?>

<?php Pjax::end() ?>