<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 26.07.2017
 * Time: 14:24
 */
use yii\bootstrap\Html;
use yii\bootstrap\ButtonDropdown;
use common\models\Product;
use yii\helpers\ArrayHelper;

/** @var \yii\web\View $this */
/** @var \frontend\models\ProductForm $model */
/** @var array $data */

//print_r($data);exit;
$isNew = empty($data);
?>

<div class="form-group">
    <?= Html::activeTextInput($model, $isNew ? 'precond[new][][price]' : "precond[{$data['id']}][price]",
        ['placeholder' => 2000]) ?>

    <span><img src="" alt="&curren;" class="currency-icon" /><span>/ <span class="_price_period"></span> при бронировании от</span>

        <?= Html::activeTextInput($model, $isNew ? 'precond[new][][amount]':"precond[{$data['id']}][amount]", ['placeholder' => 3]) ?>

        <?= Html::activeDropDownList($model, $isNew ? 'precond[new][][lease_time]' : "precond[{$data['id']}][lease_time]", [
            Product::LEASING_HOUR  => \Yii::t('UI10.4', '$SIMPLE_PLURAL_HOUR$'),
            Product::LEASING_DAY   => \Yii::t('UI10.4', '$SIMPLE_PLURAL_DAY$'),
            Product::LEASING_WEEK  => \Yii::t('UI10.4', '$SIMPLE_PLURAL_WEEK$'),
            Product::LEASING_MONTH => \Yii::t('UI10.4', '$SIMPLE_PLURAL_MONTH$'),
        ]) ?>

    </span><span data-target="rv-condition" class="deleteLine" aria-hidden="true">
<svg width="14px" height="19px" viewBox="0 0 14 19">
<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="deleteIcon" transform="translate(-1090.000000, -370.000000)" fill-rule="nonzero" stroke-width="0.3">
            <g id="content" transform="translate(280.000000, 151.000000)">
                <g id="Group-22" transform="translate(20.000000, 89.000000)">
                    <g id="Group-20">
                        <g id="Group-4">
                            <g id="Group-24" transform="translate(770.000000, 26.000000)">
                                <g id="delete_icon" transform="translate(21.000000, 105.000000)">
                                    <path d="M11.9953016,2.04505221 L8.84168254,2.04505221 L8.84168254,1.36682731 C8.84168254,0.617769076 8.27685714,0.01037751 7.58022222,0.01037751 L4.42660317,0.01037751 C3.72996825,0.01037751 3.16514286,0.617769076 3.16514286,1.36682731 L3.16514286,2.04505221 L0.0115238095,2.04505221 L0.0115238095,2.72327711 L0.661365079,2.72327711 L1.29269841,15.6094478 C1.29269841,16.3585402 1.85752381,16.9658976 2.55415873,16.9658976 L9.49212698,16.9658976 C10.1887619,16.9658976 10.7535873,16.3585402 10.7535873,15.6094478 L11.3738413,2.72327711 L11.9953333,2.72327711 L11.9953333,2.04505221 L11.9953016,2.04505221 Z M3.79587302,1.36682731 C3.79587302,0.99262249 4.0792381,0.68860241 4.42660317,0.68860241 L7.58022222,0.68860241 C7.92822222,0.68860241 8.21095238,0.99262249 8.21095238,1.36682731 L8.21095238,2.04505221 L3.79587302,2.04505221 L3.79587302,1.36682731 Z M10.1240635,15.5743213 L10.1228254,15.5915602 L10.1228254,15.6094478 C10.1228254,15.983004 9.84009524,16.2876727 9.49209524,16.2876727 L2.55412698,16.2876727 C2.2067619,16.2876727 1.92339683,15.983004 1.92339683,15.6094478 L1.92339683,15.5915602 L1.92279365,15.5737068 L1.29269841,2.72327711 L10.7425079,2.72327711 L10.1240635,15.5743213 Z"
                                            id="Shape"></path>
                                    <rect id="Rectangle-path"
                                            x="5.68803175"
                                            y="4.07969277"
                                            width="1"
                                            height="10.851496"></rect>
                                    <polygon id="Shape"
                                            points="4.4407619 14.9093755 3.79526984 4.07907831 3.16577778 4.12209036 3.81130159 14.9523876"></polygon>
                                    <polygon id="Shape"
                                            points="8.846 4.10089157 8.21650794 4.05852811 7.58022222 14.9100241 8.20971429 14.9523876"></polygon>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
    </span>
</div>