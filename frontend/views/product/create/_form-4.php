<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.07.2017
 * Time: 23:05
 */

use yii\bootstrap\Html;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;


/** @var \yii\web\View $this */
/** @var \yii\bootstrap\ActiveForm $form */
/** @var \frontend\models\ProductForm $model */
/** @var \common\models\Currency[] $currencies */

$cdropitems = \yii\helpers\ArrayHelper::toArray($currencies, [
    \common\models\Currency::className() => [
        'url'     => function() { return '#'; },
        'label'   => function($model) {
            return Html::img('/img/currencies/icon/' . mb_strtolower($model->code, 'UTF-8') . '.png',
                    ['class' => 'currency-icon'])
                . \Yii::t('UI10.4', $model['code']);
        },
        'options' => function($model) {
            return ['data-value' => $model->id];
        },
    ],
]);


$geo     = \Yii::$app->geoip;
$iplocal = $geo->ip();

\yii\jui\JuiAsset::register($this);
?>


    <div class="col-sm-12 prise styleText styleInput">
        <p class="hidden-sm hidden-md hidden-lg beforeShow">
            <a href="#">
                <svg width="19px" height="19px" viewBox="0 0 19 19">
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="UI10.3--Создание-товара3--Календарь-свободных-дней"
                                transform="translate(-20.000000, -423.000000)"
                                fill-rule="nonzero"
                                fill="#E35F46">
                            <g id="PROF" transform="translate(0.000000, 396.000000)">
                                <g id="Group-10" transform="translate(13.000000, 26.000000)">
                                    <g id="Group" transform="translate(7.000000, 1.000000)">
                                        <path d="M3.8,9.5 L8.4,9.5 C8.7,9.5 8.9,9.3 8.9,9 L8.9,4.4 C8.9,4.1 8.7,3.9 8.4,3.9 L3.8,3.9 C3.5,3.9 3.3,4.1 3.3,4.4 L3.3,9 C3.3,9.3 3.5,9.5 3.8,9.5 Z M4.3,4.9 L7.9,4.9 L7.9,8.5 L4.3,8.5 L4.3,4.9 Z"
                                                id="Shape"></path>
                                        <path d="M18.3,0.8 L1,0.8 C0.7,0.8 0.5,1 0.5,1.3 L0.5,18.2 C0.5,18.5 0.7,18.7 1,18.7 L5.3,18.7 L9.6,18.7 L14,18.7 C14.3,18.7 14.5,18.5 14.5,18.2 L14.5,14.8 L18.3,14.8 C18.6,14.8 18.8,14.6 18.8,14.3 L18.8,12 L18.8,1.3 C18.8,1.1 18.6,0.8 18.3,0.8 Z M17.8,1.8 L17.8,11.6 L1.5,11.6 L1.5,1.8 L17.8,1.8 Z M9.2,17.7 L5.9,17.7 L5.9,12.6 L9.2,12.6 L9.2,17.7 Z M1.5,12.6 L4.8,12.6 L4.8,17.7 L1.5,17.7 L1.5,12.6 Z M13.5,17.7 L10.2,17.7 L10.2,12.6 L13.5,12.6 L13.5,17.7 Z M14.5,13.9 L14.5,12.6 L17.8,12.6 L17.8,13.9 L14.5,13.9 Z"
                                                id="Shape"></path>
                                        <path d="M11.3,5.1 L15.8,5.1 C16.1,5.1 16.3,4.9 16.3,4.6 C16.3,4.3 16.1,4.1 15.8,4.1 L11.3,4.1 C11,4.1 10.8,4.3 10.8,4.6 C10.8,4.9 11,5.1 11.3,5.1 Z"
                                                id="Shape"></path>
                                        <path d="M11.3,7.4 L14.3,7.4 C14.6,7.4 14.8,7.2 14.8,6.9 C14.8,6.6 14.6,6.4 14.3,6.4 L11.3,6.4 C11,6.4 10.8,6.6 10.8,6.9 C10.8,7.2 11,7.4 11.3,7.4 Z"
                                                id="Shape"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg> <?= \Yii::t('UI10', '$SHOW_PREVIEW_PRODUCT_LINK$'); ?></a>
        </p>
        <h3><?= \Yii::t('UI10.4', '$PRICE_TITLE$') ?></h3>

        <?= Html::beginTag('div', [
            'class' => implode(' ', [
                'form-group',
                'field-' . Html::getInputId($model, 'currency'),
                'field-' . Html::getInputId($model, 'price'),
            ]),
        ]) ?>
        <?= Html::activeHiddenInput($model, 'currency') ?>

        <?= Html::activeLabel($model, 'price', ['label' => \Yii::t('UI10.4', '$PRICE_LABEL$')]) ?><br>


        <div class="input-group pull-left">
            <?= Html::activeTextInput($model, 'price', ['class' => 'form-control', 'placeholder' => 2000]) ?>

            <div class="input-group-btn">
                <?= ButtonDropdown::widget([
                    'id'       => 'currency-dropdown',
                    'label'    => '',
                    'dropdown' => [
                        'id'           => 'currency-dropdown-menu',
                        'encodeLabels' => false,
                        'items'        => $cdropitems,
                        'options'      => [
                            'data-target' => Html::getInputId($model, 'currency'),
                        ],
                    ],
                ]) ?>
            </div>
        </div>

        <span>/ <span class="_price_period"><a href="#" onclick='gotab(event, 3)'>период времени для цены можно указать через минимальный срок аренды</a></span></span>

        <div class="clearfix"></div>
        <?= Html::error($model, 'price', ['class' => 'help-block']) ?>
        <?= Html::endTag('div') ?>
    </div>

    <div class="col-sm-12 pledge styleText styleInput">
        <h3><?= \Yii::t('UI10.4', '$GUARANTEE_TITLE$') ?>
            <span data-toggle="popover"
                    data-placement="right auto"
                    data-trigger="hover"
                    title="<?= \Yii::t('UI10.4', '$GUARANTEE_POPOVER$') ?>">
                <svg class="fotoHover" width="20px" height="20px" viewBox="0 0 20 20">
                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="-UI14.1----Редактировать-профиль---Фото-и-видео"
                            transform="translate(-232.000000, -261.000000)">
                        <g id="Group-2" transform="translate(0.000000, 199.000000)">
                            <g id="inff" transform="translate(233.000000, 63.000000)">
                                <path d="M8.8,0 C4,0 0,3.9 0,8.8 C0,13.7 3.9,17.6 8.8,17.6 C13.7,17.6 17.6,13.7 17.6,8.8 C17.6,3.9 13.6,0 8.8,0 L8.8,0 Z"
                                        id="Shape"
                                        fill-rule="nonzero"></path>
                                <g id="Group-27" transform="translate(7.000000, 4.000000)">
                                    <path d="M1.9,0 C2.5,0 2.9,0.5 2.9,1 C2.9,1.6 2.4,2 1.9,2 C1.3,2 0.9,1.5 0.9,1 C0.8,0.4 1.3,0 1.9,0 L1.9,0 Z"
                                            id="Path"></path>
                                    <path d="M3.4,8.9 L0.8,8.9 C0.4,8.9 0,8.6 0,8.1 C0,7.6 0.3,7.3 0.8,7.3 L1.4,7.3 L1.4,4.6 L0.8,4.6 C0.4,4.6 0,4.3 0,3.8 C0,3.3 0.3,3 0.8,3 L2.1,3 C2.5,3 2.9,3.3 2.9,3.8 L2.9,7.4 L3.5,7.4 C3.9,7.4 4.3,7.7 4.3,8.2 C4.3,8.7 3.8,8.9 3.4,8.9 L3.4,8.9 Z"
                                            id="Path"></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
            </span>
        </h3>

        <div class="form-group field-<?= Html::getInputId($model, 'guaranty') ?>">
            <?= Html::activeLabel($model, 'guaranty', ['label' => \Yii::t('UI10.4', '$PRICE_LABEL$')]) ?><br>
            <?= Html::activeTextInput($model, 'guaranty', ['placeholder' => 2000]) ?>

            <img src="" alt="&curren;" class="currency-icon" />

            <div class="clearfix"></div>
            <?= Html::error($model, 'guaranty', ['class' => 'help-block']) ?>
        </div>
    </div>

    <div class="col-sm-12 longPledge styleText styleInput">
        <h3><?= \Yii::t('UI10.4', '$PRICE_LONG_TERM_TITLE$') ?></h3>
        <p class="text-left">
            <?= \Yii::t('UI10.4', '$PRICE_LONG_TERM_DESCRIPTION$') ?>
        </p>

        <?= Html::activeLabel($model, 'guaranty', ['label' => \Yii::t('UI10.4', '$PRICE_LABEL$')]) ?>

        <?php
        foreach ($model->precond as $item) {
            print $this->render('_pricetemplate', ['model' => $model, 'data' => $item]);
        }
        ?>

        <a id="add-pricecondition-btn"><span aria-hidden="true">+</span> <?= \Yii::t('UI10.4',
                '$PRICE_CONDITIONS_ADD_NEW_BTN$') ?></a>
    </div>

    <div class="col-sm-12 discount styleText ">
        <h3><?= \Yii::t('UI10.4', '$PRICE_DISCOUNT_VERIFY_TITLE$') ?></h3>
        <p class="text-left">
            <?= \Yii::t('UI10.4', '$PRICE_DISCOUNT_VERIFY_DESCRIPTION$') ?>
        </p>

        <div class="form-group pledgePrise styleInput field-<?= Html::getInputId($model, 'discountGuaranty') ?>">
            <?= Html::activeLabel($model, 'discountGuaranty',
                ['class' => 'text-left grey', 'label' => \Yii::t('UI10.4', '$PRICE_DISCOUNT_PLEDGE_LABEL$')]) ?>
            <div>
                <?= Html::activeTextInput($model, 'discountGuaranty', ['placeholder' => 20]) ?>

                <img src="/img/footer/rp.png" />
                <span>- <?= \Yii::t('UI10.4', '$PRICE_DISCOUNT_VERIFY_PLEDGE$') ?>
                    <span class="_currency_format">
                        <span class="green">0</span></span>.</span>
            </div>

            <?= Html::error($model, 'discountGuaranty', ['class' => 'help-block']) ?>
        </div>

        <div class="form-group allPrice styleInput field-<?= Html::getInputId($model, 'discountPrice') ?>">
            <?= Html::activeLabel($model, 'discountPrice',
                ['class' => 'text-left grey', 'label' => \Yii::t('UI10.4', '$PRICE_DISCOUNT_RENT_LABEL$')]) ?>
            <div>
                <?= Html::activeTextInput($model, 'discountPrice', ['placeholder' => 15]) ?>

                <img src="/img/footer/rp.png" alt="%">
                <span>- <?= \Yii::t('UI10.4', '$PRICE_DISCOUNT_VERIFY_RENT$') ?>
                    <span class="_currency_format">
                        <span class="green">0</span></span><span class="_price_period"></span>.</span>
            </div>

            <?= Html::error($model, 'discountPrice', ['class' => 'help-block']) ?>
        </div>
    </div>

    <div class="col-sm-6 mapsText styleText styleInput">
        <h3><?= \Yii::t('UI10.4', '$ADDRESS_TITLE$') ?></h3>

<!--        <p class="text-left text-danger">Укажите как можно точнее нужное местоположение, включая город, улицу и номер-->
<!--            дома.</p>-->

        <?= Html::activeHiddenInput($model, 'address_id') ?>
        <?= Html::activeHiddenInput($model, 'address_lat') ?>
        <?= Html::activeHiddenInput($model, 'address_lng') ?>

        <?= $form->field($model, 'address')->textInput([
            'data' => [
                'toggle'    => 'popover',
                'trigger'   => 'focus',
                'content'   => \Yii::t('UI10.4', '$FULL_ADDRESS_DESIRABLE$'),
                'placement' => 'top',
            ],
        ])->label(\Yii::t('UI10.4', '$ADDRESS_LABEL$')) ?>

        <h3><?= \Yii::t('UI10.4', '$DELIVERY_TITLE$') ?></h3>
        <div class="form-group field-<?= Html::getInputId($model, 'deliverydistance') ?>">
            <?= Html::activeLabel($model, 'deliverydistance', ['label' => \Yii::t('UI10.4', '$DELIVERY_LABEL$')]) ?>
            <div>
                <?= Html::activeTextInput($model, 'deliverydistance', ['placeholder' => 15]) ?>

                <span class="grey"><img src="" alt="km"> - <?= \Yii::t('UI10.4', '$DELIVERY_TEXT1$') ?></span>
            </div>

            <?= Html::error($model, 'deliverydistance', ['class' => 'help-block']) ?>
        </div>

        <h3><?= \Yii::t('UI10.4', '$DELIVERY_COST_TITLE$') ?></h3>
        <div class="form-group field-<?= Html::getInputId($model, 'deliverycost') ?>">
            <?= Html::activeLabel($model, 'deliverycost', ['label' => \Yii::t('UI10.4', '$DELIVERY_COST_LABEL$')]) ?>
            <div>
                <?= Html::activeTextInput($model, 'deliverycost', ['placeholder' => 15]) ?>

                <span>  <img src="" class="currency-icon" alt="&curren;"></span>
            </div>

            <?= Html::error($model, 'deliverycost', ['class' => 'help-block']) ?>
        </div>
    </div>

    <div class="col-sm-6 maps1">
        <div id="canvas_map"></div>
    </div>

    <div class="col-sm-12 autoPladge styleText styleInput">
        <h3><?= \Yii::t('UI10.4', '$AUTO_BOOKING_TITLE$') ?>
            <span data-toggle="popover"
                    data-placement="right auto"
                    data-trigger="hover"
                    title="<?= \Yii::t('UI10.4', '$AUTO_BOOKING_POPOVER$') ?>">
                <svg class="fotoHover" width="20px" height="20px" viewBox="0 0 20 20">
                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="-UI14.1----Редактировать-профиль---Фото-и-видео"
                            transform="translate(-232.000000, -261.000000)">
                        <g id="Group-2" transform="translate(0.000000, 199.000000)">
                            <g id="inff" transform="translate(233.000000, 63.000000)">
                                <path d="M8.8,0 C4,0 0,3.9 0,8.8 C0,13.7 3.9,17.6 8.8,17.6 C13.7,17.6 17.6,13.7 17.6,8.8 C17.6,3.9 13.6,0 8.8,0 L8.8,0 Z"
                                        id="Shape"
                                        fill-rule="nonzero"></path>
                                <g id="Group-27" transform="translate(7.000000, 4.000000)">
                                    <path d="M1.9,0 C2.5,0 2.9,0.5 2.9,1 C2.9,1.6 2.4,2 1.9,2 C1.3,2 0.9,1.5 0.9,1 C0.8,0.4 1.3,0 1.9,0 L1.9,0 Z"
                                            id="Path"></path>
                                    <path d="M3.4,8.9 L0.8,8.9 C0.4,8.9 0,8.6 0,8.1 C0,7.6 0.3,7.3 0.8,7.3 L1.4,7.3 L1.4,4.6 L0.8,4.6 C0.4,4.6 0,4.3 0,3.8 C0,3.3 0.3,3 0.8,3 L2.1,3 C2.5,3 2.9,3.3 2.9,3.8 L2.9,7.4 L3.5,7.4 C3.9,7.4 4.3,7.7 4.3,8.2 C4.3,8.7 3.8,8.9 3.4,8.9 L3.4,8.9 Z"
                                            id="Path"></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
            </span>
        </h3>

        <div class="form-group field-<?= Html::getInputId($model, 'autoOrder') ?> field-<?= Html::getInputId($model,
            'autoRate') ?>">
            <?= Html::activeRadioList($model, 'autoOrder', [
                '1' => \Yii::t('UI10.4', '$AUTO_BOOKING_YESIWANT$'),
                '2' => \Yii::t('UI10.4', '$AUTO_BOOKING_YESBUTVERIFY$') . ' <div>'
                    . Html::activeLabel($model, 'autoRate',
                        ['label' => \Yii::t('UI10.4', '$AUTO_BOOKING_CHOOSE_RATING$'), 'class' => 'grey'])
                    . ' <div>'
                    . Html::activeDropDownList($model, 'autoRate', [
                        500  => 500,
                        1000 => 1000,
                        2000 => 2000,
                    ])
                    . '</div></div>',
                '0' => \Yii::t('UI10.4', '$AUTO_BOOKING_NO$'),
            ], [
                'encode' => false,
            ]) ?>

            <?= Html::error($model, 'autoRate', ['class' => 'help-block']) ?>
        </div>

    </div>


    <div class="col-sm-12 trust styleText styleInput">
        <div class="form-group">
            <?= $form->field($model, 'onlyVerify')->checkbox([
                'uncheck' => 0,
                'label'   => \Yii::t('UI10.4', '$ONLY_VERIFY_CHECKBOX$'),
                'class'   => 'redCheckBox',
            ]) ?>

            <span data-toggle="popover"
                    data-placement="right auto"
                    data-trigger="hover"
                    title="<?= \Yii::t('UI10.4', '$ONLY_VERIFY_CHECKBOX_POPOVER$') ?>">
                <svg class="fotoHover" width="20px" height="20px" viewBox="0 0 20 20">
                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="-UI14.1----Редактировать-профиль---Фото-и-видео"
                            transform="translate(-232.000000, -261.000000)">
                        <g id="Group-2" transform="translate(0.000000, 199.000000)">
                            <g id="inff" transform="translate(233.000000, 63.000000)">
                                <path d="M8.8,0 C4,0 0,3.9 0,8.8 C0,13.7 3.9,17.6 8.8,17.6 C13.7,17.6 17.6,13.7 17.6,8.8 C17.6,3.9 13.6,0 8.8,0 L8.8,0 Z"
                                        id="Shape"
                                        fill-rule="nonzero"></path>
                                <g id="Group-27" transform="translate(7.000000, 4.000000)">
                                    <path d="M1.9,0 C2.5,0 2.9,0.5 2.9,1 C2.9,1.6 2.4,2 1.9,2 C1.3,2 0.9,1.5 0.9,1 C0.8,0.4 1.3,0 1.9,0 L1.9,0 Z"
                                            id="Path"></path>
                                    <path d="M3.4,8.9 L0.8,8.9 C0.4,8.9 0,8.6 0,8.1 C0,7.6 0.3,7.3 0.8,7.3 L1.4,7.3 L1.4,4.6 L0.8,4.6 C0.4,4.6 0,4.3 0,3.8 C0,3.3 0.3,3 0.8,3 L2.1,3 C2.5,3 2.9,3.3 2.9,3.8 L2.9,7.4 L3.5,7.4 C3.9,7.4 4.3,7.7 4.3,8.2 C4.3,8.7 3.8,8.9 3.4,8.9 L3.4,8.9 Z"
                                            id="Path"></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
            </span>
        </div>

    </div>

<?= $this->render('_progress-filled', [
    'submit' => !$model->product->status ? 2 : 3,
    'status' => !$model->product->isNewRecord || (integer)$model->product->status
]) ?>

<?php
$this->registerCss(<<<CSS
#settings .autoPladge > .form-group > div > label {
    display: block;
}
#settings .discount .green {
    color: #57cc82;
}

#canvas_map{
    width: 100%;
    height: 250px;
}

#currency-dropdown {
    font-size: 14px;
}
CSS
);

$this->registerJs(<<<JS
var tab = jQuery("#settings");
var lp = tab.find(".longPledge");

// добавить условия
var numcondition = 1;
lp.on("click", "#add-pricecondition-btn", function(event) {
    event.preventDefault();
    
    var fg = lp.find("#add-pricecondition-btn");
    var tmp = jQuery(templatePriceCondition);
    tmp.find("input[name],select[name]")
        .attr("name", function(index, attr){
            return _.replace(attr, "[]", "[" + numcondition + "]");
    })
    .attr("id", function(index, attr){
        return attr + numcondition;
    });
    tmp.find(".dropdown-menu").each(function(index, element) {
        var elem = jQuery(element);
        var target = elem.data("target");
        
        if (target === undefined){
            return;
        } 
        var newId = elem.data("target") + numcondition;
        
        if (elem.attr("data-target") !== undefined){
            elem.attr("data-target", newId);
        }
        elem.data("target", newId);
    });
    tmp.find(".currency-icon").attr("src", jQuery("#currency-dropdown").find("img").attr("src"));
    tmp.find("._price_period").text( _.upperFirst(_.trim(jQuery("#settings .discount ._price_period").text())) );
    tmp.find("select").selectmenu();
    
    numcondition++;
    fg.before(tmp);
});
// удалить условия
lp.on("click", "[data-target='rv-condition']", function(event){
    var btn = jQuery(event.target);
    var group = btn.closest(".form-group");
    
    var cnt = group.siblings(".form-group").length;
    if (!cnt) return;
    
    group.remove();
});

jQuery(".longPledge").find("select").selectmenu();
JS
);

$templatePriceCondition = trim(preg_replace(['/>\s+</', '/\s+/'], ['><', ' '],
    $this->render('_pricetemplate', ['model' => $model])));

$this->registerJs(<<<JS
window.gotab = function(event, tn){
    event.preventDefault();
    
    jQuery("#new-product-form-tab").find("a:eq(" + (tn-1) + ")").tab("show");
};

jQuery("#currency-dropdown-menu").on("change.bs.dropdown", function(event, vid, vtext){
    var si = jQuery("#currency-dropdown").find("img:first");
    jQuery("#settings").find(".pledge img.currency-icon,.longPledge img.currency-icon,.mapsText img.currency-icon").attr("src", si.attr("src"));
});

var templatePriceCondition = '$templatePriceCondition';

$("#currency-dropdown-menu").find("li[data-value='" + $("#productform-currency").val() +"'] a").trigger("click");
$("#productform-autorate").selectmenu({"disabled": true});

$("#productform-autoorder").on("change", function(event) {
    var v = $(event.currentTarget).find("input[type='radio']:checked").val();
    if (2 == v){
        $("#productform-autorate").selectmenu("enable");
    }else{
        $("#productform-autorate").selectmenu("disable");
    }
}).trigger("change");


// обработчики для изменения цен для скидки 
jQuery("#productform-price,#productform-discountprice").on("change input", function(event){
   var price =  jQuery("#productform-price").val();
   var perc = jQuery("#productform-discountprice").val();
   var currency = jQuery("#productform-currency").val();
   // console.log(price,perc,currency);return;
   jQuery.ajax({
    url: "/product/plural-currency",
    method: "post",
    data: {
        p: price,
        d: perc,
        c: currency
    }
   }).done(function(response) {
       var h = _.replace(response.text, /[\d\s,\.]+\s/, '<span class="green">' + response.number + ' </span>');
        jQuery("#productform-discountprice").closest(".form-group").find("._currency_format").html(h);
   });
}).trigger("change");
jQuery("#productform-guaranty,#productform-discountguaranty").on("change input", function(event){
   var price =  jQuery("#productform-guaranty").val();
   var perc = jQuery("#productform-discountguaranty").val();
   var currency = jQuery("#productform-currency").val();
   // console.log(price,perc,currency);return;
   jQuery.ajax({
    url: "/product/plural-currency",
    method: "post",
    data: {
        p: price,
        d: perc,
        c: currency
    }
   }).done(function(response) {
       var h = _.replace(response.text, /[\d\s,\.]+\s/, '<span class="green">' + response.number + ' </span>');
        jQuery("#productform-discountguaranty").closest(".form-group").find("._currency_format").html(h);
   });
}).trigger("change");

if (!jQuery("#settings .longPledge > .form-group").length){
    jQuery("#add-pricecondition-btn").trigger("click");
}
JS
);

$lang = mb_substr(\Yii::$app->language, 0, 2);
$this->registerJsFile("https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBr3Fcss2QdBHKHY5W2m05ZTtb8bRVLXvI&libraries=places&language={$lang}");


$this->registerJs(<<<JS
var map,
    marker,
    circleInner,
    circleOuter,
    autocomplete,
    errorBlock;

google.maps.event.addDomListener(window, "load", initializeMap);

function initializeMap() {
    var mapOptions = {
        zoom: 8,
        center: new google.maps.LatLng({$model->address_lat}, {$model->address_lng}),
        panControl: true,
        mapTypeControl: false,
        streetViewControl: false,
        fullscreenControl: false,
        rotateControl: false
    };
    
    var circleOptions = {
        strokeColor: "#0CAC47",
        strokeWeight: 2,
        fillColor: "#27B75C",
    };
    
    map = new google.maps.Map(document.getElementById("canvas_map"), mapOptions);
    
    marker = new google.maps.Marker({
        animation: google.maps.Animation.DROP,
        draggable: true,
        title: "Перенеси меня куда хочешь"
    });
    var marker_lat = _.toNumber(jQuery("#productform-address_lat").val());
    var marker_lng = _.toNumber(jQuery("#productform-address_lng").val());
    var markerRadius = _.toInteger(jQuery("#productform-deliverydistance").val());
     
    circleInner = new google.maps.Circle(_.assign(circleOptions, {strokeOpacity: 0.6, fillOpacity: 0.45}));
    circleOuter = new google.maps.Circle(_.assign(circleOptions, {strokeOpacity: 0.5, fillOpacity: 0.25}));
    circleInner.setRadius(400);
    
    if (marker_lat && marker_lng){
        marker.setPosition({lat: marker_lat, lng: marker_lng});
        marker.setMap(map);
        map.panTo(marker.getPosition());
        
        circleInner.setCenter({lat: marker_lat, lng: marker_lng});
        circleOuter.setCenter({lat: marker_lat, lng: marker_lng});
    }
    if (markerRadius > 0){
        circleOuter.setRadius(markerRadius * 1000);
        
        circleInner.setMap(map);
        circleOuter.setMap(map);
    }
    
    var options = {
        types: ["address"]
    };
    
    var input = document.getElementById("productform-address");
    // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    errorBlock = jQuery(input).nextAll(".help-block");
    
    autocomplete = new google.maps.places.Autocomplete(input, options);
    autocomplete.addListener("place_changed", function() {
        var place = autocomplete.getPlace();
        
        if (!place.geometry) {
            marker.setMap(null);
            
            errorBlock.text("Не получается найти это место на карте.");
            errorBlock.closest(".form-group").removeClass("has-success").addClass("has-error");
            
             jQuery("#productform-address_id").val("");
                    jQuery("#productform-address_lat").val("");
                    jQuery("#productform-address_lng").val("");
            
            return;
        } 
        
        marker.setPosition(place.geometry.location);
        marker.setMap(map);
        
        map.panTo(place.geometry.location);
        if (map.getZoom() < 13){
             map.setZoom(13);
        }
        
        circleInner.setCenter(place.geometry.location);
        circleOuter.setCenter(place.geometry.location);
        
        jQuery("#productform-address_id").val(place.place_id);
        jQuery("#productform-address_lat").val(_.get(place, ["geometry", "location", "lat"]));
        jQuery("#productform-address_lng").val(_.get(place, ["geometry", "location", "lng"]));
        // console.log(place);
        if (_.get(place, ["geometry", "address_components", 0, "types", 0]) === "street_number") {
            errorBlock.empty();
            errorBlock.closest(".form-group").removeClass("has-error").addClass("has-success");
        } else {
            errorBlock.text("Желательно указать место точнее, написав улицу и номер дома.");
            errorBlock.closest(".form-group").removeClass("has-success").addClass("has-error");
        }
        // google.maps.event.trigger(map, "resize");
    });
    
    marker.addListener("dragstart", function(event){
        jQuery("#productform-address").val("").trigger("change");
    });
    marker.addListener("drag", function(event) {
       circleInner.setCenter(marker.getPosition()); 
       circleOuter.setCenter(marker.getPosition()); 
    });
    marker.addListener("dragend", function(event){
        interactiveSetMapLocation(marker.getPosition().toUrlValue());
    });
    
    jQuery("a[data-toggle='tab']").on("shown.bs.tab", function(event){
       if (jQuery(event.target).attr("href") === "#settings"){
           google.maps.event.trigger(map, "resize");
       }
    });
     
     map.addListener("click", function(event){
         jQuery(input).val("").trigger("change");
         
         marker.setMap(map);
         marker.setPosition(event.latLng);
         circleInner.setCenter(event.latLng);
         circleOuter.setCenter(event.latLng);
         
         interactiveSetMapLocation(event.latLng.toUrlValue());
     });
     
     jQuery("#productform-deliverydistance").on("change input", function(event) {
        var radius = $(event.target).val();
        radius = _.max([_.toInteger(radius), 0]);
        if (radius < 1){
            circleInner.setMap(null);
            circleOuter.setMap(null);
            return;
        }
        
        circleOuter.setRadius(radius * 1000);
        
        circleInner.setMap(map);
        circleOuter.setMap(map);
     });
     
     function interactiveSetMapLocation(latlng) {
         errorBlock.empty();
         errorBlock.closest(".form-group").removeClass("has-error has-success");
         
         jQuery.get({
            url: "https://maps.googleapis.com/maps/api/geocode/json",
            timeout: 2000,
            data: {
                key: "AIzaSyBr3Fcss2QdBHKHY5W2m05ZTtb8bRVLXvI",
                latlng: latlng,
                language: "{$lang}",
                result_type: "street_address",
                location_type: "ROOFTOP"
            }
        }).done(function(response){ 
            console.log(response);
            switch(response.status){
                case "OK":
                    jQuery("#productform-address").val(_.get(response, "results.0.formatted_address")).trigger("input");
                    
                    jQuery("#productform-address_id").val(_.get(response, ["results", 0, "place_id"]));
                    jQuery("#productform-address_lat").val(_.get(response, ["results", 0, "geometry", "location", "lat"]));
                    jQuery("#productform-address_lng").val(_.get(response, ["results", 0, "geometry", "location", "lng"]));
                    
                    // if (_.get(response, ["results", 0, "address_components", 0, "types", 0]) === "street_number") {
                    //     errorBlock.empty();
                    //     errorBlock.closest(".form-group").removeClass("has-error").addClass("has-success");
                    // } else {
                    //     errorBlock.text("Желательно указать место точнее, написав улицу и номер дома.");
                    //     errorBlock.closest(".form-group").removeClass("has-success").addClass("has-error");
                    // }
                    break;
                case "ZERO_RESULTS":
                    errorBlock.text("Не найдено ни одного результата по этим координатам.");
                    errorBlock.closest(".form-group").addClass("has-error");
                    
                    jQuery("#productform-address_id").val("");
                    jQuery("#productform-address_lat").val("");
                    jQuery("#productform-address_lng").val("");
                    break;
            }
         }).fail(function(){
             errorBlock.text("Ошибка при получении нового адреса. Попробуйте ещё раз.");
            errorBlock.closest(".form-group").addClass("has-error");
            
             jQuery("#productform-address_id").val("");
                    jQuery("#productform-address_lat").val("");
                    jQuery("#productform-address_lng").val("");
          });
     }
}

JS
    , \yii\web\View::POS_END);
