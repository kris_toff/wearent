<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.07.2017
 * Time: 23:05
 */

use yii\bootstrap\Html;
use yii\bootstrap\ToggleButtonGroup;
use yii\bootstrap\ButtonDropdown;

use common\models\Product;

/** @var \yii\web\View $this */
/** @var \yii\bootstrap\ActiveForm $form */
/** @var \frontend\models\ProductForm $model */

?>


    <div class="col-sm-12 dateRent styleText">
        <p class="hidden-sm hidden-md hidden-lg beforeShow">
            <a href="#">
                <svg width="19px" height="19px" viewBox="0 0 19 19">
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="UI10.3--Создание-товара3--Календарь-свободных-дней"
                                transform="translate(-20.000000, -423.000000)"
                                fill-rule="nonzero"
                                fill="#E35F46">
                            <g id="PROF" transform="translate(0.000000, 396.000000)">
                                <g id="Group-10" transform="translate(13.000000, 26.000000)">
                                    <g id="Group" transform="translate(7.000000, 1.000000)">
                                        <path d="M3.8,9.5 L8.4,9.5 C8.7,9.5 8.9,9.3 8.9,9 L8.9,4.4 C8.9,4.1 8.7,3.9 8.4,3.9 L3.8,3.9 C3.5,3.9 3.3,4.1 3.3,4.4 L3.3,9 C3.3,9.3 3.5,9.5 3.8,9.5 Z M4.3,4.9 L7.9,4.9 L7.9,8.5 L4.3,8.5 L4.3,4.9 Z"
                                                id="Shape"></path>
                                        <path d="M18.3,0.8 L1,0.8 C0.7,0.8 0.5,1 0.5,1.3 L0.5,18.2 C0.5,18.5 0.7,18.7 1,18.7 L5.3,18.7 L9.6,18.7 L14,18.7 C14.3,18.7 14.5,18.5 14.5,18.2 L14.5,14.8 L18.3,14.8 C18.6,14.8 18.8,14.6 18.8,14.3 L18.8,12 L18.8,1.3 C18.8,1.1 18.6,0.8 18.3,0.8 Z M17.8,1.8 L17.8,11.6 L1.5,11.6 L1.5,1.8 L17.8,1.8 Z M9.2,17.7 L5.9,17.7 L5.9,12.6 L9.2,12.6 L9.2,17.7 Z M1.5,12.6 L4.8,12.6 L4.8,17.7 L1.5,17.7 L1.5,12.6 Z M13.5,17.7 L10.2,17.7 L10.2,12.6 L13.5,12.6 L13.5,17.7 Z M14.5,13.9 L14.5,12.6 L17.8,12.6 L17.8,13.9 L14.5,13.9 Z"
                                                id="Shape"></path>
                                        <path d="M11.3,5.1 L15.8,5.1 C16.1,5.1 16.3,4.9 16.3,4.6 C16.3,4.3 16.1,4.1 15.8,4.1 L11.3,4.1 C11,4.1 10.8,4.3 10.8,4.6 C10.8,4.9 11,5.1 11.3,5.1 Z"
                                                id="Shape"></path>
                                        <path d="M11.3,7.4 L14.3,7.4 C14.6,7.4 14.8,7.2 14.8,6.9 C14.8,6.6 14.6,6.4 14.3,6.4 L11.3,6.4 C11,6.4 10.8,6.6 10.8,6.9 C10.8,7.2 11,7.4 11.3,7.4 Z"
                                                id="Shape"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg> <?= \Yii::t('UI10', '$SHOW_PREVIEW_PRODUCT_LINK$'); ?></a>
        </p>
        <h3><?= \Yii::t('UI10.3', '$LEASE_RENT_TITLE$') ?></h3>

        <div class="form-group blockMin styleInput <?= 'field-'
        . Html::getInputId($model, 'minRent')
        . ' field-'
        . Html::getInputId($model, 'minRentVal') ?>">
            <?= Html::activeHiddenInput($model, 'minRentVal') ?>

            <?= Html::activeLabel($model, 'minRent',
                ['class' => 'col-sm-4', 'label' => \Yii::t('UI10.3', '$MIN_LEASE_LABEL$')]) ?>

            <div class="col-sm-8 col-xs-12">
                <?= Html::activeTextInput($model, 'minRent', ['placeholder' => 12]) ?>

                <?= ButtonDropdown::widget([
                    'id'       => 'unit-of-time-dropdown',
                    'label'    => '',
                    'dropdown' => [
                        'id'      => 'unit-of-time-dropdown-menu',
                        'items'   => [
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_HOURS$'),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => Product::LEASING_HOUR,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_HOURS$'),
                                ],
                            ],
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_DAYS$'),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => Product::LEASING_DAY,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_DAYS$'),
                                ],
                            ],
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_WEEKS$'),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => Product::LEASING_WEEK,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_WEEKS$'),
                                ],
                            ],
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_MONTHS$'),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => Product::LEASING_MONTH,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_MONTHS$'),
                                ],
                            ],
                        ],
                        'options' => [
                            'data-target' => Html::getInputId($model, 'minRentVal'),
                        ],
                    ],
                    'options'  => [
                        'data-prompt' => \Yii::t('UI10.3', '$INTERVAL_LEASE_DROPDOWN_NOOP$'),
                    ],
                ]) ?>

            </div>
            <?= Html::error($model, 'minRentVal', ['class' => 'help-block']) ?>

            <div class="clearfix"></div>
        </div>
        <div class="form-group blockMax styleInput <?= 'field-'
        . Html::getInputId($model, 'maxRent')
        . ' field-'
        . Html::getInputId($model, 'maxRentVal') ?>">
            <?= Html::activeHiddenInput($model, 'maxRentVal', ['class' => 'form-control', 'placeholder' => 12]) ?>

            <?= Html::activeLabel($model, 'maxRent',
                ['class' => 'col-sm-4 col-xs-12', 'label' => \Yii::t('UI10.3', '$MAX_LEASE_LABEL$')]) ?>
            <div class="col-sm-8 col-xs-12">
                <?= Html::activeTextInput($model, 'maxRent', ['placeholder' => 12]) ?>

                <?= ButtonDropdown::widget([
                    'id'       => 'unit-of-maxtime-dropdown',
                    'label'    => '',
                    'dropdown' => [
                        'id'      => 'unit-of-maxtime-dropdown-menu',
                        'items'   => [
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_HOURS$'),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => Product::LEASING_HOUR,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_HOURS$'),
                                ],
                            ],
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_DAYS$'),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => Product::LEASING_DAY,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_DAYS$'),
                                ],
                            ],
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_WEEKS$'),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => Product::LEASING_WEEK,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_WEEKS$'),
                                ],
                            ],
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_MONTHS$'),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => Product::LEASING_MONTH,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_MONTHS$'),
                                ],
                            ],
                        ],
                        'options' => [
                            'data-target' => Html::getInputId($model, 'maxRentVal'),
                        ],
                    ],
                    'options'  => [
                        'data-prompt' => \Yii::t('UI10.3', '$INTERVAL_LEASE_DROPDOWN_NOOP$'),
                    ],
                ]) ?>
            </div>
            <?= Html::error($model, 'maxRentVal', ['class' => 'help-block']) ?>

            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-sm-12 reserv styleText styleInput">
        <h3><?= \Yii::t('UI10.3', '$PREMATURE_WARNING_TITLE$') ?></h3>
        <p class="text-left">
            <?= \Yii::t('UI10.3', '$PREMATURE_WARNING_DESCRIPTION$') ?>
        </p>

        <div class="hidden-xs hidden-sm">
            <?= $form->field($model, 'advancebooking')->widget(ToggleButtonGroup::className(), [
                'items' => [
                    0 => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 0]),
                    1 => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 1]),
                    2 => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 2]),
                    3 => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 3]),
                    7 => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 7]),
                ],
                'type'  => 'radio',
            ])->label(false) ?>
        </div>
        <div class="hidden-md hidden-lg">
            <div class="form-group field-<?= Html::getInputId($model, 'advancebooking') ?>">
                <?= Html::activeLabel($model, 'advancebooking',
                    ['label' => \Yii::t('UI10.3', '$PREMATURE_WARNING_LABEL$')]) ?>

                <?= ButtonDropdown::widget([
                    'id'       => 'adv-mob',
                    'label'    => '',
                    'dropdown' => [
                        'id'      => 'adv-mob-menu',
                        'items'   => [
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 0]),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => 0,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 0]),
                                ],
                            ],
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 1]),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => 1,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 1]),
                                ],
                            ],
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 2]),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => 2,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 2]),
                                ],
                            ],
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 3]),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => 3,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 3]),
                                ],
                            ],
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 7]),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => 7,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 7]),
                                ],
                            ],
                        ],
                        'options' => [
                            'data-target' => Html::getInputId($model, 'advancebooking'),
                        ],
                    ],
                    'options'  => [
                        'data-prompt' => \Yii::t('UI10.3', '$INTERVAL_LEASE_DROPDOWN_NOOP$'),
                    ],
                ]) ?>

                <?= Html::error($model, 'advancebooking', ['class' => 'help-block']) ?>
            </div>
        </div>

    </div>

    <div class="col-sm-12  visit styleText">
        <h3><?= \Yii::t('UI10.3', '$MEETING_TIME_TITLE$') ?></h3>
        <div class="text-left">
            <?= \Yii::t('UI10.3', '$MEETING_TIME_DESCRIPTION$') ?>
        </div>
        <?= Html::beginTag('div', [
            'class' => implode(' ', [
                'form-group',
                'field-' . Html::getInputId($model, 'dealTimeFrom'),
                'field-' . Html::getInputId($model, 'dealTimeTo'),
                'field-' . Html::getInputId($model, 'timeZone'),
            ]),
        ]) ?>
        <div class="inline">
            <?= Html::activeHiddenInput($model, 'dealTimeFrom') ?>
            <?= Html::activeHiddenInput($model, 'dealTimeTo') ?>
            <?= Html::activeHiddenInput($model, 'timeZone') ?>

            <span class="grey col-sm-2 col-xs-12"><?= \Yii::t('UI10.3', '$MEETING_TIME_LABEL$') ?></span>
            <div class="right styleInput col-sm-10 col-xs-12">
                <?= ButtonDropdown::widget([
                    'id'               => 'deal_time_from-dropdown',
                    'label'            => '',
                    'containerOptions' => ['class' => 'hourFrom dropdown'],
                    'options'          => ['class' => 'btn-default'],
                    'dropdown'         => [
                        'id'      => 'deal_time_from-dropdown-menu',
                        'items'   => [
                            ['label' => '00:00', 'url' => '#'],
                            ['label' => '01:00', 'url' => '#'],
                            ['label' => '02:00', 'url' => '#'],
                            ['label' => '03:00', 'url' => '#'],
                            ['label' => '04:00', 'url' => '#'],
                            ['label' => '05:00', 'url' => '#'],
                            ['label' => '06:00', 'url' => '#'],
                            ['label' => '07:00', 'url' => '#'],
                            ['label' => '08:00', 'url' => '#'],
                            ['label' => '09:00', 'url' => '#'],
                            ['label' => '10:00', 'url' => '#'],
                            ['label' => '11:00', 'url' => '#'],
                            ['label' => '12:00', 'url' => '#'],
                            ['label' => '13:00', 'url' => '#'],
                            ['label' => '14:00', 'url' => '#'],
                            ['label' => '15:00', 'url' => '#'],
                            ['label' => '16:00', 'url' => '#'],
                            ['label' => '17:00', 'url' => '#'],
                            ['label' => '18:00', 'url' => '#'],
                            ['label' => '19:00', 'url' => '#'],
                            ['label' => '20:00', 'url' => '#'],
                            ['label' => '21:00', 'url' => '#'],
                            ['label' => '22:00', 'url' => '#'],
                            ['label' => '23:00', 'url' => '#'],
                        ],
                        'options' => ['class' => 'scroll-1', 'data-target' => Html::getInputId($model, 'dealTimeFrom')],
                    ],
                ]) ?>

                <?= \Yii::t('UI10.3', '$MEETING_TIME_LABEL2$') ?>

                <?= ButtonDropdown::widget([
                    'id'               => 'deal_time_to-dropdown',
                    'label'            => '',
                    'containerOptions' => ['class' => 'hourTo dropdown',],
                    'options'          => ['class' => 'btn-default',],
                    'dropdown'         => [
                        'id'      => 'deal_time_to-dropdown-menu',
                        'items'   => [
                            ['label' => '00:00', 'url' => '#'],
                            ['label' => '01:00', 'url' => '#'],
                            ['label' => '02:00', 'url' => '#'],
                            ['label' => '03:00', 'url' => '#'],
                            ['label' => '04:00', 'url' => '#'],
                            ['label' => '05:00', 'url' => '#'],
                            ['label' => '06:00', 'url' => '#'],
                            ['label' => '07:00', 'url' => '#'],
                            ['label' => '08:00', 'url' => '#'],
                            ['label' => '09:00', 'url' => '#'],
                            ['label' => '10:00', 'url' => '#'],
                            ['label' => '11:00', 'url' => '#'],
                            ['label' => '12:00', 'url' => '#'],
                            ['label' => '13:00', 'url' => '#'],
                            ['label' => '14:00', 'url' => '#'],
                            ['label' => '15:00', 'url' => '#'],
                            ['label' => '16:00', 'url' => '#'],
                            ['label' => '17:00', 'url' => '#'],
                            ['label' => '18:00', 'url' => '#'],
                            ['label' => '19:00', 'url' => '#'],
                            ['label' => '20:00', 'url' => '#'],
                            ['label' => '21:00', 'url' => '#'],
                            ['label' => '22:00', 'url' => '#'],
                            ['label' => '23:00', 'url' => '#'],
                        ],
                        'options' => ['class' => 'scroll-1', 'data-target' => Html::getInputId($model, 'dealTimeTo')],
                    ],
                ]) ?>

                <?= ButtonDropdown::widget([
                    'id'               => 'deal_timezone-dropdown',
                    'label'            => '',
                    'encodeLabel'      => false,
                    'containerOptions' => ['class' => 'timeZone dropdown',],
                    'options'          => ['class' => 'btn-default',],
                    'dropdown'         => [
                        'id'           => 'deal_timezone-dropdown-menu',
                        'items'        => [
                            [
                                'label'   => 'UTC <span class="red">-11:00</span> - Мидуэй',
                                'url'     => '#',
                                'options' => ['data-value' => 'Pacific/Midway'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">-10:00</span> - Гонолулу',
                                'url'     => '#',
                                'options' => ['data-value' => 'Pacific/Honolulu'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">-8:00</span> - Анкоридж',
                                'url'     => '#',
                                'options' => ['data-value' => 'America/Anchorage'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">-7:00</span> - Лос-Анджелес, Тихуана, Финикс',
                                'url'     => '#',
                                'options' => ['data-value' => 'America/Los_Angeles'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">-6:00</span> - Коста-Рика, Денвер, Реджайна',
                                'url'     => '#',
                                'options' => ['data-value' => 'America/Costa_Rica'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">-5:00</span> - Чикаго, Мехико',
                                'url'     => '#',
                                'options' => ['data-value' => 'America/Chicago'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">-4:00</span> - Нью-Йорк, Барбадос',
                                'url'     => '#',
                                'options' => ['data-value' => 'America/New_York'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">-3:00</span> - Буэнос-Айрес, Сантьяго, Сан-Паулу',
                                'url'     => '#',
                                'options' => ['data-value' => 'America/Argentina/La_Rioja'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">-2:00</span> - Готхоб',
                                'url'     => '#',
                                'options' => ['data-value' => 'America/Godthab'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">-1:00</span> - Острова Зелёного Мыса',
                                'url'     => '#',
                                'options' => ['data-value' => 'Atlantic/Cape_Verde'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">+0:00</span> - Азорские острова',
                                'url'     => '#',
                                'options' => ['data-value' => 'Atlantic/Reykjavik'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">+1:00</span> - Лондон, Браззавиль',
                                'url'     => '#',
                                'options' => ['data-value' => 'Europe/London'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">+2:00</span> - Амстердам, Брюссель',
                                'url'     => '#',
                                'options' => ['data-value' => 'Europe/Stockholm'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">+3:00</span> - Москва, Стамбул',
                                'url'     => '#',
                                'options' => ['data-value' => 'Europe/Moscow'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">+4:00</span> - Тбилиси, Дубай',
                                'url'     => '#',
                                'options' => ['data-value' => 'Asia/Tbilisi'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">+4:30</span> - Тегеран, Кабул',
                                'url'     => '#',
                                'options' => ['data-value' => 'Asia/Kabul'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">+5:00</span> - Екатеринбург, Баку',
                                'url'     => '#',
                                'options' => ['data-value' => 'Asia/Yekaterinburg'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">+6:00</span> - Омск, Алматы',
                                'url'     => '#',
                                'options' => ['data-value' => 'Asia/Omsk'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">+7:00</span> - Красноярск, Бангкок',
                                'url'     => '#',
                                'options' => ['data-value' => 'Asia/Novosibirsk'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">+8:00</span> - Гонконг, Иркутск, Куала-Лумпур',
                                'url'     => '#',
                                'options' => ['data-value' => 'Asia/Irkutsk'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">+9:00</span> - Якутск, Сеул, Токио',
                                'url'     => '#',
                                'options' => ['data-value' => 'Asia/Yakutsk'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">+10:00</span> - Магадан, Сидней',
                                'url'     => '#',
                                'options' => ['data-value' => 'Australia/Sydney'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">+11:00</span> - Сахалин',
                                'url'     => '#',
                                'options' => ['data-value' => 'Asia/Sakhalin'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">+12:00</span> - Окленд, Маджуро, Фиджи',
                                'url'     => '#',
                                'options' => ['data-value' => 'Asia/Kamchatka'],
                            ],
                            [
                                'label'   => 'UTC <span class="red">+13:00</span> - Тонгатапу',
                                'url'     => '#',
                                'options' => ['data-value' => 'Pacific/Enderbury'],
                            ],
                        ],
                        'encodeLabels' => false,
                        'options'      => ['data-target' => Html::getInputId($model, 'timeZone')],
                    ],
                ]) ?>

            </div>
        </div>

        <?= Html::error($model, 'timeZone', ['class' => 'help-block']) ?>
        <?= Html::endTag('div') ?>
    </div>

    <div class="col-sm-12 pause styleText styleInput">
        <h3><?= \Yii::t('UI10.3', '$RENTAL_BREAK_TITLE$') ?></h3>
        <p class="text-left"><?= \Yii::t('UI10.3', '$RENTAL_BREAK_DESCRIPTION$') ?></p>

        <div class="hidden-xs hidden-sm">
            <?= $form->field($model, 'restbetweenrest')->widget(ToggleButtonGroup::className(), [
                'items' => [
                    0 => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 0]),
                    1 => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 1]),
                    2 => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 2]),
                    3 => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 3]),
                    7 => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 7]),
                ],
                'type'  => 'radio',
            ])->label(false) ?>
        </div>

        <div class="hidden-md hidden-lg">
            <div class="form-group field-<?= Html::getInputId($model, 'restbetweenrest') ?>">
                <?= Html::activeLabel($model, 'restbetweenrest',
                    ['label' => \Yii::t('UI10.3', '$RENTAL_BREAK_LABEL$')]) ?>

                <?= ButtonDropdown::widget([
                    'id'       => 'betweenrest-dropdown',
                    'id'       => 'restbetween-mob',
                    'label'    => '',
                    'dropdown' => [
                        'id'      => 'betweenrest-dropdown-menu',
                        'items'   => [
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 0]),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => 0,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 0]),
                                ],
                            ],
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 1]),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => 1,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 1]),
                                ],
                            ],
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 2]),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => 2,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 2]),
                                ],
                            ],
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 3]),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => 3,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 3]),
                                ],
                            ],
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 7]),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => 7,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_OF_SEVERAL_DAYS$', ['d' => 7]),
                                ],
                            ],
                        ],
                        'options' => [
                            'data-target' => Html::getInputId($model, 'restbetweenrest'),
                        ],
                    ],
                    'options'  => [
                        'data-prompt' => \Yii::t('UI10.3', '$INTERVAL_LEASE_DROPDOWN_NOOP$'),
                    ],
                ]) ?>

                <?= Html::error($model, 'restbetweenrest', ['class' => 'help-block']) ?>
            </div>

        </div>
    </div>

    <div class="col-sm-12 yearPlan styleText styleInput">
        <h3><?= \Yii::t('UI10.3', '$FUTURE_PLANS_TITLE$') ?></h3>
        <p class="text-left">
            <?= \Yii::t('UI10.3', '$FUTURE_PLANES_DESCRIPTION$') ?>
        </p>

        <div class="hidden-xs hidden-sm">
            <?= $form->field($model, 'futureplans')->widget(ToggleButtonGroup::className(), [
                'items' => [
                    90  => \Yii::t('UI10.3', '$INTERVAL_OF_LONG_DAYS$', ['d' => 90]),
                    180 => \Yii::t('UI10.3', '$INTERVAL_OF_LONG_DAYS$', ['d' => 180]),
                    365 => \Yii::t('UI10.3', '$INTERVAL_OF_LONG_DAYS$', ['d' => 365]),
                ],
                'type'  => 'radio',
            ])->label(false) ?>
        </div>
        <div class="hidden-md hidden-lg">
            <div class="form-group field-<?= Html::getInputId($model, 'futureplans') ?>">
                <?= Html::activeLabel($model, 'futureplans', ['label' => \Yii::t('UI10.3', '$FUTURE_PLANS_LABEL$')]) ?>

                <?= ButtonDropdown::widget([
                    'id'       => 'futureplans-mob',
                    'label'    => '',
                    'dropdown' => [
                        'id'      => 'futureplans-mob-menu',
                        'items'   => [
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_OF_LONG_DAYS$', ['d' => 90]),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => 90,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_OF_LONG_DAYS$', ['d' => 90]),
                                ],
                            ],
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_OF_LONG_DAYS$', ['d' => 180]),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => 180,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_OF_LONG_DAYS$', ['d' => 180]),
                                ],
                            ],
                            [
                                'label'   => \Yii::t('UI10.3', '$INTERVAL_OF_LONG_DAYS$', ['d' => 365]),
                                'url'     => '#',
                                'options' => [
                                    'data-value' => 365,
                                    'data-vtext' => \Yii::t('UI10.3', '$INTERVAL_OF_LONG_DAYS$', ['d' => 365]),
                                ],
                            ],
                        ],
                        'options' => [
                            'data-target' => Html::getInputId($model, 'futureplans'),
                        ],
                    ],
                    'options'  => [
                        'data-prompt' => \Yii::t('UI10.3', '$INTERVAL_LEASE_DROPDOWN_NOOP$'),
                    ],
                ]) ?>

                <?= Html::error($model, 'futureplans', ['class' => 'help-block']) ?>
            </div>

        </div>
    </div>

    <div class="col-sm-12 datePicker styleText">
        <h3><?= \Yii::t('UI10.3', '$EXCLUDE_DATES_TITLE$') ?></h3>
        <p class="text-left"><?= \Yii::t('UI10.3', '$EXCLUDE_DATES_DESCRIPTION$') ?></p>

        <?= Html::activeHiddenInput($model, 'exclude_dates') ?>
        <?= Html::activeHiddenInput($model, 'exclude_lock_after') ?>

        <input type="text" class="hidden" id="dateVisible">
    </div>

<?= $this->render('_progress-filled', [
    'submit' => !$model->product->status ? 1 : 3,
    'status' => !$model->product->isNewRecord || (integer)$model->product->status
]) ?>

<?php
$transTiemUnit = \yii\helpers\Json::encode([
    Product::LEASING_HOUR  => \Yii::t('UI10.4', '$SMALL_CLARIFICATION_PRICE_TIME_HOUR$'),
    Product::LEASING_DAY   => \Yii::t('UI10.4', '$SMALL_CLARIFICATION_PRICE_TIME_DAY$'),
    Product::LEASING_WEEK  => \Yii::t('UI10.4', '$SMALL_CLARIFICATION_PRICE_TIME_WEEK$'),
    Product::LEASING_MONTH => \Yii::t('UI10.4', '$SMALL_CLARIFICATION_PRICE_TIME_MONTH$'),
]);

$this->registerJs(<<<JS
    var dropdowndepends = {
        'productform-minrent': {
            1: [1,2,3,4,5,6], // days
            2: [1,2,3], // weeks
            3: [1,2,3,4,5,6,7,8,9,10,11,12], // months
            4: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23], // hours
        },
        'productform-maxrent': {
            1: [1,2,3,4,5,6], // days
            2: [1,2,3], // weeks
            3: [1,2,3,4,5,6,7,8,9,10,11,12], // months
            4: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23], // hours
        }
    };
JS
    , \yii\web\View::POS_BEGIN);

$this->registerJsFile('/js/bs.dropdown-depends.js', [
    'position' => \yii\web\View::POS_END,
    'depends'  => [
        'frontend\assets\AppAsset',
    ],
]);

$this->registerJs(<<<JS
var tunit = $transTiemUnit;

jQuery("#unit-of-time-dropdown-menu").on("change.bs.dropdown", function(event, vid, vtext) {
    var val = _.get(tunit, vid, "");
    if (!val.length){ return; }
    
     jQuery("#settings")
        .find(".prise ._price_period,.longPledge ._price_period").text(val)
        .end()
        .find(".discount ._price_period").text(" " + _.toLower(val));
}).trigger("change.bs.dropdown", [{$model->minRentVal}]);


// поле "заранее о бронировании"
$("#adv-mob").parent().on("change.bs.dropdown", function(event, vid){
     $("#productform-advancebooking").find("input[value='" + vid + "']").trigger("click");
});
$("#productform-advancebooking").on("click", "label", function(event) {
    var label = $(event.currentTarget);
    if (label.is(".active")) {
        return false;
    }
    
    var val = label.find("input").val();
    var a = $("#adv-mob").next("ul").find("li[data-value='" + val + "'] a");
    
    _.delay(function(){
        a.trigger("click");
    }, 300);
});

// поле "перерыв на обслуживание"
$("#restbetween-mob").parent().on("change.bs.dropdown", function(event, vid){
     $("#productform-restbetweenrest").find("input[value='" + vid + "']").trigger("click");
});
$("#productform-restbetweenrest").on("click", "label", function(event) {
    var label = $(event.currentTarget);
    if (label.is(".active")) {
        return false;
    }
    
    var val = label.find("input").val();
    var a = $("#restbetween-mob").next("ul").find("li[data-value='" + val + "'] a");
    
    _.delay(function(){
        a.trigger("click");
    }, 300);
});

// поле "планы на год вперед"
$("#futureplans-mob").parent().on("change.bs.dropdown", function(event, vid){
     $("#productform-futureplans").find("input[value='" + vid + "']").trigger("click");
});
$("#productform-futureplans").on("click", "label", function(event) {
    var label = $(event.currentTarget);
    if (label.is(".active")) {
        return false;
    }
    
    var val = label.find("input").val();
    var a = $("#futureplans-mob").next("ul").find("li[data-value='" + val + "'] a");
    
    _.delay(function(){
        a.trigger("click");
    }, 300);
});
$("#futureplans-mob").next("ul").find("li[data-value='" + $("#productform-futureplans").find("input:checked:first").val() + "'] a").trigger("click");

JS
);

$btn_text_on  = \Yii::t('UI10.3', '$CALENDAR_EXCLUDE_BOOKING_BTN_ON$');
$btn_text_off = \Yii::t('UI10.3', '$CALENDAR_EXCLUDE_BOOKING_BTN_OFF$');
$this->registerJs(<<<JS
var di = jQuery("#dateVisible");
var dp = di.data("dateRangePicker").getDatePicker();
dp.find("div.time").remove();
dp.find(".month-wrapper").after('<div class="wrappBtn"><button type="button" class="btn btn-default btn-block" id="exclude_dates_btn"><span class="ton">{$btn_text_on}</span><span class="toff">{$btn_text_off}</span></button></div>');
var bl = dp.find("#exclude_dates_btn").addClass("ton");

var datesInput = jQuery("#productform-exclude_dates");
var dateLockAfter = jQuery("#productform-exclude_lock_after");

var excludedDate = [];

di.on("datepicker-change", function(event, obj) {
    var actualTime = moment(obj.date1.getTime());
    var timestamp = actualTime.clone();
    
    // переводим дату в unix и устанавливаем время 00:01:00
    timestamp.utc().add(actualTime.utcOffset(), "m").set({hour: 0, minute: 1, second: 0, millisecond: 0});
    var tsu = timestamp.valueOf();
    var lockAfter = _.toInteger(dateLockAfter.val());       // 0 or unix timestamp
    
    if (lockAfter && tsu >= lockAfter) {
        // do nothing
        return;
    } else if (jQuery.inArray(tsu, excludedDate) >= 0) {
        // не факт, что timestamp у атрибута останется такой же, с момента как дату выделили. Будем перелистывать все даты
        _.pull(excludedDate, tsu);
        
        dp.find(".day.active").each(function(index, element) {
            var e = jQuery(element);
            var tm = _.toInteger(e.attr("time"));
            var m = moment.utc(tm);
            m.add(moment(tm).utcOffset(), "m")
                .set({hour: 0, minute: 1, second: 0, millisecond: 0});
            // console.log(m,timestamp);
            if (m.isSame(timestamp)) {
                e.removeClass("active");
                return false;
            }
        });
    } else {
        excludedDate.push(tsu);
        dp.find(".day[time='" + actualTime + "']").addClass("active");
    }
    
    datesInput.val(_.join(excludedDate,","));
});
dp.on("click", "thead .next,thead .prev", function() {
    _.delay(reselect, 50);
});
dp.on("click", "#exclude_dates_btn", function() {
    var cd = di.val();
    if (!cd.length) {
        return;
    }
    
    var lockAfter = _.toInteger(dateLockAfter.val());
    // день начиная с которого нужно блокировать или разблокировать календарь
    var m = moment.utc(cd, "DD.MM.YYYY").add(1, "days").minutes(1);         // тут важно правильно задать формат даты
    if (lockAfter) {
        var tm = m.valueOf();
        dp.find("tbody .day[time]").filter(function(index, element) {
            var ts = _.toInteger(jQuery(element).attr("time"));
            var mitem = moment.utc(ts).add(moment(ts).utcOffset(), "m").set({hour: 0, minute: 1, second: 0, millisecond: 0});
            
            return  mitem.isSameOrAfter( m );
        }).removeClass("active");
        
        // добавить даты между начальным днем блокирования и текущим днём 
        excludedDate = _.concat(excludedDate, _.range(lockAfter, tm, 86400000));
        // удалить даты после выбранной
        _.remove(excludedDate, function(o){
            return m.isSameOrBefore(o);
        });
        
        bl.toggleClass("ton toff");
        
        dateLockAfter.val("");
    } else {
        bl.toggleClass("ton toff");
        
        dp.find("tbody .day[time]").filter(function(index, element) {
            var ts = _.toInteger(jQuery(element).attr("time"));
            var mitem = moment.utc(ts).add(moment(ts).utcOffset(), "m").set({hour: 0, minute: 1, second: 0, millisecond: 0});
            
            return  mitem.isSameOrAfter( m );
        }).addClass("active");
        
        _.remove(excludedDate, function(o) {
            return m.isSameOrBefore(o);
        });
        _.sortBy(excludedDate);
        
        dateLockAfter.val(m.valueOf());
    }
    
    datesInput.val(_.join(excludedDate, ","));
});

function reselect(){
    var lockAfter = _.toInteger(dateLockAfter.val());
    
    dp.find("tbody .day.toMonth").each(function(index, element) {
        var e = jQuery(element);
        var tm = _.toInteger(e.attr("time"));
        var m = moment.utc(tm).add(moment(tm).utcOffset(), "m").set({hour: 0, minute: 1, second: 0, millisecond: 0});
        
        if (lockAfter && m.isSameOrAfter(lockAfter) || jQuery.inArray(m.valueOf(), excludedDate) >= 0){
            e.addClass("active");
        }
    });
}

excludedDate = _.map(_.split(datesInput.val(), ","), _.toInteger);
reselect();
if (_.toInteger(dateLockAfter.val()) > 0){
    bl.toggleClass("ton toff");
}

dp.on("mousedown", ".month1 div.day", function(event) {
    // console.log(event.currentTarget, event.ctrlKey);
});

JS
);

$this->registerCss(<<<CSS
.date-picker-wrapper .month-wrapper table .day.active{
    background-color: #e35f46;
    color: #f6f6f6 !important;
}
.date-picker-wrapper .month-wrapper table .day.checked{
    background-color: transparent !important;
}
.date-picker-wrapper .month-wrapper table .day.checked.hovering{
    background-color: #b98f5c !important;
}
.date-picker-wrapper .month-wrapper table .day.checked,
.date-picker-wrapper .month-wrapper table .day.checked.hovering,
.date-picker-wrapper .day.first-date-select{
    color: #000 !important;
}
.date-picker-wrapper .month-wrapper table .day.checked.active{
    background-color: #e39c5a !important;
    color: #edf6d0 !important;
}
.date-picker-wrapper .month-wrapper table .day.active.hovering{
    background-color: #b98f5c !important;
}
#exclude_dates_btn{
    white-space: normal;
    max-width: 280px;
}
#exclude_dates_btn.ton .ton,
#exclude_dates_btn.toff .toff{
    display: inline-block;
}
#exclude_dates_btn.ton .toff,
#exclude_dates_btn.toff .ton{
    display: none;
}
.dropdown-menu {
    z-index: 1005 !important;
}
CSS
);