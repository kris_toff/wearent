<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 13.07.2017
 * Time: 16:55
 */

?>

    <div class="thumbnail col-sm-6 col-xs-12 file-preview-frame {frameClass}"
            data-id="{previewId}"
            data-fileindex="{fileindex}"
            data-template="{template}" type="image">
        <div class="kv-file-content img">
            <img src="{data}"
                    class="img-responsive kv-preview-data file-preview-image"
                    title="{caption}"
                    alt="{caption}" />

            <div class="shadow">
                <div class="header-line">
                    <button type="button" class="kv-file-zoom">{zoomIcon}</button>
                </div>
                <div class="header-line text-right">
                    <button class="kv-file-remove btn btn-xs btn-default" type="button">
                        <i>
                            <svg width="8px" height="8px" viewBox="0 0 8 8" >
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
                                    <g id="UI10.2---Создание-товара2--Загрузка-изображений-/-2" transform="translate(-1188.000000, -653.000000)" stroke="#FFFFFF" fill="#FFFFFF">
                                        <g id="Group" transform="translate(210.000000, 151.000000)">
                                            <g id="Group-6" transform="translate(19.000000, 84.000000)">
                                                <g id="upload" transform="translate(1.000000, 49.000000)">
                                                    <g id="Group-8" transform="translate(0.000000, 355.000000)">
                                                        <g id="mini_photo-copy-4" transform="translate(500.000000, 0.000000)">
                                                            <g id="close" transform="translate(454.000000, 10.000000)">
                                                                <g transform="translate(4.888889, 4.888889)" id="Combined-Shape-Copy-3">
                                                                    <path d="M3.11111111,2.48257175 L0.832788199,0.204248838 C0.659221847,0.0306824861 0.37781519,0.0306824861 0.204248838,0.204248838 L0.204248838,0.832788199 L2.48257175,3.11111111 L0.151606014,5.44207685 C-0.0219603375,5.6156432 -0.0219603375,5.89704986 0.151606014,6.07061621 C0.325172366,6.24418256 0.606579024,6.24418256 0.780145375,6.07061621 L3.11111111,3.73965047 L5.44207685,6.07061621 C5.6156432,6.24418256 5.89704986,6.24418256 6.07061621,6.07061621 C6.24418256,5.89704986 6.24418256,5.6156432 6.07061621,5.44207685 L3.73965047,3.11111111 L6.01797338,0.832788199 C6.19153974,0.659221847 6.19153974,0.37781519 6.01797338,0.204248838 L5.38943402,0.204248838 L3.11111111,2.48257175 Z"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg></i></button>
                </div>
                <div class="middleLine text-center">
                    <a class="mk-default"><?= \Yii::t('UI10.2', '$MAKE_DEFAULT_BTN$') ?></a>
                </div>
                <div class="footer-line">
                    <a data-direction="right"><?= \Yii::t('UI10.2', '$MOVE_THUMBNAIL_RIGHT_BTN$') ?></a>
                    <a data-target="crop"><?= \Yii::t('UI10.2', '$CROP_IMAGE_BTN$') ?></a>
                    <a data-direction="left"><?= \Yii::t('UI10.2', '$MOVE_THUMBNAIL_LEFT_BTN$') ?></a>
                </div>
            </div>
            <div class="v-default text-center">
                <?= \Yii::t('UI10.2', '$THUMBNAIL_MARK_DEFAULT_PHOTO$') ?>
            </div>

        </div>

        {footer}
    </div>

<?php
/*


    <div class="col-sm-12 file">
        <div class="topLine">
            <div class="btn-group" role="group" aria-label="...">
                <button type="button" id="all" class="btn ">Все</button>
                <button type="button" id="toggleFoto" class="btn ">Фото</button>
                <button type="button" id="toggleVideo" class="btn ">Видео</button>
            </div>
            <span>Нет файлов</span>
        </div>


        <div class="row">
            <div class="col-sm-6 videoBlock">
                <div class="thumbnail">
                    <div class="img">
                        <img class="img-responsive" src="/img/KTC.jpg" alt="KTC">
                        <span class="shadow">
                                                    <span class="topLine text-right">
                                                        <span class="glyphicon glyphicon-remove"></span>
                                                    </span>

                                                    <span class="middleLine text-center">
                                                        <button class="">Сделать основной</button>
                                                    </span>

                                                    <span class="bottomLine">
                                                        <a href="#">Вправо</a>
                                                        <a href="#">Обрезать</a>
                                                        <a href="#">Влево</a>
                                                    </span>
                                                </span>
                    </div>

                    <div class="caption styleInput">
                        <textarea name="" id="" placeholder="Описние к фото"></textarea>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 imgBlock">
                <div class="thumbnail">
                    <div class="img">
                        <img class="img-responsive" src="/img/KTC.jpg" alt="KTC">
                        <span class="shadow">
                                                    <span class="topLine text-right">
                                                        <span class="glyphicon glyphicon-remove"></span>
                                                    </span>

                                                    <span class="middleLine text-center">
                                                        <button class="">Сделать основной</button>
                                                    </span>

                                                    <span class="bottomLine">
                                                        <a href="#">Вправо</a>
                                                        <a href="#">Обрезать</a>
                                                        <a href="#">Влево</a>
                                                    </span>
                                                </span>
                    </div>

                    <div class="caption styleInput">
                        <textarea name="" id="" placeholder="Описние к фото"></textarea>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 videoBlock">
                <div class="thumbnail">
                    <div class="img">
                        <img class="img-responsive" src="/img/KTC.jpg" alt="KTC">
                        <span class="shadow">
                                                    <span class="topLine text-right">
                                                        <span class="glyphicon glyphicon-remove"></span>
                                                    </span>

                                                    <span class="middleLine text-center">
                                                        <button class="">Сделать основной</button>
                                                    </span>

                                                    <span class="bottomLine">
                                                        <a href="#">Вправо</a>
                                                        <a href="#">Обрезать</a>
                                                        <a href="#">Влево</a>
                                                    </span>
                                                </span>
                    </div>

                    <div class="caption styleInput">
                        <textarea name="" id="" placeholder="Описние к фото"></textarea>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 imgBlock">
                <div class="thumbnail">
                    <div class="img">
                        <img class="img-responsive" src="/img/KTC.jpg" alt="KTC">
                        <span class="shadow">
                                                    <span class="topLine text-right">
                                                        <span class="glyphicon glyphicon-remove"></span>
                                                    </span>

                                                    <span class="middleLine text-center">
                                                        <button class="">Сделать основной</button>
                                                    </span>

                                                    <span class="bottomLine">
                                                        <a href="#">Вправо</a>
                                                        <a href="#">Обрезать</a>
                                                        <a href="#">Влево</a>
                                                    </span>
                                                </span>
                    </div>

                    <div class="caption styleInput">
                        <textarea name="" id="" placeholder="Описние к фото"></textarea>
                    </div>
                </div>
            </div>
        </div>

    </div>
*/