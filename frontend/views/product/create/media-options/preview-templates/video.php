<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 17.07.2017
 * Time: 21:22
 */

/** @var \yii\web\View $this */
?>

<div class="thumbnail col-sm-6 col-xs-12 file-preview-frame {frameClass}" data-id="{previewId}" data-fileindex="{fileindex}" data-template="{template}" title="{caption}" type="video">
    <div class="kv-file-content video">
        <video class="kv-preview-data" width="{width}" height="{height}" controls>
            <source src="{data}" type="{type}" />
            video not supported
        </video>
        <div class="shadow">
            <div class="header-line text-right">
                <button class="kv-file-remove btn btn-xs btn-default" type="button">
                    <i>
                        <svg width="8px" height="8px" viewBox="0 0 8 8" >
                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
                                <g id="UI10.2---Создание-товара2--Загрузка-изображений-/-2" transform="translate(-1188.000000, -653.000000)" stroke="#FFFFFF" fill="#FFFFFF">
                                    <g id="Group" transform="translate(210.000000, 151.000000)">
                                        <g id="Group-6" transform="translate(19.000000, 84.000000)">
                                            <g id="upload" transform="translate(1.000000, 49.000000)">
                                                <g id="Group-8" transform="translate(0.000000, 355.000000)">
                                                    <g id="mini_photo-copy-4" transform="translate(500.000000, 0.000000)">
                                                        <g id="close" transform="translate(454.000000, 10.000000)">
                                                            <g transform="translate(4.888889, 4.888889)" id="Combined-Shape-Copy-3">
                                                                <path d="M3.11111111,2.48257175 L0.832788199,0.204248838 C0.659221847,0.0306824861 0.37781519,0.0306824861 0.204248838,0.204248838 L0.204248838,0.832788199 L2.48257175,3.11111111 L0.151606014,5.44207685 C-0.0219603375,5.6156432 -0.0219603375,5.89704986 0.151606014,6.07061621 C0.325172366,6.24418256 0.606579024,6.24418256 0.780145375,6.07061621 L3.11111111,3.73965047 L5.44207685,6.07061621 C5.6156432,6.24418256 5.89704986,6.24418256 6.07061621,6.07061621 C6.24418256,5.89704986 6.24418256,5.6156432 6.07061621,5.44207685 L3.73965047,3.11111111 L6.01797338,0.832788199 C6.19153974,0.659221847 6.19153974,0.37781519 6.01797338,0.204248838 L5.38943402,0.204248838 L3.11111111,2.48257175 Z"></path>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg></i></button>
            </div>
<!--            <div class="middleLine text-center">-->
<!--                <a>Play</a>-->
<!--            </div>-->
            <div class="footer-line text-center">
                <a class="mk-default"><?= \Yii::t('UI10.2', '$MAKE_DEFAULT_BTN$') ?></a>
            </div>
        </div>
        <div class="v-default text-center">
            <?= \Yii::t('UI10.2', '$THUMBNAIL_MARK_DEFAULT_VIDEO$') ?>
        </div>
    </div>

    {footer}
</div>
