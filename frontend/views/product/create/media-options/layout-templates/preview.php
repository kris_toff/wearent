<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 13.07.2017
 * Time: 16:47
 */

/** @var \yii\web\View $this */
/** @var array[] $iniMedia */
?>

    <div class="media-preview {class}">
        <div class="noneFile text-center <?= count($iniMedia['preview']) ? 'hidden' : '' ?>">
            <svg width="177px"
                    height="140px"
                    viewBox="0 0 177 140"
                    version="1.1"
                    xmlns="http://www.w3.org/2000/svg"
                    xmlns:xlink="http://www.w3.org/1999/xlink">
                <defs></defs>
                <g id="Page-1"
                        stroke="none"
                        stroke-width="1"
                        fill="none"
                        fill-rule="evenodd">
                    <g id="UI10.2---Создание-товара2--Загрузка-изображений"
                            transform="translate(-632.000000, -866.000000)">
                        <g id="Group" transform="translate(210.000000, 151.000000)">
                            <g id="Page-1" transform="translate(422.000000, 715.000000)">
                                <path d="M28.9411,122.167 L6.5001,122.167 C3.1861,122.167 0.5001,119.48 0.5001,116.167 L0.5001,6.5 C0.5001,3.187 3.1861,0.5 6.5001,0.5 L169.8331,0.5 C173.1471,0.5 175.8331,3.187 175.8331,6.5 L175.8331,116.167 C175.8331,119.48 173.1471,122.167 169.8331,122.167 L147.5571,122.167"
                                        id="Stroke-1"
                                        stroke="#AFAFAF"
                                        stroke-linecap="round"
                                        stroke-linejoin="round"></path>
                                <path d="M11.1066,11.1025 C11.1066,11.7215 10.6046,12.2235 9.9856,12.2235 C9.3666,12.2235 8.8646,11.7215 8.8646,11.1025 C8.8646,10.4835 9.3666,9.9815 9.9856,9.9815 C10.6046,9.9815 11.1066,10.4835 11.1066,11.1025"
                                        id="Fill-3" fill="#F55656"></path>
                                <path d="M17.8307,11.1025 C17.8307,11.7215 17.3297,12.2235 16.7097,12.2235 C16.0907,12.2235 15.5897,11.7215 15.5897,11.1025 C15.5897,10.4835 16.0907,9.9815 16.7097,9.9815 C17.3297,9.9815 17.8307,10.4835 17.8307,11.1025"
                                        id="Fill-5" fill="#E8B434"></path>
                                <path d="M24.5553,11.1025 C24.5553,11.7215 24.0533,12.2235 23.4343,12.2235 C22.8153,12.2235 22.3133,11.7215 22.3133,11.1025 C22.3133,10.4835 22.8153,9.9815 23.4343,9.9815 C24.0533,9.9815 24.5553,10.4835 24.5553,11.1025"
                                        id="Fill-7" fill="#4DC97B"></path>
                                <polyline id="Stroke-9"
                                        stroke="#AFAFAF"
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        points="147.5568 130.959 147.5568 48.839 123.1558 24.438"></polyline>
                                <path d="M45.4455,24.4385 L123.1555,24.4385" id="Stroke-11"
                                        stroke="#AFAFAF" stroke-linecap="round"
                                        stroke-linejoin="round"></path>
                                <polyline id="Stroke-13"
                                        stroke="#AFAFAF"
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        points="28.9411 48.8872 28.9411 24.4382 52.7951 24.4382"></polyline>
                                <polyline id="Stroke-15"
                                        stroke="#AFAFAF"
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        points="147.514617 130.33188 147.514617 139.2129 28.9411 139.2129 28.9411 44.5219"></polyline>
                                <path d="M123.0426,24.4385 L123.0426,48.9525" id="Stroke-39"
                                        stroke="#AFAFAF" stroke-linecap="round"
                                        stroke-linejoin="round"></path>
                                <path d="M147.5568,48.9526 L123.0428,48.9526" id="Stroke-41"
                                        stroke="#AFAFAF" stroke-linecap="round"
                                        stroke-linejoin="round"></path>
                                <g id="Group-12"
                                        transform="translate(72.000000, 103.000000)"
                                        stroke="#AFAFAF"
                                        stroke-linecap="round"
                                        stroke-linejoin="round">
                                    <path d="M0.1105,0.5254 L20.8545,0.5254"
                                            id="Stroke-29"></path>
                                    <path d="M0.1105,5.9478 L31.8545,5.9478"
                                            id="Stroke-31"></path>
                                    <path d="M0.1105,11.3701 L27.8545,11.3701"
                                            id="Stroke-33"></path>
                                    <path d="M0.1105,16.7925 L31.8545,16.7925"
                                            id="Stroke-35"></path>
                                    <path d="M25.7502,0.5254 L31.8542,0.5254"
                                            id="Stroke-93"></path>
                                </g>
                                <g id="Group-8" transform="translate(63.000000, 41.000000)"
                                        stroke="#E8B434" stroke-linecap="round"
                                        stroke-linejoin="round">
                                    <path d="M50.0231,24.9976 C50.0231,38.7866 38.8451,49.9636 25.0571,49.9636 C11.2691,49.9636 0.0911,38.7866 0.0911,24.9976 C0.0911,11.2096 11.2691,0.0316 25.0571,0.0316 C38.8451,0.0316 50.0231,11.2096 50.0231,24.9976 Z"
                                            id="Stroke-27"></path>
                                    <path d="M12.759,39.6328 C12.759,32.8408 18.265,27.3348 25.057,27.3348 C31.849,27.3348 37.356,32.8408 37.356,39.6328"
                                            id="Stroke-37"></path>
                                    <path d="M10.5822,16.1089 L15.5322,21.0589"
                                            id="Stroke-95"></path>
                                    <path d="M10.5822,21.0586 L15.5322,16.1086"
                                            id="Stroke-97"></path>
                                    <path d="M34.5822,16.1089 L39.5322,21.0589"
                                            id="Stroke-99"></path>
                                    <path d="M34.5822,21.0586 L39.5322,16.1086"
                                            id="Stroke-101"></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
        </div>
        <div class="preview-thumbnails">
            <?php foreach ($iniMedia['preview'] as $k => $item): ?>

                <?= strtr($this->render("/product/create/media-options/preview-templates/{$iniMedia['config'][$k]['type']}"),
                    [
                        '{frameClass}' => '',
                        '{previewId}'  => $iniMedia['config'][ $k ]['key'],
                        '{fileindex}'  => '',
                        '{template}'   => '',
                        '{data}'       => $item,
                        '{caption}'    => '',
                        '{footer}'     => $this->render('/product/create/media-options/layout-templates/footer-' . $iniMedia['config'][$k]['type'], [
                            'v' => $iniMedia['extra'][ $k ]['description'],
                        ]),
                        '{type}'       => 'video/mp4',
                        '{zoomIcon}' => '<i class="glyphicon glyphicon-zoom-in"></i>'
                    ]) ?>

                <?php
                endforeach;
                ?>
        </div>
    </div>
    <div class="clearfix"></div>

<?php

/*
<div class="file-preview {class}">
    <div class="noneFile text-center">
        <svg width="177px"
                height="140px"
                viewBox="0 0 177 140"
                version="1.1"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink">
            <defs></defs>
            <g id="Page-1"
                    stroke="none"
                    stroke-width="1"
                    fill="none"
                    fill-rule="evenodd">
                <g id="UI10.2---Создание-товара2--Загрузка-изображений"
                        transform="translate(-632.000000, -866.000000)">
                    <g id="Group" transform="translate(210.000000, 151.000000)">
                        <g id="Page-1" transform="translate(422.000000, 715.000000)">
                            <path d="M28.9411,122.167 L6.5001,122.167 C3.1861,122.167 0.5001,119.48 0.5001,116.167 L0.5001,6.5 C0.5001,3.187 3.1861,0.5 6.5001,0.5 L169.8331,0.5 C173.1471,0.5 175.8331,3.187 175.8331,6.5 L175.8331,116.167 C175.8331,119.48 173.1471,122.167 169.8331,122.167 L147.5571,122.167"
                                    id="Stroke-1"
                                    stroke="#AFAFAF"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"></path>
                            <path d="M11.1066,11.1025 C11.1066,11.7215 10.6046,12.2235 9.9856,12.2235 C9.3666,12.2235 8.8646,11.7215 8.8646,11.1025 C8.8646,10.4835 9.3666,9.9815 9.9856,9.9815 C10.6046,9.9815 11.1066,10.4835 11.1066,11.1025"
                                    id="Fill-3" fill="#F55656"></path>
                            <path d="M17.8307,11.1025 C17.8307,11.7215 17.3297,12.2235 16.7097,12.2235 C16.0907,12.2235 15.5897,11.7215 15.5897,11.1025 C15.5897,10.4835 16.0907,9.9815 16.7097,9.9815 C17.3297,9.9815 17.8307,10.4835 17.8307,11.1025"
                                    id="Fill-5" fill="#E8B434"></path>
                            <path d="M24.5553,11.1025 C24.5553,11.7215 24.0533,12.2235 23.4343,12.2235 C22.8153,12.2235 22.3133,11.7215 22.3133,11.1025 C22.3133,10.4835 22.8153,9.9815 23.4343,9.9815 C24.0533,9.9815 24.5553,10.4835 24.5553,11.1025"
                                    id="Fill-7" fill="#4DC97B"></path>
                            <polyline id="Stroke-9"
                                    stroke="#AFAFAF"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    points="147.5568 130.959 147.5568 48.839 123.1558 24.438"></polyline>
                            <path d="M45.4455,24.4385 L123.1555,24.4385" id="Stroke-11"
                                    stroke="#AFAFAF" stroke-linecap="round"
                                    stroke-linejoin="round"></path>
                            <polyline id="Stroke-13"
                                    stroke="#AFAFAF"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    points="28.9411 48.8872 28.9411 24.4382 52.7951 24.4382"></polyline>
                            <polyline id="Stroke-15"
                                    stroke="#AFAFAF"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    points="147.514617 130.33188 147.514617 139.2129 28.9411 139.2129 28.9411 44.5219"></polyline>
                            <path d="M123.0426,24.4385 L123.0426,48.9525" id="Stroke-39"
                                    stroke="#AFAFAF" stroke-linecap="round"
                                    stroke-linejoin="round"></path>
                            <path d="M147.5568,48.9526 L123.0428,48.9526" id="Stroke-41"
                                    stroke="#AFAFAF" stroke-linecap="round"
                                    stroke-linejoin="round"></path>
                            <g id="Group-12"
                                    transform="translate(72.000000, 103.000000)"
                                    stroke="#AFAFAF"
                                    stroke-linecap="round"
                                    stroke-linejoin="round">
                                <path d="M0.1105,0.5254 L20.8545,0.5254"
                                        id="Stroke-29"></path>
                                <path d="M0.1105,5.9478 L31.8545,5.9478"
                                        id="Stroke-31"></path>
                                <path d="M0.1105,11.3701 L27.8545,11.3701"
                                        id="Stroke-33"></path>
                                <path d="M0.1105,16.7925 L31.8545,16.7925"
                                        id="Stroke-35"></path>
                                <path d="M25.7502,0.5254 L31.8542,0.5254"
                                        id="Stroke-93"></path>
                            </g>
                            <g id="Group-8" transform="translate(63.000000, 41.000000)"
                                    stroke="#E8B434" stroke-linecap="round"
                                    stroke-linejoin="round">
                                <path d="M50.0231,24.9976 C50.0231,38.7866 38.8451,49.9636 25.0571,49.9636 C11.2691,49.9636 0.0911,38.7866 0.0911,24.9976 C0.0911,11.2096 11.2691,0.0316 25.0571,0.0316 C38.8451,0.0316 50.0231,11.2096 50.0231,24.9976 Z"
                                        id="Stroke-27"></path>
                                <path d="M12.759,39.6328 C12.759,32.8408 18.265,27.3348 25.057,27.3348 C31.849,27.3348 37.356,32.8408 37.356,39.6328"
                                        id="Stroke-37"></path>
                                <path d="M10.5822,16.1089 L15.5322,21.0589"
                                        id="Stroke-95"></path>
                                <path d="M10.5822,21.0586 L15.5322,16.1086"
                                        id="Stroke-97"></path>
                                <path d="M34.5822,16.1089 L39.5322,21.0589"
                                        id="Stroke-99"></path>
                                <path d="M34.5822,21.0586 L39.5322,16.1086"
                                        id="Stroke-101"></path>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </svg>
    </div>
    <div>
        <div class="file-preview-thumbnails"></div>
        <div class="clearfix"></div>
        <div class="file-preview-status text-center text-success"></div>
        <div class="kv-fileinput-error"></div>
    </div>
</div>
*/