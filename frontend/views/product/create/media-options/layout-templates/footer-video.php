<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 13.07.2017
 * Time: 16:49
 */

use yii\bootstrap\Html;

/** @var \yii\web\View $this */
/** @var string $v */

?>

<div class="file-thumbnail-footer caption styleInput">
    <?= Html::textInput('description[]', $v, ['class' => 'file-thumbnail-description', 'placeholder' =>\Yii::t('UI10.2', '$THUMBNAIL_INPUT_VIDEO_PLACEHOLDER$') ]) ?>
</div>
