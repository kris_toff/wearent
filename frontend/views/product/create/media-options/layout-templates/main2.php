<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 13.07.2017
 * Time: 15:17
 */

use yii\bootstrap\Progress;
use yii\helpers\ArrayHelper;

/** @var \yii\web\View $this */
/** @var array[] $iniMedia */
/** @var array $placeholders */

$cf  = count($iniMedia['preview']);
$cfi = count(array_filter($iniMedia['config'], function($v) {
    return $v['type'] === 'image';
}));
$cfv = $cf - $cfi;

$pcnt = ceil($cf / 30 * 100);
$sm   = array_sum(ArrayHelper::getColumn($iniMedia['extra'], 'size', false)); // Bytes
?>

    {remove}
    {cancel}
    {upload}

    <div class="formDownload text-center">
        <div class="file-drop-zone">
            <div class="common-state">
                <p class="boldText"><b><?= \Yii::t('UI10.2', '$DRAG_FIELD_TITLE$') ?></b></p>
                <p class="smallText"><?= \Yii::t('UI10.2', '$DRAG_FIELD_TEXT$') ?></p>

                <div id="mediafile-statistics" class="<?= $cf ? '' : 'hidden' ?>">
                    <div>
                        <span class="pull-right progress-files"><?= $pcnt ?>%</span>
                        <div>
                            <span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span>
                            <span class="count-files">
                        <span class="i"><?= \Yii::t('UI10.2', '$COUNT_PHOTO_FILES$',
                                ['c' => $cfi]) ?></span>
                        <span class="v"><?= \Yii::t('UI10.2', '$COUNT_VIDEO_FILES$',
                                ['c' => $cfv]) ?></span>
                    </span>
                            <span class="size-files"><?= round($sm / (1024 * 1024), 2) ?> Mb</span>
                        </div>
                    </div>
                    <div class="kv-upload-progress hidden"
                            role="progressbar"
                            aria-valuenow="{percent}"
                            aria-valuemin="0"
                            aria-valuemax="100"
                            style="width:{percent}%;"
                            id="mediafile-progress">
                        {status}
                    </div>
                </div>

                {browse}
            </div>
            <div class="reverse-state">
                <p class="boldText"><b><?= \Yii::t('UI10.2', '$DRAG_FIELD_TITLE_HOVER$') ?></b></p>
            </div>
        </div>
    </div>

    <div class="od-define <?= $cf ? 'hidden' : '' ?>">
        <?= $this->render('//product/create/_media-define', [
            'placeholders' => $placeholders,
        ]) ?>
    </div>
    <div class="clearfix"></div>

    <div class="file">
        <div class="topLine">
            <div class="btn-group" role="group">
                <button type="button" id="all" class="btn" data-filter="all"><?= \Yii::t('UI10.2',
                        '$FILTER_ALL_BTN$') ?></button>
                <button type="button" id="toggleFoto" class="btn" data-filter="image"><?= \Yii::t('UI10.2',
                        '$FILTER_PHOTO_BTN$') ?></button>
                <button type="button" id="toggleVideo" class="btn" data-filter="video"><?= \Yii::t('UI10.2',
                        '$FILTER_VIDEO_BTN$') ?></button>
            </div>
            <span class="preview-title">
                <span class="noneFile <?= $cf ? 'hidden' : '' ?>"><?= \Yii::t('UI10.2', '$NOFILES_TITLE$') ?></span>
                <span class="countfiles <?= $cf ? '' : 'hidden' ?>"><?= \Yii::t('UI10.2', '$COUNT_ALL_FILES$',
                        ['c' => $cf]) ?></span>
            </span>
        </div>
    </div>

    {preview}

<?php

$this->registerCss(<<<CSS
#mediafile-statistics {
    min-width: 35%;
    max-width: 90%;
    width: 150px;
    margin-left: auto;
    margin-right: auto;
}
#mediafile-statistics .progress {
    height: 4px;
    background-color: #d8d8d8;
}
#mediafile-statistics .progress .progress-bar {
    background-color: #4dc97b;
}
CSS
);
