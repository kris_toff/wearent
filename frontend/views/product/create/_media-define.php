<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 12.07.2017
 * Time: 15:32
 */

use yii\helpers\ArrayHelper;

/** @var \yii\web\View $this */
/** @var array $placeholders */
?>

<div class="col-sm-4">
    <h5><a><?= \Yii::t('UI10.2', '$VIDEO_COLUMN_TITLE$') ?></a></h5>
    <p style="display: none;"><?= \Yii::t('UI10.2.PHOTO_VIDEO_INFO',
            ArrayHelper::getValue($placeholders, 'video_informer')) ?></p>
</div>
<div class="col-sm-4">
    <h5><a><?= \Yii::t('UI10.2', '$PHOTO_COLUMN_TITLE$') ?></a></h5>
    <p class="text-left" style="display: none;"><?= \Yii::t('UI10.2.PHOTO_VIDEO_INFORMER',
            ArrayHelper::getValue($placeholders, 'photo_informer', '')) ?></p>
</div>
<div class="col-sm-4">
    <h5><a><?= \Yii::t('UI10.2', '$RESTRICT_COLUMN_TITLE$') ?></a></h5>
    <div class="text-left" style="display: none;"><?= \Yii::t('UI10.2.PHOTO_VIDEO_INFORMER',
            ArrayHelper::getValue($placeholders, 'limits', ''), [
                'vd' => ArrayHelper::getValue($placeholders, 'max_video_duration', 0),
                'vs' => ArrayHelper::getValue($placeholders, 'max_video_size', 0),
                'ps' => ArrayHelper::getValue($placeholders, 'max_photo_size', 0),
                'c'  => ArrayHelper::getValue($placeholders, 'max_files', 0),
            ]) ?></div>
</div>
