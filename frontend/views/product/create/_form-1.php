<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.07.2017
 * Time: 23:03
 */

use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

/** @var \yii\web\View $this */
/** @var \yii\bootstrap\ActiveForm $form */
/** @var \frontend\models\ProductForm $model */
/** @var array $categories */
/** @var array $categoryBreadCrumbs */
/** @var array $placeholders */

$modelProduct = $model->product;

$maxVA = 1;
$attrs = $modelProduct->getEavAttributes()->all();
if (count($attrs)) {
    $attrs = array_chunk($attrs, ceil(count($attrs) / 2), false);
}
$excessAttrs = 0;

array_walk($attrs, function($v) use ($maxVA, &$excessAttrs) {
    $excessAttrs += count($v) - $maxVA;
});
?>

    <div class="col-sm-6 topBlock styleText">
        <p class="hidden-sm hidden-md hidden-lg beforeShow">
            <a href="#">
                <svg width="19px" height="19px" viewBox="0 0 19 19">
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="UI10.3--Создание-товара3--Календарь-свободных-дней"
                                transform="translate(-20.000000, -423.000000)"
                                fill-rule="nonzero"
                                fill="#E35F46">
                            <g id="PROF" transform="translate(0.000000, 396.000000)">
                                <g id="Group-10" transform="translate(13.000000, 26.000000)">
                                    <g id="Group" transform="translate(7.000000, 1.000000)">
                                        <path d="M3.8,9.5 L8.4,9.5 C8.7,9.5 8.9,9.3 8.9,9 L8.9,4.4 C8.9,4.1 8.7,3.9 8.4,3.9 L3.8,3.9 C3.5,3.9 3.3,4.1 3.3,4.4 L3.3,9 C3.3,9.3 3.5,9.5 3.8,9.5 Z M4.3,4.9 L7.9,4.9 L7.9,8.5 L4.3,8.5 L4.3,4.9 Z"
                                                id="Shape"></path>
                                        <path d="M18.3,0.8 L1,0.8 C0.7,0.8 0.5,1 0.5,1.3 L0.5,18.2 C0.5,18.5 0.7,18.7 1,18.7 L5.3,18.7 L9.6,18.7 L14,18.7 C14.3,18.7 14.5,18.5 14.5,18.2 L14.5,14.8 L18.3,14.8 C18.6,14.8 18.8,14.6 18.8,14.3 L18.8,12 L18.8,1.3 C18.8,1.1 18.6,0.8 18.3,0.8 Z M17.8,1.8 L17.8,11.6 L1.5,11.6 L1.5,1.8 L17.8,1.8 Z M9.2,17.7 L5.9,17.7 L5.9,12.6 L9.2,12.6 L9.2,17.7 Z M1.5,12.6 L4.8,12.6 L4.8,17.7 L1.5,17.7 L1.5,12.6 Z M13.5,17.7 L10.2,17.7 L10.2,12.6 L13.5,12.6 L13.5,17.7 Z M14.5,13.9 L14.5,12.6 L17.8,12.6 L17.8,13.9 L14.5,13.9 Z"
                                                id="Shape"></path>
                                        <path d="M11.3,5.1 L15.8,5.1 C16.1,5.1 16.3,4.9 16.3,4.6 C16.3,4.3 16.1,4.1 15.8,4.1 L11.3,4.1 C11,4.1 10.8,4.3 10.8,4.6 C10.8,4.9 11,5.1 11.3,5.1 Z"
                                                id="Shape"></path>
                                        <path d="M11.3,7.4 L14.3,7.4 C14.6,7.4 14.8,7.2 14.8,6.9 C14.8,6.6 14.6,6.4 14.3,6.4 L11.3,6.4 C11,6.4 10.8,6.6 10.8,6.9 C10.8,7.2 11,7.4 11.3,7.4 Z"
                                                id="Shape"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg> <?= \Yii::t('UI10', '$SHOW_PREVIEW_PRODUCT_LINK$'); ?></a>
        </p>
        <h3><?= \Yii::t('UI10.1', '$QUESTION_OFFER_TITLE$') ?></h3>

        <div class="form-group field-<?= Html::getInputId($model, 'category') ?>" style="position: relative;">
            <?= Html::activeLabel($model, 'category', [
                'label' => \Yii::t('UI10.1', '$PRODUCT_CATEGORY_LABEL$'),
            ]) ?>

            <?= \yii\widgets\Breadcrumbs::widget([
                'links'    => $categoryBreadCrumbs,
                'homeLink' => false,
            ]) ?>
            <?= Html::error($model, 'category', ['class' => 'help-block']) ?>

            <div id="category-list" class="hidden">
                <ul class="list-unstyled"></ul>
            </div>

            <a id="change_category-btn" class="btn"><?= \Yii::t('UI10.1', '$CHANGE_CATEGORY_BTN$') ?></a>
        </div>

        <div class="formAbout styleInput">
            <?= $form->field($model, 'name')->textInput([
                'placeholder'    => \Yii::t('UI10.1.CATEGORY_PLACEHOLDERS',
                    ArrayHelper::getValue($placeholders, 'name_placeholder', '')),
                'data-toggle'    => 'popover',
                'data-trigger'   => 'focus',
                'data-placement' => 'right',
                'data-content'   => \Yii::t('UI10.1.CATEGORY_PLACEHOLDERS',
                    ArrayHelper::getValue($placeholders, 'name_popover', '')),
                'id'             => 'nameProd',
            ])->label(\Yii::t('UI10.1', '$PRODUCT_NAME_FIELD$')) ?>

            <?= $form->field($model, 'description')->textarea([
                'id'             => 'description',
                'placeholder'    => \Yii::t('UI10.1.CATEGORY_PLACEHOLDERS',
                    ArrayHelper::getValue($placeholders, 'description_placeholder', '')),
                'data-toggle'    => 'popover',
                'data-trigger'   => 'focus',
                'data-placement' => 'right',
                'data-content'   => \Yii::t('UI10.1.CATEGORY_PLACEHOLDERS',
                    ArrayHelper::getValue($placeholders, 'description_popover', '')),
            ])->label(\Yii::t('UI10.1', '$PRODUCT_DESCRIPTION_FIELD$')) ?>

        </div>
    </div>

<?php if (count($attrs)): ?>
    <div class="col-sm-12 specification styleText">
        <h3><?= \Yii::t('UI10.1', '$SPECIFYING_ATTRIBUTES_LABEL$') ?></h3>
        <p class="text-left"><?= \Yii::t('UI10.1', '$TEXT_BEFORE_ATTRIBUTES$') ?></p>

        <div class="row styleInput" id="list-attrs">

            <?php foreach ($attrs as $item): ?>
                <div class="col-sm-4">
                    <?php foreach ($item as $attr): ?>

                        <?= $form->field($modelProduct, $attr->name, [
                            'class' => '\mirocow\eav\widgets\ActiveField',
                        ])->eavWriteInput(); ?>

                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>

            <div class="col-sm-4 hidden">
                <span class="labelDroppMenu">Производитель:</span>
                <div class="dropdown">
                    <button class="btn dropdown-toggle"
                            type="button"
                            id="dropdownMenu1"
                            data-toggle="dropdown"
                            aria-expanded="true">
                        Спадний список
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                        <li role="presentation"><a role="menuitem"
                                    tabindex="-1"
                                    href="#">Дія</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Інша
                                дія</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Щось
                                ще тут</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Відокремлений
                                лінк</a></li>
                    </ul>
                </div>


                <span class="labelDroppMenu">Количество струн:</span>
                <div class="dropdown">
                    <button class="btn  dropdown-toggle"
                            type="button"
                            id="dropdownMenu1"
                            data-toggle="dropdown"
                            aria-expanded="true">
                        Спадний список
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                        <li role="presentation"><a role="menuitem"
                                    tabindex="-1"
                                    href="#">Дія</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Інша
                                дія</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Щось
                                ще тут</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Відокремлений
                                лінк</a></li>
                    </ul>
                </div>


                <span class="labelDroppMenu Wi">Wi-Fi роутер:</span><br>
                <input type="checkbox" id="3G" class="redCheckBox">
                <label for="3G" class="blackLabel ">3G</label><br>

                <input type="checkbox" id="4G" class="redCheckBox">
                <label for="4G" class="blackLabel ">4G</label>
            </div>
            <div class="col-sm-4 paddBlock hidden">
                <span class="labelDroppMenu">Схема звукоснимателей:</span>
                <div class="dropdown">
                    <button class="btn dropdown-toggle"
                            type="button"
                            id="dropdownMenu1"
                            data-toggle="dropdown"
                            aria-expanded="true">
                        Спадний список
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                        <li role="presentation"><a role="menuitem"
                                    tabindex="-1"
                                    href="#">Дія</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Інша
                                дія</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Щось
                                ще тут</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Відокремлений
                                лінк</a></li>
                    </ul>
                </div>


                <span class="labelDroppMenu">Тип бриджа:</span>
                <div class="dropdown">
                    <button class="btn  dropdown-toggle"
                            type="button"
                            id="dropdownMenu1"
                            data-toggle="dropdown"
                            aria-expanded="true">
                        Спадний список
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                        <li role="presentation"><a role="menuitem"
                                    tabindex="-1"
                                    href="#">Дія</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Інша
                                дія</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Щось
                                ще тут</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Відокремлений
                                лінк</a></li>
                    </ul>
                </div>


                <span class="labelDroppMenu">Утюг:</span><br>
                <input type="radio" id="yes">
                <label for="yes" class="blackLabel">Да</label><br>

                <input type="radio" id="no">
                <label for="no" class="blackLabel">Нет</label>
            </div>
        </div>

        <?php if ($excessAttrs > 0): ?>
            <a href="#" class="btn btn-block" id="openmore-attrs"><?= \Yii::t('UI10.1', '$ATTRIBUTES_OPEN_MORE_LABEL$',
                    ['c' => $excessAttrs]) ?></a>
        <?php endif; ?>
    </div>
<?php endif; ?>

<?= $this->render('_progress-filled', [
    'submit' => !$modelProduct->status ? 1 : 3,
]) ?>


<?php
$categoriesJs = \yii\helpers\Json::encode($categories);

$this->registerJs(<<<JS
var ca = 0;
jQuery("#list-attrs").children().not(".hidden").each(function(index, element){
    var ea = $(element).children(".form-group").slice({$maxVA});
    ea.addClass("hidden");
    
    ca += ea.length;
});

if (ca){
    jQuery("#openmore-attrs").on("click", shM);
}else{
    jQuery("#openmore-attrs").remove();
}

function shM(event){
    var btn = jQuery(event.target);
    btn.off("click", shM);
    
    event.preventDefault();
        
    btn.remove();
    jQuery("#list-attrs").children().not(".hidden").each(function(index, element){
        $(element).children(".form-group").removeClass("hidden");
    });
}

var list = $("#category-list");
var bread = list.prev(".breadcrumb");
var itemLevel = 0;
jQuery("#change_category-btn").on("click", function(event) {
   event.preventDefault();
   
   if (confirm("Изменение категории приведёт к удалению части данных.")) {
       var ul = list.children("ul");
       itemLevel = 0;
       
       list.removeClass("hidden");
       ul.empty();
       
       var currentSelect = bread.children("li").eq(itemLevel).text();
       $.each(getListCategory([0, null]), function(key, val) {
           var item = '<li data-id="' + val.id + '"' + ((currentSelect === val.name) ? ' class="active"' : "") + '><a>' + val.name + '</a></li>';
          ul.append(item);
       });   
   }
});
jQuery(document).on("mousedown", function(event) {
    if (!jQuery(event.target).closest("#category-list").length){
        list.addClass("hidden");
    }
});
list.on("click", "li", function(event) {
    var ul = list.children("ul");
    var id = jQuery(event.currentTarget).data("id");
    
    var node = getListCategory([id]);
    if (!_.size(node)) {
        // это конечная категория, надо обновить товар
        var form = jQuery("#change_category-form");
        form.find("#productform-category").val(id);
        form.trigger("submit");
    }
    ul.empty();
    itemLevel++;
    
     var currentSelect = bread.children("li").eq(itemLevel).text();
    $.each(node, function(key, val){
        var item = '<li data-id="' + val.id + '"' + ((currentSelect === val.name) ? ' class="active"' : "") + '><a>' + val.name + '</a></li>';
          ul.append(item);
    });
});

function getListCategory(p){
    return _.filter($categoriesJs, function(v, k) {
        return jQuery.inArray(v.parent_id, p) >= 0;
    });
}
JS
);

$this->registerCss(<<<CSS
#category-list {
    position: absolute;
    z-index: 3;
    background-color: #fff;
    box-shadow: 0 3px 9px rgba(69, 69, 69, .3);
}
#category-list li.active > a {
    color: #fff !important;
    background-color: #e35f46;
}
CSS
);

