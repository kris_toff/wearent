<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 16.04.2017
 * Time: 0:18
 */

use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use yii\helpers\HtmlPurifier;
use common\models\Currency;
use common\models\Product;
use frontend\models\ProductCurrency;
use yii\helpers\Json;

/** @var \yii\web\View $this */
/** @var \common\models\Product $model */
/** @var string $owner_avatar адрес аватара владельца товара */
/** @var \common\models\ProductMedia[] $media */
/** @var array $prices */
/** @var boolean $goodUser если пользователь авторизован и верифицирован - true, иначе false */
/** @var array $related */
/** @var \frontend\models\OrderForm $orderModel */

\frontend\assets\FrontendAsset::register($this);
\yii\bootstrap\BootstrapPluginAsset::register($this);
\frontend\assets\SliderAsset::register($this);
\frontend\assets\ReadmoreAsset::register($this);
\frontend\assets\CheckboxAsset::register($this);
\frontend\assets\yyAsset::register($this);
\frontend\assets\ProductAsset::register($this);
\frontend\assets\RateAsset::register($this);
\frontend\assets\DateRangePickerAsset::register($this);
\kartik\growl\GrowlAsset::register($this);


$a = array_filter([$model->seo_title, $model->seo_h1, $model->name], function ($v) {
    return !empty($v);
});
$this->title = Html::encode(array_shift($a));

$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->seo_description,
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->seo_keywords,
]);
//print_r($media);exit;
$mediaList = array_filter(ArrayHelper::map($media, 'id', 'link'), function ($v) {
    $l = substr($v, strrpos($v, '.') + 1);

    return in_array($l, ['png', 'jpg', 'gif'], true);
});
$firstPhoto = ArrayHelper::getValue($mediaList, $model->media_default);
if (is_null($firstPhoto)) {
    if (count($mediaList)) {
        $firstPhoto = $mediaList[array_rand($mediaList)];
    } else {
        $firstPhoto = '/img/category/imgCategory/defolt.svg';
    }
}
//print_r($mediaList);exit;
//print 'img:';var_dump($firstPhoto);exit;

$a = [

    1 => [
        'id' => 1,
        'name' => 'Музыка',
    ],

    3 => [
        'id' => 3,
        'name' => 'Спорт, Отдых',
        'node' => [
            101 => [
                'id' => 101,
                'name' => 'Велосипеды, гироскутеры, ролики, скейты',
            ],
            1011 => [
                'id' => 3302,
                'name' => 'Лыжи, коньки, санки',
            ],
            102 => [
                'id' => 3302,
                'name' => 'Пикник, кемпинг, пляж',
            ],
            103 => [
                'id' => 103,
                'name' => 'Туризм, путешествия',
                'node' => [
                    3320 => [
                        'id' => 3332,
                        'name' => 'Велосипеды, гироскутеры, ролики, скейты',
                    ],
                    3321 => [
                        'id' => 3333,
                        'name' => 'Лыжи, коньки, санки',
                    ],
                    3322 => [
                        'id' => 3334,
                        'name' => 'Пикник, кемпинг, пляж',
                    ],
                    3323 => [
                        'id' => 3335,
                        'name' => 'Туризм, путешествия',
                    ],
                    3324 => [
                        'id' => 3336,
                        'name' => 'Спортивные игры',
                    ],
                    3325 => [
                        'id' => 3337,
                        'name' => 'Дайвинг, водный спорт',
                    ],
                    3326 => [
                        'id' => 3338,
                        'name' => 'Услуги инструкторов',
                    ],
                    3327 => [
                        'id' => 3339,
                        'name' => 'Аренда спортивных помещений',
                    ],
                    3328 => [
                        'id' => 3340,
                        'name' => 'Картинг',
                    ],
                    3329 => [
                        'id' => 3341,
                        'name' => 'Тренажеры, фитнес, единоборства',
                    ],
                    3330 => [
                        'id' => 3342,
                        'name' => 'Пневматика и снаряжение',
                    ],

                ],
            ],
            104 => [
                'id' => 3304,
                'name' => 'Спортивные игры',
            ],
            105 => [
                'id' => 3305,
                'name' => 'Дайвинг, водный спорт',
            ],
            106 => [
                'id' => 3306,
                'name' => 'Услуги инструкторов',
            ],
            107 => [
                'id' => 3307,
                'name' => 'Аренда спортивных помещений',
            ],
            108 => [
                'id' => 3308,
                'name' => 'Картинг',
            ],
            109 => [
                'id' => 3309,
                'name' => 'Тренажеры, фитнес, единоборства',
            ],
            1010 => [
                'id' => 3310,
                'name' => 'Пневматика и снаряжение',
            ],

        ],
    ],

    4 => [
        'id' => 331,
        'name' => 'Фото, Видео',
    ],

    5 => [
        'id' => 332,
        'name' => 'Праздники и события',
    ],

    6 => [
        'id' => 333,
        'name' => 'Инструменты и спец. техника',
    ],

    2 => [
        'id' => 334,
        'name' => 'Детские товары',
    ],

    7 => [
        'id' => 334,
        'name' => 'Авто Мото',
    ],

    8 => [
        'id' => 335,
        'name' => 'Красота и здоровье',
    ],

    9 => [
        'id' => 336,
        'name' => 'Бизнес',
    ],

    10 => [
        'id' => 337,
        'name' => 'Одежда, обувь, акссесуары',
    ],

    11 => [
        'id' => 337,
        'name' => 'Бытовая техника',
    ],

    12 => [
        'id' => 337,
        'name' => 'Все для дома',
    ],

    13 => [
        'id' => 337,
        'name' => 'Дача, сад',
    ],


];

$when = [
    1 => [
        'id' => 1,
        'name' => 'Россия, Москва',
    ],

    2 => [
        'id' => 2,
        'name' => 'Московская область, Мытищи улица Новосельева',
    ],
    3 => [
        'id' => 3,
        'name' => 'Россия, Москва',
    ],
    4 => [
        'id' => 4,
        'name' => 'Московская область, Мытищи улица Новосельева',
    ],
    5 => [
        'id' => 5,
        'name' => 'Россия, Москва',
    ],
    6 => [
        'id' => 6,
        'name' => 'Московская область, Мытищи улица Новосельева',
    ],
];

// атрибуты
$attrs = $model->getEavAttributes()->all();
if (!empty($attrs)) {
    $attrs = array_chunk($attrs, ceil(count($attrs) / 2));
}
$attrs = array_pad($attrs, 2, []);

?>

<?= Html::activeHiddenInput($model, 'id', ['desabled' => true, 'readonly' => true]) ?>

    <div class="row wrapp contentBody">
        <div class="container-fluid" ng-controller="ProductController as PC">
            <!--            <div class="row hidden-sm hidden-md hidden-lg prevPage">-->
            <!--                <div class="col-xs-12 text-left">-->
            <!--                    <a href="#">← Предыдущая страница</a>-->
            <!--                </div>-->
            <!--            </div>-->

            <div class="row imgProduct">
                <div class="windowProfil hidden-xs">
                    <div class="topTitleWindow">
                        <?= Html::img($owner_avatar, ['class' => 'imgProfil']) ?>

                        <span class="inputFoto online"></span>
                        <div class="dropdown" id="iReadAll">
                            <a class="btn btn-default" href="#manInf" id="dropdownMenu1" aria-expanded="true">
                                <?= \Yii::t('UI9.1', '$FLOAT_BOX_ABOUT_OWNER$') ?>
                                <span>
                                    <svg class="" width="10px" height="6px" viewBox="0 0 10 6" version="1.1">

    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"
       stroke-linejoin="round">
        <g id="rotateBtn" transform="translate(-365.000000, -1307.000000)" stroke-width="2">
            <g id="DESCRIBTION" transform="translate(210.000000, 624.000000)">
                <g id="about" transform="translate(0.000000, 315.000000)">
                    <g id="Group-38" transform="translate(22.000000, 359.000000)">
                        <g id="Group-6">
                            <polyline id="Path-3-Copy"
                                      transform="translate(137.757144, 11.935000) scale(-1, 1) rotate(-270.000000) translate(-137.757144, -11.935000) "
                                      points="135.822144 15.6921435 139.692143 11.8221436 136.047856 8.17785643"></polyline>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                </span>
                            </a>
                        </div>
                    </div>

                    <div class="confirmed">
                        <ul class="list-unstyled">
                            <li class="frequency"><?= \Yii::t('UI9.1', '$FLOAT_BOX_CONFIRMED_PERCENT$') ?>:
                                <span>?%</span></li>
                            <li class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="96"
                                     aria-valuemin="0" aria-valuemax="100" style="width: 96%;">
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <?php if (count($media) > 1): ?>
                    <button type="button" class="btn showAll" data-toggle="modal" data-target="#myModal">
                        <?= \Yii::t('UI9.1', '$SEE_ALL_PHOTO_BTN$', ['n' => count($media)]) ?>
                    </button>
                <?php endif; ?>
                <h4 class="titleSidebar text-center hidden-sm hidden-md hidden-lg">
                    <?= ProductCurrency::formatPrice($model->price, $model->currency) ?>
                    /
                    <?= \Yii::t('UI9.1', '$PAYMENT_PERIOD_' . strtoupper($model->leasing) . '$'); ?>
                    <?= Html::tag('span', '', [
                        'class' => 'popPrise',
                        'data' => [
                            'container' => 'body',
                            'toggle' => 'tooltip',
                            'placement' => 'top',
                        ],
                        'title' => \Yii::t('UI9.1', '$SIGN_INDICATOR_FAST_RENT_HINT_HOVER$'),
                    ]) ?>
                </h4>

                <!-- фото на фоне -->
                <?php
                $options = ['class' => 'img-responsive imgWrap'];
                if (count($media)) {
                    $options['data'] = ['toggle' => 'modal', 'target' => '#myModal'];
                }
                if (!substr_compare($firstPhoto, 'svg', -3)) {
                    Html::addCssClass($options, 'default');
                }
                ?>

                <?= Html::img($firstPhoto, $options); ?>
            </div>

            <div class="hidden-sm hidden-md hidden-lg mobileBtnRent"
                 ng-class="{'hidden-xs': (isOpenModal && !PC.openMobileIntervalPanel)}">
                <button type="button" class="btn btn-block green" ng-click="openMobileModals()">
                    <?= \Yii::t('UI9.1', '$BOOKING_OPEN_MODAL_BTN_LABEL$') ?>
                </button>
            </div>

            <div class="row contentProduct">
                <div class="container">
                    <div class="row">
                        <div class="leftContent">
                            <div class="aboutProduct">
                                <h4 class="text-left titlePageProduct"><?= Html::encode($model->name) ?></h4>

                                <div class="blockLink hidden-sm hidden-md hidden-lg">
                                    <a href="#totalInf">Общее</a>
                                    <a href="#aboutProduct">О товаре</a>
                                    <a href="#ratingProduct">Рейтинг</a>
                                    <a href="#revievsProduct">Отзывы</a>
                                    <a href="#manInf">О хозяине</a>
                                </div>

                                <div class="blockForSmallScreen hidden-sm hidden-md hidden-lg">
                                    <div class="windowProfil">
                                        <div class="topTitleWindow">
                                            <?= Html::img($owner_avatar, ['class' => 'imgProfil']) ?>
                                            <!--                                            <img class="imgProfil" src="/img/product/fotoFace.jpg" alt="">-->
                                            <!--                                        <span class="greenInputFoto"></span>-->
                                            <div class="dropdown" id="iReadAll">
                                                <a class="btn btn-default"
                                                   href="#manInf"
                                                   id="dropdownMenu1"
                                                   aria-expanded="true">
                                                    <?= \Yii::t('UI9.1', '$FLOAT_BOX_ABOUT_OWNER$') ?>
                                                    <span>
                                    <svg class="rotateBtn" width="10px" height="6px" viewBox="0 0 10 6" version="1.1">

    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"
       stroke-linejoin="round">
        <g id="#rotateBtn" transform="translate(-365.000000, -1307.000000)" stroke-width="2">
            <g id="DESCRIBTION" transform="translate(210.000000, 624.000000)">
                <g id="about" transform="translate(0.000000, 315.000000)">
                    <g id="Group-38" transform="translate(22.000000, 359.000000)">
                        <g id="Group-6">
                            <polyline id="Path-3-Copy"
                                      transform="translate(137.757144, 11.935000) scale(-1, 1) rotate(-270.000000) translate(-137.757144, -11.935000) "
                                      points="135.822144 15.6921435 139.692143 11.8221436 136.047856 8.17785643"></polyline>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                </span>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="confirmed">
                                            <ul class="list-unstyled">
                                                <li class="frequency"><?= \Yii::t('UI9.1',
                                                        '$FLOAT_BOX_CONFIRMED_PERCENT$') ?>:
                                                    <span>?%</span>
                                                </li>
                                                <li class="progress">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="96"
                                                         aria-valuemin="0" aria-valuemax="100" style="width: 96%;">
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="rating">
                                        <span class="nameRating"><?= \Yii::t('UI9.1',
                                                '$PRODUCT_RATING_STARS_LABEL$') ?></span><br>
                                        <span class="numberRating text-right">?,?</span>
                                        <span class="rateyoProd"></span>
                                    </div>
                                </div>



                                <div class="informationProduct">
                                    <div class="row">
                                        <div class="leftInformation col-xs-12 col-sm-8" id="totalInf">
                                            <!--                                            <h4 class="text-left hidden-sm hidden-md hidden-lg titlePageProductSmall"-->
                                            <!--                                                    id="product_name">-->
                                            <? //= Html::encode($model->name) ?><!--</h4>-->

                                            <div class="row">
                                                <?php foreach ($attrs as $item): ?>
                                                    <div class="col-xs-6">
                                                        <?php foreach ($item as $attr): ?>
                                                            <div>
                                                                <div class="text-muted">
                                                                    <?= $attr->label; ?>:
                                                                </div>
                                                                <div class="text">
                                                                    <?php
                                                                    $handler = ArrayHelper::getValue($model,
                                                                        [$attr->name, 'handlers', $attr->name]);

                                                                    if ($handler
                                                                        instanceof
                                                                        \mirocow\eav\widgets\CheckBoxList) {
                                                                        print 'checkbox';

                                                                        $options = array_intersect_key($handler->attributeModel->getEavOptions()
                                                                            ->asArray()
                                                                            ->indexBy('id')
                                                                            ->all(),
                                                                            array_flip($model[$attr->name]['value']));

                                                                        foreach ($options as $option) {
                                                                            print '<div>' . $option['value'] . '</div>';
                                                                        }
                                                                    } elseif ($handler
                                                                        instanceof
                                                                        \mirocow\eav\widgets\DropDownList) {
                                                                        print 'dropdown';
                                                                        $options = $handler->attributeModel->getEavOptions()
                                                                            ->asArray()
                                                                            ->indexBy('id')
                                                                            ->all();

                                                                        print '<div>' . ArrayHelper::getValue($options,
                                                                                [
                                                                                    $model[$attr->name] ['value'],
                                                                                    'value',
                                                                                ], '') . '</div>';
                                                                    } else {
                                                                        print get_class($handler)
                                                                            . $model[$attr->name]
                                                                            . $model->c23->value;
                                                                    }
                                                                    ?>
                                                                    <!--                                                                    --><? //= $model[ $attr->name ]; ?>
                                                                </div>
                                                            </div>

                                                        <?php endforeach; ?>
                                                    </div>
                                                    <!--                                                    --><? //= trim($attr->label )?>
                                                    <!--                                                    --><?php //print_r($attr->name )  ?>
                                                    <!--                                                    --><?php //print_r($model[ $attr->name ]['value']);
//                                                    print
//                                                        ' -'
//                                                        . gettype($model[ $attr->name ]['value'])
//                                                        . '-' ?>
                                                <?php endforeach; ?>
                                            </div>
                                            <ul class="list-unstyled list hidden">
                                                <li class="inputList">Отдельный вход со стороны мясокомбината</li>
                                                <li class="inputList">Бабушка вахтерша</li>

                                                <li class="inputList">При входе не надо протирать ноги</li>
                                                <li class="inputList">И мыть тоже не надо</li>
                                                <li class="inputList">Можно приводить друзей</li>
                                                <li class="inputList">Отдельный вход со стороны мясокомбината</li>
                                                <li class="inputList">Бабушка вахтерша</li>

                                                <li class="inputList">При входе не надо протирать ноги</li>
                                                <li class="inputList">И мыть тоже не надо</li>
                                                <li class="inputList">Можно приводить друзей</li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-4 rating hidden-xs">
                                            <span class="nameRating"><?= \Yii::t('UI9.1',
                                                    '$PRODUCT_RATING_STARS_LABEL$') ?></span><br>
                                            <span class="numberRating text-right">?,?</span>
                                            <span class="rateyoProd"></span>
                                        </div>
                                        <div class="col-sm-12 col-xs-12 aboutProduct2" id="aboutProduct">
                                            <?php if (!empty($model->description)): ?>
                                                <h4 class="text-left"><?= \Yii::t('UI9.1',
                                                        '$ABOUT_PRODUCT_DESCRIPTION_TITLE$') ?></h4>
                                                <div class="blockTexxt readmoreProduct">
                                                    <?= HtmlPurifier::process($model->description) ?>
                                                </div>
                                            <?php endif; ?>

                                            <span class="borderAboutProduct"></span>
                                            <div class="row bottomBlock">
                                                <div class="col-sm-6 col-xs-12 minRent">
                                                    <?= \Yii::t('UI9.1', '$MIN_RENT_TIME_LABEL$') ?>:
                                                    <strong>
                                                        <?= $model->min_period . ' ' . \Yii::t('UI9.1',
                                                            '$MIN_RENT_TIME_VAL_'
                                                            . mb_strtoupper($model->leasing ?: 'none', 'UTF-8')
                                                            . '$', ['nd' => $model->min_period]) ?>
                                                    </strong>
                                                </div>
                                                <div class="col-sm-6 col-xs-12 datePickerProduct">
                                                    <a href="#" class="btn btn-block freeDate">
                                                        <svg class="imgButtDate" width="18px" height="20px"
                                                             viewBox="0 0 18 20" version="1.1"
                                                             xmlns="http://www.w3.org/2000/svg">


                                                            <path d="M17.2,1.9 L14.2,1.9 L14.2,0.6 C14.2,0.3 14,0.1 13.7,0.1 C13.4,0.1 13.2,0.3 13.2,0.6 L13.2,1.9 L4.6,1.9 L4.6,0.6 C4.6,0.3 4.4,0.1 4.1,0.1 C3.8,0.1 3.6,0.3 3.6,0.6 L3.6,1.9 L0.6,1.9 C0.3,1.9 0.1,2.1 0.1,2.4 L0.1,19.1 C0.1,19.4 0.3,19.6 0.6,19.6 L17.3,19.6 C17.6,19.6 17.8,19.4 17.8,19.1 L17.8,2.4 C17.7,2.2 17.5,1.9 17.2,1.9 Z M3.6,2.9 L3.6,4.2 C3.6,4.5 3.8,4.7 4.1,4.7 C4.4,4.7 4.6,4.5 4.6,4.2 L4.6,2.9 L13.2,2.9 L13.2,4.2 C13.2,4.5 13.4,4.7 13.7,4.7 C14,4.7 14.2,4.5 14.2,4.2 L14.2,2.9 L16.7,2.9 L16.7,7.1 L1,7.1 L1,2.9 L3.6,2.9 Z M1,18.6 L1,8.1 L16.7,8.1 L16.7,18.6 L1,18.6 Z"
                                                                  id="Shape"></path>
                                                            <rect id="Rectangle-path" x="3.2" y="10.1" width="1.9"
                                                                  height="1.9"></rect>
                                                            <rect id="Rectangle-path" x="12.7" y="10.1" width="1.9"
                                                                  height="1.9"></rect>
                                                            <rect id="Rectangle-path" x="8" y="10.1" width="1.9"
                                                                  height="1.9"></rect>
                                                            <rect id="Rectangle-path" x="3.2" y="13.9" width="1.9"
                                                                  height="1.9"></rect>
                                                            <rect id="Rectangle-path" x="8" y="13.9" width="1.9"
                                                                  height="1.9"></rect>

                                                        </svg>
                                                        <?= \Yii::t('UI9.1', '$PRODUCT_FREE_DAYS_BTN$') ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="ratingProductBlock" id="ratingProduct">
                                    <div class="row">
                                        <h4 class="nameBlock"><?= \Yii::t('UI9.1', '$PRODUCT_RATE_TITLE$') ?></h4>
                                        <div class="col-sm-8 col-xs-12 ratingBlockLeft">
                                            <table class="table-responsive">
                                                <tr class="
provider">
                                                    <td class="fitrsTd"><?= \Yii::t('UI9.1',
                                                            '$PRODUCT_RATE_ITEM_PROVIDER_LABEL$') ?>:
                                                    </td>
                                                    <td class="twoTd">?</td>
                                                    <td>
                                                        <div class="rateyo"></div>
                                                    </td>
                                                    <td class="lastTd">
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="60"
                                                                 aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 100%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="product">
                                                    <td class="fitrsTd"><?= \Yii::t('UI9.1',
                                                            '$PRODUCT_RATE_ITEM_QUALITY_LABEL$') ?>:
                                                    </td>
                                                    <td class="twoTd">?</td>
                                                    <td>
                                                        <div class="rateyo"></div>
                                                    </td>
                                                    <td class="lastTd">
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="60"
                                                                 aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 80%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="description">
                                                    <td class="fitrsTd"><?= \Yii::t('UI9.1',
                                                            '$PRODUCT_RATE_ITEM_ACCURACY_LABEL$') ?>:
                                                    </td>
                                                    <td class="twoTd">?</td>
                                                    <td>
                                                        <div class="rateyo"></div>
                                                    </td>
                                                    <td class="lastTd">
                                                        <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                 aria-valuenow="60"
                                                                 aria-valuemin="0" aria-valuemax="100"
                                                                 style="width: 80%;">
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>

                                        </div>
                                        <div class="col-sm-4 hidden-xs text-right ratingBlockRight">
                                            <span class="nameRating"><?= \Yii::t('UI9.1',
                                                    '$PRODUCT_RATING_STARS_LABEL$') ?></span><br>
                                            <span class="numberRating">?,?</span>
                                            <span class="rateyoProd"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="reviews" id="revievsProduct">
                                    <?php if (count([1])): ?>
                                        <!-- посты присутствуют -->
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12 titleReviews">
                                                <h4 class="title"><?= \Yii::t('UI9.1', '$PRODUCT_REVIEWS_TITLE$') ?>
                                                    <span>(??)</span>
                                                </h4>
                                            </div>

                                            <div class="col-sm-12">
                                                <?= $this->render('reviewBlock') ?>
                                            </div>
                                        </div>
                                        <a href="#" class="btn btn-block allComment text-center">Посмотреть
                                            еще
                                            10
                                            отзывов</a>
                                    <?php else: ?>
                                        <!-- постов к товару нет-->
                                        <img src="/img/category/imgCategory/defolt.svg" class="img-responsive"/>
                                        <div><strong><?= \Yii::t('UI9.1', '$REVIEWS_HAVE_NOONE_POST$') ?></strong></div>
                                    <?php endif; ?>
                                </div>

                                <div class="accountMan" id="manInf">
                                    <div class="row">
                                        <div class="col-sm-12 text-center">
                                            <?= \Yii::t('UI9.1', '$PRODUCT_ADDED_DATE$',
                                                ['date' => $model->created_at]) ?>
                                        </div>
                                        <div class="col-xs-12 relativeTitle">
                                            <h4 class="titleAccount">
                                                <svg width="28px" height="31px" viewBox="0 0 28 31">
                                                    <g id="Page-1"
                                                       stroke="none"
                                                       stroke-width="1"
                                                       fill="none"
                                                       fill-rule="evenodd">
                                                        <g id="UI9.-Карточка-товара---альтернатива"
                                                           transform="translate(-19.000000, -2978.000000)"
                                                           fill-rule="nonzero"
                                                           fill="#AFAFAF">
                                                            <g id="Group-13"
                                                               transform="translate(0.000000, 2963.000000)">
                                                                <g id="about-host---icon"
                                                                   transform="translate(19.000000, 15.000000)">
                                                                    <circle id="Oval"
                                                                            cx="18.8"
                                                                            cy="18.8"
                                                                            r="1.1"></circle>
                                                                    <path d="M27.3,30.1 L25.2,28 C26.5,26.6 27.2,24.7 27.2,22.7 C27.2,18.2 23.6,14.6 19.1,14.6 C14.6,14.6 11,18.2 11,22.7 C11,27.2 14.6,30.8 19.1,30.8 C21.2,30.8 23,30 24.4,28.8 L26.5,30.9 C26.6,31 26.7,31 26.9,31 C27.1,31 27.2,31 27.3,30.9 C27.5,30.6 27.5,30.3 27.3,30.1 Z M12,22.6 C12,18.7 15.2,15.5 19.1,15.5 C23,15.5 26.2,18.7 26.2,22.6 C26.2,26.5 23,29.7 19.1,29.7 C15.2,29.7 12,26.5 12,22.6 Z"
                                                                          id="Shape"></path>
                                                                    <path d="M20.4,25.8 L19.6,25.8 L19.6,22 C19.6,21.7 19.4,21.5 19.1,21.5 L17.8,21.5 C17.5,21.5 17.3,21.7 17.3,22 C17.3,22.3 17.5,22.5 17.8,22.5 L18.6,22.5 L18.6,25.8 L17.8,25.8 C17.5,25.8 17.3,26 17.3,26.3 C17.3,26.6 17.5,26.8 17.8,26.8 L20.4,26.8 C20.7,26.8 20.9,26.6 20.9,26.3 C20.9,26 20.7,25.8 20.4,25.8 Z"
                                                                          id="Shape"></path>
                                                                    <path d="M11,27.4 L2.1,27.4 L2.1,20.5 C2.1,16.6 4.5,14.4 9.3,13.9 C9.5,13.9 9.7,13.7 9.7,13.5 C9.7,13.3 9.6,13.1 9.5,13 C7.7,12 6.3,9.6 6.3,7.4 C6.3,4.6 8.6,1.2 11.5,1.2 C14.4,1.2 16.7,4.6 16.7,7.4 C16.7,9.6 15.4,12 13.5,13 C13.3,13.1 13.2,13.3 13.3,13.5 C13.3,13.7 13.5,13.9 13.7,13.9 C14.4,14 14.9,14.1 15.5,14.2 C15.8,14.3 16,14.1 16.1,13.8 C16.2,13.5 16,13.3 15.7,13.2 C15.5,13.2 15.3,13.1 15,13.1 C16.6,11.7 17.6,9.5 17.6,7.4 C17.6,4.1 14.9,0.2 11.4,0.2 C7.9,0.2 5.2,4.1 5.2,7.4 C5.2,9.5 6.3,11.7 7.8,13.1 C3.3,13.9 0.9,16.5 0.9,20.5 L0.9,27.9 C0.9,28.2 1.1,28.4 1.4,28.4 L10.8,28.4 C11.1,28.4 11.3,28.2 11.3,27.9 C11.3,27.6 11.3,27.4 11,27.4 Z"
                                                                          id="Shape"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <?= \Yii::t('UI9.1', '$ABOUT_OWNER_TITLE$') ?>
                                            </h4>
                                            <?= Html::a(\Yii::t('UI9.1', '$ABOUT_OWNER_LINK_PROFILE$'),
                                                ["/{$model->author->id}"],
                                                ['class' => 'btn btnXs red hidden-sm hidden-md hidden-lg']) ?>
                                        </div>

                                        <div class="col-sm-8 col-xs-12 infProfile">
                                            <div class="media">
                                                <div class="media-left">
                                                    <?= Html::a(Html::img($owner_avatar, ['class' => 'imgFotoProfile'])
                                                        . '<span class="inputFoto online"></span>',
                                                        ['/profile', 'id' => $model['author']['id']]) ?>
                                                </div>

                                                <div class="media-body">
                                                    <h4 class="media-heading">
                                                        <?= \Yii::t('UI9.1', '$ABOUT_OWNER_HELLO_PHRASE$',
                                                            ['name' => $model->author->userProfile->firstname]) ?>
                                                    </h4>
                                                    <p class="place">
                                                        <svg width="13px" height="18px" viewBox="0 0 13 18">
                                                            <g id="Page-1" stroke="none" stroke-width="1"
                                                               fill="none"
                                                               fill-rule="evenodd">
                                                                <g id="+UI5.-Квитанция-об-оплате"
                                                                   transform="translate(-973.000000, -382.000000)"
                                                                   fill-rule="nonzero" fill="#AFAFAF">
                                                                    <g id="Group-13"
                                                                       transform="translate(366.000000, 142.000000)">
                                                                        <g id="Group-11"
                                                                           transform="translate(20.000000, 208.000000)">
                                                                            <g id="Group"
                                                                               transform="translate(587.000000, 30.000000)">
                                                                                <g id="Group-16"
                                                                                   transform="translate(0.000000, 2.000000)">
                                                                                    <path d="M6.5,17.5 L6.1,17.1 C5.9,16.8 0.4,10.6 0.4,6.4 C0.4,3 3.1,0.3 6.5,0.3 C9.9,0.3 12.6,3 12.6,6.4 C12.6,10.6 7.1,16.8 6.9,17.1 L6.5,17.5 Z M6.5,1.3 C3.7,1.3 1.4,3.6 1.4,6.4 C1.4,9.7 5.2,14.5 6.5,16 C7.7,14.5 11.6,9.7 11.6,6.4 C11.6,3.6 9.3,1.3 6.5,1.3 Z"
                                                                                          id="Shape"></path>
                                                                                    <path d="M6.5,9.6 C4.7,9.6 3.2,8.1 3.2,6.3 C3.2,4.5 4.7,3 6.5,3 C8.3,3 9.8,4.5 9.8,6.3 C9.8,8.1 8.3,9.6 6.5,9.6 Z M6.5,4 C5.2,4 4.2,5 4.2,6.3 C4.2,7.6 5.2,8.6 6.5,8.6 C7.8,8.6 8.8,7.6 8.8,6.3 C8.8,5.1 7.8,4 6.5,4 Z"
                                                                                          id="Shape"></path>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                        <?php
                                                        $c = $model->author->userProfile->country;
                                                        if (!is_null($c)) {
                                                            print \Yii::t('UI14.3-countries',
                                                                '$COUNTRY_NAME_' . strtoupper($c->code) . '$');
                                                        }
                                                        ?>,
                                                        <?= $model->author->userProfile->city ?>
                                                    </p>
                                                    <p class="place">
                                                        <svg width="15px" height="17px" viewBox="0 0 15 17">
                                                            <g id="Page-1" stroke="none" stroke-width="1"
                                                               fill="none"
                                                               fill-rule="evenodd">
                                                                <g id="UI18.2.-Блог"
                                                                   transform="translate(-232.000000, -596.000000)"
                                                                   fill-rule="nonzero"
                                                                   fill="#AFAFAF">
                                                                    <g id="Group-10"
                                                                       transform="translate(210.000000, 520.000000)">
                                                                        <g id="Group"
                                                                           transform="translate(22.000000, 76.000000)">
                                                                            <path d="M14.1,1.8 L11,1.8 L11,1 C11,0.7 10.8,0.5 10.5,0.5 C10.2,0.5 10,0.7 10,1 L10,1.9 L5,1.9 L5,1 C5,0.7 4.8,0.5 4.5,0.5 C4.2,0.5 4,0.7 4,1 L4,1.9 L0.9,1.9 C0.6,1.9 0.4,2.1 0.4,2.4 L0.4,15.6 C0.4,15.9 0.6,16.1 0.9,16.1 L14.1,16.1 C14.4,16.1 14.6,15.9 14.6,15.6 L14.6,2.4 C14.6,2.1 14.4,1.8 14.1,1.8 Z M4,2.8 L4,3.7 C4,4 4.2,4.2 4.5,4.2 C4.8,4.2 5,4 5,3.7 L5,2.8 L10,2.8 L10,3.7 C10,4 10.2,4.2 10.5,4.2 C10.8,4.2 11,4 11,3.7 L11,2.8 L13.6,2.8 L13.6,6.2 L1.4,6.2 L1.4,2.8 L4,2.8 Z M1.4,15.1 L1.4,7.3 L13.6,7.3 L13.6,15.1 L1.4,15.1 Z"
                                                                                  id="Shape"></path>
                                                                            <path d="M8.8,9.5 L7.1,11.5 L6.2,10.6 C6,10.4 5.7,10.4 5.5,10.6 C5.3,10.8 5.3,11.1 5.5,11.3 L6.7,12.6 C6.8,12.7 6.9,12.8 7.1,12.8 C7.2,12.8 7.4,12.7 7.5,12.6 L9.6,10.2 C9.8,10 9.8,9.7 9.5,9.5 C9.3,9.3 9,9.3 8.8,9.5 Z"
                                                                                  id="Shape"></path>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                        <?= \Yii::t('UI9.1', '$ABOUT_OWNER_JOINED$',
                                                            ['date' => $model->author->created_at]) ?>
                                                    </p>
                                                    <?= Html::a(\Yii::t('UI9.1', '$ABOUT_OWNER_LINK_PROFILE$'),
                                                        ['/profile', 'id' => $model['author']['id']],
                                                        ['class' => 'btn red hidden-xs']) ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-4 col-xs-12 statistic">
                                            <div class="statisticsProfil">
                                                <ul class="list-unstyled">
                                                    <li class="frequency"><?= \Yii::t('UI9.1',
                                                            '$ABOUT_OWNER_FREQUENCY_OF_ANSWER_LABEL$'); ?>:
                                                        <span>?%</span></li>
                                                    <li class="progress">
                                                        <div class="progress-bar" role="progressbar"
                                                             aria-valuenow="60"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width: 96%;">
                                                            <span class="sr-only">60% Завершено</span>
                                                        </div>
                                                    </li>
                                                    <li class="time"><?= \Yii::t('UI9.1',
                                                            '$ABOUT_OWNER_RESPONSE_TIME_LABEL$') ?>:
                                                        <span>? часов</span></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <!--                                        <div class="col-xs-12 hidden-sm hidden-md hidden-lg">-->
                                        <!--                                            <a href="/profile" class="btn btn-block red">Посмотреть профиль</a>-->
                                        <!--                                        </div>-->

                                    </div>
                                    <div class="row">
                                        <div class="data">
                                            <div class=" col-md-3 col-xs-6">
                                                <div class="thumbnail">
                                                    <img src="/img/editProfile/awards/confirmed.svg"
                                                         class="text-center"
                                                         alt="...">
                                                    <div class="caption">
                                                        <h5 class="text-center">Подтвержден на 100%</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class=" col-md-3 col-xs-6">
                                                <div class="thumbnail">
                                                    <img src="/img/editProfile/awards/bestOwner.svg"
                                                         class="text-center"
                                                         alt="...">
                                                    <div class="caption">
                                                        <h5 class="text-center">Лучший хозяин</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix hidden-sm hidden-lg hidden-md"></div>
                                            <div class=" col-md-3 col-xs-6">
                                                <div class="thumbnail">
                                                    <img src="/img/editProfile/awards/activeMan.svg"
                                                         class="text-center"
                                                         alt="...">
                                                    <div class="caption">
                                                        <h5 class="text-center">Активный участник сообщества</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-6">
                                                <div class="thumbnail">
                                                    <img src="/img/editProfile/awards/awardForWearent.svg"
                                                         class="text-center"
                                                         alt="...">
                                                    <div class="caption ">
                                                        <h5 class="text-center">Награда от компании <span
                                                                    class="redText">Wearent</span>
                                                        </h5>
                                                    </div>
                                                </div>
                                            </div>

                                            <span class="borderAboutProduct"></span>
                                        </div>
                                        <div class="clearfix"></div>

                                        <?php if (!empty($model->author->userProfile->description)): ?>
                                            <div class="aboutMe col-sm-12">
                                                <h5 class="titleAboutMe"><?= \Yii::t('UI9.1',
                                                        '$ABOUT_OWNER_DESCRIPTION_TITLE$') ?></h5>
                                                <div class="blockaboutMe ">
                                                    <?= HtmlPurifier::process($model->author->userProfile->description) ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php if ($model->delivery_status): ?>
                            <div class="maps hidden-sm hidden-md hidden-lg">
                                <div class="addressDelivery text-center">
                                    <h4><?= \Yii::t('UI9.1', '$MAP_PANEL_TITLE$') ?></h4>
                                    <div id="search_box-mobile-container"></div>

                                    <div ng-show="isNewAddress" ng-cloak class="wrappButtonMaps">
                                        <p class="textRequest">Доставка по данному адресу доступна, оформить аренду?</p>
                                        <button type="button"
                                                class="btn cansel"
                                                ng-click="cancelLocation()"><?= \Yii::t('UI9.1',
                                                '$MAP_PANEL_CANCEL_BTN$') ?></button>
                                        <button type="button"
                                                class="btn send"
                                                ng-click="confirmLocation()"><?= \Yii::t('UI9.1',
                                                '$MAP_PANEL_OK_BTN$') ?></button>
                                    </div>
                                </div>

                                <!--                                <div id="map-mobile"></div>-->
                                <?= Html::beginTag('ui-gmap-google-map', [
                                    'center' => "{latitude: $model->lat, longitude: $model->lng}",
                                    'zoom' => 12,
                                    'pan' => 1,
                                    'controls' => '{}',
                                    'options' => 'config.maps',
                                ]); ?>
                                <ui-gmap-search-box template="config.searchbox.template"
                                                    events="config.searchbox.events"
                                                    options="config.searchbox.options"
                                                    parentdiv="'search_box-mobile-container'"></ui-gmap-search-box>

                                <?= Html::tag('ui-gmap-marker', '', [
                                    'idKey' => "'IamHereMarker'",
                                    'coords' => 'config.marker.coords',
                                    'options' => 'config.marker.options',
                                    'control' => 'markerControl',
                                ]) ?>
                                <?= Html::tag('ui-gmap-circle', '', [
                                    'center' => '_coordinatesDistributionPoint',
                                    'radius' => $model->delivery_radius,
                                    'stroke' => '{ color: \'#53c77d\', weight: 2, opacity: 0.9 }',
                                    'fill' => '{ color: \'#53c77d\', opacity: 0.3 }',
                                    'clickable' => 'false',
                                ]); ?>
                                <?= Html::tag('ui-gmap-circle', '', [
                                    'center' => '_coordinatesDistributionPoint',
                                    'radius' => 300,
                                    'stroke' => '{ color: \'#53c77d\', weight: 2, opacity: 0.9 }',
                                    'fill' => '{ color: \'#53c77d\', opacity: 0.45 }',
                                    'clickable' => 'false',
                                ]); ?>
                                <?= Html::endTag('ui-gmap-google-map'); ?>
                            </div>
                        <?php endif; ?>

                        <div class="sidebarProduct">
                            <div class="topBlock" ng-class="{'hidden-xs': !isOpenModal}">
                                <!-- Цена товара -->
                                <h4 class="titleSidebar text-center">
                                    <?= ProductCurrency::formatPrice($model->price, $model->currency) ?>
                                    /
                                    <?= \Yii::t('UI9.1', '$PAYMENT_PERIOD_' . strtoupper($model->leasing) . '$'); ?>

                                    <?php // 501 - мифическое значение рейтинга текущего пользователя, надо поменять на реальное значение, когда будет готово ?>
                                    <?php if ($model->autoorder == 1
                                        || $model->autoorder == 2
                                        && 501
                                        > $model->autoorder_rate
                                    ): ?>
                                        <?= Html::tag('span', '', [
                                            'class' => 'popPrise',
                                            'data' => [
                                                'container' => 'body',
                                                'toggle' => 'tooltip',
                                                'placement' => 'top',
                                            ],
                                            'title' => \Yii::t('UI9.1', '$SIGN_INDICATOR_FAST_RENT_HINT_HOVER$'),
                                            'ng-init' => 'isFastRent = true',
                                        ]) ?>
                                    <?php endif; ?>
                                </h4>

                                <?php Modal::begin([
                                    'id' => 'intervalModal',
                                    'toggleButton' => false,
                                    'header' => false,
                                    'closeButton' => false,
                                    'options' => [
                                        'class' => '',
                                        'ng-class' => '{inline: !PC.mobileWidth, fade: PC.mobileWidth}',
                                    ],
                                ]); ?>

                                <form method="post" class="sryleInput" name="bookingForm">
                                    <!-- форма бронирования -->
                                    <!-- поля с календарями -->
                                    <?= Html::tag('div', '', [
                                        'ng-init' => '_minTimeInterval = '
                                            . ($model->min_period ?: 1)
                                            . ';_maxTimeInterval = '
                                            . ($model->max_period ?: 0)
                                            . ';_maxperiod = '
                                            . ($model->beforehand_rent ?: 360)
                                            . ';_timeFrom = \'' . (($model->time_from ?: '') . '\'')
                                            . ';_timeTo = \'' . (($model->time_to ?: '') . '\'')
                                            . ';_lp = ' . ($model->lease_time ?: 0)
                                            . ';_lmp = ' . ($model->lease_max_time ?: 0),
                                    ]) ?>
                                    <div class="input-floating whBlock">
                                        <label class="labelWhat labelWhere" for="inputWhenRent"><?= \Yii::t('UI9.1',
                                                '$BOOKING_FORM_RENT_FROM_LABEL$') ?></label>
                                        <label class="labelWhat infocus" for="inputWhenRent"><?= \Yii::t('UI9.1',
                                                '$BOOKING_FORM_RENT_FROM_LABEL$') ?>:</label>
                                        <span class="datePickerProduct"><img src="/img/product/datePGrey.svg"></span>
                                        <input name="fieldWhere"
                                               type="text"
                                               id="inputWhenRent"
                                               class="dateOne1"
                                               ng-focus="openModal($event)"
                                               readonly="readonly"
                                               ng-model="rentFrom"
                                               uib-datepicker-popup="dd MMMM yyyy HH:mm"/>
                                    </div>

                                    <div class="input-floating whenBack whBlock">
                                        <label class="labelWhat labelWhere" for="inputWhenBack"><?= \Yii::t('UI9.1',
                                                '$BOOKING_FORM_RENT_TO_LABEL$') ?></label>
                                        <label class="labelWhat infocus" for="inputWhenBack"><?= \Yii::t('UI9.1',
                                                '$BOOKING_FORM_RENT_TO_LABEL$') ?>:</label>
                                        <span class="datePickerProduct"><img src="/img/product/datePGrey.svg"
                                                                             alt=""></span>
                                        <input name="fieldWhere"
                                               type="text"
                                               id="inputWhenBack"
                                               class="dateOne2"
                                               ng-focus="openModal($event)"
                                               readonly="readonly"
                                               ng-model="rentTo"
                                               uib-datepicker-popup="dd MMMM yyyy HH:mm"/>
                                    </div>
                                    <!-- /поля с календарями -->

                                    <div class="clearfix"></div>
                                    <!-- поля доставки -->
                                    <?php
                                    //print 'language:' .\Yii::$app->language . ';locale:'.\Yii::$app->formatter->locale;
                                    if ($model->delivery_status):
                                        print Html::beginTag('div',
                                            [
                                                'class' => 'addDetail',
                                                'ng-init' => 'isDelivery = '
                                                    . ($model->delivery_status ? 'true' : 'false')
                                                    . ";deliveryMaxRadius = {$model->delivery_radius};deliveryPrice = "
                                                    . ProductCurrency::getActualPrice($model->delivery_price,
                                                        $model->currency_id),
                                            ]) ?>

                                    <md-checkbox id="checkInf" class="checkInf redCheckBox" ng-model="useDelivery"><?= \Yii::t('UI9.1',
                                            '$BOOKING_FORM_DELIVERY_CHECKBOX$') ?></md-checkbox>

                                        <div class="leftDetail" hide>
                                            <span class="infCheck">
                                                <a href="#" type="button" data-toggle="popover" data-trigger="hover"
                                                   data-placement="top auto"
                                                   data-content="<?= \Yii::t('UI9.1',
                                                       '$BOOKING_FORM_DELIVERY_POPOVER$') ?>">
                                                    <svg width="17px" height="17px" viewBox="0 0 17 17">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g class="re" transform="translate(-1068.000000, -837.000000)" fill-rule="nonzero">
            <g id="FORM" transform="translate(903.000000, 624.000000)">
                <g id="Group-16" transform="translate(20.000000, 213.000000)">
                    <g id="Group-14">
                        <g id="Group-3" transform="translate(145.000000, 0.000000)">
                            <path d="M14.2,13.4 C15.5,12 16.2,10.1 16.2,8.1 C16.2,3.6 12.6,0 8.1,0 C3.6,0 0,3.6 0,8.1 C0,12.6 3.6,16.2 8.1,16.2 C10.2,16.2 12,15.4 13.4,14.2 L14.2,13.4 Z M1,8 C1,4.1 4.2,0.9 8.1,0.9 C12,0.9 15.2,4.1 15.2,8 C15.2,11.9 12,15.1 8.1,15.1 C4.2,15.1 1,12 1,8 Z"
                                  id="Shape"></path>
                            <path d="M9.5,11.2 L8.6,11.2 L8.6,7.4 C8.6,7.1 8.4,6.9 8.1,6.9 L6.8,6.9 C6.5,6.9 6.3,7.1 6.3,7.4 C6.3,7.7 6.5,7.9 6.8,7.9 L7.6,7.9 L7.6,11.2 L6.8,11.2 C6.5,11.2 6.3,11.4 6.3,11.7 C6.3,12 6.5,12.2 6.8,12.2 L9.4,12.2 C9.7,12.2 9.9,12 9.9,11.7 C9.9,11.4 9.7,11.2 9.5,11.2 Z"
                                  id="Shape"></path>
                            <circle id="Oval" cx="7.9" cy="4.4" r="1.1"></circle>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                            </a></span>

                                            <span class="textCheck">
                                                <svg width="11px" height="10px" viewBox="0 0 11 10">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Style_Kit" transform="translate(-1545.000000, -151.000000)" stroke-width="2" stroke="#4DC97B">
            <g id="Group-19-Copy-4" transform="translate(1277.000000, 112.000000)">
                <polyline id="Path-2" points="269 43.7031967 271.895392 46.958698 277.491129 40"></polyline>
            </g>
        </g>
    </g>
</svg><?= \Yii::t('UI9.1', '$BOOKING_FORM_DELIVERY_ON_TEXT$') ?></span>
                                        </div>
                                        <?= Html::endTag('div') ?>

                                        <div class="rentSidebar" ng-show="useDelivery">
                                            <div id="outerParentSearchbox"></div>

                                            <span class="greenCheck greenText" ng-show="deliveryStatus === 'ok'">
                                                <img src="/img/editProfile/chat/delivered.svg"/>
                                                <?= \Yii::t('UI9.1', '$BOOKING_FORM_ADDED_TO_PRICE_TEXT$', [
                                                    'price' => ProductCurrency::formatPrice($model->delivery_price,
                                                        $model->currency_id),
                                                ]) ?>
                                            </span>
                                            <span class="notDelivery redText" ng-show="deliveryStatus === 'wrong'">
<!--                                                <img src="/img/editProfile/chat/" />-->
                                                <?= \Yii::t('UI9.1', '$BOOKING_FORM_WRONG_DELIVERY_ADDRESS$') ?>
                                            </span>

                                            <div class="paddBlocks inFlex" hide>
                                                <span class="greeyText left">
                                                    <?= \Yii::t('UI9.1', '$BOOKING_FORM_DELIVERY_PRICE_LABEL$') ?>
                                                </span>
                                                <span class="boldText right"></span>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <!-- /поля доставки -->

                                    <?php
                                    $symb = Currency::getCurrentSymbol();
                                    ?>
                                    <?= Html::beginTag('div', [
                                        'class' => 'priceSidebar',
                                        'ng-init' => "currencySymbol = '{$symb}';_price = "
                                            . ProductCurrency::getActualPrice($model->price,
                                                $model->currency ?: \Yii::$app->params['currency']),
                                    ]) ?>
                                    <div class="inlinePrice paddBlocks inFlex">
                                        <span class="greeyText left">{{(_price|currency:currencySymbol) + ' х ' + period }} <?= \Yii::t('UI9.1',
                                                '$BOOKING_FORM_PLURAL_INTERVAL_'
                                                . strtoupper($model->leasing)
                                                . '_PERIOD$') ?></span>
                                        <span class="boldText right">{{_price*period|currency:currencySymbol}}</span>
                                    </div>

                                    <div class="serviceSidebar paddBlocks inFlex">
                                        <span class="greeyText left"><?= \Yii::t('UI9.1',
                                                '$BOOKING_FORM_CHARITY_LABEL$') ?></span>
                                        <span class="infService" data-toggle="popover"
                                              data-placement="top auto"
                                              data-trigger="hover"
                                              title="<?= \Yii::t('UI9.1', '$BOOKING_FORM_CHARITY_INFO_POPOVER$') ?>">
                                            <svg class="infServiseSVG" width="20px" height="20px" viewBox="0 0 20 20">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="infSVG" transform="translate(-232.000000, -261.000000)">
            <g id="Group-2" transform="translate(0.000000, 199.000000)">
                <g id="Group-24-Copy" transform="translate(233.000000, 63.000000)">
                    <path d="M8.8,0 C4,0 0,3.9 0,8.8 C0,13.7 3.9,17.6 8.8,17.6 C13.7,17.6 17.6,13.7 17.6,8.8 C17.6,3.9 13.6,0 8.8,0 L8.8,0 Z"
                          id="Shape" fill-rule="nonzero"></path>
                    <g id="Group-27" transform="translate(7.000000, 4.000000)">
                        <path d="M1.9,0 C2.5,0 2.9,0.5 2.9,1 C2.9,1.6 2.4,2 1.9,2 C1.3,2 0.9,1.5 0.9,1 C0.8,0.4 1.3,0 1.9,0 L1.9,0 Z"
                              id="Path"></path>
                        <path d="M3.4,8.9 L0.8,8.9 C0.4,8.9 0,8.6 0,8.1 C0,7.6 0.3,7.3 0.8,7.3 L1.4,7.3 L1.4,4.6 L0.8,4.6 C0.4,4.6 0,4.3 0,3.8 C0,3.3 0.3,3 0.8,3 L2.1,3 C2.5,3 2.9,3.3 2.9,3.8 L2.9,7.4 L3.5,7.4 C3.9,7.4 4.3,7.7 4.3,8.2 C4.3,8.7 3.8,8.9 3.4,8.9 L3.4,8.9 Z"
                              id="Path"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                        </span>
                                        <span class="boldText right">{{serviceCommission|currency:currencySymbol}}</span>
                                    </div>

                                    <?= Html::beginTag('div', [
                                        'class' => 'discount paddBlocks',
                                        'ng-init' => '_priceCondition = ' . Json::encode($model->price_conditions),
                                    ]) ?>
                                    <div class="topDiscount inFlex" ng-click="visibleDiscountBox = !visibleDiscountBox">
                                        <span class="yourDiscout greeyText">
                                            <?= \Yii::t('UI9.1', '$BOOKING_FORM_CURRENT_DISCOUNT_LABEL$') ?>
                                            <svg
                                                    class="greyBtnDown" width="10px" height="6px"
                                                    viewBox="0 0 10 6">
<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"
   stroke-linejoin="round">
    <g id="greyBtnDown" transform="translate(-360.000000, -3196.000000)" stroke-width="2">
        <g id="DESCRIBTION-Copy" transform="translate(211.000000, 2623.000000)">
            <g id="Group-5">
                <g id="Group-16" transform="translate(20.000000, 393.000000)">
                    <g id="Group-38" transform="translate(0.000000, 171.000000)">
                        <polyline id="Path-3-Copy"
                                  transform="translate(133.757144, 11.935000) scale(-1, 1) rotate(-270.000000) translate(-133.757144, -11.935000) "
                                  points="131.822144 15.6921435 135.692143 11.8221436 132.047856 8.17785643"></polyline>
                    </g>
                </g>
            </g>
        </g>
    </g>
</g>
</svg>
                                        </span>
                                        <span class="result boldText right">{{discount|currency:currencySymbol}}</span>
                                    </div>
                                    <div class="bottomDiscount inFlex" ng-show="visibleDiscountBox">
                                        <ul>
                                            <li>
                                                <md-icon md-svg-src="/img/editProfile/smallCheck.svg"
                                                "></md-icon>
                                                <span><?= \Yii::t('UI9.1', '$BOOKING_FORM_DISCOUNT_GOOD_USER_LABEL$')
                                                    . " {$model->discount_price}%" ?></span> -
                                                <?= Html::tag('span',
                                                    '{{(goodUserDiscount * (period || 1))|currency:currencySymbol}}', [
                                                        'ng-init' => '_discount_price = '
                                                            . ($model->discount_price ?: 0)
                                                            . ';_isGoodUser = '
                                                            . ($goodUser ? 1 : 0),
                                                    ]) ?>
                                                <span class="infRating"
                                                      data-toggle="popover"
                                                      data-placement="top auto"
                                                      data-trigger="hover"
                                                      data-content="<?= \Yii::t('UI9.1',
                                                          '$BOOKING_FORM_DISCOUNT_GOOD_INFO_POPOVER$') ?>">
                                            <svg class="infServiseSVG" width="20px" height="20px" viewBox="0 0 20 20">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="infSVG" transform="translate(-232.000000, -261.000000)">
            <g id="Group-2" transform="translate(0.000000, 199.000000)">
                <g id="Group-24-Copy" transform="translate(233.000000, 63.000000)">
                    <path d="M8.8,0 C4,0 0,3.9 0,8.8 C0,13.7 3.9,17.6 8.8,17.6 C13.7,17.6 17.6,13.7 17.6,8.8 C17.6,3.9 13.6,0 8.8,0 L8.8,0 Z"
                          id="Shape" fill-rule="nonzero"></path>
                    <g id="Group-27" transform="translate(7.000000, 4.000000)">
                        <path d="M1.9,0 C2.5,0 2.9,0.5 2.9,1 C2.9,1.6 2.4,2 1.9,2 C1.3,2 0.9,1.5 0.9,1 C0.8,0.4 1.3,0 1.9,0 L1.9,0 Z"
                              id="Path"></path>
                        <path d="M3.4,8.9 L0.8,8.9 C0.4,8.9 0,8.6 0,8.1 C0,7.6 0.3,7.3 0.8,7.3 L1.4,7.3 L1.4,4.6 L0.8,4.6 C0.4,4.6 0,4.3 0,3.8 C0,3.3 0.3,3 0.8,3 L2.1,3 C2.5,3 2.9,3.3 2.9,3.8 L2.9,7.4 L3.5,7.4 C3.9,7.4 4.3,7.7 4.3,8.2 C4.3,8.7 3.8,8.9 3.4,8.9 L3.4,8.9 Z"
                              id="Path"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                        </span>
                                            </li>

                                            <?php
                                            $periods = [1 => 'DAY', 2 => 'WEEK', 3 => 'MONTH', 4 => 'HOUR'];
                                            foreach ($model->price_raw_conditions as $item): ?>
                                                <?= Html::tag('li', Html::tag('md-icon', 'done', [
                                                        'ng-if' => 'amountHours > ' . ArrayHelper::getValue([
                                                                1 => 24,
                                                                2 => 168,
                                                                3 => 720,
                                                            ], $item['lease_time'], 1) * $item['val'],
                                                    ]) . \Yii::t('UI9.1',
                                                        "\$BOOKING_FORM_DISCOUNT_PERIOD_ITEM_{$periods[$item['lease_time']]}$",
                                                        ['amount' => $item['val']])
                                                    . ' '
                                                    . (!(int)$model->price ? 0 : round((1 - $item['price']
                                                            / $model->price) * 100, 2)) . '%') ?>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                    <?= Html::endTag('div') ?>

                                    <div class="allPrice inFlex">
                                        <span class="textPrice"><?= \Yii::t('UI9.1',
                                                '$BOOKING_FORM_TOTAL_PRICE_LABEL$') ?></span>
                                        <span class="numberPrice right">{{totalPrice|currency:currencySymbol}}</span>
                                    </div>

                                    <div class="pledge">
                                        <div class="topPledge inFlex"
                                             ng-click="visibleGuarantyBox = !visibleGuarantyBox">
                                            <span class="pledgeText greeyText"><?= \Yii::t('UI9.1',
                                                    '$BOOKING_FORM_GUARANTY_LABEL$') ?>
                                                <svg
                                                        class="greyBtnDown" width="10px" height="6px"
                                                        viewBox="0 0 10 6">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"
       stroke-linejoin="round">
        <g id="greyBtnDown2" transform="translate(-360.000000, -3196.000000)" stroke-width="2">
            <g id="DESCRIBTION-Copy" transform="translate(211.000000, 2623.000000)">
                <g id="Group-5">
                    <g id="Group-16" transform="translate(20.000000, 393.000000)">
                        <g id="Group-38" transform="translate(0.000000, 171.000000)">
                            <polyline id="Path-3-Copy"
                                      transform="translate(133.757144, 11.935000) scale(-1, 1) rotate(-270.000000) translate(-133.757144, -11.935000) "
                                      points="131.822144 15.6921435 135.692143 11.8221436 132.047856 8.17785643"></polyline>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span>
                                            <span class="rightText boldText right"><?= ProductCurrency::formatPrice($goodUser ? $prices['pledge']['good'] : $prices['pledge']['default'],
                                                    null, true) ?></span>
                                        </div>
                                        <div class="bottomPredge" ng-show="visibleGuarantyBox">
                                            <ul>
                                                <li>
                                                    <md-icon md-svg-src="/img/editProfile/smallCheck.svg"></md-icon>
                                                    <span class="defaultColor"><?= \Yii::t('UI9.1',
                                                            '$BOOKING_FORM_GUARANTY_DEFAULT_LABEL$') ?></span> -
                                                    <span><?= ProductCurrency::formatPrice($prices['pledge']['default'],
                                                            null, true) ?></span>
                                                    <span class="infRating"
                                                          data-toggle="popover"
                                                          data-placement="top auto"
                                                          data-trigger="hover"
                                                          data-content="<?= \Yii::t('UI9.1',
                                                              '$BOOKING_FORM_GUARANTY_DEFAULT_INFO_POPOVER$') ?>">
                                            <svg class="infServiseSVG" width="20px" height="20px" viewBox="0 0 20 20">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="infSVG" transform="translate(-232.000000, -261.000000)">
            <g id="Group-2" transform="translate(0.000000, 199.000000)">
                <g id="Group-24-Copy" transform="translate(233.000000, 63.000000)">
                    <path d="M8.8,0 C4,0 0,3.9 0,8.8 C0,13.7 3.9,17.6 8.8,17.6 C13.7,17.6 17.6,13.7 17.6,8.8 C17.6,3.9 13.6,0 8.8,0 L8.8,0 Z"
                          id="Shape" fill-rule="nonzero"></path>
                    <g id="Group-27" transform="translate(7.000000, 4.000000)">
                        <path d="M1.9,0 C2.5,0 2.9,0.5 2.9,1 C2.9,1.6 2.4,2 1.9,2 C1.3,2 0.9,1.5 0.9,1 C0.8,0.4 1.3,0 1.9,0 L1.9,0 Z"
                              id="Path"></path>
                        <path d="M3.4,8.9 L0.8,8.9 C0.4,8.9 0,8.6 0,8.1 C0,7.6 0.3,7.3 0.8,7.3 L1.4,7.3 L1.4,4.6 L0.8,4.6 C0.4,4.6 0,4.3 0,3.8 C0,3.3 0.3,3 0.8,3 L2.1,3 C2.5,3 2.9,3.3 2.9,3.8 L2.9,7.4 L3.5,7.4 C3.9,7.4 4.3,7.7 4.3,8.2 C4.3,8.7 3.8,8.9 3.4,8.9 L3.4,8.9 Z"
                              id="Path"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                        </span>
                                                </li>
                                                <li>
                                                    <?php if ($goodUser): ?>
                                                        <md-icon md-svg-src="/img/editProfile/smallCheck.svg"></md-icon>
                                                        <span class="defaultColor"><?= \Yii::t('UI9.1', '$BOOKING_FORM_GUARANTY_GOOD_LABEL$') ?></span> -
                                                        <span><?= ProductCurrency::formatPrice($prices['pledge']['good'], null,
                                                                true) ?></span>
                                                    <?php else: ?>
                                                        <?= \Yii::t('UI9.1', '$BOOKING_FORM_GUARANTY_GOOD_LABEL$') ?>
                                                        - <?= ProductCurrency::formatPrice($prices['pledge']['good'], null,
                                                            true) ?>
                                                    <?php endif; ?>

                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <?= Html::endTag('div') ?>
                                </form>

                                <?php Modal::end() ?>

                                <a href="#"
                                   class="btn btnTopBlock hidden-xs"
                                   data-toggle="modal"
                                   data-target="#rentModal"><?= \Yii::t('UI9.1',
                                        '$BOOKING_OPEN_MODAL_BTN_LABEL$') ?></a>

                            </div>

                            <div class="bottomBlock">
                                <?= Html::button('<span ng-switch-when="1">'
                                    . \Yii::t('UI9.1', '$REMOVE_FROM_FAVOURITE_BTN$')
                                    . '</span><span ng-switch-when="0">'
                                    . \Yii::t('UI9.1', '$ADD_TO_FAVOURITE_BTN$')
                                    . '</span>', [
                                    'class' => 'btn add selected',
                                    'ng-switch' => 'isFavourite',
                                    'ng-init' => 'isFavourite = '
                                        . intval($model->is_favourite)
                                        . ';_productId = '
                                        . $model->id,
                                    'ng-click' => 'changeFavourite()',
                                    'ng-class' => '{active: isFavourite}',
                                ]) ?>

                                <div class="input-group notClassic"
                                     ng-class="{selected: isAmazing}"
                                     ng-init="isAmazing = <?= intval($model->is_amazing) ?>">
                                    <label for="notClassic"><?= \Yii::t('UI9.1', '$AMAZING_PRODUCT_BTN$') ?></label>
                                    <input type="text"
                                           id="notClassic"
                                           class="form-control text-center"
                                           ng-click="changeAmazing($event)"/>
                                    <span class="input-group-addon" ng-click="changeAmazing($event)"></span>
                                </div>

                                <?= Html::button(\Yii::t('UI9.1', '$QUESTION_FOR_OWNER_BTN$'), [
                                    'data-toggle' => 'modal',
                                    'data-target' => '#chatWindowPanel',
                                    'class' => 'btn question add',
                                    'data-purpose' => 'info',
                                ]) ?>

                                <?= Html::button(\Yii::t('UI9.1', '$COMPLAIN_ABOUT_A_PRODUCT_BTN$'), [
                                    'data-toggle' => 'modal',
                                    'data-target' => '#chatWindowPanel',
                                    'class' => 'btn violation',
                                    'data-purpose' => 'complaint',
                                ]) ?>

                                <!--                            <a href="#" class="btn question add">Задать вопрос хозяину</a>-->
                                <!--                            <a href="#" class="btn violation">Сообщить о нарушении</a>-->

                                <span class="borderAboutProduct"></span>
                                <div class="social">
                                    <p class="text-center"><?= \Yii::t('UI9.1',
                                            '$SHARE_POST_WITH_FRIENDS_LABEL$') ?></p>
                                    <div class="socialBlock">
                                        <?= Html::a(Html::img('/img/product/mail.svg', ['class' => 'mail']), '#', [
                                            'socialshare' => '',
                                            'socialshare-provider' => 'email',
                                            'socialshare-subject' => 'Смотри на новый товар',
                                            'socialshare-body' => $model->description,
                                        ]) ?>
                                        <a href="#"
                                           socialshare
                                           socialshare-provider="facebook"
                                           socialshare-type="share"
                                           socialshare-text="New product"
                                           socialshare-quote="My product"><img src="/img/product/Facebook_3_.svg"
                                                                               class="faseboock"></a>
                                        <a href="#" socialshare socialshare-provider="vk"><img src="/img/product/Vk.svg"
                                                                                               class="vkontakte"></a>
                                        <a href="#"><img src="/img/product/instagram.svg"
                                                         class="instagram"
                                                         alt="Instagram"/></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php if ($model->delivery_status): ?>
                <div class="maps hidden-xs">
                    <div class="addressDelivery text-center">
                        <h4><?= \Yii::t('UI9.1', '$MAP_PANEL_TITLE$') ?></h4>
                        <div id="search_box-desktop-container"></div>

                        <div ng-show="isNewAddress" ng-cloak>
                            <button type="button" class="btn cansel" ng-click="cancelLocation()"><?= \Yii::t('UI9.1',
                                    '$MAP_PANEL_CANCEL_BTN$') ?></button>
                            <button type="button" class="btn send" ng-click="confirmLocation()"><?= \Yii::t('UI9.1',
                                    '$MAP_PANEL_OK_BTN$') ?></button>
                        </div>
                    </div>

                    <!--                <div id="map-desktop"></div>-->
                    <script id="searchbox.tpl.html" type="text/ng-template">
                        <?= Html::input('text', 'mapAddressDesktop', null, [
                            'class' => 'addressInMaps gmap-autocomplete-field',
                            'placeholder' => \Yii::t('UI9.1', '$MAP_PANEL_INPUT_PLACEHOLDER$'),
                            'id' => 'desktopInputSearchbox',
                        ]) ?>
                    </script>
                    <script id="searchbox.outer.tpl.html" type="text/ng-template">
                        <?= Html::input('text', 'mapAddressDesktop', null, [
                            'class' => 'form-control gmap-autocomplete-field',
                            'placeholder' => \Yii::t('UI9.1', '$MAP_PANEL_INPUT_PLACEHOLDER$'),
                            'id' => 'outerInputSearchbox',
                        ]) ?>
                    </script>

                    <?= Html::beginTag('ui-gmap-google-map', [
                        'center' => "{latitude: $model->lat, longitude: $model->lng}",
                        'zoom' => 12,
                        'pan' => 1,
                        'controls' => '{}',
                        'options' => 'config.maps',
                        'ng-init' => '_coordinatesDistributionPoint = ' . Json::encode([
                                'latitude' => $model->lat,
                                'longitude' => $model->lng,
                            ]),
                    ]); ?>
                    <ui-gmap-search-box template="config.searchbox.template"
                                        events="config.searchbox.events"
                                        options="config.searchbox.options"
                                        parentdiv="'search_box-desktop-container'"></ui-gmap-search-box>
                    <ui-gmap-search-box template="config.searchbox.templateOuter"
                                        events="config.searchbox.events"
                                        options="config.searchbox.options"
                                        parentdiv="'outerParentSearchbox'"></ui-gmap-search-box>

                    <?= Html::tag('ui-gmap-marker', '', [
                        'idKey' => "'IamHereMarker'",
                        'coords' => 'config.marker.coords',
                        'options' => 'config.marker.options',
                        'control' => 'markerControl',
                    ]) ?>
                    <?= Html::tag('ui-gmap-circle', '', [
                        'center' => '_coordinatesDistributionPoint',
                        'radius' => $model->delivery_radius,
                        'stroke' => '{ color: \'#53c77d\', weight: 2, opacity: 0.9 }',
                        'fill' => '{ color: \'#53c77d\', opacity: 0.3 }',
                        'clickable' => 'false',
                    ]); ?>
                    <?= Html::tag('ui-gmap-circle', '', [
                        'center' => '_coordinatesDistributionPoint',
                        'radius' => 300,
                        'stroke' => '{ color: \'#53c77d\', weight: 2, opacity: 0.9 }',
                        'fill' => '{ color: \'#53c77d\', opacity: 0.45 }',
                        'clickable' => 'false',
                    ]); ?>
                    <?= Html::endTag('ui-gmap-google-map'); ?>
                </div>
            <?php endif; ?>

            <?php if (!empty($related)): ?>
                <div class="row sliderProduct">
                    <div class="container">
                        <div class="row body">

                            <?= $this->render('//blocks/product-slider', [
                                'title' => \Yii::t('UI9.1', '$RELATED_PRODUCTS_TITLE$'),
                                'slides' => $related,
                            ]) ?>

                        </div>
                    </div>
                </div>
            <?php endif; ?>


            <?php Modal::begin([
                'id' => 'myModal',
                'header' => '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><svg class="close" width="8px" height="8px" viewBox="0 0 8 8">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
        <g id="UI15.1----Отзывы-(личный-кабинет)---дефолт" transform="translate(-685.000000, -774.000000)" stroke="#FFFFFF" fill="#FFFFFF">
            <g id="content" transform="translate(280.000000, 151.000000)">
                <g id="Group-22" transform="translate(20.000000, 89.000000)">
                    <g id="Group-20">
                        <g id="Group-19" transform="translate(0.000000, 204.000000)">
                            <g id="Group-25" transform="translate(70.000000, 273.000000)">
                                <g id="close-copy" transform="translate(311.000000, 53.000000)">
                                    <g id="close" transform="translate(4.888889, 4.888889)">
                                        <path d="M3.11111111,2.48257175 L0.832788199,0.204248838 C0.659221847,0.0306824861 0.37781519,0.0306824861 0.204248838,0.204248838 L0.204248838,0.832788199 L2.48257175,3.11111111 L0.151606014,5.44207685 C-0.0219603375,5.6156432 -0.0219603375,5.89704986 0.151606014,6.07061621 C0.325172366,6.24418256 0.606579024,6.24418256 0.780145375,6.07061621 L3.11111111,3.73965047 L5.44207685,6.07061621 C5.6156432,6.24418256 5.89704986,6.24418256 6.07061621,6.07061621 C6.24418256,5.89704986 6.24418256,5.6156432 6.07061621,5.44207685 L3.73965047,3.11111111 L6.01797338,0.832788199 C6.19153974,0.659221847 6.19153974,0.37781519 6.01797338,0.204248838 L5.38943402,0.204248838 L3.11111111,2.48257175 Z" id="Combined-Shape-Copy"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span></button>
<div class="row">
                        <div class="col-sm-6 hidden-xs text-left leftHeader">
                            <span>'
                    . \Yii::t('UI9.1', '$SHARE_POST_WITH_FRIENDS_LABEL$')
                    . ':</span>
                            <div class="blockSvg">
                                <a href="#" class="mailP"><svg width="24px" height="16px" viewBox="0 0 24 16">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g class="reMail" transform="translate(-394.000000, -37.000000)" >
            <g id="share" transform="translate(293.000000, 35.000000)">
                <g id="Group-2" transform="translate(101.000000, 0.000000)">
                    <path d="M1.23926278,3.20054155 C1.05648733,3.37344812 0.942399979,3.61876503 0.942399979,3.89125116 L0.942399979,15.1871485 C0.942399979,15.4601935 1.05469603,15.7063015 1.23747189,15.8794155 L1.23747189,15.8794155 L8.52871981,9.53919983 L1.23926278,3.20054155 L1.23926278,3.20054155 L1.23926278,3.20054155 L1.23926278,3.20054155 Z M22.3225276,3.19898425 C22.5053035,3.3720982 22.6175995,3.61820617 22.6175995,3.89125116 L22.6175995,15.1871485 C22.6175995,15.4596346 22.5035122,15.7049516 22.3207367,15.8778581 L15.0312797,9.53919983 L22.3225276,3.19898425 L22.3225276,3.19898425 L22.3225276,3.19898425 Z M14.326204,10.1523091 L21.2039995,16.1359997 L2.35599995,16.1359997 L9.23379548,10.1523091 L11.7799997,12.3663998 L14.326204,10.1523091 L14.326204,10.1523091 L14.326204,10.1523091 Z M1.88818775,2 C0.845370449,2 0,2.84298975 0,3.87920237 L0,15.1991973 C0,16.2370521 0.838512858,17.0783997 1.88818775,17.0783997 L21.6718117,17.0783997 C22.714629,17.0783997 23.5599995,16.2354099 23.5599995,15.1991973 L23.5599995,3.87920237 C23.5599995,2.84134756 22.7214866,2 21.6718117,2 L1.88818775,2 L1.88818775,2 L1.88818775,2 Z M11.7799997,11.1412913 L21.2039995,2.94239998 L2.35599995,2.94239998 L11.7799997,11.1412913 L11.7799997,11.1412913 L11.7799997,11.1412913 Z" id="mail-envelope-closed"></path>
                </g>
            </g>
        </g>
    </g>
</svg></a>
                                <a href="#" class="fbP"><svg width="9px" height="20px" viewBox="0 0 9 20">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="nonzero">
        <g class="reFb" transform="translate(-529.000000, -5471.000000)" >
            <g id="Group-15-Copy-6" transform="translate(280.000000, 5286.000000)">
                <g id="Group-16">
                    <g id="Group-17" transform="translate(227.000000, 177.000000)">
                        <path d="M23.945245,11.6822896 L23.945245,14.2993864 L22,14.2993864 L22,17.5002088 L23.945245,17.5002088 L23.945245,27.0099224 L27.942474,27.0099224 L27.942474,17.5002088 L30.6235868,17.5002088 C30.6235868,17.5002088 30.8743072,15.9650202 30.9963423,14.2872596 L27.9567723,14.2872596 L27.9567723,12.0988619 C27.9567723,11.7720935 28.3920417,11.3319231 28.8229883,11.3319231 L31,11.3319231 L31,8 L28.0405675,8 C23.8468189,8.00032775 23.945245,11.2037722 23.945245,11.6822896 L23.945245,11.6822896 Z" id="Facebook_3_"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></a>
                                <a href="#" class="vkP"><svg width="24px" height="14px" viewBox="0 0 24 14" >
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g class="reVk" transform="translate(-477.000000, -38.000000)">
            <g id="share" transform="translate(293.000000, 35.000000)">
                <g id="Group-2" transform="translate(101.000000, 0.000000)">
                    <g id="1482090935_038_018_vkontakte_vk_social_network_android_material" transform="translate(84.000000, 4.000000)">
                        <g id="Layer_21">
                            <path d="M21.0694894,0.546667685 L17.5108173,0.546667685 C17.2148724,0.546665768 16.9428928,0.709374846 16.8029503,0.970141969 C16.8029503,0.970141969 15.383615,3.584065 14.9277064,4.46448617 C13.7012143,6.83291827 12.9069783,6.08938326 12.9069783,4.99017353 L12.9069783,1.20008085 C12.9069783,0.540561232 12.372332,0.00591494631 11.7128124,0.00591494631 L9.03716781,0.00591494631 C8.29629836,-0.0491696322 7.5797405,0.283327951 7.14345172,0.884638146 C7.14345172,0.884638146 8.50210379,0.664962754 8.50210379,2.49562708 C8.50210379,2.94957818 8.52548594,4.25548523 8.5467916,5.35080153 C8.55501913,5.68140665 8.35616627,5.98205148 8.04870651,6.10385433 C7.74124676,6.22565718 7.39045062,6.14276057 7.17003513,5.89621556 C6.07485248,4.37300895 5.16767603,2.72308586 4.46821814,0.982298091 C4.34555254,0.716381927 4.07927107,0.546241577 3.78642627,0.546667685 L0.553146685,0.546667685 C0.372612602,0.545439173 0.20299664,0.632984429 0.0994179408,0.780854363 C-0.00416075803,0.928724297 -0.0284814128,1.1180448 0.0343593226,1.28729345 C1.01613918,3.98030698 5.2453339,12.4432279 10.0725363,12.4432279 L12.1043065,12.4432279 C12.5476116,12.443222 12.9069783,12.0838505 12.9069783,11.6405454 L12.9069783,10.4136964 C12.9069806,10.099148 13.0938758,9.81467159 13.3825709,9.68978841 C13.6712661,9.56490523 14.0065631,9.62349274 14.2358024,9.83887622 L16.6676864,12.1237728 C16.8860894,12.3289755 17.1744961,12.443215 17.4741759,12.4432279 L20.6676561,12.4432279 C22.2078821,12.4432279 22.2078821,11.374441 21.3681255,10.5474137 C20.7771369,9.96537992 18.6445487,7.71751405 18.6445487,7.71751405 C18.2672055,7.32659523 18.2312217,6.71893833 18.5597911,6.28621725 C19.2491102,5.37909372 20.3765905,3.89419751 20.8547457,3.25807762 C21.5083319,2.38861211 22.6912285,0.546667685 21.0694894,0.546667685 L21.0694894,0.546667685 Z" id="Shape"></path>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></a>
                                <a href="#" class="instP"><svg width="20px" height="20px" viewBox="0 0 20 20">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="UI13.8-и-13.9-------Галерея---как-в-карточке-товара-" transform="translate(-529.000000, -35.000000)">
            <g id="share" transform="translate(293.000000, 35.000000)">
                <g id="Group-2" transform="translate(101.000000, 0.000000)">
                    <g id="Group-7" transform="translate(136.000000, 1.000000)">
                        <rect class="reIns" x="0" y="0" width="18" height="18" rx="5"></rect>
                        <circle class="reIns" cx="9" cy="9" r="4"></circle>
                        <ellipse class="reMail"  cx="13.7838462" cy="4.1976834" rx="1.07307692" ry="1.07722008"></ellipse>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></a>
                            </div>
                        </div>
                        
                        <div class="col-sm-6 col-xs-12 text-right rightHeader">
                            <p><span>'
                    . ProductCurrency::formatPrice($model->price, $model->currency)
                    . '</span>/'
                    . \Yii::t('UI9.1', '$PAYMENT_PERIOD_' . strtoupper($model->leasing) . '$')
                    . '</p><a href="#" class="btn greenBtnHeader" ng-click="openModal($event)">'
                    . \Yii::t('UI9.1', '$PRODUCT_FREE_DAYS_BTN$')
                    . '</a>
                        </div></div>'
                ,
                'footer' => <<<FOOTER
        <div class="slickItemDown"></div>
FOOTER
                ,
                'size' => Modal::SIZE_LARGE,
                'closeButton' => false,
            ]) ?>

            <div class="wrapBody">
                <div class="content">
                    <div class="slickItem">
                        <?php foreach ($media as $item): ?>
                            <?php if ($item->type == 'image'): ?>
                                <?= Html::img($item->link,
                                    ['data' => ['covering-text' => $item->description, 'type' => 'image']]) ?>
                            <?php elseif ($item->type == 'video'): ?>
                                <?= Html::beginTag('video',
                                    [
                                        'controls' => 'controls',
                                        'data' => [
                                            'covering-text' => $item->description,
                                            'type' => 'video',
                                        ],
                                    ]); ?>
                                <?= Html::tag('source', '', ['src' => $item->link, 'type' => 'video/mp4']) ?>
                                <?= Html::endTag('video'); ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="textForSlider text-left">
                        <p class="white"></p>
                        <div class="blockOpenMemu hidden-xs">
                            <a href="#" class="openSubSlider"><?= \Yii::t('UI9.1',
                                    '$MEDIA_MODAL_OPEN_BOTTOM_LIST$') ?></a>
                            <a href="#" class="closeSubSlider hidden"><?= \Yii::t('UI9.1',
                                    '$MEDIA_MODAL_CLOSE_BOTTOM_LIST$') ?></a>
                            <span class="karetOpen">
                                <svg class="toggleOpen" width="10px" height="6px" viewBox="0 0 10 6">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"
       stroke-linejoin="round">
        <g id="UI9.-Карточка-товара---альтернатива" transform="translate(-360.000000, -3196.000000)" stroke="#E35F46"
           stroke-width="2">
            <g id="DESCRIBTION-Copy" transform="translate(211.000000, 2623.000000)">
                <g id="Group-5">
                    <g id="Group-16" transform="translate(20.000000, 393.000000)">
                        <g id="Group-38" transform="translate(0.000000, 171.000000)">
                            <polyline id="Path-3-Copy"
                                      transform="translate(133.757144, 11.935000) scale(-1, 1) rotate(-270.000000) translate(-133.757144, -11.935000) "
                                      points="131.822144 15.6921435 135.692143 11.8221436 132.047856 8.17785643"></polyline>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="bottomPathBody hidden-sm hidden-md hidden-lg">
                <div class="hidden-sm hidden-lg hidden-md leftHeader">
                    <span><?= \Yii::t('UI9.1', '$SHARE_POST_WITH_FRIENDS_LABEL$') ?>:</span>
                    <div class="blockSvg">
                        <a href="#" class="mailP">
                            <svg width="24px" height="16px" viewBox="0 0 24 16">
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g class="reMail" transform="translate(-394.000000, -37.000000)">
                                        <g id="share" transform="translate(293.000000, 35.000000)">
                                            <g id="Group-2" transform="translate(101.000000, 0.000000)">
                                                <path d="M1.23926278,3.20054155 C1.05648733,3.37344812 0.942399979,3.61876503 0.942399979,3.89125116 L0.942399979,15.1871485 C0.942399979,15.4601935 1.05469603,15.7063015 1.23747189,15.8794155 L1.23747189,15.8794155 L8.52871981,9.53919983 L1.23926278,3.20054155 L1.23926278,3.20054155 L1.23926278,3.20054155 L1.23926278,3.20054155 Z M22.3225276,3.19898425 C22.5053035,3.3720982 22.6175995,3.61820617 22.6175995,3.89125116 L22.6175995,15.1871485 C22.6175995,15.4596346 22.5035122,15.7049516 22.3207367,15.8778581 L15.0312797,9.53919983 L22.3225276,3.19898425 L22.3225276,3.19898425 L22.3225276,3.19898425 Z M14.326204,10.1523091 L21.2039995,16.1359997 L2.35599995,16.1359997 L9.23379548,10.1523091 L11.7799997,12.3663998 L14.326204,10.1523091 L14.326204,10.1523091 L14.326204,10.1523091 Z M1.88818775,2 C0.845370449,2 0,2.84298975 0,3.87920237 L0,15.1991973 C0,16.2370521 0.838512858,17.0783997 1.88818775,17.0783997 L21.6718117,17.0783997 C22.714629,17.0783997 23.5599995,16.2354099 23.5599995,15.1991973 L23.5599995,3.87920237 C23.5599995,2.84134756 22.7214866,2 21.6718117,2 L1.88818775,2 L1.88818775,2 L1.88818775,2 Z M11.7799997,11.1412913 L21.2039995,2.94239998 L2.35599995,2.94239998 L11.7799997,11.1412913 L11.7799997,11.1412913 L11.7799997,11.1412913 Z"
                                                      id="mail-envelope-closed"></path>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </a>
                        <a href="#" class="fbP">
                            <svg width="9px" height="20px" viewBox="0 0 9 20">
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="nonzero">
                                    <g class="reFb" transform="translate(-529.000000, -5471.000000)">
                                        <g id="Group-15-Copy-6" transform="translate(280.000000, 5286.000000)">
                                            <g id="Group-16">
                                                <g id="Group-17" transform="translate(227.000000, 177.000000)">
                                                    <path d="M23.945245,11.6822896 L23.945245,14.2993864 L22,14.2993864 L22,17.5002088 L23.945245,17.5002088 L23.945245,27.0099224 L27.942474,27.0099224 L27.942474,17.5002088 L30.6235868,17.5002088 C30.6235868,17.5002088 30.8743072,15.9650202 30.9963423,14.2872596 L27.9567723,14.2872596 L27.9567723,12.0988619 C27.9567723,11.7720935 28.3920417,11.3319231 28.8229883,11.3319231 L31,11.3319231 L31,8 L28.0405675,8 C23.8468189,8.00032775 23.945245,11.2037722 23.945245,11.6822896 L23.945245,11.6822896 Z"
                                                          id="Facebook_3_"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </a>
                        <a href="#" class="vkP">
                            <svg width="24px" height="14px" viewBox="0 0 24 14">
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g class="reVk" transform="translate(-477.000000, -38.000000)">
                                        <g id="share" transform="translate(293.000000, 35.000000)">
                                            <g id="Group-2" transform="translate(101.000000, 0.000000)">
                                                <g id="1482090935_038_018_vkontakte_vk_social_network_android_material"
                                                   transform="translate(84.000000, 4.000000)">
                                                    <g id="Layer_21">
                                                        <path d="M21.0694894,0.546667685 L17.5108173,0.546667685 C17.2148724,0.546665768 16.9428928,0.709374846 16.8029503,0.970141969 C16.8029503,0.970141969 15.383615,3.584065 14.9277064,4.46448617 C13.7012143,6.83291827 12.9069783,6.08938326 12.9069783,4.99017353 L12.9069783,1.20008085 C12.9069783,0.540561232 12.372332,0.00591494631 11.7128124,0.00591494631 L9.03716781,0.00591494631 C8.29629836,-0.0491696322 7.5797405,0.283327951 7.14345172,0.884638146 C7.14345172,0.884638146 8.50210379,0.664962754 8.50210379,2.49562708 C8.50210379,2.94957818 8.52548594,4.25548523 8.5467916,5.35080153 C8.55501913,5.68140665 8.35616627,5.98205148 8.04870651,6.10385433 C7.74124676,6.22565718 7.39045062,6.14276057 7.17003513,5.89621556 C6.07485248,4.37300895 5.16767603,2.72308586 4.46821814,0.982298091 C4.34555254,0.716381927 4.07927107,0.546241577 3.78642627,0.546667685 L0.553146685,0.546667685 C0.372612602,0.545439173 0.20299664,0.632984429 0.0994179408,0.780854363 C-0.00416075803,0.928724297 -0.0284814128,1.1180448 0.0343593226,1.28729345 C1.01613918,3.98030698 5.2453339,12.4432279 10.0725363,12.4432279 L12.1043065,12.4432279 C12.5476116,12.443222 12.9069783,12.0838505 12.9069783,11.6405454 L12.9069783,10.4136964 C12.9069806,10.099148 13.0938758,9.81467159 13.3825709,9.68978841 C13.6712661,9.56490523 14.0065631,9.62349274 14.2358024,9.83887622 L16.6676864,12.1237728 C16.8860894,12.3289755 17.1744961,12.443215 17.4741759,12.4432279 L20.6676561,12.4432279 C22.2078821,12.4432279 22.2078821,11.374441 21.3681255,10.5474137 C20.7771369,9.96537992 18.6445487,7.71751405 18.6445487,7.71751405 C18.2672055,7.32659523 18.2312217,6.71893833 18.5597911,6.28621725 C19.2491102,5.37909372 20.3765905,3.89419751 20.8547457,3.25807762 C21.5083319,2.38861211 22.6912285,0.546667685 21.0694894,0.546667685 L21.0694894,0.546667685 Z"
                                                              id="Shape"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </a>
                        <a href="#" class="instP">
                            <svg width="20px" height="20px" viewBox="0 0 20 20">
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="UI13.8-и-13.9-------Галерея---как-в-карточке-товара-"
                                       transform="translate(-529.000000, -35.000000)">
                                        <g id="share" transform="translate(293.000000, 35.000000)">
                                            <g id="Group-2" transform="translate(101.000000, 0.000000)">
                                                <g id="Group-7" transform="translate(136.000000, 1.000000)">
                                                    <rect class="reIns"
                                                          x="0"
                                                          y="0"
                                                          width="18"
                                                          height="18"
                                                          rx="5"></rect>
                                                    <circle class="reIns" cx="9" cy="9" r="4"></circle>
                                                    <ellipse class="reMail"
                                                             cx="13.7838462"
                                                             cy="4.1976834"
                                                             rx="1.07307692"
                                                             ry="1.07722008"></ellipse>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="blockBtnBody" role="group">
                    <div class="btn-group">
                        <button type="button" class="btn all active hidden-xs"><?= \Yii::t('UI9.1',
                                '$MEDIA_MODAL_BOTTOM_LIST_FILTER_ALL$'); ?></button>
                        <button type="button" class="btn foto"><?= \Yii::t('UI9.1',
                                '$MEDIA_MODAL_BOTTOM_LIST_FILTER_PHOTO$'); ?></button>
                        <button type="button" class="btn video"><?= \Yii::t('UI9.1',
                                '$MEDIA_MODAL_BOTTOM_LIST_FILTER_VIDEO$'); ?></button>
                    </div>
                </div>
                <div class="countSlides hidden-xs text-center"><p><?= \Yii::t('UI9.1', '$MEDIA_MODAL_COUNT_PHOTO$',
                            ['all' => count($media)]) ?></p></div>
                <div class="sliderNav hidden-xs">
                    <?php foreach ($media as $item): ?>
                        <div>
                            <?php if ($item->type == 'image'): ?>
                                <?= Html::img($item->link,
                                    ['class' => 'img-responsive', 'data' => ['type' => 'image']]) ?>
                            <?php elseif ($item->type == 'video') : ?>
                                <?= Html::tag('video',
                                    Html::tag('source', '', ['src' => $item->link, 'type' => 'video/mp4']),
                                    ['data' => ['type' => 'video']]) ?>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>

            <?php Modal::end() ?>


            <div class="modal fade rentModal bs-example-modal-lg" id="rentModal" tabindex="-1" role="dialog"
                 aria-labelledby="rentModal" aria-hidden="true" ng-controller="RentModalController as RMC">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-body">
                            <?php $form = \yii\widgets\ActiveForm::begin([
                                'id' => 'rent-form',
                                'options' => [
                                    'name' => 'rentForm',
                                    'ng-submit' => 'submitOrder($event)',
                                ],
                            ]); ?>
                            <?= Html::activeHiddenInput($orderModel, 'product_id'); ?>
                            <?= Html::activeHiddenInput($orderModel, 'total_cost', ['ng-value' => 'totalPrice']); ?>

                            <!--                            <form name="rentForm">-->
                            <div class="title text-justify">
                                <h4 class="text-left"><?= \Yii::t('UI9.1', '$RENT_MODAL_TITLE$') ?></h4>
                                <p class="countStep hidden-sm hidden-md hidden-lg">
                                    <?= \Yii::t('UI9.1', '$RENT_MODAL_MOBILE_STEP_PROCESS$', ['all' => 3]) ?>
                                </p>

                                <button type="button" class="close closeDesk hidden-xs" data-dismiss="modal">
                                    <span aria-hidden="true">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
  <path fill="#FFF" fill-rule="evenodd" stroke="#FFF" stroke-linecap="round" d="M7,5.58578644 L1.87377345,0.459559885 C1.48324916,0.0690355937 0.850084177,0.0690355937 0.459559885,0.459559885 L0.459559885,1.87377345 L5.58578644,7 L0.341113532,12.2446729 C-0.0494107594,12.6351972 -0.0494107594,13.2683622 0.341113532,13.6588865 C0.731637824,14.0494108 1.3648028,14.0494108 1.75532709,13.6588865 L7,8.41421356 L12.2446729,13.6588865 C12.6351972,14.0494108 13.2683622,14.0494108 13.6588865,13.6588865 C14.0494108,13.2683622 14.0494108,12.6351972 13.6588865,12.2446729 L8.41421356,7 L13.5404401,1.87377345 C13.9309644,1.48324916 13.9309644,0.850084177 13.5404401,0.459559885 L12.1262266,0.459559885 L7,5.58578644 Z" transform="translate(1 1)"/>
</svg>
                                    </span>
                                </button>
                                <button type="button" class="close closeMob hidden-sm hidden-md hidden-lg" data-dismiss="modal">
                                    <span aria-hidden="true">
                                        <svg width="22px" height="21px" viewBox="0 0 22 21">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
        <g id="UI1.6.2-Модал-категориии" transform="translate(-373.000000, -25.000000)" stroke="#3C3D3E"
           stroke-width="2">
            <g id="Group-4">
                <g id="close---icon---grey" transform="translate(374.000000, 26.000000)">
                    <path d="M0.318645628,18.83413 L19.9895953,0.203126916" id="Line"></path>
                    <path d="M0.318645628,18.83413 L19.9895953,0.203126916" id="Line-Copy-2"
                          transform="translate(10.154120, 9.518628) scale(-1, 1) translate(-10.154120, -9.518628) "></path>
                </g>
            </g>
        </g>
    </g>
</svg>
                                    </span>
                                </button>
                            </div>
                            <div class="topBlock">
                                <div class="aboutProduct">
                                    <div class="media">
                                        <a class="media-left hidden-xs" href="#">
                                            <?= Html::img($firstPhoto, ['class' => 'imgProduct']) ?>
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading">
                                                <?= Html::encode($model->name) ?>
                                            </h4>

                                            <span class="btnTypeProduct"><?= $model->category->name ?></span>
                                            <a href="#" class="hidden-sm hidden-md hidden-lg imgProdXs">
                                                <?= Html::img($firstPhoto, ['class' => 'img-responsive']) ?>
                                            </a>

                                            <div class="media mediaMan">
                                                <?= Html::a(Html::img($owner_avatar),
                                                    ['/profile', 'id' => $model['creator_id']],
                                                    ['class' => 'media-left']); ?>
                                                <div class="media-body">
                                                        <span class="greyText"><?= \Yii::t('UI9.1',
                                                                '$RENT_MODAL_OWNER_TITLE$') ?></span>
                                                    <?= Html::a(Html::tag('h5',
                                                        Html::encode($model->author->userProfile->fullName)),
                                                        ['/profile', 'id' => $model['creator_id']],
                                                        ['class' => 'name']); ?>
                                                </div>
                                            </div>
                                            <p class="please"><span>
                                                    <svg width="13px" height="18px" viewBox="0 0 13 18">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="UI9.-Карточка-товара---запрос-на-бронирование-1/3" transform="translate(-21.000000, -461.000000)"
           fill-rule="nonzero" fill="#AFAFAF">
            <g id="Group" transform="translate(21.000000, 461.000000)">
                <path d="M6.5,17.5 L6.1,17.1 C5.9,16.8 0.4,10.6 0.4,6.4 C0.4,3 3.1,0.3 6.5,0.3 C9.9,0.3 12.6,3 12.6,6.4 C12.6,10.6 7.1,16.8 6.9,17.1 L6.5,17.5 Z M6.5,1.3 C3.7,1.3 1.4,3.6 1.4,6.4 C1.4,9.7 5.2,14.5 6.5,16 C7.7,14.5 11.6,9.7 11.6,6.4 C11.6,3.6 9.3,1.3 6.5,1.3 Z"
                      id="Shape"></path>
                <path d="M6.5,9.6 C4.7,9.6 3.2,8.1 3.2,6.3 C3.2,4.5 4.7,3 6.5,3 C8.3,3 9.8,4.5 9.8,6.3 C9.8,8.1 8.3,9.6 6.5,9.6 Z M6.5,4 C5.2,4 4.2,5 4.2,6.3 C4.2,7.6 5.2,8.6 6.5,8.6 C7.8,8.6 8.8,7.6 8.8,6.3 C8.8,5.1 7.8,4 6.5,4 Z"
                      id="Shape"></path>
            </g>
        </g>
    </g>
</svg></span>
                                                <?= (is_null($model->author->userProfile->country) ? '' : $model->author->userProfile->country->name
                                                    . ', ')
                                                . $model->author->userProfile->city
                                                . ', ' . \Yii::t('UI9.1', '$ABOUT_OWNER_JOINED$',
                                                    ['date' => $model->author->created_at]); ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="starReting text-right">
                                        <div class="rateyoHome"></div>
                                        <br>
                                        <span class="btnTypeProduct hidden-xs"><?= $model->category->name ?></span>
                                    </div>
                                </div>

                                <div class="rent">
                                    <?= Html::activeHiddenInput($orderModel, 'order_start', ['ng-value' => '+(rentFrom.getTime() / 1000)']); ?>
                                    <?= Html::activeHiddenInput($orderModel, 'order_stop', ['ng-value' => '+(rentTo.getTime() / 1000)']); ?>

                                    <table class="table-responsive">
                                        <tr>
                                            <td class="greyText"><?= \Yii::t('UI9.1',
                                                    '$RENT_MODAL_START_RENT_DATE_LABEL$') ?>:
                                            </td>
                                            <td class="leftXs">{{(rentFrom | date:"shortDate") || "-"}}
                                                <span class="greyText">{{rentFrom | date:"shortTime"}}</span></td>
                                        </tr>
                                        <tr>
                                            <td class="greyText"><?= \Yii::t('UI9.1',
                                                    '$RENT_MODAL_STOP_RENT_DATE_LABEL$') ?>:
                                            </td>
                                            <td class="leftXs">{{(rentTo | date:"shortDate") || "-"}}
                                                <span class="greyText">{{rentTo | date:"shortTime"}}</span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="priceRent">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="left">
                                                <table class="table-responsive">
                                                    <tr>
                                                        <td class="blackText"><?= \Yii::t('UI9.1',
                                                                '$RENT_MODAL_PRICE_LABEL$') ?>:
                                                        </td>
                                                        <td class="leftXs">{{_price | currency:currencySymbol}}
                                                            / <?= \Yii::t('UI9.1', '$PAYMENT_PERIOD_'
                                                                . strtoupper($model->leasing)
                                                                . '$'); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="blackText"><?= \Yii::t('UI9.1',
                                                                '$RENT_MODAL_TRADE_DELIVERY_PRICE_LABEL$') ?>:
                                                        </td>
                                                        <td class="leftXs">{{((isDelivery && useDelivery) ?
                                                            deliveryPrice : 0) | currency:currencySymbol}}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="blackText"><?= \Yii::t('UI9.1',
                                                                '$RENT_MODAL_TRADE_COMMISSION_LABEL$') ?>:
                                                        </td>
                                                        <td class="leftXs">{{serviceCommission |
                                                            currency:currencySymbol}}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="right">
                                                <p class="black summRent"><?= \Yii::t('UI9.1',
                                                        '$RENT_MODAL_PLEDGE_PRICE_LABEL$') ?>: <span
                                                            class="red"><?= ProductCurrency::formatPrice($goodUser ? $prices['pledge']['good'] : $prices['pledge']['default'],
                                                            null, true) ?></span></p>
                                                <p class="grey">
                                                    <?= \Yii::t('UI9.1', '$RENT_MODAL_REFUND_TEXT$') ?>
                                                </p>
                                                <a href="#" class="red"><?= \Yii::t('UI9.1',
                                                        '$RENT_MODAL_REFUND_DETAIL_LINK$') ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="price hidden-xs">
                                    <h4><?= \Yii::t('UI9.1', '$RENT_MODAL_TRADE_FULL_PRICE_LABEL$') ?>:
                                        <span class="red">{{totalPrice | currency:currencySymbol}}</span></h4>
                                </div>
                            </div>

                            <div class="massage hidden-xs">
                                <p class="textTop"><?= \Yii::t('UI9.1',
                                        '$RENT_MODAL_ADDITIONAL_MESSAGE_FOR_OWNER_LABEL$') ?></p>
                                <?= Html::activeTextarea($orderModel, 'comment', [
                                    'placeholder' => \Yii::t('UI9.1',
                                        '$RENT_MODAL_ADDITIONAL_MESSAGE_FOR_OWNER_PLACEHOLDER$'),
                                ]); ?>
                            </div>

                            <div class="lastBlock hidden">
                                <div class="payment">
                                    <h4><?= \Yii::t('UI9.1', '$RENT_MODAL_PAYMENT_TITLE$') ?></h4>
                                    <p><?= \Yii::t('UI9.1', '$RENT_MODAL_PAYMENT_DESCRIPTION$') ?></p>
                                </div>

                                <div class="cancelRent">
                                    <h4><?= \Yii::t('UI9.1', '$RENT_MODAL_CANCEL_AFTER_BOOKING_TITLE$') ?></h4>
                                    <p><?= \Yii::t('UI9.1', '$RENT_MODAL_CANCEL_AFTER_BOOKING_DESCRIPTION$') ?>
                                        <span class="red count">???</span>.</p>
                                </div>

                                <div class="methodPayment">
                                    <?= Html::activeHiddenInput($orderModel, 'payment',
                                        ['ng-value' => 'payment_method']); ?>

                                    <h4><?= \Yii::t('UI9.1', '$RENT_MODAL_PAYMENT_METHODS_TITLE$') ?>:</h4>
                                    <div class="topInput">
                                        <label for="payment"><?= \Yii::t('UI9.1',
                                                '$RENT_MODAL_PAYMENT_CARD_TITLE$') ?>:</label><br>
                                        <div uib-dropdown>
                                            <button type="button" uib-dropdown-toggle>{{payment_method || "Select..."}}
                                                <span class="caret"></span></button>
                                            <ul uib-dropdown-menu
                                                class="dropdown-menu"
                                                role="menu"
                                                ng-click="setPayment($event)">
                                                <li role="menuitem" ng-repeat="n in ['1234 1234 1234 1234']"
                                                    name="{{n}}"><a href="#">{{n}}</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="bottomInput">
                                        <md-checkbox ng-model="agree_rules" class="redCheckBox">
<span class="hidden-sm hidden-md hidden-lg"><?= \Yii::t('UI9.1',
        '$RENT_MODAL_MOBILE_ACCEPT_TERMS_LINK$') ?></span>
                                            <span class="hidden-xs"><?= \Yii::t('UI9.1',
                                                    '$RENT_MODAL_ACCEPT_TERMS_LINK$') ?></span>
                                        </md-checkbox>
                                    </div>
                                </div>
                            </div>
                            <a href="#"
                               class="btn btn-block questionOwner hidden-sm hidden-md hidden-lg"><?= \Yii::t('UI9.1',
                                    '$RENT_MODAL_COMPOSE_DISCUSSION_WITHOUT_TRADE$') ?></a>

                            <div class="bottomBlock text-justify">
                                <div class="price hidden-sm hidden-md hidden-lg">
                                    <h4>{{_price | currency:currencySymbol}}
                                        <span class="grey">/ <?= \Yii::t('UI9.1', '$PAYMENT_PERIOD_'
                                                . strtoupper($model->leasing)
                                                . '$'); ?></span></h4>
                                    <a href="#" class="red"><?= \Yii::t('UI9.1',
                                            '$RENT_MODAL_MOBILE_PAYMENT_DETAIL_LINK$'); ?></a>
                                </div>
                                <a href="#" class="question hidden-xs"><?= \Yii::t('UI9.1',
                                        '$RENT_MODAL_COMPOSE_DISCUSSION_WITHOUT_TRADE$'); ?></a>
                                <button type="button"
                                        class="nextStep send hidden-sm hidden-md hidden-lg"><?= \Yii::t('UI9.1',
                                        '$RENT_MODAL_MOBILE_NEXT_STEP_BTN$') ?>
                                </button>
                                <button type="button"
                                        class="send paymentStep hidden-xs"
                                        ng-disabled="!(rentFrom && rentTo)">
                                    <?= \Yii::t('UI9.1', '$RENT_MODAL_GOTO_PAYMENT_BTN$') ?>
                                </button>
                                <button type="submit"
                                        class="send reqyestPayment hidden hidden-sm hidden-md hidden-lg"
                                        ng-disabled="!(rentFrom && rentTo && payment_method && agree_rules) || isComplete"
                                        ng-switch="isComplete">
                                    <span ng-switch-when="1">Complete</span>
                                    <span ng-switch-default><?= \Yii::t('UI9.1',
                                            '$RENT_MODAL_MOBILE_REQUEST_TRADE_BTN$') ?></span></button>
                                <button type="submit"
                                        class="send reqyestPayment hidden hidden-xs"
                                        ng-disabled="!(rentFrom && rentTo && payment_method && agree_rules) || isComplete"
                                        ng-switch="isComplete">
                                    <span ng-switch-when="1">Complete</span>
                                    <span ng-switch-default><?= \Yii::t('UI9.1',
                                            '$RENT_MODAL_REQUEST_TRADE_BTN$') ?></span></button>
                            </div>
                            <!--                            </form>-->
                            <?php \yii\widgets\ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- панель с выбором даты и времени бронирования -->
            <div class="hidden">
                <div id="timePanel" layout-padding layout="row" layout-align="start start">
                    <div layout="column" flex="45">
                        <div>Дата начала сделки</div>
                        <div uib-datepicker ng-model="rentFrom" datepicker-options="config.datepicker"></div>

                        <!-- только если аренда на сутки или час -->
                        <div>Время начала аренды</div>
                        <div uib-timepicker=""
                             ng-model="rentFrom"
                             min="config.timepicker.minTime"
                             max="config.timepicker.maxTime"
                             minute-step="config.timepicker.minuteStep" show-meridian="false"></div>

                        <div><?= \Yii::t('UI9.1',
                                '$SELECT_DATE_INTERVAL_PANEL_PERIOD_'
                                . strtoupper($model->leasing)
                                . '_TITLE$') ?></div>
                        <md-slider-container>
                            <md-slider ng-model="period"
                                       min="{{_minTimeInterval}}"
                                       max="{{_maxTimeInterval}}"
                                       md-discrete=""></md-slider>

                            <md-input-container>
                                <input type="text" ng-model="period" flex readonly="readonly"/>
                            </md-input-container>
                        </md-slider-container>
                    </div>
                    <div flex="55" layout="column">
                        <div ui-calendar="config.calendar"
                             calendar="intervalHoursCalendar"
                             ng-model="intervalCalendar"></div>

                        <div layout="row" layout-align="end stretch">
                            <md-button ng-click="closeModal()">cancel</md-button>
                            <md-button ng-click="closeModal(true)">confirm</md-button>
                        </div>
                    </div>

                    <!--                <div id="calendar"></div>-->
                </div>
                <div id="datePanel" layout-padding>
                    <div>
                        <div>Дата начала сделки</div>
                        <div uib-datepicker ng-model="rentFrom" datepicker-options="config.datepicker"></div>

                        <div><?= \Yii::t('UI9.1',
                                '$SELECT_DATE_INTERVAL_PANEL_PERIOD_'
                                . strtoupper($model->leasing)
                                . '_TITLE$') ?></div>
                        <md-slider-container>
                            <md-slider ng-model="period"
                                       min="{{_minTimeInterval}}"
                                       max="{{_maxTimeInterval}}"
                                       md-discrete=""></md-slider>

                            <md-input-container>
                                <input type="text" ng-model="period" flex readonly="readonly"/>
                            </md-input-container>
                        </md-slider-container>

                        <div>
                            <md-button ng-click="closeModal()">cancel</md-button>
                            <md-button ng-click="closeModal(true)">confirm</md-button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php
$this->registerJsFile('/js/grom.js', [
    'position' => \yii\web\View::POS_END,
    'depends' => ['frontend\assets\AppAsset',],
]);

$ja = \yii\helpers\Json::encode($a);
$mw = \yii\helpers\Json::encode($when);

$this->registerJs(<<<JS
var categoryNestedList = {$ja};
var categoryWhenList = {$mw};
JS
    , \yii\web\View::POS_BEGIN);

$this->registerJs(<<<JS
    jQuery("[data-toggle='tooltip']").tooltip();
JS
);

$this->registerJsFile('/js/aproduct.js', [
    'depends' => [
        'frontend\assets\FrontendAsset',
        'frontend\assets\AngularAsset',
    ],
    'position' => \yii\web\View::POS_END,
]);
//$lang = mb_substr(\Yii::$app->language, 0, 2);
//$this->registerJsFile("//maps.googleapis.com/maps/api/js?key=AIzaSyBureh4Q5KfHjIxlbtLqoaDHM1xqDYI-zY&v=3.exp&language={$lang}&libraries=places");
/*
$this->registerJs(<<<JS
var placexy = {lat: {$model->lat}, lng: {$model->lng}};
var mapd = new google.maps.Map(jQuery("#map-desktop").get(0), {
    center: placexy,
    zoom: 11
});
var mapm = new google.maps.Map(jQuery("#map-mobile").get(0), {
    center: placexy,
    zoom: 11
});
var autocompletemobile = new google.maps.places.Autocomplete(jQuery(".addressInMaps").get(0));
autocompletemobile.bindTo("bounds", mapm);

// добавить 2 круга на карту
var circleBig = new google.maps.Circle({
    strokeColor: "#53ca7e",
    strokeWeight: 2,
    fillColor: "#53ca7e",
    fillOpacity: 0.25,
    center: placexy,
    radius: 3000
});
var circleSmall = new google.maps.Circle({
    strokeColor: "#53ca7e",
    strokeWeight: 2,
    fillColor: "#53ca7e",
    fillOpacity: 0.4,
    center: placexy,
    radius: 250
});
var circleSmall2 = _.cloneDeep(circleSmall);
var circleBig2 = _.cloneDeep(circleBig);

circleSmall.setMap(mapm);
circleSmall2.setMap(mapd);
circleBig.setMap(mapm);
circleBig2.setMap(mapd);

autocompletemobile.addElementListener("place_changed", function(){
    var place = autocompletemobile.getPlace();
});
JS
);
*/
$this->registerCss(<<<CSS
#map-desctop,#map-mobile{
    height: 100%;
}
.md-panel-outer-wrapper{
    position: fixed;
}
.md-panel.float_panel{
    background-color: #fff;
}

/* карты */
ui-gmap-google-map{
    display: block;
    height: 100%;
}
.angular-google-map,.angular-google-map-container{
    height: 100%;
}

/* inline modal*/
.modal.inline{
    position: static;
    display: block !important;
    /*overflow: auto;*/
}
.modal.inline .modal-dialog{
    width: auto;
    margin: 0;
}
.modal.inline .modal-header{
    display: none;
}
.modal.inline .modal-content{
    box-shadow: none;
    -webket-box-shadow: none;
    border: none;
}
.modal.inline .modal-body{
    padding: 0;
}

.contentBody .mobileBtnRent{
    z-index: 1051;
}
.mobileBtnRent .btn.green{
    z-index: 1051;
}
CSS
);