<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.05.2017
 * Time: 12:49
 */

use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var array $cgr */
/** @var array $list */


\frontend\assets\ScrollAsset::register($this);
\yii\bootstrap\BootstrapPluginAsset::register($this);
\frontend\assets\ProductAsset::register($this);
?>


    <div class="row categoryPage">
        <div class="container">
            <div class="row" ng-controller="CategoryController as CC">
                <div class="col-sm-6 hidden-xs">
                    <h1 class="text-left titleCategoryPage"><?= \Yii::t('UI7', '$PAGE_TITLE$') ?></h1>
                </div>
                <div class="col-sm-6 text-right col-xs-12 formSearchForCategory styleInput">
                    <form method="get" action="/category/search" id="search-category-form" autocomplete="off">
                        <?= Html::input('text', 'fields', null, [
                            'class'       => 'subCategorySearch',
                            'placeholder' => \Yii::t('UI7', '$FIELD_AUTOCOMPLETE_PLACEHOLDER$'),
                            'ng-keypress' => 'scanAll($event)',
                            'ng-focus'    => 'isFocusAutoComplete = true',
                            'ng-blur'     => 'isFocusAutoComplete = false',
                        ]) ?>
                    </form>

                    <div class="wrappSearch" ng-show="isFocusAutoComplete && outcome.length">
                        <div id="listCategorySearch" class="listCategorySearch text-left scroll-1">
                            <ul class="list-unstyled" id="scrollList">
                                <li ng-repeat="c in outcome">
                                    <!--                                    <a href="{{'/search?PS[category_id]=' + c.id + '&PS[note]=' + c.name}}"-->
                                    <a href="{{'/category/search?fields=' + c.name}}"
                                            ng-bind-html="c.label"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="blockAllCategory">
                    <?php $index = 1; ?>
                    <?php foreach ($cgr as $item): ?>

                        <?= $this->render('_category-item', [
                            'model' => $item,
                            'index' => $index++,
                        ]) ?>

                    <?php endforeach; ?>
                </div>

                <div class="col-xs-12 hidden-xs hidden-md hidden-sm hidden-lg firstSubCategoryMob">
                    <p class="imgCateg text-center">imgCateg</p>
                    <ul class="list-unstyled">
                        <li><a href="#">CATEG<span>(0)</span></a></li>
                    </ul>
                </div>

                <div class="col-xs-12 hidden-lg hidden-md hidden-sm hidden-xs twoSubCategMob">
                    <ul class="list-unstyled">
                        <li><a href="#">CATE<span>(0)</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php
$this->registerJsFile('/js/grom.js', [
    'position' => \yii\web\View::POS_END,
    'depends'  => ['frontend\assets\AppAsset', 'frontend\assets\FrontendAsset'],
]);
$this->registerJsFile('/js/acategory.js', [
    'position' => \yii\web\View::POS_END,
    'depends'  => ['frontend\assets\AppAsset', 'frontend\assets\FrontendAsset'],
]);


$listJson = \yii\helpers\Json::encode($list);

$this->registerJs(<<<JS
var categoryProductGroup = $listJson;


JS
    , \yii\web\View::POS_BEGIN);


$this->registerJsFile('/js/aheadPanel.js', [
    'position' => \yii\web\View::POS_END,
    'depends'  => [
        'frontend\assets\AppAsset',
    ],
]);

$this->registerJs(<<<JS
  (function() {
      
      var dropmenu = $(".headerMobile .droppMenuMob");
      var cont = $(".headerMobile .content");
      var namePage = $(".headerMobile .categContent");
      
    if ($(window).width() <= '767'){
        cont.hide();
        dropmenu.hide();
        namePage.fadeIn();
        
    }    else {
        return;
    }
  }());

JS
);