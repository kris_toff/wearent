<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 15.05.2017
 * Time: 14:48
 */

use yii\bootstrap\Html;

/** @var \yii\web\View $this */
/** @var \yiimodules\categories\models\Categories[] $list */
/** @var string $word */

//print '<pre>';print_r($list);exit;

\yii\bootstrap\BootstrapPluginAsset::register($this);
\frontend\assets\FrontendAsset::register($this);
\frontend\assets\ProductAsset::register($this);
\frontend\assets\yyAsset::register($this);
\frontend\assets\RateAsset::register($this);

?>


    <div class="row filterAll">
        <div class="container">
            <div class="row filterCategory">
                <div class="col-sm-6 hidden-xs titleCategoryPage">
                    <h1 class="text-left "><?= \Yii::t('UI7', '$PAGE_TITLE$') ?></h1>
                </div>
                <div class="col-sm-6 col-xs-12 text-right">
                    <form id="search-form" class="styleInput" action="/category/repel">

                        <div class="blockInputSearch">
                            <?= Html::input('text', 'fields', \Yii::$app->request->get('fields'), [
                                'class'       => 'subCategorySearch',
                                'placeholder' => \Yii::t('UI7', '$FIELD_AUTOCOMPLETE_PLACEHOLDER$'),
                            ]) ?>

                            <a href="/category"><span>
                                    <svg width="12px" height="12px" viewBox="0 0 12 12" >
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
        <g id="UI14.2-------Дизайн-подсказок-по-ховеру" transform="translate(-716.000000, -1734.000000)" stroke="#E35F46" fill="#E35F46">
            <g id="Group-15-Copy-2" transform="translate(280.000000, 1392.000000)">
                <g id="Group-4" transform="translate(19.000000, 22.000000)">
                    <g id="Group-19-Copy-9" transform="translate(207.000000, 283.000000)">
                        <g id="Group-6-Copy" transform="translate(114.000000, 31.000000)">
                            <path d="M102,10.8976492 L98.3130114,7.2106606 C98.0321306,6.9297798 97.5767328,6.9297798 97.295852,7.2106606 L97.295852,8.22781997 L100.982841,11.9148086 L97.2106606,15.6869886 C96.9297798,15.9678694 96.9297798,16.4232672 97.2106606,16.704148 C97.4915414,16.9850288 97.9469392,16.9850288 98.22782,16.704148 L102,12.931968 L105.77218,16.704148 C106.053061,16.9850288 106.508459,16.9850288 106.789339,16.704148 C107.07022,16.4232672 107.07022,15.9678694 106.789339,15.6869886 L103.017159,11.9148086 L106.704148,8.22781997 C106.985029,7.94693916 106.985029,7.4915414 106.704148,7.2106606 L105.686989,7.2106606 L102,10.8976492 Z" id="Combined-Shape-Copy"></path>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                                </span></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row searchTape">
        <div class="container">
            <div class="row">

                <?php foreach ($list as $item): ?>

                    <!--                    --><? //= Html::tag('div', Html::a("<b>{$item['label']}</b>", $item['url']), ['class' => 'col-sm-12']) ?>
                    <?= Html::tag('div', "<b>{$item['label']}</b>", ['class' => 'col-sm-12']) ?>

                    <?php
                    if (isset($item['items'])) {
                        $count = count($item['items']);
                        switch ($count) {
                        case 1:
                        case 2:
                        case 3:
                            $ch = array_chunk($item['items'], 1);
                            break;
                        case 4:
                            $ch = [
                                [$item['items'][0], $item['items'][1]],
                                [$item['items'][2]],
                                [$item['items'][3]],
                            ];
                            break;
                        default:
                            $ch = array_chunk($item['items'], ceil($count / 3));
                        }

                        // цикл по колонкам
                        foreach ($ch as $z) {
                            print Html::beginTag('div', ['class' => 'col-sm-4']);

                            // цикл по элементам колонки
                            foreach ($z as $q) {
                                $isChild = isset($q['items']) && count($q['items']);
                                if ($isChild) {
                                    print Html::tag('div', $q['label'], ['class' => 'sub-list']);
                                    print Html::beginTag('div', ['style' => '']);

                                    foreach ($q['items'] as $n) {
                                        print Html::a(preg_replace("~$word~iu", '<span class="sel">$0</span>',
                                                $n['label']), $n['url']) . '<br>';
                                    }

                                    print Html::endTag('div');
                                } else {
                                    print Html::tag('div',
                                        Html::a(preg_replace("~$word~iu", '<span class="sel">$0</span>', $q['label']),
                                            $q['url']));
                                }
                            }

                            print Html::endTag('div');
                        }
                    }
                    print Html::tag('div', '', ['class' => 'clearfix']);
                    ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <div class="row similarAll">
        <div class="container">
            <div class="row body">
                <?= $this->render('//blocks/product-slider', [
                    'title'  => 'Похожие товари',
                    'slides' => [
                        [
                            'minislide'     => [
                                ['src' => '/img/Bitmap.jpg',],
                                ['src' => '/img/mitsubishi1.jpg'],
                                ['src' => '/img/mitsubishi2.jpg'],
                            ],
                            'product_name'  => 'Длинное название товара',
                            'category'      => 'Транспорт',
                            'slug'          => '#',
                            'price'         => 100,
                            'currency'      => 'rub',
                            'min_period'    => 1,
                            'step'          => 'Day',
                            'countComments' => 5,
                        ],
                        [
                            'minislide'     => [
                                ['src' => '/img/Bitmap.jpg',],
                                ['src' => '/img/mitsubishi1.jpg'],
                                ['src' => '/img/mitsubishi2.jpg'],
                            ],
                            'product_name'  => 'Длинное название товара',
                            'category'      => 'Транспорт',
                            'price'         => 200,
                            'slug'          => '#',
                            'currency'      => 'usd',
                            'min_period'    => 1,
                            'step'          => 'Day',
                            'countComments' => 5,
                        ],
                        [
                            'minislide'     => [
                                ['src' => '/img/Bitmap.jpg',],
                                ['src' => '/img/mitsubishi1.jpg'],
                                ['src' => '/img/mitsubishi2.jpg'],
                            ],
                            'product_name'  => 'Оооооочень длинное название товара',
                            'category'      => 'Транспорт',
                            'price'         => 100,
                            'slug'          => '#',
                            'currency'      => 'rub',
                            'min_period'    => 1,
                            'step'          => 'Day',
                            'countComments' => 5,
                        ],
                        [
                            'minislide'     => [
                                ['src' => '/img/Bitmap.jpg',],
                                ['src' => '/img/mitsubishi1.jpg'],
                                ['src' => '/img/mitsubishi2.jpg'],
                            ],
                            'product_name'  => 'Оооооочень длинное название товара',
                            'category'      => 'Транспорт',
                            'price'         => 100,
                            'slug'          => '#',
                            'currency'      => 'rub',
                            'min_period'    => 1,
                            'step'          => 'Day',
                            'countComments' => 5,
                        ],
                    ],
                ]) ?>

            </div>
        </div>
    </div>


<?php

//$this->registerJsFile('/js/grom.js', [
//    'position' => \yii\web\View::POS_END,
//    'depends'  => ['frontend\assets\AppAsset'],
//]);
$this->registerJs(<<<JS
jQuery(".sub-list").on("click", function(event){
    jQuery(event.currentTarget).next().slideToggle(300);
});
JS
);

$this->registerCss(<<<CSS
.sub-list{
    color: #5f70ff;
    cursor: pointer;
}
CSS
);