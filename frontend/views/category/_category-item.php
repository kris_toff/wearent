<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.05.2017
 * Time: 15:19
 */

use yii\bootstrap\Html;
use yii\widgets\Menu;
use yii\helpers\ArrayHelper;

/** @var \yii\web\View $this */
/** @var array $model */
/** @var integer $index */

?>


<div class="col-md-4 col-xs-6 blockCategory">
    <div class="imgTypCategory"><?= Html::img($model['image']) ?></div>
    <span class="borderTop hidden-xs">aaaaaaa</span>
    <div class="titleBlockCategory"><b><?= $model['label'] ?></b>
        <a class="openAllCtegory hidden-xs" href="#" onclick="return false;">Развернуть все</a></div>
    <div class="listBlockCategory hidden-xs">
        <?= Menu::widget([
            'items'           => ArrayHelper::getValue($model, 'items'),
            'encodeLabels'    => false,
            'submenuTemplate' => "\n<ul class=\"submenu list-unstyled\" style='display: none'>\n{items}\n</ul>\n",
            'itemOptions'     => [
                'class' => 'retert',
            ],
            'options'         => [
                'class' => 'list-unstyled',
            ],
        ]) ?>
    </div>
</div>


<?php
if (!($index % 3)) print '<div class="clearfix hidden-xs"></div>'; ?>

<?php
if (!($index % 2)) print '<div class="clearfix hidden-sm hidden-md hidden-lg"></div>'; ?>
