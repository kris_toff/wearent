<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 07.04.2017
 * Time: 10:37
 */

use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$c = \common\models\Currency::find()
    ->filterWhere([
        '<>',
        'id',
        ArrayHelper::getValue(\Yii::$app->params, 'currency.id'),
    ])
    ->orderBy(['is_payment' => SORT_DESC])->all();

$items = [];
foreach ($c as $item) {
    $img = $item['icon'];

    $items[] = [
        'label' => '<span class="labelImg">' . (is_null($img) ? '' : Html::img($img) . ' ') . '</span>' . \Yii::t('UI21',
                '$FOOTER_DROPDOWN_CURRENCY_LABEL_'
                . strtoupper($item['code'])
                . '$'),
        'url'   => Url::to(['/utils/change-currency', 'return_url' => 'test', 'c' => $item['id']]),
    ];
}
?>


<div class="row foter ">
    <div class="container">
        <div class="row body">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-md-3 col-sm-12 wearent">
                        <ul class="list-unstyled">
                            <li><a href="/"><img src="/img/category/logo@2x.png" alt=""></a></li>
                            <div class="inlineBlock">
                                <li class="translate dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="translateMenu"
                                            data-toggle="dropdown" aria-expanded="true">
                                        <?= '<span class="labelImg">' . Html::img([
                                            '/img/footer/lang_flags/flag-'
                                            . strtolower(substr(\Yii::$app->language, 0, 2))
                                            . '.png',
                                        ], ['alt' => 'country flag']) . '</span>' ?><?= \Yii::t('UI21',
                                            mb_strtoupper('$footer_language_' . \Yii::$app->language . '$', 'UTF-8')) ?>
                                        <span class="caret"></span>
                                    </button>

                                    <?= \frontend\wizards\LanguageDropdown::widget([
                                        'options' => [
                                            'role'       => 'menu',
                                            'aria-label' => 'translateMenu',
                                        ],
                                    ]) ?>
                                </li>
                                <?php if (isset(\Yii::$app->params['currency'])): ?>
                                    <li class="manu manuDrop">
                                        <?= \yii\bootstrap\ButtonDropdown::widget([
                                            'id'               => 'switchCurrencyBtn',
                                            'label'            => '<span class="labelImg"> '
                                                . (isset(\Yii::$app->params['currency'], \Yii::$app->params['currency']['icon']) ? Html::img(\Yii::$app->params['currency']['icon']) : '')
                                                . '</span>'
                                                . \Yii::t('UI21', '$FOOTER_DROPDOWN_CURRENCY_LABEL_'
                                                    . strtoupper(\Yii::$app->params['currency']['code'])
                                                    . '$'),
                                            'dropdown'         => [
                                                'id'           => 'switchCurrencyList',
                                                'items'        => $items,
                                                'encodeLabels' => false,
                                            ],
                                            'encodeLabel'      => false,
                                            'containerOptions' => [
                                                'class' => 'dropdown',
                                            ],
                                        ]) ?>
                                    </li>
                                <?php endif; ?>
                                <li class="manu dropdown hidden">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="manuMenu"
                                            data-toggle="dropdown" aria-expanded="true">
                                        <img class="rubl" src="/img/footer/rubl.png" alt="">
                                        Рубли
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="manuMenu">
                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Дія</a></li>
                                    </ul>
                                </li>
                            </div>
                            <li class="wear hidden-xs hidden-sm"><span>© Wearent, 2016.<br>
Все права защищены.</span></li>

                        </ul>
                    </div>

                    <div class="col-md-3 col-sm-4 col-xs-6 company">
                        <ul class="list-unstyled">
                            <li class="titleList"><a href="#"><strong>Компания</strong></a></li>
                            <li><a href="#">О проекте</a></li>
                            <li><a href="#">Работа</a></li>
                            <li><a href="#">Пресса о нас</a></li>
                            <li><a href="#">Правила и Политики</a></li>
                            <li><a href="#"> Социальная ответственность</a></li>
                        </ul>
                    </div>

                    <div class="col-md-3 col-sm-4 col-xs-6 aboutUs">
                        <ul class="list-unstyled">
                            <li class="titleList"><a href="#"><strong>Больше о нас</strong></a></li>
                            <li><a href="#">Доверие и безопасность</a></li>
                            <li><a href="#">Акции</a></li>
                            <li><a href="#">Самые необычные товары</a></li>
                            <li><a href="#"> Популярные вопросы</a></li>
                            <li><a href="#">Мобильная версия</a></li>
                            <li><a href="#">Карта сайта </a></li>
                        </ul>
                    </div>

                    <div class="col-md-3 col-sm-4 col-xs-12 social">
                        <span class="hidden-xs titleList"><strong>Социальные сети</strong></span>
                        <ul class="list-unstyled">
                            <li class="faseboock"><img src="/img/category/Facebook_3_.svg" alt="facebook"><a href="#"
                                        class="hidden-xs">Facebook</a>
                            </li>
                            <li class="instagram"><img src="/img/category/instagram.svg" alt="instagram"><a href="#"
                                        class="hidden-xs">Instagram</a>
                            </li>
                            <li class="vkontakte"><img src="/img/category/Vk.svg" alt="vkontakte"><a href="#"
                                        class="hidden-xs">Vkontakte</a>
                            </li>
                            <li class="google"><img src="/img/category/google+.svg" alt="google+"><a href="#"
                                        class="hidden-xs">GooglePlus</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xs-12  hidden-md hidden-lg">
                        <p class="text-center">© Wearent, 2016.<br>
                            Все права защищены.</p>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <div class="paymenImg">
        <div class="wrap">
            <div><img src="/img/editProfile/paymentImg/qiwi.png" alt="qivi"></div>
            <div><img src="/img/editProfile/paymentImg/yandex.png" alt="yandex"></div>
            <div><img src="/img/editProfile/paymentImg/skrill.png" alt="skrill"></div>
            <div><img src="/img/editProfile/paymentImg/visa.png" alt="visa"></div>
            <div><img src="/img/editProfile/paymentImg/master.png" alt="mastercard"></div>
        </div>
    </div>
</div>

