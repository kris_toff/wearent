<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 13.11.2017
 * Time: 23:01
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/** @var \frontend\models\HistorySearch $model */
/** @var string $key */
/** @var integer $index */

$m = new \frontend\models\ProductSearch();

$params = [
    $m->formName() . '[category_id]'     => $model['field_category_id'],
    $m->formName() . '[note]'            => $model['field_what'],
    $m->formName() . '[location_string]' => $model['field_where_string'],
    $m->formName() . '[location]'        => $model['field_where_coords'],
    $m->formName() . '[period_from]'     => $model['field_when_from'],
    $m->formName() . '[period_to]'       => $model['field_when_to'],
];

?>

<?= Html::beginTag('a', ArrayHelper::merge(['href' => '/search'], $params)) ?>
<span class="left">
    <?= ArrayHelper::getValue($model, 'field_what', '') ?>

    <p class="date"><?= is_null($model['field_when_from']) ? '' : \Yii::$app->formatter->asDate($model['field_when_from'],
            'dd.MM.yyyy') ?>
        <svg width="22px" height="8px" viewBox="0 0 22 8" version="1.1">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="UI1.5.4-Дропдаун--при-фокусе-на-даты" transform="translate(-138.000000, -206.000000)">
            <g id="Group-15" transform="translate(138.000000, 206.000000)">
                <polyline id="Path-3-Copy-2" stroke="#AFAFAF" stroke-linecap="round" stroke-linejoin="round"
                        transform="translate(18.757144, 3.935000) scale(-1, 1) rotate(-180.000000) translate(-18.757144, -3.935000) "
                        points="16.8221436 7.69214346 20.6921435 3.82214357 17.0478563 0.177856427"></polyline>
                <rect id="Rectangle-2" fill="#AFAFAF" x="0" y="3.5" width="20" height="1"></rect>
            </g>
        </g>
    </g>
</svg> <?= is_null($model['field_when_to']) ? '' : \Yii::$app->formatter->asDate($model['field_when_to'],
            'dd.MM.yyyy') ?></p>
</span>
<span class="right"><span class="glyphicon glyphicon-chevron-right"></span></span>
<?= Html::endTag('a') ?>

