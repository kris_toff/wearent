<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 07.04.2017
 * Time: 10:37
 */

use yii\bootstrap\Html;

/** @var \yii\web\View $this */
/** @var \frontend\models\ProductSearch|null $searchForm */

\frontend\assets\InputAnimate::register($this);
\frontend\assets\FrontendAsset::register($this);

$searchForm = \yii\helpers\ArrayHelper::getValue($this->params, 'searchForm', new \frontend\models\ProductSearch());

$a = \frontend\models\CategoryList::tree();
/*
$a = [

    1 => [
        'id'   => 1,
        'name' => 'Музыка',
    ],

    3 => [
        'id'   => 3,
        'name' => 'Спорт, Отдых',
        'node' => [
            101  => [
                'id'   => 101,
                'name' => 'Велосипеды, гироскутеры, ролики, скейты',
            ],
            1011 => [
                'id'   => 3302,
                'name' => 'Лыжи, коньки, санки',
            ],
            102  => [
                'id'   => 3302,
                'name' => 'Пикник, кемпинг, пляж',
            ],
            103  => [
                'id'   => 103,
                'name' => 'Туризм, путешествия',
                'node' => [
                    3320 => [
                        'id'   => 3332,
                        'name' => 'Велосипеды, гироскутеры, ролики, скейты',
                    ],
                    3321 => [
                        'id'   => 3333,
                        'name' => 'Лыжи, коньки, санки',
                    ],
                    3322 => [
                        'id'   => 3334,
                        'name' => 'Пикник, кемпинг, пляж',
                    ],
                    3323 => [
                        'id'   => 3335,
                        'name' => 'Туризм, путешествия',
                    ],
                    3324 => [
                        'id'   => 3336,
                        'name' => 'Спортивные игры',
                    ],
                    3325 => [
                        'id'   => 3337,
                        'name' => 'Дайвинг, водный спорт',
                    ],
                    3326 => [
                        'id'   => 3338,
                        'name' => 'Услуги инструкторов',
                    ],
                    3327 => [
                        'id'   => 3339,
                        'name' => 'Аренда спортивных помещений',
                    ],
                    3328 => [
                        'id'   => 3340,
                        'name' => 'Картинг',
                    ],
                    3329 => [
                        'id'   => 3341,
                        'name' => 'Тренажеры, фитнес, единоборства',
                    ],
                    3330 => [
                        'id'   => 3342,
                        'name' => 'Пневматика и снаряжение',
                    ],

                ],
            ],
            104  => [
                'id'   => 3304,
                'name' => 'Спортивные игры',
            ],
            105  => [
                'id'   => 3305,
                'name' => 'Дайвинг, водный спорт',
            ],
            106  => [
                'id'   => 3306,
                'name' => 'Услуги инструкторов',
            ],
            107  => [
                'id'   => 3307,
                'name' => 'Аренда спортивных помещений',
            ],
            108  => [
                'id'   => 3308,
                'name' => 'Картинг',
            ],
            109  => [
                'id'   => 3309,
                'name' => 'Тренажеры, фитнес, единоборства',
            ],
            1010 => [
                'id'   => 3310,
                'name' => 'Пневматика и снаряжение',
            ],

        ],
    ],

    4 => [
        'id'   => 331,
        'name' => 'Фото, Видео',
    ],

    5 => [
        'id'   => 332,
        'name' => 'Праздники и события',
    ],

    6 => [
        'id'   => 333,
        'name' => 'Инструменты и спец. техника',
    ],

    2 => [
        'id'   => 334,
        'name' => 'Детские товары',
    ],

    7 => [
        'id'   => 334,
        'name' => 'Авто Мото',
    ],

    8 => [
        'id'   => 335,
        'name' => 'Красота и здоровье',
    ],

    9 => [
        'id'   => 336,
        'name' => 'Бизнес',
    ],

    10 => [
        'id'   => 337,
        'name' => 'Одежда, обувь, акссесуары',
    ],

    11 => [
        'id'   => 337,
        'name' => 'Бытовая техника',
    ],

    12 => [
        'id'   => 337,
        'name' => 'Все для дома',
    ],

    13 => [
        'id'   => 337,
        'name' => 'Дача, сад',
    ],


];
*/

//print_r($this->params['searchForm']);
//print_r($searchForm);exit;
$when = [
    1 => [
        'id' => 1,
        'name' => 'Россия, Москва',
    ],

    2 => [
        'id' => 2,
        'name' => 'Московская область, Мытищи улица Новосельева',
    ],
    3 => [
        'id' => 3,
        'name' => 'Россия, Москва',
    ],
    4 => [
        'id' => 4,
        'name' => 'Московская область, Мытищи улица Новосельева',
    ],
    5 => [
        'id' => 5,
        'name' => 'Россия, Москва',
    ],
    6 => [
        'id' => 6,
        'name' => 'Московская область, Мытищи улица Новосельева',
    ],
];

$listPopup = [
    1 => [
        'id' => 1,
        'name' => 'Музыка',
        'img' => '/img/category/music.svg',
    ],
    3 => [
        'id' => 3,
        'name' => 'Спорт, Отдых',
        'node' => [
            101 => [
                'id' => 3301,
                'name' => 'Велосипеды, гироскутеры, ролики, скейты',
            ],
            1011 => [
                'id' => 3302,
                'name' => 'Лыжи, коньки, санки',
            ],
            102 => [
                'id' => 3302,
                'name' => 'Пикник, кемпинг, пляж',
            ],
            103 => [
                'id' => 103,
                'name' => 'Туризм, путешествия',
                'node' => [
                    3320 => [
                        'id' => 3332,
                        'name' => 'Велосипеды, гироскутеры, ролики, скейты',
                    ],
                    3321 => [
                        'id' => 3333,
                        'name' => 'Лыжи, коньки, санки',
                    ],
                    3322 => [
                        'id' => 3334,
                        'name' => 'Пикник, кемпинг, пляж',
                    ],
                    3323 => [
                        'id' => 3335,
                        'name' => 'Туризм, путешествия',
                    ],
                    3324 => [
                        'id' => 3336,
                        'name' => 'Спортивные игры',
                    ],
                    3325 => [
                        'id' => 3337,
                        'name' => 'Дайвинг, водный спорт',
                    ],
                    3326 => [
                        'id' => 3338,
                        'name' => 'Услуги инструкторов',
                    ],
                    3327 => [
                        'id' => 3339,
                        'name' => 'Аренда спортивных помещений',
                    ],
                    3328 => [
                        'id' => 3340,
                        'name' => 'Картинг',
                    ],
                    3329 => [
                        'id' => 3341,
                        'name' => 'Тренажеры, фитнес, единоборства',
                    ],
                    3330 => [
                        'id' => 3342,
                        'name' => 'Пневматика и снаряжение',
                    ],

                ],
            ],
            104 => [
                'id' => 3304,
                'name' => 'Спортивные игры',
            ],
            105 => [
                'id' => 3305,
                'name' => 'Дайвинг, водный спорт',
            ],
            106 => [
                'id' => 3306,
                'name' => 'Услуги инструкторов',
            ],
            107 => [
                'id' => 3307,
                'name' => 'Аренда спортивных помещений',
            ],
            108 => [
                'id' => 3308,
                'name' => 'Картинг',
            ],
            109 => [
                'id' => 3309,
                'name' => 'Тренажеры, фитнес, единоборства',
            ],
            1010 => [
                'id' => 3310,
                'name' => 'Пневматика и снаряжение',
            ],

        ],
        'img' => '/img/category/backpack.svg',
    ],

    4 => [
        'id' => 331,
        'name' => 'Фото, Видео',
        'img' => '/img/category/foto.svg',
    ],

    5 => [
        'id' => 332,
        'name' => 'Праздники и события',
        'img' => '/img/category/music.svg',
    ],

    6 => [
        'id' => 333,
        'name' => 'Инструменты и спец. техника',
        'img' => '/img/category/music.svg',
    ],

    2 => [
        'id' => 334,
        'name' => 'Детские товары',
        'img' => '/img/category/music.svg',
    ],

    7 => [
        'id' => 334,
        'name' => 'Авто Мото',
        'img' => '/img/category/auto.svg',
    ],

    8 => [
        'id' => 335,
        'name' => 'Красота и здоровье',
        'img' => '/img/category/music.svg',
    ],

    9 => [
        'id' => 336,
        'name' => 'Бизнес',
        'img' => '/img/category/tv.svg',
    ],

    10 => [
        'id' => 337,
        'name' => 'Одежда, обувь, акссесуары',
        'img' => '/img/category/stuff.svg',
    ],

    11 => [
        'id' => 337,
        'name' => 'Бытовая техника',
        'img' => '/img/category/drill.svg',
    ],

    12 => [
        'id' => 337,
        'name' => 'Все для дома',
        'img' => '/img/category/bett.svg',
    ],

    13 => [
        'id' => 337,
        'name' => 'Дача, сад',
        'img' => '/img/category/music.svg',
    ],


];

?>

    <div class="header">
        <div class="body">
            <div class="leftBlockHeader">
                <div class="logo text-uppercase">
                    <a class="logoBig" href="/"><img src="/img/category/logo.svg" alt="Wearent"></a>
                    <a class="logoSmall hidden-sm" href="/">
                        <img src="/img/category/logoMobile.svg" alt="Wearent">
                    </a>
                </div>

                <div class="rent" ng-controller="SearchBarController as SBC">
                    <?= Html::beginForm(['/search'], 'get', ['autocomplete' => 'off', 'class' => 'fooorm']) ?>
                    <?= Html::activeHiddenInput($searchForm, 'isOnlyCategory', [
                        'ng-value' => 'onlyCategory',
                        'ng-init' => 'onlyCategory = ' . (integer)$searchForm->isOnlyCategory,
                    ]) ?>

                    <div class="input-floating fst">
                        <?= Html::activeHiddenInput($searchForm, 'category_id', [
                            'ng-value' => 'categoryId',
                            'ng-init' => 'categoryId = ' . ($searchForm['category_id'] ?: 'null'),
                        ]) ?>

                        <label class="labelWhat" for="labelSearch"><?= \Yii::t('UI2',
                                '$SEARCH_BAR_FIELD_WHAT_LONG_LABEL$') ?></label>
                        <label class="labelWhat infocus" for="labelSearch"><?= \Yii::t('UI2',
                                '$SEARCH_BAR_FIELD_WHAT_LABEL$') ?>:</label>
                        <span style="display: none" class="labelWhatBtn">
                                <svg width="11px" height="11px" viewBox="0 0 8 14" version="1.1">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"
       stroke-linejoin="round">
        <g id="btnLeftHeader" transform="translate(-1207.000000, -591.000000)" stroke-width="2">
            <g id="Group-11" transform="translate(0.000000, 579.000000)">
                <g id="Recommended">
                    <g id="Lenta" transform="translate(210.000000, 0.000000)">
                        <g id="arrows" transform="translate(927.000000, 1.000000)">
                            <g id="Group-37" transform="translate(56.000000, 0.000000)">
                                <polyline id="arrow_right_black"
                                          transform="translate(17.935389, 17.699575) scale(-1, 1) rotate(90.000000) translate(-17.935389, -17.699575) "
                                          points="15 23.3991501 20.8707778 17.5283723 15.3424055 12"></polyline>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
                            </span>
                        <!--                                <input name="fieldWhat" type="text" id="" class=" ">-->
                        <?= Html::activeTextInput($searchForm, 'note',
                            [
                                'id' => 'what_input-header',
                                'class' => 'rentBlockImg rentBlock focusInput',
                                'ng-keypress' => 'enterWord($event)',
                                'ng-focus' => 'isWhatFocus = true',
                                'ng-blur' => 'isWhatFocus = false',
                            ]) ?>

                        <div class="autocomplete_searchbar"
                             ng-show="asyncCompleteCategory.length && isWhatFocus"
                             ng-mousedown="setAutoWord($event)">
                            <div class="text-left scroll-1">
                                <ul class="list-unstyled">
                                    <li ng-repeat="a in asyncCompleteCategory track by a.id" data-id="{{a.id}}">
                                        <a ng-bind-html="a.name"></a>
                                        <!--                                            <a href="/search?PS[category_id]={{a.id}}" ng-bind-html="a.name"></a></li>-->
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="extraSearchFields" style="display: none;">
                        <div class="input-floating fff">
                            <?= Html::activeHiddenInput($searchForm, 'location', [
                                'ng-value' => 'location',
                                'ng-init' => 'location = '
                                    . ($searchForm['location'] ? "'{$searchForm['location']}'" : 'null'),
                            ]) ?>

                            <label class="labelWhat labelWhere" for="when_input-header"><?= \Yii::t('UI2',
                                    '$SEARCH_BAR_FIELD_WHERE_LABEL$') ?></label>
                            <label class="labelWhat infocus" for="when_input-header"><?= \Yii::t('UI2',
                                    '$SEARCH_BAR_FIELD_WHERE_LABEL$') ?>:</label>

                            <?= Html::activeTextInput($searchForm, 'location_string', [
                                'id' => 'when_input-header',
                                'class' => 'rentBlock rentBlockWhat focusInput search-autocomplete',
                                'placeholder' => '',
                            ]) ?>
                        </div>

                        <div class="input-floating input-floating2 inputgreat10">
                            <?= Html::activeHiddenInput($searchForm, 'period_from', [
                                'ng-value' => 'period_from',
                                'ng-init' => 'period_from = ' . ((int)$searchForm['period_from']
                                    > 0 ? (int)$searchForm['period_from'] : 'undefined'),
                            ]) ?>
                            <?= Html::activeHiddenInput($searchForm, 'period_to', [
                                'ng-value' => 'period_to',
                                'ng-init' => 'period_to = ' . ((int)$searchForm['period_to']
                                    > 0 ? (int)$searchForm['period_to'] : 'undefined'),
                            ]) ?>

                            <label class="labelWhat labelWhen" for="labelSearch"><?= \Yii::t('UI2',
                                    '$SEARCH_BAR_FIELD_WHEN_LABEL$') ?></label>
                            <label class="labelWhat infocus" for="labelSearch"><?= \Yii::t('UI2',
                                    '$SEARCH_BAR_FIELD_WHEN_LABEL$') ?>:</label>

                            <?= Html::textInput('period', null, [
                                'class' => 'rentBlock rentBlockWhenRight focusInput',
                                'id' => 'from',
                                'role' => 'datepicker',
                            ]) ?>

                        </div>

                        <?= Html::submitButton('Найти', ['class' => 'btn btnSend']) ?>
                    </div>
                    <?= Html::endForm() ?>

                </div>
            </div>

            <div class="rightRent">
                <?php if (!\Yii::$app->user->isGuest): ?>
                    <a href="#" tabindex="0" class="btn btnRightRent" role="button" data-toggle="popover"
                       data-trigger="hover" data-container="body" data-html="true" data-placement="bottom"
                       data-content="<p>Почти у каждого человек имеется от 10 до 30 вещей, которые бессмысленно хранить  в кладовке, но которые жалко выкинуть.</p>
<p>Вы можете зарабатывать <span style='color: #0f0;'>$10 000</span>, сдавая в аренду ненужные вам вещи.</p>">
                        <svg class="imgBtnRightRent" width="17px" height="23px" viewBox="0 0 17 23" version="1.1">

                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g class="colorImgRent" transform="translate(-726.000000, -26.000000)"
                                   fill-rule="nonzero">
                                    <g id="MENU" transform="translate(1.000000, 0.000000)">
                                        <g id="Group-6" transform="translate(708.000000, 21.000000)">
                                            <g id="Group" transform="translate(17.000000, 5.000000)">
                                                <path d="M16.2,6.5 L14.7,6.5 C14.4,6.5 14.2,6.7 14.2,7 C14.2,7.3 14.4,7.5 14.7,7.5 L15.7,7.5 L15.7,21.4 L1,21.4 L1,7.5 L2,7.5 C2.3,7.5 2.5,7.3 2.5,7 C2.5,6.7 2.3,6.5 2,6.5 L0.5,6.5 C0.2,6.5 0,6.7 0,7 L0,22 C0,22.3 0.2,22.5 0.5,22.5 L16.2,22.5 C16.5,22.5 16.7,22.3 16.7,22 L16.7,7 C16.7,6.8 16.5,6.5 16.2,6.5 Z"
                                                      id="Shape"></path>
                                                <path d="M4.3,16.9 C4,16.9 3.8,17.1 3.8,17.4 C3.8,17.7 4,17.9 4.3,17.9 L12.5,17.9 C12.8,17.9 13,17.7 13,17.4 C13,17.1 12.8,16.9 12.5,16.9 L4.3,16.9 Z"
                                                      id="Shape"></path>
                                                <path d="M4.8,8.8 L4.1,12.7 C4.1,12.9 4.1,13.1 4.3,13.2 C4.4,13.3 4.5,13.3 4.6,13.3 C4.7,13.3 4.8,13.3 4.8,13.2 L8.3,11.3 L11.8,13.2 C12,13.3 12.2,13.3 12.3,13.2 C12.5,13.1 12.5,12.9 12.5,12.7 L11.8,8.8 L14.6,6 C14.7,5.9 14.8,5.7 14.7,5.5 C14.6,5.3 14.5,5.2 14.3,5.2 L10.4,4.6 L8.6,1 C8.4,0.7 7.9,0.7 7.7,1 L5.9,4.6 L2,5.2 C1.8,5.2 1.7,5.4 1.6,5.5 C1.5,5.7 1.6,5.9 1.7,6 L4.8,8.8 Z M6.6,5.6 C6.8,5.6 6.9,5.5 7,5.3 L8.4,2.4 L9.8,5.3 C9.9,5.4 10,5.6 10.2,5.6 L13.4,6.1 L11.1,8.4 C11,8.5 10.9,8.7 11,8.8 L11.5,12 L8.6,10.5 C8.5,10.5 8.4,10.4 8.4,10.4 C8.4,10.4 8.2,10.4 8.2,10.5 L5.3,12 L5.8,8.8 C5.8,8.6 5.8,8.5 5.7,8.4 L3.4,6.1 L6.6,5.6 Z"
                                                      id="Shape"></path>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        <span class="hiddenLep hidden-sm">Pазместить объявление</span></a>
                <?php endif; ?>
                <a href="#" class="help"><img src="/img/category/help.svg" alt=""><span class="hiddenLep"><span
                                class="hidden-sm">Помощь</span></span></a>

                <?php if (!\Yii::$app->user->isGuest): ?>
                    <a href="#" class="message"><span class="hiddenLep hidden-sm">Сообщения </span><span
                                class="dynamicHelp"></span></a>
                <?php endif; ?>

                <!--    інфомація про повідомлення в хедері-->
                <div class="massageHeader" id="massageHeader" style="display: none">
                    <div class="titleWindow">
                        <h5>Сообщения</h5>
                    </div>
                    <div class="list-group  scroll-1">
                        <div class="media hidden">
                            <div class="media-left">
                                <a href="#">
                                    <img src="/img/chat/manLeft.jpg" alt="">
                                </a>

                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Тема сообщения</h4>
                                <div class="inlineBlock">
                                    <h5 class="red">Виталий Разгулин</h5><span class="grey"> &nbsp;•  1 час назад</span>
                                </div>
                                <p>Существует целый ряд различных факторов, оказывающих влияние</p>
                            </div>
                            <div class="media-labels">

                            </div>
                        </div>
                        <div class="media">
                            <div class="media-left">
                                <a href="#">
                                    <img src="/img/chat/manLeft.jpg" alt="">
                                </a>

                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Тема сообщения</h4>
                                <div class="inlineBlock">
                                    <h5 class="red">Виталий Разгулин</h5><span class="grey"> &nbsp;•  1 час назад</span>
                                </div>
                                <p>Существует целый ряд различных факторов, оказывающих влияние</p>
                            </div>
                            <div class="media-labels">

                            </div>
                        </div>
                    </div>
                    <div class="footerWindow">
                        <a href="#" class="btn btn-block red">Написать в службу поддержки</a>
                    </div>
                </div>
            </div>


            <!--            HOVER НА ПЕРШИЙ ІНПУТ І ВІДКРИВАЄ ІСТОРІЮ-->
            <?php
            $cache = \Yii::$app->cache;
            if ($this->params['historySearchDataProvider']->getCount()): ?>
                <div id="WRAPPANEL" style="display: none">
                    <div class="col-sm-10 history scroll-1">

                        <?= \yii\widgets\ListView::widget([
                            'beforeItem' => function ($model, $key, $index, $widget) use ($cache) {
                                $c = (int)$model->field_category_id;
                                if ($c > 0) {
                                    $model->field_what = $cache->getOrSet('HistorySearch' . $c,
                                        function () use ($c) {
                                            return \yiimodules\categories\models\Categories::find()
                                                ->select('name')
                                                ->where(['id' => $c])
                                                ->scalar();
                                        });
                                }
                            },
                            'dataProvider' => $this->params['historySearchDataProvider'],
                            'emptyText' => 'нет истории',
                            'itemView' => '/search/_history-panel',
                            'itemOptions' => [
                                'class' => 'liText',
                            ],
                            'layout' => '{items}',
                        ]) ?>

                    </div>
                </div>
            <?php endif; ?>


            <!--        КЛІК НА КНОПКУ В ПОЛІ ШТО І ВІДКРИВАЄ СПИСОК-->
            <div class="col-sm-9 selectProduct" style="display: none">
                <div class="row1">
                    <div class="leftSelect">
                        <ul class="list-unstyled">
                            <?php foreach ($a as $k => $i): ?>
                                <?= Html::tag('li', $i['name'], ['id' => $k]) ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="leftSelectDisplay">
                        <span><img src="/img/popup/mail.svg" alt=""></span>
                        <span>Выберите категорию товара</span>
                    </div>
                    <div class="centerSelect scroll-1">
                        <ul class="list-unstyled" style="display: none"></ul>
                    </div>
                    <div class="rightSelect scroll-1">
                        <ul class="list-unstyled" style="display: none"></ul>
                    </div>
                </div>
            </div>


            <!--        HOVER  НА ІНПУТ ГДЕ-->
            <div class="col-sm-5  whenBlock" style="display: none">
                <ul class="list-unstyled scrollbar1">
                    <?php foreach ($when as $k => $i): ?>
                        <?= Html::beginTag('li', ['data-id' => $k]) ?>
                        <svg class="sityImg" width="14px" height="14px" viewBox="0 0 14 14" version="1.1">
                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g id="sityImg" transform="translate(-539.000000, -140.000000)" fill-rule="nonzero">
                                    <g id="product" transform="translate(519.000000, 71.000000)">
                                        <path d="M32.8122803,75.1122805 L31.8719295,75.1122805 C31.6368418,72.6438595 29.6385962,70.7631578 27.2877191,70.5280701 L27.2877191,69.5877193 C27.2877191,69.2350877 27.0526314,69 26.6999998,69 C26.3473682,69 26.1122805,69.2350877 26.1122805,69.5877193 L26.1122805,70.5280701 C23.6438595,70.7631578 21.7631578,72.7614034 21.5280701,75.1122805 L20.5877193,75.1122805 C20.2350877,75.1122805 20,75.3473682 20,75.6999998 C20,76.0526314 20.2350877,76.2877191 20.5877193,76.2877191 L21.5280701,76.2877191 C21.7631578,78.7561401 23.7614034,80.6368418 26.1122805,80.8719295 L26.1122805,81.8122803 C26.1122805,82.1649119 26.3473682,82.3999996 26.6999998,82.3999996 C27.0526314,82.3999996 27.2877191,82.1649119 27.2877191,81.8122803 L27.2877191,80.8719295 C29.7561401,80.6368418 31.6368418,78.6385962 31.8719295,76.2877191 L32.8122803,76.2877191 C33.1649119,76.2877191 33.3999996,76.0526314 33.3999996,75.6999998 C33.3999996,75.3473682 33.1649119,75.1122805 32.8122803,75.1122805 Z M27.2877191,79.6964909 L27.2877191,78.8736839 C27.2877191,78.5210524 27.0526314,78.2859646 26.6999998,78.2859646 C26.3473682,78.2859646 26.1122805,78.5210524 26.1122805,78.8736839 L26.1122805,79.6964909 C24.3491227,79.4614032 22.9385964,78.0508769 22.7035087,76.2877191 L23.5263157,76.2877191 C23.8789473,76.2877191 24.114035,76.0526314 24.114035,75.6999998 C24.114035,75.3473682 23.8789473,75.1122805 23.5263157,75.1122805 L22.7035087,75.1122805 C22.9385964,73.3491227 24.3491227,71.9385964 26.1122805,71.7035087 L26.1122805,72.5263157 C26.1122805,72.8789473 26.3473682,73.114035 26.6999998,73.114035 C27.0526314,73.114035 27.2877191,72.8789473 27.2877191,72.5263157 L27.2877191,71.7035087 C29.0508769,71.9385964 30.4614032,73.3491227 30.6964909,75.1122805 L29.8736839,75.1122805 C29.5210524,75.1122805 29.2859646,75.3473682 29.2859646,75.6999998 C29.2859646,76.0526314 29.5210524,76.2877191 29.8736839,76.2877191 L30.6964909,76.2877191 C30.4614032,78.0508769 29.0508769,79.4614032 27.2877191,79.6964909 Z"
                                              id="Shape-Copy"></path>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        <?= $i['name'] ?>

                        <?= Html::endTag('li') ?>
                    <?php endforeach; ?>
                </ul>
            </div>


            <!--        БЛОК АВТОРИЗАЦІЇ\РЕЄСТРАЦІЇ КЛІЄНТА         -->
            <?php if (\Yii::$app->user->isGuest): ?>
                <div class="regBlock">
                    <button class="login btn" data-toggle="modal" data-target="#Modal1">Войти</button>

                    <button class="reg btn btn-danger" data-toggle="modal" data-target="#Modal1">Регистрация
                    </button>
                </div>
            <?php else: ?>
                <div class="manHeader text-right ">

                    <div class="dropdown dropdownAccount">

                        <button class="btn  dropdown-toggle btnAccount" type="button" id="dropdownMenu1"
                                data-toggle="dropdown" aria-expanded="true">
                            <div class="imgAccount">
                                <div>
                                    <svg width="24px" height="30px" viewBox="0 0 24 30">
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"
                                           stroke-linecap="round"
                                           stroke-linejoin="round">
                                            <g id="+UI8.7----По-заданным-параметрам-ничего-не-нашлось"
                                               transform="translate(-1356.000000, -29.000000)"
                                               stroke="#A0A0A0">
                                                <g id="MENU" transform="translate(1.000000, 0.000000)">
                                                    <g id="Group-3" transform="translate(1346.000000, 20.000000)">
                                                        <g id="Group-24">
                                                            <g id="ava">
                                                                <g id="Group">
                                                                    <g id="Group-2"
                                                                       transform="translate(9.000000, 10.000000)">
                                                                        <path d="M14.7775,16.1 L16.5025,16.1 C19.8375,16.1 22.54,18.8025 22.54,22.1375 L22.54,26.45"
                                                                              id="Shape"></path>
                                                                        <path d="M0.5175,26.5075 L0.5175,22.195 C0.5175,18.86 3.22,16.1575 6.555,16.1575 L8.28,16.1575"
                                                                              id="Shape"></path>
                                                                        <path d="M14.5475,15.295 C14.72,15.6975 14.835,16.1 14.835,16.56 C14.835,18.3425 13.3975,19.78 11.615,19.78 C9.8325,19.78 8.395,18.3425 8.395,16.56 C8.395,16.1575 8.4525,15.6975 8.625,15.3525"
                                                                              id="Shape"></path>
                                                                        <path d="M17.135,8.6825 C17.135,12.075 15.0075,16.3875 11.615,16.3875 C8.2225,16.3875 6.095,12.075 6.095,8.6825 C6.095,5.29 6.3825,0.345 11.615,0.345 C16.79,0.345 17.135,5.29 17.135,8.6825 Z"
                                                                              id="Shape"></path>
                                                                        <path d="M4.4275,28.52 L4.4275,24.265"
                                                                              id="Shape"></path>
                                                                        <path d="M18.6875,28.52 L18.6875,24.265"
                                                                              id="Shape"></path>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </div>

                            </div>
                            <span class="droppMenu">
                                    <svg class="droppAcc" width="14px" height="14px" viewBox="0 0 8 14">
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"
                                           stroke-linecap="round" stroke-linejoin="round">
        <g id="droppAcc" transform="translate(-1207.000000, -591.000000)" stroke-width="2">
            <g id="Group-11" transform="translate(0.000000, 579.000000)">
                <g id="Recommended">
                    <g id="Lenta" transform="translate(210.000000, 0.000000)">
                        <g id="arrows" transform="translate(927.000000, 1.000000)">
                            <g id="Group-37" transform="translate(56.000000, 0.000000)">
                                <polyline id="arrow_right_black"
                                          transform="translate(17.935389, 17.699575) scale(-1, 1) rotate(90.000000) translate(-17.935389, -17.699575) "
                                          points="15 23.3991501 20.8707778 17.5283723 15.3424055 12"></polyline>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right listAccount" role="menu"
                            aria-labelledby="dropdownMenu1">
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="/profile/edit">
                                    <svg class="imgLiAccont" width="16px" height="21px" viewBox="0 0 16 21"
                                         version="1.1">

                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                           fill-rule="evenodd">
                                            <g class="ui" transform="translate(-1206.000000, -103.000000)"
                                               fill-rule="nonzero" fill="#AFAFAF">
                                                <g id="product" transform="translate(1170.000000, 73.000000)">
                                                    <path d="M46.5,39.5 C47.6,38.5 48.3,36.9 48.3,35.4 C48.3,32.9 46.3,30 43.7,30 C41.1,30 39.1,32.9 39.1,35.4 C39.1,36.9 39.8,38.4 40.9,39.5 C36.9,40.3 36,42.9 36,45 L36,50.4 C36,50.5 36.1,50.7 36.1,50.8 C36.1,50.9 36.3,50.9 36.5,50.9 L50.9,50.9 C51.2,50.9 51.4,50.7 51.4,50.4 L51.4,45 C51.3,42.9 50.4,40.3 46.5,39.5 Z M50.3,49.9 L36.9,49.9 L36.9,45 C36.9,42.2 38.6,40.7 42,40.3 C42.2,40.3 42.4,40.1 42.4,39.9 C42.4,39.7 42.3,39.5 42.2,39.4 C40.9,38.7 40,37 40,35.4 C40,33.4 41.6,31 43.6,31 C45.6,31 47.2,33.4 47.2,35.4 C47.2,37 46.3,38.6 45,39.4 C44.8,39.5 44.7,39.7 44.8,39.9 C44.9,40.1 45,40.3 45.2,40.3 C48.7,40.7 50.3,42.2 50.3,45 L50.3,49.9 L50.3,49.9 Z"
                                                          id="profile_icon"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    Профиль</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="/profile/setings">
                                    <svg class="imgLiAccont" width="19px" height="19px" viewBox="0 0 19 19"
                                         version="1.1">

                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                           fill-rule="evenodd">
                                            <g class="ui" transform="translate(-1205.000000, -155.000000)"
                                               fill-rule="nonzero" fill="#AFAFAF">
                                                <g id="product" transform="translate(1170.000000, 73.000000)">
                                                    <g id="settings_icon"
                                                       transform="translate(35.000000, 82.000000)">
                                                        <path d="M17.1,7 L16.7,7 C16.6,6.6 16.4,6.3 16.3,5.9 L16.6,5.6 C16.9,5.3 17.1,4.8 17.1,4.3 C17.1,3.8 16.9,3.4 16.6,3 L16,2.4 C15.3,1.7 14.1,1.7 13.4,2.4 L13.1,2.7 C12.8,2.5 12.4,2.4 12,2.3 L12,1.9 C12,0.9 11.2,0.1 10.2,0.1 L9.4,0.1 C8.4,0.1 7.6,0.9 7.6,1.9 L7.6,2.3 C7.2,2.4 6.9,2.6 6.5,2.7 L6.2,2.4 C5.5,1.7 4.3,1.7 3.6,2.4 L3,3 C2.3,3.7 2.3,4.9 3,5.6 L3.3,5.9 C3.1,6.2 3,6.6 2.9,7 L2.5,7 C1.5,7 0.7,7.8 0.7,8.8 L0.7,9.6 C0.7,10.6 1.5,11.4 2.5,11.4 L2.9,11.4 C3,11.8 3.2,12.1 3.3,12.5 L3,12.8 C2.7,13.1 2.5,13.6 2.5,14.1 C2.5,14.6 2.7,15 3,15.4 L3.6,16 C4.3,16.7 5.5,16.7 6.2,16 L6.5,15.7 C6.8,15.9 7.2,16 7.6,16.1 L7.6,16.5 C7.6,17.5 8.4,18.3 9.4,18.3 L10.2,18.3 C11.2,18.3 12,17.5 12,16.5 L12,16.1 C12.4,16 12.7,15.8 13.1,15.7 L13.4,16 C14.1,16.7 15.3,16.7 16,16 L16.6,15.4 C16.9,15.1 17.1,14.6 17.1,14.1 C17.1,13.6 16.9,13.2 16.6,12.8 L16.3,12.5 C16.5,12.2 16.6,11.8 16.7,11.4 L17.1,11.4 C18.1,11.4 18.9,10.6 18.9,9.6 L18.9,8.8 C18.9,7.8 18.1,7 17.1,7 Z M17.9,9.6 C17.9,10 17.5,10.4 17.1,10.4 L16.3,10.4 C16.1,10.4 15.9,10.6 15.8,10.8 C15.7,11.3 15.4,11.8 15.2,12.3 C15.1,12.5 15.1,12.7 15.3,12.9 L15.9,13.5 C16.1,13.7 16.1,13.9 16.1,14.1 C16.1,14.3 16,14.5 15.9,14.7 L15.3,15.3 C15,15.6 14.5,15.6 14.2,15.3 L13.6,14.7 C13.4,14.5 13.2,14.5 13,14.6 C12.5,14.9 12,15.1 11.5,15.2 C11.3,15.3 11.1,15.5 11.1,15.7 L11.1,16.5 C11.1,16.9 10.7,17.3 10.3,17.3 L9.5,17.3 C9.1,17.3 8.7,16.9 8.7,16.5 L8.7,15.7 C8.7,15.5 8.5,15.3 8.3,15.2 C7.8,15.1 7.3,14.8 6.8,14.6 C6.7,14.6 6.6,14.5 6.5,14.5 C6.4,14.5 6.2,14.6 6.1,14.6 L5.5,15.2 C5.2,15.5 4.7,15.5 4.4,15.2 L3.8,14.6 C3.6,14.4 3.6,14.2 3.6,14 C3.6,13.8 3.7,13.6 3.8,13.4 L4.4,12.8 C4.6,12.6 4.6,12.4 4.5,12.2 C4.2,11.7 4,11.2 3.9,10.7 C3.8,10.5 3.6,10.3 3.4,10.3 L2.6,10.3 C2.2,10.3 1.8,9.9 1.8,9.5 L1.8,8.7 C1.8,8.3 2.2,7.9 2.6,7.9 L3.4,7.9 C3.6,7.9 3.8,7.7 3.9,7.5 C4,7 4.3,6.4 4.5,6 C4.6,5.8 4.6,5.6 4.4,5.4 L3.8,4.8 C3.5,4.5 3.5,4 3.8,3.7 L4.4,3.1 C4.7,2.8 5.2,2.8 5.5,3.1 L6.1,3.7 C6.3,3.9 6.5,3.9 6.7,3.8 C7.2,3.5 7.7,3.3 8.2,3.2 C8.4,3.1 8.6,2.9 8.6,2.7 L8.6,1.9 C8.6,1.5 9,1.1 9.4,1.1 L10.2,1.1 C10.6,1.1 11,1.5 11,1.9 L11,2.7 C11,2.9 11.2,3.1 11.4,3.2 C11.9,3.3 12.4,3.6 12.9,3.8 C13.1,3.9 13.3,3.9 13.5,3.7 L14.1,3.1 C14.4,2.8 14.9,2.8 15.2,3.1 L15.8,3.7 C16.1,4 16.1,4.5 15.8,4.8 L15.2,5.4 C15,5.6 15,5.8 15.1,6 C15.4,6.5 15.6,7 15.7,7.5 C15.8,7.7 16,7.9 16.2,7.9 L17,7.9 C17.4,7.9 17.8,8.3 17.8,8.7 L17.8,9.6 L17.9,9.6 Z"
                                                              id="Shape"></path>
                                                        <path d="M9.8,6.1 C8.1,6.1 6.7,7.5 6.7,9.2 C6.7,10.9 8.1,12.3 9.8,12.3 C11.5,12.3 12.9,10.9 12.9,9.2 C12.9,7.5 11.5,6.1 9.8,6.1 Z M9.8,11.3 C8.7,11.3 7.7,10.4 7.7,9.2 C7.7,8 8.6,7.1 9.8,7.1 C11,7.1 11.9,8 11.9,9.2 C11.9,10.4 10.9,11.3 9.8,11.3 Z"
                                                              id="Shape"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    Настройки аккаунта</a>
                            </li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="/profile/my-products">
                                    <svg class="imgLiAccont" width="19px" height="17px" viewBox="0 0 19 17"
                                         version="1.1">

                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                           fill-rule="evenodd">
                                            <g class="ui" transform="translate(-1205.000000, -204.000000)"
                                               fill-rule="nonzero" fill="#AFAFAF">
                                                <g id="product" transform="translate(1170.000000, 73.000000)">
                                                    <g id="Group" transform="translate(34.000000, 131.000000)">
                                                        <path d="M19.6,1.6 C19.5,1.5 19.4,1.4 19.2,1.4 L5.1,1.4 L5.1,0.5 C5.1,0.2 4.9,0 4.6,0 L1.5,0 C1.2,0 1,0.2 1,0.5 C1,0.8 1.2,1 1.5,1 L4.1,1 L4.1,9.7 C4.1,11.1 4.9,12.3 6.1,12.9 C5.7,13.3 5.5,13.8 5.5,14.4 C5.5,15.6 6.4,16.5 7.6,16.5 C8.8,16.5 9.7,15.6 9.7,14.4 C9.7,14 9.6,13.6 9.4,13.3 L14.7,13.3 C14.5,13.6 14.4,14 14.4,14.4 C14.4,15.6 15.3,16.5 16.5,16.5 C17.7,16.5 18.6,15.6 18.6,14.4 C18.6,13.2 17.7,12.3 16.5,12.3 L7.6,12.3 C6.4,12.3 5.4,11.4 5.1,10.3 L15.9,10.3 C17.3,10.3 18.3,9.7 18.9,8.4 C19.2,7.7 19.4,6.8 19.5,5.8 L19.7,2 C19.8,1.9 19.7,1.7 19.6,1.6 Z M8.8,14.5 C8.8,15.1 8.3,15.6 7.7,15.6 C7.1,15.6 6.6,15.1 6.6,14.5 C6.6,13.9 7.1,13.4 7.7,13.4 C8.3,13.4 8.8,13.8 8.8,14.5 Z M17.7,14.5 C17.7,15.1 17.2,15.6 16.6,15.6 C16,15.6 15.5,15.1 15.5,14.5 C15.5,13.9 16,13.4 16.6,13.4 C17.2,13.4 17.7,13.8 17.7,14.5 Z M18.6,5.7 C18.6,6.6 18.4,7.3 18.1,7.9 C17.7,8.8 17,9.3 16,9.3 L5.1,9.3 L5.1,2.5 L18.7,2.5 L18.6,5.7 Z"
                                                              id="products_icon"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    Ваши товары</a>
                            </li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="/profile/deal">
                                    <svg class="imgLiAccont" width="18px" height="19px" viewBox="0 0 18 19"
                                         version="1.1">

                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                           fill-rule="evenodd">
                                            <g class="ui" transform="translate(-1205.000000, -250.000000)"
                                               fill-rule="nonzero" fill="#AFAFAF">
                                                <g id="product" transform="translate(1170.000000, 73.000000)">
                                                    <g id="Group" transform="translate(34.000000, 131.000000)">
                                                        <g id="Group-14" transform="translate(1.000000, 46.000000)">
                                                            <path d="M14.9,15.1 C14.9,15.1 14.9,15 14.9,15.1 C14.9,15 14.9,15 14.8,14.9 C14.8,14.9 14.8,14.8 14.7,14.8 L14.7,14.8 L14.7,14.8 C14.7,14.8 14.6,14.8 14.6,14.7 L14.5,14.7 C14.5,14.7 14.5,14.7 14.4,14.7 L11.3,14.3 C11,14.3 10.8,14.5 10.7,14.7 C10.7,15 10.9,15.2 11.1,15.3 L13.1,15.6 C11.8,16.5 10.3,17.1 8.7,17.1 C6.7,17.1 4.9,16.3 3.5,14.9 C1.2,12.6 0.7,9 2.2,6.2 C2.3,6 2.2,5.7 2,5.5 C1.8,5.4 1.5,5.5 1.3,5.7 C-0.4,8.9 0.2,13 2.8,15.6 C4.4,17.2 6.5,18.1 8.7,18.1 C10.5,18.1 12.2,17.5 13.6,16.5 L13.4,18.3 C13.4,18.6 13.6,18.8 13.8,18.9 L13.9,18.9 C14.1,18.9 14.4,18.7 14.4,18.5 L14.8,15.4 C14.9,15.2 14.9,15.2 14.9,15.1 C14.9,15.2 14.9,15.1 14.9,15.1 Z"
                                                                  id="Shape"></path>
                                                            <path d="M14.4239157,3.28513728 C12.4572823,1.58422617 10.9300839,1.17481512 8.7300839,1.17481512 C6.9300839,1.17481512 5.3,1.8 3.9,2.8 L4.1,1 C4.1,0.7 3.9,0.5 3.7,0.4 C3.4,0.4 3.2,0.6 3.1,0.8 L2.7,3.9 L2.7,4 L2.7,4.1 L2.7,4.2 C2.7,4.2 2.7,4.3 2.8,4.3 L2.9,4.3 L3,4.3 L3.1,4.3 L6.2,4.7 L6.3,4.7 C6.5,4.7 6.8,4.5 6.8,4.3 C6.8,4 6.6,3.8 6.4,3.7 L4.4,3.4 C7.3,1.3 11.4,1.5 14,4.1 C16.3,6.4 16.8,10 15.3,12.8 C15.2,13 15.3,13.3 15.5,13.5 C15.6,13.5 15.7,13.6 15.7,13.6 C15.9,13.6 16.1,13.5 16.1,13.3 C17.9,10.3 17.3152539,5.80994282 14.4239157,3.28513728 Z"
                                                                  id="Shape"></path>
                                                            <path d="M8.8,6.5 C8.5,6.5 8.3,6.7 8.3,7 L8.3,10.3 C8.3,10.6 8.5,10.8 8.8,10.8 L11.2,10.8 C11.5,10.8 11.7,10.6 11.7,10.3 C11.7,10 11.5,9.8 11.2,9.8 L9.3,9.8 L9.3,7 C9.3,6.7 9,6.5 8.8,6.5 Z"
                                                                  id="Shape"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    История
                                    сделок</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="/profile/casa">
                                    <svg class="imgLiAccont" width="19px" height="21px" viewBox="0 0 19 21"
                                         version="1.1">

                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                           fill-rule="evenodd">
                                            <g class="ui" transform="translate(-1204.000000, -297.000000)"
                                               fill-rule="nonzero" fill="#AFAFAF">
                                                <g id="product" transform="translate(1170.000000, 73.000000)">
                                                    <g id="Group" transform="translate(34.000000, 131.000000)">
                                                        <g id="balance_icon"
                                                           transform="translate(0.000000, 93.000000)">
                                                            <path d="M17.5,13.9 L16.8,13.9 L16.8,4.8 C16.8,4.5 16.6,4.3 16.3,4.3 L13.9,4.3 C13.6,4.3 13.4,4.5 13.4,4.8 C13.4,5.1 13.6,5.3 13.9,5.3 L15.8,5.3 L15.8,13.9 L2.4,13.9 L2.4,5.3 L4.3,5.3 C4.6,5.3 4.8,5.1 4.8,4.8 C4.8,4.5 4.6,4.3 4.3,4.3 L1.9,4.3 C1.6,4.3 1.4,4.5 1.4,4.8 L1.4,13.9 L0.7,13.9 C0.4,13.9 0.2,14.1 0.2,14.4 L0.2,20.1 C0.2,20.4 0.4,20.6 0.7,20.6 L17.6,20.6 C17.9,20.6 18.1,20.4 18.1,20.1 L18.1,14.4 C18,14.1 17.8,13.9 17.5,13.9 Z M17,19.6 L1.1,19.6 L1.1,14.9 L17,14.9 L17,19.6 Z"
                                                                  id="Shape"></path>
                                                            <circle id="Oval" cx="9.1" cy="17.2" r="1"></circle>
                                                            <path d="M4.4,8.4 C4.4,8.7 4.6,8.9 4.9,8.9 L6.3,8.9 L11.9,8.9 L13.3,8.9 C13.6,8.9 13.8,8.7 13.8,8.4 C13.8,8.1 13.6,7.9 13.3,7.9 L12.4,7.9 L12.4,0.9 C12.4,0.6 12.2,0.4 11.9,0.4 L6.3,0.4 C6,0.4 5.8,0.6 5.8,0.9 L5.8,7.9 L4.9,7.9 C4.6,7.9 4.4,8.1 4.4,8.4 Z M6.8,1.4 L11.4,1.4 L11.4,7.9 L6.8,7.9 L6.8,1.4 Z"
                                                                  id="Shape"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    Касса</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                    <svg class="imgLiAccont" width="16px" height="25px" viewBox="0 0 16 25"
                                         version="1.1">

                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                           fill-rule="evenodd">
                                            <g class="ui" transform="translate(-1205.000000, -344.000000)"
                                               fill-rule="nonzero" fill="#AFAFAF">
                                                <g id="product" transform="translate(1170.000000, 73.000000)">
                                                    <g id="Group" transform="translate(34.000000, 131.000000)">
                                                        <path d="M11.9,149.6 C12.9,148.4 13.3,146.7 13.3,145.4 C13.3,142.9 11.3,140 8.7,140 C6.1,140 4.1,142.9 4.1,145.4 C4.1,146.7 4.5,148.5 5.5,149.6 C3.3,150 1,151.2 1,154.9 L1,160.3 C1,160.6 1.2,160.8 1.5,160.8 L4.5,160.8 C4.8,160.8 5,160.6 5,160.3 C5,160 4.8,159.8 4.5,159.8 L2,159.8 L2,154.9 C2,152.2 3.3,150.9 6.2,150.5 L6.2,151.1 C6.2,151.7 6.4,152.2 6.8,152.7 L5.5,161 C5.5,161.2 5.5,161.3 5.6,161.4 L8.3,164.1 C8.4,164.2 8.5,164.2 8.7,164.2 C8.9,164.2 9,164.2 9.1,164.1 L11.8,161.4 C11.9,161.3 12,161.1 11.9,161 L10.6,152.7 C11,152.3 11.2,151.7 11.2,151.1 L11.2,150.5 C14.2,150.9 15.4,152.2 15.4,154.9 L15.4,159.8 L13,159.8 C12.7,159.8 12.5,160 12.5,160.3 C12.5,160.6 12.7,160.8 13,160.8 L15.9,160.8 C16.2,160.8 16.4,160.6 16.4,160.3 L16.4,154.9 C16.4,151.2 14.1,150 11.9,149.6 Z M5.1,145.4 C5.1,143.4 6.7,141 8.7,141 C10.7,141 12.3,143.4 12.3,145.4 C12.3,146.9 11.7,148.8 10.4,149.5 C9.3,150 8,150 6.9,149.5 C5.7,148.8 5.1,146.9 5.1,145.4 Z M7.2,151.2 L7.2,150.7 C8.2,151 9.2,151 10.2,150.7 L10.2,151.2 C10.2,151.9 9.6,152.5 8.9,152.5 L8.6,152.5 C7.8,152.5 7.2,151.9 7.2,151.2 Z M8.7,163 L6.6,160.9 L7.8,153.3 C8.1,153.4 8.3,153.5 8.6,153.5 L8.9,153.5 C9.2,153.5 9.5,153.4 9.7,153.3 L10.9,160.9 L8.7,163 Z"
                                                              id="business_account"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    Бизнес аккаунт</a>
                            </li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="/profile/actions">

                                    <svg class="imgLiAccont" width="16px" height="18px" viewBox="0 0 16 18"
                                         version="1.1">

                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                           fill-rule="evenodd">
                                            <g class="ui" transform="translate(-1207.000000, -394.000000)"
                                               fill-rule="nonzero">
                                                <g id="product" transform="translate(1170.000000, 73.000000)">
                                                    <g id="Group" transform="translate(34.000000, 131.000000)">
                                                        <path d="M15,196.8 L18.8,190.8 C18.9,190.6 18.9,190.5 18.8,190.3 C18.7,190.1 18.5,190 18.4,190 L3.5,190 C3.2,190 3,190.2 3,190.5 L3,207.4 C3,207.7 3.2,207.9 3.5,207.9 C3.8,207.9 4,207.7 4,207.4 L4,203.4 L18.4,203.4 C18.6,203.4 18.8,203.3 18.8,203.1 C18.9,202.9 18.9,202.7 18.8,202.6 L15,196.8 Z M3.9,202.5 L3.9,191.1 L17.4,191.1 L13.9,196.6 C13.8,196.8 13.8,197 13.9,197.1 L17.4,202.6 L3.9,202.6 L3.9,202.5 Z"
                                                              id="actions_icon"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    Акции</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="profile/feedback">
                                    <svg class="imgLiAccont" width="19px" height="18px" viewBox="0 0 19 18"
                                         version="1.1">

                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none"
                                           fill-rule="evenodd">
                                            <g class="ui" transform="translate(-1206.000000, -444.000000)"
                                               fill-rule="nonzero">
                                                <g id="product" transform="translate(1170.000000, 73.000000)">
                                                    <g id="Group" transform="translate(34.000000, 131.000000)">
                                                        <g id="reviews_icon"
                                                           transform="translate(2.000000, 240.000000)">
                                                            <path d="M17.7,0.1 L1.9,0.1 C1.2,0.1 0.6,0.7 0.6,1.4 L0.6,13 C0.6,13.7 1.2,14.3 1.9,14.3 L3.7,14.3 L3.7,16.6 C3.7,16.8 3.8,17 4,17.1 L4.2,17.1 C4.3,17.1 4.4,17.1 4.5,17 L7.4,14.3 L17.6,14.3 C18.3,14.3 18.9,13.7 18.9,13 L18.9,1.4 C18.9,0.7 18.4,0.1 17.7,0.1 Z M17.9,12.9 C17.9,13 17.8,13.2 17.6,13.2 L7.3,13.2 C7.2,13.2 7.1,13.2 7,13.3 L4.7,15.4 L4.7,13.7 C4.7,13.4 4.5,13.2 4.2,13.2 L1.9,13.2 C1.8,13.2 1.6,13.1 1.6,12.9 L1.6,1.3 C1.6,1.2 1.7,1 1.9,1 L17.7,1 C17.8,1 18,1.1 18,1.3 L18,12.9 L17.9,12.9 Z"
                                                                  id="Shape"></path>
                                                            <path d="M13.6,4.9 L6,4.9 C5.7,4.9 5.5,5.1 5.5,5.4 C5.5,5.7 5.7,5.9 6,5.9 L13.6,5.9 C13.9,5.9 14.1,5.7 14.1,5.4 C14.1,5.1 13.9,4.9 13.6,4.9 Z"
                                                                  id="Shape"></path>
                                                            <path d="M13.6,8.4 L6,8.4 C5.7,8.4 5.5,8.6 5.5,8.9 C5.5,9.2 5.7,9.4 6,9.4 L13.6,9.4 C13.9,9.4 14.1,9.2 14.1,8.9 C14.1,8.6 13.9,8.4 13.6,8.4 Z"
                                                                  id="Shape"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    Отзывы</a></li>

                            <li class="liBtnExit"><a class="btn btnAccountExit text-center" role="menuitem"
                                                     tabindex="-1" href="/auth/logout">Выйти</a></li>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>

            <!--                КІНЕЦЬ РЕЄСТРАЦІЇ ТОВАРУ        -->
        </div>
    </div>

<?= $this->render('headerMobile',
    ['searchForm' => $searchForm, 'when' => $when, 'listPopup' => $listPopup]) ?>

    <!--            ПОПАП ПРИ КЛІКУ НА ЗЕЛЕНУ КНОПКУ ТОВАРУ     -->
    <div class="row popup">
        <div class="container">
            <div class="row body">
                <div class="col-md-12 blockPopup">

                    <?= Html::beginForm(['/product/create-new'], 'post', [
                        'id' => 'create_product-form',
                    ]) ?>
                    <?= Html::activeHiddenInput(new \frontend\models\ProductForm(), 'category') ?>
                    <?= Html::submitButton('Next', ['class' => 'hidden']) ?>
                    <?= Html::endForm() ?>

                    <div class="popupTitle">

                        <span class="hidden stepBack">

                            <svg width="15px" height="25px" viewBox="0 0 15 25" version="1.1">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="UI1.5.2-Дропдаун-при-фокусе-на-категорию-1">
            <g id="back---icon---grey">


                <polyline id="Path-3-Copy-2"
                          stroke="#3C3D3E"
                          stroke-width="2"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          transform="translate(25.978098, 35.500000) rotate(-180.000000) translate(-6.978098, -12.500000) "
                          points="21.0854186 45 30.8707778 35.2146408 21.656137 26"></polyline>
            </g>
        </g>
    </g>
</svg>

                        </span>

                        <h2 class="text-left titlePopup hidden-xs">Что вы хотите разместить?</h2>
                        <div class="titleXs hidden-lg hidden-md hidden-sm  text-center">
                            <h2 class="titlePopup ">Разместить объявление</h2>
                            <span class="countStep">Шаг 1 из 3</span>

                        </div>

                        <span class="text-right btnCloce">

                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                              <path fill="#FFF" fill-rule="evenodd" stroke="#FFF" stroke-linecap="round"
                                    d="M7,5.58578644 L1.87377345,0.459559885 C1.48324916,0.0690355937 0.850084177,0.0690355937 0.459559885,0.459559885 L0.459559885,1.87377345 L5.58578644,7 L0.341113532,12.2446729 C-0.0494107594,12.6351972 -0.0494107594,13.2683622 0.341113532,13.6588865 C0.731637824,14.0494108 1.3648028,14.0494108 1.75532709,13.6588865 L7,8.41421356 L12.2446729,13.6588865 C12.6351972,14.0494108 13.2683622,14.0494108 13.6588865,13.6588865 C14.0494108,13.2683622 14.0494108,12.6351972 13.6588865,12.2446729 L8.41421356,7 L13.5404401,1.87377345 C13.9309644,1.48324916 13.9309644,0.850084177 13.5404401,0.459559885 L12.1262266,0.459559885 L7,5.58578644 Z"
                                    transform="translate(1 1)"/>
                            </svg>


                            <svg class="hidden-lg hidden-md hidden-sm" width="22px" height="21px" viewBox="0 0 22 21">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
        <g id="UI1.6.2-Модал-категориии" transform="translate(-373.000000, -25.000000)" stroke="#3C3D3E"
           stroke-width="2">
            <g id="Group-4">
                <g id="close---icon---grey" transform="translate(374.000000, 26.000000)">
                    <path d="M0.318645628,18.83413 L19.9895953,0.203126916" id="Line"></path>
                    <path d="M0.318645628,18.83413 L19.9895953,0.203126916" id="Line-Copy-2"
                          transform="translate(10.154120, 9.518628) scale(-1, 1) translate(-10.154120, -9.518628) "></path>
                </g>
            </g>
        </g>
    </g>
</svg>
                        </span>
                    </div>

                    <div class="menuPopup hidden-xs">
                        <div class="row">
                            <div class="col-md-12">
                                <a href="#" class="allCategory">Все категории</a>
                                <a href="#" class="popular">Популярные</a>
                            </div>
                        </div>
                    </div>

                    <div class="categoryPopup">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12 leftCategory scroll-1">
                                <h3 class="titleLeftCategory"><a href="#">Выберите категорию:</a></h3>
                                <div class="row ">

                                    <div class="wrapCategory">
                                        <?php foreach ($listPopup as $k => $item): ?>
                                            <a href="#" class="thumbnail oneCategory leftSelect " data-id="<?= $k ?>">
                                                <span class="checkBoxCategory hidden-xs"></span>
                                                <img src="<?= $item['img'] ?>" alt="<?= $item['name'] ?>"/>
                                                <span class="borderCateg hidden-xs"></span>
                                                <h4 class="text-center"><?= $item['name'] ?></h4>
                                            </a>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="rightCategoru hidden-xs col-sm-6 col-xs-12">

                                <div class="voidBlock hidden-xs">
                                    <img src="/img/popup/mail.svg" alt="">
                                    <p class="textRight text-center">Для размещения объявления выберите категорию, к
                                        которой относится ваш товар или услуга.</p>
                                </div>

                                <div class="centerSelect scroll-1">
                                    <p class="hiddenTitleCateg" style="display: none">Выберите подкатегорию:</p>
                                    <ul class="list-unstyled"></ul>
                                </div>

                                <div class="rightSelect hidden-xs scroll-1">
                                    <p class="hiddenTitleSubCateg" style="display: none">Выберите товар/услугу:</p>
                                    <ul class="list-unstyled"></ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bottomPopup">
                        <div class="row">
                            <div class="col-md-12 hidden-xs">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--    повідомлення з чату  -->
    <div class="modal fade" id="masenges" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <svg class="imgRed" width="12px" height="12px" viewBox="0 0 12 12" version="1.1">
                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"
                               stroke-linecap="round">
                                <g id="imgRed" transform="translate(-716.000000, -1734.000000)" stroke="#E35F46"
                                   fill="#E35F46">
                                    <g id="Group-15-Copy-2" transform="translate(280.000000, 1392.000000)">
                                        <g id="Group-4" transform="translate(19.000000, 22.000000)">
                                            <g id="Group-19-Copy-9" transform="translate(207.000000, 283.000000)">
                                                <g id="Group-6-Copy" transform="translate(114.000000, 31.000000)">
                                                    <path d="M102,10.8976492 L98.3130114,7.2106606 C98.0321306,6.9297798 97.5767328,6.9297798 97.295852,7.2106606 L97.295852,8.22781997 L100.982841,11.9148086 L97.2106606,15.6869886 C96.9297798,15.9678694 96.9297798,16.4232672 97.2106606,16.704148 C97.4915414,16.9850288 97.9469392,16.9850288 98.22782,16.704148 L102,12.931968 L105.77218,16.704148 C106.053061,16.9850288 106.508459,16.9850288 106.789339,16.704148 C107.07022,16.4232672 107.07022,15.9678694 106.789339,15.6869886 L103.017159,11.9148086 L106.704148,8.22781997 C106.985029,7.94693916 106.985029,7.4915414 106.704148,7.2106606 L105.686989,7.2106606 L102,10.8976492 Z"
                                                          id="Combined-Shape-Copy"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">У вас новое уведомление</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрити</button>
                </div>
            </div>
        </div>
    </div>


    <!--    модал для реєстрації \ входу-->
    <div class="modal fade regModal" id="Modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <button type="button" class="btn" id="login">Войти</button>
                    <button type="button" class="btn" id="reg">Регистрация</button>
                </div>
                <div class="modal-body">

                    <div class="loginBlock">
                        <?= $this->render('//auth/auth-form', [
                            'model' => new \frontend\modules\user\models\LoginForm(),
                            'v' => 'desktop',
                        ]) ?>
                    </div>

                    <div class="registrationBlock" style="display: none">
                        <?= $this->render('//auth/register-form', [
                            'model' => new \frontend\modules\user\models\SignupForm(),
                            'v' => 'desktop',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--відновлення паролю-->
    <div class="modal fade" id="generatePass" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Закрити</span></button>

                </div>
                <div class="modal-body">

                    <?= $this->render('//auth/restore-pass-form', [
                        'model' => new \frontend\modules\user\models\PasswordResetRequestForm(),
                    ]) ?>
                </div>

            </div>
        </div>
    </div>


<?php

$ja = \yii\helpers\Json::encode($a);
$mw = \yii\helpers\Json::encode($when);
$listCategoryPopup = \yii\helpers\Json::encode($listPopup);

$this->registerJs(<<<JS
var categoryNestedList = {$ja};
var categoryWhenList = {$mw};
var categoryNestedList1 = {$listCategoryPopup};



JS
    , \yii\web\View::POS_BEGIN);


//
//$this->registerJsFile('js/jquery-ui-1.7.2.custom.min.js', [
//        'position' => \yii\web\View::POS_END,
//    'depends'  => [
//        'yii\web\JqueryAsset',
//    ],
//]);
//$this->registerJsFile('/js/', [
//        'position' => \yii\web\View::POS_END,
//    'depends'  => [
//        'yii\web\JqueryAsset',
//    ],
//]);
//$this->registerCssFile('/css/');

$this->registerCss(<<<CSS
.pac-container{
    margin-top: 23px;
    padding-top: 18px;
    font-family: inherit !important;
}
.pac-container .pac-icon{
    display: none;
}
.pac-container .pac-item{
    padding: 8px 3px 8px 20px;
    line-height: inherit;
    font-size: 14px;
    color: #3c3d3e;
    border: none;
}
.pac-container .pac-item:hover{
    background-color: #e35f46;
    color: #fff;
}
.pac-container .pac-item-query{
    font-size: inherit;
    color: inherit;
}
.pac-container .pac-item-query::after{
    content: ",";
}
CSS
);