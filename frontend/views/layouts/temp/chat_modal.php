<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 03.07.2017
 * Time: 12:14
 */

use yii\bootstrap\Html;
use yii\bootstrap\Modal;

/** @var \yii\web\View $this */

\frontend\assets\ProductAsset::register($this);
\frontend\assets\AngularAsset::register($this);
\frontend\assets\ScrollAsset::register($this);

$model = new \frontend\models\ChatConversationForm();
?>


<?php Modal::begin([
    'id'           => 'chatWindowPanel',
    'toggleButton' => false,
    'closeButton'  => false,
    'size'         => Modal::SIZE_LARGE,
]);
?>

    <div class="leftChat" ng-controller="ListController as LC">
        <div class="blockHeader styleInput">
            <h4 class="hidden-sm hidden-md hidden-lg text-center titleMobile">
                <button class="btn searchMobileMasaage">
                    <svg width="16px" height="16px" viewBox="0 0 16 16" >
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="UI1.6.7-------Дропдаун--при-фокусе-на-даты-с-историей-прошлых-дат" transform="translate(-212.000000, -31.000000)" fill-rule="nonzero" fill="#EB4620">
                                <g id="Group-23" transform="translate(212.000000, 12.000000)">
                                    <path d="M15.8,34.1 L12.6,30.9 C13.7,29.6 14.4,28 14.4,26.2 C14.4,22.2 11.2,19 7.2,19 C3.2,19 0,22.2 0,26.2 C0,30.2 3.2,33.4 7.2,33.4 C9,33.4 10.7,32.7 11.9,31.6 L15.1,34.8 C15.2,34.9 15.3,34.9 15.5,34.9 C15.7,34.9 15.8,34.9 15.9,34.8 C16,34.6 16,34.3 15.8,34.1 Z M1,26.1 C1,22.7 3.8,19.9 7.2,19.9 C10.6,19.9 13.4,22.7 13.4,26.1 C13.4,29.5 10.6,32.3 7.2,32.3 C3.8,32.3 1,29.6 1,26.1 Z" id="Shape"></path>
                                </g>
                            </g>
                        </g>
                    </svg>
                </button>
                Список сообщений
                <button data-dismiss="modal" class="close">
                    <svg width="22px" height="21px" viewBox="0 0 22 21" >
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
                            <g id="UI1.6.2-Модал-категориии" transform="translate(-373.000000, -25.000000)" stroke="#3C3D3E" stroke-width="2">
                                <g id="Group-4">
                                    <g id="close---icon---grey" transform="translate(374.000000, 26.000000)">
                                        <path d="M0.318645628,18.83413 L19.9895953,0.203126916" id="Line"></path>
                                        <path d="M0.318645628,18.83413 L19.9895953,0.203126916" id="Line-Copy-2" transform="translate(10.154120, 9.518628) scale(-1, 1) translate(-10.154120, -9.518628) "></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg></button></h4>
            <div class="hidden-xs searchField">
                <input type="text" placeholder="Поиск сообщения" class="searchMasseg " ng-model="filterWord">
                <button class="btn hidden-sm hidden-md hidden-lg closeSearchInput" ng-click="filterWord = '';"></button>
            </div>


        </div>
        <div class="blockContent scroll-1">
            <div class="rezult" ng-show="filterWord.length && LC._mdMedia('min-width: 768px')">
                <span class="text">Результатов:</span>
                <span class="count">{{LC.conversations().length}}</span>
            </div>

            <div class="massage scroll-1">
                <div class="btn-group" role="group" ng-hide="filterWord.length && LC._mdMedia('max-width: 768px')">
                    <button type="button" class="btn" ng-class="{active: !onlyFavorite}" ng-click="onlyFavorite = 0;">
                        Все сообщения
                    </button>
                    <button type="button" class="btn" ng-class="{active: onlyFavorite}" ng-click="onlyFavorite = 1;">
                        Прикрепленные <span class="budge"
                                ng-show="1">{{LC.fc()}}</span></button>
                </div>

                <div id="list_of_chats" class="list-group">
                    <conversation-item ng-repeat="gossip in LC.conversations() | orderBy:'-updated_at'"
                            model="gossip"
                            index="$index"
                            filter-word="filterWord"
                            ng-click="LC.setActive(gossip.id)"></conversation-item>
                </div>
            </div>
        </div>
        <div class="blockFooter" ng-hide="filterWord.length && LC._mdMedia('max-width: 768px')">
            <button class="supportBtn btn-block" ng-click="createDialog()">Написать в службу поддержки</button>
        </div>
    </div>

    <div class="rightChat hiddenRightXs" ng-controller="ChatController as CC">
        <div class="blockHeader text-justify">
            <a href="#" class="hidden visible-xs backXs">
                <svg width="15px" height="25px" viewBox="0 0 15 25" >
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="UI1.5.2-Дропдаун-при-фокусе-на-категорию-1">
                            <g id="back---icon---grey">


                                <polyline id="Path-3-Copy-2" stroke="#3C3D3E" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" transform="translate(25.978098, 35.500000) rotate(-180.000000) translate(-6.978098, -12.500000) " points="21.0854186 45 30.8707778 35.2146408 21.656137 26"></polyline>
                            </g>
                        </g>
                    </g>
                </svg></a>
            <h3 class="theMassage"
                    ng-mousedown="changeTitle($event)"
                    ng-attr-contenteditable="{{CC.editTitle}}"
                    ng-blur="finishChangeTitle($event)">{{CC.model().subject}}</h3>

            <span class="left">
            <a href="#"
                    class="snap_conversation"
                    style="display: inline-block;"
                    ng-click="toggleSnap()"
                    ng-class="{active: CC.model().is_favorite}">
                <svg width="16px" height="27px" viewBox="0 0 16 27">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="UI11.2----Переписка" transform="translate(-1140.000000, -198.000000)" fill="#AFAFAF">
            <g id="POP-UP-CHAT" transform="translate(211.000000, 174.000000)">
                <g id="HEAD-POPUP" transform="translate(345.000000, 0.000000)">
                    <g id="pin" transform="translate(584.000000, 24.000000)">
                        <path d="M12.7021,14.0873 L13.4671,14.0873 L13.9581,16.0943 L1.3061,16.0943 L1.7981,14.0873 L2.5481,14.0873 C2.5531,14.0873 2.5571,14.0903 2.5621,14.0903 L10.3601,14.0903 C10.6371,14.0903 10.8601,13.8663 10.8601,13.5903 C10.8601,13.3143 10.6371,13.0903 10.3601,13.0903 L3.0621,13.0903 L3.0621,5.8813 C3.0621,4.7423 2.2471,3.7903 1.1701,3.5783 L1.1701,1.5273 L14.0951,1.5273 L14.0951,3.5783 C13.0171,3.7903 12.2021,4.7423 12.2021,5.8813 L12.2021,13.5873 C12.2021,13.8633 12.4261,14.0873 12.7021,14.0873 L12.7021,14.0873 Z M8.2351,24.0213 L7.6331,24.8333 L7.0291,24.0213 L7.0291,17.0943 L8.2351,17.0943 L8.2351,24.0213 Z M14.5491,4.5343 C14.8251,4.5343 15.0951,4.2653 15.0951,3.9883 L15.0951,1.0273 C15.0951,0.7513 14.8711,0.5273 14.5951,0.5273 L0.6701,0.5273 C0.3931,0.5273 0.1701,0.7513 0.1701,1.0273 L0.1701,3.9883 C0.1701,4.2653 0.4391,4.5343 0.7161,4.5343 C1.4581,4.5343 2.0621,5.1393 2.0621,5.8813 L2.0621,13.0873 L1.4051,13.0873 C1.1751,13.0873 0.9741,13.2443 0.9201,13.4683 L0.1841,16.4753 C0.1471,16.6243 0.1811,16.7813 0.2761,16.9023 C0.3711,17.0243 0.5161,17.0943 0.6701,17.0943 L6.0291,17.0943 L6.0291,24.1863 C6.0291,24.2933 6.0631,24.3983 6.1281,24.4833 L7.2311,25.9713 C7.3251,26.0983 7.4741,26.1733 7.6331,26.1733 C7.7911,26.1733 7.9401,26.0983 8.0341,25.9713 L9.1371,24.4833 C9.2011,24.3983 9.2351,24.2933 9.2351,24.1863 L9.2351,17.0943 L14.5951,17.0943 C14.7481,17.0943 14.8931,17.0243 14.9881,16.9023 C15.0831,16.7813 15.1171,16.6243 15.0801,16.4753 L14.3451,13.4683 C14.2901,13.2443 14.0901,13.0873 13.8591,13.0873 L13.2021,13.0873 L13.2021,5.8813 C13.2021,5.1393 13.8061,4.5343 14.5491,4.5343 L14.5491,4.5343 Z"
                                id="Fill-23"></path>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
            </a>

            <button type="button" class="close hidden-xs" data-dismiss="modal" aria-label="Close">
                <svg width="15px" height="15px" viewBox="0 0 8 8" >
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
        <g id="UI15.1----Отзывы-(личный-кабинет)---дефолт" transform="translate(-525.000000, -774.000000)" stroke="#FFFFFF" fill="#FFFFFF">
            <g id="content" transform="translate(280.000000, 151.000000)">
                <g id="Group-22" transform="translate(20.000000, 89.000000)">
                    <g id="Group-20">
                        <g id="Group-19" transform="translate(0.000000, 204.000000)">
                            <g id="Group-25" transform="translate(70.000000, 273.000000)">
                                <g id="close" transform="translate(151.000000, 53.000000)">
                                    <g transform="translate(4.888889, 4.888889)" id="Combined-Shape">
                                        <path d="M3.11111111,2.48257175 L0.832788199,0.204248838 C0.659221847,0.0306824861 0.37781519,0.0306824861 0.204248838,0.204248838 L0.204248838,0.832788199 L2.48257175,3.11111111 L0.151606014,5.44207685 C-0.0219603375,5.6156432 -0.0219603375,5.89704986 0.151606014,6.07061621 C0.325172366,6.24418256 0.606579024,6.24418256 0.780145375,6.07061621 L3.11111111,3.73965047 L5.44207685,6.07061621 C5.6156432,6.24418256 5.89704986,6.24418256 6.07061621,6.07061621 C6.24418256,5.89704986 6.24418256,5.6156432 6.07061621,5.44207685 L3.73965047,3.11111111 L6.01797338,0.832788199 C6.19153974,0.659221847 6.19153974,0.37781519 6.01797338,0.204248838 L5.38943402,0.204248838 L3.11111111,2.48257175 Z"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
            </button>
        </span>

        </div>
        <div class="blockContent scroll-1" id="conversation_area">
            <conversation-message-item ng-repeat="model in CC.messages() | orderBy: ['+created_at','+id'] track by model.id"
                    model="model"
                    il="$last"></conversation-message-item>
            <div class="empty_list" ng-hide="1">empty list</div>
        </div>

        <div class="blockFooter">
            <form name="addNewMessageForm"
                    class="styleInput inlineForm"
                    enctype="multipart/form-data"
                    method="post"
                    ng-submit="sendMessage($event)">

                <span class="attach">
                    <label for="addForMassage">
                        <svg width="25px" height="26px" viewBox="0 0 25 26">
            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <g id="UI11.5----Бронировании-товара"
                        transform="translate(-590.000000, -753.000000)"
                        fill-rule="nonzero"
                        fill="#AFAFAF">
                    <g id="POP-UP-CHAT" transform="translate(211.000000, 174.000000)">
                        <g id="CHAT-">
                            <path d="M386,581.8 L386,600.1 C386,600.1 386,607.000003 392,607 C398,606.999997 398,600.1 398,600.1 L398,581.8 C398,581.05 396.666667,581.05 396.666667,581.8 L396.666667,600.100001 C396.666667,600.100001 397.151515,605.636364 392,605.636364 C387.333333,605.636364 387.333333,600.100001 387.333333,600.100001 L387.333333,581.8 C387.333333,581.8 387.333333,578.363638 390.666667,578.363636 C394,578.363635 394,581.8 394,581.8 L394,600.100001 C394,600.100001 394,602.251211 392.054421,602.25121 C390.108844,602.251207 390,600.100001 390,600.100001 L390,582.7 C390,581.95 388.666667,581.95 388.666667,582.7 L388.666667,600.1 C388.666667,600.1 388.418059,603.751213 392.054421,603.75121 C395.333333,603.751207 395.333333,600.1 395.333333,600.1 L395.333333,581.8 C395.333333,581.8 395.333333,576.999999 390.666667,577 C386,577.000001 386,581.8 386,581.8 Z"
                                    id="attach"
                                    transform="translate(392.000000, 592.000000) rotate(45.000000) translate(-392.000000, -592.000000) "></path>
                        </g>
                    </g>
                </g>
            </g>
        </svg>
                    </label>

                    <?= Html::fileInput('image', null, ['id' => 'addForMessage', 'class' => 'hidden']) ?>
                </span>

                <div class="textEdit scroll-1" contenteditable="true"></div>
                <?= Html::submitButton(\Yii::t('UI11.2', '$CHATWINDOW_SEND_MESSAGE_BTN$'), ['class' => 'send']) ?>
            </form>
        </div>
    </div>

<?php Modal::end() ?>

<?php
$this->registerJsFile('@web/js/chat.js', [
    'position' => \yii\web\View::POS_END,
    'depends'  => [
        'frontend\assets\AngularAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ],
]);

$this->registerCss(<<<CSS
#list_of_chats .list-group-item {
    padding: 0;
}
#chatWindowPanel .snap_conversation{
    transition: transform 250ms ease-out;
}
#chatWindowPanel .snap_conversation.active{
    transform: rotate(45deg);
}
#chatWindowPanel .leftChat .massage .btn-group .btn.active{
    background-color: #e35f46 !important;
    border-color: #e35f46 !important;
    color: #fff !important;
}
#conversation_area{
    overflow: scroll;
}
#conversation_area .date_line{
    position: relative;
}
#conversation_area .date_line div{
    position: absolute;
    top: -15px;
    left: 0;
    width: 100%;
}
#conversation_area .date_line span{
    display: inline-block;
    padding: 5px 10px;
    background-color: #fff;
}

CSS
);
