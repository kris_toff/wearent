<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 07.04.2017
 * Time: 10:40
 */

/** @var \yii\web\View $this */
/** @var string $content */

if (\Yii::$app->user->isGuest && is_null(\Yii::$app->request->userIP)) {
    $pr = new \yii\data\ArrayDataProvider(['allModels' => []]);
} else {
    $pr = new \yii\data\ActiveDataProvider([
        'query'      => \frontend\models\HistorySearch::find()
            ->filterWhere(['user_id' => \Yii::$app->user->isGuest ? null : \Yii::$app->user->id])
            ->orFilterWhere(['ip' => \Yii::$app->request->userIP]),
        'pagination' => [
            'defaultPageSize' => 6,
            'page'            => 0,
            'forcePageParam'  => false,
            'pageSize'        => 6,
            'validatePage'    => true,
        ],
        'sort'       => [
            'defaultOrder'    => [
                'created_at' => SORT_DESC,
                'user_id'    => SORT_DESC,
            ],
            'enableMultiSort' => true,
        ],
    ]);
}

$this->params['historySearchDataProvider'] = $pr;
?>

<?php $this->beginContent('@app/views/layouts/base.php'); ?>

<div class="container-fluid">
    <?= $this->render('temp/header') ?>

    <?= $content ?>

    <?= $this->render('temp/chat_modal') ?>
    <?= $this->render('temp/footer') ?>
</div>

<?php $this->endContent() ?>
