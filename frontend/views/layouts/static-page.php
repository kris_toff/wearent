<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 10.10.2017
 * Time: 23:26
 */

/** @var \yii\web\View $this */
/** @var string $content */


?>

<?php $this->beginContent('@app/views/layouts/typicalFrontend.php') ?>

<div class="row marg hidden-xs"></div>

<div class="row wrapp contentBody">
    <div class="container-fluid">

        <?= $content ?>

    </div>
</div>

<?php $this->endContent() ?>
