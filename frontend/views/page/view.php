<?php
/**
 * @var $this  \yii\web\View
 * @var $model \common\models\Page
 */
$this->title = $model->title;
$this->registerMetaTag(['name' => 'keywords', 'content' => $model->keywords]);
$this->registerMetaTag(['name' => 'description', 'content' => $model->description]);
?>


<div class="content">
    <h1><?= $model->h1 ?: $model->title ?></h1>
    <?= $model->body ?>
</div>