<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.08.2017
 * Time: 9:26
 */

use yii\helpers\Url;

/** @var \yii\web\View $this */
\frontend\assets\ProductAsset::register($this);
\frontend\assets\CheckboxAsset::register($this);

?>



    <div style="margin-top: 150px;" ng-controller="TestController">

        <?= \Yii::t('UI-slider',
            '$MINIMAL_PERIOD_DAY$',
            ['min_period' => 24]) ?>

        <input type="checkbox" ng-checked="isTest" ng-init="isTest = true" />
        <md-button ng-click="isTest = !isTest">change</md-button>
        {{2+1 + isTest}}

        <input type="checkbox" ng-model="q" />
        <md-checkbox ng-model="q">ce</md-checkbox>
        {{ +q }}

        <form  method="get" id="tf" action="">
            <input type="text" name="go" />

            <md-checkbox ng-model="y">oc</md-checkbox>

            <md-button ng-click="s()">save</md-button>
            <input type="submit" value="Submit" />
        </form>
    </div>



<?php
$this->registerJsFile('/js/atest.js', [
    'depends'  => [
        'frontend\assets\AngularAsset',
    ],
    'position' => \yii\web\View::POS_END,
]);