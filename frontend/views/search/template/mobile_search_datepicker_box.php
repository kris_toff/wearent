<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 14.11.2017
 * Time: 17:56
 */

use yii\helpers\Html;

?>
<div layout="column" layout-align="start stretch" style="height: 100%;">
    <div class="panel_header" flex="none" layout="row" layout-align="center center">
        <span class="header_title" flex="none"><?= \Yii::t('UI.MOBILE.1.7.9', '$PAGE_TITLE$') ?></span>
        <md-icon ng-click="closeWindow()" md-svg-src="/img/icons/close_btn.svg" class="close_btn"></md-icon>
    </div>

    <md-content class="panel_body text-center" flex layout-padding>
        <div layout="row" layout-align="center center">
            <div><?= Html::input('text', 'uib_start', null, [
                    'ng-model'             => 'start',
                    'uib-datepicker-popup' => 'dd.MM.yyyy',
                    'readonly'             => 'readonly',
                    'placeholder'          => \Yii::t('UI2', '$SEARCH_BAR_FIELD_WHEN_FROM_PLACEHOLDER$'),
                ]) ?></div>
            <div><?= Html::input('text', 'uib_stop', null, [
                    'ng-model'             => 'stop',
                    'uib-datepicker-popup' => 'dd.MM.yyyy',
                    'readonly'             => 'readonly',
                    'placeholder'          => \Yii::t('UI2', '$SEARCH_BAR_FIELD_WHEN_TO_PLACEHOLDER$'),
                ]) ?></div>
        </div>

        <div uib-datepicker
                ng-model="start"
                datepicker-options="doptions1"
                template-url="/search/template?run=mobile_datepicker_template"></div>
        <div uib-datepicker
                ng-model="stop"
                datepicker-options="doptions2"
                template-url="/search/template?run=mobile_datepicker_template"></div>
    </md-content>
    <div class="panel_footer" flex="none">
        <md-button ng-click="saveInterval()"><?= \Yii::t('UI.MOBILE.1.7.9', '$APPLY_PERIOD_BTN$') ?></md-button>
    </div>
</div>