<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.11.2017
 * Time: 14:22
 */

use yii\bootstrap\Html;

?>


<div class="paddSlide">
    <div class="thumbnail pageProduct">
        <div class="main_product_photo">
            <?= Html::a(Html::img('{{parameter.face_img}}', ['class' => 'img-responsive']), "{{'/' + parameter.slug}}",
                ['target' => '_blank']) ?>
        </div>

        <div class="caption">
            <?= Html::a(Html::tag('h3', '{{parameter.name}}', ['class' => 'longName']),
                "{{'/' + parameter.slug}}", ['target' => '_blank']) ?>

            <?= Html::a('{{parameter.category}}', "{{'/category/repel?fields=' + parameter.category}}",
                ['class' => 'btn btnTrans', 'role' => 'button', 'target' => '_blank']) ?>

            <table class="table-block table-responsive">
                <tr>
                    <td ng-switch="parameter.price_step">
                        <span ng-switch-when="1"><?= \Yii::t('UI-slider', '$PRICE_PER_DAY_LABEL$') ?></span>
                        <span ng-switch-when="2"><?= \Yii::t('UI-slider', '$PRICE_PER_WEEK_LABEL$') ?></span>
                        <span ng-switch-when="3"><?= \Yii::t('UI-slider', '$PRICE_PER_MONTH_LABEL$') ?></span>
                        <span ng-switch-when="4"><?= \Yii::t('UI-slider', '$PRICE_PER_HOUR_LABEL$') ?></span>
                    <td class="text-right">
                        {{parameter.price}}
                    </td>
                </tr>
                <tr>
                    <td><?= \Yii::t('UI-slider', '$PLEDGE_PRICE_LABEL$') ?></td>
                    <td class="text-right">{{parameter.pledge_price}}</td>
                </tr>
                <tr>
                    <td><?= \Yii::t('UI-slider', '$MINIMAL_PERIOD_LABEL$') ?></td>
                    <td class="text-right" ng-switch="parameter.price_step">
                        <span ng-switch-when="1"><?= \Yii::t('UI-slider', '$MINIMAL_PERIOD_DAY_TMP$'); ?></span>
                        <span ng-switch-when="2"><?= \Yii::t('UI-slider', '$MINIMAL_PERIOD_WEEK_TMP$'); ?></span>
                        <span ng-switch-when="3"><?= \Yii::t('UI-slider', '$MINIMAL_PERIOD_MONTH_TMP$'); ?></span>
                        <span ng-switch-when="4"><?= \Yii::t('UI-slider', '$MINIMAL_PERIOD_HOUR_TMP$'); ?></span>
                    </td>
                </tr>
                <tr>
                    <td><?= \Yii::t('UI-slider', '$RATING_LABEL$') ?></td>
                    <td class="text-right">
                        <span uib-rating ng-model="parameter.rate" read-only="true" state-on="'pga-icon-star-on'" state-off="'pga-icon-star-off'"></span>
                        <div class="rateyoHome" data-rating="1"></div>
                    </td>
                </tr>
            </table>

            <div class="row margg">
                <div class="col-xs-12 rowBottom">
                    <div class="margLeft">
                        <span class="face">
                            <?= Html::a(Html::img('{{parameter.face_author}}', ['class' => 'menFase']),
                                "{{'/profile/id' + parameter.user_id}}", ['target' => '_blank']) ?>
                        </span>
                        <span class="mail">
                            <img class="menFase" src="img/home/mail.svg" alt=""> <?= '???' ?>
                        </span>
                    </div>

                    <div hidden class="markProduct">
                        <span class="count">25</span>
                    </div>

                    <span class="leftRightBtn" ng-show="parameter.is_fastrent"></span>
                </div>
            </div>
        </div>
    </div>
</div>