<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 15.11.2017
 * Time: 12:36
 */


?>

<div ng-switch="datepickerMode">
    <div uib-daypicker ng-switch-when="day" tabindex="0" class="uib-daypicker" template-url="/search/template?run=mobile_datepicker_day_template"></div>
    <div uib-monthpicker ng-switch-when="month" tabindex="0" class="uib-monthpicker"></div>
    <div uib-yearpicker ng-switch-when="year" tabindex="0" class="uib-yearpicker"></div>
</div>