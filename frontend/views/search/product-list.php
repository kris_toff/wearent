<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 15.05.2017
 * Time: 11:37
 */

use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use frontend\models\ProductCurrency;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $products */

?>

<?php //print '<pre>';print_r($products->getModels());exit;
foreach ($products->getModels() as $prd): ?>

    <?= $this->render('/blocks/_product-list-item', ['model' => $prd]); ?>

<?php endforeach; ?>

<?php
$this->registerCss(<<<CSS
.product_slider .product_slider_item .favorite_status .in{
    display: none;
}
.product_slider .product_slider_item .favorite_status.active .without{
    display: none;
}
CSS
    , [], 'product_slider');
$this->registerJs(<<<JS
jQuery(".product_slider_item .favorite_status")
.on("click", function(event){
    var fs = jQuery(event.currentTarget);
    
    jQuery.ajax( "/product/change-favourite", {
        method: "put",
        data: {product_id: fs.data("id")},
        timeout: 2000,
        cache: false,
        global: false
    })
    .done(function(response) {
        fs.toggleClass("active");
    });
});
JS
    , \yii\web\View::POS_READY, 'product_slider');