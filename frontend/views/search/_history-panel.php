<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 11.11.2017
 * Time: 13:41
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/** @var \frontend\models\HistorySearch $model */
/** @var string $key */
/** @var integer $index */
?>

<?= Html::tag('span', '', [
    'class' => 'data',
    'data'  => [
        'category' => $model->field_category_id,
        'location' => $model->field_where_coords,
        'from'     => $model->field_when_from,
        'to'       => $model->field_when_to,
    ],
]) ?>
<div class="left">
    <span class="historyWhat">
        <svg class="historyImg" width="13px" height="16px" viewBox="0 0 13 16">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="historyImg" transform="translate(-591.000000, -127.000000)">
            <g id="product" transform="translate(572.000000, 71.000000)">
                <g id="icon-copy" transform="translate(20.000000, 56.000000)">
                    <ellipse id="Oval-2" cx="5.5" cy="9.45378151" rx="5.5" ry="5.54621849"></ellipse>
                    <ellipse id="Oval-2-Copy" cx="5.5" cy="9.45378151" rx="0.5" ry="0.504201681"></ellipse>
                    <rect id="Rectangle-7"
                            transform="translate(4.500000, 8.445378) rotate(-315.000000) translate(-4.500000, -8.445378) "
                            x="3" y="7.94117647" width="3" height="1.00840336"></rect>
                    <rect id="Rectangle-7-Copy"
                            transform="translate(5.500000, 2.394958) rotate(-270.000000) translate(-5.500000, -2.394958) "
                            x="4" y="1.8907563" width="3" height="1.00840336"></rect>
                    <rect id="Rectangle-7-Copy-2"
                            transform="translate(5.500000, 1.386555) rotate(-180.000000) translate(-5.500000, -1.386555) "
                            x="4" y="0.882352941" width="3" height="1.00840336"></rect>
                </g>
            </g>
        </g>
    </g>
</svg>
        <?= Html::encode(ArrayHelper::getValue($model, 'field_what', '')) ?>
    </span>
</div>
<div class="center">
    <span class="historyWhen"><?= Html::encode(ArrayHelper::getValue($model, 'field_where_string', '')) ?></span>
</div>
<div class="right">
    <span class="startDate"><?= (int)$model->field_when_from
        > 0 ? \Yii::$app->formatter->asDate($model->field_when_from, 'dd.MM.yyyy') : '' ?></span>
    <span class="img">
        <svg class="whenImg" width="22px" height="9px" viewBox="0 0 22 9">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="whenImg" transform="translate(-710.000000, -132.000000)">
            <g id="product" transform="translate(572.000000, 71.000000)">
                <g id="Group-15-Copy" transform="translate(138.000000, 61.666667)">
                    <polyline id="whenImg1" stroke-linecap="round" stroke-linejoin="round"
                            transform="translate(18.757144, 3.935000) scale(-1, 1) rotate(-180.000000) translate(-18.757144, -3.935000) "
                            points="16.8221436 7.69214346 20.6921435 3.82214357 17.0478563 0.177856427"></polyline>
                    <rect id="whenImg2" x="0" y="3.5" width="20" height="1"></rect>
                </g>
            </g>
        </g>
    </g>
</svg>
                            </span>
    <span class="end"><?= (int)$model->field_when_to > 0 ? \Yii::$app->formatter->asDate($model->field_when_to,
            'dd.MM.yyyy') : '' ?></span>
</div>
