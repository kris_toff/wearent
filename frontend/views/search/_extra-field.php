<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 14.06.2017
 * Time: 13:06
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use frontend\models\ProductCurrency;
use common\models\Currency;
use yii\helpers\Json;

/** @var \yii\web\View $this */
/** @var \frontend\models\ProductSearch $model */
/** @var array $prices */
/** @var integer $allAmount */

\yii\jui\JuiAsset::register($this);

$timelist = [
    '00:00:00' => '00:00',
    '01:00:00' => '01:00',
    '02:00:00' => '02:00',
    '03:00:00' => '03:00',
    '04:00:00' => '04:00',
    '05:00:00' => '05:00',
    '06:00:00' => '06:00',
    '07:00:00' => '07:00',
    '08:00:00' => '08:00',
    '09:00:00' => '09:00',
    '10:00:00'=> '10:00',
    '11:00:00'=> '11:00',
    '12:00:00'=> '12:00',
    '13:00:00'=> '13:00',
    '14:00:00'=> '14:00',
    '15:00:00'=> '15:00',
    '16:00:00'=> '16:00',
    '17:00:00'=> '17:00',
    '18:00:00'=> '18:00',
    '19:00:00'=> '19:00',
    '20:00:00'=> '20:00',
    '21:00:00'=> '21:00',
    '22:00:00'=> '22:00',
    '23:00:00'=> '23:00',
];
//print '<pre>';print_r($model);exit;
?>


<?php $form = ActiveForm::begin([
    'id'                     => 'extra-search-fields-form',
    'action'                 => ['index'],
    'method'                 => 'get',
    'fieldConfig'            => [
        'template'     => "{label}\n<div class='col-sm-9'>{input}\n{hint}\n{error}</div>",
        'labelOptions' => [
            'class' => 'col-sm-3 control-label',
        ],
        'options'      => [
            'class' => 'form-group clearfix',
        ],
    ],
    'enableClientValidation' => false,
]) ?>
    <!-- элементы формы из общего поиска -->
<?= Html::activeHiddenInput($model, 'category_id'); ?>
<?= Html::activeHiddenInput($model, 'note'); ?>
<?= Html::activeHiddenInput($model, 'location_string'); ?>
<?= Html::activeHiddenInput($model, 'location'); ?>
<?= Html::activeHiddenInput($model, 'period_from'); ?>
<?= Html::activeHiddenInput($model, 'period_to'); ?>
<?= Html::activeHiddenInput($model, 'isOnlyCategory'); ?>

<?= Html::tag('div', '', [
    'ng-init' => implode(';', [
        'minimal_rent_price = ' . ArrayHelper::getValue($model, 'rent_cost_b.0', $prices['minp']),
        'maximal_rent_price = ' . ArrayHelper::getValue($model, 'rent_cost_b.1', $prices['maxp']),
        'minimal_pledge_price = ' . ArrayHelper::getValue($model, 'deposit_cost_b.0', $prices['minplp']),
        'maximal_pledge_price = ' . ArrayHelper::getValue($model, 'deposit_cost_b.1', $prices['maxplp']),
        "amount_update_products = {$allAmount}",
        'currency_symbol = \'' . Currency::getCurrentSymbol() . '\'',
    ]),
]); ?>
<?= $form->field($model, 'rent_cost', [

    'inputTemplate' => '<b>'
        . \Yii::t('UI8.5', '$EXTRA_FILTER_SLIDER_LEFT$')
        . '&nbsp;{{minimal_rent_price|currency:currency_symbol}}</b> {input} <b>'
        . \Yii::t('UI8.5', '$EXTRA_FILTER_SLIDER_RIGHT$')
        . '&nbsp;{{maximal_rent_price|currency:currency_symbol}}</b>',
])->widget(\kartik\slider\Slider::className(), [
    'pluginConflict'=>true,
    'pluginOptions' => [
        'step'  => 1,
        'min'   => max(0, (int)$prices['minp']),
        'max'   => (integer)$prices['maxp'],
        'range' => true,
    ],
])->label(\Yii::t('UI8.5', '$EXTRA_FILTER_RENT_COST_LABEL$')) ?>

<?= $form->field($model, 'deposit_cost', [

    'inputTemplate' => '<b>'
        . \Yii::t('UI8.5', '$EXTRA_FILTER_SLIDER_LEFT$')
        . '&nbsp;{{minimal_pledge_price|currency:currency_symbol}}</b> {input} <b>'
        . \Yii::t('UI8.5', '$EXTRA_FILTER_SLIDER_RIGHT$')
        . '&nbsp;{{maximal_pledge_price|currency:currency_symbol}}</b>',
])->widget(\kartik\slider\Slider::className(), [
    'pluginConflict'=>true,
    'pluginOptions' => [
        'step'  => 1,
        'min'   => max(0, (int)$prices['minplp']),
        'max'   => (int)$prices['maxplp'],
        'range' => true,
    ],
])->label(\Yii::t('UI8.5', '$EXTRA_FILTER_PLEDGE_AMOUNT_LABEL$')) ?>

    <div class="form-group">
        <?= Html::label(\Yii::t('UI8.5', '$EXTRA_FILTER_DISTANCE_LABEL$'), null,
            ['class' => 'control-label col-sm-3']); ?>
        <div class="col-sm-9">
            <?= Html::activeHiddenInput($model, 'distanceDelivery', [
                'ng-value' => 'distanceDelivery',
                'ng-init'  => 'distanceDelivery = ' . intval($model['distanceDelivery']),
            ]); ?>

            <div class="checkbox">
                <?= Html::checkbox('enableDelivery', false, [
                    'ng-model'  => 'enableDelivery',
                    'ng-change' => 'changeDistance( +enableDelivery -1 )',
                    'label'     => \Yii::t('UI8.6', '$EXTRA_FILTER_DELIVERY_ENABLE$'),
                ]); ?>
            </div>
            <div class="checkbox">
                <?= Html::checkbox('deliveryNoMore5', false, [
                    'ng-model'  => 'delivery5',
                    'ng-change' => 'changeDistance(5)',
                    'label'     => \Yii::t('UI8.6', '$EXTRA_FILTER_DELIVERY_LESS_5$'),
                ]); ?>
            </div>
            <div class="checkbox">
                <?= Html::checkbox('deliveryNoMore25', false, [
                    'ng-model'  => 'delivery25',
                    'ng-change' => 'changeDistance(25)',
                    'label'     => \Yii::t('UI8.6', '$EXTRA_FILTER_DELIVERY_LESS_25$'),
                ]); ?>
            </div>

            <div>
                <?= Html::input('text', 'deliveryElse', null, [
                    'placeholder' => \Yii::t('UI8.6', '$EXTRA_FILTER_DELIVERY_ELSE_DISTANCE_PLACEHOLDER$'),
                    'ng-model'    => 'deliveryElse',
                    'ng-change'   => 'changeDistance( deliveryElse )',
                ]); ?>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="form-group">
        <?= Html::activeHiddenInput($model, 'rate',
            ['ng-value' => 's_rate', 'ng-init' => 's_rate = ' . ($model['rate'] ?: 0)]) ?>

        <label class="control-label col-sm-3"><?= \Yii::t('UI8.5', '$EXTRA_FILTER_RATE_LABEL$') ?></label>
        <div class="col-sm-9 ">
            <?= Html::tag('span', '', [
                'uib-rating'             => '',
                'ng-model'               => 's_rate',
                'state-off'              => '\'pga-icon-star-off\'',
                'state-on'               => '\'pga-icon-star-on\'',
                'on-hover' => 's_temp_rate = value - s_rate',
                'on-leave' => 's_temp_rate = 0',

                'uib-popover'            => \Yii::t('UI8.5', '$EXTRA_FILTER_RATE_POPOVER$'),
                'popover-trigger'        => '{\'mouseenter\': \'mouseleave\'}',
                'popover-placement'      => 'right',
                'popover-append-to-body' => 'true',
            ]); ?>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="form-group">
        <?= Html::label(\Yii::t('UI8.5', '$EXTRA_FILTER_TIME_LABEL$'), null, ['class' => 'control-label col-sm-3']); ?>
        <div class="col-sm-9">
            <div>
                <?php if ((integer)$model['period_from']): ?>
                    <?= \Yii::t('UI8.5', '$EXTRA_FILTER_TIME_FROM_PREFIX$'); ?>
                    <?= \Yii::$app->formatter->asDate((integer)$model['period_from'], 'dd.MM.y'); ?>
                <?php endif; ?>

                <?= Html::activeDropDownList($model, 'time_from', $timelist, ['placeholder' => '10:00', 'class' => 'form-control', 'prompt' => 'select...', 'ng-model' => 'time_from']) ?>
            </div>
            <div>
                <?php if ((integer)$model['period_to']): ?>
                    <?= \Yii::t('UI8.5', '$EXTRA_FILTER_TIME_TO_PREFIX$'); ?>
                    <?= \Yii::$app->formatter->asDate((integer)$model['period_to'], 'dd.MM.y'); ?>
                <?php endif; ?>

                <?= Html::activeDropDownList($model, 'time_to', $timelist, ['placeholder' => '22:00', 'class' => 'form-control', 'prompt' => 'select...', 'ng-model' => 'time_to']); ?>
            </div>
        </div>
    </div>

<?php
foreach ($model->getEavAttributes()->all() as $attr) {
    print $form->field($model, $attr->name, ['class' => '\mirocow\eav\widgets\ActiveField', 'options' => ['class' => 'form-group fil_atr']])->eavInput();
}
?>

    <div class="bottomBtn">
        <!---->
        <!--        --><? //= Html::button('More filters <i class="gluphicon">
        //<svg width="10px" height="6px" viewBox="0 0 10 6" >
        //    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
        //        <g class="re" transform="translate(-1201.000000, -118.000000)"  stroke-width="2">
        //            <g id="product" transform="translate(572.000000, 71.000000)">
        //                <polyline id="Path-3-Copy" transform="translate(633.757144, 49.935000) scale(-1, 1) rotate(-270.000000) translate(-633.757144, -49.935000) " points="631.822144 53.6921435 635.692143 49.8221436 632.047856 46.1778564"></polyline>
        //            </g>
        //        </g>
        //    </g>
        //</svg>
        //
        //</i>',
        //            ['class' => 'btn more moreSmall hidden-sm hidden-md hidden-lg', 'id' => 'showMoreFilters']) ?>

        <?= Html::button(\Yii::t('UI8.5', '$EXTRA_FILTER_CANCEL$'),
            ['class' => 'btn  cancel', 'id' => 'closeFilterWindow']) ?>
        <?= Html::button(\Yii::t('UI8.5', '$EXTRA_FILTER_SHOW_MORE$') . ' <i class="gluphicon">
<svg width="10px" height="6px" viewBox="0 0 10 6" >
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
        <g class="re" transform="translate(-1201.000000, -118.000000)"  stroke-width="2">
            <g id="product" transform="translate(572.000000, 71.000000)">
                <polyline id="Path-3-Copy" transform="translate(633.757144, 49.935000) scale(-1, 1) rotate(-270.000000) translate(-633.757144, -49.935000) " points="631.822144 53.6921435 635.692143 49.8221436 632.047856 46.1778564"></polyline>
            </g>
        </g>
    </g>
</svg>

</i>',
            ['class' => 'btn more', 'id' => 'showMoreFilters']) ?>
        <?= Html::submitButton(\Yii::t('UI8.5', '$EXTRA_FILTER_UPDATE_PRODUCTS$') . ' <span class="counterProduct" ng-switch-when="false">({{amount_update_products}})</span> <span class="preloader" ng-switch-when="true"><svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
  <path fill="#FFF" fill-rule="evenodd" d="M9,0 L10,0 L10,4 L9,4 L9,0 Z M15.2234601,1.90118399 L15.9895045,2.5439716 L13.4183541,5.60814937 L12.6523096,4.96536176 L15.2234601,1.90118399 Z M18.7688496,7.35793844 L18.9424977,8.34274619 L15.0032667,9.0373389 L14.8296186,8.05253115 L18.7688496,7.35793844 Z M17.9772413,13.8169873 L17.4772413,14.6830127 L14.0131397,12.6830127 L14.5131397,11.8169873 L17.9772413,13.8169873 Z M13.2190377,18.2560698 L12.2793451,18.59809 L10.9112645,14.8393195 L11.8509571,14.4972993 L13.2190377,18.2560698 Z M6.72065495,18.59809 L5.78096233,18.2560698 L7.1490429,14.4972993 L8.08873552,14.8393195 L6.72065495,18.59809 Z M1.52275866,14.6830127 L1.02275866,13.8169873 L4.48686028,11.8169873 L4.98686028,12.6830127 L1.52275866,14.6830127 Z M0.0575022576,8.34274619 L0.231150435,7.35793844 L4.17038145,8.05253115 L3.99673327,9.0373389 L0.0575022576,8.34274619 Z M3.01049549,2.5439716 L3.77653993,1.90118399 L6.34769037,4.96536176 L5.58164593,5.60814937 L3.01049549,2.5439716 Z"/>
</svg>
</span>', ['class' => 'btn show', 'ng-disabled' => '(amount_update_products < 1) || SEFC.updatingAmount', 'ng-switch' => 'SEFC.updatingAmount']); ?>
    </div>
    <div class="clearfix"></div>

<?php ActiveForm::end() ?>


<?php
$eav = Json::encode($model->EavModel);
$formName = $model->formName();

$this->registerJs(<<<JS
var form = $("#extra-search-fields-form");
var ls = form.find(".form-group").slice(7);
ls = ls.wrapAll("<div class='tl_attrs' />");

var tgl_attrs = form.find(".tl_attrs");
tgl_attrs.hide();

var btnMore = $("#showMoreFilters");
var btnIcon = btnMore.find(".glyphicon");

if (ls.length){
    btnMore.on("click", function(event){
    tgl_attrs.slideToggle(ls.length * 25 + 100);
    
    btnIcon.toggleClass("glyphicon-menu-up glyphicon-menu-down");
});
}else{
    btnMore.remove();
}

jQuery.each({$eav}, function(prop, val){
    var sl = _.isArray(val) ? _.join(_.map(val, function(o){ return "[value='" + o + "']"; })) : ("[value='" + val + "']");
    var fl = form.find("input[name^='{$formName}[EavModel][" + prop + "]']");
    fl = fl.filter(sl);
    
    if (fl.is("[type='radio'],[type='checkbox']")){
        fl.prop("checked", true);
    } else if(fl.is("[type='text'],[type='number']")){
        fl.val(val);
    }
});
JS
);