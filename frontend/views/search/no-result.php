<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 14.06.2017
 * Time: 11:47
 */

/** @var \yii\web\View $this */

/** @var \frontend\models\ProductSearch $searchForm */

use yii\bootstrap\Html;

//$searchForm = new \frontend\models\ProductSearch();

\frontend\assets\ProductAsset::register($this);
\frontend\assets\AppAsset::register($this);
\frontend\assets\DateRangePickerAsset::register($this);

$this->params['searchForm'] = $searchForm;

?>

    <div class="row windowFound">
        <div class="container">
            <div class="row found">
                <div class="col-sm-12 notFound">
                    <p class="text-center">
                        <svg width="177px" height="144px" viewBox="0 0 177 144">
                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g id="+UI8.7----По-заданным-параметрам-ничего-не-нашлось"
                                        transform="translate(-632.000000, -200.000000)">
                                    <g id="Group-4" transform="translate(210.000000, 140.000000)">
                                        <g id="Page-1" transform="translate(422.000000, 60.000000)">
                                            <path d="M28.9411,122.167 L6.5001,122.167 C3.1861,122.167 0.5001,119.48 0.5001,116.167 L0.5001,6.5 C0.5001,3.187 3.1861,0.5 6.5001,0.5 L169.8331,0.5 C173.1471,0.5 175.8331,3.187 175.8331,6.5 L175.8331,116.167 C175.8331,119.48 173.1471,122.167 169.8331,122.167 L147.5571,122.167"
                                                    id="Stroke-1" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"></path>
                                            <path d="M11.1066,11.1025 C11.1066,11.7215 10.6046,12.2235 9.9856,12.2235 C9.3666,12.2235 8.8646,11.7215 8.8646,11.1025 C8.8646,10.4835 9.3666,9.9815 9.9856,9.9815 C10.6046,9.9815 11.1066,10.4835 11.1066,11.1025"
                                                    id="Fill-3" fill="#F55656"></path>
                                            <path d="M17.8307,11.1025 C17.8307,11.7215 17.3297,12.2235 16.7097,12.2235 C16.0907,12.2235 15.5897,11.7215 15.5897,11.1025 C15.5897,10.4835 16.0907,9.9815 16.7097,9.9815 C17.3297,9.9815 17.8307,10.4835 17.8307,11.1025"
                                                    id="Fill-5" fill="#E8B434"></path>
                                            <path d="M24.5553,11.1025 C24.5553,11.7215 24.0533,12.2235 23.4343,12.2235 C22.8153,12.2235 22.3133,11.7215 22.3133,11.1025 C22.3133,10.4835 22.8153,9.9815 23.4343,9.9815 C24.0533,9.9815 24.5553,10.4835 24.5553,11.1025"
                                                    id="Fill-7" fill="#4DC97B"></path>
                                            <polyline id="Stroke-9" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="147.5568 130.959 147.5568 48.839 123.1558 24.438"></polyline>
                                            <path d="M45.4455,24.4385 L123.1555,24.4385" id="Stroke-11" stroke="#AFAFAF"
                                                    stroke-linecap="round" stroke-linejoin="round"></path>
                                            <polyline id="Stroke-13" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="28.9411 28.8872 28.9411 24.4382 32.7951 24.4382"></polyline>
                                            <path d="M28.9411,139.2129 L28.9411,44.5219" id="Stroke-15" stroke="#AFAFAF"
                                                    stroke-linecap="round" stroke-linejoin="round"></path>
                                            <polygon id="Stroke-17" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="38.5363 18.5464 31.1113 26.7574 22.7763 35.9744 22.9253 38.9224 25.8723 38.7744 26.0213 41.7224 28.9693 41.5744 29.1173 44.5224 32.0653 44.3734 32.2133 47.3214 35.1613 47.1734 43.4953 37.9564 50.9213 29.7454"></polygon>
                                            <path d="M76.3874,135.2598 L76.3874,115.3958"
                                                    id="Stroke-19"
                                                    stroke="#AFAFAF"
                                                    stroke-linecap="round"
                                                    stroke-linejoin="round"></path>
                                            <path d="M100.1105,115.3955 L100.1105,135.2595"
                                                    id="Stroke-21"
                                                    stroke="#AFAFAF"
                                                    stroke-linecap="round"
                                                    stroke-linejoin="round"></path>
                                            <path d="M123.8337,115.3955 L123.8337,135.2595"
                                                    id="Stroke-23"
                                                    stroke="#AFAFAF"
                                                    stroke-linecap="round"
                                                    stroke-linejoin="round"></path>
                                            <path d="M52.6642,139.2129 L52.6642,115.3959"
                                                    id="Stroke-25"
                                                    stroke="#AFAFAF"
                                                    stroke-linecap="round"
                                                    stroke-linejoin="round"></path>
                                            <path d="M88.0231,71.9976 C88.0231,85.7866 76.8451,96.9636 63.0571,96.9636 C49.2691,96.9636 38.0911,85.7866 38.0911,71.9976 C38.0911,58.2096 49.2691,47.0316 63.0571,47.0316 C76.8451,47.0316 88.0231,58.2096 88.0231,71.9976 Z"
                                                    id="Stroke-27" stroke="#E8B434" stroke-linecap="round"
                                                    stroke-linejoin="round"></path>
                                            <path d="M100.1105,63.5254 L120.8545,63.5254"
                                                    id="Stroke-29"
                                                    stroke="#AFAFAF"
                                                    stroke-linecap="round"
                                                    stroke-linejoin="round"></path>
                                            <path d="M100.1105,68.9478 L131.8545,68.9478"
                                                    id="Stroke-31"
                                                    stroke="#AFAFAF"
                                                    stroke-linecap="round"
                                                    stroke-linejoin="round"></path>
                                            <path d="M100.1105,74.3701 L127.8545,74.3701"
                                                    id="Stroke-33"
                                                    stroke="#AFAFAF"
                                                    stroke-linecap="round"
                                                    stroke-linejoin="round"></path>
                                            <path d="M100.1105,79.7925 L131.8545,79.7925"
                                                    id="Stroke-35"
                                                    stroke="#AFAFAF"
                                                    stroke-linecap="round"
                                                    stroke-linejoin="round"></path>
                                            <path d="M50.759,86.6328 C50.759,79.8408 56.265,74.3348 63.057,74.3348 C69.849,74.3348 75.356,79.8408 75.356,86.6328"
                                                    id="Stroke-37" stroke="#E8B434" stroke-linecap="round"
                                                    stroke-linejoin="round"></path>
                                            <path d="M123.0426,24.4385 L123.0426,48.9525"
                                                    id="Stroke-39"
                                                    stroke="#AFAFAF"
                                                    stroke-linecap="round"
                                                    stroke-linejoin="round"></path>
                                            <path d="M147.5568,48.9526 L123.0428,48.9526"
                                                    id="Stroke-41"
                                                    stroke="#AFAFAF"
                                                    stroke-linecap="round"
                                                    stroke-linejoin="round"></path>
                                            <polyline id="Stroke-43" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="36.8488 139.2129 32.8948 143.1669 28.9408 139.2129"></polyline>
                                            <polyline id="Stroke-45" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="44.7565 139.2129 40.8025 143.1669 36.8485 139.2129"></polyline>
                                            <polyline id="Stroke-47" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="52.6642 139.2129 48.7102 143.1669 44.7562 139.2129"></polyline>
                                            <polyline id="Stroke-49" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="57.4089 135.2598 55.0359 137.6318 52.6639 135.2598"></polyline>
                                            <polyline id="Stroke-51" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="62.1535 135.2598 59.7805 137.6318 57.4085 135.2598"></polyline>
                                            <polyline id="Stroke-53" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="66.8981 135.2598 64.5251 137.6318 62.1531 135.2598"></polyline>
                                            <polyline id="Stroke-55" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="71.6427 135.2598 69.2697 137.6318 66.8977 135.2598"></polyline>
                                            <polyline id="Stroke-57" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="76.3874 135.2598 74.0144 137.6318 71.6424 135.2598"></polyline>
                                            <polyline id="Stroke-59" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="104.8551 135.2598 102.4821 137.6318 100.1101 135.2598"></polyline>
                                            <polyline id="Stroke-61" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="109.5998 135.2598 107.2268 137.6318 104.8548 135.2598"></polyline>
                                            <polyline id="Stroke-63" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="114.3444 135.2598 111.9714 137.6318 109.5994 135.2598"></polyline>
                                            <polyline id="Stroke-65" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="119.089 135.2598 116.716 137.6318 114.344 135.2598"></polyline>
                                            <polyline id="Stroke-67" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="123.8337 135.2598 121.4607 137.6318 119.0887 135.2598"></polyline>
                                            <polyline id="Stroke-69" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="128.5783 130.959 126.2053 133.331 123.8333 130.959"></polyline>
                                            <polyline id="Stroke-71" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="133.3229 130.959 130.9499 133.331 128.5779 130.959"></polyline>
                                            <polyline id="Stroke-73" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="138.0676 130.959 135.6946 133.331 133.3226 130.959"></polyline>
                                            <polyline id="Stroke-75" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="142.8122 130.959 140.4392 133.331 138.0672 130.959"></polyline>
                                            <polyline id="Stroke-77" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="147.5568 130.959 145.1838 133.331 142.8118 130.959"></polyline>
                                            <polyline id="Stroke-79" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="87.006 127.5488 89.04 129.6668 90.96 127.5488"></polyline>
                                            <path d="M76.3874,127.5488 L83.0524,127.5488"
                                                    id="Stroke-81"
                                                    stroke="#AFAFAF"
                                                    stroke-linecap="round"
                                                    stroke-linejoin="round"></path>
                                            <polyline id="Stroke-83" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="90.9601 127.5488 92.9931 129.6668 94.9141 127.5488 100.1101 127.5488"></polyline>
                                            <polyline id="Stroke-85" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    points="83.0524 127.5488 85.0854 129.6668 87.0064 127.5488"></polyline>
                                            <path id="Stroke-87" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"></path>
                                            <path d="M31.9064,115.3955 L146.0734,115.3955"
                                                    id="Stroke-89"
                                                    stroke="#AFAFAF"
                                                    stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    stroke-dasharray="0,2.965"></path>
                                            <path id="Stroke-91" stroke="#AFAFAF" stroke-linecap="round"
                                                    stroke-linejoin="round"></path>
                                            <path d="M125.7502,63.5254 L131.8542,63.5254"
                                                    id="Stroke-93"
                                                    stroke="#AFAFAF"
                                                    stroke-linecap="round"
                                                    stroke-linejoin="round"></path>
                                            <path d="M48.5822,63.1089 L53.5322,68.0589" id="Stroke-95" stroke="#E8B434"
                                                    stroke-linecap="round" stroke-linejoin="round"></path>
                                            <path d="M48.5822,68.0586 L53.5322,63.1086" id="Stroke-97" stroke="#E8B434"
                                                    stroke-linecap="round" stroke-linejoin="round"></path>
                                            <path d="M72.5822,63.1089 L77.5322,68.0589" id="Stroke-99" stroke="#E8B434"
                                                    stroke-linecap="round" stroke-linejoin="round"></path>
                                            <path d="M72.5822,68.0586 L77.5322,63.1086" id="Stroke-101" stroke="#E8B434"
                                                    stroke-linecap="round" stroke-linejoin="round"></path>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </p>

                    <p class="text-center bigText"><?= \Yii::t('UI8.7', '$PAGE_TITLE$') ?></p>

                    <p class="text-center smallText" id="open_searchbar"><?= \Yii::t('UI8.7',
                            '$OPEN_SEARCHBAR_BTN$') ?></p>
                </div>

                <div class="col-sm-12 headSearch" style="display: none" ng-controller="SearchBarController as SBC">
                    <?= Html::beginForm(['/search'], 'get', ['autocomplete' => 'off', 'class' => 'titleSearch']) ?>
                    <?= Html::activeHiddenInput($searchForm, 'isOnlyCategory', [
                        'ng-value' => 'onlyCategory',
                        'ng-init'  => 'onlyCategory = ' . (integer)$searchForm->isOnlyCategory,
                    ]) ?>

                    <div class="left" style="position: relative;">
                        <div class="leftBlockLeft">
                            <?= Html::activeHiddenInput($searchForm, 'category_id', [
                                'ng-value' => 'categoryId',
                                'ng-init'  => 'categoryId = ' . ($searchForm['category_id'] ?: 'null'),
                            ]) ?>

                            <a href="#" class="aTopLeft"><strong><?= \Yii::t('UI2',
                                        '$SEARCH_BAR_FIELD_WHAT_LABEL$') ?></strong></a>
                            <?= Html::activeTextInput($searchForm, 'note', [
                                'id'          => 'whoSearchLeft',
                                'class'       => 'inputSearchHeadLeft focusInput1',
                                'placeholder' => \Yii::t('UI2', '$SEARCH_BAR_FIELD_WHAT_PLACEHOLDER$'),
                                'ng-keypress' => 'enterWord($event)',
                                'ng-focus'    => 'isWhatFocus = true',
                                'ng-blur'     => 'isWhatFocus = false',
                            ]) ?>
                        </div>
                        <div class="rightBlockLeft">
                            <a href="#" onclick="return false;" class="btnBig">
                                <svg width="14px" height="14px" viewBox="0 0 8 14" version="1.1">
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"
                                            stroke-linecap="round" stroke-linejoin="round">
                                        <g id="btnLeft" transform="translate(-1207.000000, -591.000000)"
                                                stroke-width="2">
                                            <g id="Group-11" transform="translate(0.000000, 579.000000)">
                                                <g id="Recommended">
                                                    <g id="Lenta" transform="translate(210.000000, 0.000000)">
                                                        <g id="arrows" transform="translate(927.000000, 1.000000)">
                                                            <g id="Group-37" transform="translate(56.000000, 0.000000)">
                                                                <polyline id="arrow_right_black"
                                                                        transform="translate(17.935389, 17.699575) scale(-1, 1) rotate(90.000000) translate(-17.935389, -17.699575) "
                                                                        points="15 23.3991501 20.8707778 17.5283723 15.3424055 12"></polyline>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </div>

                        <div class="autocomplete_searchbar"
                                ng-show="asyncCompleteCategory.length && isWhatFocus"
                                ng-mousedown="setAutoWord($event)">
                            <div class="text-left scroll-1">
                                <ul class="list-unstyled">
                                    <li ng-repeat="a in asyncCompleteCategory track by a.id" data-id="{{a.id}}">
                                        <a ng-bind-html="a.name"></a>
                                        <!--                                            <a href="/search?PS[category_id]={{a.id}}" ng-bind-html="a.name"></a></li>-->
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="center">
                        <?= Html::activeHiddenInput($searchForm, 'location', [
                            'ng-value' => 'location',
                            'ng-init'  => 'location = '
                                . ($searchForm['location'] ? "'{$searchForm['location']}'" : 'null'),
                        ]) ?>

                        <a href="#" class="aTop"><strong><?= \Yii::t('UI2',
                                    '$SEARCH_BAR_FIELD_WHERE_LABEL$') ?></strong></a>
                        <?= Html::activeTextInput($searchForm, 'location_string', [
                            'id'          => 'whoSearchCenter',
                            'class'       => 'inputSearchHead search-autocomplete',
                            'placeholder' => \Yii::t('UI2', '$SEARCH_BAR_FIELD_WHERE_PLACEHOLDER$'),
                        ]) ?>
                    </div>

                    <div class="right">
                        <div class="leftBlockRight">
                            <a href="#" class="aTopRight"><strong><?= \Yii::t('UI2',
                                        '$SEARCH_BAR_FIELD_WHEN_LABEL$') ?></strong></a>

                            <?= Html::activeHiddenInput($searchForm, 'period_from', [
                                'ng-value' => 'period_from',
                                'ng-init'  => 'period_from = '
                                    . ($searchForm['period_from'] ? "{$searchForm['period_from']}" : 'undefined'),
                            ]) ?>
                            <?= Html::activeHiddenInput($searchForm, 'period_to', [
                                'ng-value' => 'period_to',
                                'ng-init'  => 'period_to = '
                                    . ($searchForm['period_to'] ? "{$searchForm['period_to']}" : 'undefined'),
                            ]) ?>

                            <div class="someBlock">
                                <div class="leftSomeBlock">
                                    <?= Html::textInput('period_from', null, [
                                        'class'       => 'getInput inputDate',
                                        'id'          => 'getDate',
                                        'role'        => 'datepicker1',
                                        'placeholder' => \Yii::t('UI2', '$SEARCH_BAR_FIELD_WHEN_FROM_PLACEHOLDER$'),
                                    ]) ?>
                                </div>

                                <div class="rightSomeBlock">
                                    <?= Html::textInput('period_to', null, [
                                        'class'       => 'returnInput inputDate',
                                        'id'          => 'returnDate',
                                        'role'        => 'datepicker1',
                                        'placeholder' => \Yii::t('UI2', '$SEARCH_BAR_FIELD_WHEN_TO_PLACEHOLDER$'),
                                    ]) ?>
                                </div>

                            </div>
                        </div>
                        <div class="rightBlockRight">
                            <?= Html::submitButton('Найти', ['class' => 'btnSearch']) ?>
                        </div>
                    </div>
                    <?= Html::endForm() ?>
                </div>

                <div class="bottomBtn">
                    <a href="/" class="green btn"><?= \Yii::t('UI8.7', '$GOTO_HOME_BTN$') ?></a>
                    <a href="/category" class="red btn"><?= \Yii::t('UI8.7', '$ALL_CATEGORIES_BTN$') ?></a>
                </div>
            </div>
        </div>
    </div>

    <!--        КЛІК НА КНОПКУ В ПОЛІ ШТО І ВІДКРИВАЄ СПИСОК-->
    <div class="col-sm-9 selectProductTitle" style="display: none;">
        <div class="row">
            <div class="leftSelect">
                <ul class="list-unstyled">
                    <?php foreach ($a as $k => $i): ?>
                        <?= Html::tag('li', $i['name'], ['id' => $k]) ?>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="leftSelectDisplay">
                <span><img src="/img/popup/mail.svg" alt=""></span>
                <span>Выберите категорию товара</span>
            </div>
            <div class="centerSelect scroll-1">
                <ul class="list-unstyled " style="display: none"></ul>
            </div>
            <div class="rightSelect scroll-1">
                <ul class="list-unstyled" style="display: none"></ul>
            </div>
        </div>
    </div>


<?php
$this->registerJsFile('/js/grom.js', [
    'position' => \yii\web\View::POS_END,
    'depends'  => ['frontend\assets\AppAsset', 'frontend\assets\FrontendAsset'],
]);
$this->registerJsFile('/js/category.js', [
    'position' => \yii\web\View::POS_END,
    'depends'  => [
        'frontend\assets\AppAsset',
        'frontend\assets\FrontendAsset',
        '\frontend\assets\DateRangePickerAsset',
    ],
]);

$this->registerJs(<<<JS
jQuery("#open_searchbar").one("click", function(){
    jQuery("#open_searchbar").slideUp(300, function(){
        jQuery("#open_searchbar").remove();
    });
    jQuery(".windowFound .headSearch").slideDown(300);
});
JS
    , \yii\web\View::POS_READY);