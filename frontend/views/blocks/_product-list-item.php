<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 12/8/2017
 * Time: 5:32 PM
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use frontend\models\ProductCurrency;

/** @var \yii\web\View $this */
/** @var \common\models\Product $model */

$step = [1 => 'Day', 'Week', 'Month', 'Hour'];
$media = ArrayHelper::toArray($model['media'], [
        \common\models\ProductMedia::className() => []
]);
//print_r($media);print '<br><br>';return;
?>

<?php if (!count($media)) {
    $media[] = [
        'link' => '/img/category/imgCategory/defolt.svg',
        'class' => ['defoltImg']
    ];
} else{
    foreach($media as $k => $item){
        if (empty($item['link'])){
            $media[ $k ]['link'] = '/img/category/imgCategory/defolt.svg';
            $media[ $k ]['class'] = ['defoltImg'];
        }
    }
}
//print_r($prd['author']);exit;
$avatars = ArrayHelper::getValue($model, 'author.userProfile.avatars');
reset($avatars);
$avatar = ArrayHelper::getValue($avatars, '0.path', '/img/category/man.png');

$countMiniSlides = count($model['media']);
$i               = 1;

$isFavorite = (boolean)$model['isFavorite'];
?>


<div class="paddSlide col-sm-6 col-md-4 product_slider_item">
    <div class="thumbnail pageProduct">
        <div class="slider1">
            <?php foreach ($media as $item): ?>

                <?= Html::beginTag('div', ['class' => implode(' ', array_merge(['sl'], ArrayHelper::getValue($item, 'class', [])))]); ?>

                <?php
                $options = ['class' => 'favorite_status', 'data' => ['id' => $model['id']]];
                if ($isFavorite) {
                    Html::addCssClass($options, 'active');
                }
                print Html::beginTag('span', $options); ?>

                <span class="glyphiconHeart without">
                         <svg xmlns="http://www.w3.org/2000/svg" width="21" height="19" viewBox="-1 -1 22 20">
                              <path  fill-rule="evenodd" d="M9.84718182,17.6587043 L9.5,17.8116311 L9.15281818,17.6587043 C4.69040909,14.9064558 0,8.64166921 0,5.16214939 C0.0151136364,2.31301829 2.33354545,3.85870198e-16 5.18181818,3.85870198e-16 C6.98206818,3.85870198e-16 8.57115909,0.924512195 9.5,2.32387957 C10.4288409,0.924512195 12.0179318,0 13.8181818,0 C16.6664545,0 18.9848864,2.31301829 19,5.16214939 C19,8.64166921 14.3095909,14.9064558 9.84718182,17.6587043 L9.84718182,17.6587043 Z"/>
                            </svg>
                        </span>
                <span class="glyphicon glyphicon-heart in"></span>
                <?= Html::endTag('span'); ?>


                <?= Html::a(Html::img($item['link'],
                ['alt' => '', 'class' => 'imgSl']), ["/{$model['slug']}"], []) ?>

                <div class="textSl">
                    <p class="pSl"><?= \Yii::t('UI-slider', '$SLIDER_PHOTO_NUMBER$',
                            ['index' => $i, 'count' => $countMiniSlides]) ?></p>
                </div>
                <?= Html::endTag('div'); ?>
                <?php $i++ ?>

            <?php endforeach; ?>
        </div>

        <div class="caption">
            <h3 class="longName"><?= Html::a(Html::encode($model['name']), ["/{$model['slug']}"], []) ?></h3>
            <?= Html::a(Html::encode($model['category']['name']),
                ['/category/search', 'fields' => $model['category']['name']],
                ['class' => 'btn btnTrans', 'role' => 'button']) ?>

            <a href="#" hidden class="advertising">Рекламма</a>
            <table class="table-block table-responsive">
                <tr>
                    <td><?= \Yii::t('UI-slider',
                            '$PRICE_PER_' . strtoupper($step[ $model['lease_time'] ]) . '_LABEL$') ?></td>
                    <td class="text-right">
                        <strong><?= ProductCurrency::formatPrice($model['price'],
                                (integer)$model['currency_id']) ?></strong>
                    </td>
                </tr>
                <tr>
                    <td><?= \Yii::t('UI-slider', '$PLEDGE_PRICE_LABEL$'); ?></td>
                    <td class="text-right"><?= ProductCurrency::formatPrice($model['pledge_price'],
                            (integer)$model['currency_id']); ?></td>
                </tr>
                <tr>
                    <td><?= \Yii::t('UI-slider', '$MINIMAL_PERIOD_LABEL$') ?></td>
                    <td class="text-right">
                        <strong><?= \Yii::t('UI-slider',
                                '$MINIMAL_PERIOD_' . strtoupper($step[ $model['lease_time'] ]) . '$',
                                ['min_period' => $model['min_period']]); ?></strong></td>
                </tr>
                <tr>
                    <td><?= \Yii::t('UI-slider', '$RATING_LABEL$'); ?></td>
                    <td class="text-right">
                        <div class="rateyoHome"></div>
                    </td>
                </tr>
            </table>

            <div class="row marg">
                <div class="col-xs-12 rowBottom">
                    <div class="margLeft">
                        <span class="face">
                            <?= Html::a(Html::img($avatar, ['class' => 'menFase']),
                                ['/profile', 'id' => $model['creator_id']]); ?>
                        </span>
                        <span class="mail">
                            <img class="menFase" src="/img/editProfile/feedback.svg" alt=""><?= /* count comments */
                            '??' ?>
                        </span>
                    </div>

                    <?php if ((int)$model['autoorder']): ?>
                        <span class="leftRightBtn"></span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
