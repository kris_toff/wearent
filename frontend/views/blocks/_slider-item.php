<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 04.05.2017
 * Time: 12:35
 */

use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

/** @var \yii\web\View $this */
/** @var array $model свойства товара для слайдера
 * minislide => [ [src => ''] ],
 * product_name => '',
 * slug => '',
 * category => '',
 * price => 0,
 * currency => '',
 * min_period => 0,
 * step => '',
 * countComments => 0
 * */

if (!count($model['minislide'])) {
    $model['minislide'][] = [
        'src' => '/img/category/imgCategory/defolt.svg',
        'class' => ['defoltImg']
    ];
}

$countMiniSlides = count($model['minislide']);
$i = 1;
?>


<div class="paddSlide">
    <div class="thumbnail pageProduct">
        <div class="slider1">

            <?php foreach ($model['minislide'] as $item): ?>
            <?= Html::beginTag('div', ['class' => implode(' ', array_merge(['sl'], ArrayHelper::getValue($item, 'class', [])))]); ?>
                        <span class="glyphiconHeart">
                             <svg xmlns="http://www.w3.org/2000/svg" width="21" height="19" viewBox="-1 -1 22 20">
                              <path  fill-rule="evenodd" d="M9.84718182,17.6587043 L9.5,17.8116311 L9.15281818,17.6587043 C4.69040909,14.9064558 0,8.64166921 0,5.16214939 C0.0151136364,2.31301829 2.33354545,3.85870198e-16 5.18181818,3.85870198e-16 C6.98206818,3.85870198e-16 8.57115909,0.924512195 9.5,2.32387957 C10.4288409,0.924512195 12.0179318,0 13.8181818,0 C16.6664545,0 18.9848864,2.31301829 19,5.16214939 C19,8.64166921 14.3095909,14.9064558 9.84718182,17.6587043 L9.84718182,17.6587043 Z"/>
                            </svg>

                        </span>
                    <?= Html::a(Html::img($item['src'], ['alt' => '', 'class' => 'imgSl']), ['/product', 'name' => $model['slug']]) ?>
                    <div class="textSl">
                        <p class="pSl">
                            <?= \Yii::t('UI-slider', '$SLIDER_PHOTO_NUMBER$', [
                                'index' => $i,
                                'count' => $countMiniSlides,
                            ]) ?>
                        </p>
                    </div>
            <?= Html::endTag('div'); ?>

                <?php $i++; endforeach; ?>

        </div>

        <div class="caption">
            <?= Html::a(Html::tag('h3', Html::encode($model['product_name']), ['class' => 'longName']),
                ['/product', 'name' => $model['slug']]) ?>

            <?= Html::a(Html::encode($model['category']), ['/search', ['PS' => ['category_id' => $model['category']]]],
                ['class' => 'btn btnTrans', 'role' => 'button']) ?>

            <a href="#" hidden class="advertising">Рекламма</a>
            <table class="table-block table-responsive">
                <tr>
                    <td><?= \Yii::t('UI-slider',
                            '$PRICE_PER_' . strtoupper($step[ $model['lease_time'] ]) . '_LABEL$') \Yii::t('frontend', 'Цена за сутки') ?></td>
                    <td class="text-right">
                        <?= \Yii::$app->formatter->asCurrency($model['price'], $model['currency']) ?>
                    </td>
                </tr>
                <tr>
                    <td>Сумма залога</td>
                    <td class="text-right"><?= \Yii::$app->formatter->asCurrency($model['price'], $model['currency']) ?></td>
                </tr>
                <tr>
                    <td>Мин. срок аренды</td>
                    <td class="text-right"><?= "{$model['min_period']} {$model['step']}" ?> </td>
                </tr>
                <tr>
                    <td>Оценка товара</td>
                    <td class="text-right">
                        <div class="rateyoHome" data-rating="1"></div>
                    </td>
                </tr>

            </table>


            <div class="row margg">
                <div class="col-xs-12 rowBottom">
                    <div class="margLeft">
                        <span class="face">
                            <a href="#">
                                <img class="menFase" src="/img/category/man.png" alt="">
                            </a>
                        </span>
                        <span class="mail">
                            <img class="menFase" src="img/home/mail.svg" alt="">&nbsp; <?= $model['countComments'] ?>
                        </span>
                    </div>

                    <div hidden class="markProduct">
                        <span class="count">25</span>
                    </div>

                    <span class="leftRightBtn"></span>
                </div>
            </div>
        </div>

    </div>
</div>


