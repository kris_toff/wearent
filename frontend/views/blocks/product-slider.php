<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 04.05.2017
 * Time: 12:22
 */

use yii\bootstrap\Html;

/** @var \yii\web\View $this */
/** @var string $title */
/** @var array $slides */

\frontend\assets\SliderAsset::register($this);

?>


<div class="col-sm-12 content">
    <!--    TITLE PAGE-->
    <div class="row title">
        <div class="col-sm-7 col-xs-8 text-left">
            <h3><?= Html::encode($title) ?></h3>
        </div>
        <div class="col-sm-5 col-xs-4 leftSlider text-right form"></div>
    </div>
    <!--    END TITLE PAGE-->

    <div class="row visited slider2 product_slider">

        <?php foreach ($slides as $item): ?>

            <?= $this->render('_slider-item', ['model' => $item]) ?>

        <?php endforeach; ?>

    </div>
</div>
