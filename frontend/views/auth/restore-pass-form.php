<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 12.05.2017
 * Time: 13:59
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\widgets\Pjax;

?>

<?php Pjax::begin([
    'id' => 'restore-password-form-pjax',
    'enablePushState'    => false,
    'enableReplaceState' => false,
    'timeout'            => 3000,
]) ?>

<p  class="headWindowPass text-center">Восстановление пароля</p>


<?php $form = ActiveForm::begin([
    'id' => 'restore-password-form',
    'action' => ['/auth/restore'],
    'options' => [
        'data-pjax' => '',
    ]
]) ?>

<?= $form->field($model, 'email')->textInput() ?>

<?= Html::submitButton('Восстановление пароля', ['class' => 'btn btn-block btn-lg']) ?>

<?php ActiveForm::end() ?>


<?php Pjax::end() ?>
