<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 11.05.2017
 * Time: 15:26
 */

use yii\widgets\Pjax;

?>

<?php Pjax::begin([
    'id' => 'auth-register-form-pjax',
]) ?>

<p class="text-center"><b>Регистрация успешно завершена</b></p>
<p class="text-center">Пожалуйста, проверьте почту, для подтверждения электронного адреса.</p>

<?php Pjax::end() ?>
