<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 23.05.2017
 * Time: 11:39
 */

/** @var \yii\web\View $this */
/** @var integer|null $time_to */

use yii\widgets\Pjax;
use yii\bootstrap\Html;

$df = null;
if ((int)$time_to > 0) {
    $df = (int)$time_to - time();

    $days = floor($df / 86400);
    $df   -= $days * 86400;

    $hours = floor($df / 3600);
    $df    -= $hours * 3600;

    $minutes = floor($df / 60);
    $seconds = $df - $minutes * 60;
}

?>

<?php Pjax::begin([
    'id' => 'auth-login-form-pjax',
]); ?>

    <div class="text-center">
        <p>Аккаунт временно заблокирован</p>
        <?php if (!is_null($df) && $df > 0): ?>
            <p>Будет разблокирован через: <?= Html::tag('span',
                    \Yii::t('frontend', '{days, plural, =0{} =1{1 day} one{# day} few{# days} many{# days} other{# days}}, {hours, number} {hours, plural, =0{hour} =1{hour} one{hour} few{hours} many{hours} other{hours}}, {minutes, number} {minutes, plural, =0{minute} =1{minute} one{minute} few{minutes} many{minutes} other{minutes}}', ['days' => $days, 'hours' => $hours, 'minutes' => $minutes]),
                    ['data-time' => ((int)$time_to) * 1000]) ?></p>
        <?php else: ?>
            <p>Для более детальной информации обращайтесь в службу поддержки.</p>
        <?php endif; ?>
    </div>

<?php Pjax::end() ?>


