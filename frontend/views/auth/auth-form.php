<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 11.05.2017
 * Time: 15:08
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\widgets\Pjax;

/** @var \yii\web\View $this */
/** @var \frontend\modules\user\models\LoginForm $model */
/** @var string $v */

?>


<?php Pjax::begin([
    'id'                 => "auth-login-form-{$v}-pjax",
    'enablePushState'    => false,
    'enableReplaceState' => false,
    'timeout'            => 3000,
]) ?>

<?php $form = ActiveForm::begin([
    'id'      => "auth-login-{$v}-form",
    'action'  => ['/auth/auth'],
    'options' => [
        'data-pjax' => '',
    ],
]) ?>

<?= $form->field($model, 'identity', [
    'template' => "{label}\n{input}\n<span class=\"glyphicon glyphicon-ok form-control-feedback\" aria-hidden=\"true\"></span>\n{hint}\n{error}",
    'options'  => [
        'class' => 'form-group has-feedback',
    ],
])->textInput() ?>
<?= $form->field($model, 'password')->passwordInput() ?>
<?= $form->field($model, 'rememberMe')->checkbox(['uncheck' => false])->label('Не выходить из аккаунта') ?>

<?= Html::submitButton('Войти', ['class' => 'btn btn-block btn-lg']) ?>
<?php ActiveForm::end() ?>

<a href="#" class="generatePass" data-toggle="modal" data-target="#generatePass">Восстановить пароль</a>
<?php Pjax::end() ?>

<?php
$this->registerJs(<<<JS
jQuery("#auth-login-form-desktop-pjax,#auth-login-form-mobile-pjax")
.on("pjax:beforeSend", function(event, xhr, options){
    console.log(xhr,options);
    options.data.append("return_url", window.location.pathname);
});
JS
, \yii\web\View::POS_READY, 'login-form');