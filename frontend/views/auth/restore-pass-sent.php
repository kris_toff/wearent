<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 12.05.2017
 * Time: 14:36
 */

use yii\widgets\Pjax;

/** @var \yii\web\View $this */
/** @var string $text */

?>

<?php Pjax::begin([
    'id'                 => 'restore-password-form-pjax',
    'enablePushState'    => false,
    'enableReplaceState' => false,
    'timeout'            => 3000,
]) ?>

<p class="text-center titlePageFedback">Успех!</p>
<p class="text-center">Вам на почту было отправлено письмо для восстановления пароля</p>

<a href="/auth/resend-restore" class="text-center">Отправить письмо повторно</a>
<?= empty($text) ? '' : "<p class='text-center'>$text</p>"; ?>
<?php Pjax::end() ?>

