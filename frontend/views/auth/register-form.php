<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 11.05.2017
 * Time: 15:08
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\widgets\Pjax;

/** @var \yii\web\View $this */
/** @var \frontend\modules\user\models\SignupForm $model */
/** @var string $v */

?>

<?php Pjax::begin([
    'id'                 => "auth-register-form-{$v}-pjax",
    'enablePushState'    => false,
    'enableReplaceState' => false,
    'timeout'            => 3000,
]) ?>
<?php //print_r($model->errors); ?>
<?php $form = ActiveForm::begin([
    'id'      => "auth-register-{$v}-form",
    'action' => ['/auth/register'],
    'options' => [
        'data-pjax' => '',
    ],
]) ?>

<?= $form->field($model, 'firstName', [
    'template' => "{label}\n{input}\n<span class=\"glyphicon glyphicon-ok form-control-feedback\" aria-hidden=\"true\"></span>\n{hint}\n{error}",
    'options'  => [
        'class' => 'form-group has-feedback',
    ],
])->textInput() ?>
<?= $form->field($model, 'lastName', [
    'template' => "{label}\n{input}\n<span class=\"glyphicon glyphicon-ok form-control-feedback\" aria-hidden=\"true\"></span>\n{hint}\n{error}",
    'options'  => [
        'class' => 'form-group has-feedback',
    ],
])->textInput() ?>
<?= $form->field($model, 'email')->textInput()->label(\Yii::t('frontend', 'Email')
    . ' (будет отправлена верификация):') ?>
<?= $form->field($model, 'password')->passwordInput() ?>

<?= $form->field($model, 'accept_terms')
    ->checkbox(['label' => 'Регистрируясь, я принимаю установленные сервисом все <a href="#" onclick="return false;">Правила и политики</a> сервиса и подтверждаю своё совершеннолетие.']) ?>

<?= Html::submitButton('Войти', ['class' => 'btn  btn-block btn-lg']) ?>
<?php ActiveForm::end() ?>
<?php Pjax::end() ?>