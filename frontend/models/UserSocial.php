<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 01.10.2017
 * Time: 17:18
 */

namespace frontend\models;


use common\models\User;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class UserSocial extends ActiveRecord {

    protected $_name;
    protected $_url;

    public static function tableName() {
        return '{{%user_social}}';
    }

    public function rules() {
        return [
            [['client', 'source_id'], 'required'],
            [['client', 'source_id', 'name', 'url'], 'string'],
        ];
    }

    public function getName() {
        return $this->_name;
    }

    public function getUrl() {
        return $this->_url;
    }

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class'              => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ],
        ];
    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}