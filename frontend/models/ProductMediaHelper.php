<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 21.07.2017
 * Time: 12:39
 */

namespace frontend\models;


use common\models\ProductMedia;
use yii\base\Object;

class ProductMediaHelper extends Object {
    /**
     * @param int $productId
     *
     * @return int Size of all files (in bytes)
     */
    public static function getSize($productId) {
        $webroot = \Yii::getAlias('@webroot');
        $size    = 0;

        foreach (ProductMedia::findAll(['is_deleted' => 0, 'product_id' => $productId]) as $k => $v) {
            $file = $webroot . '/' . ltrim($v['link'], '/');

            if (!file_exists($file)) {
                continue;
            }

            $size += filesize($file);
        }

        return $size;
    }

    public static function separateFiles($productId) {
        $webroot = \Yii::getAlias('@webroot');
        $mlc     = ['images' => [], 'videos' => [], 'other' => []];

        foreach (ProductMedia::findAll(['is_deleted' => 0, 'product_id' => $productId]) as $k => $v) {
            $file = $webroot . '/' . ltrim($v['link'], '/');
            if (!file_exists($file)) {
                continue;
            }

            switch (pathinfo($file, PATHINFO_EXTENSION)) {
            case 'png':
            case 'jpg':
            case 'jpeg':
                $ftype = 'images';
                break;
            case 'mp4':
                $ftype = 'videos';
                break;
            default:
                $ftype = 'other';
            }

            $mlc[ $ftype ][] = $v;
        }

        return $mlc;
    }
}