<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.10.2017
 * Time: 15:14
 */

namespace frontend\models;


use common\models\User;
use common\models\UserProfile;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

class UserDocument extends ActiveRecord {
    const SCENARIO_MODERATION = 'moderation';

    public $file;

    public static function tableName() {
        return '{{%user_documents}}';
    }

    public function rules() {
        return [
            [['file', 'type'], 'required'],
            ['file', 'file', 'maxSize' => 10 * pow(1024, 2), 'mimeTypes' => ['image/*']],

            [['type', 'cause'], 'trim'],
            ['type', 'string'],
            ['type', 'in', 'range' => ['passport', 'registration', 'other'], 'strict' => true],

            ['status', 'integer', 'min' => -1, 'max' => 1],
            [
                'status',
                'in',
                'range' => [-1, 1],
                'when'  => function($model) {
                    return !is_null($model->getOldAttribute('status'));
                },
            ],
            ['status', 'default', 'value' => null],

            [
                'cause',
                'required',
                'when' => function($model) {
                    return $model->status == -1;
                },
            ],
            ['cause', 'string', 'min' => 2, 'max' => 17215],
            [
                'cause',
                'filter',
                'filter' => function($value) {
                    return null;
                },
                'when'   => function($model) {
                    return in_array($model->status, [0, 1]);
                },
            ],

            ['is_deleted', 'boolean'],
            ['is_deleted', 'default', 'value' => false],
        ];
    }

    public function scenarios() {
        $scenarios                              = parent::scenarios();
        $scenarios[ self::SCENARIO_MODERATION ] = ['status', 'cause'];

        return $scenarios;
    }

    public function afterValidate() {
        if (!empty($this->file)) {
            $this->name      = $this->file->name;
            $this->mime_type = $this->file->type;
            $this->size      = $this->file->size;
        }

        parent::afterValidate();
    }


    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);

        if (!empty($this->file)) {
            $rootPath = \Yii::getAlias('@documents');
            $relPath  = "person{$this->user_id}";
            $path     = "{$rootPath}/{$relPath}";

            FileHelper::createDirectory($path);

            do {
                $uniqName = \Yii::$app->security->generateRandomString(12) . '.' . $this->file->extension;
            } while (file_exists("{$path}/{$uniqName}"));

            $relPath .= "/{$uniqName}";

            if ($this->file->saveAs("{$rootPath}/{$relPath}")) {
                $this->updateAttributes(['path' => $relPath]);
            }
        }
    }

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            'softDeleteBehavior' => [
                'class'                     => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'is_deleted' => true,
                ],
                'replaceRegularDelete'      => true,
            ],
            [
                'class'              => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ],
        ];
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserProfile() {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
    }
}