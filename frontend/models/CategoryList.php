<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 18.05.2017
 * Time: 17:12
 */

namespace frontend\models;


use yii\base\Object;
use yiimodules\categories\models\Categories;

class CategoryList extends Object {
    public static function tree() {
        $r = Categories::find()
            ->select(['id', 'parent_id', 'name', 'img' => 'image'])
            ->where(['is_active' => 1])
            ->asArray()
            ->indexBy('id')
            ->all();

        $root = array_filter($r, function($v) {
            return (int)$v['parent_id'] == 0;
        });
        $rest = array_diff_key($r, $root);

        return self::nestedList($root, $rest);
    }

    protected static function nestedList($r, $e){
        foreach($r as $root_key => $root_item){
            $n = array_filter($e, function($v)use($root_key){
                return (int)$v['parent_id'] == $root_key;
            });

            if (count($n)>0){
                $r[$root_key]['node'] = self::nestedList($n, array_diff_key($e, $n));
            }
        }

        return $r;
    }
}