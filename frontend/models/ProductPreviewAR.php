<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.08.2017
 * Time: 22:01
 */

namespace frontend\models;


use common\models\Product;
use yii\helpers\ArrayHelper;

class ProductPreviewAR extends Product {
    public $description;
    public $category_id;
    public $media;

    public $minRent;
    public $minRentVal;
    public $maxRent;
    public $maxRentVal;

    public $advancebooking  = 0;
    public $restbetweenrest = 0;
    public $futureplans     = 180;

    public $dealTimeFrom = '09:00';
    public $dealTimeTo   = '22:00';
    public $timeZone;

    public $price;
    public $currency_id;
    public $guaranty;
    public $precond = [];
    public $discountPrice;
    public $discountGuaranty;

    public $address;        // полный адрес словами
    public $address_id;     // идентификатор места google maps api
    public $address_lat;    // широта места
    public $address_lng;    // долгота места
    public $deliverycost;   // цена за доставку (если радиус доставки > 0)
    public $deliverydistance;   // радиус доставки товара с места (в км)

    public $autoOrder;
    public $autoRate   = 500;
    public $onlyVerify = false;

    public $exclude_dates;
    public $exclude_lock_after;

    public $timezonelist = [
        'Pacific/Midway'             => -39600,
        'Pacific/Honolulu'           => -36000,
        'America/Anchorage'          => -28800,
        'America/Los_Angeles'        => -25200,
        'America/Costa_Rica'         => -21600,
        'America/Chicago'            => -18000,
        'America/New_York'           => -14400,
        'America/Argentina/La_Rioja' => -10800,
        'America/Godthab'            => -7200,
        'Atlantic/Cape_Verde'        => -3600,
        'Atlantic/Reykjavik'         => 0,
        'Europe/London'              => 3600,
        'Europe/Stockholm'           => 7200,
        'Europe/Moscow'              => 10800,
        'Asia/Tbilisi'               => 14400,
        'Asia/Kabul'                 => 16200,
        'Asia/Yekaterinburg'         => 18000,
        'Asia/Omsk'                  => 21600,
        'Asia/Novosibirsk'           => 25200,
        'Asia/Irkutsk'               => 28800,
        'Asia/Yakutsk'               => 32400,
        'Australia/Sydney'           => 36000,
        'Asia/Sakhalin'              => 39600,
        'Asia/Kamchatka'             => 43200,
        'Pacific/Enderbury'          => 46800,
    ];

    public static function tableName() {
        return '{{%preview_product}}';
    }

    public function init() {
        \Yii::$app->db->createCommand('CREATE TEMPORARY TABLE {{%preview_product}} LIKE {{product}}')->execute();
        \Yii::$app->db->createCommand('CREATE TEMPORARY TABLE {{%eav_attribute_value}} SELECT * FROM {{%eav_attribute_value}} WHERE 1 LIMIT 1')
            ->execute();

        \Yii::$app->db->createCommand()->alterColumn('{{%preview_product}}', 'time_from', 'TIME')->execute();
        \Yii::$app->db->createCommand()->alterColumn('{{%preview_product}}', 'time_to', 'TIME')->execute();
        \Yii::$app->db->createCommand()->alterColumn('{{%preview_product}}', 'place_id', 'VARCHAR(255)')->execute();
        \Yii::$app->db->createCommand()->alterColumn('{{%preview_product}}', 'full_address', 'TEXT')->execute();
        \Yii::$app->db->createCommand()->alterColumn('{{%preview_product}}', 'lat', 'FLOAT')->execute();
        \Yii::$app->db->createCommand()->alterColumn('{{%preview_product}}', 'lng', 'FLOAT')->execute();

        parent::init();

        $this->scenario =self::SCENARIO_DRAFT;
    }

    public function formName() {
        return (new ProductForm())->formName();
    }

    public function rules() {
        $rules = array_merge(parent::rules(), [
            ['id', 'safe'],
            [['category_id', 'address', 'address_id', 'address_lat', 'address_lng', 'currency', 'guaranty'], 'safe'],
        ]);

        return $rules;
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_DRAFT]=array_merge($scenarios[self::SCENARIO_DRAFT], ['id', 'guaranty']);

        return $scenarios;
    }


    public function loadProduct($data) {
        $data[ $this->formName() ]['category_id'] = ArrayHelper::getValue($data, [$this->formName(), 'category']);
        $data[ $this->formName() ]['currency_id'] = ArrayHelper::getValue($data, [$this->formName(), 'currency']);
        unset($data[ $this->formName()]['category'], $data[$this->formName()]['currency']);
//print_r($data);exit;
//        print $this->scenario;print_r($this->activeAttributes());exit;
        $this->id = ArrayHelper::getValue($data, [$this->formName(), 'id']);
        if (!$this->load($data)) {
            return false;
        }
//print $this->guaranty;print $data[$this->formName() ]['guaranty'];exit;
        return $this->load(ArrayHelper::toArray($this, [
            self::className() => [
                'place_id'           => 'address_id',
                'lat'                => 'address_lat',
                'lng'                => 'address_lng',
                'full_address'       => 'address',
                'delivery_radius'    => function($model) {
                    // переводим км в м
                    return $model->deliverydistance * 1000;
                },
                'delivery_price'     => 'deliverycost',
                'pledge_price'       => 'guaranty',
                'lease_time'         => 'minRentVal',
                'lease_max_time'     => 'maxRentVal',
                'min_period'         => 'minRent',
                'max_period'         => 'maxRent',
                'discount_price'     => 'discountPrice',
                'discount_pledge'    => 'discountGuaranty',
                'autoorder'          => 'autoOrder',
                'autoorder_rate'     => 'autoRate',
                'is_only_verify'     => 'onlyVerify',
                'prenotify'          => 'advancebooking',
                'restbetweenbooking' => 'restbetweenrest',
                'beforehand_rent'    => 'futureplans',
                'time_from'          => 'dealTimeFrom',
                'time_to'            => 'dealTimeTo',
                'timezone'           => function($model) {
                    return ArrayHelper::getValue($this->timezonelist, $model->timeZone, 0);
                },
            ],
        ]), '');
    }

}