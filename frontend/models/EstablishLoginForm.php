<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 30.09.2017
 * Time: 19:41
 */

namespace frontend\models;


use common\models\User;
use yii\base\Model;

class EstablishLoginForm extends Model {
    public $email;
    public $sp;
    public $rp;

    public function rules() {
        return [
            [['id', 'email', 'sp', 'rp'], 'required'],
            ['email', 'email'],
            [['sp', 'rp'], 'string', 'length' => [8, 45]],
            ['rp', 'compare', 'compareAttribute' => 'sp', 'operator' => '==='],
        ];
    }

    public function getId() {
        return \Yii::$app->user->id;
    }

    public function save($validate = true) {
        if ($validate && !$this->validate()) {
            return false;
        }

        $model                = \Yii::$app->user->identity;
        $model->email         = $this->email;
        $model->password_hash = \Yii::$app->security->generatePasswordHash($this->sp);

        return $model->save();
    }
}