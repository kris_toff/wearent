<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 12.05.2017
 * Time: 15:53
 */

namespace frontend\models\components;


use frontend\models\UserSocial;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;

class AuthHandler {
    private $client;

    public function __construct(ClientInterface $client) {
        $this->client = $client;
    }

    public function handle() {
        $attributes = $this->client->getUserAttributes();

        $auth = UserSocial::findOne(['user_id' => \Yii::$app->user->id, 'client' => $this->client->getId()]);

        if (\Yii::$app->user->isGuest) {
            if (is_null($auth)) {
                // sign up

                $auth             = new UserSocial();
                $auth->attributes = [
                    'client'    => $this->client->getId(),
                    'source_id' => $attributes['id'],
                ];
            } else {
                // auth
                \Yii::$app->user->login($auth->user, 360);
            }
        } else {
            if (is_null($auth)) {
                $auth             = new UserSocial();
                $auth->attributes = [
                    'client'    => $this->client->getId(),
                    'source_id' => (string)$attributes['id'],
                ];

                $auth->save();
            } else {
                // пользовательская запись этой соцсети уже найдена
                // ничего не делаем
            }
        }


//        print 'result:<pre>';
//        print_r($attributes); exit;
    }
}