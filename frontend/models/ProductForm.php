<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.07.2017
 * Time: 1:06
 */

namespace frontend\models;


use common\models\Currency;
use common\models\Product;
use common\models\ProductMedia;
use mirocow\eav\EavBehavior;
use mirocow\eav\models\EavAttribute;
use mirocow\eav\models\EavAttributeValue;
use yii\base\DynamicModel;
use yii\base\Model;
use yii\db\IntegrityException;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\validators\NumberValidator;
use yii\validators\RangeValidator;
use yiimodules\categories\models\Categories;

class ProductForm extends Model {
    const SCENARIO_MEDIA           = 'media';
    const SCENARIO_DRAFT           = 'draft';
    const SCENARIO_CREATE          = 'create';
    const SCENARIO_UPDATE_CATEGORY = 'update_category';

    public $id;
    public $name;
    public $description;
    public $category;
    public $media;

    public $minRent;
    public $minRentVal;
    public $maxRent;
    public $maxRentVal;

    public $advancebooking  = 0;
    public $restbetweenrest = 0;
    public $futureplans     = 180;

    public $dealTimeFrom = '09:00';
    public $dealTimeTo   = '22:00';
    public $timeZone;

    public $price;
    public $currency;
    public $guaranty;
    public $precond = [];
    public $discountPrice;
    public $discountGuaranty;

    public $address;        // полный адрес словами
    public $address_id;     // идентификатор места google maps api
    public $address_lat;    // широта места
    public $address_lng;    // долгота места
    public $deliverycost;   // цена за доставку (если радиус доставки > 0)
    public $deliverydistance;   // радиус доставки товара с места (в км)

    public $autoOrder;
    public $autoRate   = 500;
    public $onlyVerify = false;

    public $exclude_dates;
    public $exclude_lock_after;

    public  $timezonelist = [
        'Pacific/Midway'             => -39600,
        'Pacific/Honolulu'           => -36000,
        'America/Anchorage'          => -28800,
        'America/Los_Angeles'        => -25200,
        'America/Costa_Rica'         => -21600,
        'America/Chicago'            => -18000,
        'America/New_York'           => -14400,
        'America/Argentina/La_Rioja' => -10800,
        'America/Godthab'            => -7200,
        'Atlantic/Cape_Verde'        => -3600,
        'Atlantic/Reykjavik'         => 0,
        'Europe/London'              => 3600,
        'Europe/Stockholm'           => 7200,
        'Europe/Moscow'              => 10800,
        'Asia/Tbilisi'               => 14400,
        'Asia/Kabul'                 => 16200,
        'Asia/Yekaterinburg'         => 18000,
        'Asia/Omsk'                  => 21600,
        'Asia/Novosibirsk'           => 25200,
        'Asia/Irkutsk'               => 28800,
        'Asia/Yakutsk'               => 32400,
        'Australia/Sydney'           => 36000,
        'Asia/Sakhalin'              => 39600,
        'Asia/Kamchatka'             => 43200,
        'Pacific/Enderbury'          => 46800,
    ];
    private $_mediaType;
    /** @var Product $_product */
    private $_product;

    public static function getPrimaryKey() {
        return 'id';
    }

    public function rules() {
        return [
            [
                'id',
                'required',
                'when' => function() {
                    return !empty($this->_product);
                },
            ],
            [['category'], 'required'],
            [['id', 'category'], 'integer'],

            ['id', 'exist', 'targetClass' => Product::className(), 'targetAttribute' => 'id'],
            [
                'category',
                'exist',
                'targetClass'     => Categories::className(),
                'targetAttribute' => 'id',
                'filter'          => ['is_active' => 1],
            ],

            [['name', 'description'], 'trim'],
            ['name', 'string', 'min' => 2, 'max' => 255],
            ['description', 'string', 'max' => 65353],

            [['address', 'address_id', 'address_lat', 'address_lng'], 'required', 'except' => self::SCENARIO_DRAFT],
            [['address', 'address_id'], 'string', 'max' => 255],
            ['address_lat', 'double', 'min' => -90, 'max' => 90],
            ['address_lng', 'double', 'min' => -180, 'max' => 180],
            [['deliverydistance', 'deliverycost'], 'double', 'min' => 0],
            [['deliverydistance', 'deliverycost'], 'default', 'value' => 0],

            [['price', 'currency'], 'required', 'except' => self::SCENARIO_DRAFT],
            [['price', 'guaranty'], 'double', 'min' => 0],
            ['guaranty', 'default', 'value' => 0],
            ['currency', 'integer', 'min' => 1],
            [
                'currency',
                'exist',
                'targetClass'     => Currency::className(),
                'targetAttribute' => 'id',
                'filter'          => ['is_payment' => 1],
            ],

            [['minRent', 'maxRent'], 'integer', 'min' => 1],
            ['minRent', 'default', 'value' => 1],
            [['minRent', 'minRentVal'], 'required', 'except' => self::SCENARIO_DRAFT],
            [
                ['minRentVal', 'maxRentVal'],
                'in',
                'range' => [Product::LEASING_DAY, Product::LEASING_WEEK, Product::LEASING_MONTH, Product::LEASING_HOUR],
            ],

            [['onlyVerify'], 'boolean'],
            ['autoOrder', 'in', 'range' => [0, 1, 2]],
            ['autoOrder', 'default', 'value' => 0],
            ['onlyVerify', 'default', 'value' => false],
            ['autoRate', 'integer', 'min' => 1],
            [
                'autoRate',
                'required',
                'when'   => function($model) { return $model->autoOrder == 2; },
                'except' => self::SCENARIO_DRAFT,
            ],
            ['autoRate', 'default', 'value' => 500],

            [['restbetweenrest', 'advancebooking'], 'in', 'range' => [0, 1, 2, 3, 7]],
            [['restbetweenrest', 'advancebooking'], 'default', 'value' => 0],
            ['futureplans', 'in', 'range' => [90, 180, 365]],
            ['futureplans', 'default', 'value' => 180],

            ['media', 'file', 'maxSize' => 1024 * 1024 * 100],

            [['dealTimeFrom', 'dealTimeTo', 'timeZone'], 'required', 'except' => self::SCENARIO_DRAFT],
            [['dealTimeFrom', 'dealTimeTo'], 'time', 'format' => 'HH:mm'],
            [['dealTimeFrom', 'dealTimeTo'], 'default', 'value' => null],
            ['timeZone', 'in', 'range' => array_keys($this->timezonelist)],

            [['discountPrice', 'discountGuaranty'], 'double', 'min' => 0, 'max' => 100],

            [['exclude_dates', 'exclude_lock_after'], 'safe'],
            ['precond', 'safe'],
        ];
    }

    public function scenarios() {
        $scenarios                                   = parent::scenarios();
        $scenarios[ self::SCENARIO_MEDIA ]           = ['id', 'media'];
        $scenarios[ self::SCENARIO_DRAFT ]           = [
            'id',
            'name',
            'description',
            'category',
            'minRent',
            'minRentVal',
            'maxRent',
            'maxRentVal',
            'advancebooking',
            'restbetweenrest',
            'futureplans',
            'dealTimeFrom',
            'dealTimeTo',
            'timeZone',
            'price',
            'guaranty',
            'currency',
            'discountPrice',
            'discountGuaranty',
            'precond',
            'address',
            'address_id',
            'address_lat',
            'address_lng',
            'deliverycost',
            'deliverydistance',
            'autoOrder',
            'autoRate',
            'onlyVerify',
            'exclude_dates',
            'exclude_lock_after',
        ];
        $scenarios[ self::SCENARIO_CREATE ]          = ['category'];
        $scenarios[ self::SCENARIO_UPDATE_CATEGORY ] = ['category'];

        return $scenarios;
    }

    public function loadProduct($slug = null) {
        $query = Product::find();
        if (!is_null($slug)) {
            if (is_numeric($slug)) {
                $query->where(['id' => $slug]);
            } else {
                $query->where(['slug' => $slug]);
            }
        } else {
            $query->where(['is_draft' => 1, 'is_deleted' => 0]);
        }
        $query->andWhere(['creator_id' => \Yii::$app->user->id]);
        $product = $query->one();

        if (is_null($product)) {
            return false;
        }
        $this->_product = $product;
        $exclude_dates  = (new Query())->select('date_from,date_to')
            ->from('{{%product_exclude_dates}}')
            ->where(['product_id' => $product->id])
            ->all();

        if (count($exclude_dates)
            && (int)$exclude_dates[ count($exclude_dates) - 1 ]['date_to'] == 2147483647
        ) {
            $this->exclude_lock_after = 1000 * array_pop($exclude_dates)['date_from'];
        }
        $ed = array_reduce($exclude_dates, function($carry, $item) {
            if (is_numeric($item['date_to']) && (int)$item['date_to'] > 0) {
                array_merge($carry, range($item['date_from'], 1 + $item['date_to'], 86400));
            } else {
                $carry[] = $item['date_from'];
            }

            return $carry;
        }, []);
        array_walk($ed, function(&$v) {
            $v *= 1000;
        });

        $this->id            = $product->id;
        $this->exclude_dates = implode(',', $ed);

        // условные цены товара
        $this->precond = (new Query())
            ->select([
                'id',
                'price',
                'amount' => 'val',
                'lease_time',
            ])
            ->from('{{%product_price_condition}}')
            ->where(['product_id' => $product->id])
            ->indexBy('id')
            ->all();


        return $this->load(ArrayHelper::toArray($product, [
            Product::className() => [
                'category' => 'category_id',
                'name',
                'description',

                'minRent'    => 'min_period',
                'maxRent'    => 'max_period',
                'minRentVal' => 'lease_time',
                'maxRentVal' => 'lease_max_time',

                'advancebooking'   => 'prenotify',
                'restbetweenrest'  => 'restbetweenbooking',
                'futureplans'      => 'beforehand_rent',

                'price',
                'currency'         => 'currency_id',
                'guaranty'         => 'pledge_price',
                'discountPrice'    => 'discount_price',
                'discountGuaranty' => 'discount_pledge',

                'address'          => 'full_address',
                'address_id'       => 'place_id',
                'address_lat'      => 'lat',
                'address_lng'      => 'lng',
                'deliverycost'     => 'delivery_price',
                'deliverydistance' => function($model) {
                    return $model->delivery_radius / 1000;
                },

                'dealTimeFrom' => function($model) {
                    $t = explode(':', $model->time_from);

                    return implode(':', array_slice($t, 0, 2));
                },
                'dealTimeTo'   => function($model) {
                    $t = explode(':', $model->time_to);

                    return implode(':', array_slice($t, 0, 2));
                },
                'timeZone'     => function($model) {
                    return array_search($model->timezone, $this->timezonelist);
                },

                'autoOrder'  => 'autoorder',
                'autoRate'   => 'autoorder_rate',
                'onlyVerify' => 'is_only_verify',
            ],
        ], false), '');
    }

    public function save($validate = true) {
        if ($validate && !$this->validate()) {
//            print_r($this->errors);
//            exit;

            return false;
        }

        switch ($this->scenario) {
        case self::SCENARIO_MEDIA:
            return $this->saveMedia();
        case self::SCENARIO_DRAFT:
            return $this->saveDraft();
        case self::SCENARIO_CREATE:
            return $this->create();
        case self::SCENARIO_UPDATE_CATEGORY:
            return $this->update();
        default:
            return $this->fullSave();
        }
    }

    private function saveMedia() {
        if (empty($this->media)) {
            return false;
        }

        $r         = \Yii::$app->security->generateRandomString(13);
        $extension = $this->media->extension;

        $file     = "{$r}.{$extension}";
        $webroot  = \Yii::getAlias('@webroot');
        $webfoler = "/uploads/products/id{$this->id}";
        $webpath  = "$webfoler/$file";
        $path     = "$webroot/$webpath";

        FileHelper::createDirectory("$webroot/$webfoler");
        $this->media->saveAs($path);

        $model = new ProductMedia();
        switch ($extension) {
        case 'png':
        case 'jpg':
        case 'gif':
            $this->_mediaType = 'image';
            break;
        case 'mp4':
        case 'avi':
            $this->_mediaType = 'video';
            break;
        default:
            return false;
        }

        $model->attributes = [
            'product_id' => $this->id,
            'link'       => $webpath,
        ];

        return $model->save() ? $model : false;
    }

    public function saveDraft() {
        $product = $this->_product;
        $product->load(ArrayHelper::toArray($this, [
            ProductForm::className() => [
                'name',
                'description',
                'place_id'           => 'address_id',
                'lat'                => 'address_lat',
                'lng'                => 'address_lng',
                'full_address'       => 'address',
                'delivery_radius'    => function($model) {
                    // переводим км в м
                    return $model->deliverydistance * 1000;
                },
                'delivery_price'     => 'deliverycost',
                'currency_id'        => 'currency',
                'price',
                'pledge_price'       => 'guaranty',
                'lease_time'         => 'minRentVal',
                'lease_max_time'     => 'maxRentVal',
                'min_period'         => 'minRent',
                'max_period'         => 'maxRent',
                'discount_price'     => 'discountPrice',
                'discount_pledge'    => 'discountGuaranty',
                'autoorder'          => 'autoOrder',
                'autoorder_rate'     => 'autoRate',
                'is_only_verify'     => 'onlyVerify',
                'prenotify'          => 'advancebooking',
                'restbetweenbooking' => 'restbetweenrest',
                'beforehand_rent'    => 'futureplans',
                'time_from'          => 'dealTimeFrom',
                'time_to'            => 'dealTimeTo',
                'timezone'           => function($model) {
                    return ArrayHelper::getValue($this->timezonelist, $model->timeZone, 0);
                },
            ],
        ], false), '');
        $product->is_draft = true;

        // удалить записи с датами исключения из заказа продукта
        $db = \Yii::$app->db;
        $db->createCommand()->delete('{{%product_exclude_dates}}', ['product_id' => $product->id])->execute();
        // а теперь сформировать диапазон новых дат и добавить обратно
        $rawdates = array_diff(explode(',', $this->exclude_dates), ['', null]);
        array_walk($rawdates, function(&$v) {
            $v = (int)($v / 1000);
        });

        $rawdates = array_unique($rawdates, SORT_NUMERIC);
        sort($rawdates, SORT_NUMERIC);

        $ls   = [];
        $step = 86400;

        while (!empty($rawdates)) {
            $iteration = array_shift($rawdates);
            $temp      = [$product->id, $iteration, null];

            $iteration += $step;
            while (!empty($rawdates) && $iteration == $rawdates[0]) {
                $temp[2]   = array_shift($rawdates);
                $iteration += $step;
            }
            $ls[] = $temp;
        }

        if (!empty($this->exclude_lock_after) && (int)$this->exclude_lock_after > 0) {
            $ls[] = [$product->id, (int)($this->exclude_lock_after / 1000), 2147483647];
        }
        if (!empty($ls)) {
            $db->createCommand()->batchInsert('{{%product_exclude_dates}}', [
                'product_id',
                'date_from',
                'date_to',
            ], $ls)->execute();
        }

        // условные цены
        $prec    = is_null($this->precond) ? [] : $this->precond;
        $newPreC = ArrayHelper::remove($prec, 'new', []);

        $prec = array_filter($prec, function($o) {
            $model = DynamicModel::validateData($o, [
                [['price', 'amount', 'lease_time'], 'required'],
                ['price', 'double', 'min' => 0],
                ['amount', 'integer', 'min' => 1],
                [
                    'lease_time',
                    'in',
                    'range' => [
                        Product::LEASING_DAY,
                        Product::LEASING_WEEK,
                        Product::LEASING_MONTH,
                        Product::LEASING_HOUR,
                    ],
                ],
            ]);

            return !$model->hasErrors();
        });

        $db->createCommand()
            ->delete('{{%product_price_condition}}',
                ['and', ['product_id' => $product->id], ['not in', 'id', array_keys($prec)]])
            ->execute();
        foreach ($prec as $id => $item) {
            $db->createCommand()
                ->update('{{%product_price_condition}}',
                    ['price' => $item['price'], 'val' => $item['amount'], 'lease_time' => $item['lease_time']],
                    ['id' => $id, 'product_id' => $product->id])
                ->execute();
        }
        foreach ($newPreC as $item) {
            $model = DynamicModel::validateData($item, [
                [['price', 'amount', 'lease_time'], 'required'],
                ['price', 'double', 'min' => 0],
                ['amount', 'integer', 'min' => 1],
                [
                    'lease_time',
                    'in',
                    'range' => [
                        Product::LEASING_DAY,
                        Product::LEASING_WEEK,
                        Product::LEASING_MONTH,
                        Product::LEASING_HOUR,
                    ],
                ],
            ]);

            if ($model->hasErrors()) {
                continue;
            }

            $db->createCommand()->insert('{{%product_price_condition}}', [
                'product_id' => $product->id,
                'price'      => $item['price'],
                'val'        => $item['amount'],
                'lease_time' => $item['lease_time'],
            ])->execute();
        }

        try {
            $timeBeforeUpdate = $product->updated_at;
            $result           = $product->save();
        } catch (IntegrityException $e) {
            // ошибка при сохранеии, модель пытается сразу сохранять динамические атрибуты, которых может не быть. Не получается избежать такого поведения.
            print $e->getMessage();
            exit;
            if ($product->updated_at == $timeBeforeUpdate) {
                return false;
            } else {
                return true;
            }
        }

        return $result;
    }

    protected function create() {
        $db = \Yii::$app->db;

        $id = $db->createCommand('SELECT [[id]] FROM '
            . Product::tableName()
            . ' WHERE [[is_draft]] = 1 AND [[is_deleted]] = 0 AND [[creator_id]] = :creator')
            ->bindValue(':creator', \Yii::$app->user->id)
            ->queryScalar();
        if ((bool)$id) {
            // запись существует
            $query = $db->createCommand()
                ->update(Product::tableName(), ['category_id' => $this->category], ['id' => $id]);
        } else {
            $query = $db->createCommand()->insert(Product::tableName(), [
                'category_id'  => $this->category,
                'name'         => '',
                'place_id'     => '',
                'lat'          => 0,        // сюда попробовать через ip достать координаты приблизительные
                'lng'          => 0,        // сюда попробовать через ip достать координаты приблизительные
                'full_address' => '',
                'currency_id'  => 1,        // сюда ставить предпочитаемую пользователем,
                'price'        => 0,
                'time_from'    => '09:00',
                'time_to'      => '22:00',
                'timezone'     => 0,
                'is_draft'     => 1,
                'slug'         => 'newproductuser' . \Yii::$app->user->id,
                'creator_id'   => \Yii::$app->user->id,
                'created_at'   => time(),
                'updated_at'   => time(),
            ]);
        }
        try {
            $query->execute();
        } catch (IntegrityException $e) {
            print 'false' . $e->getMessage();
            exit;

            return false;
        }

        return true;
    }

    protected function update() {
        $db = \Yii::$app->db;

        return (bool)$db->createCommand()
            ->update(Product::tableName(), ['category_id' => $this->category],
                ['id' => $this->id, 'creator_id' => \Yii::$app->user->id])
            ->execute();
    }

    /**
     * Обычное сохранение товара
     */
    protected function fullSave() {
        $product = $this->_product;
        $product->load(ArrayHelper::toArray($this, [
            ProductForm::className() => [
                'name',
                'description',
                'place_id'           => 'address_id',
                'lat'                => 'address_lat',
                'lng'                => 'address_lng',
                'full_address'       => 'address',
                'delivery_radius'    => function($model) {
                    // переводим км в м
                    return $model->deliverydistance * 1000;
                },
                'delivery_price'     => 'deliverycost',
                'currency_id'        => 'currency',
                'price',
                'pledge_price'       => 'guaranty',
                'lease_time'         => 'minRentVal',
                'lease_max_time'     => 'maxRentVal',
                'min_period'         => 'minRent',
                'max_period'         => 'maxRent',
                'discount_price'     => 'discountPrice',
                'discount_pledge'    => 'discountGuaranty',
                'autoorder'          => 'autoOrder',
                'autoorder_rate'     => 'autoRate',
                'is_only_verify'     => 'onlyVerify',
                'status'             => function() {
                    return \Yii::$app->user->identity->verified ? 3 : 1;
                },
                'prenotify'          => 'advancebooking',
                'restbetweenbooking' => 'restbetweenrest',
                'beforehand_rent'    => 'futureplans',
                'time_from'          => 'dealTimeFrom',
                'time_to'            => 'dealTimeTo',
                'timezone'           => function($model) {
                    return ArrayHelper::getValue($this->timezonelist, $model->timeZone, 0);
                },
            ],
        ]), '');

        // удалить записи с датами исключения из заказа продукта
        $db = \Yii::$app->db;
        $db->createCommand()->delete('{{%product_exclude_dates}}', ['product_id' => $product->id])->execute();
        // а теперь сформировать диапазон новых дат и добавить обратно
        $rawdates = array_diff(explode(',', $this->exclude_dates), ['', null]);
        array_walk($rawdates, function(&$v) {
            $v = (int)($v / 1000);
        });

        $rawdates = array_unique($rawdates, SORT_NUMERIC);
        sort($rawdates, SORT_NUMERIC);

        $ls   = [];
        $step = 86400;

        while (!empty($rawdates)) {
            $iteration = array_shift($rawdates);
            $temp      = [$product->id, $iteration, null];

            $iteration += $step;
            while (!empty($rawdates) && $iteration == $rawdates[0]) {
                $temp[2]   = array_shift($rawdates);
                $iteration += $step;
            }
            $ls[] = $temp;
        }

        if (!empty($this->exclude_lock_after) && (int)$this->exclude_lock_after > 0) {
            $ls[] = [$product->id, (int)($this->exclude_lock_after / 1000), 2147483647];
        }
        if (!empty($ls)) {
            $db->createCommand()->batchInsert('{{%product_exclude_dates}}', [
                'product_id',
                'date_from',
                'date_to',
            ], $ls)->execute();
        }

        // условные цены
        $prec    = is_null($this->precond) ? [] : $this->precond;
        $newPreC = ArrayHelper::remove($prec, 'new', []);

        $prec = array_filter($prec, function($o) {
            $model = DynamicModel::validateData($o, [
                [['price', 'amount', 'lease_time'], 'required'],
                ['price', 'double', 'min' => 0],
                ['amount', 'integer', 'min' => 1],
                [
                    'lease_time',
                    'in',
                    'range' => [
                        Product::LEASING_DAY,
                        Product::LEASING_WEEK,
                        Product::LEASING_MONTH,
                        Product::LEASING_HOUR,
                    ],
                ],
            ]);

            return !$model->hasErrors();
        });

        $db->createCommand()
            ->delete('{{%product_price_condition}}',
                ['and', ['product_id' => $product->id], ['not in', 'id', array_keys($prec)]])
            ->execute();
        foreach ($prec as $id => $item) {
            $db->createCommand()
                ->update('{{%product_price_condition}}',
                    ['price' => $item['price'], 'val' => $item['amount'], 'lease_time' => $item['lease_time']],
                    ['id' => $id, 'product_id' => $product->id])
                ->execute();
        }
        foreach ($newPreC as $item) {
            $model = DynamicModel::validateData($item, [
                [['price', 'amount', 'lease_time'], 'required'],
                ['price', 'double', 'min' => 0],
                ['amount', 'integer', 'min' => 1],
                [
                    'lease_time',
                    'in',
                    'range' => [
                        Product::LEASING_DAY,
                        Product::LEASING_WEEK,
                        Product::LEASING_MONTH,
                        Product::LEASING_HOUR,
                    ],
                ],
            ]);

            if ($model->hasErrors()) {
                continue;
            }

            $db->createCommand()->insert('{{%product_price_condition}}', [
                'product_id' => $product->id,
                'price'      => $item['price'],
                'val'        => $item['amount'],
                'lease_time' => $item['lease_time'],
            ])->execute();
        }

//        try {
        $timeBeforeUpdate = $product->updated_at;
        $result           = $product->save();
//        } catch (IntegrityException $e) {
//            // ошибка при сохранеии, модель пытается сразу сохранять динамические атрибуты, которых может не быть. Не получается избежать такого поведения.
//            if ($product->updated_at == $timeBeforeUpdate) {
//                return false;
//            } else {
//                return true;
//            }
//        }

        return $result;
    }

    public function getMedia() {
        $queryP = ProductMedia::find()->where(['product_id' => $this->id, 'is_deleted' => 0]);

        return $queryP->orderBy([
            'position'   => SORT_ASC,
            'created_at' => SORT_ASC,
            'updated_at' => SORT_ASC,
        ]);
    }

    public function getMediatype() {
        return $this->_mediaType ?: 'other';
    }

    public function getMediaDefault() {
        return ArrayHelper::getValue($this->_product, 'media_default', 0);
    }

    public function getProduct() {
        return $this->_product;
    }
}