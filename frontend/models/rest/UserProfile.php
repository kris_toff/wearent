<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.08.2017
 * Time: 23:01
 */

namespace frontend\models\rest;

use common\models\UserProfile as BasicUserProfile;

class UserProfile extends BasicUserProfile {
    public function fields() {
        return [
            'fullName',
        ];
    }


}