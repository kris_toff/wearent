<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.08.2017
 * Time: 21:19
 */

namespace frontend\models\rest;

use common\models\Chat as BasicChat;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class Chat
 * @package frontend\models\rest
 *
 * @property string  $subject тема чата, задаётся автоматически при создании нового диалога
 * @property integer from_id  идентификатор пользователя, который создал чат и отправил первое сообщение
 * @property integer to_id    идентификатор пользователя, которому отправлялось сообщение
 *
 */
class Chat extends BasicChat {
    const SCENARIO_UPDATE     = 'update';
    const SCENARIO_UPDATE_OWN = 'uown';

    public $lastMessage;
    public $amountNewMessages = 0;
    public $isFavorite        = 0;

    private $_data = [];

    public function fields() {
        return [
            'id',
            'subject',
            'pid'                => 'product_id',
            'amountNewMessages',
            'is_favorite'        => 'isFavorite',
            'updated_at',
            'status'             => function() {
                return array_sum($this->_data);
            },
            'interlocutor_name'  => function() {
                $y = $this->interlocutor->id === \Yii::$app->user->id ? $this->author : $this->interlocutor;

                return $y->userProfile->fullName;
            },
            'interlocutorOnline' => function() {
                return 0;
            },
            'lastMessage',
        ];
    }

    public function extraFields() {
        return [
            'inter'  => function() {
                return $this->interlocutor->userProfile;
            },
            'author' => function() {
                return $this->author->userProfile;
            },
        ];
    }

    public function getInterlocutor() {
        return $this->hasOne(User::className(), ['id' => 'to_id']);
    }

    public function getAuthor() {
        return $this->hasOne(User::className(), ['id' => 'from_id']);
    }

    public function scenarios() {
        $scenarios                              = parent::scenarios();
        $scenarios[ self::SCENARIO_UPDATE_OWN ] = ['subject', 'isFavorite', 'is_deleted'];
        $scenarios[ self::SCENARIO_UPDATE ]     = ['isFavorite', 'is_deleted'];

        return $scenarios;
    }

    public function setIsDelivered($v) {
        $this->_data['is_delivered'] = intval($v);
    }

    public function setIsViewed($v) {
        $this->_data['is_viewed'] = intval($v);
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);

        // дополнительно сохранить маркер избранного диалога, он в отдельной таблице
        $db = \Yii::$app->db;

        $exist_entry = (new Query())->from('{{%chat_favorite}}')->where([
            'chat_id' => $this->id,
            'user_id' => \Yii::$app->user->id,
        ])->exists();
//\Yii::$app->response->statusCode=400;
//print 'this' . $this->isFavorite;
//\Yii::$app->response->send();exit;
        if ((int)$this->isFavorite && !$exist_entry) {
            // нужно создать
            $db->createCommand()
                ->insert('{{%chat_favorite}}', ['chat_id' => $this->id, 'user_id' => \Yii::$app->user->id])
                ->execute();
        } elseif (!(int)$this->isFavorite && $exist_entry) {
            // нужно удалить
            $db->createCommand()
                ->delete('{{%chat_favorite}}', ['chat_id' => $this->id, 'user_id' => \Yii::$app->user->id])
                ->execute();
        }
    }
}