<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.08.2017
 * Time: 22:59
 */

namespace frontend\models\rest;

use common\models\User as BasicUser;

class User extends BasicUser {
    public function getUserProfile() {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'id']);
    }
}