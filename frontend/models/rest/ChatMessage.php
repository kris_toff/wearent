<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 25.08.2017
 * Time: 9:32
 */

namespace frontend\models\rest;

use common\models\ChatMessage as BasicChatMessage;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

/**
 * Class ChatMessage
 * @package frontend\models\rest
 *
 * @property integer $chat_id      идентификатор диалога, к которому относится сообщение
 * @property bool    $is_delivered (0 or 1) true - указывает, что пользователь, КОМУ отправлялось сообщение, получил
 *           уведомление о сообщении и должен был видеть новое поступление
 * @property bool    $is_viewed    (0 or 1) true - указывает, что пользователь, КОМУ отправлялось сообщение, открыл
 *           диалог и мог полностью прочитать его
 *
 */
class ChatMessage extends BasicChatMessage {
    public $image;

    public static function find() {
        return (new ActiveQuery(get_called_class()))
            ->select(self::tableName() . '.*,cm.link AS image')
            ->join('LEFT JOIN', ['cm' => (new Query())->from('{{%chat_media}}')->groupBy('chat_message_id')],
                '{{cm}}.[[chat_message_id]] = ' . self::tableName() . '.[[id]]');
    }

    public function fields() {
        return [
            'id',
            'chat_id',
            'message',
            'status' => function() {
                return array_sum([intval($this->is_delivered), intval($this->is_viewed)]);
            },
            'created_at',
            'author' => function() {
                return $this->author->userProfile;
            },
            'is_own' => function() {
                return (int)($this->author->id == \Yii::$app->user->id);
            },
            'media'  => function() {
                return ArrayHelper::getColumn($this->media, 'link', false);
            },
        ];
    }

    public function getAuthor() {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    public function getMedia() {
        return $this->hasMany(ChatMedia::className(), ['chat_message_id' => 'id']);
    }

    public function rules() {
        $rules   = parent::rules();
        $rules[] = ['image', 'image'];
        $rules[] = ['image', 'required', 'when' => function($model) { return empty($model->message); }];

        return $rules;
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);

        // сохраним изображение, если передалось
        if (!is_null($this->image)) {
            FileHelper::createDirectory(\Yii::getAlias('@webroot') . "/uploads/chat/{$this->chat_id}");

            $path = "/uploads/chat/{$this->chat_id}/{$this->image->baseName}.{$this->image->extension}";
            $this->image->saveAs(\Yii::getAlias('@webroot') . $path);

            \Yii::$app->db->createCommand()->insert('{{%chat_media}}', [
                'chat_message_id' => $this->id,
                'link'            => \Yii::getAlias('@web') . $path,
            ])->execute();
        }
    }
}
