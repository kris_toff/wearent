<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 03.05.2017
 * Time: 17:28
 */

namespace frontend\models;

use common\models\Product;
use yii\bootstrap\Html;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class ProcessCategory {
    private static $list;
    private static $followlist;
    private static $childlist;
    private static $loadFollow;

    private static $countProducts;

    public static function menuTree($a, $loadFollows = true) {
        self::$loadFollow = $loadFollows;
        self::$list       = $a;

        $followList       = (new Query())->from('{{%ymd_categories_followlinks}}')->all();
        self::$followlist = ArrayHelper::map($followList,
            'parent_id',
            'parent_id', 'category_id');
        self::$childlist  = ArrayHelper::map($followList, 'category_id', 'category_id', 'parent_id');

        self::$countProducts = ArrayHelper::map((new Query())
            ->select(['category_id', 'count' => 'COUNT([[id]])'])
            ->from(Product::tableName())
            ->where(['is_deleted' => 0, 'status' => Product::OPEN])
            ->groupBy('category_id')
            ->having(['>', 'count', 0])
            ->all(), 'category_id', 'count');

//print_r(self::$countProducts);exit;
        $root = array_filter($a, function($v) {
            return !intval($v['parent_id']);
        });

//print_r(self::$followlist);exit;
        return static::process($root, array_diff_key($a, $root));
    }

    protected static function process($r, $a, $iter = 1) {
        $result = [];

        foreach ($r as $item) {
            $label = Html::tag('span', $item['name'], ['class' => 'name']);
            if ($iter > 1) {
                $countProduct = self::countGoods($item['id']);
                $label        .= "<span class=\"small\">($countProduct)";
            }
            $t = [
                'label' => $label,
                'url'   => ['/search', 'PS[category_id]' => $item['id'], 'PS[isOnlyCategory]' => 1],
                'image' => \Yii::getAlias("@web/uploads/category-images/small/{$item['image']}"),
            ];
            $d = array_filter($a, function($v) use ($item) {
                return (int)$v['parent_id'] == (int)$item['id']
                    || (self::$loadFollow
                        && (bool)ArrayHelper::getValue(self::$followlist, "{$v['id']}.{$item['id']}", false));
            });

            if (count($d) > 0) {
                if ($iter > 1) $t['label'] = '<span class="icon icon-plus"></span>' . $label;
                $t['items'] = self::process($d, array_diff_key($a, $d), $iter + 1);
            }

            $result[] = $t;
        }

        return $result;
    }

    private static function countGoods($id) {
        $id    = (int)$id;
        $count = (int)ArrayHelper::getValue(self::$countProducts, $id, 0);

        $p = array_filter(self::$list, function($v) use ($id) {
            return (int)$v['parent_id'] == $id && (int)$v['is_active'] == 1;
        });

        foreach ($p as $v) {
            $count += self::countGoods($v['id']);
        }

        foreach (ArrayHelper::getValue(self::$childlist, $id, []) as $v) {
            $count += self::countGoods($v);
        }

        return $count;
    }
}