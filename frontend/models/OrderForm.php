<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 12/18/2017
 * Time: 5:06 PM
 */

namespace frontend\models;


use common\models\Order;
use common\models\Product;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class OrderForm extends Model {
    public $product_id;
    public $order_start;
    public $order_stop;
    public $comment;
    public $total_cost;
    public $payment;

    public function rules() {
        return [
            [['product_id', 'order_start', 'order_stop', 'total_cost', 'payment'], 'required'],

            ['product_id', 'integer'],
            [
                'product_id',
                'exist',
                'targetClass' => Product::className(),
                'targetAttribute' => 'id',
                'filter' => ['is_deleted' => 0, 'status' => Product::OPEN],
            ],
            [['order_start', 'order_stop'], 'integer', 'min' => time()],
            ['comment', 'trim'],
            ['comment', 'string'],

            ['total_cost', 'double', 'min' => 0],
            ['payment', 'safe'],
        ];
    }

    public function save() {
        $model = new Order();
        $model->load(ArrayHelper::toArray($this, [
            self::className() => [
                'product_id',
                'start' => 'order_start',
                'stop' => 'order_stop',
                'comment',
            ]
        ], false), '');

        return $model->save();
    }
}