<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 18.10.2017
 * Time: 12:48
 */

namespace frontend\models;

use backend\models\CurrencyExchange;
use common\models\Currency;
use yii\base\ErrorException;
use yii\base\InvalidParamException;


use yii\base\Object;
use yii\helpers\ArrayHelper;

class ProductCurrency extends Object {
    /**
     * Функция возвращает форматированную цену товара согласно выбора языка и предпочитаемой валюты пользователем
     *
     * @param float            $number цена товара
     * @param Currency|integer $code   валюта товара
     * @param boolean          $approximate
     *
     * @return string возвращается строка с ценой и при необходимости изменённой валютой для пользователя
     * @throws ErrorException
     */
    public static function formatPrice($number, $code = null, $approximate = true) {
        /** @var Currency $productCurrency валюта товара */
        if (is_null($code)) {
            $productCurrency = Currency::findOne(['code' => \Yii::$app->formatter->currencyCode]);
        } elseif (is_integer($code)) {
            $productCurrency = Currency::findOne(['id' => $code]);
        } elseif (is_string($code)) {
            $productCurrency = Currency::findOne(['code' => $code]);
        } elseif ($code instanceof Currency) {
            $productCurrency = $code;
        }
        if (!isset($productCurrency) || is_null($productCurrency)) {
            throw new InvalidParamException('Parameter $code must be either an integer primary key for currency or instance of Currency model.');
        }

        // сравним предпочитаемый пользователем язык
        $userCurrency = ArrayHelper::getValue(\Yii::$app->params, 'currency');
        if (is_null($userCurrency)) {
            $userCurrency = $productCurrency;
        }

        $price = $number;

        if ($productCurrency->id !== $userCurrency->id) {
            $query    = CurrencyExchange::find()->where([
                'currency_withdraw' => $userCurrency->id,
                'currency_receive'  => $productCurrency->id,
            ]);
            $exchange = $query->one();

            if (is_null($exchange)) {
                $query->where([
                    'currency_withdraw' => $productCurrency->id,
                    'currency_receive'  => $userCurrency->id,
                ]);
                $exchange = $query->one();

                if (is_null($exchange)) {
                    $query->where([
                        'and',
                        [
                            'currency_withdraw' => Currency::find()
                                ->select('id')
                                ->where(['is_payment' => 1]),
                        ],
                        [
                            'currency_withdraw' => CurrencyExchange::find()
                                ->select('currency_withdraw')
                                ->where(['currency_receive' => [$userCurrency->id, $productCurrency->id]])
                                ->groupBy('currency_withdraw')
                                ->having('COUNT(*) = 2'),
                        ],
                    ])->indexBy('currency_receive');
                    $exchange = $query->all();

                    if (empty($exchange)) {
                        throw new ErrorException("Not found primary currency.");
                    } else {
                        $price = $number
                            / ArrayHelper::getValue($exchange, [$productCurrency->id, 'value'], 0)
                            * ArrayHelper::getValue($exchange, [$userCurrency->id, 'value'], 0);
                    }
                } else {
                    $price = $number * $exchange->value;
                }
            } else {
                $price = $number / $exchange->value;
            }
        }

        $result = \Yii::$app->formatter->asCurrency($price);
        if ($productCurrency->id !== $userCurrency->id && $price > 0 && $approximate) {
            $result = '&#8776;&nbsp;' . $result;
//            $result = '≈ ' . $result;
        }

        return $result;
    }

    /**
     * Функция возвращает только цену товара согласно выбора языка и предпочитаемой валюты пользователем
     *
     * @param float            $number    цена товара
     * @param Currency|integer $code      валюта товара
     * @param Currency|integer $forceCode валюта, в которую принудительно нужно перевести стоимость
     *
     * @return string возвращается цена и при необходимости изменённой валютой для пользователя
     * @throws ErrorException
     */
    public static function getActualPrice($number, $code, $forceCode = null) {
        /** @var Currency $productCurrency валюта товара */
        if (is_integer($code)) {
            $productCurrency = Currency::findOne(['id' => $code]);
        } elseif (is_string($code)) {
            $productCurrency = Currency::findOne(['code' => $code]);
        } elseif ($code instanceof Currency) {
            $productCurrency = $code;
        }
        if (!isset($productCurrency) || is_null($productCurrency)) {
            throw new InvalidParamException('Parameter $code must be either an integer primary key for currency or instance of Currency model.');
        }

        // сравним предпочитаемый пользователем язык
        if (is_null($forceCode)) {
            $userCurrency = ArrayHelper::getValue(\Yii::$app->params, 'currency');
        } elseif (is_integer($forceCode)) {
            $userCurrency = Currency::findOne(['id' => $forceCode]);
        } elseif (is_string($forceCode)) {
            $userCurrency = Currency::findOne(['code' => $forceCode]);
        } elseif ($forceCode instanceof Currency) {
            $userCurrency = $forceCode;
        }
        if (!isset($userCurrency) || is_null($userCurrency)) {
            $userCurrency = $productCurrency;
        }

        $price = $number;

        if ($productCurrency->id !== $userCurrency->id) {
            $query    = CurrencyExchange::find()->where([
                'currency_withdraw' => $userCurrency->id,
                'currency_receive'  => $productCurrency->id,
            ]);
            $exchange = $query->one();

            if (is_null($exchange)) {
                $query->where([
                    'currency_withdraw' => $productCurrency->id,
                    'currency_receive'  => $userCurrency->id,
                ]);
                $exchange = $query->one();

                if (is_null($exchange)) {
                    $query->where([
                        'and',
                        [
                            'currency_withdraw' => Currency::find()
                                ->select('id')
                                ->where(['is_payment' => 1]),
                        ],
                        [
                            'currency_withdraw' => CurrencyExchange::find()
                                ->select('currency_withdraw')
                                ->where(['currency_receive' => [$userCurrency->id, $productCurrency->id]])
                                ->groupBy('currency_withdraw')
                                ->having('COUNT(*) = 2'),
                        ],
                    ])->indexBy('currency_receive');
                    $exchange = $query->all();

                    if (empty($exchange)) {
                        throw new ErrorException("Not found primary currency.");
                    } else {
                        $price = $number
                            / ArrayHelper::getValue($exchange, [$productCurrency->id, 'value'], 0)
                            * ArrayHelper::getValue($exchange, [$userCurrency->id, 'value'], 0);
                    }
                } else {
                    $price = $number * $exchange->value;
                }
            } else {
                $price = $number / $exchange->value;
            }
        }

        return $price;
    }
}