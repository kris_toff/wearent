<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.08.2017
 * Time: 23:36
 */

namespace frontend\models;


use common\models\Chat;
use common\models\ChatMessage;
use common\models\Product;
use yii\base\Model;
use yii\helpers\FileHelper;

class ChatConversationForm extends Model {

    public $chat_id;
    public $product_id;
    public $file;
    public $message;


    public function rules() {
        return [
            ['product_id', 'required', 'when' => function($model) { return empty($model->chat_id); }],

            [['chat_id', 'product_id'], 'integer', 'min' => 1],
            ['chat_id', 'exist', 'targetClass' => Chat::className(), 'targetAttribute' => 'id'],
            ['product_id', 'exist', 'targetClass' => Product::className(), 'targetAttribute' => 'id'],

            ['message', 'required'],
            ['message', 'string', 'min' => 2],

            ['file', 'file', 'extensions' => ['png', 'gif', 'jpg']],
        ];
    }

    public function save($validate = true) {
        if ($validate && !$this->validate()) {
            return false;
        }

        if (!empty($this->chat_id)) {
            $model = Chat::findOne(['id' => $this->chat_id]);

        } elseif (!empty($this->product_id)) {
            $product = Product::find()->where(['id' => $this->product_id])->with('author')->one();

            $model             = new Chat();
            $model->attributes = [
                'subject'    => \Yii::t('UI11.2', 'Question for ' . $product->name),
                'to_id'      => $product->author->id,
                'product_id' => $product->id,
            ];

            if (!$model->save()) {
                return false;
            }
        } else {
            return false;
        }

        $path = \Yii::getAlias('@webroot/uploads/chat/' . $model->id);
        if (FileHelper::createDirectory($path)) {
//            $this->file->saveAs($path . '/' . $this->file->baseName . '.' . $this->file->extension);
        }

        $mc             = new ChatMessage();
        $mc->attributes = [
            'chat_id' => $model->id,
            'message' => $this->message,
        ];

        return $mc->save();
    }
}
