<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 09.11.2017
 * Time: 21:44
 */

namespace frontend\models;


use common\models\User;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yiimodules\categories\models\Categories;

class HistorySearch extends ActiveRecord {
    public static function tableName() {
        return '{{%history_search}}';
    }

    public function rules() {
        return [
            [['field_what', 'field_where_string'], 'string', 'max' => 105],

            ['field_category_id', 'default', 'value' => null, 'isEmpty' => function($v) { return (int)$v < 1; }],
            ['field_category_id', 'integer'],
            ['field_category_id', 'exist', 'targetClass' => Categories::className(), 'targetAttribute' => 'id'],

            ['field_where_coords', 'string', 'max' => 100],
            [['field_when_from', 'field_when_to'], 'integer'],
        ];
    }

    public function behaviors() {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
            [
                'class'              => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ],
            [
                'class'      => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => 'ip',
                ],
                'value'      => \Yii::$app->request->userIP,
            ],
            [
                'class'      => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => 'browser',
                ],
                'value'      => \Yii::$app->request->userAgent,
            ],
        ];
    }

    public function beforeValidate() {
        if (empty($this->field_what)
            && empty($this->field_category_id)
            && empty($this->field_where_coords)
            && empty($this->field_when_from)
            && empty($this->field_when_to)) {
            return false;
        }

        return parent::beforeValidate();
    }


}