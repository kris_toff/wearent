<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 17.05.2017
 * Time: 22:40
 */

namespace frontend\models;


use backend\models\CategoryAlias;
use common\models\Product;
use common\models\ProductReserved;
use frontend\models\ProductCurrency;
use mirocow\eav\EavBehavior;
use mirocow\eav\models\EavAttribute;
use mirocow\eav\models\EavAttributeOption;
use mirocow\eav\models\EavAttributeSearch;
use mirocow\eav\models\EavAttributeValue;
use yii\base\Model;
use yii\behaviors\AttributeBehavior;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yiimodules\categories\models\Categories;

/**
 * Форма поиска товаров
 *
 * @property integer $rent_cost    стоимость аренды
 * @property integer $deposit_cost стоимость залога
 *
 */
class ProductSearch extends Product {
    public $category_id = 0; // >0 - id категории, а слово/фраза для поиска это название категории; 0 - слово для поиска это обычный набор символов для фильтра
    public $note; // если не указан category_id текущее свойство содержит набор букв/слов для поиска
    public $location_string;
    public $location; // содержит координаты области(!) города/объекта для поиска товаров в формате: lat_lo,lng_lo,lat_hi,lng_hi, где lo - южно-западный угол, hi - северо-восточный угол
    public $period_from; // дата unix начала вероятной сделки для фильтра
    public $period_to; // дата unix окончания вероятной сделки для фильтра

    public $time_from; // в дополнительных фильтрах время ОТ
    public $time_to; // в дополнительных фильтрах время ДО

    public $rent_cost;
    public $deposit_cost;
    public $rent_cost_b;
    public $deposit_cost_b;

    public $rate;

    public $distanceDelivery = -1; // число (в км) расстояние; -1 - не применяется фильтр, 0 - применить фильтр но не учитывать расстояние

    public $isOnlyCategory = 0; // true - поиск только по категории, не учитывая слова $note

    public $EavModel = [];

    protected $_coords; // заполняется через behaviors в структуре sw:{latitude:**, longitude:**}, ne:{latitude:**, longitude:**}

    private $_provider;
    private $_categoryIds = [];

    public function rules() {
        return [
            [['note', 'location'], 'trim'],

            ['category_id', 'integer', 'min' => 1],
            ['note', 'string', 'max' => 99],

            [['location_string', 'location'], 'string'],
            ['distanceDelivery', 'integer', 'min' => 0],

            [['period_from', 'period_to'], 'integer'],
            [['time_from', 'time_to'], 'time', 'format' => 'HH:mm:ss'],
            [['time_from', 'time_to'], 'default', 'value' => null],

            ['isOnlyCategory', 'boolean'],
            [
                'isOnlyCategory',
                'filter',
                'filter' => function($value) {
                    return !empty($this->category_id) && !$this->hasErrors('category_id') ? (boolean)$value : false;
                },
            ],
            ['isOnlyCategory', 'default', 'value' => false],

            [['rent_cost', 'deposit_cost'], 'trim'],
            [['rent_cost', 'deposit_cost'], 'string'],

            ['rate', 'integer', 'min' => 1, 'max' => 5],

            ['EavModel', 'safe'],
        ];
    }

    public function beforeValidate() {
        if (!(empty($this->period_from) && empty($this->period_to))) {
            $this->period_from = min($this->period_from, $this->period_to);
            $this->period_to   = max($this->period_from, $this->period_to);
        }

        if (!empty($this->rent_cost)) {
            $this->rent_cost_b = explode(',', $this->rent_cost);
        }
        if (!empty($this->deposit_cost)) {
            $this->deposit_cost_b = explode(',', $this->deposit_cost);
        }

        return parent::beforeValidate();
    }

    public function analyze() {
        $query = Product::find()
            ->select(Product::tableName() . '.*,{{u}}.[[isFavorite]]')
            ->distinct()
            ->with([
                'category',
                'currency',
                'media' => function(ActiveQuery $query) {
                    $query->andWhere(['or like', 'link', ['%.png', '%.jpg', '%.gif'], false]);
                },
                'author.userProfile.avatars',
            ]);

        $favQuery = (new Query());
        $favQuery->select(['product_id', 'isFavorite' => new Expression('1')])->from('{{%product_favorite}}');
        if (\Yii::$app->user->isGuest) {
            $favQuery->where('0=1');
        } else {
            $favQuery->where(['user_id' => \Yii::$app->user->id]);
        }
        $query->leftJoin([
            'u' => $favQuery,
        ], '{{u}}.[[product_id]] = ' . Product::tableName() . '.[[id]]');

        $query->where(['status' => Product::OPEN, 'status' => Product::OPEN])
            ->andFilterWhere(['<', 'created_at', \Yii::$app->request->get('maxtime')])
//            ->joinWith('eavValue.option', false)
            ->asArray();

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize'       => 3,
                'forcePageParam' => false,
                'validatePage'   => false,
            ],
            'sort'       => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'updated_at' => SORT_DESC,
                ],
            ],
        ]);

        $this->validate();

        $this->_provider    = $dataProvider;
        $this->_categoryIds = $this->getCategoryIds();


        // категория
        if (!$this->hasErrors('category_id')) {
            $query->andFilterWhere(['category_id' => $this->_categoryIds]);
        }
        if (!$this->isOnlyCategory && !$this->hasErrors('note')) {
//        if (!$this->isOnlyCategory && !$this->hasErrors('note') && strlen($this->note)) {
            // поиск по названиям товаров
            $query->andFilterWhere(['like', 'name', $this->note]);
            /*
            $alq = [];
            foreach (CategoryAlias::find()->select('category_id')->where([
                'like',
                'name',
                $this->note,
            ])->all() as $item) {
                $alq = array_merge($alq, $this->getCategoryIds($item));
            }
            $alq = array_unique($alq, SORT_NUMERIC);

            $query->andWhere(['or', ['in', 'category_id', $alq], ['like', 'name', $this->note]]);
            */
        }

// локация
        if (!empty($this->_coords)) {
            $query->andFilterWhere([
                'and',
                ['between', 'lat', min($this->_coords['lat']), max($this->_coords['lat'])],
                ['between', 'lng', min($this->_coords['lng']), max($this->_coords['lng'])],
            ]);
        }

        // свободная дата от - до
        if (!(empty($this->period_from) && empty($this->period_to))) {
            $squery = (new Query())
                ->select('product_id')
                ->from('{{%product_exclude_dates}}')
                ->where([
                    'and',
                    ['<', 'date_from', $this->period_to],
                    ['or', ['date_to' => null], ['>', 'date_to', $this->period_from]],
                ]);
            $query->andWhere(['not in', 'id', $squery]);
        }

        // цены фильтра от - до
        $fi = Product::find()
            ->select('currency_id')
            ->distinct(true)
            ->where(['is_deleted' => 0, 'status' => Product::OPEN])
            ->column();
//        print_r($fi);exit;
        $w = ['or'];
        if (!empty($this->rent_cost_b) && count($this->rent_cost_b) == 2) {
            foreach ($fi as $item) {
                $min = ProductCurrency::getActualPrice($this->rent_cost_b[0],
                    ArrayHelper::getValue(\Yii::$app->params, 'currency'), (integer)$item);
                $max = ProductCurrency::getActualPrice($this->rent_cost_b[1],
                    ArrayHelper::getValue(\Yii::$app->params, 'currency'), (integer)$item);
                $w[] = ['and', ['currency_id' => $item], ['between', 'price', $min, $max]];
            }

            $query->andFilterWhere($w);
        }
//        print_r($w);exit;
        $w = ['or'];
        if (!empty($this->deposit_cost_b) && count($this->deposit_cost_b) == 2) {
            foreach ($fi as $item) {
                $min = ProductCurrency::getActualPrice($this->deposit_cost_b[0],
                    ArrayHelper::getValue(\Yii::$app->params, 'currency'), (integer)$item);
                $max = ProductCurrency::getActualPrice($this->deposit_cost_b[1],
                    ArrayHelper::getValue(\Yii::$app->params, 'currency'), (integer)$item);
                $w[] = ['and', ['currency_id' => $item], ['between', 'pledge_price', $min, $max]];
            }

            $query->andFilterWhere($w);
        }

        // фильтры на расстояние, дистанция
        if (!$this->hasErrors('distanceDelivery')) {
            $query->andFilterWhere(['and', ['>=', 'delivery_radius', $this->distanceDelivery], ['>', 'delivery_radius', 0]]);
        }

        // фильтры времени начала сделки
        $query->andFilterWhere(['and', ['<=', 'time_from', $this->time_from], ['>=', 'time_to', $this->time_to]]);

        // атрибуты товаров
//        print '<pre>';print_r($this->getEavAttributes()->with('eavType')->all());exit;
        $this->EavModel = array_filter($this->EavModel);
        if (!empty($this->EavModel)) {
            // добавим атрибуты
            $aq = EavAttributeValue::find();
            $aq->select(EavAttributeValue::tableName() . '.[[entityId]]')->distinct(true);
            $aq->joinWith('eavAttribute');
            foreach ($this->EavModel as $k => $v) {
                $aq->andWhere([
                    'and',
                    ['name' => $k],
                    [
                        'or',
                        ['optionId' => $v],
                        [
                            'and',
                            new Expression('[[value]] IS NOT NULL'),
                            new Expression('LENGTH([[value]]) > 0'),
                            ['like', 'value', $v],
                        ],
                    ],
                ]);
            }

            $query->andWhere(['id' => $aq]);
        }

//        exit;
        /*

        if (!($this->load($params) && $this->validate())) {
//            print_r($this->errors);
            $ctl = [];
            if (!empty($ctid)) {
                $ctl = $this->getCategoryIds($ctid);
            }
            $this->_categoryIds = $ctl;
            $query->andFilterWhere(['category_id' => $ctl]);

//print_r($ctl);exit;
            return $dataProvider;
        }

//        $price  = explode(',', $this->rent_cost);
//        $pledge = explode(',', $this->deposit_cost);

        if (!empty($ctid)) {
            $this->_categoryIds = $this->getCategoryIds($ctid);
        } elseif (!empty($this->category)) {
            // здесь поиск по имени категории
            $this->_categoryIds = Categories::find()->select('id')->where([
                'and',
                ['is_active' => 1],
                [
                    'or like',
                    'name',
                    $this->category,
                ],
            ])->column();
        } else {
            $this->_categoryIds = [];
        }
        $query->andFilterWhere(['category_id' => $this->_categoryIds]);

        // цены и залог
        $query
            ->andFilterWhere([
                'between',
                'price',
                ArrayHelper::getValue($price, 0, 0),
                ArrayHelper::getValue($price, 1, -1),
            ])
            ->andFilterWhere([
                'between',
                'pledge_price',
                ArrayHelper::getValue($pledge, 0, 0),
                ArrayHelper::getValue($pledge, 1, -1),
            ]);

        // атрибуты
        foreach ($this->getEavAttributes()->with('eavType')->all() as $attr) {
            $val = $this->{$attr->name};
            if (!empty($val->value)) {
                switch ($attr->eavType->name) {
                case 'text':
                    $query->andWhere([
                        'and',
                        [EavAttributeValue::tableName() . '.[[attributeId]]' => $attr->id],
                        ['like', EavAttributeValue::tableName() . '.[[value]]', (string)$val],
                    ]);
                    break;
                case 'numeric':
                    $query->andWhere([
                        'and',
                        [EavAttributeValue::tableName() . '.[[attributeId]]' => $attr->id],
                        [EavAttributeValue::tableName() . '.[[value]]' => floatval((string)$val)],
                    ]);
                    break;
                case 'radio':
                case 'option':
                    $query->andWhere([
                        'and',
                        [EavAttributeValue::tableName() . '.[[attributeId]]' => $attr->id],
                        [EavAttributeValue::tableName() . '.[[optionId]]' => intval((string)$val)],
                    ]);
                    break;
                case 'checkbox':
                    $query->andWhere([
                        'in',
                        EavAttributeValue::tableName() . '.[[attributeId]]',
                        EavAttributeValue::find()
                            ->select('attributeId')
                            ->where(['optionId' => (array)$val->value])
                            ->groupBy('attributeId')
                            ->having('COUNT([[attributeId]]) = ' . count((array)$val->value)),
                    ]);
                    break;
                }
            }
        }
*/

//print_r($query->createCommand()->rawSql);exit;
        \Yii::$app->response->headers->add('sql', $query->createCommand()->rawSql);

//        print $this->category_id;print_r($this->_categoryIds);print $query->createCommand()->rawSql;exit;

        return $dataProvider;
    }

    private function getCategoryIds() {
        $id = $this->category_id;
        if (intval($id) < 1 || $this->hasErrors('category_id')) {
            return [];
        }

        $cta = Categories::findAll(['id' => $id, 'is_active' => 1]);
        $ctl = [];

        while (!empty($cta)) {
            $tmpids = ArrayHelper::getColumn($cta, 'id', false);
            $ctl    = array_merge($ctl, $tmpids);
            $cta    = array_merge(Categories::findAll(['parent_id' => $tmpids, 'is_active' => 1]),
                Categories::find()
                    ->leftJoin('{{%ymd_categories_followlinks}} {{ycf}}',
                        '{{ycf}}.[[category_id]] = ' . Categories::tableName() . '.[[id]]')
                    ->where(['ycf.parent_id' => $tmpids])
                    ->all());
        }

        return array_unique($ctl, SORT_NUMERIC);
    }

    public function getEavAttributes() {
        if (intval($this->category_id) < 1 || $this->hasErrors('category_id')) {
            return EavAttributeSearch::find()->where('0=1');
        }
//print 'eav';exit;
        $query = EavAttributeSearch::find()
            ->joinWith('entity e')
            ->where(['e.entityModel' => Product::className()])
            ->orderBy(['order' => SORT_DESC, 'typeId' => SORT_ASC, 'entityId' => SORT_ASC]);

        $fo = $this->category_id;
        $q  = Categories::find()->select('parent_id');

        $cc = [$this->category_id];
        $fo = $q->where([
            'and',
            ['id' => $fo],
            '[[parent_id]] IS NOT NULL',
        ])->column();

        while (!empty($fo)) {
            $cc = array_merge($cc, $fo);
            $fo = $q->where([
                'and',
                ['id' => $fo],
                '[[parent_id]] IS NOT NULL',
            ])->column();
        }

        return $query->andWhere(['e.categoryId' => $cc]);

        $ct  = Categories::find()->select('id,parent_id')->where(['id' => $this->_categoryIds])->all();
        $ctl = ArrayHelper::map($ct, 'id', 'parent_id');

        $rl = array_diff($ctl, array_keys($ctl));
        $tc = array_keys($rl);
        $cc = [];
        $i  = 0;
        do {
            $cc = array_merge($cc, $tc);
            $g  = array_intersect($ctl, $tc);
            $tc = array_keys($g);

            $i++;
        } while (count($g) == 1 && $i < 20);

        $cc = array_unique($cc, SORT_NUMERIC);


//        print $query->createCommand()->rawSql;exit;
//        print_r($query->all());exit;
        return $query->andWhere(['e.categoryId' => $cc]);
    }

    public function behaviors() {
        return [
            'eav' => [
                'class'      => EavBehavior::className(),
                'valueClass' => EavAttributeValue::className(),
            ],
            [
                'class'      => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_AFTER_VALIDATE => 'coords',
                ],
                'value'      => function($event) {
//            print_r($event);exit;
                    $r = array_filter(explode(',', $event->sender->location));
                    if (count($r) !== 4) {
                        return [];
                    }

                    $c = array_chunk($r, 2);

                    return [
                        'lat' => array_column($c, 0),
                        'lng' => array_column($c, 1),
                    ];
                },
            ],
        ];
    }

    public function catalogTreeBreadcrumbs() {
        $link = [];
        if (!empty($this->note)) {
            $link = [$this->note];
        }
        if (empty($this->category_id)) {
            return $link;
        }

        $category = Categories::findOne($this->category_id);
        if (is_null($category)) {
            return $link;
        }

        $link = [];
        do {
            array_unshift($link, [
                'label' => $category->name,
                'url'   => ['/category/repel', 'fields' => $category->name],
            ]);
            $category = $category->parentCategory;
        } while (!is_null($category));

        return $link;
    }

    public function getPrices() {
        /** @var ActiveQuery $query */
        $query       = clone $this->_provider->query;
        $query->with = null;

        $query->select([
            'minp'   => 'MIN([[price]])',
            'maxp'   => 'MAX([[price]])',
            'minplp' => 'MIN([[pledge_price]])',
            'maxplp' => 'MAX([[pledge_price]])',
            'currency_id',
        ])->distinct(false);
        $query->with('currency')->groupBy('currency_id');
        $query->where('[[currency_id]] IS NOT NULL');

        $a = [];
//print_r($query->all());exit;
        foreach ($query->all() as $item) {
            $item['currency_id'] = intval($item['currency_id']);
            $a[]                 = [
                'minprice'  => ProductCurrency::getActualPrice($item['minp'], $item['currency_id']),
                'maxprice'  => ProductCurrency::getActualPrice($item['maxp'], $item['currency_id']),
                'minpledge' => ProductCurrency::getActualPrice($item['minplp'], $item['currency_id']),
                'maxpledge' => ProductCurrency::getActualPrice($item['maxplp'], $item['currency_id']),
            ];
        }
//        print $query->createCommand()->rawSql;exit;
        $r['minp']   = floor(min(array_column($a, 'minprice')));
        $r['maxp']   = ceil(max(array_column($a, 'maxprice')));
        $r['minplp'] = floor(min(array_column($a, 'minpledge')));
        $r['maxplp'] = ceil(max(array_column($a, 'maxpledge')));

        if (empty($this->rent_cost)) {
            $this->rent_cost = "{$r['minp']},{$r['maxp']}";
        }
        if (empty($this->deposit_cost)) {
            $this->deposit_cost = "{$r['minplp']},{$r['maxplp']}";
        }

        return $r;
    }

    public function formName() {
        return 'PS';
    }

    public function setCoords($v) {
        $this->_coords = $v;
    }
}

