<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 18.07.2017
 * Time: 19:45
 */

return [
    'yi {n, plural, =0{zero} =1{one} one{q} few{ee43} many{wner} other{----}}' => 'ок {n, plural, =0{ноль} =1{один} one{немного} few{побольше} many{много} other{неверно}}',
];