<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.12.2017
 * Time: 21:27
 */

namespace frontend\config\ruleClass;


use common\models\User;
use yii\base\Object;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\Request;
use yii\web\UrlManager;
use yii\web\UrlRuleInterface;

class ProfileUrlRule extends Object implements UrlRuleInterface {
    public function parseRequest($manager, $request) {
        $pathInfo = trim($request->pathInfo);

        if (preg_match('/^(id)?([\w-]+)$/', $pathInfo, $matched)) {
            if (isset($matched[2]) && is_numeric($matched[2]) ){
                // search by id
                return ['profile/index', ['id' => $matched[2]]];
            } else {
                $user = User::findOne(['username' => $matched[0]]);
                if (!is_null($user)){
                    return ['profile/index', ['id' => $user['id']]];
                }
            }
        }

        return false;
    }

    public function createUrl($manager, $route, $params) {
        $route = rtrim($route, '/');

        if (!strcmp($route, 'profile')){
//            print_r($route);print_r($params);exit;
            if (isset($params['id'])){
                $user = User::findOne( $params['id'] );
                if (!(is_null($user) && empty($user['username']))){
                    return $user['username'];
                } else{
                    return "id{$params['id']}";
                }
            } elseif (isset($params['name'])){
                $user = User::find()->where(['and', ['username' => $params['name']], new Expression('LENGTH([[username]]) > 0')])->one();
                if (!is_null($user)){
                    return $user['username'];
                }
            }
        }

        return false;
    }

}