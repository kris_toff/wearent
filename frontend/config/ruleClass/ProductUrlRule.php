<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 16.08.2017
 * Time: 15:34
 */

namespace frontend\config\ruleClass;


use common\models\Product;
use yii\base\Object;
use yii\web\Request;
use yii\web\UrlManager;
use yii\web\UrlRuleInterface;

class ProductUrlRule extends Object implements UrlRuleInterface {
    protected $actions;

    public function createUrl($manager, $route, $params) {
        $route = rtrim($route, '/');
//        print $route;print_r($params);exit;

        if (!strcasecmp($route, 'product/view')) {
            if (isset($params['name'])
                && Product::find()
                    ->where(['is_deleted' => 0, 'slug' => $params['name']])
                    ->exists()) {
                return $params['name'];
            } elseif (isset($params['id']) && is_numeric($params['id'])) {
                $model = Product::findOne(['id' => $params['id'], 'is_deleted' => 0]);

                if (!is_null($model)) {
                    return $model['slug'];
                }
            }
        } elseif (preg_match('/product/', $route)){
//            print $route;exit;
        }

        return false;
    }

    public function parseRequest($manager, $request) {
//        print_r($request->pathInfo);exit;

        if (preg_match('/^(\w{2}-\w{2}\/)?([\w-]+)$/', $request->pathInfo, $matched)) {
//            print_r($matched);exit;
            if (isset($matched[2]) && Product::find()->where(['slug' => $matched[2], 'is_deleted' => 0])->exists()) {
                return ['product/view', ['slug' => $matched[2]]];
            }
//            print 'this'.$request->pathInfo;exit;
        }

//        print 'there'.$request->pathInfo;exit;

        return false;
    }
}