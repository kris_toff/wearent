<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 10.10.2017
 * Time: 21:42
 */

namespace frontend\config\ruleClass;


use common\models\Page;
use yii\base\Object;
use yii\web\UrlRuleInterface;

class StaticPagesRule extends Object implements UrlRuleInterface {
    public function parseRequest($manager, $request) {
        $pathInfo = $request->pathInfo;
//        print_r($pathInfo);exit;
        if (preg_match('/^([\w-]+)(\/(\w+))?$/', $pathInfo, $matched)) {
//            print_r($matched);exit;
            if (!empty($matched) && count($matched) <= 2) {
                $link = array_pop($matched);

                if (isset($matched[0]) && Page::find()->where(['slug' => $link])->exists()) {
                    return ['page/view', ['slug' => $link]];
                }
            }
        }

        return false;
    }

    public function createUrl($manager, $route, $params) {
        $r = explode('/', $route);

        if ('page/view' === $route && isset($params['slug'])
            && Page::find()
                ->where(['slug' => $params['slug']])
                ->exists()) {
            return $params['slug'];
        }

        return false;
//        print_r($r);exit;
    }

}