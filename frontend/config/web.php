<?php
$config = [
    'homeUrl'             => '/',//Yii::getAlias('@frontendUrl'),
//    'catchAll'            => ['developer-mode/page-suspend'],
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute'        => 'site/index',
    'bootstrap'           => ['maintenance'],
    'layout'              => 'typicalFrontend',
    'modules'             => [
        'user'   => [
            'class'             => 'frontend\modules\user\Module',
            'shouldBeActivated' => true,
        ],
        'api'    => [
            'class'   => 'frontend\modules\api\Module',
            'modules' => [
                'v1' => 'frontend\modules\api\v1\Module',
            ],
        ],
        'social' => [
            'class'    => 'kartik\social\Module',
            'facebook' => [
                'appId'  => '165614500684123',
                'secret' => '2a0f6fff4749307b17526e84840679c0',
            ],
            'google'   => [
                'clientId' => '625669350546-f4qmho1qakc90t42reohsiveejaen7n5.apps.googleusercontent.com',
            ],
        ],
    ],
    'components'          => [
        'assetManager'         => [
            'bundles' => [
                'yii\jui\JuiAsset' => [
//                    'css' => [],
                ],
                'dosamigos\google\maps\MapAsset' => [
                    'options' => [
                        'key'     => 'AIzaSyBr3Fcss2QdBHKHY5W2m05ZTtb8bRVLXvI',
                        'version' => '3.1.18',
                    ],
                ],
            ],
        ],
        'authClientCollection' => [
            'class'   => 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class'          => 'yii\authclient\clients\Facebook',
                    'clientId'       => '165614500684123',
                    'clientSecret'   => '2a0f6fff4749307b17526e84840679c0',
                    'scope'          => 'email,public_profile',
                    'attributeNames' => [
                        'name',
                        'email',
                        'first_name',
                        'last_name',
                    ],
                ],
                'google'   => [
                    'class'        => 'yii\authclient\clients\Google',
                    'clientId'     => '625669350546-f4qmho1qakc90t42reohsiveejaen7n5.apps.googleusercontent.com',
                    'clientSecret' => 'PyxZH9mYoAvNdTf8cNdJKtuI',
                ],
                'twitter'  => [
                    'class'           => 'yii\authclient\clients\Twitter',
                    'attributeParams' => [
                        'include_email' => 'true',
                    ],
                    'consumerKey'     => 'EoRipEv1S8hMxfYkV0mt2x3ly',
                    'consumerSecret'  => 'GpvNKKbAe9KWgvRzF0TkpIa9Z000ev0fB2ldWZSiybNZA2tKR9',
                ],
            ],
        ],
        'errorHandler'         => [
            'errorAction' => 'site/error',
        ],
        'maintenance'          => [
            'class'   => 'common\components\maintenance\Maintenance',
            'enabled' => function($app) {
                return $app->keyStorage->get('frontend.maintenance') === 'enabled';
            },
        ],
        'request'              => [
            'baseUrl'             => '',
            'cookieValidationKey' => env('FRONTEND_COOKIE_VALIDATION_KEY'),
            'parsers'             => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'user'                 => [
            'class'           => 'yii\web\User',
            'identityClass'   => 'common\models\User',
            'loginUrl'        => ['/user/sign-in/login'],
            'enableAutoLogin' => true,
            'enableSession'   => true,
            'as afterLogin'   => 'common\behaviors\LoginTimestampBehavior',
        ],
        'places'               => [
            'class'  => '\dosamigos\google\places\Places',
            'key'    => 'AIzaSyBr3Fcss2QdBHKHY5W2m05ZTtb8bRVLXvI',
            'format' => 'json',
        ],
        'placesSearch'         => [
            'class'  => 'dosamigos\google\places\Search',
            'key'    => 'AIzaSyBr3Fcss2QdBHKHY5W2m05ZTtb8bRVLXvI',
            'format' => 'json',
        ],
        'geoip'                => [
            'class' => 'lysenkobv\GeoIP\GeoIP',
        ],
    ],
];

if (YII_ENV_DEV) {
    $config['modules']['gii'] = [
        'class'      => 'yii\gii\Module',
        'generators' => [
            'crud' => [
                'class'           => 'yii\gii\generators\crud\Generator',
                'messageCategory' => 'frontend',
            ],
        ],
    ];
}

return $config;
