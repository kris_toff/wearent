<?php

$l = require \Yii::getAlias('@common/components/langs/_languages.php');
$l = array_column($l, 'code');

//array_walk($lang, function(&$v) {
//    $v = strtolower(substr($v, 0, 2));
//});

return [
    'class'                        => 'codemix\localeurls\UrlManager',
    //    'class'           => 'yii\web\UrlManager',
    'enablePrettyUrl'              => true,
    'enableDefaultLanguageUrlCode' => true,
    'enableLanguagePersistence'    => true,
    'enableLanguageDetection'      => true,
    'languageCookieName'           => '_lang',
    'ignoreLanguageUrlPatterns'    => [
        // route pattern => url pattern
        '#^img/#'         => '#^img/#',
        '#^conversation#' => '#^conversation#',

        '#^product/plural#'       => '#^product/plural#',
        '#^product/delete#'       => '#^product/delete#',
        '#^product/load-media#'   => '#^product/load-media#',
        '#^product/make-default#' => '#^product/make-default#',
        '#^product/set-define#'   => '#^product/set-define#',
        '#^product/rotate#'       => '#^product/rotate#',
        '#^product/crop#'         => '#^product/crop#',
//        '#^product/create-#'      => '#^product/create-#',
        '#^product/change-#'      => '#^product/change-#',
//        '#^red/#' => '#^red/#',

        '#[\w-]+rest-data#' => '#[\w-]+rest-data#',
        '#^auth/social#'    => '#^auth/social#',

        '#^profile/send-ver-sms#'   => '#^profile/send-ver-sms#',
        '#^profile/set-user-login#' => '#^profile/set-user-login#',
        '#^profile/check#'          => '#^profile/check#',

        '#^utils#' => '#^utils#',
    ],
    'languages'                    => $l,
    'normalizer'                   => [
        'class'  => 'yii\web\UrlNormalizer',
        'action' => \yii\web\UrlNormalizer::ACTION_REDIRECT_TEMPORARY,
    ],
    'rules'                        => [
        // Static pages
        //        ['pattern' => 'page/<slug>', 'route' => 'page/view'],
        ['class' => 'frontend\config\ruleClass\StaticPagesRule'],

        // Articles
        ['pattern' => 'article/index', 'route' => 'article/index'],
        ['pattern' => 'article/attachment-download', 'route' => 'article/attachment-download'],
        ['pattern' => 'article/<slug>', 'route' => 'article/view'],

        // catalog
        ['pattern' => 'catalog/<id:\d+>', 'route' => 'catalog/index'],
        ['pattern' => 'catalog/<slug:[\w-]+>', 'route' => 'catalog/index'],

        // product
        ['class' => 'frontend\config\ruleClass\ProductUrlRule'],
        ['pattern' => 'red/<link:[\w-]+>', 'route' => 'product/create-new'],
        ['pattern' => 'preview/<slug:[\w-]+>', 'route' => 'product/preview'],

        // Api
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/article', 'only' => ['index', 'view', 'options']],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/user', 'only' => ['index', 'view', 'options']],

        // profile
        ['class' => 'frontend\config\ruleClass\ProfileUrlRule'], // for "username" links
//        ['pattern' => 'id<id:\d+>', 'route' => 'profile/index'],

        [
            'class'      => 'yii\rest\UrlRule',
            'controller' => [
                'conversation-rest-data',
                'conversation-message-rest-data',
                'profile-email-rest-data',
                'profile-phone-rest-data',
                'profile-avatar-rest-data',
                'profile-video-rest-data',
                'profile-document-rest-data',
            ],
            'pluralize'  => false,
        ],
    ],
    'showScriptName'               => false,
];
