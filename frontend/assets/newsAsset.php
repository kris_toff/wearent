<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 11.08.2017
 * Time: 17:39
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class newsAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/news.css',
    ];

    public $js = [
        'js/news.js',
    ];


    public $depends = [
        'frontend\assets\AppAsset',
    ];

}