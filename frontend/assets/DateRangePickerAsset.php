<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 30.08.2017
 * Time: 16:23
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class DateRangePickerAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl  = '@web';


    public $depends = [
        'frontend\assets\AppAsset',
        'frontend\assets\MomentAsset',
    ];

    public $css = [
        'css/pickermaster/src/daterangepicker.css',
    ];

    public $js = [
        'css/pickermaster/dist/jquery.daterangepicker.min.js',
    ];
}