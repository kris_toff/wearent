<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 01.09.2017
 * Time: 14:22
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class AngularMaterialAsset extends AssetBundle {

    public $sourcePath = '@bower/angular-material';

    public $depends = [
        'frontend\assets\AngularAsset',
    ];

    public $js  = [
        'angular-material.min.js',
    ];
    public $css = [
        'angular-material.min.css',
        '//fonts.googleapis.com/icon?family=Material+Icons',
    ];
}