<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.08.2017
 * Time: 14:14
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class MomentAsset extends AssetBundle {

    public $sourcePath = '@bower/moment/min';
    public $js = [
        'moment-with-locales.min.js',
    ];

}