<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 24.10.2017
 * Time: 21:22
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class AngularUIGoogleMap extends AssetBundle {

    public $sourcePath = '@bower/angular-ui';
    public $js         = [
        'angular-simple-logger/dist/angular-simple-logger.min.js',
        'angular-google-map/angular-google-maps/dist/angular-google-maps.min.js',
    ];

    public $depends = [
        'frontend\assets\AngularAsset',
        'common\assets\LodashAsset',
    ];

}