<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 01.09.2017
 * Time: 19:41
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class AngularUIAsset extends AssetBundle {
    public $sourcePath = '@bower/angular-ui';

    public $js = [
        'angular-bootstrap/ui-bootstrap.min.js',
        'angular-bootstrap/ui-bootstrap-tpls.min.js',
        'angular-ui-mask/dist/mask.min.js',
        'angular-ui-validate/dist/validate.min.js',
        'angular-ui-uploader/dist/uploader.min.js',
        'angular-ui-calendar/src/calendar.js',
    ];

    public $css = [
        'angular-bootstrap/ui-bootstrap-csp.css',
    ];

    public $depends = [
        'frontend\assets\AngularAsset',
        'frontend\assets\FullCalendarAsset',
        'frontend\assets\AngularUIGoogleMap',
    ];
}