<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 29.10.2017
 * Time: 17:13
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class AngularPluginsAsset extends AssetBundle {

    public $sourcePath = '@bower/angular-plugins';
    public $js=[
        'angular-socialshare/dist/angular-socialshare.min.js',
    ];

    public $depends = [
        'frontend\assets\AngularAsset',
    ];
}