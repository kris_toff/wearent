<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FrontendAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/style.css',
        'css/rating/min/rating.css',
    ];

    public $js = [
        'js/app.js',
        'js/headerMobile.js',
        'css/rating/min/rating.js',
    ];

    public $depends = [
        'frontend\assets\AppAsset',
    ];

}
