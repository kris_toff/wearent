<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 19.08.2017
 * Time: 15:57
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class CropperAsset extends AssetBundle {
    public $sourcePath = '@bower/cropperjs/dist';
    public $js         = [
        'cropper.js',
    ];
    public $css        = [
        'cropper.css',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}