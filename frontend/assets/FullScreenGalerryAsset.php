<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 20.08.2017
 * Time: 10:43
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class FullScreenGalerryAsset extends AssetBundle {

    public $sourcePath = '@bower/photoswipe/dist';

    public $js = [
        'photoswipe.min.js',
        'photoswipe-ui-default.min.js'
    ];
    public $css = [
        'photoswipe.css',
        'default-skin/default-skin.css',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}