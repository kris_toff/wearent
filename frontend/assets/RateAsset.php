<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 25.05.2017
 * Time: 13:50
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class RateAsset extends AssetBundle
{

    public $sourcePath = '@bower/rateYo/min';

    public $css = [

        'jquery.rateyo.min.css',
    ];

    public $js = [
        'jquery.rateyo.min.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}