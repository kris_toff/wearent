<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 22.08.2017
 * Time: 13:39
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class AngularAsset extends AssetBundle {
    public $sourcePath = '@bower/angular';

    public $js = [
        'angular/angular.min.js',
        'angular-animate/angular-animate.min.js',
        'angular-sanitize/angular-sanitize.min.js',
        'angular-aria/angular-aria.min.js',
        'angular-messages/angular-messages.min.js',
        'angular-resource/angular-resource.min.js',
        //        'angular-route/angular-route.min.js',
        'angular-touch/angular-touch.min.js',
        'angular-cookies/angular-cookies.min.js',
        'angular-message-format/angular-message-format.min.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\assets\MomentAsset',
    ];

    public function init() {
        parent::init();

        $this->js[] = 'angular-i18n/angular-locale_' . strtolower(\Yii::$app->language) . '.js';
    }
}
