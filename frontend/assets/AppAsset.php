<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl  = '@web';
    public $css      = [
        'css/page.less',
        'css/service.less',
        'css/astyle.css',
    ];
    public $js       = [
        'js/app.js',
        'js/ainit.js',
        'js/asearch.js',
    ];
    public $depends  = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'common\assets\Html5shiv',
        'frontend\assets\ScrollAsset',
        'common\assets\LodashAsset',

        'frontend\assets\AngularMaterialAsset',
        'frontend\assets\AngularUIAsset',
        'frontend\assets\AngularPluginsAsset',
    ];
}
