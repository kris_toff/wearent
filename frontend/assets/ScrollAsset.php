<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 29.05.2017
 * Time: 12:17
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class ScrollAsset extends AssetBundle
{

    public $baseUrl = '@web';
    public $basePath = '@webroot';

    public $js = [
        'js/scrollBar/perfect-scrollbar.jquery.min.js'

    ];
    public $css = [
        'css/scrollBar/perfect-scrollbar.min.css'

    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}