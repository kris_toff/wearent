<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 19.10.2017
 * Time: 16:14
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class FullCalendarAsset extends AssetBundle {
    public $sourcePath = '@bower/fullcalendar/dist';

    public $js  = [
        'fullcalendar.min.js',
        'gcal.min.js',
    ];
    public $css = [
        'fullcalendar.min.css',
//        'fullcalendar.print.min.css',
    ];

    public function init() {
        parent::init();

        if (file_exists(\Yii::getAlias($this->sourcePath . '/locale/' . strtolower(\Yii::$app->language) . '.js'))) {
            $this->js[] = 'locale/' . strtolower(\Yii::$app->language) . '.js';
        } elseif (file_exists(\Yii::getAlias($this->sourcePath
            . '/locale/'
            . substr(\Yii::$app->language, 0, 2)
            . '.js'))) {
            $this->js[] = 'locale/' . substr(\Yii::$app->language, 0, 2) . '.js';
        }
    }

    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\assets\MomentAsset',
    ];
}