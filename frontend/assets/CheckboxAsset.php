<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 21.06.2017
 * Time: 14:23
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class CheckboxAsset extends AssetBundle
{

    public $baseUrl = '@web';
    public $basePath = '@webroot';

    public $js = [
        'css/icheck/icheck.min.js'
    ];
    public $css = [
        'css/icheck/skins/flat/red.css'
    ];

    public $depends = [
        'frontend\assets\AppAsset',
    ];

}