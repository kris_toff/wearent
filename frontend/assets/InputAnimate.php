<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class InputAnimate extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/inputAnimate.css',
    ];
    public $js = [
        'js/inputAnimate.js',

    ];
    public $depends = [
        'frontend\assets\AppAsset',
    ];
}
