<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 26.05.2017
 * Time: 16:41
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class ReadmoreAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/readmore/readmore.min.js',
    ];
    public $css = [];

    public $depends = [
    'frontend\assets\AppAsset',
    ];

}