<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 28.04.2017
 * Time: 13:41
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class ProductAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/casa.js',
        'js/product.js',
        'js/category.js',
        'js/profile.js',

    ];
    public $css = [
        'css/product.css',
        'css/category.css',
        'css/profile.css',
        'css/search.css',
        'css/editProfile.css',
        'css/prodctEdit.css',
        'css/businessAccount.css',
        'css/amazing.css',
    ];

    public $depends = [
        'frontend\assets\AppAsset',
    ];
}