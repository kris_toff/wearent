<?php
/**
 * Created by IntelliJ IDEA.
 * User: Hp
 * Date: 20.06.2017
 * Time: 17:51
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class SliderAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl  = '@web';

    public $js  = [
        'js/slick/slick.min.js',
        'js/sliderInit.js',
    ];
    public $css = [
        'css/slick/slick.css',
        'css/sliderStyle.css',
    ];

    public $depends = [
        'frontend\assets\AppAsset',
    ];
}