<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.04.2017
 * Time: 23:07
 */

namespace backend\assets;


use yii\web\AssetBundle;

class PageAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl  = '@web';

    public $css     = [
        'css/page.min.css',
    ];
    public $js      = [
        'js/page.js',
    ];
    public $depends = [
        'backend\assets\BackendAsset',
    ];
}