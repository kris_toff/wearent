<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 31.08.2017
 * Time: 11:58
 */

namespace backend\controllers;


use backend\models\CurrencyExchange;
use common\models\Currency;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\UploadedFile;

class CurrencyController extends Controller {
    public $enableCsrfValidation = false;

    public function actionIndex() {
        $query = Currency::find();

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => ['defaultPageSize' => 50],
            'sort'       => false,
        ]);

        $model = new Currency();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model'        => $model,
        ]);
    }

    public function actionUpdate($id = -1) {
        $request = \Yii::$app->request;

        if ($request->isGet && !$request->isPjax) {
            return $this->redirect(['index']);
        } elseif ($id == -1) {
            $model = new Currency();
            $model->file = UploadedFile::getInstance($model, 'file');

            if (!($request->isPost && $model->load($request->post()) && $model->save())) {
                throw new BadRequestHttpException('Save new currency failed.');
            }
        } else {
            $model = Currency::findOne($id);
            if (is_null($model)) {
                throw new BadRequestHttpException("Not found currency with id $id.");
            } elseif ($request->isPatch && $model->load($request->getBodyParams())) {
                $model->file = UploadedFile::getInstance($model, 'file');

                if (!$model->validate()) {
//                    print_r($model->errors);
                    throw new BadRequestHttpException("Update currency with id $id failed. Validation not processed.");
                } elseif (!$model->save(false)) {
                    // обновление валюты не удалось
                    throw new BadRequestHttpException("Update currency with id $id failed. Save not processed.");
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        $model = Currency::findOne($id);
        $model->delete();

        return $this->redirect('index', 307);
    }

    public function actionAppend() {
        $request = \Yii::$app->request;

        if (!$request->isPjax) {
            return $this->redirect(['index'], 307);
        }

        $model = new Currency();
        if (!($model->load($request->post()) && $model->save())) {
            throw new BadRequestHttpException("Save new currency failed.");
        }

        $model = new Currency();

        return $this->render('append', [
            'model' => $model,
        ]);
    }

    public function actionExchange() {
        $query = CurrencyExchange::find()
            ->with('currencyIn', 'currencyOut');

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'defaultPageSize' => 35,
            ],
            'sort'       => false,
        ]);

        // принудительно обновить курс валют
        if (\Yii::$app->request->get('refresh', 0)) {
            CurrencyExchange::retrieve();

            return $this->redirect('exchange', 307);
        }

        return $this->render('exchange', [
            'dataProvider' => $dataProvider,
        ]);
    }

}