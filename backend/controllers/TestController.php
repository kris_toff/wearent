<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 29.08.2017
 * Time: 11:49
 */

namespace backend\controllers;


use frontend\models\rest\Chat;
use frontend\models\rest\ChatMessage;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class TestController extends Controller {

    public function actionIndex() {
        $query = ChatMessage::find();
        $model = $query->one();

        return $this->render('index', [
            'model' => $model,
            'dataProvider' => new ActiveDataProvider(['query' => $query, 'pagination' => false, 'sort' => false]),
        ]);
    }

}