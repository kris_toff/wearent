<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 10.04.2017
 * Time: 17:30
 */

namespace backend\controllers;

use backend\models\ProductAdvertisement;
use backend\models\UploadMedia;
use common\models\Currency;
use common\models\Product;
use common\models\ProductMediaPhoto;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\IntegrityException;
use yii\db\Schema;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use yiimodules\categories\models\Categories;

class ProductController extends Controller {

    public function actionIndex($id = 0) {
        \Yii::setAlias('@categories-assets', '@vendor/yiimodules/yii2-categories/assets');

        $category = null;
        if ($id > 0) {
            // нужно найти все категории и подкатегории
            $ids  = [];
            $nids = [$id];
            do {
                $ids  = array_merge($ids, $nids);
                $nids = Categories::find()->select('id')->where(['parent_id' => $nids])->column();
            } while (!empty($nids));
            $ids = array_unique($ids, SORT_NUMERIC);
            $ids = array_values($ids);

            $products = new ActiveDataProvider([
                'query'      => Product::find()
                    ->where(['category_id' => $ids, 'is_deleted' => 0])
                    ->with('category', 'currency'),
                'pagination' => [
                    'defaultPageSize' => 10,
                    'forcePageParam'  => false,
                ],
                'sort'       => false,
            ]);
            $category = Categories::findOne(['id' => $id]);
        } else {
            $query = Categories::find()
                ->select('id')
                ->where(['or', ['parent_id' => 0], ['parent_id' => null]])
                ->limit(1);

            $id = $query->scalar();

            return $this->redirect(['', 'id' => $id]);
        }

        return $this->render('index', [
            'id'                   => $id,
            'productsDataProvider' => $products,
            'category'             => $category,
        ]);
    }

    public function actionUpdate($id = 0, $cid = 0) {
        if ($cid > 0 && Categories::find()->where(['parent_id' => $cid, 'is_active' => 1])->exists()) {
            \Yii::$app->session->setFlash('error', 'Добавить товар можно только в категорию нижнего уровня.');

            return $this->redirect(['index']);
        } elseif ($id > 0) {
            $model = Product::find()->where(['id' => $id])->with('media', 'advertisement')->limit(1)->one();
        } else {
            $model              = new Product();
            $model->category_id = $cid;
        }

        $request = \Yii::$app->request;
        if ($request->isPost) {
            if (!$model->load($request->post())) {
                throw new BadRequestHttpException("Wrong data.");
            } elseif (!$model->validate()) {
                print_r(ArrayHelper::toArray($model));
                print_r($model->errors);
                exit;
            } elseif (!$model->save(false)) {
                throw new BadRequestHttpException("Save failed.");
            }

            // сохраняем изображения товара
            $photos = new UploadMedia();

            $photos->photos = UploadedFile::getInstances($photos, 'photos');
            if (!$photos->upload($id)) print 'error';

            // акции товара
            $ads = $model->advertisement;
            if (Model::loadMultiple($ads, $request->post())) {
                foreach ($ads as $a) {
                    if (!$a->save()) print_r($a->errors);
                }
            }
            $newAds = array_diff_key($request->post('ProductAdvertisement', []), $ads);
            foreach ($newAds as $a) {
                $r = new ProductAdvertisement();
                $r->load($a, '');
                $r->product_id = $id;
                $r->save();
            }

            $params = ['index', 'id' => $model->category_id];

            return $this->redirect($params);
        }

        $currency   = Currency::find()->where(['is_payment' => 1])->all();
        $currencyGN = ArrayHelper::map($currency, 'id', 'name');
        $currencyGC = ArrayHelper::map($currency, 'id', 'code');

        return $this->render('update', [
            'model'            => $model,
            'cid'              => $cid,
            'currencyListName' => $currencyGN,
            'currencyListCode' => $currencyGC,
        ]);
    }

    public function actionDelete($id) {
        Product::updateAll(['is_deleted' => 1], ['id' => $id]);

        return $this->redirect(['index']);
    }

    public function actionRemoveMedia() {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $id = \Yii::$app->request->post('id');

        ProductMediaPhoto::updateAll(['is_deleted' => 1], ['id' => $id]);

        return ['result' => 'ok', 'product' => $id];
    }

    public function actionCloseAds($id, $product) {
        $model            = ProductAdvertisement::findOne(['id' => $id]);
        $model->active    = 0;
        $model->stop_sell = time();
        $model->update();

        return $this->redirect(['update', 'id' => $product]);
    }

    public function actionTemplate($name) {
        return $this->renderAjax("template/$name");
    }
}