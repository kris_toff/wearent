/**
 * Created by Planess group on 11.04.2017.
 */
(function ($) {
    // плавающая панель с seo настройками, редактирование статей и страниц
    $(".drop-panel").on("click", ".drop-panel-title", function (e) {
        $(e.target).closest(".drop-panel").toggleClass("active");
    });
    // появление дополнительных свойств доставки, редактирование товара
    $("#product-delivery_status").on("change", function (e) {
        // $("#depend-delivery").toggleClass("active", $(e.target).prop("checked"));
        if ($(e.target).prop("checked")) {
            $("#depend-delivery").show(300);
        }
        else {
            $("#depend-delivery").hide(300);
        }
    });
    // смена валют, редактирование товара
    var addonCurrency = $("#product-price,#product-delivery_price,#product-pledge_price").next(".input-group-addon");
    var curIdF = $("#product-currency_id");
    addonCurrency.text(currencyCodes[curIdF.val()]);
    curIdF.on("change", function (e) {
        addonCurrency.text(currencyCodes[$(e.target).val()]);
    });
    // смена валют на вкладке рекламы товара
    var addonAdvCurrency = $("#productadvertisement-click_cost,#productadvertisement-budget")
        .next(".input-group-addon");
    var curIdA = $("#productadvertisement-currency_id");
    addonAdvCurrency.text(currencyCodes[curIdA.val()]);
    curIdA.on("change", function (e) {
        addonAdvCurrency.text(currencyCodes[$(e.target).val()]);
    });
    // смена шага аренды товара (сутки, неделя, месяц)
    var addonLease = $("#product-min_period,#product-max_period");
    var leaseF = $("#product-lease_time");
    addonLease.attr("placeholder", "** " + leaseF.find("option:selected").text());
    leaseF.on("change", function (e) {
        addonLease.attr("placeholder", "** " + $(e.target).find("option:selected").text());
    });
    // удаление изображения, редактирование товара через админ
    $(".box-body").on("click", "a[data-purpose='remove']", function (e) {
        e.preventDefault();
        var thumb = $(e.target).closest(".thumbnail");
        if (!thumb.length)
            return;
        var id = thumb.data("id");
        $.post({
            url: "/admin/product/remove-media",
            data: { id: id }
        })
            .done(function (data) {
            if (data.result == "ok") {
                thumb.parent("div").remove();
            }
        });
    });
    // добавление новой рекламы, редактирование через админ
    var idx = 1;
    $("#new-adv-btn").on("click", function (e) {
        e.preventDefault();
        $.get({
            url: "/admin/product/template?name=ads&index=" + idx
        })
            .done(function (data) {
            $("#adv-list").prepend(data);
        });
        idx++;
    });
    // удаление новых записей рекламы
    $("form").on("click", ".adv-present a.remove-block", function (e) {
        e.preventDefault();
        $(e.target).closest(".adv-present").remove();
    });
}(window.jQuery));
