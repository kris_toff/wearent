<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\bootstrap\ActiveForm */

?>

<div class="page-form">
    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?php echo $form->field($model, 'body')->widget(
            \yii\imperavi\Widget::className(),
            [
                'plugins' => ['fullscreen', 'fontcolor', 'video'],
                'options' => [
                    'minHeight'    => 400,
                    'maxHeight'    => 400,
                    'buttonSource' => true,
                    'imageUpload'  => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi']),
                ],
            ]
        ) ?>

<!--    --><?php //echo $form->field($model, 'view')->textInput(['maxlength' => true]) ?>

    <div class="drop-panel">
        <div class="drop-panel-title"><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span> SEO</div>
        <div class="drop-panel-content">

            <?php echo $form->field($model, 'slug', ['enableAjaxValidation' => true])->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'h1')->textInput() ?>
            <?= $form->field($model, 'description')->textarea() ?>
            <?= $form->field($model, 'keywords')->textInput() ?>
        </div>
    </div>

    <?php echo $form->field($model, 'status')->checkbox() ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
