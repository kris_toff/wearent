<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 31.08.2017
 * Time: 20:39
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;
use dosamigos\switchinput\SwitchBox;

/** @var \yii\web\View $this */
/** @var \common\models\Currency $model */


?>

<?php Pjax::begin([
    'id'              => 'append_currency_box-pjax',
    'timeout'         => 2500,
    'enablePushState' => false,
]) ?>

<p>Добавляя валюту следует обновить курс перевода, иначе на сайте будут ошибочные значения цен для товаров.</p>

<?php $form = ActiveForm::begin([
    'id'      => 'append_currency-form',
    'action'  => ['append'],
    'options' => [
        'data-pjax' => 1,
    ],
]) ?>

<div class="row">
    <div class="col-sm-8">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'is_payment')->widget(SwitchBox::className(), [
            'inlineLabel'   => false,
            'options'       => ['label' => false],
            'clientOptions' => [
                'size'     => 'normal',
                'onText'   => 'YES',
                'offText'  => 'NO',
                'onColor'  => 'info',
                'offColor' => 'default',
            ],
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?= $form->field($model, 'code')->textInput() ?>
    </div>
    <div class="col-sm-6">
        <?= $form->field($model, 'digital_code')->textInput() ?>
    </div>
</div>

<?= Html::submitButton('', ['class' => 'hidden']) ?>
<?php ActiveForm::end() ?>

<?php Pjax::end() ?>

