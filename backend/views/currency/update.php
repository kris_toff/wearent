<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 31.08.2017
 * Time: 13:45
 */

use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use dosamigos\switchinput\SwitchBox;
use kartik\file\FileInput;

/** @var \yii\web\View $this */
/** @var \common\models\Currency $model */
?>

<?php Pjax::begin([
    'id'      => 'currency_info_box-pjax',
    'timeout' => 2500,
]) ?>

<?php $form = ActiveForm::begin([
    'id'          => 'update_currency-form',
    'action'      => ['update', 'id' => $model->id],
    'method'      => 'patch',
    'options'     => [
        'data-pjax' => 1,
    ],
    'fieldConfig' => [
        'hintOptions' => [
            'class' => 'text-muted',
        ],
    ],
]) ?>

<?= $form->field($model, 'name')
    ->textInput()
    ->hint('Название валюты для удобства. Только на этой странице.')
    ->label('Название') ?>
<?= $form->field($model, 'code')
    ->textInput()
    ->hint('Код валюты из 3 букв согласно <a href="//www.iso.org/iso-4217-currency-codes.html" target="_blank">стандарту ISO 4217</a>.')
    ->label('Символьный код') ?>
<?= $form->field($model, 'digital_code')
    ->textInput()
    ->hint('Числовой код валюты согласно <a href="//www.iso.org/iso-4217-currency-codes.html" target="_blank">стандарту ISO 4217</a>.')
    ->label('Числовой код') ?>

<?= $form->field($model, 'is_payment')->widget(SwitchBox::className(), [
    'inlineLabel'   => false,
    'options'       => ['label' => false],
    'clientOptions' => [
        'size'     => 'normal',
        'onText'   => '+ YES',
        'offText'  => '- NO',
        'onColor'  => 'info',
        'offColor' => 'default',
    ],
]) ?>

<?= $form->field($model, 'file')->widget(FileInput::className(), [
    'pluginOptions' => [
        'uploadAsync' => false,

        'language'           => 'ru',
        'autoReplace'        => true,
        'showRemove'         => false,
        'showUpload'         => false,
        'showCaption'        => false,
        'showCancel'         => false,
        'showClose'          => false,
        'showZoom'           => false,
        'showDrag'           => false,
        'showUploadedThumbs' => false,

        'browseClass'     => 'btn btn-info btn-block',
        'browseIcon'      => '<i class="glyphicon glyphicon-camera"></i>',
        'previewFileType' => 'image',

        'initialPreview'           => is_null($model->icon) ? [] : [
            $model->icon . '?v=' . time(),
        ],
        'initialPreviewAsData'     => true,
        'initialCaption'           => false,
        'initialPreviewShowDelete' => false,
        'overwriteInitial'         => true,

        'allowedFileTypes'      => ['image'],
        'allowedFileExtensions' => ['png', 'jpg', 'gif'],

        //        'minImageHeight' => 30,
        //        'maxImageHeight' => 160,
        //        'minImageWidth' => 30,
        //        'maxImageWidth' => 160,
        'maxFileSize'           => 320,

        'layoutTemplates' => [
            'actions'  => '',
            'progress' => '',
            'footer'   => '<p></p>',
        ],
    ],
    'options'       => [
        'accept' => 'image/*',
    ],
]) ?>

<?= Html::submitButton('Save', ['class' => 'btn btn-default btn-block']) ?>

<?php ActiveForm::end() ?>

<?php Pjax::end() ?>
