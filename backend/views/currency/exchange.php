<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 31.08.2017
 * Time: 17:56
 */

use yii\grid\GridView;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */

?>

<div class="row">
    <div class="col-sm-3">
        Time now: <?= \Yii::$app->formatter->asTime(new DateTime('now', new DateTimeZone('Europe/Moscow'))); ?>
    </div>
    <div class="col-sm-9">
        <div class="text-right">
            <?= Html::a('Обновить принудительно', ['', 'refresh' => 1], ['class' => 'btn btn-info']) ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns'      => [
                [
                    'class'     => 'yii\grid\DataColumn',
                    'attribute' => 'currency_withdraw',
                    'label'     => 'Продаю',
                    'format'    => 'html',
                    'value'     => function($model, $key, $index, $column) {
                        $options = [];
                        $text    = "{$model->currencyIn->name} (100 {$model->currencyIn->code})";

                        if ((int)$model->currencyIn->is_payment) {
                            Html::addCssClass($options, 'text-success');
                            $text = Html::tag('strong', $text);
                        } else {
                            Html::addCssClass($options, 'text-danger');
                        }

                        return Html::tag('span', $text, $options);
                    },
                ],
                [
                    'class'     => 'yii\grid\DataColumn',
                    'attribute' => 'currency_receive',
                    'label'     => 'Покупаю',
                    'format'    => 'html',
                    'value'     => function($model) {
                        $options = [];
                        $text    = "{$model->currencyOut->name} ({$model->currencyOut->code})";

                        if ((int)$model->currencyOut->is_payment) {
                            Html::addCssClass($options, 'text-success');
                            $text = Html::tag('strong', $text);
                        } else {
                            Html::addCssClass($options, 'text-danger');
                        }

                        return Html::tag('span', $text, $options);
                    },
                ],
                [
                    'class'     => 'yii\grid\DataColumn',
                    'attribute' => 'value',
                    'label'     => 'Цена',
                    'format'    => 'text',
                    'value'     => function($model, $key, $index, $column) {
                        return 100 * $model[ $column->attribute ];
                    },
                ],
                [
                    'class'     => 'yii\grid\DataColumn',
                    'attribute' => 'updated_at',
                    'label'     => 'Обновлено',
                    'format'    => ['date', 'dd MMMM y H:mm:ss'],
                ],
            ],
        ]); ?>
    </div>
</div>
