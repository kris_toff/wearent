<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 31.08.2017
 * Time: 11:59
 */

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \common\models\Currency $model */

\dosamigos\switchinput\SwitchAsset::register($this);
?>

    <div class="row">
        <div class="col-md-8 text-right">
            <?= Html::button('Добавить новую валюту', ['class' => 'btn btn-info btn-sm', 'id' => 'add_currency-btn']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">

            <?php Pjax::begin([
                'id'                 => 'currency_list-pjax',
                'timeout'            => 2500,
                'enableReplaceState' => true,
                'linkSelector'       => '#currency_table_info_list .pagination a',
            ]); ?>

            <?= GridView::widget([
                'id'           => 'currency_table_info_list',
                'dataProvider' => $dataProvider,
                'columns'      => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'name:text:Название валюты',
                    [
                        'class'         => 'yii\grid\DataColumn',
                        'attribute'     => 'code',
                        'format'        => 'text',
                        'label'         => 'Код',
                        'headerOptions' => [
                            'data-toggle'  => 'popover',
                            'data-content' => 'Символьный код валюты в соответствии с международным стандартом ISO 4217.',
                        ],
                    ],
                    [
                        'class'         => 'yii\grid\DataColumn',
                        'attribute'     => 'digital_code',
                        'format'        => 'text',
                        'label'         => 'Цифровой код',
                        'headerOptions' => [
                            'data-toggle'  => 'popover',
                            'data-content' => 'Цифровой код валюты согласно международному стандарту ISO 4217.',
                        ],
                    ],
                    [
                        'class'     => 'yii\grid\DataColumn',
                        'attribute' => 'is_payment',
                        'format'    => 'text',
                        'label'     => 'Платёжная?',
                        'value'     => function($model, $key, $index, $column) {
                            return $model[ $column->attribute ] ? '+' : '-';
                        },
                    ],
                    [
                        'class'     => 'yii\grid\DataColumn',
                        'attribute' => 'icon',
                        'format'    => 'html',
                        'value'     => function($model, $key, $index, $column) {
                            $src = $model[ $column->attribute ];

                            return is_null($src) ? null : Html::img($src . '?v=' . time());
                        },
                    ],
                    [
                        'class'    => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                        'buttons'  => [
                            'update' => function($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                            },
                        ],
                    ],
                ],
                'options'      => [
                    'data-pjax' => true,
                ],
            ]) ?>

            <?php Pjax::end() ?>
        </div>
        <div class="col-md-4">

            <?php Pjax::begin([
                'id'              => 'currency_info_box-pjax',
                'enablePushState' => false,
                'timeout'         => 2500,
                //                        'linkSelector'       => '#test-link',
                'linkSelector'    => '#currency_table_info_list table a',
            ]) ?>

            <?php Pjax::end() ?>
        </div>
    </div>

    <div id="new_currency-box" title="Новая валюта">

        <?= $this->render('append', [
            'model' => $model,
        ]) ?>

    </div>

<?php
$this->registerJs(<<<JS
var statusForm = false;

// элементы DOM
var cibp = jQuery("#currency_info_box-pjax");

cibp
    .on("submit", "form", function(){
        statusForm = true;
    })
    .on("pjax:success", function(event){
        if (statusForm){
             jQuery.pjax.reload("#currency_list-pjax", {
                replace: true,
                scrollTo: false
            });
        }
    })
    .on("pjax:error pjax:timeout", function(event){
        event.preventDefault();
        
        jQuery("#currency_table_info_list").before('error');
    })
    .on("pjax:end", function(){
        var hb = cibp.height();
        var hw = jQuery(window).height();
        
        if (hb > hw) {
            // Высота формы больше высоты окна браузера. Прокрутить страницу к верху начала формы
            jQuery("body,html").animate({scrollTop: cibp.offset().top - 3}, 450);
        } else {
            // Высота формы меньше высоты окна. Ориентируемся на нижнюю границу формы и прокручиваем к нижней стороне окна браузера
            if (cibp.offset().top < jQuery(document).scrollTop()){
                // покрутим страницу к верхнему краю формы
                jQuery("body,html").animate({scrollTop: cibp.offset().top - 3}, 450);
            } else if (cibp.offset().top + hb > jQuery(document).scrollTop() + hw){
                // покрутим страницу к нижнему краю формы
                jQuery("body,html").animate({scrollTop: cibp.offset().top + hb - hw + 7}, 450);
            }
        }
        
        
        statusForm = false;
    });

jQuery("#append_currency_box-pjax")
    .on("pjax:success", function(){
        jQuery.pjax.reload("#currency_list-pjax", {
            replace: true,
            scrollTo: false
        });
    });

jQuery("#currency_list-pjax")
    .on("pjax:end", initPopover);

// add new currency
jQuery("#add_currency-btn").on("click", function(){
    jQuery("#new_currency-box").dialog("open");
});
jQuery("#new_currency-box").dialog({
    autoOpen: false,
    closeOnEscape: true,
    position: {my: "right top+6", at: "right bottom", of: "#add_currency-btn"},
    resizable: false,
    buttons: [{
        text: "Сохранить",
        icon: "ui-icon-disk",
        click: function(){
            jQuery(this).find("form").trigger("submit");
        }
    }],
    minHeight: 400,
    minWidth: 500,
    maxHeight: 750,
    maxWidth: 950
});
jQuery("#new_currency-box")
    .on("dialogclose", function(event){
        jQuery(event.currentTarget).find("form").get(0).reset();
    });

function initPopover(){
     jQuery("#currency_table_info_list").find('[data-toggle="popover"]').popover({
        container: "body",
        placement: "bottom",
        trigger: "hover",
        delay: {"show": 500}
    });
}

initPopover();
JS
);

$this->registerCss(<<<CSS
#currency_table_info_list table img {
    max-width: 60px;
}
CSS
);