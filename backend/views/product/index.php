<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 10.04.2017
 * Time: 17:31
 */

use yii\helpers\Html;
use yiimodules\categories\ModuleAsset;

/** @var \yii\web\View $this */
/** @var \common\models\Product[] $productsDataProvider */
/** @var \yiimodules\categories\models\Categories $category */

ModuleAsset::register($this);

$this->params['breadcrumbs'] = [
    [
        'label' => 'Products',
        'url'   => ['/product'],
    ],
    [
        'label' => 'Список',
    ],
];
$this->title                 = 'Просмотр товаров';
?>

    <div class="row">
        <div class="col-sm-12">
            <!-- errors -->
            <?php if (\Yii::$app->session->hasFlash('error')): ?>
                <?= \yii\bootstrap\Alert::widget([
                    'options' => [
                        'class' => 'alert-danger',
                    ],
                    'body'    => \Yii::$app->session->getFlash('error'),
                ]) ?>
            <?php endif; ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div id="categories-jstree">
                <?= \yiimodules\categories\models\Categories::createTreeList(null, $category->id); ?>
            </div>

            <div>
                <a href="/category" target="_blank">Каталог</a>
            </div>

        </div>
        <div class="col-md-9">

            <?php if (!$id): ?>
                Выберите категорию слева
            <?php else: ?>
                <div>
                    Выбрана категория <strong><?= $category->name ?></strong>. <?= Html::a('Создать товар здесь?',
                        ['update', 'cid' => $category->id], ['class' => 'btn btn-link']) ?>
                </div>

                <?= \yii\grid\GridView::widget([
                    'dataProvider' => $productsDataProvider,
                    'columns'      => [
                        'id',
                        'name:text:Товар',
                        [
                            'class'     => \yii\grid\DataColumn::className(),
                            'attribute' => 'category.name',
                            'label'     => 'Category (id)',
                            'value'     => function($model, $key, $index) {
                                return "{$model->category->name} ({$model->category_id})";
                            },
                        ],
                        [
                            'class'     => \yii\grid\DataColumn::className(),
                            'attribute' => 'delivery_status',
                            'label'     => 'Доставка/Расстояние/Цена',
                            'value'     => function($model, $key, $index, $column) {
//                                $i = (bool)$model->delivery_status;
//                                $r = $i ? '+' : '-';
                                $r = '';$i = false;
                                if ($i) {
                                    $r .= "/{$model->delivery_radius}км./{$model->delivery_price}{$model->currency->code}.";
                                }

                                return $r;
                            },
                        ],
                        [
                            'class'     => \yii\grid\DataColumn::className(),
                            'attribute' => 'price',
                            'value'     => function($model, $key, $index) {
                                return "{$model->price}/{$model->leasing}";
                                return "{$model->price}{$model->currency->code}/{$model->leasing}";
                            },
                        ],
                        [
                            'class'     => \yii\grid\DataColumn::className(),
                            'attribute' => 'max_period',
                            'value'     => function($model, $key, $index) {
                                $mx = (int)$model->max_period;

                                return $mx ? mb_strtolower("{$model->max_period} {$model->leasing}",
                                    'UTF-8') : '&#8734;';
                            },
                            'format'    => 'html',
                        ],
                        [
                            'class'    => \yii\grid\ActionColumn::className(),
                            'template' => '{update} {delete}',

                        ],
                    ],
                ]) ?>
            <?php endif; ?>
        </div>
    </div>

<?php
$this->registerJs(<<<JS
    var el = $("#categories-jstree");
    el.jstree();
    el.on("select_node.jstree", function(e, data){
        var url = data.node.a_attr.href;
        var params = url.match(/^.+(\?[^?]*)$/i);
        window.location.href = "/admin/product/index"+params[1];
    });
JS
    , \yii\web\View::POS_READY);