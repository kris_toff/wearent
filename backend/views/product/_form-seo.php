<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 11.04.2017
 * Time: 13:47
 */

use yii\bootstrap\Html;

/** @var \yii\web\View $this */
/** @var \common\models\Product $model */

/** @var \yii\bootstrap\ActiveForm $form */
?>

<?php
print $form->field($model, 'seo_title')->textInput();
print $form->field($model, 'seo_h1')->textInput();
print $form->field($model, 'seo_description')->textarea();
print $form->field($model, 'seo_keywords')->textInput();
print $form->field($model, 'slug')->textInput()->hint('Будет взят <b>title</b> автоматически, если  не указать строку.');

print Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'btn btn-success'])
?>

