<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 15.04.2017
 * Time: 12:35
 */

use yii\jui\DatePicker;
use yii\bootstrap\Html;

/** @var \yii\web\View $this */

$model        = new \backend\models\ProductAdvertisement();
$currencyName = \yii\helpers\ArrayHelper::map(\common\models\Currency::find()->all(), 'id', 'name');
$index        = 'new-' . \Yii::$app->request->get('index');
?>


<div class="adv-present active">
    <div class="present-head">

        <div class="row">
            <div class="col-sm-12">
                <div class="pull-left">
                    <a href="#" class="remove-block">remove this adv!</a>
                </div>
                <div class="pull-right">
                    <?= DatePicker::widget([
                        'model'     => $model,
                        'attribute' => "[{$index}]date_start",
                    ]) ?>
                    -
                    <?= DatePicker::widget([
                        'model'     => $model,
                        'attribute' => "[{$index}]date_stop",
                    ]) ?>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="form-group">
                    <?= Html::activeLabel($model, 'budget') ?>
                    <div class="input-group">
                        <?= Html::activeTextInput($model, "[{$index}]budget", ['class' => 'form-control']) ?>
                        <span class="input-group-addon"></span>
                    </div>
                </div>

            </div>

            <div class="col-lg-4 col-md-6">
                <div class="form-group">
                    <?= Html::activeLabel($model, 'click_cost') ?>
                    <div class="input-group">
                        <?= Html::activeTextInput($model, "[{$index}]click_cost", ['class' => 'form-control']) ?>
                        <span class="input-group-addon"></span>
                    </div>
                </div>

            </div>

            <div class="col-lg-4">
                <div class="form-group">
                    <?= Html::activeLabel($model, 'currency_id') ?>
                    <?= Html::activeDropDownList($model, "[{$index}]currency_id", $currencyName, ['class' => 'form-control']) ?>
                </div>

            </div>

            <div class="col-sm-12">
                <div class="form-group">
                    <?= Html::activeLabel($model, 'target_area') ?>
                    <?= Html::activeTextarea($model, "[{$index}]target_area", ['class' => 'form-control']) ?>
                </div>

            </div>
        </div>
    </div>
</div>

