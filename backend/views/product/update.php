<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 11.04.2017
 * Time: 11:14
 */

use \yii\bootstrap\ActiveForm;

/** @var \yii\web\View $this */
/** @var \common\models\Product $model */
/** @var integer $cid Идентификатор категории, если это новый товар */
/** @var array $currencyListName */
/** @var array $currencyListCode */

\backend\assets\PageAsset::register($this);

$this->params['breadcrumbs'] = [
    [
        'label' => 'Список товаров',
        'url' => ['/product/index', 'id' => $cid],
    ],
    'Форма товара'
];
$this->title                 = $model->isNewRecord ? 'Создание товара' : 'Редактирование';
?>


<?php $form = ActiveForm::begin() ?>

<?= \yii\bootstrap\Tabs::widget([
    'items' => [
        [
            'label'   => 'Form',
            'content' => $this->render('_form-update',
                [
                    'model'        => $model,
                    'form'         => $form,
                    'currencyName' => $currencyListName,
                ]),
            'active'  => true,
        ],
        [
            'label'   => 'SEO',
            'content' => $this->render('_form-seo', ['model' => $model, 'form' => $form]),
        ],
        [
            'label'   => 'Images',
            'content' => $this->render('_form-images', ['form' => $form, 'models' => $model->media]),
            'visible' => !$model->isNewRecord,
        ],
        [
            'label'   => 'Ads',
            'content' => $this->render('_form-ads',
                ['form' => $form, 'models' => $model->advertisement, 'currencyName' => $currencyListName]),
        ],
        [
            'label'   => 'Attributes',
            'content' => $this->render('_form-attr', ['form' => $form, 'model' => $model]),
            'visible' => !$model->isNewRecord,
        ],
    ],
]) ?>

<?php ActiveForm::end() ?>

<?php
$cc = \yii\helpers\Json::encode($currencyListCode);
$this->registerJs(<<<JS
var currencyCodes = {$cc};
JS
    , \yii\web\View::POS_BEGIN);