<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 12.04.2017
 * Time: 23:17
 */

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;

/** @var \yii\web\View $this */
/** @var \yii\bootstrap\ActiveForm $form */
/** @var \backend\models\ProductAdvertisement[] $models */
/** @var array $currencyName */

?>

<div class="row">
    <div class="col-sm-12">
        <div class="text-right">
            <?= Html::a('Добавить рекламу', '#', ['class' => 'btn btn-link', 'id' => 'new-adv-btn']) ?>
        </div>
    </div>
    <div class="col-sm-12" id="adv-list">

        <?php foreach ($models as $idx => $model) : ?>
            <?php
            $params = ['class' => 'adv-present'];
            $t      = time();
            $active = false;

            if ((bool)$model->active && (int)$model->start_sell <= $t && (int)$model->stop_sell > $t
                && (float)$model->budget > 0
            ) {
                Html::addCssClass($params, 'active');
                $active = true;
            }
            print Html::beginTag('div', $params); ?>
            <div class="present-head">

                <?php
                if ($active) {
                    print $this->render('_form-ads-item-active', [
                        'model'        => $model,
                        'index'        => $idx,
                        'currencyName' => $currencyName,
                        'form'         => $form,
                    ]);

                } else {
                    print $this->render('_form-ads-item-closed', [
                        'model' => $model,
                    ]);
                }
                ?>
            </div>
            <div class="clearfix"></div>
            <?= Html::endTag('div') ?>

        <?php endforeach; ?>

    </div>
</div>


<?php
/*
?>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <div class="checkbox">
                <?= Html::label(Html::checkbox('is-advertisement', true, ['id' => 'is_advertisement_product'])
                    . 'Реклама товара', 'is_advertisement_product', []) ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-8">
                <?= $form->field($model, 'budget', [
                    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"></span></div>',
                ])->textInput() ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'currency_id')->dropDownList($currencyName) ?>
            </div>
        </div>


        <?= $form->field($model, 'click_cost', [
            'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"></span></div>',
        ])->textInput() ?>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'start_sell')->widget(\yii\jui\DatePicker::className(), [
                    'clientOptions' => [
                        'minDate' => 0,
                    ],
                    'options'       => [
                        'class' => 'form-control',
                    ],
                ]) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'stop_sell')->widget(\yii\jui\DatePicker::className(), [
                    'clientOptions' => [
                        'minDate' => 1,
                    ],
                    'options'       => [
                        'class' => 'form-control',
                    ],
                ])->hint('Либо до окончания средств на рекламу.') ?>
            </div>
        </div>

        <?= $form->field($model, 'target_area')->textarea() ?>
    </div>
</div>
*/
?>


<?= Html::submitButton('GO', ['class' => 'btn btn-success']) ?>


