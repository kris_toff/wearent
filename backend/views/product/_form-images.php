<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 12.04.2017
 * Time: 1:24
 */

use yii\bootstrap\Html;

/** @var \yii\web\View $this */
/** @var \yii\bootstrap\ActiveForm $form */
/** @var \common\models\Product $models */

$model = new \backend\models\UploadMedia();

$webroot = \Yii::getAlias('@frontend/web');
$infoImg = function($path) use ($webroot) {
    list($w, $h) = getimagesize($webroot . '/' . ltrim($path, '/'));

    return "({$w}x{$h})";
};
?>
    <div class="row">
        <div class="col-sm-12">
            <?= \yii\bootstrap\Alert::widget([
                'options'     => [
                    'class' => 'alert-warning',
                ],
                'body'        => 'При удалении изображения восстановить его будет невозможно!',
                'closeButton' => false,
            ]) ?>
        </div>
    </div>

    <div class="row">
        <?php $i = 1; ?>
        <?php foreach ($models as $item): ?>
            <div class="col-lg-2 col-md-3 col-sm-6">
                <div class="thumbnail" data-id="<?= $item->id ?>">
                    <?= Html::img($item->link,
                        ['class' => 'img-responsive', 'alt' => "Photo{$i}", 'data-id' => $item->id]) ?>
                    <div class="caption">
                        <div>
                            <?= Html::a('delete', '#', ['data-purpose' => 'remove', 'class' => 'pull-right']) ?>
                            <?= Html::tag('span', $infoImg($item->link)) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

<?= $form->field($model, 'photos[]')->fileInput(['multiple' => true, 'accept' => 'image/*'])->label(false) ?>

<?= Html::submitButton('GO', ['class' => 'btn btn-success']) ?>