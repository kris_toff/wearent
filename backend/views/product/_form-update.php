<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 11.04.2017
 * Time: 13:36
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

use yiimodules\categories\models\Categories;

/** @var \yii\web\View $this */
/** @var \common\models\Product $model */
/** @var array $currencyName */

/** @var ActiveForm $form */

$ct  = Categories::find()->indexBy('id')->all();
$gc  = ArrayHelper::map($ct, 'id', 'name', function($v) {
    return intval($v['parent_id']);
});
$cat = rom(ArrayHelper::remove($gc, 0, []), $gc);

function rom($items, $list) {
    $ls = [];

    foreach ($items as $index => $name) {
        if (ArrayHelper::keyExists($index, $list)) {
            $ls[ $name ] = rom(ArrayHelper::remove($list, $index), $list);
        } else {
            $ls[ $index ] = $name;
        }
    }

    return $ls;
}

//print '<pre>';print_r($cat);exit;
?>


<?= $form->field($model, 'category_id')->dropDownList($cat) ?>
<?= $form->field($model, 'name')->textInput() ?>
<?= $form->field($model, 'description')->textarea() ?>
<? //= $form->field($model, 'address')->textarea() ?>

<div id="depend-delivery" class="thide row" style="display: <?= (bool)$model->delivery_status ? 'block' : 'none' ?>;">
    <?= $form->field($model, 'delivery_radius', [
        'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">км.</span></div>',
    ])->textInput() ?>
    <?= $form->field($model, 'delivery_price', [
        'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"></span></div>',
    ])->textInput()->hint('Валюта этой позиции такая же как валюта товара.') ?>
</div>

<div class="row">
    <div class="col-sm-8">
        <?= $form->field($model, 'price', [
            'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"></span></div>',
        ])->textInput()->hint('Разделителем целой и дробной части используйте точку (.).') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'currency_id')->dropDownList($currencyName, ['prompt' => 'Валюта...']) ?>
    </div>
</div>

<?= $form->field($model, 'lease_time')->dropDownList([
    1 => 'Сутки',
    2 => 'Неделя',
    3 => 'Месяц',
    4 => 'Час',
]) ?>
<?= $form->field($model, 'lease_max_time')->dropDownList([
    1 => 'Сутки',
    2 => 'Неделя',
    3 => 'Месяц',
    4 => 'Час',
], ['prompt' => '']) ?>

<?= $form->field($model, 'pledge_price', [
    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"></span></div>',
])
    ->textInput()
    ->hint('Разделителем целой и дробной части используйте точку (.).') ?>
<?= $form->field($model, 'min_period')
    ->textInput(['placeholder' => 'C'])
    ->hint('Количество <b>единиц</b> шагов аренды. По-умолчанию принимается 1.') ?>
<?= $form->field($model, 'max_period')
    ->textInput(['placeholder' => 'c'])
    ->hint('Количество <b>единиц</b> шагов аренды. Не указывайте ничего если максимальный период аренды не требуется.') ?>

<?= $form->field($model, 'place_id') ?>

<div class="row">
    <div class="col-sm-4">
        <?= $form->field($model, 'time_from')->textInput() ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'time_to')->textInput() ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'timezone')->dropDownList([]) ?>
    </div>
</div>

<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'btn btn-success']) ?>




