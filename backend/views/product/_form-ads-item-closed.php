<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 15.04.2017
 * Time: 0:01
 */

/** @var \yii\web\View $this */
/** @var \backend\models\ProductAdvertisement $model */
?>


<div class="row">
    <div class="col-sm-12">
        <div class="pull-right">
            <?= \Yii::$app->formatter->asDate($model->start_sell) ?>
            -
            <?= \Yii::$app->formatter->asDate($model->stop_sell) ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        Бюджет: <?= \Yii::$app->formatter->asCurrency($model->budget, $model->currency->code) ?>
    </div>
    <div class="col-md-6">
        Цена за клик: <?= \Yii::$app->formatter->asCurrency($model->click_cost , $model->currency->code) ?>
    </div>
    <div class="col-md-12">
        Города: <?= $model->target_area ?>
    </div>

</div>