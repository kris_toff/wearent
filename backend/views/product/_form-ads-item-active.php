<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 15.04.2017
 * Time: 0:01
 */
use yii\jui\DatePicker;

/** @var \yii\web\View $this */
/** @var \backend\models\ProductAdvertisement $model */
/** @var integer $index */

?>

<div class="row">
    <div class="col-sm-12">
        <div class="pull-left">
            <a href="/admin/product/close-ads?id=<?= $index ?>&product=<?= $model->id ?>">close this adv!</a>
        </div>
        <div class="pull-right">
            <?= DatePicker::widget([
                'model'     => $model,
                'attribute' => "[{$index}]date_start",
            ]) ?>
            -
            <?= DatePicker::widget([
                'model'     => $model,
                'attribute' => "[{$index}]date_stop",
            ]) ?>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-4 col-md-6">
        <?= $form->field($model, "[{$index}]budget", [
            'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"></span></div>',
        ])->textInput() ?>
    </div>

    <div class="col-lg-4 col-md-6">
        <?= $form->field($model, "[{$index}]click_cost", [
            'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"></span></div>',
            'enableClientValidation' => false,
        ])->textInput() ?>
    </div>

    <div class="col-lg-4">
        <?= $form->field($model, "[{$index}]currency_id")->dropDownList($currencyName) ?>
    </div>

    <div class="col-sm-12">
        <?= $form->field($model, "[{$index}]target_area")->textarea() ?>
    </div>
</div>

