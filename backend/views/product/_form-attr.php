<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 30.05.2017
 * Time: 23:34
 */

use yii\bootstrap\Html;

/** @var \yii\web\View $this */
/** @var \yii\bootstrap\ActiveForm $form */
/** @var \common\models\Product $model */
?>

<?php
//print $form->field($model, 'c1', ['class' => '\mirocow\eav\widgets\ActiveField'])->eavInput();
foreach($model->getEavAttributes()->all() as $attr){
    print $form->field($model, $attr->name, ['class' => '\mirocow\eav\widgets\ActiveField'])->eavInput();
}
?>
<?//= $form->field($model, 'c12', ['class' => '\mirocow\eav\widgets\ActiveField'])->eavInput() ?>

<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
