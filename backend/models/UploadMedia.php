<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 12.04.2017
 * Time: 13:54
 */

namespace backend\models;


use common\models\ProductMediaPhoto;
use yii\base\Model;
use yii\helpers\FileHelper;

class UploadMedia extends Model {

    public $photos;

    public function rules() {
        return [
            ['photos', 'image', 'extensions' => 'png, jpg', 'maxFiles' => 20],
        ];
    }

    public function attributeLabels() {
        return [
            'photos' => 'Изображения',
        ];
    }

    public function upload($pid) {
        if (!$this->validate()) return false;

        $webroot = \Yii::getAlias('@frontend/web');
        $linkDir = "/img-catalog/products/id{$pid}/photos";

        $catalog = "{$webroot}{$linkDir}";
        if (!FileHelper::createDirectory($catalog)) return false;
        $insert = [];
        foreach ($this->photos as $photo) {
            do {
                $fileName = \Yii::$app->security->generateRandomString(12) . ".{$photo->extension}";
                $fullPath = "{$catalog}/{$fileName}";
            } while (file_exists($fullPath));
            if ($photo->saveAs($fullPath)) {
                $insert[] = [$pid, "{$linkDir}/{$fileName}"];
            }
        }
        if (!empty($insert)) {
            \Yii::$app->db->createCommand()
                ->batchInsert(ProductMediaPhoto::tableName(), ['product_id', 'link'], $insert)
                ->execute();
        }

        return true;
    }
}