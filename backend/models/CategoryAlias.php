<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 09.11.2017
 * Time: 12:06
 */

namespace backend\models;


use yii\db\ActiveRecord;
use yiimodules\categories\models\Categories;

class CategoryAlias extends ActiveRecord {
    public static function tableName() {
        return '{{%ymd_categories_aliases}}';
    }

    public function rules(){
        return [
            ['name', 'trim'],
            [['category_id', 'name'], 'required'],

            ['category_id', 'integer'],
            ['category_id', 'exist', 'targetClass' => Categories::className(), 'targetAttribute' => 'id'],
            ['name', 'string', 'max' => 100],
        ];
    }

}