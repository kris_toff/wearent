<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 12.04.2017
 * Time: 23:04
 */

namespace backend\models;


use common\models\Currency;
use common\models\Product;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class ProductAdvertisement extends ActiveRecord {
    public $date_start;
    public $date_stop;

    public static function tableName() {
        return '{{%product_advt}}';
    }

    public function afterFind() {
        $this->date_start = date('Y-m-d', $this->start_sell);
        $this->date_stop  = date('Y-m-d', $this->stop_sell);

        parent::afterFind();
    }


    public function attributeLabels() {
        return [
            'click_cost'  => \Yii::t('app', 'Цена за клик'),
            'currency_id' => \Yii::t('app', 'Валюта'),
            'start_sell'  => \Yii::t('app', 'Начало кампании'),
            'stop_sell'   => \Yii::t('app', 'Конец кампании'),
            'budget'      => \Yii::t('app', 'Баланс'),
            'target_area' => \Yii::t('app', 'Целевые города'),
        ];
    }

    public function rules() {
        return [
            [
                ['product_id', 'click_cost', 'budget', 'start_sell',],
                'required',
                'when' => function($model) { return (bool)$model->active; },
            ],
            ['product_id', 'integer', 'min' => 1],
            [
                'product_id',
                'exist',
                'targetClass'     => Product::className(),
                'targetAttribute' => 'id',
                'filter'          => ['is_deleted' => 0],
            ],
            ['click_cost', 'number', 'min' => 0],
            ['click_cost', 'compare', 'compareAttribute' => 'budget', 'type' => 'number', 'operator' => '<'],
            ['currency_id', 'integer', 'min' => 1],
            [
                'currency_id',
                'exist',
                'targetClass'     => Currency::className(),
                'targetAttribute' => 'id',
                'filter'          => ['is_payment' => 1],
            ],
            ['date_start', 'date', 'timestampAttribute' => 'start_sell'],
            ['date_stop', 'date', 'timestampAttribute' => 'stop_sell'],
            [['start_sell', 'stop_sell'], 'integer'],
            ['target_area', 'string'],
            ['budget', 'number', 'min' => 0],
            ['active', 'default', 'value' => 1],
            ['active', 'boolean'],
        ];
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getCurrency() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    public function getProduct(){
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}