<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 31.08.2017
 * Time: 23:35
 */

namespace backend\models;


use common\models\Currency;
use yii\base\ErrorException;
use yii\base\InvalidParamException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class CurrencyExchange
 * @package backend\models
 *
 * Для конвертации валют использован сервис https://currencylayer.com/documentation
 */
class CurrencyExchange extends ActiveRecord {
    protected static $resource = 'http://apilayer.net/api/';
    protected static $endpoint_live = 'live';
    protected static $access_key    = 'ea26631dbdd6d63ef207e60410dbe084';

    public static function retrieve() {
        $ac = Currency::find()->select('id,code')->all();

// Initialize CURL:
        if (empty($ac)) {
            throw new InvalidParamException("Must be at least 1 currency in DB.");
        }
        $cmap = ArrayHelper::map($ac, 'code', 'id');

        $params = [
            'access_key' => self::$access_key,
            'currencies' => implode(',', ArrayHelper::getColumn($ac, 'code')),
        ];
        array_walk($params, function(&$v, $k) {
            $v = "$k=$v";
        });

        $url = self::$resource;
        $url .= self::$endpoint_live;
        $url .= '?';
        $url .= implode('&', $params);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Store the data:
        $data = Json::decode(curl_exec($ch));
        curl_close($ch);

        if (!(int)$data['success']) {
            throw new ErrorException("Retrieve currency live quotes failed.");
        }

        // Save in DB
        $db = \Yii::$app->db;
        $db->createCommand()->truncateTable(CurrencyExchange::tableName())->execute();

        $l = [];
        foreach ($data['quotes'] as $cpair => $v) {
            $l[] = [
                $cmap[ mb_substr($cpair, 0, 3, 'UTF-8') ],
                $cmap[ mb_substr($cpair, 3, 3, 'UTF-8') ],
                $v,
                time(),
            ];
        }

        $db->createCommand()->batchInsert(CurrencyExchange::tableName(), [
            'currency_withdraw',
            'currency_receive',
            'value',
            'updated_at',
        ], $l)->execute();

        return $data;
    }

    public static function tableName() {
        return '{{%currency_exchange}}';
    }

    public function getCurrencyIn() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_withdraw']);
    }

    public function getCurrencyOut() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_receive']);
    }
}