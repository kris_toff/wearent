<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.10.2017
 * Time: 11:00
 */

namespace backend\modules\moderation\controllers;


use backend\modules\moderation\models\FilterFormSearch;
use frontend\models\UserDocument;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

class DocumentsController extends Controller {

    public function actionIndex() {
        $model = new FilterFormSearch();

        $dataProvider = $model->search(\Yii::$app->request->get());

        return $this->render('index', [
            'model'        => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSet() {
        $id    = \Yii::$app->request->post('id');
        $model = UserDocument::findOne($id);
        if (is_null($model)) {
            throw new BadRequestHttpException("No entry");
        }
        $model->scenario = UserDocument::SCENARIO_MODERATION;

        if (!$model->load(\Yii::$app->request->post(), '')) {
            throw new BadRequestHttpException("No load");
        } elseif (!$model->validate()) {
            throw new BadRequestHttpException("No validate");
        } elseif (!$model->save(false)) {
            throw new BadRequestHttpException("No save");
        }
    }

    public function actionOpen($id) {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = UserDocument::findOne($id);
        if (is_null($model)) {
            throw new BadRequestHttpException("Not found.");
        }

        $model->scenario = UserDocument::SCENARIO_MODERATION;
        $model->status   = 0;

        if (!$model->validate()) {
            \Yii::$app->response->statusCode = 400;
            \Yii::$app->response->data       = $model->errors;
            \Yii::$app->response->send();
        } elseif (!$model->save(false)) {
            throw new BadRequestHttpException("Not save");
        }

        return [];
    }

    public function actionRemove() {
        $id    = \Yii::$app->request->getBodyParam('id');
        $model = UserDocument::findOne($id);
        if (is_null($model)) {
            throw new BadRequestHttpException("Not found.");
        }

        $model->softDelete();
    }
}