<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.10.2017
 * Time: 11:46
 */

namespace backend\modules\moderation\controllers;


use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class FileController extends Controller {
    public function actionGet($file) {
        $file = ltrim($file, '/');

        $filePath = \Yii::getAlias("@documents/{$file}");
        if (!file_exists($filePath)) {
            throw new NotFoundHttpException("Image not found.");
        }

        return \Yii::$app->response->sendFile($filePath, null, ['inline' => true]);
    }
}
