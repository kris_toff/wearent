<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.10.2017
 * Time: 20:05
 */

use yii\bootstrap\Html;

/** @var \frontend\models\UserDocument $model */
/** @var integer $key */
/** @var integer $integer */

?>

<div class="row">
    <div class="col-sm-12">
        <div class="media">
            <div class="media-left col-sm-3 col-xs-4">
                <div class="square">
                    <a href="#" class="img-circle">
                        <div class="img-circle">
                            <?= Html::img("/admin/moderation/file/{$model->path}",
                                ['class' => 'img-responsive']) ?>

                            <div class="text-center">
                                <span class="glyphicon glyphicon-zoom-in"></span>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="text-center"><?= \Yii::$app->formatter->asDatetime($model->created_at) ?></div>
            </div>
            <div class="media-body">
                <div class="row">
                    <!-- profile -->

                    <div class="col-sm-8">
                        <div>
                            <?php if (empty($model->userProfile->fullName)): ?>
                                <em>no name</em>
                            <?php else: ?>
                                <strong><?= Html::encode($model->userProfile->fullName) ?></strong>
                            <?php endif; ?>
                        </div>
                        <p>Type: <?= $model->type ?></p>
                    </div>
                    <div class="col-sm-4">
                        <?= Html::a('Profile', "/profile/id{$model->user_id}",
                            ['target' => '_blank', 'class' => 'btn btn-default btn-block', 'data-pjax' => 0]) ?>
                    </div>
                </div>
                <div>
                    <!-- actions -->
                    <?php if (is_null($model->status)): ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <a class="open-paper">Принять в обработку</a>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="row actions<?= is_null($model->status) ? ' hidden' : '' ?>">
                        <div class="col-sm-8 block-label">
                            <?= Html::radioList('n' . $model->id, $model->status,
                                ['1' => 'Принять', '-1' => 'Отклонить']) ?>

                            <?= Html::textarea('u', $model->cause,
                                ['class' => 'hidden form-control', 'placeholder' => 'Почему?']) ?>
                        </div>
                        <div class="col-sm-4">
                            <?= Html::button('Подтвердить', ['class' => 'btn btn-success btn-block submit']) ?>
                            <?= Html::button('Удалить',
                                ['class' => 'btn btn-danger btn-block hidden', 'pga-remove' => '']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

