<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.10.2017
 * Time: 13:39
 */

namespace backend\modules\moderation\assets;


use yii\web\AssetBundle;

class ModerationAsset extends AssetBundle {

    public $sourcePath = '@backend/modules/moderation/web';
    public $css        = [
        'css/moderation.css',
    ];

    public $depends = [
        '\frontend\assets\FullScreenGalerryAsset',
        'yii\web\JqueryAsset',
    ];

    public $js = [
        'js/moderation.js',
    ];
}