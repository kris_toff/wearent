(function($) {
    var pswpElement = $(".pswp").get(0);

    // click on photo
    $("#profile-list-pjax")
        .on("click", ".media-left a", function(event) {
            event.preventDefault();
            var element = $(event.currentTarget).find("img");

            // build items array
            var items = [
                {
                    src: element.attr("src"),
                    w: element.prop("naturalWidth"),
                    h: element.prop("naturalHeight")
                }
            ];

            // define options (if needed)
            var options = {
                // optionName: 'option value'
                // for example:
                index: 0 // start at first slide
            };

            // Initializes and opens PhotoSwipe
            var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
            gallery.init();
        })
        .on("change", ".media-body [type='radio']", function(event) {
            var b = $(event.target).closest(".media-body");
            var t = b.find("textarea");

            switch (b.find("[type='radio']:checked").val()) {
                case "-1":
                    t.removeClass("hidden");
                    break;
                case "0":
                case "1":
                    t.addClass("hidden");
                    break;
            }
        })
        .on("click", ".media-body .open-paper", function(event) {
            var l = $(event.target).closest(".list-group-item");

            $.ajax("/admin/moderation/documents/open", {
                data: {id: l.data("key")}
            })
                .done(function(response) {
                    console.log('done');
                    l
                        .find(".actions")
                        .removeClass("hidden")
                        .prev("div")
                        .remove();
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    console.log('fail', jqXHR, textStatus, errorThrown);
                });
        })
        .on("click", ".list-group-item [pga-remove]", function(event) {
            event.preventDefault();
            var l = $(event.target).closest(".list-group-item");

            $.ajax("/admin/moderation/documents/remove", {
                method: "delete",
                data: {id: l.data("key")}
            })
                .done(function() {
                    $.pjax.reload("#profile-list-pjax", {
                        replace: true,
                        scrollTo: false,
                        url: window.location.href
                    });
                });
        })
        .on("click", ".list-group-item .submit", function(event) {
            event.preventDefault();

            var list_item = $(event.target).closest(".list-group-item");

            var data = {};
            data.id = list_item.data("key");
            data.status = list_item.find("[type='radio']:checked").val();
            data.cause = list_item.find("textarea").val();

            $.post({
                url: "/admin/moderation/documents/set",
                data: data
            })
                .done(function() {

                })
                .fail(function() {

                });
        });


    $("#profile-list-pjax").find(".media-body [type='radio']:checked").trigger("change");

    $("#common-form").on("change", "input[type='checkbox']", function(event) {
        $($(event.target).prop("form")).trigger("submit");
    });

    $("#profile-list-pjax")
        .on("pjax:error pjax:timeout", function(event) {
            event.preventDefault();
        })
        .on("pjax:send", function(event) {
            $("#profile-list-pjax").prepend('<img src="/admin/img/30.gif" />');
        })
        .on("pjax:complete", function(event) {
            $("#profile-list-pjax").children("img").remove();

            $("#profile-list-pjax").find(".media-body [type='radio']:checked").trigger("change");
        });

    $(document)
        .on("keydown", function(event) {
            if (event.keyCode === 17) {
                $("#profile-list-pjax").find(".list-group-item [pga-remove]").removeClass("hidden");
            }
        })
        .on("keyup", function(event) {
            if (event.keyCode === 17) {
                $("#profile-list-pjax").find(".list-group-item [pga-remove]").addClass("hidden");
            }
        });

}(window.jQuery));