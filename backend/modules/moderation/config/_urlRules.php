<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 07.10.2017
 * Time: 1:22
 */

return [
    // image links
    'moderation/file/<file:[\w\-.\/]+>' => 'moderation/file/get',
];