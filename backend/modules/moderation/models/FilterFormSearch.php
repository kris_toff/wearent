<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.10.2017
 * Time: 12:27
 */

namespace backend\modules\moderation\models;


use frontend\models\UserDocument;
use yii\data\ActiveDataProvider;

class FilterFormSearch extends FilterForm {

    public function search($params) {
        $query = UserDocument::find()
            ->with('userProfile')
            ->where(['is_deleted' => 0]);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => [
                'attributes'      => [
                    'common' => [
                        'asc'     => [ 'status' => SORT_DESC,  'created_at' => SORT_ASC,'user_id' => SORT_ASC],
                        'desc'    => [ 'status' => SORT_DESC, 'created_at' => SORT_DESC,'user_id' => SORT_ASC],
                        'default' => SORT_DESC,
                    ],
                ],
                'defaultOrder'    => [
                    'common' => SORT_DESC,
                ],
                'enableMultiSort' => true,
            ],
            'pagination' => [
                'defaultPageSize' => 20,
                'forcePageParam'  => false,
            ],
            'key'        => 'id',
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $c = array_filter($this->attributes);
        $c = array_diff_key($c, array_flip(['isNew']));
        $c = array_intersect_key(['isReject' => -1, 'isActual' => 1, 'isProcessing' => 0], $c);
        $c = [['status' => $c]];

        if ($this->isNew) {
            array_unshift($c, '[[status]] IS NULL');
        }

        array_unshift($c, 'or');
        $query->andFilterWhere($c);

//print $query->createCommand()->rawSql;exit;
        return $dataProvider;
    }
}