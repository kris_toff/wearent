<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.10.2017
 * Time: 12:27
 */

namespace backend\modules\moderation\models;


use yii\base\Model;

class FilterForm extends Model {
    public $isNew        = false;
    public $isReject     = false;
    public $isActual     = false;
    public $isProcessing = false;

    public function rules() {
        return [
            [['isNew', 'isReject', 'isActual', 'isProcessing'], 'boolean'],
            [['isNew', 'isReject', 'isActual', 'isProcessing'], 'default', 'value' => null],
        ];
    }
}