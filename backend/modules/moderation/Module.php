<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 05.10.2017
 * Time: 11:03
 */

namespace backend\modules\moderation;


use yii\base\BootstrapInterface;

class Module extends \yii\base\Module implements BootstrapInterface {
    public function bootstrap($app) {
        $app->getUrlManager()->addRules(require __DIR__ . '/config/_urlRules.php', false);
    }
}
