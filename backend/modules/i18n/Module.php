<?php

namespace backend\modules\i18n;

use backend\modules\i18n\models\I18nSourceMessage;
use yii\db\Query;
use yii\i18n\MissingTranslationEvent;

class Module extends \yii\base\Module {
    public $controllerNamespace = 'backend\modules\i18n\controllers';

    protected $_allCategories;

    /**
     * @param \yii\i18n\MissingTranslationEvent $event
     */
    public static function missingTranslation(MissingTranslationEvent $event) {
        // do something with missing translation
        if (in_array($event->category, ['app', 'eav', 'frontend', 'common', 'backend']) || empty($event->message)) {
            return false;
        }

        $tb = (new Query())->from('{{%i18n_missing_translate}}')->where([
            'category' => $event->category,
            'message'  => $event->message,
        ])->limit(1)->one();

        if (is_null($tb) || $tb === false) {
            if (!I18nSourceMessage::find()
                ->where(['category' => $event->category, 'message' => $event->message])
                ->exists()
            ) {
                $user = \Yii::$app->user->identity;
                \Yii::$app->db->createCommand()
                    ->insert('{{%i18n_missing_translate}}',
                        [
                            'category'   => $event->category,
                            'message'    => $event->message,
                            'link'       => \Yii::$app->request->url,
                            'action'     => \Yii::$app->controller->id . '/' . \Yii::$app->controller->action->id,
                            'creator_id' => is_null($user) ? null : $user->id,
                            'ip'         => \Yii::$app->request->userIP,
                            'created_at' => time(),
                            'updated_at' => time(),
                        ])
                    ->execute();
            }

            // добавим дурацкий перевод на страницу для видимости недостающих переводов
            $event->translatedMessage = '__' . $event->message . '__';
        } elseif ($tb['is_ignore']) {
            return false;
        } else {
            // добавим дурацкий перевод на страницу для видимости недостающих переводов
            $event->translatedMessage = '__' . $event->message . '__';
        }
    }

//        print 'missing: '.$event->category.' for word: ' . $event->message . '(' .$event->language .')' ;

    public function init() {
        parent::init();
    }

    protected function getCategories() {
        $c = $this->_allCategories;

        if (empty($c)) {
            $c                    = (new Query())->select('category')
                ->from('{{%i18n_source_message}}')
                ->distinct()
                ->column();
            $this->_allCategories = $c;
        }

        return $c;
    }
}
