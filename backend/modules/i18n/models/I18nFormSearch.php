<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 01.08.2017
 * Time: 16:09
 */

namespace backend\modules\i18n\models;

use common\models\Language;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;


class I18nFormSearch extends I18nForm {
    public $onlyOmitted;
    public $onlyFilled;

    public $translateWord;
    public $comment;

    public $onlyMissing = false;

    public function rules() {
        return [
            ['category', 'in', 'range' => $this->_allowedCategory],
            [['originalText', 'translateWord', 'comment'], 'string', 'min' => 2, 'max' => 255],
            ['sync_id', 'string'],
            ['onlyMissing', 'boolean'],
        ];
    }

    public function search($params) {
//print_r($params);exit;
        $query = I18nSourceMessage::find()
            ->select('ism.*,im.language,im.translation')
            ->from(['ism' => I18nSourceMessage::tableName()])
            ->joinWith([
                'i18nMessages im' => function($query) {
                    /** @var ActiveQuery $query */
                    $query->indexBy('language');
                },
            ])
            ->groupBy('ism.id');

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'forcePageParam'  => false,
                'defaultPageSize' => 50,
                'pageSizeLimit'   => [10, 95],
                'route'           => '/i18n/translation/index',
            ],
            'sort'       => [
                'attributes'   => [
                    'dia' => [
                        'asc'     => ['sync_seq' => SORT_ASC, 'id' => SORT_ASC],
                        'desc'    => ['sync_seq' => SORT_DESC, 'id' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                ],
                'defaultOrder' => [
                    'dia' => SORT_ASC,
                ],
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'translation', $this->translateWord]);
        $query->andFilterWhere(['like', 'message', $this->originalText]);
        $query->andFilterWhere(['like', 'comment', $this->comment]);
        $query->andFilterWhere(['like', 'sync_id', $this->sync_id]);

        $query->andFilterWhere(['category' => $this->category]);

        if ($this->onlyMissing) {
            $countLanguages = \Yii::$app->languages->count;//Language::getCount();
            $query->addSelect(['ctr' => 'COUNT({{ism}}.[[id]])'])->having(['<', 'ctr', $countLanguages,]);
        }

//print $query->createCommand()->rawSql;exit;
        return $dataProvider;
    }
}