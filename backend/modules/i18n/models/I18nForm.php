<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 29.07.2017
 * Time: 15:54
 */

namespace backend\modules\i18n\models;


use common\models\Language;
use yii\base\ErrorException;
use yii\base\InvalidParamException;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\i18n\DbMessageSource;

class I18nForm extends Model {
    const SCENARIO_INSERT          = 'create';
    const SCENARIO_UPDATE          = 'update';
    const SCENARIO_UPDATE_CATEGORY = 'update_category';

    public $id;
    public $category;
    public $isNewCategory;
    public $newCategory;
    public $originalText;
    public $translations;
    public $link;
    public $comment;
    public $sync_id;

    protected $_allowedCategory;

    public function init() {
        parent::init();

        $this->translations = array_fill_keys(\Yii::$app->languages->codes, '');

        // список категорий
        $translations     = array_filter(\Yii::$app->i18n->translations, function($val) {
            return (is_array($val)
                    && !strcasecmp(ArrayHelper::getValue($val, 'class', ''), DbMessageSource::className()))
                || (is_object($val) && !strcasecmp(get_class($val), DbMessageSource::className()));
        });
        $translations_map = ArrayHelper::map($translations, 'sourceMessageTable', 'messageTable');

        $categories             = (new Query())->select('category')
            ->from(array_keys($translations_map))
            ->distinct()
            ->column();
        $this->_allowedCategory = array_combine($categories, $categories);
    }

    public function formName() {
        return 'I18nForm';
    }


    public function rules() {
        return [
            [['originalText', 'category', 'newCategory', 'link', 'comment'], 'trim'],
            [['originalText'], 'required'],
            [['originalText'], 'string', 'min' => 2, 'max' => 255],
            [
                'originalText',
                'unique',
                'targetClass'     => I18nSourceMessage::className(),
                'targetAttribute' => ['category', 'originalText' => 'message'],
                'when'            => function($model) {
                    return empty($model->id);
                },
            ],

            [
                'category',
                'required',
                'when'       => function($model) {
                    return !(bool)$model->isNewCategory;
                },
                'whenClient' => <<<WHEN
function(){
    var ck = $("#i18nform-isnewcategory");
    if (!ck.is("input[type='checkbox']")){
        ck = ck.find("input[type='checkbox']");
    }
    
    return !ck.prop("checked");
}
WHEN
                ,
            ],
            ['category', 'in', 'range' => $this->_allowedCategory],

            ['isNewCategory', 'boolean'],
            ['isNewCategory', 'default', 'value' => false],

            [
                'newCategory',
                'required',
                'when'       => function($model) {
                    return (bool)$model->isNewCategory;
                },
                'whenClient' => <<<WHEN
function(attribute, value) { 
    var ck = $("#i18nform-isnewcategory");
    if (!ck.is("input[type='checkbox']")){
        ck = ck.find("input[type='checkbox']");
    }
    
    return ck.prop("checked");
}
WHEN
                ,
            ],
            ['newCategory', 'string', 'min' => 2, 'max' => 255],
//            [
//                'newCategory',
//                'in',
//                'range'   => $this->_allowedCategory,
//                'not'     => true,
//                'message' => \Yii::t('product-edit', 'This category already exists.'),
//            ],

            ['id', 'integer', 'min' => 1],
            ['id', 'exist', 'targetClass' => I18nSourceMessage::className()],

            [['link', 'comment'], 'string'],
            ['translations', 'safe'],

            ['sync_id', 'string', 'max' => 25],
        ];
    }

    public function save($validation = true) {
        $isUpdate = !(bool)$this->isNewCategory && !empty($this->id) && (int)$this->id > 0;

        $this->scenario = $isUpdate ? self::SCENARIO_UPDATE : self::SCENARIO_INSERT;
        if ($validation && !$this->validate()) {
//            print_r($this->errors);exit;
            throw new InvalidParamException("validate failed");
        }

        if ($isUpdate) {
            return $this->update();
        }

        $model             = new I18nSourceMessage();
        $model->attributes = [
            'category'     => $this->isNewCategory ? $this->newCategory : $this->category,
            'message'      => $this->originalText,
            'link_to_page' => $this->link,
            'comment'      => $this->comment,
            'sync_id'      => $this->sync_id,
        ];
        if (!$model->save()) {
            throw new ErrorException("no saved");
        }
        $id       = $model->id;
        $this->id = $id;

        foreach ($this->translations as $langCode => $text) {
            if (empty($text)) {
                continue;
            }

            $model             = new I18nMessage();
            $model->attributes = [
                'id'          => $id,
                'language'    => $langCode,
                'translation' => $text,
            ];

            if (!$model->save()) {
                // do some
            }
        }

        return true;
    }

    protected function update() {
        $model = I18nSourceMessage::findOne(['id' => $this->id]);
        if (is_null($model)) {
            return false;
        }

        $model->attributes = [
            'message'      => $this->originalText,
            'link_to_page' => $this->link,
            'comment'      => $this->comment,
            'sync_id'      => $this->sync_id,
        ];
        $model->update();

        foreach ($this->translations as $langCode => $text) {
            $model = I18nMessage::findOne(['id' => $this->id, 'language' => $langCode]);
            if (is_null($model)) {
                if (empty($text)) {
                    continue;
                }

                $model             = new I18nMessage();
                $model->attributes = [
                    'id'       => $this->id,
                    'language' => $langCode,
                ];
            } elseif (empty($text)) {
                $model->delete();
                continue;
            }
            $model->translation = $text;
            $model->save();
        }

        return true;
    }

    public function scenarios() {
        $scenarios                                   = parent::scenarios();
        $scenarios[ self::SCENARIO_INSERT ]          = [
            'category',
            'isNewCategory',
            'newCategory',
            'originalText',
            'translations',
            'link',
            'comment',
            'sync_id',
        ];
        $scenarios[ self::SCENARIO_UPDATE ]          = [
            'id',
            'originalText',
            'translations',
            'link',
            'comment',
            'sync_id',
        ];
        $scenarios[ self::SCENARIO_UPDATE_CATEGORY ] = ['id', 'category'];

        return $scenarios;
    }

    public function getAllowedCategory() {
        return $this->_allowedCategory;
    }

}