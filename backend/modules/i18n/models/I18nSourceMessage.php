<?php

namespace backend\modules\i18n\models;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%i18n_source_message}}".
 *
 * @property integer       $id
 * @property string        $category
 * @property string        $message
 *
 * @property I18nMessage[] $i18nMessages
 */
class I18nSourceMessage extends ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%i18n_source_message}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['category', 'message', 'link_to_page', 'comment'], 'trim'],
            [['message', 'comment'], 'string', 'max' => 1212],
            [['category'], 'string', 'max' => 32],
            ['link_to_page', 'string'],
            ['sync_id', 'string', 'max' => 25],
            ['sync_seq', 'integer'],
        ];
    }

    public function behaviors() {
        return [
            [
                'class'      => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => 'sync_seq',
                    self::EVENT_BEFORE_UPDATE => 'sync_seq',
                ],
                'value'      => function($event) {
                    $code = $event->sender->sync_id;
                    if (empty($code)) {
                        return null;
                    }

                    $acode = preg_split('/\D+/', $code);
                    $acode = array_filter($acode);
                    if (empty($acode)) {
                        return null;
                    }

                    array_walk($acode, function(&$v, $k) {
                        $v = intval($v) * (int)str_pad('1', 7 - 2 * $k + 1, '0');
                    });

                    return array_sum($acode);
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'           => Yii::t('backend', 'ID'),
            'category'     => Yii::t('backend', 'Category'),
            'message'      => Yii::t('backend', 'Message'),
            'link_to_page' => Yii::t('backend', 'Link to page'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getI18nMessages() {
        return $this->hasMany(I18nMessage::className(), ['id' => 'id']);
    }

    public function afterDelete() {
        parent::afterDelete();

        I18nMessage::deleteAll(['id' => $this->id]);
    }

}
