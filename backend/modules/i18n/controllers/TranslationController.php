<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 26.07.2017
 * Time: 17:15
 */

namespace backend\modules\i18n\controllers;


use backend\modules\i18n\models\I18nForm;
use backend\modules\i18n\models\I18nFormSearch;
use backend\modules\i18n\models\I18nMessage;
use backend\modules\i18n\models\I18nSourceMessage;
use common\models\Language;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

class TranslationController extends Controller {
    public $enableCsrfValidation = false;

    public function behaviors() {
        return [
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'index'           => ['get'],
                    'remove'          => ['delete'],
                    'change-category' => ['put', 'patch'],
                    'get-data'        => ['get'],
                    'create'          => ['post'],

                    'missing-list'     => ['get'],
                    'missing-list-tab' => ['post'],
                    'clear-miss'       => ['get'],
                    'remove-miss'      => ['get', 'delete'],

                    //                    'test' => ['get'],
                ],
            ],
        ];
    }

    public function actionIndex() {
//        foreach(I18nSourceMessage::find()->each() as $item){
//            $item->markAttributeDirty('sync_id');
//            $item->save();
//        }
        // form
        $model       = new I18nForm();
        $modelFilter = new I18nFormSearch();

        // провайдер слов/фраз для ListView
        $dataProvider = $modelFilter->search(\Yii::$app->request->get());

        // языки
        $languages = \Yii::$app->languages->list;

        return $this->render('index', [
            'categories'   => $model->allowedCategory,
            'dataProvider' => $dataProvider,
            'languages'    => $languages,
            'model'        => $model,
            'modelFilter'  => $modelFilter,
        ]);
    }

    public function actionRemove() {
//        \Yii::$app->response->format = Response::FORMAT_JSON;
        $request = \Yii::$app->request;
        $model   = I18nSourceMessage::findOne(['id' => $request->getBodyParam('id')]);

        if (!is_null($model)) {
            $model->delete();
        }

        return $this->render('pjax-list', [
            'dataProvider' => (new I18nFormSearch())->search($request->get()),
            'languages'    => Language::getList(),
        ]);
    }

    public function actionChangeCategory() {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $request                     = \Yii::$app->request;

        $id       = $request->getBodyParam('id');
        $category = $request->getBodyParam('category');

        $model = I18nSourceMessage::findOne(['id' => $id]);
        if (is_null($model)) {
            throw new BadRequestHttpException("No phrase with id $id.");
        }
        $model->category = $category;
        if (!$model->update()) {
            throw new Exception("The change are unaffected.");
        }

        return [
            'status'   => 'ok',
            'id'       => $id,
            'category' => $model->category,
        ];
    }

    public function actionGetData($id) {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = I18nSourceMessage::find()->with(['i18nMessages'])->where(['id' => $id])->one();
        if (is_null($model)) {
            throw new BadRequestHttpException("Not found source message with id $id");
        }

        return ArrayHelper::toArray($model, [
            I18nSourceMessage::className() => [
                'id',
                'category',
                'message',
                'trans' => 'i18nMessages',
                'link'  => 'link_to_page',
                'comment',
                'sync_id',
            ],
            I18nMessage::className()       => [
                'translation',
                'language',
            ],
        ]);
    }

    public function actionCreate() {
        $request = \Yii::$app->request;

        $model = new I18nForm();
        if ($model->load($request->post()) && $model->save()) {
            \Yii::$app->response->headers->set('ProductId', $model->id);

            return $this->render('pjax-list', [
                'dataProvider' => (new I18nFormSearch())->search($request->get()),
                'languages'    => \Yii::$app->languages->list,
            ]);
        } elseif ($model->hasErrors()) {
            throw new InvalidParamException("The form contains errors.");
        }

        throw new BadRequestHttpException("Save failed!");
    }

    public function actionMissingListTab() {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $this->renderPartial('_tab_missing', [
            'dataProvider' => $this->dataProviderMissing(),
        ]);
    }

    protected function dataProviderMissing() {
        return new ActiveDataProvider([
            'query'      => (new Query())->from('{{%i18n_missing_translate}}'),
            'key'        => 'id',
            'pagination' => [
                'defaultPageSize' => 45,
                'forcePageParam'  => false,
                'pageParam'       => 'mispage',
                'route'           => 'i18n/translation/missing-list',
            ],
            'sort'       => [
                'attributes'   => [
                    'default' => [
                        'asc'     => ['is_ignore' => SORT_ASC, 'created_at' => SORT_DESC, 'updated_at' => SORT_DESC],
                        'desc'    => ['is_ignore' => SORT_DESC, 'created_at' => SORT_DESC, 'updated_at' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                ],
                'defaultOrder' => [
                    'default' => SORT_ASC,
                ],
            ],
        ]);
    }

    public function actionMissingList() {
        $request = \Yii::$app->request;
        if (!$request->isPjax) {
            return $this->runAction('');
        }

        return $this->render('_tab_missing', [
            'dataProvider' => $this->dataProviderMissing(),
        ]);
    }

    public function actionRemoveMiss() {
        $request = \Yii::$app->request;
        $db      = \Yii::$app->db;

        if (!$request->isAjax) {
            return $this->runAction('');
        }

        $query = $db->createCommand();
        if ((bool)$request->getBodyParam('soft')) {
            $query->update('{{%i18n_missing_translate}}', ['is_ignore' => 1], ['id' => $request->getBodyParam('id')]);
        } else {
            $query->delete('{{%i18n_missing_translate}}', ['id' => $request->getBodyParam('id')]);
        }
        $query->execute();

        return $this->render('_tab_missing', [
            'dataProvider' => $this->dataProviderMissing(),
        ]);
    }

    public function actionEjectMiss() {
        $request = \Yii::$app->request;
        $db      = \Yii::$app->db;

        $db->createCommand()
            ->update('{{%i18n_missing_translate}}', ['is_ignore' => 0], ['id' => $request->getBodyParam('id')])
            ->execute();

        return $this->render('_tab_missing', [
            'dataProvider' => $this->dataProviderMissing(),
        ]);
    }

    public function actionClearMiss() {
        $request = \Yii::$app->request;
        $db      = \Yii::$app->db;

        $db->createCommand()
            ->truncateTable('{{%i18n_missing_translate}}')
            ->execute();

        return $this->render('_tab_missing', [
            'dataProvider' => $this->dataProviderMissing(),
        ]);
    }

    public function actionLanguagesTab() {
        $request = \Yii::$app->request;

        if ($request->isGet) {
            return $this->redirect('index', 307);
        }

        $dataProvider = new ActiveDataProvider([
            'query'      => Language::find(),
            'pagination' => false,
            'sort'       => false,
        ]);

        if (\Yii::$app->request->isPjax) {
            $model = new Language();

            $model->load($request->post());
            $model->save();

            return $this->render('_tab_languages', [
                'dataProvider' => $dataProvider,
            ]);
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $this->renderPartial('_tab_languages', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
