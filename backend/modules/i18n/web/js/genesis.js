/**
 * Created by Planess group on 27.07.2017.
 */

(function($) {
    "use strict";

    var listp = $("#translation-list-pjax");
    var categoryBox = $("#choose_category_box-dialog");
    var editModelBox = $("#edit_entry_box-dialog");
    var filterForm = $("#filter-form");

    var filter_select = $("#i18nform-category");

    var _closeAfterSave = false;
    var _saving = false;

    // initialize
    // dialogs
    $("[pga-box]").dialog({
        autoOpen: false,
        resizable: false,
        draggable: true,
        modal: true,
        width: "65%",
        height: "auto",
        maxHeight: 550,
        minWidth: 320,
        // maxWidth: 900,
        closeOnEscape: true,
        show: {
            effect: "drop",
            duration: 450
        },
        hide: {
            effect: "drop",
            duration: 350
        }
    })
        .on("dialogclose", function(event) {
            $(event.target).find("form").each(function(index, element) {
                element.reset();
            });
        });

    // set buttons for change category dialog
    categoryBox.dialog("option", "buttons", [
        {
            text: "Update",
            icon: "ui-icon-disk",
            click: function() {
                var c = categoryBox.find("#category-phrase").val();
                var id = categoryBox.find("#model_id").val();
                var btns = categoryBox.find(".ui-dialog-buttonset .ui-button");
                btns.button("disable");

                $.ajax("/admin/i18n/translation/change-category", {
                    method: "put",
                    timeout: 2500,
                    data: {
                        id: id,
                        category: c
                    }
                })
                    .done(function(response) {
                        if (response.status === "ok") {
                            listp.find('.list-item[data-key="' + response.id + '"] .category_name')
                                .html('<a href="/admin/i18n/translation/index?category='
                                    + response.category
                                    + '">'
                                    + response.category
                                    + '</a>');
                        }
                        categoryBox.dialog("close");
                    })
                    .fail(function() {
                        btns.button("enable");
                    });
            }
        },
        {
            text: "Close",
            icon: "ui-icon-cancel",
            click: function() {
                categoryBox.dialog("close");
            }
        }
    ]);
    editModelBox.dialog("option", "buttons", [
        {
            text: "Save",
            icon: "ui-icon-disk",
            click: function() {
                editModelBox.find("form").trigger("submit");
            }
        },
        {
            text: "Close",
            icon: "ui-icon-cancel",
            click: function() {
                editModelBox.dialog("close");
            }
        },
        {
            text: "Save & Close",
            icon: "ui-icon-disk",
            click: function() {
                _closeAfterSave = true;
                editModelBox.find("form").trigger("submit");
            }
        }
    ])
        .dialog("option", "resizable", true)
        .on("dialogopen", function(event) {
            editModelBox.siblings(".ui-dialog-buttonpane").children(".ui-widget").remove();
        });

    // select
    $("[pga-uiselect]").selectmenu({
        width: "auto"
    });
    $("#amount-item-on-page").on("selectmenuchange", function(event, ui) {
        var location = window.location;

        var link = location.protocol + "//" + location.hostname + location.pathname;
        var gets = _.split(decodeURIComponent(_.trim(location.search, "&?")), "&");
        gets = _.pull(gets, "");
        // console.log(location.search);
// console.log('gets',gets);
        var data = {};
        _.forEach(gets, function(v, i) {
            var s = _.split(v, "=");
            if (_.isUndefined(s[1]) || _.isNull(s[1]) || (_.isString(s[1]) && !s[1].length)) {
                return;
            }
            data[s[0]] = s[1];
        });
        data["per-page"] = ui.item.value;
        // console.log(link,data);

        $.pjax.reload("#translation-list-pjax", {
            url: link,
            data: data
        });
    });

    // buttons
    $("[pga-uibutton]").button({
        iconPosition: "beginning"
    });
    categoryBox.find("ui-button").button();

    // checkbox and radiobox
    // $("input[type='checkbox'],input[type='radio']").checkboxradio();

    // tolltips
    initTooltips();


    // update list for PJAX
    listp
        .on("pjax:start", function() {
            // $("html,body").animate({scrollTop: 10}, 120);
            if (editModelBox.dialog("isOpen")) {
                editModelBox.siblings(".ui-dialog-buttonpane").children(".ui-widget").remove();
            }
        })
        .on("pjax:send", function() {
            listp.find(".summary")
                .after('<div class="text-center updating_content"><i class="fa fa-refresh fa-spin"></i><div class="updating_text">Updating list...</div></div>');
        })
        .on("pjax:timeout", function(event) {
            event.preventDefault();

            listp.html('<div class="text-center updating_content"><div><i class="fa fa-hourglass-2 fa-spin"></i></div><div class="updating_text text-error"><div>Response timeout. Probably the connection to the internet is too slow.</div><div>If waiting too long refresh the page.</div></div></div>');
        })
        .on("pjax:error", function(event) {
            event.preventDefault();

            listp.html('<div class="text-center updating_content"><i class="fa fa-blind"></i><div class="updating_text text-error">Update failed. Please refresh page by pressing the button F5.</div></div>');
        })

        .on("pjax:success", function(event, data, status, xhr) {
            if (editModelBox.dialog("isOpen")) {
                // предполагаем сохраняли форму
                editModelBox.siblings(".ui-dialog-buttonpane")
                    .append('<div class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="padding: 0.7em;"><p><span class="ui-icon ui-icon-info" style="float: left;margin-right: .3em;"></span><strong>Great!</strong> Form was processed successfully.</p></div></div>');

                editModelBox.find("#i18nform-id").val(xhr.getResponseHeader("ProductId"));

                editModelBox.find("#i18nform-isnewcategory").bootstrapSwitch("state", false);
// console.log(data, status, xhr);
            }
        })
        .on("pjax:error pjax:timeout", function() {
            if (editModelBox.dialog("isOpen")) {
                // предполагаем сохраняли форму
                editModelBox.siblings(".ui-dialog-buttonpane")
                    .append('<div class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0.7em;"><p><span class="ui-icon ui-icon-info" style="float: left;margin-right: .3em;"></span><strong>Oppss...</strong> Form processed was failed.</p></div></div>');
            }
        })
        .on("pjax:complete", function(event, xhr, textStatus, options) {
            if (_closeAfterSave && textStatus === "success") {
                editModelBox.dialog("close");
            }
        })
        .on("pjax:end", function() {
            if (_closeAfterSave) {
                _closeAfterSave = false;
                _saving = false;
            }

            initTooltips();
        });


    // set handle for select filter list
    listp
        .on("click", ".category_name", function(event) {
            event.preventDefault();
            event.stopPropagation();
        })
        .on("click", ".category_name a", changeCategory)
        .on("click", ".category_name .btn-xs", function(event) {
            var btn = $(event.currentTarget);
            copySelection(btn.prev("a"), btn);
        });
    filter_select.selectmenu("option", {
        change: changeCategory
    });


    // handle for click at row list item
    listp
        .on("click", ".action_buttons", function(event) {
            var btn = $(event.target).closest(".btn", event.currentTarget);
            if (!btn.length) {
                return;
            }

            event.preventDefault();
            event.stopPropagation();

            var item = btn.closest(".list-item");
            var id = item.data("key");

            switch (btn.data("target")) {
                case "remove":
                    // delete item from list
                    $.pjax({
                        url: "/admin/i18n/translation/remove" + window.location.search,
                        container: "#translation-list-pjax",
                        scrollTo: false,
                        timeout: 1800,
                        push: false,
                        replace: false,
                        method: "delete",
                        data: {
                            id: id
                        }
                    });
                    break;
                case "copy":
                    // copy original phrase from row
                    copySelection(item.find(".original_text").get(0), btn);
                    break;
                case "edit":
                    // заблокируем форму до момента получения из сервера данных
                    editModelBox.find("input,textarea").prop("readonly", true);
                    editModelBox.find("#category_row").addClass("hidden");

                    // set model value
                    editModelBox.find("#i18nform-id").val(id);

                    $.ajax("/admin/i18n/translation/get-data", {
                        method: "get",
                        timeout: 2200,
                        data: {
                            id: id
                        }
                    })
                        .done(function(response) {
                            editModelBox.find("#i18nform-category").val(response.category);
                            editModelBox.find("i18nform-newcategory").val("");
                            editModelBox.find("#i18nform-originaltext").val(response.message);

                            $.each(response.trans, function(index, value) {
                                var name = "I18nForm[translations][" + value.language + "]";
                                editModelBox.find(".form-control[name='" + name + "']").val(value.translation);
                            });

                            editModelBox.find("#i18nform-link").val(response.link);
                            editModelBox.find("#i18nform-comment").val(response.comment);
                            editModelBox.find("#i18nform-sync_id").val(response.sync_id);

                            // разблокировать поля формы
                            editModelBox.find("input,textarea").prop("readonly", false);
                        })
                        .fail(function() {
                            editModelBox.dialog("close");
                        });

                    editModelBox.dialog("open");
                    break;
                case "change_category":
                    // open dialog for change phrase category
                    var phrase = _.trim(item.find(".original_text").text());
                    var category = _.trim(item.find(".category_name").text());

                    categoryBox.find("#model_id").val(id);
                    categoryBox.find(".box-title strong").text(phrase);
                    categoryBox.find("#category-phrase").val(category).selectmenu("refresh");

                    categoryBox.dialog("open");
                    break;
            }
        })
        .on("click", ".list-item-row", function(event) {
            // freeze info panel top/bottom at item row
            var row = $(event.currentTarget);
            var item = row.closest(".list-item");
            if (item.hasClass("fasten-top") || item.hasClass("fasten-bottom")) {
                item.removeClass("fasten-top fasten-bottom staple");
                return;
            }

            var info = item.children(".list-item-info");

            var windowHeight = $(window).height();
            var windowScroll = $(window).scrollTop();
            var rowTop = row.offset().top;

            var cls = "fasten-bottom";
            if (windowHeight + windowScroll - rowTop - row.outerHeight() < rowTop - windowScroll) {
                cls = "fasten-top";
            }
            item.addClass(cls).addClass("staple");
        })
    ;

    // open modal for edit phrase
    $("#add_phrases-btn").on("click", openModalTranslate);
    listp.on("click", "#add_phrases_from_empty-btn", openModalTranslate);

    $("#i18nform-isnewcategory").on("switchChange.bootstrapSwitch", function(event, state) {
        if (state) {
            editModelBox.find(".field-i18nform-category").addClass("hidden");
            editModelBox.find(".field-i18nform-newcategory").removeClass("hidden");
        } else {
            editModelBox.find(".field-i18nform-category").removeClass("hidden");
            editModelBox.find(".field-i18nform-newcategory").addClass("hidden");
        }
    });

    editModelBox.on("submit", "form", function(event) {
        event.preventDefault();

        var $form = $(event.currentTarget);

        $.pjax({
            url: "/admin/i18n/translation/create" + window.location.search,
            container: "#translation-list-pjax",
            method: "post",
            data: $form.serialize(),
            scrollTo: false,
            timeout: 1800,
            push: false,
            replace: false
        });
    });

    $(document).on("keydown", function(event) {
        if (event.which === 13 && event.ctrlKey && editModelBox.dialog("isOpen") && !_saving) {
            _closeAfterSave = true;
            _saving = true;
            editModelBox.find("form").trigger("submit");
        }
    });


    // private functions
    function openModalTranslate(event) {
        event.preventDefault();

        editModelBox.find("#category_row").removeClass("hidden");
        editModelBox.find(".field-i18nform-category").removeClass("hidden");
        editModelBox.find(".field-i18nform-newcategory").addClass("hidden");

        var category = filter_select.val();
        var selecter = editModelBox.find("#i18nform-category");
        if (category.length) {
            selecter.val(category);
        } else {
            selecter.find("option:first").prop("selected", true);
        }
        selecter.selectmenu("refresh");
        editModelBox.find("#i18nform-id").val("");

        editModelBox.dialog("open");
    }

    function changeCategory(event, data) {
        event.preventDefault();

        filterForm.trigger("submit");
    }

    function copySelection(element, btn) {
        if (element instanceof Element) {
            // do nothing
        } else if (element instanceof jQuery) {
            element = element.get(0);
        } else {
            return false;
        }

        var range = document.createRange();
        window.getSelection().removeAllRanges();
        try {
            range.selectNode(element);

            window.getSelection().addRange(range);
            document.execCommand("copy");
        } catch (e) {
            console.log("catch");
            return false;
        }
        window.getSelection().removeAllRanges();

        if (btn.hasClass("btn") || btn.hasClass("btn-xs")) {
            var icon = btn.hasClass("glyphicon") ? btn : btn.find(".glyphicon");
            icon.toggleClass("glyphicon-copy glyphicon-duplicate");
            _.delay(function() {
                icon.toggleClass("glyphicon-copy glyphicon-duplicate");
            }, 1000);
        }
    }

    function initTooltips() {
        // tooltips
        $("[pga-uititle]").tooltip({
            position: {
                using: function(position, feedback) {
                    $(this)
                        .css(position)
                        .removeClass("ui-widget-shadow");
                }
            },
            show: {
                effect: "clip",
                delay: 800,
                duration: 350
            },
            hide: {
                effect: "clip",
                duration: 200
            }
        });
    }

    var handle_btns_row ;
    $("#mani_tab-container")
        .on("click", "[pga-remove]", function(event) {
            var id = $(event.target).closest(".list-item").data("key");

            // delete item from list
            $.pjax({
                url: "/admin/i18n/translation/remove-miss" + window.location.search,
                container: "#missing_tab_pjax",
                scrollTo: false,
                timeout: 1800,
                push: false,
                replace: false,
                method: "delete",
                data: {
                    id: id,
                    soft: event.ctrlKey ? 0 : 1
                }
            });
        })
        .on("click", "[pga-eject]", function(event) {
            var id = $(event.target).closest(".list-item").data("key");

            // delete item from list
            $.pjax({
                url: "/admin/i18n/translation/eject-miss" + window.location.search,
                container: "#missing_tab_pjax",
                scrollTo: false,
                timeout: 1800,
                push: false,
                replace: false,
                method: "delete",
                data: {
                    id: id
                }
            });
        })
        .on("click", "[pga-append]", function(event) {
            var item = $(event.target).closest(".list-item");

            editModelBox.find("#category_row").removeClass("hidden");

            var category = _.trim(item.find(".category_name").text());

            var selecter = editModelBox.find("#i18nform-category");
            var cor_sel = selecter.find("option[value='" + category + "']");
            editModelBox.find("#i18nform-isnewcategory").bootstrapSwitch("state", !cor_sel.length);

            if (cor_sel.length) {
                cor_sel.prop("selected", true);
                selecter.selectmenu("refresh");
            } else {
                editModelBox.find("#i18nform-newcategory").val(category);
            }
            editModelBox.find("#i18nform-originaltext").val(_.trim(item.find(".original_text").text()));

            editModelBox.find("#i18nform-id").val("");

            editModelBox.dialog("open");

            $(document).on("pjax:complete", removeMiss);
            editModelBox.one("dialogclose", function() {
                $(document).off("pjax:complete", removeMiss);
            });

            function removeMiss(event, xhr, textStatus) {
                $(document).off("pjax:complete", removeMiss);

                if (textStatus !== "success") {
                    return;
                }

                $.pjax.reload("#missing_tab_pjax", {
                    url: "/admin/i18n/translation/remove-miss" + window.location.search,
                    data: {
                        id: item.data("key")
                    },
                    replace: false,
                    push: false,
                    timeout: 2000,
                    method: "delete"
                });
            }
        })
        .on("click", "[pga-clear-miss]", function(event) {
            if (event.ctrlKey) {
                $.pjax.reload("#missing_tab_pjax", {
                    url: "/admin/i18n/translation/clear-miss" + window.location.search,
                    replace: false,
                    push: false,
                    timeout: 2000
                });
            } else {
                $.notify({
                    title: "Не получилось.",
                    message: "Кнопка не подействовала, надо удерживать кнопку <i>Ctrl</i> для получения желаемого."
                }, {
                    type: "warning",
                    animate: {
                        enter: "animated fadeInDown",
                        exit: "animated fadeOutUp"
                    },
                    showProgressbar: false,
                    delay: 2000,
                    timer: 1000,
                    allow_dismiss: false
                });
            }
        })
        .on("click", "[pga-update]", function() {
            $.pjax.reload("#missing_tab_pjax", {
                url: "/admin/i18n/translation/missing-list" + window.location.search,
                replace: false,
                push: false,
                timeout: 2000
            });
        })
    ;

    $(document)
        .on("pjax:start", "#missing_tab_pjax", function() {
            if (!handle_btns_row){
                handle_btns_row = $("#missing_tab_pjax").closest(".tab-pane").find(".handle-btn");
            }
            handle_btns_row.find(".btn").prop("disabled", true);
        })
        .on("pjax:end", "#missing_tab_pjax", function() {
            handle_btns_row.find(".btn").prop("disabled", false);
        })
        .on("pjax:error pjax:timeout", "#missing_tab_pjax", function(event) {
            event.preventDefault();

            $.notify({
                message: "Обновление неудачное. Попробуйте ещё раз или обновите страницу, а вдруг поможет."
            }, {
                type: "danger",
                delay: 2500,
                timer: 1000,
                animate: {
                    enter: "animated fadeInDown",
                    exit: "animated fadeOutUp"
                }
            });
        });
}(window.jQuery));
