<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 27.07.2017
 * Time: 14:24
 */

namespace backend\modules\i18n\assets;


use yii\web\AssetBundle;

class SAsset extends AssetBundle {
    public $sourcePath = '@backend/modules/i18n/web';

    public $js = [
        'js/genesis.js',
    ];
    public $css = [
        'css/default.css',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
        'common\assets\LodashAsset',
    ];

}