<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 27.07.2017
 * Time: 11:41
 */

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Alert;

/** @var \yii\web\View $this */
/** @var array $model */
/** @var integer $key the key value associated with the data item */
/** @var integer $index the zero-based index of the data item in the items array returned by $dataProvider */

$options = ['class' => 'list-item-row'];
?>

<?= Html::beginTag('div', $options) ?>

<div class="text_row_title">
    <?php if ($model['is_ignore']): ?>
        <span class="pull-right glyphicon glyphicon-log-out btn" pga-eject></span>
    <?php else: ?>
        <span class="pull-right glyphicon glyphicon-remove btn" pga-remove></span>
        <span class="pull-right glyphicon glyphicon-piggy-bank btn" pga-append></span>
    <?php endif; ?>

    <div class="h4">
        <span class="text-muted">Word:</span>
        <span class="original_text"><?= Html::encode($model['message']) ?></span>
    </div>
    <div>
        <span class="text-muted">Category:</span>
        <span class="category_name"><?= Html::encode($model['category']) ?></span>
    </div>
    <div>
        <span class="text-muted">Url:</span>
        <?= Html::encode($model['link']) ?>
    </div>

</div>

<?= Html::endTag('div') ?>

