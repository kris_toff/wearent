<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 28.07.2017
 * Time: 22:23
 */
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \backend\modules\i18n\models\I18nForm $model */

$modelTest = new \frontend\models\ProductForm();
?>

<?php $form = ActiveForm::begin() ?>
<?= $form->field($model, 'originalText')->textInput() ?>

<div class="form-group field-<?= Html::getInputId($modelTest, 'discountPrice') ?>">
    <?= Html::activeLabel($modelTest, 'discountPrice', ['class' => 'control-label']) ?>
    <?= Html::activeTextInput($modelTest, 'discountPrice', ['class' => 'form-control', 'placeholder' => 'custom']) ?>

    <?= Html::error($modelTest, 'discountPrice', ['class' => 'help-block']) ?>
</div>
<?//= $form->field($modelTest, 'discountPrice')->textInput() ?>

<?php
if ($form->enableClientScript && $form->enableClientValidation){
    foreach(['discountPrice'] as $inputs){
        $clientOptions = getClientOptions($inputs, $modelTest, $form);
        if (!empty($clientOptions)) {
            $form->attributes[] = $clientOptions;
        }
    }
}

function getClientOptions($attr, $modelTest, $form){
    $attribute = Html::getAttributeName($attr);
    if (!in_array($attribute, $modelTest->activeAttributes(), true)) {
        return [];
    }

    $clientValidation = $form->enableClientValidation;
    $ajaxValidation = false;

    if ($clientValidation) {
        $validators = [];
        foreach ($modelTest->getActiveValidators($attribute) as $validator) {
            /* @var $validator \yii\validators\Validator */
            $js = $validator->clientValidateAttribute($modelTest, $attribute, $form->getView());
            if ($validator->enableClientValidation && $js != '') {
                if ($validator->whenClient !== null) {
                    $js = "if (({$validator->whenClient})(attribute, value)) { $js }";
                }
                $validators[] = $js;
            }
        }
    }

    if (!$ajaxValidation && (!$clientValidation || empty($validators))) {
        return [];
    }

    $options = [];

    $inputID = Html::getInputId($modelTest, $attr);
    $options['id'] = Html::getInputId($modelTest, $attr);
    $options['name'] = $attr;

    $options['container'] = ".field-$inputID";
    $options['input'] = "#$inputID";
        $options['error'] = '.' . implode('.', preg_split('/\s+/',  'help-block', -1, PREG_SPLIT_NO_EMPTY));

    $options['encodeError'] = true;
    foreach (['validateOnChange', 'validateOnBlur', 'validateOnType', 'validationDelay'] as $name) {
        $options[$name] = $form->$name;
    }

    if (!empty($validators)) {
        $options['validate'] = new \yii\web\JsExpression("function (attribute, value, messages, deferred, \$form) {" . implode('', $validators) . '}');
    }


    // only get the options that are different from the default ones (set in yii.activeForm.js)
    return array_diff_assoc($options, [
        'validateOnChange' => true,
        'validateOnBlur' => true,
        'validateOnType' => false,
        'validationDelay' => 500,
        'encodeError' => true,
        'error' => '.help-block',
        'updateAriaInvalid' => true,
    ]);
}
?>


<?= Html::submitButton('send') ?>
<?php ActiveForm::end() ?>
