<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 14.08.2017
 * Time: 13:53
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap\Alert;
use dosamigos\switchinput\SwitchBox;

/** @var string[] $languages */


?>


<?php $form = ActiveForm::begin([
    'id'      => 'filter-form',
    'action'  => ['index'],
    'method'  => 'get',
    'options' => [
        'data-pjax' => 1,
    ],
]) ?>

<div class="row">
    <div class="col-sm-8">
        <div class="form-group">

            <table>
                <tr>
                    <td style="vertical-align: middle;">
                        <?= Html::label('Category translate', null,
                            ['class' => 'control-label', 'style' => 'margin: 0 10px 15px 0;']) ?>
                    </td>
                    <td>

                        <?= $form->field($modelFilter, 'category')->dropDownList($categories, [
                            'prompt'       => 'All',
                            'pga-uiselect' => '',
                        ])->label(false) ?>

                    </td>
                    <td>--</td>
                    <td style="vertical-align: middle;"><label class="control-label" style="margin: 0 10px 15px 0;">On
                            page:</label></td>
                    <td>
                        <div class="form-group">
                            <?= Html::dropDownList('per-page', \Yii::$app->request->get('per-page', 50),
                                [11 => 11, 27 => 27, 50 => 50, 95 => 95],
                                ['id' => 'amount-item-on-page', 'pga-uiselect' => '']) ?>
                        </div>
                    </td>
                </tr>
            </table>

        </div>
    </div>
    <div class="col-sm-3">
        <div class="pull-right">
            <?= Html::button(Html::tag('span', '',
                    ['class' => 'ui-icon ui-icon-plus']) . ' add phrase',
                ['id' => 'add_phrases-btn', 'pga-uibutton' => '', 'class' => 'ui-button ui-widget ui-corner-all']) ?>
        </div>
    </div>


</div>
<div class="row">
    <div class="col-md-4 col-md-push-8" style="margin-bottom: 32px;">

        <?= $form->field($modelFilter, 'originalText')->textInput() ?>

        <?= $form->field($modelFilter, 'translateWord')->textInput() ?>

        <?= $form->field($modelFilter, 'comment')->textInput() ?>

        <?= $form->field($modelFilter, 'sync_id')->textInput()->label('ID') ?>

        <?= $form->field($modelFilter, 'onlyMissing')->widget(SwitchBox::className(), [
            'inlineLabel'   => false,
            'clientOptions' => [
                'size'    => 'normal',
                'onColor' => 'success',
                'onText'  => 'Ja Ja',
                'offText' => 'Nein',
            ],
        ]) ?>

        <?= Html::submitButton('Apply filter', ['class' => 'btn btn-primary btn-block']) ?>
    </div>
    <div class="col-md-8 col-md-pull-4">
        <!-- Here are displayed phrases -->
        <?= $this->render('pjax-list', [
            'dataProvider' => $dataProvider,
            'languages'    => $languages,
        ]) ?>
    </div>
</div>


<?php ActiveForm::end() ?>


<div id="edit_entry_box-dialog" title="Manipulate" pga-box>

    <?php
    $params = \Yii::$app->request->get();
    array_unshift($params, 'create');

    $form = ActiveForm::begin([
        'id'     => 'edit_entry-form',
        'action' => $params,
        'method' => 'post',
    ]) ?>

    <?= Html::activeHiddenInput($model, 'id') ?>

    <div class="row" id="category_row">
        <div class="col-sm-5 col-sm-push-7">
            <?= $form->field($model, 'isNewCategory')->widget(SwitchBox::className(), [
                'inlineLabel'   => false,
                'clientOptions' => [
                    'size'    => 'normal',
                    'onColor' => 'success',
                    'onText'  => 'YES',
                    'offText' => 'no',
                ],
            ])->label('New category'); ?>
        </div>
        <div class="col-sm-7 col-sm-pull-5">
            <?= $form->field($model, 'category')->dropDownList($categories, [
                'pga-uiselect' => '',
            ]) ?>
            <?= $form->field($model, 'newCategory', [
                'validateOnType'  => true,
                'validationDelay' => 350,
                'options' => [
                        'class' => 'form-group hidden',
                ]
            ])->textInput() ?>
        </div>
    </div>

    <?= $form->field($model, 'originalText')->textInput(['placeholder' => 'Some text']) ?>

    <hr />

    <p>Иногда фразы содержат внешние параметры, например фраза <mark>Показать ещё 3 варианта</mark> должна не только использовать динамическое числовое значение <i>3</i> но и сформировать правильное склонение слова <i>вариант</i>. Не забывайте указывать такие особенности в комментариях, это поможет в будущем сразу понять что и зачем делается.</p>

    <hr/>

    <div class="row">

        <?php foreach ($languages as $k => $item): ?>
            <?= $form->field($model, "translations[$k]", [
                'template'       => '{beginLabel}{labelTitle}{endLabel}{beginWrapper}{input}{hint}{error}{endWrapper}',
                'labelOptions'   => [
                    'class' => 'col-sm-3',
                ],
                'wrapperOptions' => [
                    'class' => 'col-sm-9',
                ],
            ])->textarea([
                'rows'        => 2,
                'placeholder' => 'Translate for ' . $item,
            ])->label($item) ?>
        <?php endforeach; ?>
    </div>

    <hr />

    <?= $form->field($model, 'link')->textInput(['placeholder' => 'http://somehere.com/index.php']) ?>
    <?= $form->field($model, 'comment')->textarea(['placeholder' => 'Small tales']) ?>

    <?= $form->field($model, 'sync_id')->textInput(['placeholder' => 'Unique id'])->label('ID') ?>

    <?= Html::submitButton('SEND', ['class' => 'btn btn-primary hidden']) ?>

    <?php ActiveForm::end() ?>
</div>

<div id="choose_category_box-dialog" title="Choose category" pga-box>
    <p class="box-title h3">Select new category for phrase "<strong></strong>".</p>

    <?= Html::hiddenInput('model_id', null, ['id' => 'model_id']) ?>
    <div class="form-group">
        <?= Html::label('New category', 'category-phrase') ?>
        <?= Html::dropDownList('category-phrase', null, $categories,
            ['id' => 'category-phrase', 'pga-uiselect' => '']) ?>
    </div>
</div>

