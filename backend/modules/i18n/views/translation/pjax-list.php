<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 31.07.2017
 * Time: 23:50
 */

use yii\widgets\Pjax;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var string[] $languages */

?>

<?php Pjax::begin([
    'id'                 => 'translation-list-pjax',
    'timeout'            => 1800,
    'enablePushState'    => true,
    'enableReplaceState' => true,
    'scrollTo'           => false,
    'formSelector'       => '#filter-form',
]) ?>

<?= $this->render('_list', [
    'dataProvider' => $dataProvider,
    'languages'    => $languages,
]) ?>

<?php Pjax::end() ?>

<p>
    Красным цветом фона выделяются фразы, у которых нет переводов для всех языков. Фразы вообще без перевода будут во второй вкладке.
</p>
