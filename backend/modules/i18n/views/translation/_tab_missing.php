<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 14.08.2017
 * Time: 13:52
 */

use yii\widgets\Pjax;
use yii\widgets\ListView;
use yii\helpers\Html;
use yii\bootstrap\Alert;

?>

<div class="row">
    <div class="col-sm-8">
        <?= Alert::widget([
            'body'    => 'В списке только фразы без единого перевода.',
            'options' => [
                'class' => 'alert-warning',
            ],
        ]) ?>
    </div>
    <div class="col-sm-4 handle-btn">
        <?= Html::button('Clear all', ['class' => 'btn btn-block btn-primary', 'pga-clear-miss' => '']) ?>
        <?= Html::button('Update', ['class' => 'btn btn-block btn-default', 'pga-update' => '']) ?>
    </div>
</div>

<?php Pjax::begin([
    'id'                 => 'missing_tab_pjax',
    'enablePushState'    => true,
    'enableReplaceState' => true,
    'scrollTo'           => false,
    'timeout'            => 2000,
]) ?>

<?= ListView::widget([
    'id'               => 'missing-list',
    'dataProvider'     => $dataProvider,
    'itemView'         => '_missing-list-item',
    'layout'           => "{pager}\n{summary}\n{items}\n{pager}",
    'itemOptions'      => [
        'class' => 'list-item',
    ],
    'emptyText'        => '<h3 class="text-warning">Сейчас список пуст. И это очень хорошо.</h3>',
    'emptyTextOptions' => [
        'class' => 'text-center',
    ],
]) ?>

<?php Pjax::end() ?>

<hr />

<p>Для управления этим списком есть кнопки у каждой фразы:
<ul>
    <li><b>Копилка</b> - открывает окно для создания новой фразы с переводами. Выбрать категорию можно другую, но и на
        странице надо будет поменять фразу.
    </li>
    <li><b>Крестик</b> не удаляет фразу из списка, иначе оно появится здесь при повторном обновлении страницы с
        отсутствующим переводом. Так можно добавить фразу в список исключений, оно будет здесь оставаться но не будет
        ошибки перевода на странице сайта.
    </li>
    <li>Кнопка <b>выход</b> появится если добавить фразу в исключения. Она просто вернёт слово обратно в актуальный
        список и его можно редактировать.
    </li>
</ul>
Также стоит учитывать сортировку. Исключения добавляются в конце, но если слово вернуть в обычный список оно окажется выше и его надо будет искать.</p>