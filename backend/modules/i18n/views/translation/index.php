<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 26.07.2017
 * Time: 17:16
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var string[] $categories */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var string[] $languages */
/** @var \backend\modules\i18n\models\I18nForm $model */
/** @var \backend\modules\i18n\models\I18nFormSearch $modelFilter */

\backend\modules\i18n\assets\SAsset::register($this);
\kartik\growl\GrowlAsset::register($this);

?>

<?= \kartik\tabs\TabsX::widget([
    'id'            => 'mani_tab',
    'position'      => \kartik\tabs\TabsX::POS_ABOVE,
    'align'         => \kartik\tabs\TabsX::ALIGN_LEFT,
    'sideways'      => true,
    'encodeLabels'  => false,
    'items'         => [
        [
            'label'   => '<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Actual',
            'content' => $this->render('_tab_actual', [
                'modelFilter'  => $modelFilter,
                'dataProvider' => $dataProvider,
                'languages'    => $languages,
                'categories'   => $categories,
                'model'        => $model,
            ]),
            'active'  => true,
        ],
        [
            'label'       => '<span class="glyphicon glyphicon-fire" aria-hidden="true"></span> Full missing',
            //            'content' => $this->render('_tab_missing'),
            'linkOptions' => [
                'data-url' => Url::toRoute('/i18n/translation/missing-list-tab'),
            ],
        ],
    ],
    'pluginOptions' => [
        'enableCache' => true,
    ],
    'pluginEvents'  => [
        'tabsX.success' => 'function(event, data, status, jqXHR) { 
//            console.log("success", event, data, status, jqXHR); 
            
            jQuery(jQuery(event.target).attr("href")).find("[data-pjax-container]").each(function(index, element){
                var container = jQuery(element);
                container.pjax("a", "#" + container.attr("id"), {
                    push: true,
                    replace: true,
                    timeout: container.data("pjax-timeout") || 750,
                    scrollTo: false
                });
            });
        }',
    ],
]) ?>
