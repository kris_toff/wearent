<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 27.07.2017
 * Time: 11:41
 */

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;

/** @var \yii\web\View $this */
/** @var \backend\modules\i18n\models\I18nSourceMessage $model */
/** @var integer $key the key value associated with the data item */
/** @var integer $index the zero-based index of the data item in the items array returned by $dataProvider */
/** @var string[] $languages */

$c       = count($model->i18nMessages);
$options = ['class' => 'list-item-row'];
if (count($model->i18nMessages) != count($languages)) {
    Html::addCssClass($options, 'warning_missing_message');
}
?>

<?= Html::beginTag('div', $options) ?>
<div class="text_row_title">
    <div class="original_text">
        <?= Html::encode($model->message) ?>
    </div>
    <div class="category_name">
        <?= Html::a(Html::encode($model->category), ['index', 'category' => $model->category]) ?>
        <span class="glyphicon glyphicon-copy btn-xs" title="Скопировать категорию в буфер" pga-uititle></span>
    </div>
    <div class="text-primary">
        ID: <?= Html::encode($model->sync_id) ?>
    </div>
</div>

<div class="list-item-actions">
    <div class="action_buttons">
        <span class="glyphicon glyphicon-copy btn"
                aria-hidden="true"
                title="Скопировать в буфер оригинальную фразу"
                data-target="copy"
                pga-uititle></span>

        <span class="glyphicon glyphicon-pencil btn"
                aria-hidden="true"
                title="Редактировать"
                pga-uititle=""
                data-target="edit"></span>
    </div>
</div>
</div>
<div class="list-item-info">
    <div class="pull-right action_buttons">
        <span class="glyphicon glyphicon-list-alt btn"
                aria-hidden="true"
                title="Изменить категорию сообщения"
                data-target="change_category"
                pga-uititle></span>
        <span class="glyphicon glyphicon-remove btn"
                aria-hidden="true"
                title="Удалить фразу"
                data-target="remove"
                pga-uititle></span>
    </div>
    <div class="table-responsive">
        <p class="h4">Translation:</p>
        <table class="table table-condensed">
            <?php foreach ($languages as $langCode => $langName): ?>
                <?php
                $options = [
                    'lang' => $langCode,
                ];
                $val     = ArrayHelper::getValue($model, ['i18nMessages', $langCode, 'translation']);
                if (empty($val)) {
                    Html::addCssClass($options, 'warning');
                    $val = '<span class="text-muted">Not specified</span>';
                }
                ?>

                <?= Html::beginTag('tr', $options) ?>

                <td><?= $langName ?>:</td>
                <td><?= $val ?></td>

                <?= Html::endTag('tr') ?>
            <?php endforeach; ?>
        </table>
    </div>

    <?php if (!empty($model->link_to_page)): ?>
        <div>
            <p class="h4">Link to source:</p>
            <div class="custom-link">
                <?= $model->link_to_page ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if (!empty($model->comment)): ?>
        <div>
            <p class="h4">Comment:</p>
            <div class="text-muted">
                <?= HTMLPurifier::process($model->comment, [
                    'HTML.Allowed' => 'a[target|href]',
                ]) ?>
            </div>
        </div>
    <?php endif; ?>
    <?= Html::endTag('div') ?>

