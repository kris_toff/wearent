<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 04.09.2017
 * Time: 12:45
 */

use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/** @var \yii\web\View $this */

$model = new \common\models\Language();
?>


<div class="row">
    <div class="col-md-8">
        <?php Pjax::begin([
            'id'                 => 'language_tab_pjax',
            'formSelector'       => '#languages_list_form',
            'enablePushState'    => true,
            'enableReplaceState' => true,
            'scrollTo'           => false,
            'timeout'            => 2000,
        ]) ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'options'      => [
                'data-pjax' => 1,
            ],
        ]) ?>

        <?php Pjax::end() ?>
    </div>
    <div class="col-md-4">
        <?php $form = ActiveForm::begin([
            'id'     => 'languages_list_form',
            'action' => ['languages-tab'],
        ]) ?>

        <?= $form->field($model, 'code')
            ->textInput([
                'placeholder' => 'll-CC',
            ])
            ->hint('<b>ll</b> - двух- или трёхбуквенный код языка в нижнем регистре согласно стандарту ISO-639, а <b>CC</b> - это код страны согласно стандарту ISO-3166.') ?>
        <?= $form->field($model, 'name')->textInput() ?>

        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-block']) ?>

        <?php ActiveForm::end() ?>
    </div>
</div>


