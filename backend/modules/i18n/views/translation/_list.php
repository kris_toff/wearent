<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 28.07.2017
 * Time: 22:39
 */

use yii\widgets\ListView;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var string[] $languages */
?>

<?= ListView::widget([
    'id'               => 'translation-list',
    'dataProvider'     => $dataProvider,
    'itemView'         => '_translate-list-item',
    'layout' => "{pager}\n{summary}\n{items}\n{pager}",
    'itemOptions'      => [
        'class' => 'list-item',
    ],
    'emptyText'        => <<<EMPTY
<h3 class="text-warning">Сейчас не создано ни одного перевода, и вы можете исправить это прямо сейчас!</h3>
<button role="button" aria-label="Add phrase" id="add_phrases_from_empty-btn" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Add phrase</button> 
EMPTY
,
    'emptyTextOptions' => [
        'class' => 'text-center'
    ],
    'viewParams'       => [
        'languages' => $languages,
    ]
]) ?>
