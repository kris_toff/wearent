<?php
$config = [
    'components' => [
        'assetManager' => [
            'class'           => 'yii\web\AssetManager',
            'linkAssets'      => env('LINK_ASSETS'),
            'appendTimestamp' => YII_ENV_DEV,
        ],
        'languages'    => [
            'class' => 'common\components\langs\Languages',
        ],
    ],
    'as locale'  => [
        'class'                   => 'common\behaviors\LocaleBehavior',
        'enablePreferredLanguage' => true,
    ],
    'as cren' => [
        'class' => 'frontend\behaviors\CurrencyBehavior',
    ]
];

if (YII_DEBUG) {
    $config['bootstrap'][]      = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        'allowedIPs' => [
            '127.0.0.1',
            '::1',
            '188.230.42.3', // home ip
            '193.93.217.193', // work
        ],
    ];
}

if (YII_ENV_DEV) {
    $config['modules']['gii'] = [
        'allowedIPs' => [
            '127.0.0.1',
            '::1',
        ],
    ];
}


return $config;
