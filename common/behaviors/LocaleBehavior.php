<?php

namespace common\behaviors;

use yii\base\Behavior;
use Yii;
use yii\web\Application;

/**
 * Class LocaleBehavior
 * @package common\behaviors
 */
class LocaleBehavior extends Behavior {
    /**
     * @var string
     */
    public $cookieName = '_locale';

    /**
     * @var bool
     */
    public $enablePreferredLanguage = true;

    /**
     * @return array
     */
    public function events() {
        return [
            Application::EVENT_BEFORE_REQUEST => 'beforeRequest',
            Application::EVENT_BEFORE_ACTION => 'beforeAction',
        ];
    }

    /**
     * Resolve application language by checking user cookies, preferred language and profile settings
     */
    public function beforeRequest() {
        $locale      = Yii::$app->language;
        $hasCookie   = Yii::$app->getRequest()->getCookies()->has($this->cookieName);
        $forceUpdate = Yii::$app->session->hasFlash('forceUpdateLocale');
//        print_r(\Yii::$app->language);
        if (!Yii::$app->user->isGuest) {
            $locale = Yii::$app->user->identity->userProfile->locale;
        } elseif ($hasCookie && !$forceUpdate) {
            $locale = Yii::$app->getRequest()->getCookies()->getValue($this->cookieName);
        }

        if (empty($locale) && $this->enablePreferredLanguage) {
            $locale = Yii::$app->request->getPreferredLanguage($this->getAvailableLocales());
        }
        if (!empty($locale)) {
            Yii::$app->language = $locale;
        }
//        print 'before|language:'.\Yii::$app->language;
    }

    public function beforeAction(){
        if (!\Yii::$app->user->isGuest
            && strcmp(\Yii::$app->language, \Yii::$app->user->identity->userProfile->locale)) {
            \Yii::$app->user->identity->userProfile->updateAttributes(['locale' => \Yii::$app->language]);
//            print \Yii::$app->language;exit;
        }
        \Yii::$app->formatter->locale = \Yii::$app->language;
//        print 'after|language:'.\Yii::$app->language.';locale:'.\Yii::$app->formatter->locale;
    }

    /**
     * @return array
     */
    protected function getAvailableLocales() {
        return \Yii::$app->languages->codes;
    }
}
