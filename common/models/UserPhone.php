<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.09.2017
 * Time: 10:58
 */

namespace common\models;


use yii\behaviors\AttributeBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

class UserPhone extends ActiveRecord {
    public static function tableName() {
        return '{{%user_phones}}';
    }

    public function rules() {
        return [
            [['phone'], 'trim'],
            [
                'phone',
                'filter',
                'filter' => function($v) {
                    return preg_replace('/\D+/', '', $v);
                },
            ],
            [['phone'], 'required'],
            ['phone', 'string', 'min' => 6, 'max' => 20],
            [
                'phone',
                'unique',
                'filter' => ['is_deleted' => 0, 'is_verify' => 1],
            ],

            [['is_viber', 'is_whatsapp', 'is_telegram'], 'boolean'],
            [['is_viber', 'is_whatsapp', 'is_telegram'], 'default', 'value' => 0],

            [['is_public', 'is_verify', 'is_deleted'], 'boolean'],
            [['is_verify', 'is_deleted'], 'default', 'value' => 0],
            ['is_public', 'default', 'value' => 1],
        ];
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
            [
                'class'              => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ],
            [
                'class'      => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_UPDATE => 'is_verify',
                ],
                'value'      => function($event) {
                    return empty($this->getDirtyAttributes(['phone'])) ? $event->sender->is_verify : 0;
                },
            ],
            'softDeleteBehaviors' => [
                'class'                     => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'is_deleted' => true,
                ],
            ],
        ];
    }
}