<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 11.04.2017
 * Time: 2:13
 */

namespace common\models;


use backend\models\ProductAdvertisement;
use bar\baz\source_with_namespace;
use frontend\models\ProductCurrency;
use mirocow\eav\EavBehavior;
use mirocow\eav\models\EavAttribute;
use mirocow\eav\models\EavAttributeValue;
use yii\base\UnknownPropertyException;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\console\Exception;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yiimodules\categories\models\Categories;

/**
 * Модель товара на сайте
 *
 * @property integer $id
 * @property string  $name            имя товара
 * @property string  $description     описание товара
 * @property bool    $delivery_status true - предлагается доставка
 * @property double  $delivery_radius радиус доставки товара
 * @property double  $delivery_price  цена доставки
 * @property double  $price           цена товара
 * @property double  $pledge_price    цена залога
 * @property int     $min_period      минимальный период аренды
 * @property int     $max_period      максимальный период аренды
 * @property string  $seo_title       заголовок для страницы с товаром
 * @property string  $seo_description описание для страницы с товаром
 * @property string  $seo_h1          название товара для страницы
 * @property string  $seo_keywords    ключевые слова страницы с товаром
 * @property string  $slug            уникальная строка для url товара
 *
 * @property string  $leasing
 *
 * @package common\models
 */
class Product extends ActiveRecord {
    const SCENARIO_DRAFT = 'draft';
    const SCENARIO_MEDIA = 'media';

    const LEASING_DAY   = 1;
    const LEASING_WEEK  = 2;
    const LEASING_MONTH = 3;
    const LEASING_HOUR  = 4;

    const DRAFT     = 0;
    const PROCESSED = 1;
    const OPEN      = 3;
    const BLOCKED   = 6;

    public $isFavorite = false;

    protected $_leasing;

    public static function getLastViewed() {
        $request  = \Yii::$app->request;
        $subQuery = null;
        if (is_null(\Yii::$app->user->identity)) {
            // пользователь без аутентификации, пользуемся только ip и cookies
            if (!is_null($request->userIP)) {
                $subQuery = ProductView::find()
                    ->where(['ip' => $request->userIP]);
            }
        } else {
            $subQuery = ProductView::find()->where(['watcher_id' => \Yii::$app->user->id]);
        }

        if (is_null($subQuery)) {
            return [];
        }

        $subQuery->select('product_id')->orderBy([ProductView::tableName(). '.[[created_at]]' => SORT_DESC])->distinct();
        $subQuery->joinWith('product', false)->andWhere(['is_deleted' => 0, 'status'=>3]);

        return Product::find()
            ->where(['id' => $subQuery])
            ->with('media', 'category', 'currency')
            ->limit(6)->all();
    }

    public static function find() {
        return (new ActiveQuery(get_called_class()))
//            ->select(static::tableName() . '*,{{%product_category}}.[[id]] as [[category_id]]')
//            ->joinWith('category', false, 'LEFT JOIN')
            ;
    }

    public static function getTeamSelection() {
        $query = Product::find()->select(Product::tableName() . '.*')
            ->addSelect('{{pts}}.[[valid_at]],{{pts}}.[[valid_to]]')
            ->join('RIGHT JOIN', '{{%product_team_selection}} pts',
                '{{pts}}.[[product_id]] = ' . Product::tableName() . '.[[id]]')
            ->where(['and', ['>', 'valid_to', time()], ['<', 'valid_at', time()]])
            ->with('photo', 'category', 'currency')
            ->orderBy(['valid_to' => SORT_ASC, 'valid_at' => SORT_ASC])
            ->limit(8);

        return $query->all();
    }

    public static function tableName() {
        return '{{%product}}';
    }

    public function relatedProducts($count, $forSlider = false) {
        $query = Product::find()
            ->where(['category_id' => $this->category_id])
            ->andWhere(['status' =>self::OPEN, 'is_deleted' => 0])
            ->andWhere(['<>', 'id', $this->id])
            ->orderBy(new Expression('RAND()'))
            ->limit($count);

        if ($forSlider) {
            $result = ArrayHelper::toArray($query->all(), [
                Product::className() => [
                    'minislide'     => function($model) {
                        return ArrayHelper::toArray($model->media, [
                            ProductMedia::className() => [
                                'src' => 'link',
                            ],
                        ]);
                    },
                    'product_name'  => 'name',
                    'slug',
                    'category'      => function($model) {
                        return $model->category->name;
                    },
                    'price',
                    'currency'      => function($model) {
                        return is_null($model->currency) ? null : $model->currency->code;
                    },
                    'min_period',
                    'step'          => function($model) {
                        return $model->leasing;
                    },
                    'countComments' => function() {
                        return 5;
                    },
                ],
            ]);
        } else {
            $result = $query->all();
        }

        return $result;
    }

    public function init() {
        parent::init();

        $this->_leasing = [
            self::LEASING_DAY   => 'Day',
            self::LEASING_WEEK  => 'Week',
            self::LEASING_MONTH => 'Month',
            self::LEASING_HOUR  => 'Hour',
        ];
    }

    // simple get properties

    public function behaviors() {
        return [
            TimestampBehavior::className(),
            [
                'class'              => BlameableBehavior::className(),
                'createdByAttribute' => 'creator_id',
                'updatedByAttribute' => false,
            ],
            [
                'class'        => SluggableBehavior::className(),
                'attribute'    => 'name',
                'ensureUnique' => true,
            ],
            'eav' => [
                'class'      => EavBehavior::className(),
                'valueClass' => EavAttributeValue::className(),
            ],
        ];
    }

    public function scenarios() {
        $scenarios                         = parent::scenarios();
        $scenarios[ self::SCENARIO_DRAFT ] = ['name','category_id','pledge_price', 'price'];
        $scenarios[ self::SCENARIO_MEDIA ] = ['media_default'];

        return $scenarios;
    }

    // relational data

    public function rules() {
        return [
            ['category_id', 'required'],
            ['category_id', 'integer', 'min' => 1],
            [
                'category_id',
                'exist',
                'targetClass'     => Categories::className(),
                'targetAttribute' => 'id',
                'filter'          => ['is_active' => 1],
            ],

            ['name', 'string', 'min' => 2, 'max' => 255],
            ['description', 'string'],

            [['place_id', 'lat', 'lng'], 'required', 'except' => self::SCENARIO_DRAFT],
            [['place_id', 'full_address'], 'string', 'max' => 255],
            ['lat', 'double', 'min' => -90, 'max' => 90],
            ['lng', 'double', 'min' => -180, 'max' => 180],

            ['delivery_radius', 'integer', 'min' => 0, 'max' => 100000],
            ['delivery_price', 'double', 'min' => 0],
            [['delivery_radius', 'delivery_price'], 'default', 'value' => 0],

            ['currency_id', 'required', 'except' => self::SCENARIO_DRAFT],
            ['currency_id', 'integer', 'min' => 1],
            [
                'currency_id',
                'exist',
                'targetClass'     => Currency::className(),
                'targetAttribute' => 'id',
                'filter'          => ['is_payment' => 1],
            ],

            [['price', 'pledge_price'], 'double', 'min' => 0],
            [['price', 'pledge_price'], 'default', 'value' => 0],
            [
                ['lease_time', 'lease_max_time'],
                'in',
                'range' => [self::LEASING_DAY, self::LEASING_WEEK, self::LEASING_MONTH, self::LEASING_HOUR],
            ],
            [['discount_price', 'discount_pledge'], 'double', 'min' => 0, 'max' => 100],
            [['discount_price', 'discount_pledge'], 'default', 'value' => 0],

            ['min_period', 'integer', 'min' => 1],
            ['max_period', 'integer'],
            ['min_period', 'default', 'value' => 1],
            ['max_period', 'default', 'value' => null],

            ['media_default', 'integer', 'min' => 1],
            [
                'media_default',
                'exist',
                'targetClass'     => ProductMedia::className(),
                'targetAttribute' => 'id',
                'filter'          => ['product_id' => $this->id, 'is_deleted' => 0],
            ],

            ['is_deleted', 'boolean'],
            ['is_deleted', 'default', 'value' => false],

            ['autoorder', 'in', 'range' => [0, 1, 2]],
            ['autoorder_rate', 'required', 'when' => function($model) { return (int)$model->autoorder == 2; }],
            ['autoorder_rate', 'integer', 'min' => 0],
            [['autoorder', 'autoorder_rate'], 'default', 'value' => 0],

            ['is_only_verify', 'boolean'],
            ['is_only_verify', 'default', 'value' => false],

            ['prenotify', 'integer', 'min' => 0],
            ['restbetweenbooking', 'integer', 'min' => 0],
            [['prenotify', 'restbetweenbooking'], 'default', 'value' => 0],

            ['beforehand_rent', 'integer', 'min' => 90],
            ['beforehand_rent', 'default', 'value' => 90],

            // time
            [
                ['time_from', 'time_to'],
                'filter',
                'filter' => function($v) {
                    $e = explode(':', $v);
                    if (count($e) > 2) {
                        return implode(':', array_slice($e, 0, 2));
                    } else {
                        return $v;
                    }
                },
            ],
            [['time_from', 'time_to'], 'time', 'format' => 'HH:mm'],
            [['time_from', 'time_to'], 'default', 'value' => null],
            ['timezone', 'integer'],

            ['status', 'in', 'range' => [self::OPEN, self::BLOCKED, self::PROCESSED, self::DRAFT]],
            ['status', 'default', 'value' => self::DRAFT],

            // seo
        ];
    }

    public function getEavAttributes() {
        $category = $this->category;
        $ids      = [$category->id];
        $pnt      = $category->parentCategory;
        while (!is_null($pnt)) {
            $ids[] = $pnt->id;
            $pnt   = $pnt->parentCategory;
        }
        $ids = array_unique($ids);

        return EavAttribute::find()
            ->joinWith('entity')
            ->where([
                'eav_entity.categoryId' => $ids,
                'entityModel'           => self::className(),
            ]);
    }

    public function getLeasing() {
        return ArrayHelper::getValue($this->_leasing, $this->lease_time, '');
    }

    public function getCategory() {
//        return $this->hasMany(Categories::className(), ['id' => 'category_id'])
//            ->viaTable('{{%product_category}}', ['product_id' => 'id']);
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    public function getCurrency() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    public function getMedia() {
        return $this->hasMany(ProductMedia::className(), ['product_id' => 'id'])
            ->where(['is_deleted' => 0]);
    }

    public function getAdvertisement() {
        return $this->hasMany(ProductAdvertisement::className(), ['product_id' => 'id'])
            ->indexBy('id');
    }

    public function getAuthor() {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    public function getEavValue() {
        return $this->hasMany(EavAttributeValue::className(), ['entityId' => 'id']);
    }

    public function getAmazing() {
        return $this->hasMany(ProductAmazing::className(), ['product_id' => 'id']);
    }

    public function getDelivery_status() {
        return (int)$this->delivery_radius > 0;
    }

    public function getIs_favourite() {
        return !\Yii::$app->user->isGuest
            && (new Query())->from('{{%product_favorite}}')->where([
                'product_id' => $this->id,
                'user_id'    => \Yii::$app->user->id,
            ])->exists();
    }

    public function getIs_amazing() {
        return !\Yii::$app->user->isGuest
            && ProductAmazing::find()->where([
                'product_id' => $this->id,
                'rater_id'   => \Yii::$app->user->id,
            ])->exists();
    }

    /**
     * Возвращает массив с ценами, где ключ это минимальное количество часов для новой цены а значение это цена в
     * валюте товара
     *
     * @return array
     */
    public function getPrice_conditions() {
        $e = [1 => 24, 2 => 168, 3 => 720];
        $a = [];

        foreach ((new Query())->from('{{%product_price_condition}}')
            ->where(['product_id' => $this->id])
            ->all() as $item) {
            $a[ $item['val'] * ArrayHelper::getValue($e, $item['lease_time'],
                1) ] = $this->currency_id > 0 ? ProductCurrency::getActualPrice($item['price'], (integer)$this->currency_id) : 0;
        }
        ksort($a, SORT_NATURAL);

        return $a;
    }

    public function getPrice_raw_conditions($convert = true) {
        $e = [1 => 24, 2 => 168, 3 => 720];
        $a = (new Query())->from('{{%product_price_condition}}')
            ->where(['product_id' => $this->id])
            ->all();
        $b = [];

        foreach ($a as $key => $item) {
            $b[ $key ] = $item['val'] * ArrayHelper::getValue($e, $item['lease_time'], 1);
        }
        array_multisort($b, SORT_ASC, SORT_NATURAL, $a);

        return $a;
    }

    public function formName() {
        $reflector = new \ReflectionClass(new self());
        return $reflector->getShortName();
    }
}