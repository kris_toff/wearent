<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 04.09.2017
 * Time: 23:32
 */

namespace common\models;


use yii\db\ActiveRecord;

class UserLanguage extends ActiveRecord {
    public static function tableName() {
        return '{{%user_languages}}';
    }

    public function getUserProfile(){
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
    }


}