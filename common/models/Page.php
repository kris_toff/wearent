<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string  $slug
 * @property string  $title
 * @property string  $description
 * @property string  $keywords
 * @property string  $body
 * @property string  $view
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Page extends ActiveRecord {
    const STATUS_DRAFT     = 0;
    const STATUS_PUBLISHED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%page}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            'slug' => [
                'class'        => SluggableBehavior::className(),
                'attribute'    => 'title',
                'ensureUnique' => true,
                'immutable'    => true,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'body', 'h1', 'description', 'keywords', 'slug'], 'trim'],
            [['title', 'body'], 'required'],

            [['body'], 'string'],
            [['status'], 'integer'],

            [['slug', 'description'], 'string', 'max' => 2048],
            [['slug'], 'unique'],

            [['title'], 'string', 'max' => 512],
            [['view', 'h1', 'keywords'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'          => Yii::t('common', 'ID'),
            'h1'          => Yii::t('common', 'H1 tag'),
            'description' => Yii::t('common', 'Description'),
            'keywords'    => Yii::t('common', 'Keywords'),
            'slug'        => Yii::t('common', 'Slug'),
            'title'       => Yii::t('common', 'Title'),
            'body'        => Yii::t('common', 'Body'),
            'view'        => Yii::t('common', 'Page View'),
            'status'      => Yii::t('common', 'Active'),
            'created_at'  => Yii::t('common', 'Created At'),
            'updated_at'  => Yii::t('common', 'Updated At'),
        ];
    }
}
