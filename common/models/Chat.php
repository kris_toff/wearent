<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.08.2017
 * Time: 9:54
 */

namespace common\models;


use yii\behaviors\AttributeBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

class Chat extends ActiveRecord {
    public $message;

    public static function tableName() {
        return '{{%chat}}';
    }

    public function rules() {
        return [
            [['subject',], 'trim'],
            [['subject', 'to_id'], 'required'],

            ['subject', 'string', 'min' => 2],

            [
                'to_id',
                'exist',
                'targetClass'     => User::className(),
                'targetAttribute' => 'id',
                'filter'          => ['status' => 2],
                'when'            => function($model) { return is_numeric($model->to_id) && $model->to_id > 0; },
            ],
            // если получатель не пользователь с цифровым id, то должно быть 's' как служба поддержки
            ['to_id', 'in', 'range' => ['s'], 'when' => function($model) { return !is_numeric($model->to_id); }],

            ['product_id', 'integer', 'min' => 1],
            ['product_id', 'exist', 'targetClass' => Product::className(), 'targetAttribute' => 'id'],

            ['is_deleted', 'default', 'value' => 0],
            ['is_deleted', 'boolean'],
        ];
    }

    public function getMessages() {
        return $this->hasMany(ChatMessage::className(), ['chat_id' => 'id']);
    }

    public function getInterlocutor() {
        return $this->hasOne(User::className(), ['id' => 'to_id']);
    }

    public function getAuthor() {
        return $this->hasOne(User::className(), ['id' => 'from_id']);
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
            [
                'class'      => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'lang',
                ],
                'value'      => \Yii::$app->language,
            ],
            'softDeleteBehavior' => [
                'class'                     => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => ['is_deleted' => true],
            ],
            [
                'class'              => BlameableBehavior::className(),
                'createdByAttribute' => 'from_id',
                'updatedByAttribute' => false,
            ],
        ];
    }

}