<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 10.09.2017
 * Time: 12:40
 */

namespace common\models;


use Imagine\Image\Box;
use Imagine\Image\Point;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use trntv\filekit\behaviors\UploadBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

class UserAvatar extends ActiveRecord {
    public $file;

    public static function tableName() {
        return '{{%user_avatars}}';
    }

    public function beforeSave($insert) {
        $path = '/uploads/user_avatars/id' . \Yii::$app->user->id;
        FileHelper::createDirectory(\Yii::getAlias('@webroot') . $path);

        $filename_without_extension = \Yii::$app->security->generateRandomString(12);
        $filename                   = "{$filename_without_extension}.{$this->file->extension}";
        $rel_path                   = "{$path}/{$filename}";
        $rel_thumbnail_path         = "{$path}/{$filename_without_extension}.thn.{$this->file->extension}";

        if (!$this->file->saveAs(\Yii::getAlias('@webroot') . $rel_path)) {
            return false;
        };

        // modify avatar according to the crop parameters
        $gets            = array_intersect_key(\Yii::$app->request->get(),
            ['x' => 0, 'y' => 0, 'rotate' => 0, 'width' => 0, 'height' => 0]);
        $file_dimensions = array_combine(['width', 'height'],
            array_slice(getimagesize(\Yii::getAlias('@webroot') . $rel_path), 0, 2));
        $params          = array_merge(['rotate' => 0, 'x' => 0, 'y' => 0], $file_dimensions, $gets);

        Image::getImagine()
            ->open(\Yii::getAlias('@webroot') . $rel_path)
            ->rotate(ArrayHelper::getValue($params, 'rotate', 0))
            ->crop(new Point($params['x'], $params['y']), new Box($params['width'], $params['height']))
            ->save(\Yii::getAlias('@webroot') . $rel_thumbnail_path);

        $this->path = $rel_thumbnail_path;

        return parent::beforeSave($insert);
    }

    public function rules() {
        return [
            ['file', 'required'],
            ['file', 'image'],

            ['path', 'string'],
            [['is_main', 'is_deleted'], 'boolean'],
            [['is_main', 'is_deleted'], 'default', 'value' => 0],
        ];
    }


    /**
     * @return array
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            [
                'class'              => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ],
            'softDeleteBehavior' => [
                'class'                     => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'is_deleted' => true,
                ],
            ],
        ];
    }

}