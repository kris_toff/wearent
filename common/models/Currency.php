<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 11.04.2017
 * Time: 15:43
 */

namespace common\models;


use backend\models\CurrencyExchange;
use yii\base\ErrorException;
use yii\base\InvalidParamException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use yii\web\UploadedFile;

/**
 * Class Currency
 * @package common\models
 *
 * @property UploadedFile $file
 */
class Currency extends ActiveRecord {
    public $file;

    public static function getList() {
        $query = self::find()->select('name,code');

        return ArrayHelper::map($query->all(), 'code', 'name');
    }

    public static function getCurrentSymbol() {
        // Create a NumberFormatter
        $formatter = new \NumberFormatter(\Yii::$app->formatter->locale, \NumberFormatter::CURRENCY);

        // Prevent any extra spaces, etc. in formatted currency
        $formatter->setPattern('¤');

        // Prevent significant digits (e.g. cents) in formatted currency
        $formatter->setAttribute(\NumberFormatter::MAX_SIGNIFICANT_DIGITS, 0);

        // Get the formatted price for '0'
        $formattedPrice = $formatter->formatCurrency(0, \Yii::$app->formatter->currencyCode);

        // Strip out the zero digit to get the currency symbol
        $zero           = $formatter->getSymbol(\NumberFormatter::ZERO_DIGIT_SYMBOL);
        $currencySymbol = str_replace($zero, '', $formattedPrice);

        return $currencySymbol;
    }

    public function rules() {
        return [
            [['name', 'code', 'digital_code'], 'trim'],
            [['name', 'code'], 'required'],

            ['name', 'string', 'min' => 2, 'max' => 140],

            [['code', 'digital_code'], 'string', 'length' => 3],
            ['code', 'match', 'pattern' => '/[A-Z]{3}/i'],
            ['digital_code', 'match', 'pattern' => '/\d{3}/'],

            ['code', 'filter', 'filter' => 'strtoupper'],
            [
                ['code', 'digital_code'],
                'unique',
                'when' => function($model) {
                    return $model->isNewRecord;
                },
            ],
            ['digital_code', 'default', 'value' => null],

            ['is_payment', 'boolean'],
            ['is_payment', 'default', 'value' => 0],

            ['file', 'image'],
        ];

    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);

        if (!empty($this->file)) {
            \Yii::$app->response->headers->add('add', 'ok');

            $rootpath = \Yii::getAlias('@frontend/web');
            $filepath = 'img/currencies/icon';

            // адреса для файла
            $filename = Inflector::transliterate(strtolower($this->code));
            $filename = Inflector::variablize($filename);
            $filename .= '.' . $this->file->extension;

            // создание директории для картинок
            if (FileHelper::createDirectory("{$rootpath}/{$filepath}")) {
                $fullpath = "{$rootpath}/{$filepath}/{$filename}";

                // удалить уже существующие картинки для валюты
                if (!file_exists($fullpath) || unlink($fullpath)) {
                    // сохранить иконку валюты

                    if ($this->file->saveAs($fullpath)) {
                        // обновить запись в БД
                        $this->updateAttributes(['icon' => "/{$filepath}/{$filename}"]);
                    }
                }
            }
        }
    }

    public function getExchangeTo() {
        return $this->hasMany(CurrencyExchange::className(), ['currency_withdraw' => 'code']);
    }

    public function getExchangeFrom() {
        return $this->hasMany(CurrencyExchange::className(), ['currency_receive' => 'code']);
    }
}