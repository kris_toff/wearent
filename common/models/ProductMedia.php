<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 17.07.2017
 * Time: 18:20
 */

namespace common\models;


use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii2tech\ar\position\PositionBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

class ProductMedia extends ActiveRecord {
    public static function tableName() {
        return '{{%product_media}}';
    }

    public function rules() {
        return [
            ['link', 'trim'],
            [['product_id', 'link'], 'required'],
            ['product_id', 'integer', 'min' => 1],
            [
                'product_id',
                'exist',
                'targetClass'     => Product::className(),
                'targetAttribute' => 'id',
                'filter'          => ['is_deleted' => 0],
            ],
            ['link', 'string', 'max' => 255],
            ['is_deleted', 'boolean'],
            ['is_deleted', 'default', 'value' => 0],
        ];
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
            'positionBehavior'   => [
                'class'           => PositionBehavior::className(),
                'groupAttributes' => [
                    'product_id',
                    'is_deleted',
                ],
            ],
            'softDeleteBehavior' => [
                'class'                     => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'is_deleted' => true,
                ],
            ],
        ];
    }

    public function getType() {
        $extension = mb_substr($this->link, mb_strrpos($this->link, '.', 0, 'UTF-8') + 1, null, 'UTF-8');
        if (in_array($extension, ['png', 'jpg', 'gif'], true)) {
            return 'image';
        } elseif (in_array($extension, ['mp4'], true)) {
            return 'video';
        } else {
            return 'unknown';
        }
    }
}