<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 24.06.2017
 * Time: 13:57
 */

namespace common\models;


use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class ProductAmazing extends ActiveRecord {
    public static function tableName() {
        return '{{%product_amazing}}';
    }

    public function rules() {
        return [
            ['product_id', 'required'],
            ['product_id', 'integer'],
            ['product_id', 'exist', 'targetClass' => Product::className(), 'targetAttribute' => 'id'],
        ];
    }

    public function behaviors() {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
            [
                'class'              => BlameableBehavior::className(),
                'createdByAttribute' => 'rater_id',
                'updatedByAttribute' => false,
            ],
        ];
    }

    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}