<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.09.2017
 * Time: 10:45
 */

namespace common\models;


use yii\behaviors\AttributeBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

class UserEmail extends ActiveRecord {
    public $is_main;

    public static function tableName() {
        return '{{%user_emails}}';
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    public function rules() {
        return [
            ['email', 'required'],
            ['email', 'email'],
            [
                'email',
                'unique',
                'filter' => ['is_verify' => 1, 'is_deleted' => 0],
            ],
            [
                'email',
                'unique',
                'targetClass'     => User::className(),
                'targetAttribute' => 'email',
            ],

            [['is_verify', 'is_deleted'], 'boolean'],
            [['is_verify', 'is_deleted'], 'default', 'value' => 0],
        ];
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
            [
                'class'              => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ],
            [
                'class'      => AttributeBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_UPDATE => 'is_verify',
                ],
                'value'      => function($event) {
                    return empty($this->getDirtyAttributes(['email'])) ? $event->sender->is_verify : 0;
                },
            ],
            'softDeleteBehaviors' => [
                'class'                     => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'is_deleted' => true,
                ],
            ],
        ];
    }


    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);

        // поменялся адрес, нужно отправить письмо для подтверждения
        if (key_exists('email', $changedAttributes)) {
            $hash  = \Yii::$app->security->generateRandomString(9);
            $token = crypt($hash, $this->user_id . ':' . $this->email);

            $this->updateAttributes(['token' => $hash]);
            \Yii::$app->mailer->compose('confirm_new_email', [
                'token' => $token,
            ])
                ->setTo($this->email)
                ->setSubject(\Yii::t('UI', '$EMAIL_SUBJECT_CONFIRM_USER_EMAIL_ADDRESS$'))
                ->send();
        }
    }
}