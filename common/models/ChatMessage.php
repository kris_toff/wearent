<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 06.08.2017
 * Time: 9:55
 */

namespace common\models;


use yii\behaviors\AttributeBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class ChatMessage extends ActiveRecord {
    public static function tableName() {
        return '{{%chat_message}}';
    }

    public function getChat() {
        return $this->hasOne(Chat::className(), ['id' => 'chat_id']);
    }

    public function getAuthor() {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    public function rules() {
        $uid = \Yii::$app->user->id;

        return [
            [['chat_id'], 'required'],
            ['chat_id', 'integer', 'min' => 1],
            [
                'chat_id',
                'exist',
                'targetClass'     => Chat::className(),
                'targetAttribute' => 'id',
                'filter'          => [
                    'and',
                    ['is_deleted' => 0],
                    ['or', ['from_id' => $uid], ['to_id' => $uid]],
                ],
            ],

            ['message', 'trim'],
            ['message', 'string', 'min' => 1, 'max' => 65353],

            [['is_delivered', 'is_viewed'], 'boolean'],
            [['is_delivered', 'is_viewed'], 'default', 'value' => 0],
        ];
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
            [
                'class'              => BlameableBehavior::className(),
                'createdByAttribute' => 'author_id',
                'updatedByAttribute' => false,
            ],
        ];
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);

        /** @var Chat $c */
        $c = $this->chat;
        $c->updateAttributes(['updated_at' => time()]);
    }
}