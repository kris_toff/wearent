<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 14.09.2017
 * Time: 16:59
 */

namespace common\models;


use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

class UserVideo extends ActiveRecord {
    public $file;

    public static function tableName() {
        return '{{%user_video}}';
    }

    public function rules() {
        return [
            ['file', 'required'],
            ['file', 'file'],

            ['path', 'string'],
            [['is_main', 'is_deleted'], 'boolean'],
            ['is_main', 'default', 'value' => 1],
            ['is_deleted', 'default', 'value' => 0],
        ];
    }

    public function afterValidate() {
        parent::afterValidate();

        $path = '/uploads/user_video/id' . \Yii::$app->user->id;
        FileHelper::createDirectory(\Yii::getAlias('@webroot') . $path);

        $filename_without_extension = \Yii::$app->security->generateRandomString(12);
        $filename                   = "{$filename_without_extension}.{$this->file->extension}";
        $rel_path                   = "{$path}/{$filename}";

        if (!$this->file->saveAs(\Yii::getAlias('@webroot') . $rel_path)) {
            return false;
        };

        $this->path = $rel_path;
        $this->type = $this->file->type;
    }


    public function behaviors() {
        return [
            TimestampBehavior::className(),
            [
                'class'              => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ],
            'softDeleteBehavior' => [
                'class'                     => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'is_deleted' => true,
                ],
            ],
        ];
    }

    public function beforeSave($insert) {
        self::updateAll(['is_main' => 0], ['user_id' => \Yii::$app->user->id, 'is_deleted' => 0]);

        return parent::beforeSave($insert);
    }


}