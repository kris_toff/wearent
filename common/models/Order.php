<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 19.12.2017
 * Time: 0:53
 */

namespace common\models;


use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Order extends ActiveRecord {
    public $product_id;

    public static function tableName() {
        return '{{%orders}}';
    }

    public function rules() {
        return [
            [['product_id', 'start', 'stop'], 'required'],
            [
                'product_id',
                'exist',
                'targetClass'     => Product::className(),
                'targetAttribute' => 'id',
                'filter'          => ['is_deleted' => 0, 'status' => Product::OPEN],
            ],

            [['start', 'stop'], 'integer', 'min' => time()],

            ['comment', 'trim'],
            ['comment', 'string', 'max' => 12001],
            ['comment', 'default', 'value' => null],
        ];
    }

    public function behaviors() {
        return [
            ['class' => TimestampBehavior::className()],
            [
                'class'              => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
            ],
        ];
    }

    public function beforeSave($insert) {
        $model = Product::findOne($this->product_id);

        $data               = $model->attributes;
        $data               = array_diff_key($data, array_flip(['id', 'is_deleted', 'status']));
        $data['product_id'] = $model['id'];

        if (!\Yii::$app->db->createCommand()->insert('{{%product_orderstamp}}', $data)->execute()) {
            return false;
        }

        $this->updateAttributes(['product_data_id' => \Yii::$app->db->lastInsertID]);

        return parent::beforeSave($insert);
    }

}