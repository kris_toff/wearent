<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 04.09.2017
 * Time: 11:32
 */

namespace common\models;


use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class Country extends ActiveRecord {
    private static $_list;

    public static function tableName() {
        return '{{%country}}';
    }

    public static function getList() {
        if (empty(self::$_list)) {
            self::$_list = self::find()->all();
        }

        return self::$_list;
    }
}