<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 24.05.2017
 * Time: 14:48
 */

namespace common\models;


use yii\db\ActiveRecord;

class ProductView extends ActiveRecord {
    public static function tableName() {
        return '{{%product_views}}';
    }

    public function getProduct(){
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'watcher_id']);
    }

}