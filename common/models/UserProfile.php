<?php

namespace common\models;

use frontend\models\UserSocial;
use trntv\filekit\behaviors\UploadBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user_profile".
 *
 * @property integer $user_id
 * @property integer $locale
 * @property string  $firstname
 * @property string  $middlename
 * @property string  $lastname
 * @property string  $picture
 * @property string  $avatar
 * @property string  $avatar_path
 * @property string  $avatar_base_url
 * @property integer $gender
 *
 * @property User    $user
 */
class UserProfile extends ActiveRecord {
    const GENDER_MALE   = 1;
    const GENDER_FEMALE = 2;

    public $isBusiness = false;

    public $language = "";

    /**
     * @var
     */
    public $picture;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%user_profile}}';
    }

    public function afterFind() {
        parent::afterFind();

        $this->language = implode(',', ArrayHelper::getColumn($this->languages, 'lang_code'));
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['user_id'], 'required'],
            [['user_id', 'gender'], 'integer'],
            [['gender'], 'in', 'range' => [null, self::GENDER_FEMALE, self::GENDER_MALE]],
            [['firstname', 'lastname', 'avatar_path', 'avatar_base_url'], 'string', 'max' => 255],
            ['locale', 'default', 'value' => \Yii::$app->language],
            ['locale', 'in', 'range' => \Yii::$app->languages->codes],
            ['picture', 'safe'],

            ['birthday_day', 'integer', 'min' => 1, 'max' => 31],
            ['birthday_month', 'integer', 'min' => 1, 'max' => 12],
            ['birthday_year', 'integer', 'min' => 1900, 'max' => date('Y')],

            ['country', 'string', 'length' => 2],
            ['city', 'string', 'min' => 2, 'max' => 99],
            ['company', 'string', 'min' => 2, 'max' => 99],

            ['description', 'string', 'min' => 2, 'max' => 65535],
            ['language', 'string'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return null|string
     */
    public function getFullName() {
        if ($this->firstname || $this->lastname) {
            return implode(' ', [$this->firstname, $this->lastname]);
        }

        return null;
    }

    public function getLanguages() {
        return $this->hasMany(UserLanguage::className(), ['user_id' => 'user_id']);
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);

        // сохранить языки пользователя
        $c = ArrayHelper::getColumn($this->languages, 'lang_code');
        $l = array_filter(explode(',', $this->language));

        $a = array_diff($l, $c);
        $d = array_diff($c, $l);

        foreach ($a as $item) {
            $m            = new UserLanguage();
            $m->lang_code = $item;

            if ($m->validate()) {
                $m->link('userProfile', $this);
            }
        }
        UserLanguage::deleteAll(['user_id' => $this->user_id, 'lang_code' => $d]);
//        exit;
    }

    public function getEmails() {
        return $this->hasMany(UserEmail::className(), ['user_id' => 'user_id']);
    }

    public function getPhones() {
        return $this->hasMany(UserPhone::className(), ['user_id' => 'user_id']);
    }

    public function getAvatars() {
        return $this->hasMany(UserAvatar::className(), ['user_id' => 'user_id'])->andWhere(['is_deleted' => 0]);
    }

    public function getVideo() {
        return $this->hasMany(UserVideo::className(), ['user_id' => 'user_id'])->andWhere(['is_deleted' => 0]);
    }

    public function getSocial() {
        return $this->hasMany(UserSocial::className(), ['user_id' => 'user_id']);
    }

    public function getCurrency() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }
    public function getCountry(){
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
}
