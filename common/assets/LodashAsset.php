<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 27.07.2017
 * Time: 16:12
 */

namespace common\assets;


use yii\web\AssetBundle;

class LodashAsset extends AssetBundle {

    public $sourcePath = '@bower/lodash/dist';

    public $js = [
        'lodash.min.js',
    ];
}