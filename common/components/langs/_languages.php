<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 01.07.2017
 * Time: 18:02
 */

return [
    [
        'code' => 'en-GB', // англ (британский)
        'name' => 'Английский (Британия)',
    ],
    [
        'code' => 'ru-RU',
        'name' => 'Русский',
    ],
    [
        'code' => 'uk-UA',
        'name' => 'Украинский',
    ],
    //    'es'    => 'Español',
    //    'vi'    => 'Tiếng Việt',
    //    'zh-CN' => '简体中文',
    //    'pl-PL' => 'Polski (PL)',
];
