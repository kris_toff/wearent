<?php
/**
 * Created by PhpStorm.
 * User: Planess group
 * Date: 04.09.2017
 * Time: 15:49
 */

namespace common\components\langs;


use yii\base\ErrorException;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class Languages extends Object {
    private $_data;

    public function init() {
        parent::init();

        $this->_data = require '_languages.php';
        if (!in_array(\Yii::$app->language, array_column($this->_data, 'code'))) {
            throw new ErrorException('Language ' . \Yii::$app->language . ' not included in full list ' . implode(',',
                    array_column($this->_data, 'code')));
        }
    }

    public function getCodes() {
        return array_column($this->_data, 'code');
    }

    public function getList(){
        return ArrayHelper::map($this->_data, 'code', 'name');
    }

    public function getCount(){
        return count($this->_data);
    }
}